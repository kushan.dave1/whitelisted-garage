package whitelisted.garage.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Observer
import kotlinx.coroutines.*
import whitelisted.garage.R
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.AddServiceByOwnReq
import whitelisted.garage.network.*
import whitelisted.garage.network.error.errorCode
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.AppStorePreferences
import java.lang.ref.WeakReference
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException
import kotlin.Result
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

@Suppress("UNUSED_PARAMETER")
open class BaseViewModel(private val app: Application, val apiRepo: APIRepository) :
    AndroidViewModel(app), CoroutineScope {
    private val failedJobIdList = mutableSetOf<String>()
    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    override fun onCleared() {
        super.onCleared()
        connectionLiveData.removeObserver(connectionObserver)
        failedJobIdList.clear()
        job.cancel()
    }

    private fun addJobId(jobId: String) {
        failedJobIdList.add(jobId)
    }

    private fun removeJobId(jobId: String) {
        failedJobIdList.remove(jobId)
    }

    protected fun CoroutineScope.launch(
        context: CoroutineContext = EmptyCoroutineContext,
        start: CoroutineStart = CoroutineStart.DEFAULT,
        doOnComplete: (throwable: Throwable?) -> Unit = {},
        id: String,
        block: suspend CoroutineScope.() -> Unit
    ): Job {
        return launch(context, start, block).apply {
            invokeOnCompletion {
                if (it != null) addJobId(id)
                else removeJobId(id)

                doOnComplete(it)
            }
        }
    }

    protected fun <T> CoroutineScope.async(
        context: CoroutineContext = EmptyCoroutineContext,
        start: CoroutineStart = CoroutineStart.DEFAULT,
        doOnComplete: (throwable: Throwable?) -> Unit = {},
        id: String,
        block: suspend CoroutineScope.() -> T
    ): Deferred<T> {
        return async(context, start, block).apply {
            invokeOnCompletion {
                if (it != null) addJobId(id)
                else removeJobId(id)
                doOnComplete(it)
            }
        }
    }

    protected fun CoroutineScope.addToFailedJobStack(throwable: Throwable? = null) {
        coroutineContext.cancel()
    }

    private fun onConnectedToNetwork() {
        resumeFailedJob(failedJobIdList)
    }

    open fun resumeFailedJob(jobIdList: Iterable<String>) {}

    private val connectionLiveData = ConnectionLiveData(WeakReference(app))
    private val connectionObserver = Observer<Boolean> { isConnected ->
        if (isConnected) onConnectedToNetwork()
    }

    init {
        connectionLiveData.observeForever(connectionObserver)
    }

    open fun doOnViewAttached() {}

    protected fun <T> BaseResult.Failure<*>.toFailureDataState() = when (exception) {
        is UnknownHostException, is SocketException, is SocketTimeoutException, is TimeoutException, is ConnectException -> DataState.Failure(
            null,
            app.getString(R.string.no_connection),
            emptyMap()
        )
        is RequestException -> DataState.Failure(
            exception.errorCode,
            if (exception.localizedMessage.isEmpty()) app.getString(R.string.default_error_network) else exception.localizedMessage,
            exception.errorMap
        )
        else -> DataState.Failure<T>(
            exception.errorCode,
            app.getString(R.string.default_error_network),
            emptyMap()
        )
    }

    fun getSharedPreference(): AppStorePreferences {
        return apiRepo.getSharedPreferences()
    }

    fun saveRecentSearch(searchText: String) {
        val searches = getRecentSearches().toMutableList()
        if(searches.contains(searchText)) return
        searches.add(0, searchText)
        val searchesAsString = searches
            .take(if (searches.size > 4) 4 else searches.size)
            .joinToString("#") { it }
        getSharedPreference().putString(
            app.applicationContext,
            AppENUM.RECENT_SEARCHES,
            searchesAsString
        )
    }

    fun getRecentSearches() = getSharedPreference()
        .getString(app.applicationContext, AppENUM.RECENT_SEARCHES)
        .split("#")
        .filter { it.isNotEmpty() }

    fun putStringSharedPreference(key: String, value: String) {
        getSharedPreference().putString(app.applicationContext, key, value)
    }

    fun getStringSharedPreference(key: String, defaultValue: String? = null): String {
        return getSharedPreference().getString(app.applicationContext, key, defaultValue)
    }

    fun getIntSharedPreference(key: String): Int {
        return getSharedPreference().getInt(app.applicationContext, key)
    }

    fun getFloatSharedPreference(key: String, defaultValue: Float?): Float {
        return getSharedPreference().getFloat(app.applicationContext, key, defaultValue)
    }

    fun putIntSharedPreference(key: String, value: Int) {
        getSharedPreference().putInt(app.applicationContext, key, value)
    }

    fun putFloatSharedPreference(key: String, value: Float) {
        getSharedPreference().putFloat(app.applicationContext, key, value)
    }

    fun putBooleanSharedPreference(key: String, value: Boolean) {
        getSharedPreference().putBoolean(app.applicationContext, key, value)
    }

    fun getBooleanSharedPreference(key: String, def: Boolean? = false): Boolean {
        return getSharedPreference().getBoolean(app.applicationContext, key, def ?: false)
    }

    fun clearData() {
        getSharedPreference().clearAllSharedPrefrences(app.applicationContext)
    }

    suspend fun addTwoWheelerServiceByOwn(searchText: String): Boolean =
        withContext(Dispatchers.IO) {
            val path = getBikeServicePath()
            return@withContext when (apiRepo.addBikeServiceByOwn(
                path,
                AddServiceByOwnReq(searchText)
            ).awaitAndGet()) {
                is whitelisted.garage.network.Result.Success<*> -> true
                else -> false
            }
        }

    fun getBikeServicePath(): String {
        val shop = getStringSharedPreference(
            AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT
        )
        return when (shop) {
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> AppENUM.GET_ALL_BIKE_SERVICES
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> AppENUM.BIKE_ACCESSORIES_CATALOGUE
            else -> AppENUM.RETAILERS_BIKE_CATALOGUE
        }
    }

}