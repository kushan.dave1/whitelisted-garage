package whitelisted.garage.base

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import whitelisted.garage.R

class NetworkErrorView : LinearLayout, View.OnClickListener {
    private var networkMessageView: TextView? = null
    private var tryAgainButton: Button? = null
    private var onClickListener: View.OnClickListener? = null

    constructor(context: Context) : super(context) {
        initView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView()
    }

    private fun initView() {
        LayoutInflater.from(this.context)
            .inflate(R.layout.material_network_error_layout, this, true)
        this.networkMessageView = findViewById(R.id.networkMessageView)
        this.tryAgainButton = findViewById(R.id.tryAgainButton)
        this.tryAgainButton?.setOnClickListener(this)
    }

    override fun setOnClickListener(onClickListener: View.OnClickListener?) {
        this.onClickListener = onClickListener
    }

    override fun onClick(view: View) {
        onClickListener?.onClick(view)
    }

}
