package whitelisted.garage.base

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.google.firebase.storage.StorageException
import com.google.firebase.storage.StorageException.ERROR_OBJECT_NOT_FOUND
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.fragmen_inventory.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.viewmodels.InventoryFragmentViewModel
import java.io.IOException

abstract class BaseRecordFragment : BaseFragment() {
    lateinit var storageRef: StorageReference
    var recordingRef: StorageReference? = null
    protected var isStart = false

    var outputFile: String = activity?.filesDir?.absolutePath + "/recording.3gp"
    private var timerCount = 0
    private var updater: Runnable? = null
    private var timerHandler: Handler? = null
    private var myAudioRecorder: MediaRecorder? = null
    protected var player: MediaPlayer? = null
    private var seekLength: Int = 0
    private var recLength = 0
    private var playerPosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        outputFile = (activity?.filesDir?.absolutePath + "/recording.3gp")
    }

    protected fun enablePlayLayout(enable: Boolean) {
        if (!enable) {
            tvRecord?.visibility = View.GONE
            rlRecording?.visibility = View.GONE
            rlRecorded?.visibility = View.VISIBLE
            btnRecord?.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.bg_mic_disabled)
            btnRecord?.isEnabled = false
            imgPlayPause?.setImageDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_play))
            imgPlayPause?.isEnabled = false
            sbVoiceRecord?.progress = 0
            tvStartTime?.text = getString(R.string.start_time)
            tvEndTime?.text = getString(R.string.start_time)
        } else {
            tvRecord?.visibility = View.GONE
            rlRecording?.visibility = View.GONE
            rlRecorded?.visibility = View.VISIBLE
            btnRecord?.isEnabled = true
            btnRecord?.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.bg_mic_enabled)
            imgPlayPause?.isEnabled = true
            imgPlayPause?.setImageDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_play))
            sbVoiceRecord?.progress = 0
            tvStartTime?.text = getString(R.string.start_time)
            tvEndTime?.text =
                CommonUtils.convertSecondsToRecFormat(recLength.toString()) // mp3 length time here
        }

    }

    protected fun enableOnlyRecordLayout() {
        tvRecord?.visibility = View.VISIBLE
        rlRecording?.visibility = View.GONE
        rlRecorded?.visibility = View.GONE
        btnRecord?.isEnabled = true
        btnRecord?.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.bg_mic_enabled)
    }

    protected fun startPlaying() {
        imgPlayPause?.setImageDrawable(ContextCompat.getDrawable(requireContext(),
            R.drawable.ic_pause))
        imgPlayPause?.isEnabled = true
        btnRecord?.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.bg_mic_disabled)
        btnRecord?.isEnabled = false
        if (player != null) {
            player?.seekTo(playerPosition)
            player?.start()
            updateTimer(seekLength, true)
        } else {
            prepareMediaPlayer()
            player?.start()
            updateTimer(0, false)
        }
    }

    private fun prepareMediaPlayer() {
        player = MediaPlayer().apply {
            try {
                setDataSource(outputFile)
                prepare()

                setOnCompletionListener {
                    btnRecord?.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_mic_enabled)
                    btnRecord?.isEnabled = true

                    imgPlayPause?.setImageDrawable(ContextCompat.getDrawable(requireContext(),
                        R.drawable.ic_play))
                    imgPlayPause?.isEnabled = true
                    sbVoiceRecord?.progress = 0
                    player = null
                    tvStartTime.text = getString(R.string.start_time)
                    tvEndTime.text = CommonUtils.convertSecondsToRecFormat(recLength.toString())
                    stopTimer()
                }

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun stopTimer() {
        try {
            updater?.let {
                timerHandler?.removeCallbacks(it)
            }
        } catch (e: Exception) { //Do nothing
        }
        timerHandler = null
        updater = null
        timerCount = 0
    }

    protected fun pausePlaying() {
        try {
            if (player?.isPlaying == true) {
                player?.pause() //            player = null
                pauseTimer()

                imgPlayPause?.setImageDrawable(ContextCompat.getDrawable(requireContext(),
                    R.drawable.ic_play))
                imgPlayPause?.isEnabled = true

                btnRecord?.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_mic_enabled)
                btnRecord?.isEnabled = true
            }
        } catch (e: Exception) {
        }
    }

    private fun pauseTimer() {
        playerPosition = player?.currentPosition ?: 0
        seekLength = timerCount
        try {
            updater?.let {
                timerHandler?.removeCallbacks(it)
            }
        } catch (e: Exception) { //Do nothing
        }

    }

    @SuppressLint("WrongConstant")
    private fun startRecording() {
        tvRecord?.visibility = View.GONE
        rlRecording?.visibility = View.VISIBLE
        rlRecorded?.visibility = View.GONE
        tvRecordTime?.text = getString(R.string.start_time)
        try {

            hideLoader()

            //Now actually start recording
            myAudioRecorder = MediaRecorder()
            myAudioRecorder?.apply {
                setAudioSource(MediaRecorder.AudioSource.MIC)
                setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
                setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB)
                setOutputFile(outputFile)

                prepare()
                start()
                startRecordTimer()

            }

        } catch (e: Exception) { //Do nothing
        }
    }

    private fun startRecordTimer() {
        try {
            timerHandler = Handler(Looper.getMainLooper())
            timerCount = 0
            updater = Runnable {

                tvRecordTime?.text = CommonUtils.convertSecondsToRecFormat(timerCount.toString())
                timerHandler?.postDelayed(updater!!, 1000)
                timerCount++
            }
            updater?.let {
                timerHandler?.post(it)
            }
        } catch (e: Exception) { //Do nothing
        }
    }

    protected fun deleteAnyPreviousRecording() {
        lateinit var dialog: AlertDialog

        activity?.let {
            val builder = AlertDialog.Builder(it)

            builder.setIcon(android.R.drawable.ic_dialog_alert)
            builder.setTitle(getString(R.string.start_new_recording))
            builder.setMessage(getString(R.string.delete_recording_dialogue_msg))
            val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {

                        showLoader()

                        startRecording()
                        val delTask = recordingRef?.delete()

                        delTask?.addOnSuccessListener {

                            startRecording()

                        }?.addOnFailureListener {

                            if ((it is StorageException) && it.errorCode == ERROR_OBJECT_NOT_FOUND) { //There was no file uploaded... directly proceed to start recording the new recording

                                startRecording()

                            } else {

                                try {

                                    hideLoader()

                                    CommonUtils.showToast(activity,
                                        getString(R.string.sorry_error_occurred))

                                } catch (e: Exception) { //Do nothing
                                }

                            }

                        }

                    }
                }
            }

            builder.setPositiveButton(getString(R.string.text_continue), dialogClickListener)
            builder.setNeutralButton(getString(R.string.cancel), dialogClickListener)

            dialog = builder.create()

            dialog.show()
        }

    }

    protected fun stopRecording() {
        try {
            btnRecord?.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.bg_mic_enabled)
            btnRecord?.isEnabled = true
            myAudioRecorder?.apply {
                stop()
                release()
                myAudioRecorder = null

                recLength = timerCount
                stopTimer()
                enablePlayLayout(true)

            } ?: run {
                btnRecord?.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_mic_enabled)
                btnRecord?.isEnabled = true
            }
        } catch (e: Exception) { //Do nothing
        }

    }

    //    protected fun uploadRecording(){
    //        val file = Uri.fromFile(File(outputFile))
    //
    //        val uploadTask = recordingRef?.putFile(file)
    //
    //        bottomViewText?.visibility = View.VISIBLE
    //        bottomViewText?.text = getString(R.string.uploading_data)
    //
    //        uploadTask?.addOnFailureListener {
    //            try {
    //                CommonUtils.showToast(activity, getString(R.string.data_fail))
    //                bottomViewText?.visibility = View.GONE
    //            } catch (e: Exception) {
    //                //Handling context not attached crashes
    //            }
    //        }?.addOnSuccessListener { _ ->
    //            try {
    //                CommonUtils.showToast(activity, getString(R.string.data_success))
    //
    //                record?.isEnabled = true
    //
    //                play?.visibility = View.VISIBLE
    //
    //                play?.isEnabled = true
    //                play?.alpha = 1.toFloat()
    //
    //                bottomViewText?.visibility = View.GONE
    //            } catch (e: Exception) {
    //                //Handling context not attached crashes
    //            }
    //        }
    //    }

    private fun updateTimer(startTime: Int, isResume: Boolean) {

        try {
            timerHandler = Handler(Looper.getMainLooper())
            timerCount = startTime
            updater = Runnable {

                tvStartTime?.text = CommonUtils.convertSecondsToRecFormat(timerCount.toString())
                sbVoiceRecord?.progress =
                    CommonUtils.calculateSeekBarProgress(player?.currentPosition!!.toFloat(),
                        player?.duration!!.toFloat())
                timerHandler?.postDelayed(updater!!, 1000)
                timerCount++
            }
            updater?.let {
                timerHandler?.post(it)
            }
        } catch (e: Exception) { //Do nothing
        }

    }

    override fun onDestroyView() {
        stopTimer()
        super.onDestroyView()
    }

    protected fun onSeekBarChanged(position: Int) {
        playerPosition = if (player != null) {
            (player?.duration!! * position) / 100
        } else {
            prepareMediaPlayer()
            (player?.duration!! * position) / 100
        }
        seekLength = (position * recLength) / 100
        tvStartTime.text = CommonUtils.convertSecondsToRecFormat(seekLength.toString())
        if (seekLength == 0) {
            sbVoiceRecord.progress = 0
        }
    }

}