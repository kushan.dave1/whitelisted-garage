package whitelisted.garage.base

interface ActionListener {

    fun onActionItem(extra: Any?, extra2: Any? = null)
}