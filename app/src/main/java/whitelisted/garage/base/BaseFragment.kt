package whitelisted.garage.base

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupWindow
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.viewbinding.ViewBinding
import coil.transform.CircleCropTransformation
import com.google.android.gms.auth.api.phone.SmsRetriever
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import smartdevelop.ir.eram.showcaseviewlib.GuideView
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType
import whitelisted.garage.R
import whitelisted.garage.api.data.SelectTabParams
import whitelisted.garage.api.data.SelectWorkshopDialogParams
import whitelisted.garage.broadcast_receivers.MySMSBroadcastReceiver
import whitelisted.garage.databinding.LayoutImagePreviewBinding
import whitelisted.garage.eventBus.OTPDetectedEvent
import whitelisted.garage.utils.*
import whitelisted.garage.view.activities.DashboardActivity
import whitelisted.garage.view.fragments.bottomNavFragment.BottomNavFragment
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import whitelisted.garage.viewmodels.WalkthroughSharedViewModel
import kotlin.properties.ReadOnlyProperty

typealias ViewBindingInflater = (LayoutInflater, ViewGroup?, Boolean) -> ViewBinding

abstract class BaseFragment : Fragment(), View.OnClickListener {

    open val layoutRes: Int = 0

    private var genericBinding: ViewBinding? = null
    private lateinit var bindingInflater: ViewBindingInflater
    private var isUsingBinding = false
    private var layoutResDB: Int = 0
    private lateinit var llLoader: LinearLayoutCompat

    var isLastPageRV = false
    var isLoadingRV = false
    private lateinit var countDownTimer: CountDownTimer
    var walkThroughInProgress = false
    private val viewModel: DashboardSharedViewModel by sharedViewModel()
    private val walkThroughViewModel: WalkthroughSharedViewModel by sharedViewModel()
    private val sparesShopViewModel: SparesShopFragmentViewModel by sharedViewModel()
    var pagerAdapter: BottomNavFragment.ViewPagerAdapter? = null
    var isRetailer = false
    var isInternationalUser = false

    fun <T : ViewBinding> viewBinding(inflater: (LayoutInflater, ViewGroup?, Boolean) -> T): ReadOnlyProperty<Fragment, T> {
        bindingInflater = inflater
        return ReadOnlyProperty { _, _ -> genericBinding as T }
    }

    fun <T : ViewBinding> viewBinding2(inflater: (LayoutInflater, ViewGroup?, Boolean) -> T): ReadOnlyProperty<Fragment, T?> {
        bindingInflater = inflater
        return ReadOnlyProperty { _, _ -> genericBinding as? T }
    }

    fun <T : ViewDataBinding> dataBinding(layoutRes: Int): ReadOnlyProperty<Fragment, T> {
        isUsingBinding = true
        layoutResDB = layoutRes
        return ReadOnlyProperty { _, _ -> genericBinding as T }
    }

    fun getString(key: String) = ReadOnlyProperty<Fragment, String?> { _, _ ->
        arguments?.getString(key)
    }

    override fun onResume() {
        super.onResume()
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this)
        isRetailer = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.RETAILER_CONSTANT
    }

    override fun onClick(v: View) {
        CommonUtils.hideKeyboard(activity)
    }

    //If we need to handle the back pressed inside the fragment, we use this method
    //It will return true when overriden inside the fragment and is actually handled by the fragment
    //It basically means that if this method from the fragment returns true, do not pop the fragment off, it has already consumed the back press
    //If the back press was not handled i.e the function was either not implemented inside the fragment or it returned false, pop the fragment off
    open fun handleBackPress(): Boolean {
        return false
    }

    open fun openWhatsApp(number: String?, message: String?) {
        val pm: PackageManager = requireContext().packageManager
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            val info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA)
            intent.`package` = info.packageName
            intent.data = Uri.parse("http://api.whatsapp.com/send?phone=$number&text=$message")
            startActivity(Intent.createChooser(intent, "Share with!"))
        } catch (e: PackageManager.NameNotFoundException) {
            Toast.makeText(requireContext(), "WhatsApp not Installed", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) { //
        }
    }

    fun getFragmentTab(fragmentId: Int, context: Context?): BaseBottomFragmentModel? {
        context?.let {
            when (fragmentId) {
                FragmentFactory.Fragments.HOME_TABS -> {
                    FragmentFactory.fragment(FragmentFactory.Fragments.HOME, null)?.let {
                        return BaseBottomFragmentModel(fragmentId = FragmentFactory.Fragments.HOME_TABS,
                            it,
                            getString(R.string.home))
                    } ?: return null
                }
                FragmentFactory.Fragments.SPARES_TABS -> {
                    FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_SHOP, Bundle().apply {
                        putBoolean(AppENUM.IntentKeysENUM.IS_UNDER_TAB, true)
                    })?.let {
                        return BaseBottomFragmentModel(fragmentId = FragmentFactory.Fragments.SPARES_TABS,
                            it,
                            getString(R.string.spares))
                    } ?: return null
                }
                FragmentFactory.Fragments.INVENTORY_TABS -> {
                    FragmentFactory.fragment(FragmentFactory.Fragments.INVENTORY_FRAGMENT, null)
                        ?.let {
                            return BaseBottomFragmentModel(fragmentId = FragmentFactory.Fragments.INVENTORY_TABS,
                                it,
                                getString(R.string.inventory))
                        } ?: return null
                }
                FragmentFactory.Fragments.INVENTORY_TABS_INTERNATIONAL -> {
                    FragmentFactory.fragment(FragmentFactory.Fragments.INVENTORY_FRAGMENT, null)
                        ?.let {
                            return BaseBottomFragmentModel(fragmentId = FragmentFactory.Fragments.INVENTORY_TABS_INTERNATIONAL,
                                it,
                                getString(R.string.inventory))
                        } ?: return null
                }
                FragmentFactory.Fragments.BILLING_TABS -> {
                    FragmentFactory.fragment(FragmentFactory.Fragments.BILLING, Bundle().apply {
                        putBoolean(AppENUM.IntentKeysENUM.IS_UNDER_TAB, true)
                    })?.let {
                        return BaseBottomFragmentModel(fragmentId = FragmentFactory.Fragments.BILLING_TABS,
                            it,
                            getString(R.string.billing))
                    } ?: return null
                }
                FragmentFactory.Fragments.ACCOUNT_TABS -> {
                    FragmentFactory.fragment(FragmentFactory.Fragments.ACCOUNT, null)?.let {
                        return BaseBottomFragmentModel(fragmentId = FragmentFactory.Fragments.ACCOUNT_TABS,
                            it,
                            getString(R.string.account))
                    } ?: return null
                }
                else -> {
                    return null
                }
            }
        } ?: run {
            return null
        }
    }

    protected fun openWhatsAppText(isOpenSMSOrWhatsApp: Boolean,
        mobile: String?,
        message: String?) {
        if (isOpenSMSOrWhatsApp) {
            openWhatsApp(mobile, message)
        } else {
            CommonUtils.sendSMSTEXT(requireContext(), mobile ?: "", message ?: "")
        }
    }

    fun removeAllFragments() {
        try {
            CommonUtils.hideKeyboard(activity)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        for (fragment in requireActivity().supportFragmentManager.fragments) {
            requireActivity().supportFragmentManager.beginTransaction().remove(fragment).commit()
        }
    }

    fun removeAllFragmentsTill(fragmentName: String) {
        requireActivity().supportFragmentManager.let {
            try {
                CommonUtils.hideKeyboard(requireActivity())
            } catch (e: Exception) {
                e.printStackTrace()
            }
            it.fragments.asReversed().forEach { fragment ->
                if (fragment.javaClass.simpleName == fragmentName) return
                requireActivity().supportFragmentManager.popBackStack()
            }
        }
    }

    fun popBackStackAndHideKeyboard() {
        requireActivity().supportFragmentManager.let {
            try {
                CommonUtils.hideKeyboard(requireActivity())
            } catch (e: Exception) {
                e.printStackTrace()
            }
            it.popBackStack()
        }
    }

    fun popBackStackTill(fragment: Fragment) {
        requireActivity().supportFragmentManager.let { fragmentManager ->
            fragmentManager.fragments.forEach {
                if (fragment.javaClass.name == it.tag) return@forEach
                fragmentManager.popBackStack()
            }
        }
    }

    protected fun showLoader() { //activity?.let {
        //    (it as BaseActivity<*>).showLoader(true)
        //}
        if (::llLoader.isInitialized) llLoader.visibility = View.VISIBLE
    }

    protected fun isLoading(): Boolean {
        return llLoader.visibility == View.VISIBLE
    }

    protected fun hideLoader() { //activity?.let {
        //    (it as BaseActivity<*>).showLoader(false)
        //}
        if (::llLoader.isInitialized) llLoader.visibility = View.GONE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                AppENUM.RefactoredStrings.defaultCountryCode) != AppENUM.RefactoredStrings.defaultCountryCode) {
            isInternationalUser = true
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        return when {
            isUsingBinding -> {
                genericBinding = DataBindingUtil.inflate(inflater, layoutResDB, container, false)
                initializeLoaderWithBinding()
                genericBinding?.root
            }
            ::bindingInflater.isInitialized -> {
                genericBinding = bindingInflater.invoke(inflater, container, false)
                initializeLoaderWithBinding()
                genericBinding?.root
            }

            else -> {
                val v = inflater.inflate(layoutRes, container, false)
                v.findViewById<LinearLayoutCompat>(R.id.llMaterialLoader)?.let {
                    llLoader = it
                }
                return v
            }
        }
    }

    private fun initializeLoaderWithBinding() {
        genericBinding?.root?.findViewById<LinearLayoutCompat>(R.id.llMaterialLoader)?.let {
            llLoader = it
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onMessageEvent(event: BaseEventBusModel) { //
    }

    protected fun setPageEvent(eventName: String, keyName: String) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN, keyName)
            FirebaseAnalyticsLog.trackFireBaseEventLog(eventName, this)
        }
    }

    fun loadCircularImage(imageUrl: String,
        imageView: AppCompatImageView,
        defaultImage: Int = R.drawable.ic_placeholder_doc) {
        ImageLoader.loadImageWithCallback(imageView,
            imageUrl,
            object : ImageLoader.ImageLoadCallback {
                override fun onSuccess() {
                    imageView.scaleType = ImageView.ScaleType.CENTER_CROP
                }

                override fun onError(throwable: Throwable) {
                    imageView.scaleType = ImageView.ScaleType.CENTER_INSIDE
                }
            },
            transform = CircleCropTransformation(),
            placeholderImage = R.drawable.ic_apartment_gray,
            defaultImage = defaultImage)
    }

    fun loadCircularBitmap(bitmap: Bitmap,
        imageView: AppCompatImageView,
        defaultImage: Int = R.drawable.ic_placeholder_doc) {
        ImageLoader.loadDrawable(BitmapDrawable(requireContext().resources, bitmap),
            imageView,
            transform = CircleCropTransformation(),
            object : ImageLoader.ImageLoadCallback {
                override fun onSuccess() {
                    imageView.scaleType = ImageView.ScaleType.CENTER_CROP
                }

                override fun onError(throwable: Throwable) {
                    imageView.scaleType = ImageView.ScaleType.CENTER_INSIDE
                }
            })
    }

    fun restartTheApp() {
        val intent = Intent(requireActivity(), DashboardActivity::class.java)
        requireActivity().finish()
        startActivity(intent)
        requireActivity().overridePendingTransition(0, 0)
    }

    fun openPlayStore() {
        try {
            startActivity(Intent(Intent.ACTION_VIEW,
                Uri.parse("${AppENUM.PLAY_STORE_URL}${requireActivity().packageName}")))
        } catch (e: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW,
                Uri.parse("${AppENUM.PLAY_STORE_LINK}${requireActivity().packageName}")))
        }
    }

    fun sendEmail(to: String, userId: String?) {
        val email = Intent(Intent.ACTION_SEND)
        email.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        if (userId != null) email.putExtra(Intent.EXTRA_SUBJECT,
            getString(R.string.help_and_support) + " - " + userId)
        else email.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.help_and_support))
        email.type = "message/rfc822"
        email.setPackage("com.google.android.gm")

        try {
            startActivity(Intent.createChooser(email, getString(R.string.choose_an_email_client)))
        } catch (e: ActivityNotFoundException) {
            CommonUtils.showToast(requireContext(), e.message.toString())
        }
    }

    fun firebaseEventForCountrySelection(dialCode: String?,
        currency: String?,
        currencySymbol: String?) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_LOGIN)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.DIAL_CODE, dialCode)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CURRENCY, currency)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CURRENCY_SYMBOL, currencySymbol)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_PICK_COUNTRY,
                this)
        }
    }

    fun imagePreview(view: View?, uri: Uri) {
        val infoView = LayoutImagePreviewBinding.inflate(layoutInflater)
        val popupWindow = PopupWindow(infoView.root,
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT,
            true)
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
        ImageLoader.loadImage(infoView.imageView, uri)
        infoView.parentLayout.setOnClickListener {
            popupWindow.dismiss()
        }
    }

    fun startWalkthrough(view: View?) {
        walkThroughInProgress = true
        val builder = GuideView.Builder(requireContext())
        when (view?.id) {
            R.id.tvWorkshopNameFH -> {
                builder.setTitle(getString(R.string.add_workshop))
                builder.setContentText(getString(R.string.tap_here_to_add_multiple_workshops))
            }
            R.id.rlOrders -> {
                builder.setTitle(getString(R.string.order_status))
                builder.setContentText(getString(R.string.track_your_order_based_on_multiple_status_in_real_time))
            }
            R.id.imgNewOrder -> {
                builder.setTitle(getString(R.string.new_order))
                builder.setContentText(getString(R.string.tap_here_to_create_new_order))
            }
            R.id.imgSparesBanner -> {
                builder.setTitle(getString(R.string.easy_garage_marketplace))
                builder.setContentText(getString(R.string.easy_garage_marketplace_walkthrough_desc))
            }
            R.id.rlTab2 -> {
                builder.setTitle(getString(R.string.spares_shop))
                builder.setContentText(getString(R.string.spare_shop_walkthrogh_description))
            }
            R.id.btnFilterAndSort -> {
                builder.setTitle(getString(R.string.sort_filter_choices))
                builder.setContentText(getString(R.string.filter_sort_walkthrough_desc))
            }
            R.id.tvSeeAllBrandsFSS -> {
                builder.setTitle(getString(R.string.see_all_products))
                builder.setContentText(getString(R.string.see_all_products_walkthrough_description))
            }
            R.id.tvSeeAllAccBrandsFSS -> {
                builder.setTitle(getString(R.string.enter_shops))
                builder.setContentText(getString(R.string.see_all_acc_brands_walkthrough_desc))
            }
            R.id.ivOrderHistoryFSS -> {
                builder.setTitle(getString(R.string.order_history))
                builder.setContentText(getString(R.string.order_history_ss_walkthrough_description))
            }
            R.id.tvMyOrdersCart -> {
                builder.setTitle(getString(R.string.cart))
                builder.setContentText(getString(R.string.cart_ss_walkthrough_description))
            }
            R.id.llAddRack -> {
                builder.setTitle(getString(R.string.add_inventory))
                builder.setContentText(getString(R.string.click_here_to_add_items_and_manage_your_inventory))
            }
            R.id.tvDownloadSheet -> {
                builder.setTitle(getString(R.string.download_bill_csv))
                builder.setContentText(getString(R.string.tap_here_to_download_bills_amp_related_data_in_csv_format))
            }
            R.id.llUploadBulkInventory -> {
                builder.setTitle(getString(R.string.upload_bulk_inventory))
                builder.setContentText(getString(R.string.upload_bulk_inventory_walkthrough_desc))
            }
            R.id.llInstantReminder -> {
                builder.setTitle(getString(R.string.instant_reminder))
                builder.setContentText(getString(R.string.instant_reminder_walkthrough_desc))
            }
            1 -> {
                builder.setTitle(getString(R.string.business_cards))
                builder.setContentText(getString(R.string.add_amp_manage_your_workshop_employees))
            }
            2 -> {
                builder.setTitle(getString(R.string.add_employee))
                builder.setContentText(getString(R.string.add_amp_manage_your_workshop_employees))
            }
            3 -> {
                builder.setTitle(getString(R.string.reminder))
                builder.setContentText(getString(R.string.schedule_amp_trigger_service_reminders_to_your_customers))
            }
            4 -> {
                if (isInternationalUser) {
                    builder.setTitle(getString(R.string.my_customers))
                    builder.setContentText(getString(R.string.keep_track_of_all_the_active_amp_in_active_customers))
                } else {
                    builder.setTitle(getString(R.string.billing))
                    builder.setContentText(getString(R.string.tap_here_to_download_bills_amp_related_data_in_csv_format))
                }
            }
            5 -> {
                if (isInternationalUser) {
                    if (isRetailer) {
                        builder.setTitle(getString(R.string.ledger))
                        builder.setContentText(getString(R.string.track_amp_manage_your_payouts_amp_receivables))
                    } else {
                        builder.setTitle(getString(R.string.manage_packages))
                        builder.setContentText(getString(R.string.tap_here_to_create_edit_or_copy_existing_service_packages))
                    }
                } else {
                    builder.setTitle(getString(R.string.my_customers))
                    builder.setContentText(getString(R.string.keep_track_of_all_the_active_amp_in_active_customers))
                }
            }
            6 -> {
                if (isInternationalUser) {
                    if (isRetailer) {
                        builder.setTitle(getString(R.string.oder_history))
                        builder.setContentText(getString(R.string.get_end_to_end_details_of_all_new_amp_previous_orders))
                    } else {
                        builder.setTitle(getString(R.string.ledger))
                        builder.setContentText(getString(R.string.track_amp_manage_your_payouts_amp_receivables))
                    }
                } else {
                    if (isRetailer) {
                        builder.setTitle(getString(R.string.ledger))
                        builder.setContentText(getString(R.string.track_amp_manage_your_payouts_amp_receivables))
                    } else {
                        builder.setTitle(getString(R.string.manage_packages))
                        builder.setContentText(getString(R.string.tap_here_to_create_edit_or_copy_existing_service_packages))
                    }
                }
            }
            7 -> {
                if (isInternationalUser) {
                    builder.setTitle(getString(R.string.oder_history))
                    builder.setContentText(getString(R.string.get_end_to_end_details_of_all_new_amp_previous_orders))
                } else {
                    if (isRetailer) {
                        builder.setTitle(getString(R.string.oder_history))
                        builder.setContentText(getString(R.string.get_end_to_end_details_of_all_new_amp_previous_orders))
                    } else {
                        builder.setTitle(getString(R.string.ledger))
                        builder.setContentText(getString(R.string.track_amp_manage_your_payouts_amp_receivables))
                    }
                }
            }
            8 -> {
                builder.setTitle(getString(R.string.oder_history))
                builder.setContentText(getString(R.string.get_end_to_end_details_of_all_new_amp_previous_orders))
            }
            20 -> {
                builder.setTitle(getString(R.string.my_profile))
                builder.setContentText(getString(R.string.tap_here_to_add_edit_your_profile_details))
            } //            rvSetting?.getChildAt(1)?.id -> {
            //                if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
            //                        AppENUM.RefactoredStrings.defaultCurrency) == AppENUM.RefactoredStrings.defaultCurrency) {
            //
            //                } else {
            //
            //                }
            //            }
            21, 23 -> {
                builder.setTitle(getString(R.string.help_and_support))
                builder.setContentText(getString(R.string.reach_out_to_us_for_any_query_or_feedback))
            }
            22 -> {
                builder.setTitle(getString(R.string.qr_code))
                builder.setContentText(getString(R.string.set_up_your_upi_qr_code_to_start_accepting_payments))
            }
            25, 27 -> {
                builder.setTitle(getString(R.string.start_demo))
                builder.setContentText(getString(R.string.tap_here_to_start_demo_anytime_anywhere))
            } //            else -> {
            //                builder.setTitle(getString(R.string.reminder))
            //                builder.setContentText(getString(R.string.schedule_amp_trigger_service_reminders_to_your_customers))
            //            }
        }
        if (isAdded) {
            val guideView =
                builder.setGravity(smartdevelop.ir.eram.showcaseviewlib.config.Gravity.center)
                    .setTargetView(view)
                    .setDismissType(DismissType.outsideTargetAndMessage) //optional - default dismissible by TargetView
                    .setGuideListener {
                        countDownTimer.cancel()
                        startNextWalkthrough(null, it)
                    }.build()

            guideView.show()

            startTimer(guideView, view)
        }
    }

    @SuppressLint("NewApi")
    private fun startNextWalkthrough(guideView: GuideView?, it: View?) {
        try {
            guideView?.let {
                try {
                    (requireActivity().window.decorView as? ViewGroup)?.removeView(it)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            when (it?.id) {
                R.id.tvWorkshopNameFH -> {
                    walkThroughViewModel.setHomeWalkThrough(R.id.tvWorkshopNameFH)
                }
                R.id.imgNewOrder -> {
                    walkThroughViewModel.setHomeWalkThrough(R.id.imgNewOrder)
                }
                R.id.rlOrders -> {
                    if (isInternationalUser) {
                        viewModel.onTabSelected(SelectTabParams(FragmentFactory.Fragments.INVENTORY_TABS_INTERNATIONAL,
                            true))
                        walkThroughViewModel.setInventoryWalkThrough(R.id.rlOrders)
                    } else {
                        walkThroughViewModel.setHomeWalkThrough(R.id.rlOrders)
                    }
                }
                R.id.imgSparesBanner -> {
                    viewModel.onTabSelected(SelectTabParams(FragmentFactory.Fragments.SPARES_TABS,
                        true))
                    walkThroughViewModel.setSparesWalkThrough(R.id.imgSparesBanner)
                    walkThroughViewModel.setHomeWalkThrough(R.id.imgSparesBanner)
                }
                R.id.tvSeeAllBrandsFSS -> {
                    walkThroughViewModel.setSparesWalkThrough(R.id.tvSeeAllBrandsFSS)
                }
                R.id.tvSeeAllAccBrandsFSS -> {
                    lifecycleScope.launch {
                        waitForFiltersToGetLoaded()
                        sparesShopViewModel.setBrandsSelected(false)
                        CommonUtils.addFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER))
                        delay(700)
                        walkThroughViewModel.setShopWithFilterWalkThrough(R.id.tvSeeAllAccBrandsFSS)
                    }
                }
                R.id.btnFilterAndSort -> {
                    walkThroughViewModel.setSparesWalkThrough(R.id.btnFilterAndSort)
                }
                R.id.ivOrderHistoryFSS -> {
                    walkThroughViewModel.setSparesWalkThrough(R.id.ivOrderHistoryFSS)
                }
                R.id.tvMyOrdersCart -> {
                    viewModel.onTabSelected(SelectTabParams(FragmentFactory.Fragments.INVENTORY_TABS,
                        true))
                    walkThroughViewModel.setInventoryWalkThrough(R.id.tvMyOrdersCart)
                    walkThroughViewModel.setSparesWalkThrough(R.id.tvMyOrdersCart)
                }
                R.id.llAddRack -> {
                    walkThroughViewModel.setInventoryWalkThrough(R.id.llAddRack)
                }
                R.id.llUploadBulkInventory -> {
                    viewModel.onTabSelected(SelectTabParams(FragmentFactory.Fragments.ACCOUNT_TABS,
                        true))
                    walkThroughViewModel.setAccountWalkThrough(R.id.llUploadBulkInventory)
                }
                R.id.tvDownloadSheet -> {
                    viewModel.onTabSelected(SelectTabParams(FragmentFactory.Fragments.ACCOUNT_TABS,
                        true))
                }
                R.id.llInstantReminder -> {
                    walkThroughViewModel.setAccountWalkThrough(R.id.llInstantReminder)
                }
                1 -> {
                    walkThroughViewModel.setAccountWalkThrough(1)
                }
                2 -> {
                    walkThroughViewModel.setAccountWalkThrough(2)
                }
                3 -> {
                    walkThroughViewModel.setAccountWalkThrough(3)
                }
                4 -> {
                    walkThroughViewModel.setAccountWalkThrough(4)
                }
                5 -> {
                    walkThroughViewModel.setAccountWalkThrough(5)
                }
                6 -> {
                    walkThroughViewModel.setAccountWalkThrough(6)
                }
                7 -> {
                    walkThroughViewModel.setAccountWalkThrough(7)
                }
                8 -> {
                    walkThroughViewModel.setAccountWalkThrough(8)
                }
                20 -> {
                    walkThroughViewModel.setSettingsWalkThrough(20)
                }
                21 -> {
                    walkThroughViewModel.setSettingsWalkThrough(21)
                }
                22 -> {
                    walkThroughViewModel.setSettingsWalkThrough(22)
                }
                23 -> {
                    walkThroughViewModel.setSettingsWalkThrough(23)
                }
                else -> {
                    viewModel.putBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN, true)
                    viewModel.isUserWalkthroughStarted = false
                    walkThroughInProgress = false
                    if (isAdded) popBackStackAndHideKeyboard()
                    viewModel.onTabSelected(SelectTabParams(0, true))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private suspend fun waitForFiltersToGetLoaded() {
        showLoader()
        sparesShopViewModel.getSearchAndFilterAttributes()
        hideLoader()
    }

    private fun startTimer(guideView: GuideView, view: View?) {
        countDownTimer = object : CountDownTimer(5000, 1000) {
            override fun onTick(millisUntilFinished: Long) { // do nothing
            }

            override fun onFinish() {
                startNextWalkthrough(guideView, view)
            }
        }
        countDownTimer.start()
    }


    //    private fun removeAllFragmentsTillHome() {
    //        try {
    //            for (i in 0 until requireActivity().supportFragmentManager.fragments.size-2) { //                if ((supportFragmentManager.fragments[supportFragmentManager.fragments.size - 1] as? HomeFragment)?.tag != HomeFragment::class.java.name) {
    //                requireActivity().supportFragmentManager.popBackStack() //                }
    //            } //            CommonUtils.addFragmentUtil(this,
    //            //                FragmentFactory.fragment(FragmentFactory.Fragments.HOME, null),
    //            //                target = R.id.fragment_container)
    //        } catch (e: Exception) {
    //            e.printStackTrace()
    //        }
    //    }

    fun tabFirebaseEvents(eventName: String) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_DASHBOARD)
            FirebaseAnalyticsLog.trackFireBaseEventLog(eventName, this)
        }
    }

    override fun onStop() {
        super.onStop()
        if (::countDownTimer.isInitialized) countDownTimer.cancel()
    }

    fun showSelectWorkshopDialog(params: SelectWorkshopDialogParams) {
        viewModel.selectWorkshopDialogTrigger(params)
    }

    fun startSMSListener() {
        val smsReceiver: MySMSBroadcastReceiver?
        try {
            smsReceiver = MySMSBroadcastReceiver()
            val client = SmsRetriever.getClient(requireActivity())
            val retriever = client.startSmsRetriever()
            retriever.addOnSuccessListener {
                val listener = object : MySMSBroadcastReceiver.OTPReceiveListener {
                    override fun onOTPReceived(otp: String) {
                        Log.d("otp",
                            otp) //                        Toast.makeText(context, otp, Toast.LENGTH_SHORT).show()
                        EventBus.getDefault().post(OTPDetectedEvent(otp))
                    }

                    override fun onOTPTimeOut() {
                        Log.d("otp", "Timed Out.")
                    }
                }
                smsReceiver.initOTPListener(listener)
                requireContext().registerReceiver(smsReceiver,
                    IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION))
            }
            retriever.addOnFailureListener {
                Log.d("otp", "Problem to start listener") //Problem to start listener
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}