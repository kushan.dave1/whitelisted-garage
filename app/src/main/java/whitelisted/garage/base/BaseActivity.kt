package whitelisted.garage.base

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnSuccessListener
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import whitelisted.garage.R
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.PermissionsUtil
import whitelisted.garage.utils.RuntimeLocaleChanger
import whitelisted.garage.view.fragments.splash.SplashFragment

abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity(), View.OnClickListener {

    private var shouldFinish: Boolean = false
    private val activityEventsListeners = ArrayList<ActivityEventsListener>()

    private var llLoader: LinearLayoutCompat? = null
    private val cancelFinish = { shouldFinish = false }
    protected var isFinished = false
    private val checkSettings = 2
    private var context: Context? = null
    private var currentLocationListener: LocationListener? = null
    private var permissionsUtil: PermissionsUtil? = null
    private var isLooping = false
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationCallback: LocationCallback? = null

    /**
     * This handler exit app
     */
    private var exitAppHandler: Handler? = null

    abstract val viewModel: VM
    protected abstract fun getLayout(): Int
    protected abstract fun getClassName(): String

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(RuntimeLocaleChanger.wrapContext(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(getLayout())

        llLoader = findViewById(R.id.llMaterialLoader)
        exitAppHandler = Handler(Looper.getMainLooper())
        context = this
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        permissionsUtil = PermissionsUtil(this@BaseActivity)
    }

    //Use this to prevent two back press to go back on all activities except MainActivity
    //Call this and set this as true in all activities except MainActivity
    fun onlyOneBackPressToExit(shouldFinish: Boolean) {
        this.shouldFinish = shouldFinish
    }

    fun showLoader(shouldShow: Boolean) {
        llLoader?.let { pb ->
            pb.visibility = if (shouldShow) View.VISIBLE else View.GONE
        }
    }

    fun requestLocation(currentLocationListener: LocationListener?, isLooping: Boolean) {
        this.isLooping = isLooping
        this.currentLocationListener = currentLocationListener
        val mLocationRequest = LocationRequest.create().apply {
            this.interval = 10000
            this.fastestInterval = 10000
            this.priority = Priority.PRIORITY_HIGH_ACCURACY
        }
        val builder = LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest)
        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder.build())
        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                for (location in locationResult.locations) {
                    if (location != null) {
                        if (!isLooping) stopLocationUpdates()
                        currentLocationListener?.onLocationChanged(location)
                        break
                    }
                }
            }
        }
        task.addOnSuccessListener(this, OnSuccessListener {
            if (ActivityCompat.checkSelfPermission(this@BaseActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this@BaseActivity,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return@OnSuccessListener
            }
            mLocationCallback?.let {
                mFusedLocationClient?.requestLocationUpdates(mLocationRequest,
                    it,
                    Looper.getMainLooper())
            }

        })
        task.addOnFailureListener(this) { e ->
            when ((e as ApiException).statusCode) {
                CommonStatusCodes.RESOLUTION_REQUIRED -> try {
                    val resolvable = e as ResolvableApiException
                    resolvable.startResolutionForResult(this@BaseActivity, checkSettings)
                } catch (sendEx: IntentSender.SendIntentException) { //
                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> { //
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        stopLocationUpdates()
    }

    fun getLastLocation(currentLocationListener: LocationListener?, isLooping: Boolean) {
        this.currentLocationListener = currentLocationListener
        this.isLooping = isLooping
        TedPermission.create().setPermissionListener(object : PermissionListener {
            override fun onPermissionGranted() {
                requestLocation(currentLocationListener, isLooping)
            }

            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
                Log.d(deniedPermissions.toString(), "denied")
            }
        }).setPermissions(Manifest.permission.ACCESS_FINE_LOCATION).check()

    }

    override fun onClick(v: View) { // nothing
    }

    private fun stopLocationUpdates() {

        mLocationCallback?.let {
            mFusedLocationClient?.removeLocationUpdates(it)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    fun addActivityEventListener(listener: ActivityEventsListener) {
        activityEventsListeners.add(listener)
    }

    fun removeActivityEventListener(listener: ActivityEventsListener) {
        activityEventsListeners.remove(listener)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == checkSettings) {
            if (resultCode == Activity.RESULT_OK) {
                requestLocation(currentLocationListener, isLooping)
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, "Location access denied.", Toast.LENGTH_LONG).show()
            }
        }
        try {
            for (listener in activityEventsListeners) {
                listener.onActivityResult(requestCode, resultCode, data)
            }
        } catch (e: Exception) { //What the fuck
        }
    }

    /**
     * @return return true if event is consumed.
     */
    private fun onPopBackStack(): Boolean {
        val fragmentManager = supportFragmentManager

        try {
            if (fragmentManager.executePendingTransactions()) {
                Log.d("Working...", "")
                return true
            }
        }catch (e:Exception){
            e.printStackTrace()
        }
        val fragment = fragmentManager.findFragmentById(R.id.fragment_container_2) as BaseFragment?
        val isEventHandledByFragment = fragment != null && fragment.handleBackPress()

        //For every other activity, since there is no fragment at root, we know that we need to pop any added fragment
//        if (getClassName() == SplashFragment::class.java.simpleName) {
//            if (fragmentManager.backStackEntryCount > 1 && !isEventHandledByFragment) { //Reset should finish flag so that when we do go back we again get two back presses
//                fragmentManager.popBackStack()
//                return true
//            }
//        } else {
            if (fragmentManager.backStackEntryCount > 1 && !isEventHandledByFragment) { //Reset should finish flag so that when we do go back we again get two back presses
                fragmentManager.popBackStack()
                return true
            }
//        }

        return isEventHandledByFragment
    }

    override fun onBackPressed() {
        if (!onPopBackStack()) { //Check if we have twice press to exit logic
            //If not, finish the activity
            if (shouldFinish) {
                exitAppHandler?.removeCallbacks(cancelFinish)
                finish()
            } else {
                shouldFinish = true
                exitAppHandler?.postDelayed(cancelFinish, 2000)
                CommonUtils.showToast(this, resources.getString(R.string.exit_app), true)
            }

        }
    }

    interface ActivityEventsListener {
        fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    }

    override fun applyOverrideConfiguration(overrideConfiguration: Configuration?) {
        super.applyOverrideConfiguration(baseContext.resources.configuration)
    }

//    private var isLoadInventoryFirstTime = true
//    private var isLoadBillFirstTime = true
//    var isLoadAccountFirstTime = true

//    fun loaderAgainFirstTime() {
//        setInventoryLoader(flag = true)
//        setBillLoader(flag = true)
//    }
//
//    fun getInventoryLoader(): Boolean {
//        return isLoadInventoryFirstTime
//    }
//
//    fun getBillLoader(): Boolean {
//        return isLoadBillFirstTime
//    }
//
//    fun setInventoryLoader(flag: Boolean) {
//        isLoadInventoryFirstTime = flag
//    }
//
//    fun setBillLoader(flag: Boolean) {
//        isLoadBillFirstTime = flag
//    }

}