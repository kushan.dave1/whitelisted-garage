package whitelisted.garage.base

data class BaseBottomFragmentModel (
    val fragmentId:Int,
    val fragment : BaseFragment,
    val fragmentTitle:String? = ""
)