package whitelisted.garage.base

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewbinding.ViewBinding
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import whitelisted.garage.broadcast_receivers.MySMSBroadcastReceiver
import whitelisted.garage.eventBus.OTPDetectedEvent
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.view.activities.DashboardActivity
import kotlin.properties.ReadOnlyProperty

abstract class BaseBottomSheetFragment : BottomSheetDialogFragment(), View.OnClickListener {

    private lateinit var bindingInflater: ViewBindingInflater
    private var genericBinding: ViewBinding? = null

    fun <T : ViewBinding> viewBinding(inflater: (LayoutInflater, ViewGroup?, Boolean) -> T): ReadOnlyProperty<Fragment, T> {
        bindingInflater = inflater
        return ReadOnlyProperty { _, _ -> genericBinding as T }
    }

    override fun onClick(v: View) {
        CommonUtils.hideKeyboard(activity)
    }

    override fun onCreateView(inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        return if(::bindingInflater.isInitialized) {
            genericBinding = bindingInflater(inflater, container, false)
            genericBinding?.root
        }
        else null

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        CommonUtils.hideKeyboard(activity as FragmentActivity)
        isCancelable = false
    }

    override fun onDestroyView() {
        CommonUtils.hideKeyboard(activity as FragmentActivity)
        super.onDestroyView()
    }

    override fun onResume() {
        super.onResume()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onPause() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
        super.onPause()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onMessageEvent(event: BaseEventBusModel) {
    }

    fun popBackStack() {
        dismiss()
    }

    protected fun showLoader() {
        activity?.let {
            (it as BaseActivity<*>).showLoader(true)
        }
    }

    protected fun hideLoader() {
        activity?.let {
            (it as BaseActivity<*>).showLoader(false)
        }
    }

    fun restartTheApp() {
        val intent = Intent(requireActivity(), DashboardActivity::class.java)
        requireActivity().finish()
        requireActivity().overridePendingTransition(0,0)
        startActivity(intent)
    }


}