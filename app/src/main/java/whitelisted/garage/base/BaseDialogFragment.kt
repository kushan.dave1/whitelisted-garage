package whitelisted.garage.base

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import whitelisted.garage.R
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.isVisible
import kotlin.properties.ReadOnlyProperty


@Suppress("DEPRECATION", "UNUSED_PARAMETER")
abstract class BaseDialogFragment : DialogFragment(), View.OnClickListener {

    private lateinit var bindingInflater: ViewBindingInflater
    private var genericBinding: ViewBinding? = null
    private var llLoader: LinearLayoutCompat? = null

    private var isUsingBinding = false
    private var layoutResDB: Int = 0

    fun <T : ViewBinding> viewBinding(inflater: (LayoutInflater, ViewGroup?, Boolean) -> T): ReadOnlyProperty<Fragment, T> {
        bindingInflater = inflater
        return ReadOnlyProperty { _, _ -> genericBinding as T }
    }

    fun <T : ViewBinding> viewBinding2(inflater: (LayoutInflater, ViewGroup?, Boolean) -> T): ReadOnlyProperty<Fragment, T?> {
        bindingInflater = inflater
        return ReadOnlyProperty { _, _ -> genericBinding as? T }
    }


    fun <T : ViewDataBinding> dataBinding(layoutRes:Int): ReadOnlyProperty<Fragment, T> {
        isUsingBinding = true
        layoutResDB = layoutRes
        return ReadOnlyProperty { _, _ -> genericBinding as T }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onClick(v: View) {
        CommonUtils.hideKeyboard(activity)
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: EventBus) {
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {}

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return false
    }

    //If we need to handle the back pressed inside the fragment, we use this method
    //It will return true when overriden inside the frament and is actually handled by the fragment
    //It basically means that if this method from the fragment returns true, do not pop the fragment off, it has already consumed the back press
    //If the back press was not handled i.e the function was either not implemented inside the fragment or it returned false, pop the fragment off
    open fun handleBackPress(): Boolean {
        return false
    }

    fun popBackStackAndHideKeyboard() {
        fragmentManager?.let {
            try {
                CommonUtils.hideKeyboard(activity)
                requireFragmentManager().popBackStack()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    protected fun showLoader() { //activity?.let {
        //    (it as BaseActivity<*>).showLoader(true)
        //}
        llLoader?.visibility = View.VISIBLE
    }

    protected fun hideLoader() { //activity?.let {
        //    (it as BaseActivity<*>).showLoader(false)
        //}
        llLoader?.visibility = View.GONE
    }

    protected fun isLoaderShowing(): Boolean {
        return llLoader?.isVisible() ?: false
    }

    override fun onCreateView(inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        return if(::bindingInflater.isInitialized) {
            genericBinding = bindingInflater(inflater, container, false)
            llLoader = genericBinding?.root?.findViewById(R.id.llMaterialLoader)
            genericBinding?.root
        } else if (isUsingBinding) {
            genericBinding = DataBindingUtil.inflate(inflater, layoutResDB, container, false)
            llLoader = genericBinding?.root?.findViewById(R.id.llMaterialLoader)
            genericBinding?.root
        } else null

    }

    override fun onDestroyView() {
        super.onDestroyView()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        genericBinding = null
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onMessageEvent(event: BaseEventBusModel) {
    }


}