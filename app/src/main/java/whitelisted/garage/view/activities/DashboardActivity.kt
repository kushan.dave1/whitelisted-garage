package whitelisted.garage.view.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import com.razorpay.PaymentData
import com.razorpay.PaymentResultWithDataListener
import com.truecaller.android.sdk.TruecallerSDK
import io.branch.referral.Branch
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.base.BaseActivity
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.eventBus.*
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.fragments.SubscriptionInfoFragment
import whitelisted.garage.viewmodels.DashboardSharedViewModel

class DashboardActivity : BaseActivity<DashboardSharedViewModel>(), PaymentResultWithDataListener {

    override val viewModel: DashboardSharedViewModel by viewModel()
    private var isLocationEventSent = false
    private var deeplinkHandled = false

    override fun getLayout(): Int {
        return R.layout.activity_dashboard
    }

    override fun getClassName(): String {
        return DashboardActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState) //        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        Log.d(
            "Access token",
            viewModel.getSharedPreference().getString(this, AppENUM.ACCESS_TOKEN_, "")
        )

        setupScreen()
    }

    private fun getDeepLinkIfAvailable() {
        if (!deeplinkHandled) {
            deeplinkHandled = true
            val action: String? = intent?.action
            val data: Uri? = intent?.data
            Log.d(action, data.toString())
            data?.let {
                if (data.toString().isNotEmpty()) {
                    val pageName: String?
                    if (data.toString().contains("freegarage")) {
                        pageName = data.getQueryParameter("pagename")
                        when (pageName) {
                            AppENUM.ConfigConstants.DEEPLINK_SS_PRODUCT_DETAIL -> {
                                val productId = data.getQueryParameter("product_id")
                                viewModel.savePageNameAndRedirect(pageName.toString(), productId)
                            }
                            AppENUM.ConfigConstants.DEEPLINK_SS_CATEGORY, AppENUM.ConfigConstants.DEEPLINK_AS_CATEGORY, AppENUM.ConfigConstants.DEEPLINK_SS_BRAND -> {
                                val catName = data.getQueryParameter("name")
                                viewModel.savePageNameAndRedirect(pageName.toString(), catName)
                            }
                            else -> viewModel.savePageNameAndRedirect(pageName.toString())
                        }
                    } else {
                        handleAppLink(data)
                    }
                }
            }
        }
    }

    private fun handleAppLink(data: Uri) {
        var pageName = data.getQueryParameter("pagename")
        if (pageName.isNullOrEmpty()) {
            val pageIndex = data.toString().indexOf("=")
            pageName = data.toString().substring(pageIndex + 1)
            if (pageName.contains("=")) {
                pageName = pageName.substring(pageName.indexOf("=") + 1)
            }
        }
        if (pageName == "skuid") {
            val productId = data.getQueryParameter("product_id")
            viewModel.savePageNameAndRedirect(pageName.toString(), productId)
        } else viewModel.savePageNameAndRedirect(pageName)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        try {
            intent?.putExtra("branch_force_new_session", true)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        setIntent(intent)
        deeplinkHandled = false
        getDeepLinkIfAvailable()
        Branch.sessionBuilder(this).withCallback(branchReferralInitListener).reInit()
    }

    private fun setupScreen() {
        CommonUtils.addFragmentUtil(
            this,
            FragmentFactory.fragment(FragmentFactory.Fragments.SPLASH, null)
        )
        lifecycleScope.launch(Dispatchers.Main) {
            delay(2000)
            getDeepLinkIfAvailable()
        }
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)

        //        MiPushHelper.initialiseMiPush(this, "5131805468004", BuildConfig.MOENGAGE_ID, Region.India)
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: BaseEventBusModel) {
        when (event) {
            is SessionExpiredEvent -> {
                viewModel.logoutUser()
                val intent = intent
                finish()
                startActivity(intent)
                overridePendingTransition(0, 0)
            }

            is ProLockedEvent -> {
                FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT,
                    params = Bundle().apply {
                        putString(AppENUM.PRO_LOCKED_ALERT_MSG, event.str)
                    })?.let { baseFragment ->
                    baseFragment.show(supportFragmentManager, baseFragment.javaClass.name)
                }
            }

            is MembershipPurchasedEvent -> {
                if (event.extra == true) {
                    viewModel.putBooleanSharedPreference(
                        AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                        true
                    )
                    CommonUtils.showToast(this, "Pro Subscription Purchased")
                    FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.MEMBERSHIP_PURCHASED_DIALOG)
                        ?.let { dialog ->
                            dialog.show(supportFragmentManager, dialog.javaClass.name)
                        }
                } else {
                    val isSubsInfoAdded =
                        supportFragmentManager.fragments.any { it is SubscriptionInfoFragment }
                    if(!isSubsInfoAdded) {
                        CommonUtils.addFragmentUtil(this,
                            FragmentFactory.fragment(FragmentFactory.Fragments.SUBSCRIPTION_INFO_FRAGMENT,
                            Bundle().apply
                             { putBoolean(AppENUM.PAYMENT_FAILED, true) }))
                    }
                }

            }
        }
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    override fun onPaymentSuccess(p0: String?, p1: PaymentData?) {
        Log.d(p0, p1.toString())
        EventBus.getDefault().post(PaymentEventModel(p1 ?: PaymentData()))
    }

    override fun onPaymentError(p0: Int, p1: String?, p2: PaymentData?) {
        Log.d(p1, p0.toString())
        EventBus.getDefault().post(PaymentEventModel(p2 ?: PaymentData()))
    }

    override fun onBackPressed() {
        viewModel.onCheckBackPressEvent(true)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1001 && !isLocationEventSent) EventBus.getDefault()
            .post(RequestLocationEvent())
    }

    fun restartApp() {
        finish()
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        Branch.sessionBuilder(this).withCallback(branchReferralInitListener)
            .withData(this.intent.data).init()
    }

    fun backPressEvent() {
        super.onBackPressed()
    }

    private val branchReferralInitListener =
        Branch.BranchReferralInitListener { referringParams, error ->
            error?.let { _ -> //                Log.d("branchError", branchError.toString())
            }
            referringParams?.let { refer ->
                refer.optString("\$android_deeplink_path", "").let {
                    if (it.isNotEmpty()) viewModel.savePageNameAndRedirect(it)
                }
                refer.optString("+non_branch_link", "").let {
                    if (it.isNotEmpty()) viewModel.savePageNameAndRedirect(it)
                }
            }
        }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == TruecallerSDK.SHARE_PROFILE_REQUEST_CODE) {
            TruecallerSDK.getInstance()
                .onActivityResultObtained(this, requestCode, resultCode, data)
        }
    }
}