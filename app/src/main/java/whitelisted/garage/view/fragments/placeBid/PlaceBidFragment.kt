package whitelisted.garage.view.fragments.placeBid

import android.Manifest
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.google.gson.Gson
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.data.IdUriModel
import whitelisted.garage.api.response.BidDetailResponse
import whitelisted.garage.api.response.BidItem
import whitelisted.garage.api.response.DistanceObject
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentPlaceBidBinding
import whitelisted.garage.eventBus.UpdateHomeScreenEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.BidItemsAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.EnquiryOrderRequestsFragmentVIewModel
import whitelisted.garage.viewmodels.PlaceBidFragmentViewModel
import java.io.ByteArrayOutputStream
import java.util.*

@Suppress("DEPRECATION")
class PlaceBidFragment : BaseFragment() {

    private val binding by viewBinding(FragmentPlaceBidBinding::inflate)
    private val viewModel: PlaceBidFragmentViewModel by viewModel()
    private val sharedViewModel: EnquiryOrderRequestsFragmentVIewModel by sharedViewModel()
    private val dashboardSharedViewModel: DashboardSharedViewModel by sharedViewModel()
    private lateinit var bidId: String
    private lateinit var bidDetail: BidDetailResponse
    private lateinit var bidItemsAdapter: BidItemsAdapter
    private lateinit var storageRef: StorageReference
    private var priceEditedPosition = -1
    private var selectedUploadPhotosPosition = -1
    private lateinit var mediaToBeUploaded: MutableList<IdUriModel>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListeners()
        setObservers()

        arguments?.let {
            bidId = it.getString(AppENUM.IntentKeysENUM.ORDER_ID, "")
        }
        binding.tvBidId.text = bidId

        showLoader()
        viewModel.getBidDetails(bidId)
        dashboardSharedViewModel.getGoCoinBalance()
    }

    private fun clickListeners() {
        binding.imgBackPB.setOnClickListener(this)
        binding.btnSubmit.setOnClickListener(this)
        binding.btnAcceptAll.setOnClickListener(this)
        binding.btnCancelBid.setOnClickListener(this)
        binding.imgCall.setOnClickListener(this)
        binding.imgLocateMarker.setOnClickListener(this)
        binding.llUnlockMobile.setOnClickListener(this)
    }

    private fun setObservers() {
        observeGetBidDetailsResponse()
        observerPlaceBidResponse()
        observeCancelBid()
        observeUnlockContactResponse()
        observeGetGoCOinResponse()
    }

    private fun observeGetGoCOinResponse() {
        dashboardSharedViewModel.provideGetGoCoinResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.let { data ->
                        binding.tvGoCoinBalance.text = (data.gocoinBalance ?: 0).toString()
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeUnlockContactResponse() {
        viewModel.provideUnlockContactResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_UNLOCK_CONTACT,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACC_BID_DETAILS)

                    binding.rlPhoneNumberMasked.makeVisible(false)
                    binding.imgLocateMarker.makeVisible(true)
                    binding.imgCall.makeVisible(true)
                    dashboardSharedViewModel.getGoCoinBalance()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    private fun observeCancelBid() {
        viewModel.provideCancelBidResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_REJECT_ALL,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACC_BID_DETAILS)
                    CommonUtils.showToast(requireContext(), it.body.message)
                    sharedViewModel.provideIsUpdateList().postValue(true)
                    popBackStackAndHideKeyboard()
                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    private fun observerPlaceBidResponse() {
        viewModel.providePlaceBidResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    firebaseSubmitBidEvent()
                    CommonUtils.showToast(requireContext(), it.body.message)
                    sharedViewModel.provideIsUpdateList().postValue(true)
                    popBackStackAndHideKeyboard()
                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    private fun firebaseSubmitBidEvent() {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACC_BID_DETAILS)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PRODUCT_COUNT,
                bidDetail.totalItem.toString())
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.TOTAL_PRICE,
                bidDetail.totalAmount.toString())
            putStringArrayList(FirebaseAnalyticsLog.FirebaseEventNameENUM.PRODUCT_NAME,
                bidDetail.items as ArrayList<String>)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_SUBMIT_BID,
                this)
        }
    }

    private fun observeGetBidDetailsResponse() {
        viewModel.provideGetBidDetailResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    bidDetail = it.body.data
                    setData()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    private fun setData() = binding.apply {
        tvShopName.text = bidDetail.workshopName
        val distanceObject: DistanceObject? =
            Gson().fromJson(bidDetail.distance?.getAsJsonObject(viewModel.getStringSharedPreference(
                AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                "")).toString(), DistanceObject::class.java)
        distanceObject?.let {
            "${it.distance} ${tvDistance.context.getString(R.string.km_away)}".apply {
                tvDistance.text = this
            }
        }

        if (bidDetail.isContactNumber?.contains(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                "")) == true) {
            imgLocateMarker.makeVisible(true)
            imgCall.makeVisible(true)
            rlPhoneNumberMasked.makeVisible(false)
        } else {
            imgLocateMarker.makeVisible(false)
            imgCall.makeVisible(false)
            rlPhoneNumberMasked.makeVisible(true)
            var maskedPhone: String?
            val initialFour = bidDetail.contactNumber?.substring(0, 4)
            val noOfStars = (bidDetail.contactNumber?.length ?: 10).minus(4)
            maskedPhone = initialFour
            if (noOfStars > 0) {
                for (i in 1..(noOfStars)) {
                    maskedPhone += "*"
                }
            }
            tvPhoneNumberMasked.text = maskedPhone
        }

        setBidListAdapter()
    }

    private fun setBidListAdapter() {
        val itemList = mutableListOf<BidItem>()
        bidDetail.genericItems?.let {
            it.forEach { item ->
                item.quantityAvailable = item.quantity
                item.pricePerItem = item.sellingPrice
                item.isGeneric = true
            }
            itemList.addAll(it)
        }
        bidDetail.brandWiseItems?.let {
            it.forEach { item ->
                item.quantityAvailable = item.quantity
                item.pricePerItem = item.sellingPrice
            }
            itemList.addAll(it)
        }

        bidItemsAdapter = BidItemsAdapter(resources.getStringArray(R.array.part_availability_list)
            .toMutableList(),
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol),
            itemList,
            this)
        binding.rvBidItems.adapter = bidItemsAdapter
        binding.rvBidItems.setOnScrollChangeListener { _, _, _, _, _ ->
            CommonUtils.hideKeyboardFragment(this)
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBackPB -> {
                popBackStackAndHideKeyboard()
            }

            R.id.btnSubmit -> {
                if (formCheck() && makePlaceBidRequest()) {
                    if (checkForImages()) {
                        showLoader()
                        uploadMedia()
                    } else {
                        showLoader()
                        viewModel.placeBidAPI(bidId, bidDetail)
                    }
                }
            }

            R.id.btnAccept -> {
                formCheck()
                setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_PRODUCT_REJECT,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACC_BID_DETAILS)

            }
            R.id.btnReject -> {
                formCheck()
                setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_PRODUCT_ACCEPT,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACC_BID_DETAILS)
            }

            R.id.btnAcceptAll -> {
                checkAndAcceptAllItems()
                setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_ACCEPT_ALL,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACC_BID_DETAILS)

            }

            R.id.imgCall -> {
                CommonUtils.openPhoneCallApp("${
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                        AppENUM.RefactoredStrings.defaultCountryCode)
                } ${bidDetail.contactNumber ?: ""}", requireContext())
            }

            R.id.imgLocateMarker -> {
                val mapLik =
                    "https://maps.google.com/?q=${bidDetail.latitude},${bidDetail.longitude}&z=15"
                CommonUtils.openWebPage(requireContext(), mapLik)
            }

            R.id.etPricePerItem -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.etPricePerItem))?.let {
                    if (::bidItemsAdapter.isInitialized) {
                        bidItemsAdapter.dataList[it].isAvailable = false
                        bidItemsAdapter.notifyItemChanged(it)
                    }
                }
            }

            R.id.viewPrice -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.viewPrice))?.let {
                    priceEditedPosition = it
                }
            }

            R.id.btnCancelBid -> {
                showLoader()
                viewModel.cancelBid(bidId)
            }

            R.id.llUnlockMobile -> {
                showConfirmationDialogForUnlockMobile(bidDetail)
            }

            R.id.rlAddImage -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.rlAddImage))?.let {
                    TedPermission.create().setPermissionListener(object : PermissionListener {
                        override fun onPermissionGranted() {
                            selectedUploadPhotosPosition = it
                            TedImagePicker.with(requireContext()).showCameraTile(true)
                                .startMultiImage { uris ->
                                    onImagesPicked(uris)
                                }
                        }

                        override fun onPermissionDenied(deniedPermissions: MutableList<String>?) { // do nothing
                        }
                    }).setPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE).check()
                }
            }
        }
    }

    private fun uploadMedia() {
        if (::mediaToBeUploaded.isInitialized && mediaToBeUploaded.isNotEmpty()) {
            uploadImage(mediaToBeUploaded[0])
        }
    }

    private fun uploadImage(item: IdUriModel) {
        lifecycleScope.launch {
            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
            val itemIdRef: StorageReference = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
                .child(viewModel.getStringSharedPreference(AppENUM.USER_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                .child(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_BIDING_ORDER).child("$bidId/")
                .child("${item.id}/")

            val fileNameRand: String = (Calendar.getInstance().timeInMillis.toString())

            val uploadMediaRef = itemIdRef.child(fileNameRand)

            val uploadTask: UploadTask?

            val emptyBitmap = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(emptyBitmap)
            canvas.drawColor(ContextCompat.getColor(requireContext(), R.color.white))
            var bmp = try {
                MediaStore.Images.Media.getBitmap(requireContext().contentResolver, item.uri)
                    ?: emptyBitmap
            } catch (e: Exception) {
                emptyBitmap
            }
            val byteArrayOutputStream = ByteArrayOutputStream()
            bmp = CommonUtils.scaleBitmap(bmp)
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            val data = byteArrayOutputStream.toByteArray()
            uploadTask = uploadMediaRef.putBytes(data)

            // Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener { // Log.d("",it.toString())

                // Handle unsuccessful uploads
            }.addOnSuccessListener {

                // taskSnapshot.metadata contains file metadata such as size, content-type, etc.
                // ...
                uploadMediaRef.downloadUrl.addOnSuccessListener { downloadUrl ->
                    if (bidItemsAdapter.dataList[mediaToBeUploaded[0].position].isMediaUploaded == true) {
                        bidItemsAdapter.dataList[mediaToBeUploaded[0].position].addedImages?.add(
                            downloadUrl.toString())
                    } else {
                        bidItemsAdapter.dataList[mediaToBeUploaded[0].position].addedImages =
                            mutableListOf(downloadUrl.toString())
                    }
                    mediaToBeUploaded.removeAt(0)
                    if (mediaToBeUploaded.size > 0) {
                        uploadImage(mediaToBeUploaded[0])
                    } else {
                        makePlaceBidRequest()
                        viewModel.placeBidAPI(bidId, bidDetail)
                    }
                }
            }.addOnCompleteListener {

            }
        }
    }

    private fun checkForImages(): Boolean {
        var flag = false
        val uploadList = mutableListOf<IdUriModel>()
        var i = 0
        bidItemsAdapter.dataList.forEach { bidItem ->
            if ((bidItem.addedImages?.size ?: 0) > 1) {
                bidItem.addedImages?.forEach { image ->
                    if (image.isNotEmpty()) {
                        uploadList.add(IdUriModel(Uri.parse(image),
                            "${bidItem.brandId}${bidItem.carModelId}${bidItem.skuId}",
                            i))
                        flag = true
                    }
                }
            }
            i++
        }
        mediaToBeUploaded = uploadList
        return flag
    }

    private fun onImagesPicked(uris: List<Uri>) {
        if ((bidItemsAdapter.dataList[selectedUploadPhotosPosition].addedImages?.size?.plus(uris.size)
                ?.minus(1) ?: 0) > 5) {
            CommonUtils.showToast(requireContext(), getString(R.string.max_5_images))
        } else {
            uris.forEach {
                bidItemsAdapter.dataList[selectedUploadPhotosPosition].addedImages?.add(it.toString())
            }
            bidItemsAdapter.notifyItemChanged(selectedUploadPhotosPosition)
        }
    }

    private fun showConfirmationDialogForUnlockMobile(bidDetail: BidDetailResponse) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
            Bundle().apply {
                putString(AppENUM.TITLE, getString(R.string.unlock_contact_number))
                putString(AppENUM.DESCRIPTION,
                    getString(R.string.unlock_this_contact_number_using_10_gocoins))
                putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.okay))
                putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.cancel))
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    if (extra as? Boolean == true) {
                        showLoader()
                        viewModel.unlockContact(bidDetail.id.toString())
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun checkAndAcceptAllItems() {
        if (::bidItemsAdapter.isInitialized) {
            bidItemsAdapter.dataList.forEach {
                it.isAvailable = true
                it.isRejected = false
            }
            bidItemsAdapter.notifyItemRangeChanged(0, bidItemsAdapter.dataList.size)
            formCheck()
        }

        //                if (formCheck()) {
        //                    makePlaceBidRequest()
        //                    showLoader()
        //                    viewModel.placeBidAPI(bidId, bidDetail)
        //                }
    }


    private fun makePlaceBidRequest(): Boolean {
        var flag = false
        bidDetail.genericItems = mutableListOf()
        bidDetail.brandWiseItems = mutableListOf()
        var totalItems = 0
        var totalPrice = 0.0
        if (::bidItemsAdapter.isInitialized) {
            bidItemsAdapter.dataList.let {
                it.forEach { item ->
                    item.addedImages =
                        item.addedImages?.filter { img -> img.isNotEmpty() }?.toMutableList()
                    if (item.isAvailable == true && (item.pricePerItem ?: 0.0) < 1) {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.error_check_all_parts_for_price))
                        return false
                    }
                    if (item.isAvailable == true || item.isRejected == true) {
                        item.quantityAvailable?.let { qty ->
                            totalItems = totalItems.plus(qty)
                        }

                        if (item.isRejected == true) {
                            item.pricePerItem = 0.0
                        } else {
                            item.pricePerItem?.times(item.quantityAvailable ?: 1)?.let { price ->
                                totalPrice = totalPrice.plus(price)
                            }
                        }

                        if (item.isGeneric == true) {
                            bidDetail.genericItems?.add(item)
                        } else bidDetail.brandWiseItems?.add(item)
                    }

                    if (item.isAvailable == true) {
                        flag = true
                    }
                }
            }
        }

        if (flag) {
            bidDetail.totalItem = totalItems
            bidDetail.totalAmount = totalPrice
        } else {
            CommonUtils.showToast(requireContext(),
                getString(R.string.please_accept_at_least_one_item))
        }

        return flag
    }


    private fun formCheck(): Boolean {
        if (::bidItemsAdapter.isInitialized) {
            bidItemsAdapter.dataList.let {
                it.forEach { item ->
                    if (item.isAvailable == false && item.isRejected == false) {
                        binding.btnSubmit.isEnabled = false
                        return false
                    }
                }
            }
            binding.btnSubmit.isEnabled = true
            return true
        } else return false
    }


}