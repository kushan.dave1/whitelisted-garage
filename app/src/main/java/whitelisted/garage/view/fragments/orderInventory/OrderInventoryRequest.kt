package whitelisted.garage.view.fragments.orderInventory

import com.google.gson.annotations.SerializedName

data class OrderInventoryRequest(

    @field:SerializedName("inventory_checks") var inventoryChecks: MutableList<String>? = mutableListOf(),

    @field:SerializedName("mobile") var mobile: String? = "",

    @field:SerializedName("pdf_status") var pdfStatus: PdfStatusDataItem? = PdfStatusDataItem(),

    @field:SerializedName("id") var id: String? = "",

    @field:SerializedName("car_documents") var carDocuments: MutableList<String>? = mutableListOf(),

    @field:SerializedName("inventory_remarks") var inventoryRemarks: String? = "",

    @field:SerializedName("order_id") var orderId: String? = "",

    @field:SerializedName("inventory_items") var inventoryItems: MutableList<InventoryCheckListItem>? = mutableListOf(),

    @field:SerializedName("fuel_meter") var fuelMeter: String? = "")

data class PdfStatusDataItem(@field:SerializedName("status") var status: Boolean? = false,

    @field:SerializedName("pdf_file") var pdfFile: String? = "")

data class InventoryCheckListItem(

    @field:SerializedName("qty") var qty: String? = "0",

    @field:SerializedName("name") var name: String? = "")
