package whitelisted.garage.view.fragments.ordersList

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.OrderListResponseModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentOrdersListBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.OrdersAdapter
import whitelisted.garage.viewmodels.OrderListViewModel

class OrdersListFragment : BaseFragment() {

    private val binding by viewBinding(FragmentOrdersListBinding::inflate)
    private val viewModel: OrderListViewModel by viewModel()
    private var orderType: Int = 0
    private lateinit var ordersList: MutableList<OrderListResponseModel>
    private lateinit var ordersAdapter: OrdersAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListeners()

        observeOrderListResponse()
        setData()


    }

    private fun setData() {
        orderType = arguments?.getInt(AppENUM.ORDER_TYPE) ?: 0
        when (orderType) {
            0 -> binding.tvOrdersType.text = getString(R.string.ongoing_orders)
            1 -> binding.tvOrdersType.text = getString(R.string.open_orders)
            2 -> binding.tvOrdersType.text = getString(R.string.work_in_progress_orders)
            3 -> binding.tvOrdersType.text = getString(R.string.ready_orders)
            4 -> binding.tvOrdersType.text = getString(R.string.completed_orders)
        }

        showLoader()
        viewModel.getOrdersListAPI(orderType.toString())
    }

    private fun observeOrderListResponse() {
        viewModel.provideOrderListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    when (orderType) {
                        0 -> {
                            ordersList =
                                it.body.data.ongoingOrders?.toMutableList() ?: mutableListOf()
                            setOrderListAdapter(true)
                        }
                        1 -> {
                            ordersList = it.body.data.openOrders?.toMutableList() ?: mutableListOf()
                            setOrderListAdapter(false)
                        }
                        2 -> {
                            ordersList = it.body.data.wipOrders?.toMutableList() ?: mutableListOf()
                            setOrderListAdapter(false)
                        }
                        3 -> {
                            ordersList =
                                it.body.data.readyOrders?.toMutableList() ?: mutableListOf()
                            setOrderListAdapter(false)
                        }
                        4 -> {
                            ordersList =
                                it.body.data.completeOrders?.toMutableList() ?: mutableListOf()
                            setOrderListAdapter(false)
                        }
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setOrderListAdapter(showStatus: Boolean) {
        binding.rvOrdersList.layoutManager = LinearLayoutManager(requireContext())
        ordersAdapter =
            OrdersAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default)),
                ordersList,
                showStatus,
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                    AppENUM.RefactoredStrings.WORKSHOP_CONSTANT),
                this)
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                ordersAdapter.viewType = 0
            }
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                ordersAdapter.viewType = 0
            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                ordersAdapter.viewType = 1
            }
        }
        binding.rvOrdersList.adapter = ordersAdapter
    }

    private fun clickListeners() {
        binding.imgBack.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.cvBase -> {

                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
                        AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {

                            CommonUtils.addFragmentUtil(activity,
                                FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                                    Bundle().apply {
                                        putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                            ordersList[position].orderId)
                                    }),
                                target = R.id.fragment_container_2)
                        }
                        AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                            CommonUtils.addFragmentUtil(activity,
                                FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                                    Bundle().apply {
                                        putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                            ordersList[position].orderId)
                                    }),
                                target = R.id.fragment_container_2)
                        }
                        AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                            CommonUtils.addFragmentUtil(activity,
                                FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                                    Bundle().apply {
                                        putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                            ordersList[position].orderId)
                                    }),
                                target = R.id.fragment_container_2)
                        }
                        else -> {

                            CommonUtils.addFragmentUtil(activity,
                                FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                                    Bundle().apply {
                                        putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                            ordersList[position].orderId)
                                    }),
                                target = R.id.fragment_container_2)
                        }

                    } //                    Intent(activity, OrderDetailWrapperActivity::class.java).apply {
                    //                        putExtra(
                    //                            AppENUM.IntentKeysENUM.ORDER_ID,
                    //                            ordersList[position].orderId
                    //                        )
                    //                        startActivity(this)
                    //                    }

                }
            }
        }
    }

}