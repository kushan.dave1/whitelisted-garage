package whitelisted.garage.view.fragments.home.goCoins

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.razorpay.Checkout
import com.razorpay.PaymentData
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.AmountRequest
import whitelisted.garage.api.request.SSCheckoutRequest
import whitelisted.garage.api.response.GetGMDResponse
import whitelisted.garage.api.response.GetGOCoinOptionsResponse
import whitelisted.garage.api.response.GetGoCoinsScreenResponse
import whitelisted.garage.api.response.GoCoinUsageItem
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentGoCoinsBinding
import whitelisted.garage.eventBus.*
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeBackgroundDrawable
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.GCTransactionsAdapter
import whitelisted.garage.view.adapter.GCUsageAdapter
import whitelisted.garage.view.adapter.GoCoinsFAQAdapter
import whitelisted.garage.view.adapter.RecyclerDottedProgressAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.GoCoinsFragmentViewModel
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel

@Suppress("DEPRECATION")
class GoCoinsFragment : BaseFragment() {

    private val binding by viewBinding(FragmentGoCoinsBinding::inflate)
    private var isFAQToggled: Boolean = true
    private var isFromOutside: Boolean = false
    private var fromOutsideVal: String = AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_HOME
    private val viewModel: GoCoinsFragmentViewModel by viewModel()
    private val sparesShopSharedViewModel: SparesShopFragmentViewModel by sharedViewModel()
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var dataSize = 0
    private lateinit var goCoinsScreenResponse: GetGoCoinsScreenResponse
    private lateinit var razorpayCheckout: Checkout
    private var keyID = ""
    private var selectedAmount: String = "0"
    private var goCoinToGet: String = "0"
    private var receivedOrderId: String? = null
    private var paymentInitiated = false //    SECRET_KEY wJxyKEVBPJsv6d8H2LVizSSp
    private var goCoinValue = 1
    private var cartId = ""
    private lateinit var subscriptionId: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupScreen()
    }

    private fun observeResponses() {
        observeGetGoCoinOptions()
        observeScreenResponse()
        observeGMBResponse()
        observeOrderIdResponse()
        observeOrderIdSSResponse()
        observerRazorPayKeyResponse()
        observerRazorPayKeySSResponse()
        observeVerifyPaymentResponse()
        observeSSCheckoutResponse()
        observeBuyMembershipResponse()
    }

    private fun observeBuyMembershipResponse() {
        viewModel.provideBuyMembershipResponse().observe(viewLifecycleOwner) {
            hideLoader()
            paymentInitiated = false
            when (it) {
                is Result.Success -> { //                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    showLoader()
                    viewModel.getGoCoinsScreenResponse()
                    popBackStackAndHideKeyboard()
                    fireMembershipPurchaseEvent(true)
                    EventBus.getDefault().post(MembershipPurchasedEvent(true))
                    dashboardViewModel.provideMembershipPurchasedEvent().postValue(true)
                    CommonUtils.showReviewDialog(requireActivity())
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    if (isFromOutside) {
                        popBackStackAndHideKeyboard()
                    }
                }
            }
        }
    }

    private fun observeSSCheckoutResponse() {
        viewModel.provideSSCheckoutResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> { //                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    sparesShopSharedViewModel.provideCheckoutSuccessEvent().postValue(it.body.data)
                    firebaseEvent()
                    paymentInitiated = false
                }
                is Result.Failure -> {
                    paymentInitiated = false
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    sparesShopSharedViewModel.provideCheckoutFailureEvent().postValue("failed")
                    popBackStackAndHideKeyboard()
                }
            }
        }
    }

    private fun observeOrderIdSSResponse() {
        viewModel.provideOrderIdForSSPaymentResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    receivedOrderId = it.body.data.id
                    try {
                        openRazorPayPayment(selectedAmount.toDouble().times(100).toString())
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                is Result.Failure -> {
                    paymentInitiated = false
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    popBackStackAndHideKeyboard()
                }
            }
        }
    }

    private fun observerRazorPayKeySSResponse() {
        viewModel.provideRazorPayKeySSResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    keyID = it.body.data.key ?: ""
                    initiateRazorpay()
                    selectedAmount.toDoubleOrNull()
                        ?.let { amount -> viewModel.getOrderIdForSS(AmountRequest(amount, cartId)) }
                        ?: popBackStackAndHideKeyboard()

                }
                is Result.Failure -> {
                    paymentInitiated = false
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    popBackStackAndHideKeyboard()
                }
            }
        }
    }

    private fun setupScreen() {
        goCoinValue = viewModel.getIntSharedPreference(AppENUM.GO_COINS_VALUE)
        observeResponses()
        clickListeners()
        arguments?.let {
            isFromOutside = it.getBoolean(AppENUM.IS_FROM_OUTSIDE, false)
            fromOutsideVal =
                it.getString(AppENUM.IS_FROM, AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_HOME)
            if (isFromOutside) {
                selectedAmount = it.getString(AppENUM.AMOUNT_TO_BE_PURCHASED, "0") ?: "0"
                goCoinToGet = it.getString(AppENUM.GO_COIN_TO_GET, "0") ?: "0"
                cartId = it.getString(AppENUM.CART_ID, "")
            }
        }
        if (isFromOutside) {
            binding.nsScroll.makeVisible(false)
            binding.btnPay.callOnClick()
        } else {
            addTextWatcher()
            binding.nsScroll.makeVisible(true)
            showLoader()
            viewModel.getGoCoinsScreenResponse()
            viewModel.getGoCoinsOptions()
        }

        setDataToBuyBtn(selectedAmount)
        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            AppENUM.RefactoredStrings.defaultCurrencySymbol).apply {
            "${this}0".let {
                binding.tvAmount.setText(it)
            }
            binding.tvCurrency1.text = this
            binding.tvCurrency2.text = this
            binding.tvCurrency3.text = this
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setDataToBuyBtn(selectedAmount: String) {
        binding.btnPay.text = "${getString(R.string.pay)} ${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol)
        }${selectedAmount} ${
            getString(R.string.securely)
        }"
    }

    private fun addTextWatcher() {
        binding.tvAmount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?,
                start: Int,
                count: Int,
                after: Int) { // nothing
            }

            override fun onTextChanged(s: CharSequence?,
                start: Int,
                before: Int,
                count: Int) { // nothing
            }

            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().trim().isNotEmpty()) {
                    try {
                        selectedAmount = s.toString()
                        setDataToBuyBtn(selectedAmount)
                        (selectedAmount.toInt().times(goCoinValue).toString()).apply {
                            binding.etGoCoinAmount.setText(this)
                            goCoinToGet = this
                        }
                    } catch (e: Exception) {
                        Log.e(this.javaClass.name, e.toString())
                    }
                } else {
                    binding.etGoCoinAmount.setText("0")
                    setDataToBuyBtn("0")
                    selectedAmount = "0"
                    goCoinToGet = "0"
                }
                disableAllOptions()
            }
        })
    }

    private fun observeGetGoCoinOptions() {
        viewModel.provideGetGoCoinOptions().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setDataToOptions(it.body.data)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setDataToOptions(options: GetGOCoinOptionsResponse) {
        if ((options.gocoinsOptions?.size ?: 0) >= 3) {
            binding.tvOption1.text = (options.gocoinsOptions?.get(0) ?: 0).toString()
            binding.tvOption2.text = (options.gocoinsOptions?.get(1) ?: 0).toString()
            binding.tvOption3.text = (options.gocoinsOptions?.get(2) ?: 0).toString()
            when {
                options.defaultSelect.toString() == options.gocoinsOptions?.get(0) -> {
                    binding.tvPop1.makeVisible(true)
                }
                options.defaultSelect.toString() == options.gocoinsOptions?.get(1) -> {
                    binding.tvPop2.makeVisible(true)
                }
                options.defaultSelect.toString() == options.gocoinsOptions?.get(2) -> {
                    binding.tvPop3.makeVisible(true)
                }
            }
        }
        binding.llOption1.callOnClick()
        binding.tvAmount.requestFocus()
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    private fun observeVerifyPaymentResponse() {
        viewModel.provideVerifyPaymentResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    showLoader()
                    viewModel.getGoCoinsScreenResponse()
                    if (isFromOutside) {
                        popBackStackAndHideKeyboard()
                        EventBus.getDefault().post(GoCoinsAddedEvent())
                        EventBus.getDefault().post(IsFromGMBEvent())
                    }
                    firebaseEvent()
                    paymentInitiated = false
                    CommonUtils.showReviewDialog(requireActivity())
                }
                is Result.Failure -> {
                    paymentInitiated = false
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    if (isFromOutside) {
                        popBackStackAndHideKeyboard()
                    }
                }
            }
        }
    }

    private fun firebaseEvent() {
        Bundle().apply {
            val firesScreen = if (isFromOutside) {
                if (fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP) {
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_MARKETPLACE
                } else FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_GMB_COMBO
            } else {
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_WALLET
            }
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN, firesScreen)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.GC_ADDED, selectedAmount)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_PURCHASE_GO_COINS,
                this)
        }
    }

    private fun observerRazorPayKeyResponse() {
        viewModel.provideRazorPayKeyResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    keyID = it.body.data.key ?: ""
                    initiateRazorpay()
                    if (isFromOutside) {
                        if (fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_GMB) viewModel.getOrderId(
                            selectedAmount,
                            goCoinToGet,
                            getString(R.string.display_msg_gmb))
                        else if (fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_HOME || fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_INSUFFICIENT_DIALOG) {
                            viewModel.getOrderId(selectedAmount,
                                goCoinToGet,
                                getString(R.string.add_gocoins))
                        } else if (fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_PRO) {
                            viewModel.getOrderId(selectedAmount,
                                goCoinToGet,
                                getString(R.string.purchase_membership),
                                isMembership = "true") //                            lifecycleScope.launch {
                            //                                val result = viewModel.initiateSubscription(selectedAmount)
                            //                                result?.let { subsRes ->
                            //                                    subscriptionId = subsRes.id ?: ""
                            //                                    openRazorPayPayment(selectedAmount.toDouble().times(100)
                            //                                        .toString(), subsRes.shortUrl, subsRes.id)
                            //                                }
                            //                            }
                        }
                    } else {
                        viewModel.getOrderId(selectedAmount,
                            goCoinToGet,
                            getString(R.string.add_gocoins))
                    }
                }
                is Result.Failure -> {
                    paymentInitiated = false
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    if (isFromOutside) {
                        popBackStackAndHideKeyboard()
                    }
                }
            }
        }
    }

    private fun observeOrderIdResponse() {
        viewModel.provideOrderIdForPaymentResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    receivedOrderId = it.body.data.id
                    try {
                        openRazorPayPayment(selectedAmount.toDouble().times(100).toString())
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                is Result.Failure -> {
                    paymentInitiated = false
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    if (isFromOutside) {
                        popBackStackAndHideKeyboard()
                    }
                }
            }
        }
    }

    private fun initiateRazorpay() {
        Checkout.preload(context?.applicationContext)
        razorpayCheckout = Checkout()
        razorpayCheckout.setKeyID(keyID)
        razorpayCheckout.setImage(R.drawable.saas_logo)
    }

    private fun openRazorPayPayment(amountInPisa: String,
        callBackUrl: String? = null,
        subscriptionId: String? = null) {
        hideLoader()
        try {
            val options = JSONObject()
            options.put("name", getString(R.string.app_name))
            options.put("description",
                getString(R.string.garage_management_services)) //You can omit the image option to fetch the image from dashboard
            //            options.put("image", "")
            options.put("theme.color", "#00579c")
            options.put("currency",
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                    AppENUM.RefactoredStrings.defaultCurrency)) //            options.put("order_id", id)
            options.put("amount", amountInPisa) //pass amount in currency subunits

            callBackUrl?.let {
                options.put("key", keyID)
                options.put("subscription_id",
                    subscriptionId) //                options.put("callback_url", callBackUrl)
                options.put("recurring", "1")
                options.put("redirect", true)
            }

            val retryObj = JSONObject()
            retryObj.put("enabled", true)
            retryObj.put("max_count", 4)
            options.put("retry", retryObj)

            val prefill = JSONObject()
            prefill.put("name",
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.OWNER_NAME,
                    "")) //            prefill.put("email", "gaurav.kumar@example.com")
            prefill.put("contact",
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                    getString(R.string.ind_country_code))
                    .replace("+", "") + viewModel.getStringSharedPreference(AppENUM.MOBILE_NUMBER,
                    ""))
            var method = ""
            arguments?.getString(AppENUM.RazorPayENUM.RAZORPAY_METHOD, "")?.let {
                if (it.isNotEmpty()) {
                    prefill.put(AppENUM.RazorPayENUM.RAZORPAY_METHOD, it)
                    method = it
                }
            }
            if (method == AppENUM.RazorPayENUM.RAZORPAY_WALLET || method == AppENUM.RazorPayENUM.RAZORPAY_UPI) {
                arguments?.getString(AppENUM.RazorPayENUM.RAZORPAY_MODE, "")?.let {
                    if (it.isNotEmpty()) {
                        prefill.put(method, it)
                    }
                }
            }

            options.put("prefill", prefill)
            razorpayCheckout.open(requireActivity(), options)

        } catch (e: Exception) {
            CommonUtils.showToast(requireContext(), "Error in payment: " + e.message, true)
            e.printStackTrace()
        }
    }

    private fun observeScreenResponse() {
        viewModel.provideGoCoinsScreenResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    goCoinsScreenResponse = it.body.data
                    setData()
                    showLoader()
                    viewModel.getGMB()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    if (isFromOutside) {
                        popBackStackAndHideKeyboard()
                    }
                }
            }
        }
    }

    private fun observeGMBResponse() {
        viewModel.provideGMD().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    goCoinsScreenResponse.GoCoinUsageItem?.let { goCoinUsageItem ->
                        gmbResponseSuccess(goCoinUsageItem, it)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    if (isFromOutside) {
                        popBackStackAndHideKeyboard()
                    }
                }
            }
        }
    }

    private fun gmbResponseSuccess(goCoinUsageItem: MutableList<GoCoinUsageItem>,
        it: Result.Success<ServerResponse<GetGMDResponse>>) {
        if (viewModel.getIntSharedPreference(AppENUM.GO_COINS_VALUE) != 0) {
            binding.rvGCUsage.adapter = GCUsageAdapter((it.body.data.gmbPrice ?: "0").toInt()
                .div(viewModel.getIntSharedPreference(AppENUM.GO_COINS_VALUE)).toString(),
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    AppENUM.RefactoredStrings.defaultCurrencySymbol),
                goCoinUsageItem,
                this)
        } else {
            binding.rvGCUsage.adapter =
                GCUsageAdapter((it.body.data.gmbPrice ?: "0").toInt().div(1).toString(),
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        AppENUM.RefactoredStrings.defaultCurrencySymbol),
                    goCoinUsageItem,
                    this)
        }
        if (goCoinUsageItem.isNotEmpty()) {
            val data = mutableListOf<Boolean>()
            for (i in 0 until goCoinUsageItem.size) {
                data.add(false)
            }
            data[0] = true
            binding.progressRecycler.adapter = RecyclerDottedProgressAdapter(data)
            dataSize = data.size
            setUpViewPager()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        binding.tvGCBalance.text = goCoinsScreenResponse.gocoinBalance.toString()
        binding.tvGCAmount.text = "${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol)
        }${goCoinsScreenResponse.goCoinTotalBalance.toString()}"
        viewModel.putStringSharedPreference(AppENUM.GO_COIN_BALANCE_AMOUNT,
            goCoinsScreenResponse.gocoinBalance.toString())
        EventBus.getDefault()
            .post(GoCoinBalanceReceivedEvent(goCoinsScreenResponse.gocoinBalance ?: 0))
        if (goCoinsScreenResponse.goCoinTransactions?.isNotEmpty() == true) {
            binding.rlTransactionHistory.makeVisible(true)
            binding.rvTransactionHistory.adapter =
                GCTransactionsAdapter(goCoinsScreenResponse.goCoinTransactions?.toMutableList()
                    ?: mutableListOf())
        }
        if (goCoinsScreenResponse.moreTransactions == true) {
            binding.tvViewAll.makeVisible(true)
        } else binding.tvViewAll.makeVisible(false)

        binding.rvGoCoinsFAQ.adapter =
            GoCoinsFAQAdapter(goCoinsScreenResponse.goCoinFaqs?.toMutableList() ?: mutableListOf(),
                this)
        binding.rvGoCoinsFAQ.makeVisible(false)
    }

    private fun setUpViewPager() {
        binding.rvGCUsage.clipToPadding = false
        binding.rvGCUsage.clipChildren = false
        binding.rvGCUsage.offscreenPageLimit = 3
        binding.rvGCUsage.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.rvGCUsage.setPadding(0, 0, 72, 0)
        val pageTranslationX = (32).toFloat()
        binding.rvGCUsage.setPageTransformer { page, position ->
            page.translationX = -pageTranslationX * position
        }
        if (binding.rvGCUsage.childCount > 0) binding.rvGCUsage.getChildAt(0)?.overScrollMode =
            RecyclerView.OVER_SCROLL_NEVER

        binding.rvGCUsage.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrolled(position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)

                val newData = mutableListOf<Boolean>()
                for (i in 0 until dataSize) {
                    if (i == position) {
                        newData.add(i, true)
                    } else {
                        newData.add(i, false)
                    }
                }
                (binding.progressRecycler.adapter as RecyclerDottedProgressAdapter).setData(newData)
            }
        })
    }

    private fun clickListeners() {
        binding.imgBack.setOnClickListener(this)
        binding.btnPay.setOnClickListener(this)
        binding.tvViewAll.setOnClickListener(this)
        binding.llOption1.setOnClickListener(this)
        binding.llOption2.setOnClickListener(this)
        binding.llOption3.setOnClickListener(this)
        binding.imgToggle.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.clBase -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.model))?.let { position ->
                    (binding.rvGoCoinsFAQ.adapter as? GoCoinsFAQAdapter)?.let {
                        it.dataList[position].isSelected = it.dataList[position].isSelected != true
                        it.notifyItemChanged(position)
                    }
                }
            }
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.imgGCUsage -> {
                CommonUtils.genericCastOrNull<String>(v.getTag(R.id.model))?.let {
                    setBannerNavigation(it)
                }
            }

            R.id.btnPay -> {
                onBtnPayClick()
            }

            R.id.tvViewAll -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.GO_COIN_TRANSACTIONS, null),
                )
            }
            R.id.llOption1 -> {
                enableDisableViews(R.id.llOption1)
            }
            R.id.llOption2 -> {
                enableDisableViews(R.id.llOption2)
            }
            R.id.llOption3 -> {
                enableDisableViews(R.id.llOption3)
            }
            R.id.imgToggle -> {
                onToggleClick()
            }
        }
    }

    private fun onToggleClick() {
        if (isFAQToggled) {
            isFAQToggled = false
            binding.imgToggle.setImageResource(R.drawable.ic_arrow_up_black)
            binding.rvGoCoinsFAQ.makeVisible(true) // binding.nsScroll.smoothScrollTo(0, binding.nsScroll.bottom)

            binding.nsScroll.post {
                binding.nsScroll.fullScroll(View.FOCUS_DOWN)
            }
        } else {
            isFAQToggled = true
            binding.imgToggle.setImageResource(R.drawable.ic_arrow_down_black)
            binding.rvGoCoinsFAQ.makeVisible(false)
        }
    }

    private fun onBtnPayClick() {
        setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_GO_COIN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_GO_COIN)
        if (selectedAmount.isNotEmpty() && selectedAmount != "0") {
            if (!isFromOutside) {
                if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                        AppENUM.RefactoredStrings.defaultCurrency) == AppENUM.RefactoredStrings.defaultCurrencySymbol) {
                    checkAmountAndProceed()
                } else {
                    checkInternationalAmountAndProceed()
                }
            } else {
                if (!paymentInitiated) {
                    paymentInitiated = true
                    showLoader()
                    if (fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP) {
                        viewModel.getRazorPayKeyForSS()
                    } else viewModel.getRazorPayKey()
                }
            }
        } else {
            CommonUtils.showToast(requireContext(),
                getString(R.string.plesae_enter_the_valid_go_coin))
        }
    }

    private fun checkInternationalAmountAndProceed() {
        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            AppENUM.RefactoredStrings.defaultCurrencySymbol).let {
            if (selectedAmount.contains(it)) selectedAmount.replace(it, "")
        }

        if (!paymentInitiated && (selectedAmount.toInt() >= 10)) {
            paymentInitiated = true
            showLoader()
            viewModel.getRazorPayKey()
        } else {
            "${getString(R.string.minimum_amonut_is)} ${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    AppENUM.RefactoredStrings.defaultCurrencySymbol)
            }10".apply {
                CommonUtils.showToast(requireContext(), this)
            }
        }
    }

    private fun checkAmountAndProceed() {
        if (!paymentInitiated && (selectedAmount.toInt() >= 50)) {
            paymentInitiated = true
            showLoader()
            viewModel.getRazorPayKey()
        } else {
            "${getString(R.string.minimum_amonut_is)} ${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    AppENUM.RefactoredStrings.defaultCurrencySymbol)
            }50".apply {
                CommonUtils.showToast(requireContext(), this)
            }
        }
    }

    private fun setBannerNavigation(redirection: String) {
        when (redirection) {
            AppENUM.ConfigConstants.GMB_COMBO -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.GMB_INFO, null),
                )
                setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_G_COMBO,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_GO_COIN)
            }
            AppENUM.ConfigConstants.INSTNAT_REMINDER -> {
                activity?.let {
                    FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.INSTANT_REMINDER,
                        null,
                        object : ActionListener {
                            @RequiresApi(Build.VERSION_CODES.P)
                            override fun onActionItem(extra: Any?, extra2: Any?) {
                                if (extra as? String == AppENUM.OPEN_DIALOG) {
                                    openSuccessDialog()
                                } else if (extra == AppENUM.OPEN_FRAG) {
                                    openCustomerReminderList()
                                }
                            }
                        })?.let { baseFragment ->
                        baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
                    }
                }

                Bundle().apply {
                    putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_GO_COIN)
                    FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_INSTANT_REMINDER,
                        this)
                }
            }
        }
    }

    private fun openCustomerReminderList() {
        CommonUtils.addFragmentUtil(
            activity,
            FragmentFactory.fragment(FragmentFactory.Fragments.CUSTOMER_REMINDER_FRAGMENT, null),
        )
    }

    private fun openSuccessDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.REMINDER_SENT_SUCCESS,
            null,
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) { //
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is PaymentEventModel -> {
                (event.extra as? PaymentData)?.let { payment ->
                    if (!payment.paymentId.isNullOrEmpty()) {
                        if (isFromOutside && fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP) {
                            viewModel.checkoutSSOrder(SSCheckoutRequest(razorPayOrderId = receivedOrderId,
                                razorPayPaymentId = payment.paymentId,
                                paymentAmount = selectedAmount,
                                paymentCurrency = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                                    AppENUM.RefactoredStrings.defaultCurrency),
                                cartId = cartId,
                                clientAddressDetails = arguments?.getParcelable(AppENUM.ADDRESS_POST)))
                        } else if (isFromOutside && fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_PRO) { //                            if (::subscriptionId.isInitialized && payment.signature != null) {
                            viewModel.buyMembership(payment.paymentId,
                                receivedOrderId ?: "",
                                selectedAmount,
                                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                                    AppENUM.RefactoredStrings.defaultCurrency)) //                            } else {
                            //                                CommonUtils.showToast(requireContext(),
                            //                                    getString(R.string.payment_failed))
                            //                                lifecycleScope.launch {
                            //                                    delay(200)
                            //                                    popBackStackAndHideKeyboard()
                            //                                }
                            //                            }
                        } else {
                            viewModel.sendPaymentDetailsToServer(payment.paymentId,
                                receivedOrderId ?: "",
                                selectedAmount,
                                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                                    AppENUM.RefactoredStrings.defaultCurrency))
                        }
                    } else {
                        if (isFromOutside && fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_PRO) {
                            lifecycleScope.launch {
                                delay(400)
                                fireMembershipPurchaseEvent(false)
                                popBackStackAndHideKeyboard()
                                EventBus.getDefault().post(MembershipPurchasedEvent(false))
                            }
                        } else if (isFromOutside && fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP) {
                            lifecycleScope.launch {
                                delay(200)
                                if (fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP) sparesShopSharedViewModel.provideCheckoutFailureEvent()
                                    .postValue("failed")
                                popBackStackAndHideKeyboard()
                            }
                        } else {
                            lifecycleScope.launch {
                                delay(200)
                                popBackStackAndHideKeyboard()
                            }
                        }
                    }
                } ?: run {
                    if (isFromOutside && fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_PRO) {
                        lifecycleScope.launch {
                            delay(200)
                            fireMembershipPurchaseEvent(false)
                            EventBus.getDefault().post(MembershipPurchasedEvent(false))
                            popBackStackAndHideKeyboard()
                        }
                    } else if (isFromOutside && fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP) {
                        lifecycleScope.launch {
                            delay(200)
                            if (fromOutsideVal == AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP) sparesShopSharedViewModel.provideCheckoutFailureEvent()
                                .postValue("failed")
                            popBackStackAndHideKeyboard()
                        }
                    } else {
                        lifecycleScope.launch {
                            delay(200)
                            popBackStackAndHideKeyboard()
                        }
                    }
                }
            }
        }
    }

    private fun enableDisableViews(id: Int) {
        when (id) {
            R.id.llOption1 -> {
                binding.tvAmount.setText(binding.tvOption1.text.toString())
                binding.tvOption1.changeTextColor(requireContext(), R.color.counter_bg)
                binding.tvCurrency1.changeTextColor(requireContext(), R.color.counter_bg)
                binding.llOption1.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_popular)
                binding.tvOption2.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvOption3.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvCurrency2.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvCurrency3.changeTextColor(requireContext(), R.color.tab_gray)
                binding.llOption2.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_nor)
                binding.llOption3.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_nor)
            }
            R.id.llOption2 -> {
                binding.tvAmount.setText(binding.tvOption2.text.toString())
                binding.tvOption2.changeTextColor(requireContext(), R.color.counter_bg)
                binding.tvCurrency2.changeTextColor(requireContext(), R.color.counter_bg)
                binding.llOption2.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_popular)
                binding.tvCurrency1.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvCurrency3.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvOption1.changeTextColor(requireContext(), R.color.tab_gray)
                binding.llOption1.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_nor)
                binding.tvOption3.changeTextColor(requireContext(), R.color.tab_gray)
                binding.llOption3.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_nor)
            }
            R.id.llOption3 -> {
                binding.tvAmount.setText(binding.tvOption3.text.toString())
                binding.tvOption3.changeTextColor(requireContext(), R.color.counter_bg)
                binding.tvCurrency3.changeTextColor(requireContext(), R.color.counter_bg)
                binding.llOption3.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_popular)
                binding.tvCurrency1.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvCurrency2.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvOption2.changeTextColor(requireContext(), R.color.tab_gray)
                binding.llOption2.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_nor)
                binding.tvOption1.changeTextColor(requireContext(), R.color.tab_gray)
                binding.llOption1.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_nor)
            }
        }
        binding.tvAmount.text?.length?.let { binding.tvAmount.setSelection(it) }
    }

    private fun disableAllOptions() {
        binding.tvCurrency1.changeTextColor(requireContext(), R.color.tab_gray)
        binding.tvCurrency2.changeTextColor(requireContext(), R.color.tab_gray)
        binding.tvCurrency3.changeTextColor(requireContext(), R.color.tab_gray)
        binding.tvOption2.changeTextColor(requireContext(), R.color.tab_gray)
        binding.llOption2.changeBackgroundDrawable(requireContext(),
            R.drawable.bg_go_coins_options_nor)
        binding.tvOption1.changeTextColor(requireContext(), R.color.tab_gray)
        binding.llOption1.changeBackgroundDrawable(requireContext(),
            R.drawable.bg_go_coins_options_nor)
        binding.tvOption3.changeTextColor(requireContext(), R.color.tab_gray)
        binding.llOption3.changeBackgroundDrawable(requireContext(),
            R.drawable.bg_go_coins_options_nor)
    }

    override fun onResume() {
        super.onResume()
        paymentInitiated = false
    }


    private fun fireMembershipPurchaseEvent(isPurchased: Boolean) {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.SUBSCRIPTION_INFO)
        bundle.putBoolean(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.PAYMENT_STATUS, isPurchased)
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.PRO_PAYMENT_STATUS,
            bundle)
    }


}