package whitelisted.garage.view.fragments.billing

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.BillingResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseActivity
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentBillingBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.BillingListAdapter
import whitelisted.garage.viewmodels.BillingFragmentViewModel
import java.util.*

class BillingFragment : BaseFragment() {

    private val binding by viewBinding(FragmentBillingBinding::inflate)
    private val viewModel: BillingFragmentViewModel by viewModel()
    private lateinit var billingList: MutableList<BillingResponseModel>
    private lateinit var billingListAdapter: BillingListAdapter
    private var startDate = ""
    private var endDate = ""
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    fun apiCall() {
//        if ((activity as BaseActivity<*>).getBillLoader()) {
//            (activity as BaseActivity<*>).setBillLoader(false)
            showLoader()
            viewModel.getBillingList(startDate, endDate)
//        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupScreen() {
        if (arguments?.getBoolean(AppENUM.IntentKeysENUM.IS_UNDER_TAB, false) == true) {
            binding.imgBackFB.makeVisible(false)
            handleBackPress()
        }

        binding.imgBackFB.setOnClickListener(this)
        binding.tvSelectionDate.setOnClickListener(this)
        binding.tvDownloadSheet.setOnClickListener(this)
        val today = Calendar.getInstance()
        endDate = "${today.get(Calendar.DAY_OF_MONTH)}-${today.get(Calendar.MONTH) + 1}-${
            today.get(Calendar.YEAR)
        }"
        today.timeInMillis = today.timeInMillis - 2629800000
        startDate = "${today.get(Calendar.DAY_OF_MONTH)}-${today.get(Calendar.MONTH) + 1}-${
            today.get(Calendar.YEAR)
        }"
        binding.tvSelectionDate.text = "${CommonUtils.convertNumMonthDateToNormalWithoutUTC(startDate)} - ${
            CommonUtils.convertNumMonthDateToNormalWithoutUTC(endDate)
        }"
        observeBillingList()
        observeDownloadCSVResponse()

        binding.srl.setOnRefreshListener {
            binding.srl.isRefreshing = false
            showLoader()
            viewModel.getBillingList(startDate, endDate)
        }

        "${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }0.0".apply {
            binding.totalAmountTv.text = this
            binding.totalPaidTv.text = this
            binding.totalDueTv.text = this
            binding.totalDisCountTv.text = this
        }

        apiCall()
    }

    private fun observeDownloadCSVResponse() {
        viewModel.provideDownloadCSVResponse().observe(viewLifecycleOwner) {
            CommonUtils.showToast(requireContext(), getString(R.string.downloading), true)
            CommonUtils.saveToDisk(requireActivity(),
                it,
                "${viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME)}${AppENUM.BILLING_CSV}",
                "${viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME)}${AppENUM.BILLING_CSV}",
                getString(R.string.download_billing_csv))
        }
    }

    private fun observeBillingList() {
        viewModel.provideBillingListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    billingList = it.body.data.toMutableList()
                    if (billingList.size > 0) {
                        binding.tvDownloadSheet.makeVisible(true)
                    } else binding.tvDownloadSheet.makeVisible(false)
                    calculatesToleAmount(it.body.data)

                    setAdapter()
//                    if (arguments?.getBoolean(AppENUM.IntentKeysENUM.IS_UNDER_TAB, false) == true) {
//                        checkWalkthrough()
//                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun checkWalkthrough() {
        if (isVisible) {
            if (!viewModel.getBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN, true)) {
                Handler(Looper.getMainLooper()).postDelayed({
                    if (binding.tvDownloadSheet.visibility == View.GONE) binding.tvDownloadSheet.makeVisible(
                        true)
                    startWalkthrough(binding.tvDownloadSheet)
                }, 500)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun calculatesToleAmount(data: List<BillingResponseModel>) {
        var total = 0.0
        var tootlePaid = 0.0
        var tootleDue = 0.0
        var discount = 0.0
        for (amount in data) {
            total += amount.invoiceAmount ?: 0.0
            tootlePaid += amount.paidAmount ?: 0.0
            tootleDue += amount.dueAmount ?: 0.0
            discount += amount.discountValue ?: 0.0
        }
        binding.totalAmountTv.text = "${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }${String.format("%.1f", total)}"
        binding.totalPaidTv.text = "${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }${String.format("%.1f", tootlePaid)}"

        if (String.format(Locale.ENGLISH, "%.1f", tootleDue).toDouble() < 0.0) {
            binding.totalDueTv.text = "${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    getString(R.string.currency_symbol_default))
            }${
                String.format("%.1f", 0.0)
            }"
        } else {
            binding.totalDueTv.text = "${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    getString(R.string.currency_symbol_default))
            }${
                String.format("%.1f", tootleDue)
            }"
        }

        binding.totalDisCountTv.text = "${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }${String.format("%.1f", discount)}"
    }

    private fun setAdapter() {
        if (billingList.size > 0) {
            binding.noDataTv.makeVisible(false)
        } else {
            binding.noDataTv.makeVisible(true)
        }
        binding.rvBillingList.layoutManager = LinearLayoutManager(requireContext())
        billingListAdapter =
            BillingListAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default)), billingList, this)
        binding.rvBillingList.adapter = billingListAdapter
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBackFB -> popBackStackAndHideKeyboard()
            R.id.tvSelectionDate -> {
                showSelectDateDialog(true)
            }
            R.id.tvDownloadSheet -> {
                Bundle().apply {
                    putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_BILLING)
                    putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.BILL_DATE,
                        "from :$startDate , to: $endDate")
                    FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_BILL_DOWNLOAD,
                        this)
                }
                viewModel.downloadBillingCSV(startDate, endDate)
            }
            R.id.btnDownloadPdf -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    Intent(Intent.ACTION_VIEW).apply {
                        this.data = Uri.parse(billingList[position].billPdf)
                        startActivity(this)
                    }
                }
            }
            R.id.clBaseBuyGC -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.RETAILER_CONSTANT) {
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                                Bundle().apply {
                                    putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                        billingList[position].orderId)
                                }))
                    } else {
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                                Bundle().apply {
                                    putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                        billingList[position].orderId)
                                }))
                    }
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun showSelectDateDialog(isStart: Boolean) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_DATE, Bundle().apply {
            putBoolean(AppENUM.IS_START, isStart)
            if (!isStart) putString(AppENUM.START_TIME,
                CommonUtils.returnMillisForDate(startDate).toString())
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                if (isStart) {
                    startDate = extra as String
                    showSelectDateDialog(false)
                } else {
                    endDate = extra as String
                    ("${CommonUtils.convertNumMonthDateToNormalWithoutUTC(startDate)} - ${
                        CommonUtils.convertNumMonthDateToNormalWithoutUTC(endDate)
                    }").apply {
                        binding.tvSelectionDate.text = this
                    }
                    showLoader()
                    viewModel.getBillingList(startDate, endDate)
                }
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }
}