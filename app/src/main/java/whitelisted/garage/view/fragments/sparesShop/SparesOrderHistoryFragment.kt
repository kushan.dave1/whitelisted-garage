package whitelisted.garage.view.fragments.sparesShop

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.response.Invoice
import whitelisted.garage.api.response.SparesOrderHistory
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.CardSparesPurchaseHistoryBinding
import whitelisted.garage.databinding.FragmentSparesOrderHistoryBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.CommonUtils.onScrolledToBottom
import whitelisted.garage.utils.CommonUtils.splitCamelCase
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import java.io.IOException

class SparesOrderHistoryFragment : BaseFragment() {

    private val binding by viewBinding(FragmentSparesOrderHistoryBinding::inflate)

    private val sparesShopSharedViewModel: SparesShopFragmentViewModel by sharedViewModel()
    private var symbol = ""
    private lateinit var filterPopupWindow: PopupWindow
    private var history: MutableList<SparesOrderHistory> = mutableListOf()
    private var limit = 4
    private var filter: String? = null


    companion object {
        const val STATUS_DELIVERED = "delivered"
        const val STATUS_DISPATCHED = "dispatched"
        const val STATUS_PENDING = "pending"
        const val STATUS_IN_TRANSIT = "in_transit"
        const val STATUS_CANCELED = "cancelled"
        const val STATUS_OUT_FOR_DELIVERY = "out_for_delivery"
        val RTO = listOf("RTO", "rto_complete", "RTD", "rto_in_transit")
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        symbol =
            sparesShopSharedViewModel.getStringSharedPreference(
                AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol
            )

        setObserver()
        setupScreen()
        checkIfNested()
    }

    private fun setObserver() {
        sparesShopSharedViewModel.providePurchaseHistoryResponse().observe(viewLifecycleOwner) {
            binding.tvTotalOrders.text = it.count.toString()
            if (it.count > 0) {
                binding.clEmptyList.makeVisible(false)
                binding.rvPurchaseHistory.makeVisible(true)
            } else {
                binding.rvPurchaseHistory.makeVisible(false)
                binding.clEmptyList.makeVisible(true)
            }
        }
    }

    private fun checkIfNested() {
        val isNested = arguments?.getBoolean(AppENUM.IS_NESTED) ?: false
        binding.headerGroup.makeVisible(!isNested)
    }

    private fun setupScreen() {
        binding.imgBack.setOnClickListener(::onItemClick)
        binding.btnEmptyAction.setOnClickListener(::onItemClick)
        binding.rvPurchaseHistory.setLayoutManagerAsLinear()
        getPurchaseHistory()
        setupFilterSpinner()
        binding.filterText.setOnClickListener {
            filterPopupWindow.showAsDropDown(it, 0, 0, Gravity.END)
        }
        binding.srl1.setOnRefreshListener {
            isLastPageRV = false
            getPurchaseHistory()
        }

        setRecyclerPagination()
    }

    private fun setRecyclerPagination() {
        binding.rvPurchaseHistory.onScrolledToBottom {
            if (!isLoadingRV && !isLastPageRV) {
                isLoadingRV = true
                getPurchaseHistory(true, binding.rvPurchaseHistory.adapter?.itemCount ?: 0)
            }
        }
    }

    private fun getPurchaseHistory(isPagination: Boolean = false, offset: Int = 0) =
        lifecycleScope.launch {
            val isRefreshing = binding.srl1.isRefreshing
            if (!isRefreshing) showLoader()

            sparesShopSharedViewModel.getPurchaseHistory(limit, offset, filter)?.let {
                if (!isPagination) history.clear()
                history.addAll(it)
                isLoadingRV = false
                if (it.size < limit) isLastPageRV = true
                setAdapter(it, isPagination)
                //binding.filterText.text = getFilters()[0].splitCamelCase()
            }
            hideLoader()
            binding.srl1.isRefreshing = false //        binding.srl2.isRefreshing = false
        }

    private fun setAdapter(history: List<SparesOrderHistory>, isPagination: Boolean = false) =
        binding.apply {
            if (history.isNotEmpty()) {
                if (!isPagination) rvPurchaseHistory.clear()
                rvPurchaseHistory.addViews(
                    history,
                    CardSparesPurchaseHistoryBinding::inflate,
                    ::bind
                )
            }

            /*if (history.isNotEmpty()) SparesPurchaseHistoryAdapter(history.reversed(), symbol).let {
                it.setOnItemClickListener(::onItemClick)
                rvPurchaseHistory.setNewAdapter(it)
                srl2.makeVisible(false)
                srl1.makeVisible(true)
            } else {
                srl1.makeVisible(false)
                srl2.makeVisible(true)
            }*/
        }

    private fun onItemClick(v: View) {
        when (v.id) {
            R.id.btnOrderDetailsCSPH, R.id.clCSPH -> Bundle().apply {
                putParcelable(AppENUM.SPARES_ORDER_ITEM, v.tag as SparesOrderHistory)
                putString(AppENUM.CURRENCY_SYMBOL, symbol)
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_ORDER_DETAIL, this)
                )
            }

            R.id.btnDownloadInvoiceCSPH -> {
                val invoices = v.tag as List<*>
                invoices.forEach {
                    val invoice = it as Invoice
                    downloadPdf(invoice)
                }
            }

            R.id.imgBack -> popBackStackAndHideKeyboard()

            R.id.btnEmptyAction -> {
                popBackStackAndHideKeyboard()
                CommonUtils.addFragmentUtil(
                    requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                        Bundle().apply {
                            putString(
                                AppENUM.IntentKeysENUM.TYPE,
                                AppENUM.RefactoredStrings.CATEGORY
                            )
                            putBoolean(AppENUM.IS_SEE_ALL, true)
                            putInt(
                                AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION,
                                arguments?.getInt(AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION, 4)
                                    ?: 4
                            )
                        })
                )
            }
        }
    }

    private fun downloadPdf(invoice: Invoice) {
        try {
            val base64 = invoice.invoiceDetails?.encodedInvoice
            val bytes = Base64.decode(base64, Base64.DEFAULT)
            CommonUtils.savePDFToDisk(
                requireActivity(),
                bytes,
                invoice.invoiceDetails?.displayCode + AppENUM.INVOICE_PDF,
                invoice.invoiceDetails?.displayCode + AppENUM.INVOICE_PDF,
                getString(R.string.download_invoive_pdf)
            )
        } catch (e: IOException) {
            e.printStackTrace()
            CommonUtils.showToast(
                requireContext(),
                resources.getString(R.string.invoice_not_generated)
            )
        }
    }


    private fun setupFilterSpinner() {

        val filters = getFilters()
        val inflater =
            requireContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_spinner, null)
        val recyclerView = view.findViewById<RecyclerView>(R.id.rvWorkDone)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        val adapter = CommonUtils.spinnerRecyclerAdapter(filters) {
            filterPopupWindow.dismiss()
            filterItems(filters[it])
        }
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged() //view.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        filterPopupWindow = PopupWindow(
            view,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            true
        ).apply {
            isOutsideTouchable = true
            isFocusable = true
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        binding.filterText.text = filters[0].splitCamelCase()
    }

    private fun filterItems(filter: String) {
        binding.filterText.text = filter.splitCamelCase()
        this.filter = if(filter == "All") null else filter
        isLastPageRV = false
        getPurchaseHistory()
    }

    private fun getFilters() = listOf(
        "All",
        STATUS_CANCELED,
        STATUS_DISPATCHED,
        STATUS_PENDING,
        STATUS_IN_TRANSIT,
        STATUS_OUT_FOR_DELIVERY,
        STATUS_DELIVERED
    )

    fun bind(binding: CardSparesPurchaseHistoryBinding, item: SparesOrderHistory, position: Int) {
        binding.tvItemCountCSPH.text = (item.items?.size ?: 0).toString().plus("  ")
            .plus(binding.root.resources.getString(R.string.items))
        binding.tvDateCSPH.text = CommonUtils.formatDateNormal(item.createdAt)
        binding.tvOrderIdCSPH.text = item.orderId
        binding.tvAmountCSPH.text = symbol + (item.total?.minus(item.discount?.roundToInt()?:0))

        when (item.status) {
            STATUS_DELIVERED -> {
                binding.tvSettledCSPH.text =
                    CommonUtils.getString(binding.root.context, R.string.delivered)
                binding.tvSettledCSPH.setViewBackgroundColor(R.color.status_complete)
            }
            STATUS_DISPATCHED -> {
                binding.tvSettledCSPH.text =
                    CommonUtils.getString(binding.root.context, R.string.dispatched)
                binding.tvSettledCSPH.setViewBackgroundColor(R.color.orange)
            }
            STATUS_PENDING -> {
                binding.tvSettledCSPH.text =
                    CommonUtils.getString(binding.root.context, R.string.pending)
                binding.tvSettledCSPH.setViewBackgroundColor(R.color.pending_red)
            }
            STATUS_IN_TRANSIT -> {
                binding.tvSettledCSPH.text =
                    CommonUtils.getString(binding.root.context, R.string.in_transit)
                binding.tvSettledCSPH.setViewBackgroundColor(R.color.in_transit)
            }
            STATUS_OUT_FOR_DELIVERY -> {
                binding.tvSettledCSPH.text =
                    CommonUtils.getString(binding.root.context, R.string.out_for_delivery)
                binding.tvSettledCSPH.setViewBackgroundColor(R.color.in_transit)
            }
            /*STATUS_CANCELED -> {
                binding.tvSettledCSPH.text =
                    CommonUtils.getString(binding.root.context, R.string.canceled)
                binding.tvSettledCSPH.setViewBackgroundColor(R.color.pending_red)
            }*/

            else -> {
                binding.tvSettledCSPH.text =
                    CommonUtils.getString(binding.root.context, R.string.canceled)
                binding.tvSettledCSPH.setViewBackgroundColor(R.color.pending_red)
            }
        }

        binding.root.setOnClickListener {
            it.tag = item
            onItemClick(it)
        }

        binding.btnOrderDetailsCSPH.setOnClickListener {
            it.tag = item
            onItemClick(it)
        }

        binding.btnDownloadInvoiceCSPH.setOnClickListener {
            if (item.invoices != null && item.invoices.isNotEmpty()) {
                it.tag = item.invoices
                onItemClick(it)
            } else {
                val i = Intent(Intent.ACTION_VIEW)
                i.data =
                    Uri.parse("${BuildConfig.BASE_URL}shop/marketplace-invoice?order_id=${item.clientOrderOid}")
                binding.root.context.startActivity(i)
            }
        }

    }
}