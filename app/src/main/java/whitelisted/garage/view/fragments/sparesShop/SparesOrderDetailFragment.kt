package whitelisted.garage.view.fragments.sparesShop

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.response.HistoryItem
import whitelisted.garage.api.response.HistoryObject
import whitelisted.garage.api.response.OrderItem
import whitelisted.garage.api.response.SparesOrderHistory
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentSparesOrderDetailsBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.SpareOrderTrackingAdapter
import whitelisted.garage.view.adapter.SparesOrderDetailsAdapter
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import java.io.IOException
import kotlin.math.roundToInt

class SparesOrderDetailFragment : BaseFragment() {

    private val binding by dataBinding<FragmentSparesOrderDetailsBinding>(R.layout.fragment_spares_order_details)
    private val order by lazy { arguments?.getParcelable<SparesOrderHistory?>(AppENUM.SPARES_ORDER_ITEM) }
    private val symbol by lazy { arguments?.getString(AppENUM.CURRENCY_SYMBOL) }
    private var sparesOrderDetailsAdapter: SparesOrderDetailsAdapter? = null
    private val viewModel: SparesShopFragmentViewModel by sharedViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()

    }

    private fun setUpViews() {
        binding.imgBack.setOnClickListener(this)
        order?.let {

            /*val completeHistory = it.dspTrackingDetails
                ?.asSequence()
                ?.map { dsp -> dsp.trackingData }
                ?.map { td -> td.dspHistory }
                ?.flatten()
                ?.mapNotNull { ht -> ht.historyObjects }
                ?.flatten()
                ?.toList()*/
            val completeHistory = it.historyObjects
            completeHistory?.forEach { h->
                if(h.status == SparesOrderHistoryFragment.STATUS_IN_TRANSIT)
                    h.status = SparesOrderHistoryFragment.STATUS_DISPATCHED
                if(h.updatedAt.isNullOrEmpty())
                    h.updatedAt = it.updatedAt
            }

            binding.tvOrderId.text =
                resources.getString(R.string.order_id).plus(": ").plus(it.orderId)

            /*it.items?.forEachIndexed { index, orderItem ->
                orderItem.gomPrice = it.gomPrices?.get(index)?.gomPrice
            }*/

            it.items?.let { list ->
                list.forEach { item ->
                    item.history = mutableListOf<HistoryObject>().apply {
                        if (!it.history.isNullOrEmpty())
                            add(
                                HistoryObject(
                                    it.history[0].status,
                                    updatedAt = it.history[0].timestamp
                                )
                            )
                        completeHistory?.filter { h -> h.skuCode == item.skuCode }?.let { hs ->
                            addAll(hs)
                        }
                    }
                    setItemStatus(item)
                }
                sparesOrderDetailsAdapter = SparesOrderDetailsAdapter(list, symbol.toString())
                if (!it.dspTrackingDetails.isNullOrEmpty()) {
                    sparesOrderDetailsAdapter?.trackingId =
                        it.dspTrackingDetails[0].ucData?.trackingNumber
                }
                binding.tvViewMoreItems.visibility = if (list.size > 2) View.VISIBLE else View.GONE
                binding.tvViewMoreItems.text =
                    resources.getString(R.string.see_more_items, list.size - 2)
            }
            binding.rvOrderItems.adapter = sparesOrderDetailsAdapter
            binding.rvOrderItems.layoutManager = LinearLayoutManager(requireContext())

            binding.tvViewMoreItems.setOnClickListener(this)

            binding.tvPersonName.text = it.clientCustomerDetails?.name
            val shippingAddress = StringBuilder()
            it.clientCustomerDetails?.shippingAddress.let { a ->
                shippingAddress.append(a?.address); shippingAddress.append("\n")
                shippingAddress.append(a?.city); shippingAddress.append("\n")
                shippingAddress.append(a?.state); shippingAddress.append("\n")
                shippingAddress.append(a?.pincode); shippingAddress.append("\n")
                shippingAddress.append(it.clientCustomerDetails?.mobile)
            }
            binding.shippingAddress.text = shippingAddress.toString()

            binding.tvItemTotal.text = symbol + it.total
            binding.tvDiscount.text = symbol + it.discount?.roundToInt()
            binding.tvTotalPrice.text = symbol + (it.total?.minus(it.discount?:0.0))?.roundToInt()
            binding.tvPaymentMode.text =
                if (it.rzpPaymentId == "offline") binding.root.context.getString(R.string.pay_on_delivery)
                else binding.root.context.getString(R.string.online)

            binding.tvViewMoreItems.setOnClickListener(this)
            binding.tvDownloadInvoice.setOnClickListener(this)

        }

    }

    private fun setItemStatus(item: OrderItem) {
        item.status = when {
            item.history?.any { it.status == SparesOrderHistoryFragment.STATUS_DELIVERED }
                ?: false -> SparesOrderHistoryFragment.STATUS_DELIVERED
            item.history?.any { it.status == SparesOrderHistoryFragment.STATUS_OUT_FOR_DELIVERY }
                ?: false -> SparesOrderHistoryFragment.STATUS_OUT_FOR_DELIVERY
            item.history?.any { it.status == SparesOrderHistoryFragment.STATUS_DISPATCHED }
                ?: false -> SparesOrderHistoryFragment.STATUS_DISPATCHED
            item.history?.any { it.status == SparesOrderHistoryFragment.STATUS_IN_TRANSIT }
                ?: false -> SparesOrderHistoryFragment.STATUS_IN_TRANSIT
            item.history?.any { it.status == SparesOrderHistoryFragment.STATUS_PENDING }
                ?: false -> SparesOrderHistoryFragment.STATUS_PENDING
            /*item.history?.any { it.status == SparesOrderHistoryFragment.STATUS_CANCELED }
                ?: false -> SparesOrderHistoryFragment.STATUS_CANCELED*/
            else -> SparesOrderHistoryFragment.STATUS_CANCELED
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.tvViewMoreItems -> {
                sparesOrderDetailsAdapter?.isExpanded = true
                v.makeVisible(false)
            }
            R.id.tvDownloadInvoice -> downloadPdf()
        }
    }

    private fun onTrackingIdClicked(id: String) {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            showLoader()
            viewModel.getTrackingUrl()?.let {
                val trackingUrl = "$it$id"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(trackingUrl)
                startActivity(i)
            }
            hideLoader()
        }
    }

    private fun downloadPdf() {
        try {
            if (!order?.invoices.isNullOrEmpty())
                order?.invoices?.forEach { invoice ->
                    val base64 = invoice.invoiceDetails?.encodedInvoice
                    val bytes = Base64.decode(base64, Base64.DEFAULT)
                    CommonUtils.savePDFToDisk(
                        requireActivity(), bytes,
                        invoice.invoiceDetails?.displayCode + AppENUM.INVOICE_PDF,
                        invoice.invoiceDetails?.displayCode + AppENUM.INVOICE_PDF,
                        getString(R.string.download_invoive_pdf)
                    )
                }
            else {
                val i = Intent(Intent.ACTION_VIEW)
                i.data =
                    Uri.parse("${BuildConfig.BASE_URL}shop/marketplace-invoice?order_id=${order?.clientOrderOid}")
                startActivity(i)
            }
        } catch (e: IOException) {
            e.printStackTrace()
            CommonUtils.showToast(
                requireContext(),
                resources.getString(R.string.invoice_not_generated)
            )
        }
    }

}