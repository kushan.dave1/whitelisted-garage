package whitelisted.garage.view.fragments.inventory

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.view.View
import android.webkit.MimeTypeMap
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentInventoryBulkUploadBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.viewmodels.InventoryTabFragmentViewModel
import java.io.File

class InventoryBulkUploadFragment : BaseFragment(), View.OnClickListener {

    private val binding by viewBinding(FragmentInventoryBulkUploadBinding::inflate)
    private val viewModel: InventoryTabFragmentViewModel by sharedViewModel()
    private lateinit var pickupResult: ActivityResultLauncher<Intent>
    private lateinit var tempFile: File
    var uri: Uri? = null
    var fileSize = "0kb"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addListeners()
        fireInitInventoryEvent()
    }

    private fun addListeners() {
        binding.btnDownloadSample.setOnClickListener(this)
        binding.btnUploadInventory.setOnClickListener(this)
        binding.btnReplaceFIBU.setOnClickListener(this)
        binding.btnCancelFIBU.setOnClickListener(this)
        binding.btnConfirmFIBU.setOnClickListener(this)
        binding.btnViewInventoryFIBU.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)

    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnUploadInventory -> openFilePicker()
            R.id.btnDownloadSample -> downloadSampleCSV()
            R.id.btnReplaceFIBU -> openFilePicker()
            R.id.btnCancelFIBU -> removeSelectedFile()
            R.id.btnConfirmFIBU -> uploadCSVFile()
            R.id.btnViewInventoryFIBU -> popBackStackAndHideKeyboard()
            R.id.imgBack -> popBackStackAndHideKeyboard()
        }
    }


    private fun removeSelectedFile() {
        binding.btnDownloadSample.makeVisible(true)
        binding.btnUploadInventory.makeVisible(true)
        binding.view2.makeVisible(true)
        binding.btnViewInventoryFIBU.makeVisible(false)
        binding.clUploadingView.makeVisible(false)
        uri = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        pickupResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == Activity.RESULT_OK) result.data?.data?.let {
                    showFileSelected(it)
                }
                else CommonUtils.showToast(requireContext(),
                    getString(R.string.file_pickup_failed),
                    true)
            }
    }

    private fun showFileSelected(uri: Uri) {
        binding.btnDownloadSample.makeVisible(false)
        binding.btnUploadInventory.makeVisible(false)
        binding.tvFileUploadedFIBU.makeVisible(false)
        binding.ivDoneFIBU.makeVisible(false)
        binding.view2.makeVisible(false)
        binding.btnConfirmFIBU.makeVisible(true)
        binding.btnReplaceFIBU.makeVisible(true)
        binding.clUploadingView.makeVisible(true)

        requireActivity().contentResolver.query(uri, null, null, null, null)?.use {
            it.use { cursor ->
                val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                val sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE)
                cursor.moveToFirst()
                binding.tvFileNameFIBU.text = cursor.getString(nameIndex)
                fileSize = (cursor.getLong(sizeIndex) / 1000).toString() + "kb"
                binding.tvFileSizeFIBU.text = fileSize
                this.uri = uri
            }
        }
    }

    private fun openFilePicker() {
        val filePickupIntent = Intent(Intent.ACTION_OPEN_DOCUMENT).addCategory(Intent.CATEGORY_OPENABLE)
                .setType(AppENUM.EXCEL_MIME)
        pickupResult.launch(Intent.createChooser(filePickupIntent, "Choose file"))
    }

    private fun downloadSampleCSV() = viewLifecycleOwner.lifecycleScope.launch {
        fireSampleInventoryEvent()
        showLoader()
        CommonUtils.showToast(requireContext(), getString(R.string.downloading), true)
        val response = viewModel.downloadSampleCSV()
        hideLoader()
        if (response != null) CommonUtils.saveToDisk(requireActivity(),
            response,
            "${viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME)}${AppENUM.INVENTORY_SAMPLE_CSV}",
            "${viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME)}${AppENUM.INVENTORY_SAMPLE_CSV}",
            getString(R.string.download_inventory_xlsx),
            AppENUM.EXCEL_MIME)
        else CommonUtils.showToast(requireContext(), getString(R.string.downloading_failed), true)
    }

    private fun uploadCSVFile() = viewLifecycleOwner.lifecycleScope.launch {
        showFileUploading()
        uri?.let {  uploadFile(it) }

    }

    private fun showFileUploading() {
        binding.tvFileSizeFIBU.text = "Uploading..."
        binding.btnReplaceFIBU.makeVisible(false)
        binding.btnConfirmFIBU.makeVisible(false)
        binding.btnCancelFIBU.makeVisible(false)
        binding.vPurpleBg.makeVisible(false)
    }

    private fun showFileUploadingFailed() = lifecycleScope.launch(Dispatchers.Main) {
        binding.tvFileSizeFIBU.text = fileSize
        binding.btnReplaceFIBU.makeVisible(true)
        binding.btnConfirmFIBU.makeVisible(true)
        binding.btnCancelFIBU.makeVisible(true)
        binding.vPurpleBg.makeVisible(true)
        CommonUtils.showToast(requireContext(), getString(R.string.uploading_failed), true)
        fireUploadInventoryEvent(false)
    }

    private fun showFileUploaded() = lifecycleScope.launch(Dispatchers.Main) {
        binding.btnReplaceFIBU.makeVisible(false)
        binding.btnConfirmFIBU.makeVisible(false)
        binding.btnCancelFIBU.makeVisible(false)
        binding.ivDoneFIBU.makeVisible(true)
        binding.tvFileUploadedFIBU.makeVisible(true)
        binding.btnViewInventoryFIBU.makeVisible(true)
        binding.vPurpleBg.makeVisible(true)
        binding.tvFileSizeFIBU.text = fileSize
        fireUploadInventoryEvent(true)
        tempFile.delete()
    }

    private fun uploadFile(uri: Uri) {
        lifecycleScope.launch(Dispatchers.IO) {

            val fis = (requireActivity().contentResolver.openInputStream(uri)) ?: return@launch
            val path = requireContext().filesDir
            val file = File("$path/temp")
            if (!file.exists()) file.mkdir()
            tempFile = File(file, "inventory.xlsx")
            fis.copyTo(tempFile.outputStream())
            fis.close()

            val fileReqBody = tempFile.asRequestBody(MimeTypeMap.getSingleton().getMimeTypeFromExtension(tempFile.extension)?.toMediaTypeOrNull())
            val part = MultipartBody.Part.createFormData("file", tempFile.name, fileReqBody)
            val response = viewModel.uploadCSVFile(part)
            if (response) showFileUploaded()
            else showFileUploadingFailed()
        }

    }

    private fun fireInitInventoryEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN, FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_INVENTORY_BULK_UPLOAD)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_BULK_INVENTORY, bundle)
    }

    private fun fireSampleInventoryEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN, FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_INVENTORY_BULK_UPLOAD)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_SAMPLE_DOWNLOAD, bundle)
    }

    private fun fireUploadInventoryEvent(uploadStatus: Boolean) {
        val bundle = Bundle()
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.UPLOAD_STATUS, uploadStatus)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN, FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_INVENTORY_BULK_UPLOAD)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.UPLOAD_INVENTORY_FILE, bundle)
    }


}