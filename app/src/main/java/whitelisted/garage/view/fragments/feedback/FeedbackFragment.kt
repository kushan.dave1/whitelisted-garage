package whitelisted.garage.view.fragments.feedback

import android.os.Bundle
import android.view.View
import whitelisted.garage.R
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentFeedbackBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils

class FeedbackFragment : BaseFragment() {

    private val binding by viewBinding(FragmentFeedbackBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupScreen()
    }

    private fun setupScreen() {
        binding.backArrowImg.setOnClickListener(this)
        binding.btnSaveFeedBack.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.backArrowImg -> popBackStackAndHideKeyboard()

            R.id.btnSaveFeedBack -> {
                CommonUtils.sendEMail(requireContext(),
                    AppENUM.HELP_SUPPORT_MAIL,
                    binding.etFeedbackText.text.toString())
            }
        }
    }

}