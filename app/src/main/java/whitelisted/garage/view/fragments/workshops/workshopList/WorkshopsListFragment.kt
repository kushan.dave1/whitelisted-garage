package whitelisted.garage.view.fragments.workshops.workshopList

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentWorkshopListBinding
import whitelisted.garage.eventBus.UpdateHomeScreenEvent
import whitelisted.garage.eventBus.WorkshopUpdateEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.WorkshopsAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.WorkshopListViewModel

class WorkshopsListFragment : BaseFragment() {

    private val binding by viewBinding(FragmentWorkshopListBinding::inflate)
    private val viewModel: WorkshopListViewModel by viewModel()
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private lateinit var workshopsList: MutableList<WorkshopListResponseModel>
    private lateinit var workshopsAdapter: WorkshopsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListeners()
        showLoader()
        viewModel.getWorkshopListAPI()
        setWorkshopListResponseObserver()
        setDeleteWorkshopResponseObserver()
    }

    private fun setDeleteWorkshopResponseObserver() {
        viewModel.provideDeleteWorkshopResponse().observe(viewLifecycleOwner) {
            hideLoader()
            it.let {
                when (it) {
                    is Result.Success -> {
                        showLoader()
                        viewModel.isWorkShopDeleted.value = true
                        viewModel.getWorkshopListAPI()
                        EventBus.getDefault().post(UpdateHomeScreenEvent())
                    }
                    is Result.Failure -> CommonUtils.showToast(activity, it.errorMessage, true)
                }
            }
        }
    }

    private fun setWorkshopListResponseObserver() {
        viewModel.provideWorkshopListResponseFromAPI().observe(viewLifecycleOwner) {
            hideLoader()
            it.let {
                when (it) {
                    is Result.Success -> {
                        if (it.body.data.isNotEmpty()) {
                            workshopsList = it.body.data.toMutableList()
                            workshopsList.forEach { ws -> ws.isLocked = false }
                            viewModel.addWorkshopsToRoom(workshopsList)
                            binding.tvTotalWorkShop.text = workshopsList.size.toString()
                            setWorkshopsAdapter()
                        } else {
                            binding.clEmptyWorkshops.makeVisible(true)
                            binding.clWorkshopsList.makeVisible(false)
                        }
                    }
                    is Result.Failure -> CommonUtils.showToast(activity, it.errorMessage, true)
                }
            }
        }
    }

    private fun setWorkshopsAdapter() {
        binding.rvWorkshopList.layoutManager = LinearLayoutManager(requireContext())
        workshopsAdapter =
            WorkshopsAdapter(dashboardViewModel.screensConfigResponse.value?.membershipConfig?.workshopLimitMsg
                ?: "",
                viewModel.getStringSharedPreference(AppENUM.USER_ID, ""),
                workshopsList,
                this)
        binding.rvWorkshopList.adapter = workshopsAdapter
    }

    private fun clickListeners() {
        binding.imgBack.setOnClickListener(this)
        binding.btnAddWorkshop.setOnClickListener(this)
        binding.btnAddNewWorkshop.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()

            R.id.btnAddWorkshop -> {
                if (::workshopsList.isInitialized) {
                    if (workshopsList.any { ws -> ws.isLocked == true }) {
                        requireActivity().let {
                            fireFeatureLockedEvent()
                            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT,
                                Bundle().apply {
                                    putString(AppENUM.PRO_LOCKED_ALERT_MSG,
                                        dashboardViewModel.screensConfigResponse.value?.membershipConfig?.workshopLimitMsg)
                                })?.let { baseFragment ->
                                baseFragment.show(it.supportFragmentManager,
                                    baseFragment.javaClass.name)
                            }
                        }
                    } else {
                        Bundle().apply {
                            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_WORKSHOP_LIST)
                            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_TAP_ADD_WORKSHOP,
                                this)

                        }
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.ADD_WORKSHOP, null),
                            target = R.id.fragment_container_2)
                    }
                }
            }
            R.id.cvBase -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.VIEW_EDIT_WORKSHOP,
                            Bundle().apply {
                                this.putBoolean(AppENUM.IntentKeysENUM.IS_EDIT, false)
                                this.putSerializable(AppENUM.IntentKeysENUM.WORKSHOP_MODEL,
                                    workshopsList[position])

                            }),
                        target = R.id.fragment_container_2)

                }
            }
            R.id.imgOptions -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    showPopupMenu(v, position)
                }
            }
            R.id.imageContextCopy -> {
                CommonUtils.genericCastOrNull<String>(v.getTag(R.id.model))?.let { link ->
                    if (link.isNotEmpty()) {
                        val clipboardManager =
                            requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                        val clipData = ClipData.newPlainText("text", link)
                        clipboardManager.setPrimaryClip(clipData)
                        CommonUtils.showToast(requireContext(), getString(R.string.text_coppied))
                    } else {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.id_not_available))

                    }
                }
            }
        }
    }

    private fun showPopupMenu(view: View, position: Int) {
        val popupMenu = PopupMenu(requireContext(), view)
        popupMenu.menuInflater.inflate(R.menu.edit_del_menu, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.edit -> Bundle().apply {
                    this.putBoolean(AppENUM.IntentKeysENUM.IS_EDIT, true)
                    this.putSerializable(AppENUM.IntentKeysENUM.WORKSHOP_MODEL,
                        workshopsList[position])
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.VIEW_EDIT_WORKSHOP,
                            this),
                        target = R.id.fragment_container_2)
                }

                R.id.delete -> {
                    if (workshopsList.size > 1) {
                        deleteWorkShop(position)
                    } else {
                        CommonUtils.showToast(requireContext(), getString(R.string.workshop_delete))
                    }
                }
            }
            true
        }
        popupMenu.show()
    }

    private fun deleteWorkShop(position: Int) {
        lateinit var dialog: AlertDialog
        activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle(getString(R.string.delete))
            builder.setMessage(getString(R.string.are_you_sure_you_want_to_delete_this_workshop))
            val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        showLoader()
                        viewModel.deletedWorkshopId.value = workshopsList[position].workshopId ?: ""
                        viewModel.deleteWorkshop(workshopsList[position].workshopId ?: "")
                    }
                }
            }

            builder.setPositiveButton(getString(R.string.delete), dialogClickListener)
            builder.setNeutralButton(getString(R.string.cancel), dialogClickListener)
            dialog = builder.create()
            dialog.show()
        }

    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is WorkshopUpdateEvent -> {
                showLoader()
                viewModel.getWorkshopListAPI()
            }
        }
    }

    private fun fireFeatureLockedEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_WORKSHOP_LIST)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.ERROR_HEADER_PRO,
            bundle)

    }


}