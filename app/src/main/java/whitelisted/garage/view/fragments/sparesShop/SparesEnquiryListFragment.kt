package whitelisted.garage.view.fragments.sparesShop

import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.Enquiry
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.CardEnquiryBinding
import whitelisted.garage.databinding.FragmentSparesEnquiryBinding
import whitelisted.garage.databinding.ItemRackItemImageBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.convertDateWithDaySlashes
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.utils.ImageLoader
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel

class SparesEnquiryListFragment : BaseFragment() {

    private val binding by viewBinding(FragmentSparesEnquiryBinding::inflate)
    private val viewModel: SparesShopFragmentViewModel by sharedViewModel()
    private var expandedPos = -1
    private var symbol = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
        getEnquiryList()
    }

    private fun setUpViews() {
        symbol = viewModel.getStringSharedPreference(
            AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL, AppENUM.RefactoredStrings.defaultCurrencySymbol
        )
        binding.rvEnquiries.setLayoutManagerAsLinear()
        binding.imgBack.setOnClickListener { popBackStackAndHideKeyboard() }
    }

    private fun getEnquiryList() = lifecycleScope.launch {
        showLoader()
        val enquiries = viewModel.getAllEnquiries()
        binding.tvTotalEnquiries.text = enquiries.size.toString()
        if (enquiries.isNotEmpty()) {
            binding.rvEnquiries.addViews(enquiries, CardEnquiryBinding::inflate, ::bindEnquiry)
        }else{
            binding.tvNoResults.makeVisible(true)
        }
        hideLoader()
    }

    private fun bindEnquiry(binding: CardEnquiryBinding, model: Enquiry, position: Int) =
        with(binding) {

            val isProductEnquiry = model.price != 0.0
            ivPartImage.makeVisible(isProductEnquiry)
            tvPartName.makeVisible(isProductEnquiry)

            ImageLoader.loadImage(binding.ivPartImage,
                model.productImage,
                placeholderImage = R.drawable.ic_placeholder_square)
            //ivPartImage.load(model.productImage)

            tvPartBrand.text = if (isProductEnquiry) model.productBrand else model.productName
            tvPartName.text = model.productName
            lblRemarks.makeVisible(model.remarks.isNotEmpty())
            tvRemarks.text = model.remarks
            tvPrice.text =
                if (isProductEnquiry) symbol + model.price.toString()
                else resources.getString(R.string.enquired)

            tvQty.text = model.quantity.toString()

            tvExpiredOn.text = convertDateWithDaySlashes(model.createdAt)

            btnSeeMore.setOnClickListener {
                expandedPos = if (expandedPos == position) -1 else position
                this@SparesEnquiryListFragment.binding.rvEnquiries.adapter?.notifyItemChanged(
                    position
                )
            }

            lblPhotos.makeVisible(model.photos.isNotEmpty())
            rvImages.setLayoutManagerAsLinearHorizontal()

            rvImages.clear().addViews(model.photos, ItemRackItemImageBinding::inflate) {
                binding: ItemRackItemImageBinding, model: String, _ ->
                binding.llDelImage.makeVisible(false)
                ImageLoader.loadImage(binding.imgBusImage, model,
                    placeholderImage = R.drawable.ic_placeholder_square)
                //binding.imgBusImage.load(model)
                binding.root.setBorder(2f, R.color.bg_selected, 20f)
                val params = RelativeLayout.LayoutParams(160,160)
                params.marginStart = 20
                binding.root.layoutParams = params
                binding.root.setPadding(2, 2, 2, 2)
                binding.cvImage.useCompatPadding = false
            }

            if(!model.vehicleImage.isNullOrEmpty()) {

                binding.clVehicleDetails.makeVisible(true)

                ImageLoader.loadImage(binding.imgCarImage, model.vehicleImage,
                    placeholderImage = R.drawable.ic_placeholder_square)

                binding.tvCarName.text = model.vehicle
                binding.tvFuelType.text = model.fuelType

            }


            else binding.clVehicleDetails.makeVisible(false)

            if (position == expandedPos) {
                clExpandedLayout.makeVisible(true)
                btnSeeMore.setText(root.context.getString(R.string.view_less))
                btnSeeMore.setDrawableRotation(180f)

            } else {
                clExpandedLayout.makeVisible(false)
                btnSeeMore.setText(root.context.getString(R.string.view_more))
                btnSeeMore.setDrawableRotation(0f)
            }

            if(model.productID.isNullOrEmpty()) clTop.setOnClickListener(null)
            else clTop.setOnClickListener {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(
                        FragmentFactory.Fragments.SPARES_PRODUCT_DETAILS,
                        Bundle().apply {
                            putString(AppENUM.SS_PRODUCT_ID, model.productID)
                        }))
            }
        }


}