package whitelisted.garage.view.fragments.home

import android.annotation.SuppressLint
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.LocationListener
import com.google.gson.Gson
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.data.SelectTabParams
import whitelisted.garage.api.data.SelectWorkshopDialogParams
import whitelisted.garage.api.request.HomeSummeryData
import whitelisted.garage.api.response.*
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentHomeBinding
import whitelisted.garage.eventBus.CompleteRegLocUpdateEventHome
import whitelisted.garage.eventBus.GmbRequestedEvent
import whitelisted.garage.eventBus.GoCoinBalanceReceivedEvent
import whitelisted.garage.eventBus.UpdateHomeScreenEvent
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.changeViewCompoundDrawablesWithInterinsicBounds
import whitelisted.garage.utils.CommonUtils.isVisible
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.*
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.HomeFragmentViewModel
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import whitelisted.garage.viewmodels.WalkthroughSharedViewModel

class HomeFragment : BaseFragment(), LocationListener {

    private val binding by viewBinding2(FragmentHomeBinding::inflate)
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private val viewModel: HomeFragmentViewModel by viewModel()
    private val ssViewModel: SparesShopFragmentViewModel by sharedViewModel()
    private val walkThroughViewModel: WalkthroughSharedViewModel by sharedViewModel()
    private lateinit var ongoingOrdersList: MutableList<DataItem>
    private lateinit var ongoingOrdersAdapter: OngoingOrdersAdapter
    private lateinit var carouselList: MutableList<CarouselDataItem>
    private lateinit var carouselAdapter: CarouselAdapter
    private var homeScreenResponse: HomeScreenResponse? = null
    private lateinit var nearByBidingWorkshopResponse: Map<String, GetNearByBidingWorkshop>
    private lateinit var retryPaymentResponse: RetryPaymentResponse
    private var isComboPurchased = false
    private lateinit var summaryAdapter: HomeSummeryAdapter
    private lateinit var arrayListMonth: MutableList<String>
    private var isHomeFetched = false
    private var monthPos = -1
    private var ordersCount = -1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListeners()
        setObservers()

        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID, "")
                .isNotEmpty()) {
            setUpData() //        showLoader()
            binding?.sflNewOrderImage?.startShimmer()
            binding?.sflSparesBanner?.startShimmer()
            binding?.sflGMBBanner?.startShimmer()
            viewModel.getOnlyHomeScreenAPI(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                ""))
            if (!isInternationalUser) getSSMasterFilters()
        }

        setOrderSummaryAdapter()
        workshopWiseUIChanges()
        internationalUserUIChanges()

        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.HOME_SCREEN_RESPONSE, "").let {
            if (it.isNotEmpty()) {
                homeScreenResponse = Gson().fromJson(it, HomeScreenResponse::class.java)
                homeScreenResponse?.let { homeResponse ->
                    binding?.nsvHome.makeVisible(true)
                    setData(homeResponse)
                    setWsBiddingSummaryData(homeResponse.workshop?.biddingSummary)
                    setOrderCount()
                }
            }
        }

        binding?.srl?.setOnRefreshListener {
            binding?.srl?.isRefreshing = false
            showLoader()
            if (monthPos != -1) viewModel.getDataForMonth(viewModel.getStringSharedPreference(
                AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                ""), monthPos.toString())
            else viewModel.getOnlyHomeScreenAPI(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                ""))
        }

        setSelectMonthAdapter()

//        binding?.rlHeader?.setOnClickListener {
//            FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.MEMBERSHIP_PURCHASED_DIALOG)?.let { dialog ->
//                dialog.show(requireActivity().supportFragmentManager, dialog.javaClass.name)
//            }
//        }
    }

    private fun getSSMasterFilters() = viewLifecycleOwner.lifecycleScope.launch {
        val attrs = ssViewModel.getSearchAndFilterAttributes()
        ssViewModel.setSearchAndFilterAttributes(attrs)
        setSSCategories(attrs?.categories)
    }

    private fun setSSCategories(categories: List<Category>?) {
        if (categories?.isNotEmpty() == true) {
            binding?.clSparesAccCategories.makeVisible(true)
            binding?.rvSparesAccCategories?.adapter =
                HomeSSCategoriesAdapter(categories, false, this)
        } else {
            binding?.clSparesAccCategories.makeVisible(false)
        }
    }

    private fun internationalUserUIChanges() {
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                AppENUM.RefactoredStrings.defaultCountryCode) == AppENUM.RefactoredStrings.defaultCountryCode) { //india user UI
            if (dashboardViewModel.screensConfigResponse.value?.biddingEnabled == true) binding?.llBiding.makeVisible(
                true)
            binding?.clSparesShop.makeVisible(true)
        } else { //international User UI
            binding?.llBiding.makeVisible(false)
            binding?.clSparesShop.makeVisible(false)
        }
    }

    private fun workshopWiseUIChanges() {
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                binding?.tvSearchOrders?.text = getString(R.string.search_for_phone_name)
                binding?.llWsTotalBids.makeVisible(false)
                binding?.llWsPendingBids.makeVisible(false)
                binding?.llWsApprovedBids.makeVisible(false)
                binding?.llAccRetNewOrders.makeVisible(true)
                binding?.llAccRetAcceptedOrders.makeVisible(true)
            }
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                binding?.llWsTotalBids.makeVisible(false)
                binding?.llWsPendingBids.makeVisible(false)
                binding?.llWsApprovedBids.makeVisible(false)
                binding?.llAccRetNewOrders.makeVisible(true)
                binding?.llAccRetAcceptedOrders.makeVisible(true)
            }
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                binding?.llWsTotalBids.makeVisible(true)
                binding?.llWsPendingBids.makeVisible(true)
                binding?.llWsApprovedBids.makeVisible(true)
                binding?.llAccRetNewOrders.makeVisible(false)
                binding?.llAccRetAcceptedOrders.makeVisible(false)
            }
        }
    }

    private fun setSelectMonthAdapter() {
        val currentMonth = CommonUtils.getCurrentMonth()
        arrayListMonth = mutableListOf<String>().apply {
            addAll(resources.getStringArray(R.array.months_list))
        }
        binding?.tvMonth?.text = arrayListMonth[currentMonth - 1]

        binding?.rvMonth?.layoutManager = LinearLayoutManager(requireContext())
        SelectMonthAdapter(this).apply {
            binding?.rvMonth?.adapter = this
            setUpData(arrayListMonth)
        }
    }

    private fun setWorkshopBannerImage(newOrderBanner: NewOrderBanner?) = binding?.apply {
        clGroupNewOrder.makeVisible(false)
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                ImageLoader.loadImageWithCallback(imgNewOrder,
                    newOrderBanner?.workshop,
                    object : ImageLoader.ImageLoadCallback {
                        override fun onSuccess() {
                            sflNewOrderImage.makeVisible(false)
                        }

                        override fun onError(throwable: Throwable) {
                            sflNewOrderImage.makeVisible(false)
                        }
                    },
                    placeholderImage = R.drawable.ic_placeholder_create_order,
                    defaultImage = R.drawable.ic_placeholder_create_order)
            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                ImageLoader.loadImageWithCallback(imgNewOrder,
                    newOrderBanner?.spares,
                    object : ImageLoader.ImageLoadCallback {
                        override fun onSuccess() {
                            sflNewOrderImage.makeVisible(false)
                        }

                        override fun onError(throwable: Throwable) {
                            sflNewOrderImage.makeVisible(false)
                        }
                    },
                    placeholderImage = R.drawable.ic_placeholder_create_order,
                    defaultImage = R.drawable.ic_placeholder_create_order)
            }
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                ImageLoader.loadImageWithCallback(imgNewOrder,
                    newOrderBanner?.acc,
                    object : ImageLoader.ImageLoadCallback {
                        override fun onSuccess() {
                            sflNewOrderImage.makeVisible(false)
                        }

                        override fun onError(throwable: Throwable) {
                            sflNewOrderImage.makeVisible(false)
                        }
                    },
                    placeholderImage = R.drawable.ic_placeholder_create_order,
                    defaultImage = R.drawable.ic_placeholder_create_order)
            }
        }
    }

    private fun setUpData() = binding?.apply {
        val newOrderBanner = dashboardViewModel.screensConfigResponse.value?.newOrderBanner
        setWorkshopBannerImage(newOrderBanner)
        ImageLoader.loadImageWithCallback(imgSparesBanner,
            dashboardViewModel.screensConfigResponse.value?.sparesHomeBanner,
            object : ImageLoader.ImageLoadCallback {
                override fun onSuccess() {
                    sflSparesBanner.makeVisible(false)
                }

                override fun onError(throwable: Throwable) {
                    sflSparesBanner.makeVisible(false)
                }
            },
            placeholderImage = R.drawable.ic_placeholder_create_order,
            defaultImage = R.drawable.ic_placeholder_create_order)
//        ImageLoader.loadImageWithCallback(imgGMBBanner,
//            dashboardViewModel.screensConfigResponse.value?.screen_images?.homeProMembership,
//            object : ImageLoader.ImageLoadCallback {
//                override fun onSuccess() {
////                    tvExtraText.makeVisible(true)
//                    sflGMBBanner.makeVisible(false)
//                }
//
//                override fun onError(throwable: Throwable) {
//                    sflGMBBanner.makeVisible(false)
//                }
//            },
//            placeholderImage = R.drawable.ic_placeholder_home_gmb,
//            defaultImage = R.drawable.ic_placeholder_home_gmb)
        ImageLoader.loadImage(imgBidding,
            dashboardViewModel.screensConfigResponse.value?.biddingFlowImages?.biddingWorkshop,
            placeholderImage = R.drawable.ic_placeholder_create_order,
            defaultImage = R.drawable.ic_placeholder_create_order)

        ("${getString(R.string.only)} ${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL, getString(R.string.currency_symbol_default))
        }${
            (dashboardViewModel.screensConfigResponse.value?.membership?.membershipPrice ?: "699")
        }").apply {
            binding?.tvExtraText?.text =this
        }
    }

    private fun setOrderSummaryAdapter() = binding?.apply {
        summaryAdapter = HomeSummeryAdapter(requireContext())
        rvSummery.adapter = summaryAdapter
        PagerSnapHelper().apply {
            attachToRecyclerView(rvSummery)
        }

        rvSummery.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dx > 0) { // Scrolling up
                    imgOrdersIndicator.setImageDrawable(ContextCompat.getDrawable(requireContext(),
                        R.drawable.ic_home_indicator_inactive))
                    imgPaymentsIndicator.setImageDrawable(ContextCompat.getDrawable(requireContext(),
                        R.drawable.ic_home_indicator_active))
                } else { // Scrolling down
                    imgOrdersIndicator.setImageDrawable(ContextCompat.getDrawable(requireContext(),
                        R.drawable.ic_home_indicator_active))
                    imgPaymentsIndicator.setImageDrawable(ContextCompat.getDrawable(requireContext(),
                        R.drawable.ic_home_indicator_inactive))
                }
            }
        })
        imgOrdersIndicator.setImageDrawable(ContextCompat.getDrawable(requireContext(),
            R.drawable.ic_home_indicator_active))
        imgPaymentsIndicator.setImageDrawable(ContextCompat.getDrawable(requireContext(),
            R.drawable.ic_home_indicator_inactive))

        mutableListOf<HomeSummeryData>().apply {
            add(HomeSummeryData("0", "0", "0", "0", isOrderSummery = true))
            add(HomeSummeryData("0", "0", "0", "0", isOrderSummery = false))
            summaryAdapter.setData(this)
        }
    }

    private fun clickListeners() {
        binding?.let { binding ->
            binding.imgMenu.setOnClickListener(this)
            binding.tvWorkshopNameFH.setOnClickListener(this)
            binding.tvShopType.setOnClickListener(this)
            binding.btnNewOrder.setOnClickListener(this)
            binding.llReminders.setOnClickListener(this)
            binding.llGoCoins.setOnClickListener(this)
            binding.btnAddEmployee.setOnClickListener(this)
            binding.tvViewAll.setOnClickListener(this)
            binding.llOpenOrders.setOnClickListener(this)
            binding.llWIPOrders.setOnClickListener(this)
            binding.llReadyOrders.setOnClickListener(this)
            binding.llCompletedOrders.setOnClickListener(this)
            binding.rlEmployeeSummary.setOnClickListener(this)
            binding.tvSearchOrders.setOnClickListener(this)
            binding.imgNewOrder.setOnClickListener(this)
            binding.imgGMBBanner.setOnClickListener(this)
            binding.llViewGOCOIN.setOnClickListener(this)
            binding.llRequestGMBParent.setOnClickListener(this)
            binding.imgCancelRetryPayment.setOnClickListener(this)
            binding.tvRetryPayment.setOnClickListener(this)
            binding.tvMonth.setOnClickListener(this)
            binding.imgBidding.setOnClickListener(this)
            binding.llWsPendingBids.setOnClickListener(this)
            binding.llWsApprovedBids.setOnClickListener(this)
            binding.llWsTotalBids.setOnClickListener(this)
            binding.llAccRetNewOrders.setOnClickListener(this)
            binding.llAccRetAcceptedOrders.setOnClickListener(this)
            binding.imgSparesBanner.setOnClickListener(this)
            binding.rlHeader.setOnClickListener(this)
            binding.tvSeeAllCatsFSS.setOnClickListener(this)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setObservers() {
        homeScreenResponseObserver()
        gmdObserver()
        carouselResponseObserver()
        retryPaymentObserver()
        cancelRetryPaymentObserver()
        summaryDataObserver() //        observeNearByWorkshop()
        observeWalkThroughEvent()
        observeMembershipPurchaseEvent()
    }

    private fun observeMembershipPurchaseEvent() {
        dashboardViewModel.provideMembershipPurchasedEvent().observe(viewLifecycleOwner){
            if (it){
                binding?.clProActive.makeVisible(true)
                dashboardViewModel.provideIsWorkshopRefresh().postValue(true)
                viewModel.getHomeScreenAPI(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                    ""))
            }
        }
    }

    private fun observeWalkThroughEvent() {
        walkThroughViewModel.provideHomeWalkThroughEvent().observe(viewLifecycleOwner) {
            binding?.let { binding ->
                when (it) {
                    R.id.tvWorkshopNameFH -> startWalkthrough(binding.imgNewOrder)

                    R.id.imgNewOrder -> {
                        binding.rlOrders.let { rlOrdersView ->
                            if (binding.rvCarousel.visibility == View.VISIBLE) {
                                val scrollTo =
                                    (binding.viewBottomRlOrders.parent.parent as View).top + binding.viewBottomRlOrders.top + binding.viewBottomRlOrders.top
                                binding.nsvHome.smoothScrollTo(0, scrollTo)
                                lifecycleScope.launchWhenResumed {
                                    delay(300)
                                    startWalkthrough(rlOrdersView)
                                }
                            } else startWalkthrough(rlOrdersView)
                        }
                    }

                    R.id.rlOrders -> {
                        val scrollTo =
                            (binding.imgSparesBanner.parent.parent as View).top + binding.imgSparesBanner.top
                        binding.nsvHome.smoothScrollTo(0, scrollTo)
                        lifecycleScope.launchWhenResumed {
                            delay(300)
                            startWalkthrough(binding.imgSparesBanner)
                        }
                    }

                    R.id.imgSparesBanner -> {
                        binding.nsvHome.smoothScrollTo(0, 0)
                    }
                }
            }
        }
    }

    private fun summaryDataObserver() {
        viewModel.provideHomeScreenResponseForMonth().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setSummaryAdapterData(it.body.data)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun cancelRetryPaymentObserver() {
        viewModel.provideCancelRetryPaymentResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    binding?.rlRetryPayment.makeVisible(false)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun retryPaymentObserver() {
        viewModel.provideRetryPaymentResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    retryPaymentResponse = it.body.data
                    it.body.data.detail?.let { detail ->
                        binding?.rlRetryPayment.makeVisible(true)
                        "${getString(R.string.try_again_to)} ${detail.displayMsg ?: getString(R.string.add_gocoins)}".apply {
                            binding?.tvPaymentFailed?.text = this
                        }
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun carouselResponseObserver() {
        viewModel.provideCarouselResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    if (it.body.data.data != null && it.body.data.data.isNotEmpty()) {
                        carouselList = it.body.data.data.toMutableList()
                        if (carouselList.size > 0) {
                            setCarousel()
                            binding?.rvCarousel.makeVisible(true)
                        } else binding?.rvCarousel.makeVisible(false)
                    } else binding?.rvCarousel.makeVisible(false)
                }
                is Result.Failure -> { //                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun gmdObserver() {
        viewModel.provideGMD().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    val response = it.body.data
                    response.gocoin_unlocked_features?.let { list ->
                        if (list.isNotEmpty() && list.contains(AppENUM.GMB)) {
                            isComboPurchased = true
                            if (response.gmb_updated == null || response.gmb_updated == false) {
                                binding?.llRequestGMBParent.makeVisible(true)
                            } else {
                                binding?.llRequestGMBParent.makeVisible(false)
                            }
                        } else {
                            binding?.llRequestGMBParent.makeVisible(false)
                        }
                    } ?: binding?.llRequestGMBParent.makeVisible(false)
//                    setExtraText(it)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    binding?.llRequestGMBParent.makeVisible(false)
                }
            }
        }
    }

    private fun setExtraText() {
//        if (viewModel.getIntSharedPreference(AppENUM.GO_COINS_VALUE) == 0) {
            ("${getString(R.string.only)} ${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    getString(R.string.currency_symbol_default))
            }${
                (dashboardViewModel.screensConfigResponse.value?.membership?.membershipPrice ?: "699")
            }").apply {
                binding?.tvExtraText?.text = this
            }
//        } else ("${getString(R.string.only)} ${
//            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
//                getString(R.string.currency_symbol_default))
//        }${
//            (it.body.data.gmbPrice ?: "0").toInt()
//                .div(viewModel.getIntSharedPreference(AppENUM.GO_COINS_VALUE))
//        }").apply {
//            binding?.tvExtraText?.text = this
//        }
    }

    private fun homeScreenResponseObserver() {
        viewModel.provideHomeScreenResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    isHomeFetched = true
                    it.body.data.let { homeScreenData ->
                        if (isAdded) {
                            homeScreenResponse = homeScreenData
                            viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.HOME_SCREEN_RESPONSE,
                                Gson().toJson(homeScreenData))
                            binding?.nsvHome.makeVisible(true)
                            setData(homeScreenData)
                            setWsBiddingSummaryData(homeScreenData.workshop?.biddingSummary)
                            setOrderCount()
                            viewModel.getAfterHomeaPIs()
                        }
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeNearByWorkshop() {
        viewModel.provideGetNearByBidingWorkshop().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    nearByBidingWorkshopResponse = it.body.data
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setOrderCount() = binding?.apply {
        homeScreenResponse?.let {
            tvOOCount.text = (it.openOrder?.count ?: 0).toString()
            tvWIPCount.text = (it.wipOrder?.count ?: 0).toString()
            tvReadyCount.text = (it.readyForDelivery?.count ?: 0).toString()
            tvCompleteOCount.text = (it.completeOrder?.count ?: 0).toString()

            ordersCount = it.openOrder?.count?.plus(it.ongoingOrder?.count ?: 0)
                ?.plus(it.wipOrder?.count ?: 0)?.plus(it.readyForDelivery?.count ?: 0)
                ?.plus(it.completeOrder?.count ?: 0) ?: 0

        }
    }

    private fun setWsBiddingSummaryData(wsBiddingSummary: BiddingSummary?) = binding?.apply {
        wsBiddingSummary?.let {
            tvTotalWsBids.text = it.totalBids.toString()
            tvPendingWsBids.text = it.pendingBids.toString()
            tvApprovedWsBids.text = it.approvedBids.toString()
            tvNewOrderCount.text = it.newBids.toString()
            tvAcceptedOrderCount.text = it.acceptedBids.toString()
        }
    }

    private fun setCarousel() {
        carouselAdapter = CarouselAdapter(carouselList, this)
        binding?.rvCarousel?.adapter = carouselAdapter
    }

    private fun setData(data: HomeScreenResponse) {
        binding?.srl.makeVisible(true)
        setSummaryAdapterData(data)

        binding?.tvTotalEmployeeCount?.text = data.employee.toString()

        if ((data.ongoingOrder?.count ?: 0) > 0) {
            binding?.clOngoingOrders.makeVisible(true)
            if (::ongoingOrdersList.isInitialized) ongoingOrdersList.clear()
            ongoingOrdersList =
                (data.ongoingOrder?.data ?: mutableListOf()) as MutableList<DataItem>
            setOngoingOrdersAdapter()
            if (data.ongoingOrder?.count.toString()
                    .toInt() > 2) binding?.tvViewAll.makeVisible(true)
            else binding?.tvViewAll.makeVisible(false)
        } else {
            binding?.clOngoingOrders.makeVisible(false)
        }
        if (!viewModel.getBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN,
                true) && !walkThroughInProgress) startWalkthrough(binding?.tvWorkshopNameFH)

        binding?.tvGCBalance?.text = data.workshop?.gocoinBalance.toString()
        viewModel.putStringSharedPreference(AppENUM.GO_COIN_BALANCE_AMOUNT,
            data.workshop?.gocoinBalance.toString())
        binding?.tvGoCoinBalance?.text =
            viewModel.getStringSharedPreference(AppENUM.GO_COIN_BALANCE_AMOUNT, "")
        EventBus.getDefault().post(GoCoinBalanceReceivedEvent(data.workshop?.gocoinBalance ?: 0))
        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME,
            data.workshop?.name ?: "")
        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_CITY,
            data.workshop?.city ?: "")

        if (data.workshop?.membershipStatus == true){
            viewModel.putBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS, true)
        }else{
            viewModel.putBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS, false)
        }
        if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                false)) {
            binding?.clProActive.makeVisible(true)
        }else{
            binding?.clProActive.makeVisible(false)
        }
    }

    private fun setSummaryAdapterData(data: HomeScreenResponse) {
        val summeryData: MutableList<HomeSummeryData> = mutableListOf()
        summeryData.add(HomeSummeryData("0",
            data.openOrder?.count.toString(),
            data.wipOrder?.count.toString(),
            data.completeOrder?.count.toString(),
            isOrderSummery = true))
        val totPay = "${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }${
            String.format("%.2f", data.payment?.total ?: 0.0)
        }"
        val penCount = "${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }${
            String.format("%.2f", data.payment?.pending ?: 0.0).replace("-", "")
        }"
        val paidCount = "${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }${
            String.format("%.2f", data.payment?.paid ?: 0.0)
        }"
        summeryData.add(HomeSummeryData("0", totPay, penCount, paidCount, isOrderSummery = false))

        if (::summaryAdapter.isInitialized) summaryAdapter.setData(summeryData)
    }

    private fun setOngoingOrdersAdapter() {
        ongoingOrdersAdapter =
            OngoingOrdersAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default)),
                ongoingOrdersList,
                this,
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE))
        binding?.rvOngoingOrders?.adapter = ongoingOrdersAdapter
    }

    override fun onClick(v: View) {
        if (dashboardViewModel.getBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN, true)) {
            when (v.id) {
                R.id.llRequestGMBParent -> {
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_TAP_G_COMBO_REMINDER,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE)
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.GMB_INFO, null),
                        target = R.id.fragment_container_2)
                }
                R.id.llViewGOCOIN -> {
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_WALLET,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE)
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, null),
                        target = R.id.fragment_container_2)
                }
                R.id.tvShopType, R.id.tvWorkshopNameFH -> {
                    Bundle().apply {
                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE)
                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_TAP_WORKSHOP,
                            this)
                    }

                    showSelectWorkshopDialog(SelectWorkshopDialogParams(isStart = false,
                        isHome = true))
                }
                R.id.btnNewOrder, R.id.imgNewOrder -> {
                    createNewOrderOnClick()
                }
                R.id.llReminders -> {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.REMINDERS, null))
                }
                R.id.llGoCoins -> {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, null),
                        target = R.id.fragment_container_2)

                    Bundle().apply {
                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE)
                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_WALLET,
                            this)
                    }
                }
                R.id.btnAddEmployee -> {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.ADD_EDIT_EMPLOYEE,
                            Bundle().apply {
                                putString(AppENUM.IntentKeysENUM.WORKSHOP_ID,
                                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                                        ""))
                                putString(AppENUM.IntentKeysENUM.SHOP_TYPE,
                                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE))
                            }),
                        target = R.id.fragment_container_2)
                }
                R.id.tvViewAll -> goToOrdersList(0)
                R.id.llOpenOrders -> if ((homeScreenResponse?.openOrder?.count ?: 0) > 0) {
                    goToOrdersList(1)
                } else CommonUtils.showToast(requireContext(),
                    getString(R.string.empty_open_orders),
                    true)
                R.id.llWIPOrders -> if ((homeScreenResponse?.wipOrder?.count ?: 0) > 0) {
                    goToOrdersList(2)
                } else CommonUtils.showToast(requireContext(),
                    getString(R.string.empty_wip_orders),
                    true)
                R.id.llReadyOrders -> if ((homeScreenResponse?.readyForDelivery?.count ?: 0) > 0) {
                    goToOrdersList(3)
                } else CommonUtils.showToast(requireContext(),
                    getString(R.string.empty_ready_orders),
                    true)
                R.id.llCompletedOrders -> if ((homeScreenResponse?.completeOrder?.count ?: 0) > 0) {
                    goToOrdersList(4)
                } else CommonUtils.showToast(requireContext(),
                    getString(R.string.empty_completed_orders),
                    true)
                R.id.rlEmployeeSummary -> CommonUtils.addFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.MANAGE_EMPLOYEE, null),
                    target = R.id.fragment_container_2)
                R.id.tvSearchOrders -> {
                    Bundle().apply {
                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE)
                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_TAP_SEARCH,
                            this)
                    }
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SEARCH_ORDERS,
                            Bundle().apply {
                                putInt(AppENUM.ORDERS_COUNT, ordersCount)
                            }),
                        target = R.id.fragment_container_2)
                }
                R.id.imgGMBBanner -> { //                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_G_COMBO,
                    //                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE)
                    fireInitMembershipEvent()
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SUBSCRIPTION_INFO_FRAGMENT))
                }
                R.id.imgCancelRetryPayment -> {
                    showLoader()
                    viewModel.cancelRetryPayment(IdRequestResponse().apply {
                        id = retryPaymentResponse.detail?.id ?: ""
                    })
                }
                R.id.tvRetryPayment -> {
                    if (!retryPaymentResponse.detail?.isMembership.isNullOrEmpty() && retryPaymentResponse.detail?.isMembership=="true"){
                        dashboardViewModel.screensConfigResponse.value?.membership?.membershipPrice?.let { price->
                            Bundle().apply {
                                putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                                putString(AppENUM.IS_FROM,
                                    AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_PRO)
                                putString(AppENUM.AMOUNT_TO_BE_PURCHASED, price)
                                CommonUtils.addFragmentUtil(activity,
                                    FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, this))
                            }
                        }
                    }else {
                        Bundle().apply {
                            putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                            putString(AppENUM.IS_FROM,
                                AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_HOME)
                            putString(AppENUM.AMOUNT_TO_BE_PURCHASED,
                                (retryPaymentResponse.detail?.value.toString()))
                            putString(AppENUM.GO_COIN_TO_GET,
                                (retryPaymentResponse.detail?.coins.toString()))
                            CommonUtils.addFragmentUtil(activity,
                                FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, this),
                                target = R.id.fragment_container_2)
                        }
                    }
                }
                R.id.tvMonth -> {
                    onMonthClick()
                }
                R.id.tvMonthItem -> {
                    CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.tvMonthItem))?.let {
                        onMonthItemClick(it)
                    }
                }
                R.id.imgBidding -> {
                    handleBidingUi()
                }
                R.id.llWsTotalBids, R.id.llWsApprovedBids, R.id.llWsPendingBids -> {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_BIDDING_LIST_FRAGMENT,
                            null))
                }
                R.id.llAccRetNewOrders -> {
                    accRetNewOrdersCardClick()
                }
                R.id.llAccRetAcceptedOrders -> {
                    accRetAcceptedOrdersCardClick()
                }
                R.id.clBaseBuyGC -> {
                    CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                        CommonUtils.genericCastOrNull<String>(v.getTag(R.id.model))?.let { model ->
                            navigateToOrderDetail(position, model)
                        }
                    }
                }
                R.id.imgCarouselImage -> {
                    CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                        onCarouselImageClick(position)
                    }
                }
                R.id.imgSparesBanner -> {
                    dashboardViewModel.onTabSelected(SelectTabParams(FragmentFactory.Fragments.SPARES_TABS,
                        true))
                }
                R.id.clBaseISSC -> {
                    CommonUtils.genericCastOrNull<Category>(v.getTag(R.id.model))?.let { model ->
                        CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))
                            ?.let { position ->
                                CommonUtils.addFragmentUtil(requireActivity(),
                                    FragmentFactory.fragment(FragmentFactory.Fragments.SELECT_CATEGORY_OR_BRAND,
                                        Bundle().apply {
                                            putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                                            putInt(AppENUM.POSITION, position)
                                        }))
                                fireSelectCatEvent(model.categoryName ?: "")
                            }
                    }
                }

                R.id.tvSeeAllCatsFSS -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SELECT_CATEGORY_OR_BRAND,
                            Bundle().apply {
                                putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                                putInt(AppENUM.POSITION, 0)
                            })
                    )
                }

            }
        }
    }


    private fun fireSelectCatEvent(category: String) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.SELECTED_CATEGORY, category)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.SELECT_CAT,
            bundle)
    }

    private fun accRetAcceptedOrdersCardClick() {
        if (::nearByBidingWorkshopResponse.isInitialized && nearByBidingWorkshopResponse.isNotEmpty()) {
            CommonUtils.addFragmentUtil(requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.ENQUIRY_ORDER_REQUESTS,
                    Bundle().apply {
                        putInt(AppENUM.POSITION, 2)
                        putBoolean(AppENUM.IS_COMBO_PURCHASED, isComboPurchased)
                    }))
        } else {
            openWorkshopAddressUp(getString(R.string.can_not_view_bid))
        }
    }

    private fun accRetNewOrdersCardClick() {
        if (::nearByBidingWorkshopResponse.isInitialized && nearByBidingWorkshopResponse.isNotEmpty()) {
            CommonUtils.addFragmentUtil(requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.ENQUIRY_ORDER_REQUESTS,
                    Bundle().apply {
                        putInt(AppENUM.POSITION, 0)
                        putBoolean(AppENUM.IS_COMBO_PURCHASED, isComboPurchased)
                    }))
        } else {
            openWorkshopAddressUp(getString(R.string.can_not_view_bid))
        }
    }

    private fun onCarouselImageClick(position: Int) {
        if (carouselList[position].redirection?.isNotEmpty() == true) {
            handleRedirection(carouselList[position])

            Bundle().apply {
                putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE)
                putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.BANNER_RED,
                    carouselList[position].redirection)
                putInt(FirebaseAnalyticsLog.FirebaseEventNameENUM.BANNER_POSITION, position)
                FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_CLICK_BANNER,
                    this)
            }
        }
    }

    private fun onMonthClick() = binding?.apply {
        if (llMonthList.isVisible()) {
            tvMonth.changeViewCompoundDrawablesWithInterinsicBounds(null,
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_drop_down_white_small),
                null,
                null)
            llMonthList.makeVisible(false)
        } else {
            tvMonth.changeViewCompoundDrawablesWithInterinsicBounds(null,
                ContextCompat.getDrawable(requireContext(),
                    R.drawable.ic_drop_down_reverse_white_small),
                null,
                null)
            llMonthList.makeVisible(true)
            rvMonth.scrollToPosition(CommonUtils.getCurrentMonth() - 1)
        }
    }

    private fun onMonthItemClick(it: Int) = binding?.apply {
        monthPos = it + 1
        if (::arrayListMonth.isInitialized) {
            tvMonth.text = arrayListMonth[it]

            showLoader()
            viewModel.getDataForMonth(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                ""), monthPos.toString())
        }
        tvMonth.changeViewCompoundDrawablesWithInterinsicBounds(null,
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_drop_down_white_small),
            null,
            null)
        llMonthList.makeVisible(false)
    }

    private fun navigateToOrderDetail(position: Int, model: String) {
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                ongoingOrdersList[position].orderId)
                        }),
                    target = R.id.fragment_container_2)
            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                ongoingOrdersList[position].orderId)
                        }),
                    target = R.id.fragment_container_2)
            }
            else -> {
                if (model == "longClick") {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                            Bundle().apply {
                                putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                    ongoingOrdersList[position].orderId)
                            }),
                        target = R.id.fragment_container_2)
                } else {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                            Bundle().apply {
                                putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                    ongoingOrdersList[position].orderId)
                            }),
                        target = R.id.fragment_container_2)
                }
            }
        }
    }

    private fun createNewOrderOnClick() {
        if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                false)) {
            createNewOrderActual()
        } else {
            if (ordersCount < (dashboardViewModel.screensConfigResponse.value?.membershipConfig?.orderCreation
                    ?: 0)) {
                createNewOrderActual()
            } else {
                fireFeatureLockedEvent()
                requireActivity().let {
                    FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT, Bundle().apply {
                        putString(AppENUM.PRO_LOCKED_ALERT_MSG,  dashboardViewModel.screensConfigResponse.value?.membershipConfig?.orderCreationMsg)
                    })
                        ?.let { baseFragment ->
                            baseFragment.show(it.supportFragmentManager,
                                baseFragment.javaClass.name)
                        }
                }
            }
        }
    }

    private fun createNewOrderActual() {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_CREATE_NEW_ORDER,
                this)
        }
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE, "")) {
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_NEW_ORDER,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                        }),
                    target = R.id.fragment_container_2)
            }
            else -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                        }),
                    target = R.id.fragment_container_2)
            }
        }
    }

    private fun handleBidingUi() {
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) {
            if (::nearByBidingWorkshopResponse.isInitialized && nearByBidingWorkshopResponse.isNotEmpty()) {
                setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_BIDDING,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE)
                CommonUtils.addFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_BIDDING_ADD_PART_FRAGMENT,
                        null))
            } else {
                openWorkshopAddressUp(getString(R.string.can_not_place_order))
            }
        } else {
            dashboardViewModel.onTabSelected(SelectTabParams(1, true))
        }
    }

    private fun openWorkshopAddressUp(string: String) {
        FragmentFactory.fragmentDialog(FragmentFactory.Fragments.WORKSHOP_ADDRESS_NOT_AVL,
            Bundle().apply {
                putString(AppENUM.DATA, string)
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    if (extra as Boolean) {
                        openRegBBottomSheet()
                    }
                }
            })?.let { dialog ->
            dialog.show(parentFragmentManager, dialog.javaClass.name)
        }
    }

    private fun openRegBBottomSheet() {
        requireActivity().let {
            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.COMPLETE_REGISTRATION,
                Bundle().apply {
                    this.putBoolean("home", true)
                },
                object : ActionListener {
                    override fun onActionItem(extra: Any?, extra2: Any?) {
                        if (extra as? Boolean? == true) {
                            showLoader()
                            viewModel.getNearByWorkshop()
                        }
                    }
                })?.let { baseFragment ->
                baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
            }
        }
    }

    private fun goToOrdersList(type: Int) {
        CommonUtils.addFragmentUtil(activity,
            FragmentFactory.fragment(FragmentFactory.Fragments.ORDERS_LIST,
                Bundle().apply { putInt(AppENUM.ORDER_TYPE, type) }),
            target = R.id.fragment_container_2)
    }

    override fun onResume() {
        super.onResume()
        setWorkshopName()
        if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                false)) {
            binding?.clProActive.makeVisible(true)
        }else{
            binding?.clProActive.makeVisible(false)
        }
    }

    private fun setWorkshopName() = binding?.apply {
        tvWorkshopNameFH.text =
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME, "")

        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                tvShopType.text = getString(R.string.accessories_shop)
            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                tvShopType.text = getString(R.string.spares_shop)
            }
            else -> {
                tvShopType.text = getString(R.string.car_workshop)
            }
        }
    }

    override fun handleBackPress(): Boolean {
        return false
    }

    private fun handleRedirection(carouselDataItem: CarouselDataItem) {
        val uri = Uri.parse(carouselDataItem.redirection)
        val pageName = uri.getQueryParameter(AppENUM.PAGE_NAME)
        dashboardViewModel.savePageNameAndRedirect(pageName.toString())
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is UpdateHomeScreenEvent -> {
                2
                showLoader()
                setWorkshopName()
                viewModel.getHomeScreenAPI(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                    ""))
            }
            is GoCoinBalanceReceivedEvent -> {
                binding?.tvGCBalance?.text = event.gocoinBalance.toString()
                binding?.tvGoCoinBalance?.text = event.gocoinBalance.toString()
            }
            is GmbRequestedEvent -> {
                if (event.isGMBRequested) {
                    binding?.llRequestGMBParent.makeVisible(false)
                } else {
                    binding?.llRequestGMBParent.makeVisible(true)
                }
            }
            is CompleteRegLocUpdateEventHome -> {
                if (isResumed && isVisible) {
                    activity?.let {
                        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.COMPLETE_REGISTRATION,
                            Bundle().apply {
                                this.putParcelable(AppENUM.COMPLETE_REG_DATA, event.addressData)
                                this.putBoolean("home", true)
                            },
                            object : ActionListener {
                                @RequiresApi(Build.VERSION_CODES.P)
                                override fun onActionItem(extra: Any?, extra2: Any?) {
                                    if (extra as Boolean? == true) {
                                        showLoader()
                                        viewModel.getNearByWorkshop()
                                    }
                                }
                            })?.let { baseFragment ->
                            baseFragment.show(it.supportFragmentManager,
                                baseFragment.javaClass.name)
                        }
                    }
                }
            }
        }
    }

    override fun onLocationChanged(p0: Location) { //do nothing
    }

    fun createNewOrder() {
        binding?.imgNewOrder?.callOnClick()
    }

    fun updateScreen() {
        if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                false)) {
            binding?.clProActive.makeVisible(true)
        }else{
            binding?.clProActive.makeVisible(false)
        }
        setExtraText()
        setWorkshopName()
        internationalUserUIChanges()
        setUpData()
        workshopWiseUIChanges()
        setSelectMonthAdapter() //        showLoader()
        //        if (!isHomeFetched) {
        viewModel.getHomeScreenAPI(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
            "")) //        }
    }

    private fun fireInitMembershipEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_MEMBERSHIP,
            bundle)
    }


    private fun fireFeatureLockedEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.ERROR_HEADER_PRO,
            bundle)

    }
}