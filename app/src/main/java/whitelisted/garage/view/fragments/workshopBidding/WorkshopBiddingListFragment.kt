package whitelisted.garage.view.fragments.workshopBidding

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import com.google.android.material.tabs.TabLayout
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.request.MarkCompleteWsBidRequest
import whitelisted.garage.api.response.GetWsBidEnquiryResponse
import whitelisted.garage.api.response.GetWsOngoingBidResponse
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentWorkshopBidListBinding
import whitelisted.garage.eventBus.RefreshWsBidList
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.WorkBidHistoryListAdapter
import whitelisted.garage.view.adapter.WorkshopBidListAdapter
import whitelisted.garage.viewmodels.WorkShopBiddingViewModel

class WorkshopBiddingListFragment : BaseFragment(), TabLayout.OnTabSelectedListener {

    private val binding by viewBinding(FragmentWorkshopBidListBinding::inflate)
    private val viewModel: WorkShopBiddingViewModel by sharedViewModel()
    private var workshopBidListAdapter: WorkshopBidListAdapter? = null
    private var workshopBidListHistoryAdapter: WorkBidHistoryListAdapter? = null
    private var ongGoingEventType = "1"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListener()
        observeResponses()
        setTabLayout()
        getData()
    }

    private fun getData() {
        showLoader()
        viewModel.getOnGoingWsBiddingList(ongGoingEventType)
    }

    private fun observeResponses() {
        observeOnGoingBidList()
        observeHistoryBidList()
        observeMarkComplete()
        observeRefreshBid()
    }

    private fun setTabLayout() = binding.apply {
        tlBidList.addTab(tlBidList.newTab().apply { text = getString(R.string.new_) })
        tlBidList.addTab(tlBidList.newTab().apply { text = getString(R.string.on_going_small) })
        tlBidList.addTab(tlBidList.newTab().apply { text = getString(R.string.history) })
        tlBidList.addOnTabSelectedListener(this@WorkshopBiddingListFragment)
    }

    private fun observeRefreshBid() {
        viewModel.provideRefreshReceivedBidList().observe(viewLifecycleOwner) {
            if (it == true) {
                showLoader()
                viewModel.getOnGoingWsBiddingList(ongGoingEventType)
            }
        }
    }

    private fun observeMarkComplete() {
        viewModel.provideMarkCompleteWsBid().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.let {
                        showLoader()
                        viewModel.getOnGoingWsBiddingList(ongGoingEventType)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
    }

    private fun observeOnGoingBidList() {
        viewModel.provideOnGoingBidList().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.let { list ->
                        setDataToOnGoingNewAdapter(list)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
    }

    private fun setDataToOnGoingNewAdapter(list: List<GetWsOngoingBidResponse>) = binding.apply {
        tvBidCount.text = (list.size).toString()
        if (list.isNotEmpty()) {
            workshopBidListAdapter = WorkshopBidListAdapter(list, this@WorkshopBiddingListFragment)
            rvWsBidList.adapter = workshopBidListAdapter
            llNoDataScreen.makeVisible(false)
            rvWsBidList.makeVisible(true)
        } else {
            llNoDataScreen.makeVisible(true)
            rvWsBidList.makeVisible(false)
        }
    }

    private fun observeHistoryBidList() {
        viewModel.provideHistoryBidList().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.let { list ->
                        setDataToHistoryAdapter(list)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
    }

    private fun setDataToHistoryAdapter(list: List<GetWsOngoingBidResponse>) = binding.apply {
        if (list.isNotEmpty()) {
            workshopBidListHistoryAdapter = WorkBidHistoryListAdapter(list, this@WorkshopBiddingListFragment)
            rvWsBidList.adapter = workshopBidListHistoryAdapter
            llNoDataScreen.makeVisible(false)
            rvWsBidList.makeVisible(true)
        } else {
            llNoDataScreen.makeVisible(true)
            rvWsBidList.makeVisible(false)
        }
    }

    private fun clickListener() {
        binding.imgBack.setOnClickListener(this)
        binding.btnAddPart.setOnClickListener(this)
        binding.llSwipeRefresh.setOnRefreshListener {
            binding.llSwipeRefresh.isRefreshing = false
            when (binding.tlBidList.selectedTabPosition) {
                0 -> {
                    ongGoingEventType = "1"
                    showLoader()
                    viewModel.getOnGoingWsBiddingList(ongGoingEventType)
                }
                1 -> {
                    ongGoingEventType = "2"
                    showLoader()
                    viewModel.getOnGoingWsBiddingList(ongGoingEventType)
                }
                2 -> {
                    showLoader()
                    viewModel.getWsHistoryBiddingList()
                }
            }
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
            R.id.tvCall -> {
                CommonUtils.genericCastOrNull<String>(v.getTag(R.id.model))?.let {
                    Intent(Intent.ACTION_DIAL).apply {
                        this.data = Uri.parse("tel:${it}")
                        startActivity(this)
                    }
                }
            }
            R.id.tvDirections -> {
                CommonUtils.genericCastOrNull<GetWsOngoingBidResponse>(v.getTag(R.id.model))?.let {
                    it.retailerDetails?.let { da ->
                        openWebPage("${AppENUM.MAP_URL}${da.latitude},${da.longitude}")
                    }
                }
            }
            R.id.btnMarkComplete -> {
                CommonUtils.genericCastOrNull<GetWsOngoingBidResponse>(v.getTag(R.id.model))?.let {
                    showLoader()
                    viewModel.markCompleteWsBid(MarkCompleteWsBidRequest(id = it.id))
                    firebaseAcceptRejBidEvent(it)
                }
            }
            R.id.llParent -> {
                CommonUtils.genericCastOrNull<GetWsOngoingBidResponse>(v.getTag(R.id.model))?.let {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_BID_RECEIVED_FRAGMENT,
                            Bundle().apply {
                                putParcelable(AppENUM.IntentKeysENUM.WORKSHOP_BID_DATA, it)
                            }))
                }
            }
            R.id.tvItems -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.tvItems))?.let {
                    workshopBidListAdapter?.list?.get(it)?.isItemTextExpanded = true
                    workshopBidListAdapter?.notifyItemChanged(it)
                }
            }

            R.id.btnAddPart -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_BIDDING_ADD_PART_FRAGMENT,
                        null))
            }
            R.id.btnInvoice -> {
                CommonUtils.genericCastOrNull<GetWsOngoingBidResponse>(v.getTag(R.id.model))?.let {
                    (BuildConfig.BASE_URL + AppENUM.BID_PDF_END_POINT + it.retailerDetails?.biding_id).apply {
                        Intent(Intent.ACTION_VIEW).let { intent ->
                            intent.data = Uri.parse(this)
                            startActivity(intent)
                        }
                    }
                }
            }
        }
    }

    private fun firebaseAcceptRejBidEvent(data: GetWsOngoingBidResponse) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_WORKSHOP_BID_LIST)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.RETAILER_NAME,
                data.retailerDetails?.workshopName)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.RETAILER_LOC,
                data.retailerDetails?.address)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.RETAILER_NUMBER,
                data.retailerDetails?.contactNumber)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.TOTAL_COST, data.totalAmount)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_WORKSHOP_MARK_COMPLETE,
                this)
        }
    }

    private fun openWebPage(url: String?) {
        Uri.parse(url).apply {
            val intent = Intent(Intent.ACTION_VIEW, this)
            intent.resolveActivity(requireActivity().packageManager)?.let {
                startActivity(intent)
            }
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is RefreshWsBidList -> {
                popBackStackAndHideKeyboard()
                popBackStackAndHideKeyboard()
                viewModel.getOnGoingWsBiddingList(ongGoingEventType)
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_BIDDING_STATUS_FRAGMENT,
                        Bundle().apply {
                            putBoolean(AppENUM.IS_VIEW_ONLY, true)
                            putParcelable(AppENUM.USER_DATA,
                                event.extra as? GetWsBidEnquiryResponse)
                        }))
            }
        }
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        when (tab?.position) {
            0 -> {
                ongGoingEventType = "1"
                showLoader()
                viewModel.getOnGoingWsBiddingList(ongGoingEventType)
            }
            1 -> {
                ongGoingEventType = "2"
                showLoader()
                viewModel.getOnGoingWsBiddingList(ongGoingEventType)
            }
            2 -> {
                showLoader()
                viewModel.getWsHistoryBiddingList()
            }
        }

    }

    override fun onTabUnselected(tab: TabLayout.Tab?) { // nothing
    }

    override fun onTabReselected(tab: TabLayout.Tab?) { // nothing
    }

}