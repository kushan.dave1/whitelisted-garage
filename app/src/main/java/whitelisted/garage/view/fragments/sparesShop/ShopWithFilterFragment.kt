package whitelisted.garage.view.fragments.sparesShop

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.CarAttr
import whitelisted.garage.api.response.MarketplaceFilter
import whitelisted.garage.api.response.PlpBannerObject
import whitelisted.garage.api.response.SSItemModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentShopWithFilterBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.isVisible
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.CommonUtils.onScrolledToBottom
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.activities.DashboardActivity
import whitelisted.garage.view.adapter.SSFilterCategoryAdapter
import whitelisted.garage.view.adapter.SSItemsAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.SelectCarDialogViewModel
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import whitelisted.garage.viewmodels.WalkthroughSharedViewModel
import java.lang.Exception

class ShopWithFilterFragment : BaseFragment() {

    private val binding by viewBinding(FragmentShopWithFilterBinding::inflate)
    private val viewModel: SparesShopFragmentViewModel by sharedViewModel()
    private val selectCarViewModel: SelectCarDialogViewModel by sharedViewModel()
    private val walkThroughViewModel: WalkthroughSharedViewModel by sharedViewModel()
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var limit = 12
    private var plpBannersPosition = 4
    private var isTextSearch = false
    private var isFilterChanged = true
    private lateinit var defaultStartList: MutableList<SSItemModel>
    private var sortAndFilters = listOf<MarketplaceFilter>()
    private var plpBanners: MutableList<PlpBannerObject>? = null
    private var plpBannersOrg: List<PlpBannerObject>? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        CommonUtils.hideKeyboard(requireActivity())
        setupScreen()
    }

    private fun setupScreen() = binding.apply {
        clickListeners()
        setObservers()
        setHeaderText()
        swipeRefresh()
        setLayoutManager()

        arguments?.let {

            val isForPreviouslyOrdered = it.getBoolean(AppENUM.SHOW_PREVIOUSLY_ORDERED, false)
            if (isForPreviouslyOrdered) setupScreenForPreviouslyOrdered()
            else {
                val brandFilter = it.getString(AppENUM.IntentKeysENUM.BRAND_NAME, "")
                val categoryFilter = it.getString(AppENUM.IntentKeysENUM.CATEGORY_NAME, "")
                plpBannersPosition = it.getInt(AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION, 4)
                if (plpBannersPosition != 4) limit = plpBannersPosition.times(3)

                setSelected(brandFilter, categoryFilter)
                this@ShopWithFilterFragment.plpBannersOrg =
                    it.getParcelableArrayList<PlpBannerObject?>(AppENUM.IntentKeysENUM.PLP_BANNERS)
                        ?.toMutableList()
                getAttributesAndFetchProducts()
                setRecyclerPagination()
            }
        }

        updateViewCartCard()

        if (viewModel.provideGetCartResponse().cartList.isNotEmpty() && (viewModel.provideGetCartResponse().cartList[0].id.isEmpty() || viewModel.provideGetCartResponse().cartList[1].id.isEmpty())) viewModel.provideGetCartResponse()

    }

    private fun setLayoutManager() {
        val lm = GridLayoutManager(requireContext(), 2)
        lm.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int):Int  {
                val span = (binding.rvItemsFSWF.adapter as SSItemsAdapter).getItemViewType(position)
                return if(span > 2) 2 else span
            }
        }
        binding.rvItemsFSWF.layoutManager = lm
    }

    private fun getAttributesAndFetchProducts() = lifecycleScope.launch(Dispatchers.Main) {
        binding.slItems.makeVisible(true)
        binding.slItems.startShimmer()
        viewModel.getSearchAndFilterAttributes()
        setSelectedBrandAndCategory()
        showSelectedCar()
        getProductsList()
    }

    private fun setupScreenForPreviouslyOrdered() = binding.apply {

        slItems.makeVisible(false)
        tvSearchFSWF.makeVisible(false)
        tvFilterViaCars.makeVisible(false)
        btnFilterAndSort.makeVisible(false)
        tvHeaderFSWF.text = resources.getString(R.string.previously_ordered)
        lifecycleScope.launch {
            setItemsAdapter(viewModel.getPreviouslyOrderedItems().toMutableList())
        }


    }

    private fun setSelected(brandFilter: String?, categoryFilter: String?) =
        viewModel.searchAndFilterAttrs.let { fs ->

            if (brandFilter != null) fs.brands.find { it.name == brandFilter }?.let {
                fs.brandCount = 1
                it.isSelected = true
            }

            if (categoryFilter != null) fs.categories.find { c ->
                val sc = c.subCategory.find { sc ->
                    val sca = sc.subArray.find { it.name == categoryFilter }
                    sca?.isSelected = true
                    sca != null
                }
                sc?.isSelected = true
                sc != null
            }?.let {
                fs.categoryCount = 1
                it.isSelected = true
            }
        }

    private fun setHeaderText() {
        val search = viewModel.searchAndFilterAttrs.textSearch
        isTextSearch = search.isNotEmpty()
        binding.tvHeaderFSWF.text =
            if (isTextSearch) search else resources.getString(R.string.marketplace)

        binding.ivEnquiry.setOnClickListener {
            CommonUtils.addFragmentUtil(
                requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.SEND_ENQUIRY, Bundle().apply {
                    putString(AppENUM.SEARCH_STRING, search)
                })
            )
            fireEnquireNowEvent(search)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setSelectedBrandAndCategory() = viewModel.searchAndFilterAttrs.let { fs ->
        when {
            fs.brandCount > 0 -> {
                val brand = fs.brands.find { it.isSelected }
                if (brand != null) binding.tvPartBrand.text =
                    brand.name + if (fs.brandCount > 1) " + ${fs.brandCount - 1}" else ""
            }
            else -> binding.tvPartBrand.text = resources.getString(R.string.all)
        }

        when {
            fs.categoryCount > 0 -> {
                fs.categories.find { c ->
                    c.subCategory.find { sc ->
                        val sca = sc.subArray.find { it.isSelected }
                        if (sca != null) binding.tvSelectedCategory.text =
                            sca.name + if (fs.categoryCount > 1) " + ${fs.categoryCount - 1}" else ""
                        sca != null
                    } != null
                }
            }
            else -> binding.tvSelectedCategory.text = resources.getString(R.string.all)
        }
    }

    private fun swipeRefresh() = binding.apply {
        srlSWFF.setOnRefreshListener {
            srlSWFF.isRefreshing = false
            if(!binding.tvSearchFSWF.isVisible()) return@setOnRefreshListener
            showLoader()
            isLastPageRV = false
            isFilterChanged = true
            getProductsList()
        }
    }

    private fun getProductsList() = binding.apply {
        plpBanners = plpBannersOrg?.toMutableList()
        viewModel.getItemWithAppliedFilters(limit, 0)
    }

    private fun observeSSCategoriesResponse() {
        viewModel.provideSSHomeCategoriesResponse()
            .observe(viewLifecycleOwner) { //            hideLoader()
                when (it) {
                    is Result.Success -> {
                        if (it.body.data.isNotEmpty()) {
                            viewModel.setSSCategoriesList(it.body.data)
                            setCategoriesAdapter()
                        }
                    }
                    is Result.Failure -> {
                        CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    }
                }
            }
    }

    private fun setRecyclerPagination() = binding.apply {
        rvItemsFSWF.onScrolledToBottom {
            if (!isLoadingRV && !isLastPageRV) {
                isLoadingRV = true
                var offset = 0
                plpBannersOrg?.let {
                    offset = ((rvItemsFSWF.adapter as? SSItemsAdapter)?.dataList?.size ?: 0).minus(
                        plpBannersOrg?.size ?: 0.minus(plpBanners?.size ?: 0)
                    ).minus(2)
                } ?: run {
                    offset =
                        ((rvItemsFSWF.adapter as? SSItemsAdapter)?.dataList?.size ?: 0).minus(2)
                }
                viewModel.getItemWithAppliedFilters(limit, offset)
            }
        }

    }

    private fun setCategoriesAdapter() {
        if (viewModel.provideSSCategoriesList().isEmpty()) {
            viewModel.getSSSparesCategoriesResponse()
        } else {
            viewModel.provideSSCategoriesList().forEach {
                it.isSelected = false
            }
            binding.rvBrandCategoryFSWF.adapter =
                SSFilterCategoryAdapter(viewModel.provideSSCategoriesList(), this)
        }
    }

    private fun clickListeners() {
        binding.imgBackFSWF.setOnClickListener(this)
        binding.tvFilterViaCars.setOnClickListener(this)
        binding.tvFilteredCar.setOnClickListener(this)
        binding.imgCancelFilter.setOnClickListener(this)
        binding.clViewCart.setOnClickListener(this)
        binding.tvSearchFSWF.setOnClickListener(this)
        binding.tvSelectedCategory.setOnClickListener(this)
        binding.tvPartBrand.setOnClickListener(this)
        binding.tvSelectedCar.setOnClickListener(this)

    }

    private fun setObservers() {
        observeItemsWithFilterResponse()
        observeAddToCartAPI()
        observeGetCartAPI()
        observeSSCategoriesResponse()
        observeApplyFilter()
        observeNotifyMeEvent()
        observeWalkThroughEvent()
    }

    private fun observeWalkThroughEvent() {
        walkThroughViewModel.provideShopWithFilterWalkThroughEvent().observe(viewLifecycleOwner) {
            if (isAdded) {
                when (it) {
                    R.id.tvSeeAllAccBrandsFSS -> {
                        startWalkthrough(binding.btnFilterAndSort)
                    }
                }
            }
        }
    }

    private fun observeNotifyMeEvent() = binding.apply {
        viewModel.provideNotifyMeLiveEvent().observe(viewLifecycleOwner) {
            it?.let { changedItemModel ->
                (rvItemsFSWF.adapter as? SSItemsAdapter)?.let { adapter ->
                    var i = 0
                    kotlin.run breaking@{
                        adapter.dataList.forEach { itemModel ->
                            if (itemModel.skuCode == changedItemModel.skuCode) {
                                adapter.dataList[i] = changedItemModel
                                adapter.notifyItemChanged(i)
                                return@breaking
                            }
                            i++
                        }
                    }
                }
                lifecycleScope.launchWhenCreated {
                    val list = viewModel.getNotifiedItemsAPI()
                    viewModel.setNotifiedItems(list)
                }
            }
        }
    }

    private fun observeApplyFilter() = binding.apply {
        viewModel.provideSparesMarketplaceFilters().observe(viewLifecycleOwner) { filters ->
            rvItemsFSWF.makeVisible(false)
            slItems.makeVisible(true)
            isLastPageRV = false
            clEmptyListFSWF.makeVisible(false)
            isFilterChanged = true
            sortAndFilters = filters.filters
            setSelectedBrandAndCategory()
            showSelectedCar()
            setHeaderText()
            getProductsList()
        }

    }

    private fun observeGetCartAPI() {
        viewModel.provideGetCartResponseAPI().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    viewModel.setGetCartResponse(it.body.data)
                    updateViewCartCard()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun updateViewCartCard() = binding.apply {
        viewModel.provideGetCartResponse().let { cart ->
            val bothLists =
                if (cart.cartList.isNotEmpty()) cart.cartList[0].lineItems + cart.cartList[1].lineItems
                else listOf()

            if (bothLists.isNotEmpty()) {
                clViewCart.makeVisible(true)
                val count = cart.cartList[0].lineItems.size
                count.plus(cart.cartList[1].lineItems.size).let { itemsCount ->
                    if (itemsCount > 1) "$itemsCount ${getString(R.string.items_added)}".apply {
                        tvItemsAdded.text = this
                    }
                    else "$itemsCount ${getString(R.string.item_added)}".apply {
                        tvItemsAdded.text = this
                    }
                }
                var totalAmount =
                    cart.cartList[0].lineItems.filter { item -> item.inventory ?: 0 > 0 }
                        .sumOf { ci ->
                            (ci.gomTotalAmount ?: 0.0)
                        }
                totalAmount =
                    totalAmount.plus(cart.cartList[1].lineItems.filter { item -> item.inventory ?: 0 > 0 }
                        .sumOf { ci ->
                            (ci.gomTotalAmount ?: 0.0)
                        })
                "${
                    viewModel.getStringSharedPreference(
                        AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        AppENUM.RefactoredStrings.defaultCurrencySymbol
                    )
                }${
                    CommonUtils.convertDoubleTo2DecimalPlaces(totalAmount)
                }".apply {
                    tvTotalPriceFSWF.text = this
                }

            } else {
                clViewCart.makeVisible(false)
            }
        }
    }

    private fun observeAddToCartAPI() = binding.apply {
        viewModel.provideAddToCartResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    hideLoader() //                    viewModel.getSSCartAPI()
                    viewModel.provideAddToCartResponse().postValue(null)
                    viewModel.cartItemChanged?.let { changedItemModel ->
                        (rvItemsFSWF.adapter as? SSItemsAdapter)?.let { adapter ->
                            var i = 0
                            kotlin.run breaking@{
                                adapter.dataList.forEach { itemModel ->
                                    if (itemModel.skuCode == changedItemModel.skuCode) {
                                        adapter.dataList[i].quantity = changedItemModel.quantity
                                        adapter.notifyItemChanged(i)
                                        return@breaking
                                    }
                                    i++
                                }
                            }
                        }
                    }
                }
                is Result.Failure -> {
                    hideLoader() //                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeItemsWithFilterResponse() = binding.apply {
        viewModel.provideItemsWithFilterResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    slItems.makeVisible(false)
                    hideLoader()
                    it.body.data.let { list ->
                        if (list.isNotEmpty()) {
                            if (!::defaultStartList.isInitialized) defaultStartList = it.body.data
                            rvItemsFSWF.makeVisible(true)
                            if (isFilterChanged) {
                                rvItemsFSWF.adapter = null
                            }

                            var afterLimit = limit
                            when {
                                list.size == limit -> {
                                    if (!plpBanners.isNullOrEmpty()) {
                                        list.add(plpBannersPosition, SSItemModel().apply {
                                            isBanner = true
                                            bannerObject = plpBanners?.get(0)
                                        })
                                        afterLimit++
                                        plpBanners?.removeAt(0)
                                        if (!plpBanners.isNullOrEmpty()) {
                                            list.add((plpBannersPosition.times(2) + 1),
                                                SSItemModel().apply {
                                                    isBanner = true
                                                    bannerObject = plpBanners?.get(0)
                                                })
                                            afterLimit++
                                            plpBanners?.removeAt(0)
                                            if (!plpBanners.isNullOrEmpty()) {
                                                list.add(plpBannersPosition.times(3) + 2,
                                                    SSItemModel().apply {
                                                        isBanner = true
                                                        bannerObject = plpBanners?.get(0)
                                                    })
                                                afterLimit++
                                                plpBanners?.removeAt(0)
                                            }
                                        }
                                    }
                                }
                                list.size >= limit.minus(plpBannersPosition) -> {
                                    if (!plpBanners.isNullOrEmpty()) {
                                        list.add(plpBannersPosition, SSItemModel().apply {
                                            isBanner = true
                                            bannerObject = plpBanners?.get(0)
                                        })
                                        afterLimit++
                                        plpBanners?.removeAt(0)
                                        if (!plpBanners.isNullOrEmpty()) {
                                            list.add(plpBannersPosition.times(2) + 1,
                                                SSItemModel().apply {
                                                    isBanner = true
                                                    bannerObject = plpBanners?.get(0)
                                                })
                                            afterLimit++
                                            plpBanners?.removeAt(0)
                                        }
                                    }
                                }
                                list.size >= limit.minus(plpBannersPosition.times(2)) -> {
                                    if (!plpBanners.isNullOrEmpty()) {
                                        list.add(plpBannersPosition, SSItemModel().apply {
                                            isBanner = true
                                            bannerObject = plpBanners?.get(0)
                                        })
                                        afterLimit++
                                        plpBanners?.removeAt(0)
                                    }
                                }
                            }

                            if (list.size < afterLimit) {
                                isLastPageRV = true
                            } else {
                                list.add(SSItemModel(isShimmer = true))
                                list.add(SSItemModel(isShimmer = true))
                            }
                            setItemsAdapter(list)
                            isLoadingRV = false

                        } else if (isFilterChanged) {
                            rvItemsFSWF.makeVisible(false)
                            clEmptyListFSWF.makeVisible(true)
                        } else {
                            val size = (rvItemsFSWF.adapter as? SSItemsAdapter)?.dataList?.size ?: 0
                            if (size > 0) {
                                (rvItemsFSWF.adapter as? SSItemsAdapter)?.dataList?.removeLast()
                                (rvItemsFSWF.adapter as? SSItemsAdapter)?.dataList?.removeLast()
                                (rvItemsFSWF.adapter as? SSItemsAdapter)?.notifyItemRangeRemoved(
                                    size - 2,
                                    2
                                )
                            }
                        }
                    }
                }
                is Result.Failure -> {
                    if (it.errorMessage.isNotEmpty()) {
                        slItems.makeVisible(false)
                        hideLoader()
                    }
                }
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    @SuppressLint("NotifyDataSetChanged")
    private fun setItemsAdapter(data: MutableList<SSItemModel>) =
        binding.apply { //        check if added in cart
            data.forEach { item -> //                item.isAcc = true
                if (viewModel.provideGetCartResponse().cartList.isNotEmpty()) {
                    var bothLists = viewModel.provideGetCartResponse().cartList[0].lineItems
                    bothLists =
                        bothLists.plus(viewModel.provideGetCartResponse().cartList[1].lineItems)
                            .toMutableList()
                    item.quantity = bothLists.find { it.skuCode == item.skuCode }?.quantity ?: 0
                    viewModel.provideGetNotifiedItemsResponse()
                        .find { it.productId == item.skuCode }?.let {
                            item.isNotified = true
                        }
                }
            }

            if (rvItemsFSWF.adapter == null) {
                rvItemsFSWF.adapter =
                    SSItemsAdapter(
                        viewModel.getStringSharedPreference(
                            AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                            AppENUM.RefactoredStrings.defaultCurrencySymbol
                        ),
                        data,
                        this@ShopWithFilterFragment,
                        addEnquiryBanner =  isTextSearch
                    )
                rvItemsFSWF.setPadding(24, 24, 24, 24)
                rvItemsFSWF.adapter?.notifyDataSetChanged()

                /*imgTopBannerSWFF.makeVisible(true)
                imgBottomBannerSWFF.makeVisible(true)*/
            } else {
                (rvItemsFSWF.adapter as? SSItemsAdapter)?.addItemListToAdapter(data)
            }
            isFilterChanged = false
        }

    override fun onClick(v: View) {
        if (dashboardViewModel.getBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN, true)) {
            super.onClick(v)
            when (v.id) {
                R.id.imgBackFSWF -> {
                    popBackStackAndHideKeyboard()
                }
                R.id.tvFilterViaCars, R.id.tvFilteredCar, R.id.tvSelectedCar -> {
                    showSelectCarDialog()
                }
                R.id.imgCancelFilter -> {
                    cancelCarFilter()
                }
                R.id.btnAdd -> {
                    CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                        it.apply {
                            type = 1
                            showLoader()
                            viewModel.addToCart(productId, this)
                        }
                        fireAddCartEvent(it)
                    }
                }
                R.id.imgPlus -> {
                    CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                        it.apply {
                            type = 1
                            showLoader()
                            viewModel.addToCart(productId, this)
                        }
                    }
                }
                R.id.imgMinus -> {
                    CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                        it.apply {
                            type = 0
                            showLoader()
                            viewModel.addToCart(productId, this)
                        }
                    }
                }

                R.id.clISI, R.id.btnEnquireNow -> {
                    sharedElementReturnTransition = TransitionInflater.from(requireContext())
                        .inflateTransition(R.transition.image_shared_element_transition)
                    exitTransition = TransitionInflater.from(requireContext())
                        .inflateTransition(android.R.transition.no_transition)
                    CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                        fireEnquireNowEvent(it)
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_PRODUCT_DETAILS,
                                Bundle().apply {
                                    putParcelable(AppENUM.SSITEM, it)
                                }).apply {
                                sharedElementEnterTransition =
                                    TransitionInflater.from(requireContext())
                                        .inflateTransition(R.transition.image_shared_element_transition)
                                exitTransition = TransitionInflater.from(requireContext())
                                    .inflateTransition(android.R.transition.no_transition)
                            })
                    }
                }

                R.id.clViewCart -> {

                    val isPreviouslyOrderedItems = binding.tvSearchFSWF.isVisible()
                    if (!isPreviouslyOrderedItems) popBackStackAndHideKeyboard()
                    else CommonUtils.addFragmentUtil(
                        activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.SS_CART)
                    )
                    fireViewCartEvent()
                }

                R.id.tvSearchFSWF -> {
                    viewModel.provideItemsWithFilterResponse().postValue(null)
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SELECT_CAR_AND_SEARCH,
                            params = Bundle().apply {
                                putInt(
                                    AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION,
                                    plpBannersPosition
                                )
                            })
                    )
                }

                R.id.tvSelectedCategory -> Bundle().apply {
                    putBoolean(AppENUM.RefactoredStrings.IS_CATEGORY, true)
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(
                            FragmentFactory.Fragments.FILTER_AND_SORT_FRAGMENT,
                            this
                        )
                    )
                }

                R.id.tvPartBrand -> Bundle().apply {
                    putBoolean(AppENUM.RefactoredStrings.IS_BRAND, true)
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(
                            FragmentFactory.Fragments.FILTER_AND_SORT_FRAGMENT,
                            this
                        )
                    )
                }

                R.id.tvNotifyMeISSI -> {
                    CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                        CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))
                            ?.let { position ->
                                showLoader()
                                lifecycleScope.launchWhenCreated {
                                    val result = viewModel.notifyForProduct(
                                        it.skuCode,
                                        it.title,
                                        it.imageUrl
                                    )
                                    if (result?.isJsonNull == false) {
                                        (binding.rvItemsFSWF.adapter as? SSItemsAdapter)?.let {
                                            it.dataList[position].isNotified = true
                                            it.notifyItemChanged(position)
                                            fireRemindMeEvent(it.dataList[position])
                                        }
                                        lifecycleScope.launchWhenCreated {
                                            val list = viewModel.getNotifiedItemsAPI()
                                            viewModel.setNotifiedItems(list)
                                        }
                                    }
                                }
                                hideLoader()
                            }
                    }
                }

                R.id.imgTopBannerSWFF -> {
                    CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                        if ((it.bannerObject?.type ?: 1) == 3) {
                            FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.VIDEO_VIEW,
                                Bundle().apply {
                                    putString(AppENUM.VIDEO_URL, it.bannerObject?.url)
                                })?.let { dialog ->
                                dialog.show(parentFragmentManager, dialog.javaClass.name)
                            }
                        } else {
                            if (!it.bannerObject?.deeplink.isNullOrEmpty()) {
                                val intent: Intent =
                                    if (it.bannerObject?.deeplink?.contains("freegarage") == true) {
                                        Intent(requireContext(), DashboardActivity::class.java)
                                    } else {
                                        Intent(Intent.ACTION_VIEW)
                                    }
                                intent.data = Uri.parse(it.bannerObject?.deeplink)
                                startActivity(intent)
                            } else { //do nothing
                            }
                        }
                    }
                }

                R.id.clEnquiryBanner -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SEND_ENQUIRY)
                    )
                }
            }
        }
    }

    private fun cancelCarFilter() {
        binding.rvItemsFSWF.adapter = null
        viewModel.searchAndFilterAttrs.car.find { it.isSelected }?.isSelected = false
        binding.tvFilterViaCars.makeVisible(true)
        binding.tvFilteredCar.makeVisible(false)
        selectCarViewModel.provideSelectedModel().value = null
        selectCarViewModel.provideSelectedBrand().value = null
        binding.rvItemsFSWF.makeVisible(false)
        binding.slItems.makeVisible(true)
        binding.clEmptyListFSWF.makeVisible(false)
        showSelectedCar()
        getProductsList()
    }

    private fun showSelectCarDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_CAR, Bundle().apply {
            putInt(AppENUM.CAR_SEARCH_TYPE, 0)
            putBoolean(AppENUM.IS_FROM_SS_FILTER, true)
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                when (extra as Int) {
                    0 -> { //do nothing
                    }
                    1 -> { //search items via selected car
                        if (viewModel.searchAndFilterAttrs.textSearch.isNotEmpty()) showConfirmationDialog()
                        else addCarFilter()
                    }
                }
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun addCarFilter() = binding.apply {
        selectCarViewModel.provideSelectedModel().value?.carName?.apply {
            val car = viewModel.searchAndFilterAttrs.car.find { it.carName.contains(this) }
            viewModel.searchAndFilterAttrs.car.find { it.isSelected }?.isSelected = false
            car?.isSelected = true
            showSelectedCar(car)
        }

        rvItemsFSWF.makeVisible(false)
        slItems.makeVisible(true)
        clEmptyListFSWF.makeVisible(false)
        isFilterChanged = true

        getProductsList()
    }

    private fun showSelectedCar(car: CarAttr? = null) = binding.apply {
        val selectedCar = car ?: viewModel.searchAndFilterAttrs.car.find { it.isSelected }

        if (selectedCar != null) {
            tvFilterViaCars.makeVisible(false)
            tvFilteredCar.makeVisible(true)
            tvFilteredCar.text = selectedCar.carName
        } else {
            tvFilterViaCars.makeVisible(true)
            tvFilteredCar.makeVisible(false)
        }

        binding.tvSelectedCar.text =
            selectedCar?.carName ?: resources.getString(R.string.select_car)

    }

    private fun showConfirmationDialog() {
        val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    viewModel.searchAndFilterAttrs.textSearch = ""
                    setHeaderText()
                    addCarFilter()
                }
                DialogInterface.BUTTON_NEGATIVE -> {
                    dialog.cancel()
                }
            }
        }

        val builder = AlertDialog.Builder(context)
        builder.setMessage(resources.getString(R.string.clear_warning))
            .setPositiveButton("Proceed", dialogClickListener)
            .setNegativeButton("Cancel", dialogClickListener).show()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        try {
            viewModel.provideItemsWithFilterResponse().postValue(null)
            viewModel.provideAddToCartResponse().postValue(null)
            selectCarViewModel.provideSelectedBrand().value = null
            selectCarViewModel.provideSelectedModel().value = null
            viewModel.cancelJob()
            viewModel.clearMarketplaceFilters()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun fireAddCartEvent(item: SSItemModel) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, item.title)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.POPULAR_TAG, item.tag?.tagName)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_ENQUIRY, item.isEnquiry)
        bundle.putBoolean(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_FASTMOVING,
            item.isfastmoving
        )
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_ID, item.id?.oid ?: "")
        if (item.specifications?.isNotEmpty() == true) {
            bundle.putString(
                FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_BRAND,
                item.specifications[0].value
            )
        }
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
            item.gomPrice.toString()
        )
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_TYPE, item.skuCategory)
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_PLP
        )
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.ADD_TO_CART,
            bundle
        )
    }

    private fun fireViewCartEvent() {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_PLP
        )
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.VIEW_CART,
            bundle
        )
    }

    private fun fireRemindMeEvent(ssItemModel: SSItemModel) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, ssItemModel.title)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_ID, ssItemModel.skuCode)
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.POPULAR_TAG,
            ssItemModel.tag?.tagName
        )
        bundle.putBoolean(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_ENQUIRY,
            ssItemModel.isEnquiry
        )
        bundle.putBoolean(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_FASTMOVING,
            ssItemModel.isfastmoving
        )
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
            ssItemModel.gomPrice.toString()
        )
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_CATEGORY,
            ssItemModel.skuCategory
        )
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_PLP
        )

        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.REMIND_ME,
            bundle
        )
    }


    private fun fireEnquireNowEvent(ssItemModel: SSItemModel) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, ssItemModel.title)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_ID, ssItemModel.skuCode)
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.POPULAR_TAG,
            ssItemModel.tag?.tagName
        )
        bundle.putBoolean(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_ENQUIRY,
            ssItemModel.isEnquiry
        )
        bundle.putBoolean(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_FASTMOVING,
            ssItemModel.isfastmoving
        )
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
            ssItemModel.gomPrice.toString()
        )
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_CATEGORY,
            ssItemModel.skuCategory
        )
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_PLP
        )

        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_ENQUIRE_STOCK,
            bundle
        )
    }

    private fun fireEnquireNowEvent(search: String) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FBE_SEARCH_TERM, search)
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_PLP
        )

        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_ENQUIRE_OOS,
            bundle
        )
    }

}