package whitelisted.garage.view.fragments.workshopBidding

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.BrandWiseItem
import whitelisted.garage.api.request.GenericItem
import whitelisted.garage.api.response.GetInventoryPartItemResponse
import whitelisted.garage.api.response.OrderListResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentWorkshopBidAddPartBinding
import whitelisted.garage.eventBus.GoBackEvent
import whitelisted.garage.eventBus.UpdateCartValueInBidEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.LiveOrderCarAdapter
import whitelisted.garage.view.adapter.WorkMostUsedPartAdapter
import whitelisted.garage.viewmodels.OrderListViewModel
import whitelisted.garage.viewmodels.SelectCarDialogViewModel
import whitelisted.garage.viewmodels.WorkShopBiddingViewModel

class WorkshopBiddingAddPartFragment : BaseFragment() {

    private val binding by viewBinding(FragmentWorkshopBidAddPartBinding::inflate)
    private var mostUsedPartAdapter: WorkMostUsedPartAdapter? = null
    private var carModelIdOfCar: Long = 0
    private var brandIdOfCar: Long = 0
    private var mostUsedPart: MutableList<GenericItem>? = null
    private lateinit var ordersList: MutableList<OrderListResponseModel>
    private val viewModel: WorkShopBiddingViewModel by sharedViewModel()
    private val viewModelOrder: OrderListViewModel by sharedViewModel()
    private val selectCarViewModel: SelectCarDialogViewModel by sharedViewModel()
    private var brandWiseItems: MutableMap<String, MutableList<BrandWiseItem>> = mutableMapOf()
    private var genericItems: MutableList<GenericItem> = mutableListOf()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListener()
        observe()
        getData()
        showLoader()
        viewModel.getWsMostPart()
        viewModelOrder.getOrdersListAPI("0")
    }

    private fun getData() {
        viewModel.getDateYearRange()
    }

    private fun observe() {
        observeYearRange()
        observeMostUsedPart()
        observeCarBrand()
        observeOrderListResponse()
        observeRemovedGenericItem()
    }


    private fun observeRemovedGenericItem() {
        viewModel.provideGenericItemRemovedFromCart().observe(viewLifecycleOwner) { generic ->
            val index = mostUsedPart?.indexOfFirst { it.partName == generic?.partName } ?: 0
            if (index >= 0) {
                mostUsedPart?.get(index)?.isAdded = false
                mostUsedPartAdapter?.notifyItemChanged(index)
            }
        }
    }

    private fun observeOrderListResponse() {
        viewModelOrder.provideOrderListResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    ordersList = it.body.data.ongoingOrders?.toMutableList() ?: mutableListOf()
                    setDataToAdapter(ordersList)
                }
                is Result.Failure -> {
                    binding.llSelectFromLiveOrder.makeVisible(false)
                }
            }
        }
    }

    private fun setDataToAdapter(ordersList: MutableList<OrderListResponseModel>) {
        if (ordersList.isEmpty()) {
            binding.llSelectFromLiveOrder.makeVisible(false)
        } else {
            binding.rvLiveOrderCar.adapter = LiveOrderCarAdapter(ordersList, this)
            binding.llSelectFromLiveOrder.makeVisible(true)
        }
    }

    private fun observeCarBrand() {
        viewModel.provideCarBrand().observe(viewLifecycleOwner) {
            it?.let {
                binding.tvSelectBrand.text = it.brandName
                binding.tvEnterFuelType.text = it.fuelType
                binding.tvSelectModel.text = it.carModel
                carModelIdOfCar = it.carModelId ?: 0
                brandIdOfCar = it.brandId ?: 0
            }
        }
    }

    private fun observeMostUsedPart() {
        viewModel.provideWSMostUsedPart().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.let { data ->
                        setUpMostUsedPartRv(data.genericItems)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
    }

    private fun setUpMostUsedPartRv(genericItems: MutableList<GenericItem>?) {
        genericItems?.let {
            if (it.isEmpty()) {
                binding.llMostUsedPart.makeVisible(false)
            } else {
                binding.llMostUsedPart.makeVisible(true)
                mostUsedPart = it
                setAdapter()
            }
        }
    }

    private fun setAdapter() {

        mostUsedPartAdapter = WorkMostUsedPartAdapter(mostUsedPart ?: mutableListOf(), this).apply {
            binding.rvMostUsedParts.adapter = this
            binding.rvMostUsedParts.setHasFixedSize(true)
        }
    }

    private fun resetAllTheFields() {
        binding.tvSelectBrand.text = getString(R.string.select_brand)
        binding.tvEnterFuelType.text = getString(R.string.enter_fuel_type)
        binding.tvSelectModel.text = getString(R.string.select_model)
        binding.autoTvPartItem.text?.clear()
        binding.autoTvYearMF.text?.clear()
        carModelIdOfCar = 0
        brandIdOfCar = 0
    }

    private fun observeYearRange() {
        viewModel.provideDateYearRange().observe(viewLifecycleOwner) {
            val yearRange = it ?: arrayListOf()
            val adapter = ArrayAdapter(requireContext(),
                R.layout.support_simple_spinner_dropdown_item,
                yearRange)
            binding.autoTvYearMF.setAdapter(adapter)
        }
    }

    private fun clickListener() {
        binding.imgBack.setOnClickListener(this)
        binding.tvSelectModel.setOnClickListener(this)
        binding.tvSelectBrand.setOnClickListener(this)
        binding.tvEnterFuelType.setOnClickListener(this)
        binding.autoTvPartItem.setOnClickListener(this)
        binding.llCart.setOnClickListener(this)
        binding.btnProceed.setOnClickListener(this)
        binding.tvChangeCar.setOnClickListener(this)
    }


    private fun firebaseEventAddProduct(name: String?, brand: String?) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ADD_BIDDING)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PRODUCT_NAME, name)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PRODUCT_BRAND, brand)
            putInt(FirebaseAnalyticsLog.FirebaseEventNameENUM.PRODUCT_QTY, 1)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_BID_ADD_PRODUCT,
                this)
        }
    }

    private fun firebaseEventProceed() {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ADD_BIDDING)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.ITEM_COUNT,
                binding.tvCartCount.text.toString())
            putParcelableArrayList(FirebaseAnalyticsLog.FirebaseEventNameENUM.ITEM_NAME,
                viewModel.getAllBrandWise())
            putParcelableArrayList(FirebaseAnalyticsLog.FirebaseEventNameENUM.ITEM_NAME,
                viewModel.getAllGenericWise())
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_BID_RAISE_ENQUIRY,
                this)
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
            R.id.tvSelectBrand -> showBrandsDialog(0)
            R.id.tvSelectModel -> {
                if (binding.tvSelectBrand.text.toString().trim()
                        .isEmpty() || binding.tvSelectBrand.text == getString(R.string.select_brand)) {
                    CommonUtils.showToast(requireContext(), getString(R.string.select_brand_frist))
                } else {
                    showBrandsDialog(1)
                }
            }
            R.id.tvEnterFuelType -> {
                if (binding.tvSelectModel.text.toString().trim()
                        .isEmpty() || binding.tvSelectModel.text == getString(R.string.select_model)) {
                    CommonUtils.showToast(requireContext(), getString(R.string.select_model_frist))
                } else {
                    showBrandsDialog(2)
                }
            }
            R.id.llCart, R.id.btnProceed -> {
                viewModel.brandWiseItems = brandWiseItems
                viewModel.genericItems = genericItems
                firebaseEventProceed()
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_BIDDING_CART_DETAILS_FRAGMENT,
                        Bundle().apply {
                            putBoolean(AppENUM.IS_VIEW_ONLY, false)
                        }))

            }
            R.id.autoTvPartItem -> {
                if (validateCarDetailsSelection()) {
                    showPartSelectDialog()
                }
            }
            R.id.btnAdd -> {
                CommonUtils.genericCastOrNull<GenericItem>(v.getTag(R.id.model))?.let {
                    CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                        handleAddingMostUsedPart(it, position)
                    }
                }
            }
            R.id.imgPart -> {
                CommonUtils.genericCastOrNull<List<String>>(v.getTag(R.id.model))?.let {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.INVENTORY_IMAGE_PREVIEW,
                            Bundle().apply {
                                this.putStringArrayList(AppENUM.IMAGE_URL, ArrayList(it))
                            }))
                }
            }
            R.id.tvChangeCar -> {
                resetAllTheFields()
            }
            R.id.llLiveOrderCarParent -> {
                CommonUtils.genericCastOrNull<OrderListResponseModel>(v.getTag(R.id.model))?.let {
                    binding.tvChangeCar.makeVisible(true)
                    binding.tvSelectBrand.text = it.brand_name
                    binding.tvEnterFuelType.text = it.fuel_type
                    binding.tvSelectModel.text = it.carName
                    carModelIdOfCar = (it.model_id ?: 0)
                    brandIdOfCar = (it.brand_id ?: 0)
                }
            }
        }
    }

    private fun handleAddingMostUsedPart(generic: GenericItem, position: Int) {
        if (generic.isAdded != true) {
            generic.quantity = 1
            genericItems.add(generic)
            increaseTheCartSize()
            firebaseEventAddProduct(generic.partName, generic.brandName)
        } else {
            val index = genericItems.indexOfFirst { it.partName == generic.partName }
            if (index >= 0) {
                genericItems.removeAt(index)
                updateCartSize()
            }

        }
        mostUsedPartAdapter?.updateAddBtnState(position)
    }

    private fun validateCarDetailsSelection(): Boolean {
        if (binding.tvSelectBrand.text.toString().trim()
                .isEmpty() || binding.tvSelectBrand.text == getString(R.string.select_brand)) {
            CommonUtils.showToast(requireContext(), getString(R.string.select_brand_frist))
            return false
        }
        if (binding.tvSelectModel.text.toString().trim()
                .isEmpty() || binding.tvSelectModel.text == getString(R.string.select_model)) {
            CommonUtils.showToast(requireContext(), getString(R.string.select_model_frist))
            return false
        }
        return true
    }


    private fun increaseTheCartSize() {
        binding.tvCartCount.text = viewModel.getCompleteCartSize(brandWiseItems, genericItems).toString()
        CommonUtils.showToast(requireContext(), getString(R.string.part_add_to_cart))
    }

    private fun updateCartSize() {
        binding.tvCartCount.text = viewModel.getCompleteCartSize(brandWiseItems, genericItems).toString()
    }

    private fun showPartSelectDialog() = binding.apply {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_WORKSHOP_PART_NAME,
            null,
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    (extra as? GetInventoryPartItemResponse)?.let { data ->
                        if (brandWiseItems.contains(tvSelectBrand.text.toString().trim())) {
                            val list = brandWiseItems[tvSelectBrand.text.toString().trim()]
                            list?.add(BrandWiseItem().apply {
                                this.brandName = tvSelectBrand.text.toString().trim()
                                this.fuelType = tvEnterFuelType.text.toString().trim()
                                this.carModel = tvSelectModel.text.toString().trim()
                                this.carModelId = carModelIdOfCar
                                this.brandId = brandIdOfCar
                                this.partName = data.subCategoryName?.trim()
                                this.quantity = 1
                                this.mfYear = autoTvYearMF.text.toString().trim()
                                this.skuId = data.skuId?.toLong()
                                this.buyingPrice = 0
                                brandWiseItems[tvSelectBrand.text.toString()] = list
                            })

                        } else {
                            val list = mutableListOf<BrandWiseItem>()
                            list.add(BrandWiseItem().apply {
                                this.brandName = tvSelectBrand.text.toString().trim()
                                this.fuelType = tvEnterFuelType.text.toString().trim()
                                this.carModel = tvSelectModel.text.toString().trim()
                                this.carModelId = carModelIdOfCar
                                this.brandId = brandIdOfCar
                                this.partName = data.subCategoryName?.trim()
                                this.quantity = 1
                                this.mfYear = autoTvYearMF.text.toString().trim()
                                this.skuId = data.skuId?.toLong()
                                this.buyingPrice = 0
                            })
                            brandWiseItems[tvSelectBrand.text.toString()] = list
                        }
                        increaseTheCartSize()
                        firebaseEventAddProduct(data.subCategoryName?.trim(),
                            tvSelectBrand.text.toString().trim())
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun showBrandsDialog(selection: Int) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_CAR,
            Bundle().apply { putInt(AppENUM.CAR_SEARCH_TYPE, selection) },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    when (extra as? Int) {
                        0 -> { //
                        }
                        1 -> { //
                        }
                        2 -> {
                            binding.tvChangeCar.makeVisible(true)
                            binding.tvSelectModel.text =
                                selectCarViewModel.provideSelectedModel().value?.carName
                            binding.tvSelectBrand.text =
                                selectCarViewModel.provideSelectedBrand().value?.name
                            brandIdOfCar =
                                selectCarViewModel.provideSelectedModel().value?.brandId?.toLong()
                                    ?: 0
                            carModelIdOfCar =
                                selectCarViewModel.provideSelectedModel().value?.modelId?.toLong()
                                    ?: 0
                            binding.tvEnterFuelType.text = ""
                            binding.tvEnterFuelType.text =
                                selectCarViewModel.provideSelectedFuelType().value?.type
                        }
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is GoBackEvent -> {
                popBackStackAndHideKeyboard()
            }
            is UpdateCartValueInBidEvent -> {
                updateCartSize()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModelOrder.provideOrderListResponse().value = null
    }
}