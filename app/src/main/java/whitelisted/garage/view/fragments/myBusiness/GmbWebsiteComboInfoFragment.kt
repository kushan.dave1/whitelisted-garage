package whitelisted.garage.view.fragments.myBusiness

import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.daasuu.bl.ArrowDirection
import com.daasuu.bl.BubbleLayout
import com.daasuu.bl.BubblePopupHelper
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.UnlockGMBRequest
import whitelisted.garage.api.response.GetGMDResponse
import whitelisted.garage.api.response.GetGOCoinOptionsResponse
import whitelisted.garage.api.response.ScreensConfigResponse
import whitelisted.garage.api.response.Value
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.LayoutGmbInfoBinding
import whitelisted.garage.eventBus.IsFromGMBEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeBackgroundDrawable
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.getLocationPointOnScreen
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.GMBInfoDetailsAdapter
import whitelisted.garage.view.adapter.GoCoinsFAQAdapter
import whitelisted.garage.view.adapter.RecyclerProgressAdpater
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.WebsiteInfoViewModel
import kotlin.math.abs

class GmbWebsiteComboInfoFragment : BaseFragment() {
    private var dataSize = 0
    private var goCoinValue = 0
    private var gmbPrice = 0.0
    private var goCoinBalance = 0
    private var couponToBeApplied = ""
    private var couponPrice = 0
    private var screensConfigResponse: ScreensConfigResponse? = null
    private val viewModel: WebsiteInfoViewModel by viewModel()
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var gmbData: GetGMDResponse? = null
    private var isGMBPreviewActive = false
    private lateinit var options: GetGOCoinOptionsResponse
    private var isFAQToggled: Boolean = true
    private var isLoadingFirst: Boolean = true
    private var balanceGoCoins: Double = -1.0
    private val binding by viewBinding(LayoutGmbInfoBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListener()
        observeResponse()
        setUpScreenData()
        showLoader()
        viewModel.getGMB()
        viewModel.getGoCoinsOptions()
        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            getString(R.string.currency_symbol_default)).let {
            binding.tvx.text = it
            binding.tvCurrency0.text = it
            binding.tvCurrency1.text = it
            binding.tvCurrency2.text = it
            binding.tvCurrency3.text = it
        }
    }

    private fun setUpScreenData() {
        goCoinValue = if (viewModel.getIntSharedPreference(AppENUM.GO_COINS_VALUE) == 0) {
            1
        } else {
            viewModel.getIntSharedPreference(AppENUM.GO_COINS_VALUE)
        }
        binding.llScroll.makeVisible(false)
        binding.llGoogleUnlock.makeVisible(false)
        binding.animationView.imageAssetsFolder = "gmb_lotty_images/"
        binding.animationView.setAnimation(R.raw.data)
        setUpRvToInfo()
        setGmbWebsitePreviewBtState(isGMBPreviewActive)
    }

    private fun setGmbWebsitePreviewBtState(isGmbActive: Boolean) {
        if (isGmbActive) {
            isGMBPreviewActive = false
            binding.tvPreviewDesc.text = getString(R.string.see_how_your_google_page_will_look_like)
            binding.tvBtnWebsite.background = null
            binding.tvBtnWebsite.changeTextColor(requireContext(), R.color.black_trans_new_3)
            binding.tvBtnGooglePage.changeTextColor(requireContext(), R.color.white)
            binding.tvBtnGooglePage.changeBackgroundDrawable(requireContext(),
                R.drawable.bg_default_button_r50)
            binding.imgPreviewCombo.setImageResource(R.drawable.ic_preview_image_google)
        } else {
            binding.tvPreviewDesc.text = getString(R.string.see_how_your_website_will_look_like)
            isGMBPreviewActive = true
            binding.tvBtnWebsite.changeBackgroundDrawable(requireContext(),
                R.drawable.bg_default_button_r50)
            binding.tvBtnGooglePage.background = null
            binding.tvBtnGooglePage.changeTextColor(requireContext(), R.color.black_trans_new_3)
            binding.tvBtnWebsite.changeTextColor(requireContext(), R.color.white)
            binding.imgPreviewCombo.setImageResource(R.drawable.ic_preview_image)
        }
    }

    private fun observeResponse() {
        observeGetGMD()
        observeGetGoCoinOptions()
    }

    private fun clickListener() {
        binding.imgBack.setOnClickListener(this)
        binding.btnLockGMB.setOnClickListener(this)
        binding.btnBuy.setOnClickListener(this)
        binding.llOption1.setOnClickListener(this)
        binding.llOption2.setOnClickListener(this)
        binding.llOption3.setOnClickListener(this)
        binding.imageLowCoin.setOnClickListener(this)
        binding.llPreviewLayout.setOnClickListener(this)
        binding.llApplyCoupon.setOnClickListener(this)
        binding.tvRemoveApplyedCoupon.setOnClickListener(this)
        binding.tvBtnWebsite.setOnClickListener(this)
        binding.tvBtnGooglePage.setOnClickListener(this)
        binding.tvPreviewBtn.setOnClickListener(this)
        binding.llFAQ.imgToggle.setOnClickListener(this)

        etPriceTextWatcher()
    }

    private fun etPriceTextWatcher() {
        binding.etPrice.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?,
                start: Int,
                count: Int,
                after: Int) { // nothing
            }

            override fun onTextChanged(s: CharSequence?,
                start: Int,
                before: Int,
                count: Int) { // nothing
            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString().trim().isNotEmpty()) {
                    val coinsValue =
                        binding.etPrice.text.toString().trim().toDouble().times(goCoinValue).toInt()
                    binding.tvCoinGet.setText(coinsValue.toString())
                } else {
                    binding.tvCoinGet.setText("")
                }
                disableAllOptions()
            }
        })
    }

    private fun disableAllOptions() {
        binding.tvCurrency1.changeTextColor(requireContext(), R.color.tab_gray)
        binding.tvCurrency2.changeTextColor(requireContext(), R.color.tab_gray)
        binding.tvCurrency3.changeTextColor(requireContext(), R.color.tab_gray)
        binding.tvOption2.changeTextColor(requireContext(), R.color.tab_gray)
        binding.llOption2.changeBackgroundDrawable(requireContext(),
            R.drawable.bg_go_coins_options_nor)
        binding.tvOption1.changeTextColor(requireContext(), R.color.tab_gray)
        binding.llOption1.changeBackgroundDrawable(requireContext(),
            R.drawable.bg_go_coins_options_nor)
        binding.tvOption3.changeTextColor(requireContext(), R.color.tab_gray)
        binding.llOption3.changeBackgroundDrawable(requireContext(),
            R.drawable.bg_go_coins_options_nor)
    }

    private fun enableDisableViews(id: Int) {
        when (id) {
            R.id.llOption1 -> {
                binding.etPrice.setText(binding.tvOption1.text.toString())
                binding.tvOption1.changeTextColor(requireContext(), R.color.counter_bg)
                binding.tvCurrency1.changeTextColor(requireContext(), R.color.counter_bg)
                binding.llOption1.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_popular)
                binding.tvOption2.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvOption3.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvCurrency2.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvCurrency3.changeTextColor(requireContext(), R.color.tab_gray)
                binding.llOption2.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_nor)
                binding.llOption3.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_nor)
            }
            R.id.llOption2 -> {
                binding.etPrice.setText(binding.tvOption2.text.toString())
                binding.tvOption2.changeTextColor(requireContext(), R.color.counter_bg)
                binding.tvCurrency2.changeTextColor(requireContext(), R.color.counter_bg)
                binding.llOption2.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_popular)
                binding.tvCurrency1.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvCurrency3.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvOption1.changeTextColor(requireContext(), R.color.tab_gray)
                binding.llOption1.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_nor)
                binding.tvOption3.changeTextColor(requireContext(), R.color.tab_gray)
                binding.llOption3.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_nor)
            }
            R.id.llOption3 -> {
                binding.etPrice.setText(binding.tvOption3.text.toString())
                binding.tvOption3.changeTextColor(requireContext(), R.color.counter_bg)
                binding.tvCurrency3.changeTextColor(requireContext(), R.color.counter_bg)
                binding.llOption3.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_popular)
                binding.tvCurrency1.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvCurrency2.changeTextColor(requireContext(), R.color.tab_gray)
                binding.tvOption2.changeTextColor(requireContext(), R.color.tab_gray)
                binding.llOption2.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_nor)
                binding.tvOption1.changeTextColor(requireContext(), R.color.tab_gray)
                binding.llOption1.changeBackgroundDrawable(requireContext(),
                    R.drawable.bg_go_coins_options_nor)
            }
        }
        binding.etPrice.text?.length?.let { binding.etPrice.setSelection(it) }
    }

    private fun observeGetGoCoinOptions() {
        viewModel.provideGetGoCoinOptions().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    options = it.body.data
                    setDataToOptions(options)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setDataToOptions(options: GetGOCoinOptionsResponse) {
        if ((options.gocoinsOptions?.size ?: 0) >= 3) {
            if (balanceGoCoins == -1.0) binding.tvOption1.text =
                (options.gocoinsOptions?.get(0) ?: 0).toString()
            binding.tvOption2.text = (options.gocoinsOptions?.get(1) ?: 0).toString()
            binding.tvOption3.text = (options.gocoinsOptions?.get(2) ?: 0).toString()
            when {
                options.defaultSelect.toString() == options.gocoinsOptions?.get(0) -> {
                    binding.tvPop1.makeVisible(true)
                }
                options.defaultSelect.toString() == options.gocoinsOptions?.get(1) -> {
                    binding.tvPop2.makeVisible(true)
                }
                options.defaultSelect.toString() == options.gocoinsOptions?.get(2) -> {
                    binding.tvPop3.makeVisible(true)
                }
            }
        }
    }

    private fun observeGetGMD() {
        viewModel.provideGMD().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    gmbData = it.body.data
                    setupDataOfLockScreen(isInitial = true)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

        viewModel.provideGMDAfterPayment().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    gmbData = it.body.data
                    setupDataOfLockScreen()
                    autoUnLockTheGCB()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

        viewModel.provideUnLockGMB().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    fireBaseEvent()
                    openUnlockGMB()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun openUnlockGMB() {
        popBackStackAndHideKeyboard()
        CommonUtils.addFragmentUtil(requireActivity(),
            FragmentFactory.fragment(FragmentFactory.Fragments.GMB_UnLock_Fragment, null))
    }

    private fun setupDataOfLockScreen(isInitial: Boolean = false) {
        gmbData?.let { gmbData ->
            if (gmbData.gocoin_unlocked_features.isNullOrEmpty() || gmbData.gocoin_unlocked_features?.contains(
                    AppENUM.GMB) == false) {
                binding.llScroll.makeVisible(true)
                binding.llGoogleUnlock.makeVisible(true)
                setGoCoinState(gmbData)
            } else {
                if (isInitial) {
                    binding.llScroll.makeVisible(true)
                    binding.llGoogleUnlock.makeVisible(true)
                    binding.llPurchaseLayout.makeVisible(false)
                    binding.imgLock.makeVisible(false)
                    binding.tvUnLockText.text = getString(R.string.manage)
                } else {
                    binding.llScroll.makeVisible(false)
                    binding.llGoogleUnlock.makeVisible(false)
                    popBackStackAndHideKeyboard()
                    openGMB(gmbData)
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setGoCoinState(gmbData: GetGMDResponse) {
        gmbPrice = (gmbData.gmbPrice ?: "0").toDouble().div(goCoinValue)
        goCoinBalance = (gmbData.goCoinsBalance ?: 0).div(goCoinValue)
        binding.tvGoCoinBalanceInCurrency.text = (goCoinBalance).toString()
        binding.tvGMBPrice.text = (gmbPrice - couponPrice).toString()
        binding.tvDisCounted.text = gmbPrice.toString()
        binding.tvDisCounted.paintFlags =
            binding.tvDisCounted.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        binding.tvUnLockText.text = getString(R.string.unlock_using) + " ${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }${gmbPrice - couponPrice} "
        handleGoCoinBuyState()
    }

    private fun handleGoCoinBuyState() {
        if (gmbPrice > (goCoinBalance + couponPrice)) {
            binding.layoutLowBalance.makeVisible(true)
            binding.btnLockGMB.makeVisible(false)
            binding.btnBuy.makeVisible(true)
            binding.imageLowCoin.makeVisible(true)
            val balanceCoins = (gmbPrice - goCoinBalance - couponPrice)
            balanceGoCoins = balanceCoins
            val value = (gmbPrice - goCoinBalance.toDouble() - couponPrice)
            binding.etPrice.setText(value.toString())
            binding.tvOption1.text = balanceCoins.toString()
        } else {
            binding.layoutLowBalance.makeVisible(false)
            binding.btnBuy.makeVisible(false)
            binding.imageLowCoin.makeVisible(false)
            binding.btnLockGMB.makeVisible(true)
        }
    }

    private fun autoUnLockTheGCB() {
        if (gmbPrice <= (goCoinBalance + couponPrice)) {
            showLoader()
            val unlockGMBRequest = UnlockGMBRequest().apply {
                value = (gmbPrice - couponPrice).times(goCoinValue)
                coupon = couponToBeApplied
            }
            viewModel.unLockGMB(unlockGMBRequest)
            firebaseEventForUnlockGMBCombo((gmbPrice - couponPrice).times(goCoinValue),
                couponToBeApplied)
        }
    }

    private fun openGMB(gmbData: GetGMDResponse) {
        if (gmbData.gmb_updated == true) {
            CommonUtils.addFragmentUtil(
                requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.VIEW_GMB_FRAGMENT, null),
            )
        } else {
            CommonUtils.addFragmentUtil(
                requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.EDIT_GMB_FRAGMENT, null),
            )
        }
    }

    @SuppressLint("InflateParams")
    private fun openLowGoCoinAlert(v: View) {
        if (isAdded) {
            val bubbleLayout = LayoutInflater.from(requireContext())
                .inflate(R.layout.layout_low_cpinn_popup, null) as BubbleLayout
            (bubbleLayout.findViewById<AppCompatTextView>(R.id.tvGoCoinsToBbeAdded)).apply {
                text = getString(R.string.gocoins)
            }
            val popupWindow = BubblePopupHelper.create(requireContext(), bubbleLayout)
            bubbleLayout.arrowDirection = ArrowDirection.TOP_RIGHT
            popupWindow.showAtLocation(v,
                Gravity.NO_GRAVITY,
                v.getLocationPointOnScreen().x - 110,
                v.getLocationPointOnScreen().y + 40)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
            R.id.btnLockGMB -> {
                gmbData?.let { gmbData ->
                    if (gmbData.gocoin_unlocked_features.isNullOrEmpty() || gmbData.gocoin_unlocked_features?.contains(
                            AppENUM.GMB) == false) {
                        showLoader()
                        viewModel.unLockGMB(UnlockGMBRequest().apply {
                            this.value = (gmbPrice - couponPrice).times(goCoinValue)
                            this.coupon = couponToBeApplied
                        })
                        firebaseEventForUnlockGMBCombo((gmbPrice - couponPrice).times(goCoinValue),
                            couponToBeApplied)
                    } else {
                        binding.llScroll.makeVisible(false)
                        binding.llGoogleUnlock.makeVisible(false)
                        popBackStackAndHideKeyboard()
                        openGMB(gmbData)
                    }
                }
            }
            R.id.btnBuy -> {
                if (binding.tvCoinGet.text.isNullOrEmpty() || binding.tvCoinGet.text.toString()
                        .toIntOrNull() == 0) {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.plesae_enter_the_valid_go_coin))
                } else {
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_GO_COIN,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_G_COMBO_INFO)
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_G_COMBO_PAYMENT,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_G_COMBO_INFO)
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS,
                            Bundle().apply {
                                putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                                putString(AppENUM.IS_FROM,
                                    AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_GMB)
                                putString(AppENUM.AMOUNT_TO_BE_PURCHASED,
                                    (binding.etPrice.text.toString()))
                                putString(AppENUM.GO_COIN_TO_GET,
                                    (binding.tvCoinGet.text.toString()))
                            }),
                    )
                }
            }

            R.id.tvBtnGooglePage, R.id.tvBtnWebsite -> {
                setGmbWebsitePreviewBtState(isGMBPreviewActive)
            }
            R.id.llOption1 -> {

                enableDisableViews(R.id.llOption1)
            }
            R.id.llOption2 -> {
                enableDisableViews(R.id.llOption2)
            }
            R.id.llOption3 -> {
                enableDisableViews(R.id.llOption3)
            }
            R.id.imageLowCoin -> {
                openLowGoCoinAlert(binding.tv)
            }

            R.id.tvPreviewBtn -> {
                CommonUtils.addFragmentUtil(
                    requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.GMB_PREVIEW, Bundle().apply {
                        if (isGMBPreviewActive) {
                            putParcelable(AppENUM.GMB, screensConfigResponse?.gmbWebsite?.preview)
                            putBoolean(AppENUM.IS_FROM_GMB, false)
                        } else {
                            putParcelable(AppENUM.GMB, screensConfigResponse?.gMB?.preview)
                            putBoolean(AppENUM.IS_FROM_GMB, true)

                        }
                    }),
                )
            }
            R.id.clBase -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.model))?.let { position ->
                    (binding.llFAQ.rvGoCoinsFAQ.adapter as GoCoinsFAQAdapter).dataList[position].isSelected =
                        (binding.llFAQ.rvGoCoinsFAQ.adapter as GoCoinsFAQAdapter).dataList[position].isSelected != true
                    (binding.llFAQ.rvGoCoinsFAQ.adapter as GoCoinsFAQAdapter).notifyItemChanged(
                        position)
                }
            }
            R.id.tvRemoveApplyedCoupon -> {
                binding.tvUnLockText.text = getString(R.string.unlock_using) + " ${
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default))
                }${gmbPrice - couponPrice} "
                couponToBeApplied = ""
                binding.tvGMBPrice.text = gmbPrice.toString()
                binding.tvDisCounted.makeVisible(false)
                couponPrice = 0
                binding.llCodeApplyed.makeVisible(false)
                binding.llApplyCoupon.makeVisible(true)
                binding.tvCoinGet.setText((gmbPrice - goCoinBalance - couponPrice).times(goCoinValue)
                    .toString())
                handleGoCoinBuyState()
            }
            R.id.imgToggle -> {
                if (isFAQToggled) {
                    isFAQToggled = false
                    binding.llFAQ.imgToggle.setImageResource(R.drawable.ic_arrow_up_black)
                    binding.llFAQ.rvGoCoinsFAQ.makeVisible(true)
                    binding.llFAQ.rvGoCoinsFAQ.smoothScrollToPosition(View.FOCUS_DOWN)
                    binding.llScroll.post {
                        binding.llScroll.fullScroll(View.FOCUS_DOWN)
                    }
                } else {
                    isFAQToggled = true
                    binding.llFAQ.imgToggle.setImageResource(R.drawable.ic_arrow_down_black)
                    binding.llFAQ.rvGoCoinsFAQ.makeVisible(false)
                }
            }
            R.id.llApplyCoupon -> {
                FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.APPLY_COUPON_DIALOG,
                    Bundle().apply {
                        this.putString(AppENUM.IS_GMB, AppENUM.FilterString.GMB)
                    },
                    object : ActionListener {
                        override fun onActionItem(extra: Any?, extra2: Any?) {
                            when (extra as Int) {
                                0 -> {
                                    val data = extra2 as? Value
                                    couponToBeApplied = data?.coupon ?: ""
                                    binding.llCodeApplyed.makeVisible(true)
                                    binding.llApplyCoupon.makeVisible(false)
                                    binding.tvApplyCouponText.text =
                                        "'${data?.coupon}' " + getString(R.string.applied)

                                    couponPrice = (data?.couponValue ?: 0).div(goCoinValue)
                                    binding.tvApplyCouponValue.text = couponPrice.toString()
                                    binding.tvDisCounted.makeVisible(true)
                                    binding.tvGMBPrice.text = (gmbPrice - couponPrice).toString()
                                    binding.tvUnLockText.text =
                                        getString(R.string.unlock_using) + " ${
                                            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                                                getString(R.string.currency_symbol_default))
                                        }${(gmbPrice - couponPrice)} "
                                    binding.tvCoinGet.setText((gmbPrice - goCoinBalance - couponPrice).times(
                                        goCoinValue).toString())
                                    handleGoCoinBuyState()

                                    Bundle().apply {
                                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                                            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_G_COMBO_INFO)
                                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.COUPON_VALUE,
                                            couponPrice.toString())
                                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.COUPON,
                                            data?.coupon)
                                        FirebaseAnalyticsLog.trackFireBaseEventLog(
                                            FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_APPLY_COUPON,
                                            this)
                                    }
                                }
                            }
                        }
                    })?.let { dialog ->
                    dialog.show(parentFragmentManager, dialog.javaClass.name)
                }
            }
        }
    }

    private fun firebaseEventForUnlockGMBCombo(times: Double, couponValue: String) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_G_COMBO_INFO)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.GC_ADDED, times.toString())
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.COUPON_VALUE, couponValue)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_UNLOCK_G_COMBO,
                this)
        }
    }

    private fun setUpRvToInfo() {
        this.screensConfigResponse = dashboardViewModel.screensConfigResponse?.value
        binding.rvGMBDetails.clipToPadding = false
        binding.rvGMBDetails.clipChildren = false
        binding.rvGMBDetails.offscreenPageLimit = 3

        val compositePageTransformer = CompositePageTransformer()
        compositePageTransformer.addTransformer(MarginPageTransformer(64))
        compositePageTransformer.addTransformer { page, position ->
            val r = 1 - abs(position)
            page.scaleY = .85f + r * .15f
        }
        binding.rvGMBDetails.setPageTransformer(compositePageTransformer)
        ViewCompat.setNestedScrollingEnabled(binding.rvGMBDetails, false)

        screensConfigResponse?.let { response ->

            response.gmbWebsiteCombo?.let { gmbWebsiteCombo ->

                gmbWebsiteCombo.multiImage?.let { multiImage ->
                    binding.rvGMBDetails.adapter = GMBInfoDetailsAdapter(multiImage)
                    binding.rvGMBDetails.setCurrentItem(1, false)
                    val data = mutableListOf<Boolean>()
                    for (i in multiImage.indices) {
                        data.add(false)
                    }
                    data[0] = true
                    binding.progressRecycler.adapter = RecyclerProgressAdpater(data)
                    dataSize = data.size
                    registeredOnPageScroll(multiImage.size + 2)
                }
                binding.tvHeader.text = gmbWebsiteCombo.header
                binding.tvHeaderDec.text = gmbWebsiteCombo.description
                binding.tvBannerHeading.text = gmbWebsiteCombo.combo_benifit_header
                binding.tvBannerDEC.text = gmbWebsiteCombo.combo_benifit_description
                binding.llFAQ.rvGoCoinsFAQ.layoutManager = LinearLayoutManager(requireContext())
                binding.llFAQ.rvGoCoinsFAQ.adapter =
                    GoCoinsFAQAdapter(gmbWebsiteCombo.goCoinFaqs?.toMutableList()
                        ?: mutableListOf(), this)
                binding.llFAQ.rvGoCoinsFAQ.makeVisible(false)

            }
        }
    }

    private fun registeredOnPageScroll(size: Int) {
        binding.rvGMBDetails.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageScrolled(position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)

                if (!isLoadingFirst) {
                    val newData = mutableListOf<Boolean>()
                    for (i in 0 until dataSize) {
                        if (i == (position - 1)) {
                            newData.add(i, true)
                        } else {
                            newData.add(i, false)
                        }
                    }
                    (binding.progressRecycler.adapter as? RecyclerProgressAdpater)?.setData(newData)
                }
                isLoadingFirst = false
            }

            override fun onPageScrollStateChanged(state: Int) {
                super.onPageScrollStateChanged(state)

                if (state == ViewPager2.SCROLL_STATE_IDLE) {
                    when (binding.rvGMBDetails.currentItem) {
                        size - 1 -> binding.rvGMBDetails.setCurrentItem(1, false)
                        0 -> binding.rvGMBDetails.setCurrentItem(size - 2, false)
                    }
                }
            }
        })
    }

    private fun fireBaseEvent() {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_G_COMBO_INFO)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.G_COMBO_AMOUNT,
                (gmbPrice - couponPrice).toString())
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_PURCHASE_G_COMBO,
                this)
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is IsFromGMBEvent -> {
                showLoader()
                viewModel.getGMBAfterPayment()
            }
        }
    }
}





