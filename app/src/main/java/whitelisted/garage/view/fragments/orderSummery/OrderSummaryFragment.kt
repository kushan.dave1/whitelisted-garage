package whitelisted.garage.view.fragments.orderSummery

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.OrderSummaryResponse
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmenOrderSummaryBinding
import whitelisted.garage.eventBus.RefreshOrderSummaryEvent
import whitelisted.garage.eventBus.UpdateHomeScreenEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.changeViewCompoundDrawablesWithInterinsicBounds
import whitelisted.garage.viewmodels.OrderSummaryFragmentViewModel

class OrderSummaryFragment : BaseFragment() {

    private val binding by viewBinding(FragmenOrderSummaryBinding::inflate)
    private val viewModel: OrderSummaryFragmentViewModel by viewModel()
    private var responseData: OrderSummaryResponse? = null
    private lateinit var orderId: String
    private var isNewOrder = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        setClickListeners()
        observeData()

        if (arguments?.getBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, false) == true) isNewOrder =
            true
        if (!isNewOrder) {
            orderId = arguments?.getString(AppENUM.IntentKeysENUM.ORDER_ID, "") ?: ""
            showLoader()
            viewModel.getOrderSummary(orderId)
        } else {
            setNewOrderUI()
        }

        binding.srlOSF.setOnRefreshListener {
            binding.srlOSF.isRefreshing = false
            if (::orderId.isInitialized) {
                showLoader()
                viewModel.getOrderSummary(orderId)
            }
        }

        "${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }0.0".apply {
            binding.tvEstimateAmount.text = this
        }
    }

    private fun setNewOrderUI() = binding.apply {
        imgCheckCircle.setImageDrawable(ContextCompat.getDrawable(requireContext(),
            R.drawable.ic_error))
        tvOrderId.text = "-"
        tvCarDetail.text = "-"
        tvCustomerName.text = "-"
        tvPhoneNumber.text = "-"
        binding.layoutStep.dot1.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_dot_orange)
        binding.layoutStep.step1View.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_step_grey)
        binding.layoutStep.dot2.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_dot_grey)
        binding.layoutStep.dot4.background = ContextCompat.getDrawable(requireContext(),
            R.drawable.bg_dot_grey) //dot4 when jc removed
        binding.layoutStep.step2View.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_step_grey)
        btnInventoryEdit.background = ContextCompat.getDrawable(requireContext(),
            R.drawable.bg_button_disabled) //        btnJobCardEdit.background =
        //            ContextCompat.getDrawable(requireContext(), R.drawable.bg_button_disabled)
        btnOrderEstEdit.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.bg_button_disabled)
        btnInventoryEdit.isEnabled = false
        btnOrderEstEdit.isEnabled = false
        tvOrderDetailsEdit.text = getString(R.string.add)
        tvOrderDetailsEdit.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_add_white), null, null, null)
    }

    private fun observeData() {
        viewModel.provideOrderDetailResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) setData(it.body.data[0])
                    responseData = it.body.data[0]
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

        viewModel.provideCompletePaymentResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                    popBackStackAndHideKeyboard()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setData(orderSummaryResponse: OrderSummaryResponse) = binding.apply {
        tvOrderId.text = orderSummaryResponse.orderId
        tvCarDetail.text = orderSummaryResponse.carDetail
        tvCustomerName.text = orderSummaryResponse.customerName
        tvPhoneNumber.text = orderSummaryResponse.phone

        imgCheckCircle.setImageDrawable(ContextCompat.getDrawable(requireContext(),
            R.drawable.ic_check_circle))
        binding.layoutStep.dot1.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_dot_green)
        binding.layoutStep.step1View.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_step_green)
        binding.layoutStep.dot2.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_dot_orange)
        binding.layoutStep.dot4.background = ContextCompat.getDrawable(requireContext(),
            R.drawable.bg_dot_orange) //dot4 when jc removed
        binding.layoutStep.step2View.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.bg_step_orange)
        btnInventoryEdit.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.bg_default_button)
        btnJobCardEdit.background = ContextCompat.getDrawable(requireContext(),
            R.drawable.bg_default_button) //        btnInventoryEdit.isEnabled = false
        //        btnJobCardEdit.isEnabled = false
        tvOrderDetailsEdit.text = getString(R.string.edit)
        tvOrderDetailsEdit.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_edit), null, null, null)

        //        if (orderSummaryResponse.fuelMeter != null) {
        //            btnInventoryEdit.isEnabled = true
        //            btnJobCardEdit.isEnabled = true
        //            dot2.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_dot_green)
        //            Glide.with(requireContext()).load(R.drawable.ic_check_circle)
        //                .into(imgInventoryCheckCircle)
        //            tvFuelPercentage.text =
        //                "${orderSummaryResponse.fuelMeter}% ${getString(R.string.fuel_is_available)}"
        //            llInventoryDateAndTime.visibility = View.VISIBLE
        //            tvInventoryDate.text =
        //                CommonUtils.convertToDateWithoutUTC(orderSummaryResponse.inventoryDate)
        //            tvInventoryTime.text =
        //                CommonUtils.convertToTimeWithoutUTC(orderSummaryResponse.inventoryDate)
        //        }


        //        if (orderSummaryResponse.packages != null && orderSummaryResponse.packages?.isNotEmpty() == true) {
        //            orderSummaryResponse.jcDate = orderSummaryResponse.orderDate
        //        }
        //        if (orderSummaryResponse.servicesCount != null && orderSummaryResponse.servicesCount ?: 0 > 0) {
        //            orderSummaryResponse.jcDate = orderSummaryResponse.orderDate
        //        }
        //        if (orderSummaryResponse.jcDate != null) {
        //            btnJobCardEdit.isEnabled = false
        //            btnJobCardEdit.background =
        //                ContextCompat.getDrawable(requireContext(), R.drawable.bg_button_disabled)
        //
        //            Glide.with(requireContext()).load(R.drawable.ic_check_circle)
        //                .into(imgJobCardCheckCircle)
        //            Glide.with(requireContext())
        //                .load(R.drawable.bg_step_green)
        //                .into(step2View)
        //            dot3.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_dot_green)
        //            Glide.with(requireContext())
        //                .load(R.drawable.bg_step_orange)
        //                .into(step3View)
        //            dot4.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_dot_orange)
        //
        //            //jc card data set
        //            var jcText = ""
        //            if (orderSummaryResponse.packages == null)
        //                orderSummaryResponse.packages = mutableListOf()
        //            if (orderSummaryResponse.servicesCount == null)
        //                orderSummaryResponse.servicesCount = 0
        //            orderSummaryResponse.packages?.forEach {
        //                jcText += ",$it"
        //            }
        //            if (jcText.isNotEmpty())
        //                jcText = jcText.substring(1)
        //
        //            if (orderSummaryResponse.servicesCount ?: 0 > 0) {
        //                if (orderSummaryResponse.packages?.isNotEmpty() == true) {
        //                    jcText += " and ${orderSummaryResponse.servicesCount} ${getString(R.string.other_services_added)}"
        //                } else {
        //                    if (orderSummaryResponse.servicesCount ?: 0 > 1) {
        //                        jcText =
        //                            "${orderSummaryResponse.serviceNameList?.get(0)} and ${orderSummaryResponse.servicesCount ?: 0 - 1} ${
        //                                getString(
        //                                    R.string.other_services_added
        //                                )
        //                            }"
        //                    } else {
        //                        jcText =
        //                            "${orderSummaryResponse.serviceNameList?.get(0)} ${getString(R.string.added)}"
        //                    }
        //                }
        //            } else jcText += " ${getString(R.string.added)}"
        //
        //            tvJobCardStatus.text = jcText
        //            llJobCardDateAndTime.visibility = View.VISIBLE
        //            tvJobCardDate.text = CommonUtils.convertToDateWithoutUTC(orderSummaryResponse.jcDate)
        //            tvJobCardTime.text = CommonUtils.convertToTimeWithoutUTC(orderSummaryResponse.jcDate)
        //
        //            //estimates card data set
        //            llOrderEstimate.visibility = View.VISIBLE
        //            tvEstimatesEditTime.text =
        //                CommonUtils.convertToTimeWithoutUTC(orderSummaryResponse.jcDate)
        //        }

        if (orderSummaryResponse.eTDDate != null) {
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_check_circle),
                imgOrderEstimateCheckCircle) //step3View when jc enabled

            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.bg_step_green), binding.layoutStep.step2View) //step3View when jc enabled
            binding.layoutStep.dot4.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_dot_green)
            tvOrdEstEdit.text = getString(R.string.view)
            tvOrdEstEdit.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)

            llOrderEstimate.visibility = View.VISIBLE
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.bg_step_orange), binding.layoutStep.step4View) //step3View when jc enabled

            binding.layoutStep.dot5.background = ContextCompat.getDrawable(requireContext(), R.drawable.bg_dot_orange)

            //oe data set
            "${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    getString(R.string.currency_symbol_default))
            }${orderSummaryResponse.estimateAmount}".apply {
                tvEstimateAmount.text = this
            }
            tvEstimatesEditTime.text = CommonUtils.convertToTime(orderSummaryResponse.eTDDate)
            llOrderEstDateAndTime.visibility = View.VISIBLE
            tvOrderEstDate.text = CommonUtils.convertToDate(orderSummaryResponse.eTDDate)
            tvOrderEstTime.text = CommonUtils.convertToTime(orderSummaryResponse.eTDDate)

            //payments data set
            llPayment.visibility = View.VISIBLE
        }

        if (orderSummaryResponse.paymentPaid == null) orderSummaryResponse.paymentPaid = 0.0
        if ((orderSummaryResponse.paymentPaid ?: 0.0) > 0) {
            tvOrderDetailsEdit.text = getString(R.string.view)
            tvOrdEstEdit.text = getString(R.string.view)
            tvOrderDetailsEdit.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
            tvOrdEstEdit.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.bg_step_green), binding.layoutStep.step4View)

            "${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    getString(R.string.currency_symbol_default))
            }${orderSummaryResponse.paymentPaid}".apply {
                tvPaymentReceived.text = this
            }
            tvPaymentStatus.text = getString(R.string.ready_for_delivery)
            if ((orderSummaryResponse.paymentPaid?.plus(orderSummaryResponse.discount ?: 0.0)
                    ?: 0.0) >= (orderSummaryResponse.estimateAmount ?: 0.0)) {

                ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                    R.drawable.ic_check_circle), imgPaymentCheckCircle)
                tvPaymentEdit.changeViewCompoundDrawablesWithInterinsicBounds(null,
                    null,
                    null,
                    null)
                tvPaymentEdit.text = getString(R.string.view)

                binding.layoutStep.dot5.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_dot_green)
                llMarkComplete.visibility = View.VISIBLE
            }
            if (orderSummaryResponse.statusId == 150) {
                tvPaymentStatus.text = getString(R.string.completed)
                llMarkComplete.visibility = View.GONE
            }
        }
    }


    private fun setClickListeners() {
        binding.imgBack.setOnClickListener(this)
        binding.btnOrderDetailsEdit.setOnClickListener(this)
        binding.btnInventoryEdit.setOnClickListener(this)
        binding.btnJobCardEdit.setOnClickListener(this)
        binding.btnOrderEstEdit.setOnClickListener(this)
        binding.btnPaymentsEdit.setOnClickListener(this)
        binding.btnMarkComplete.setOnClickListener(this)
        binding.llOrderDetails.setOnClickListener(this)
        binding.llOrderEstimate.setOnClickListener(this)
        binding.llPayment.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()

            R.id.btnOrderDetailsEdit, R.id.llOrderDetails -> { //                popBackStackAndHideKeyboard()
                if (isNewOrder) {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.NEW_ORDER_DETAIL, null))
                } else {
                    if (::orderId.isInitialized) {
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                                Bundle().apply {
                                    putString(AppENUM.IntentKeysENUM.ORDER_ID, orderId)
                                }),
                            target = R.id.fragment_container_2)
                    }
                }
            }

            R.id.btnInventoryEdit -> { //                popBackStackAndHideKeyboard()
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.ORDER_ID, orderId)
                            putInt(AppENUM.IntentKeysENUM.SELECT_TAB, 0)
                        }),
                    target = R.id.fragment_container_2)
            }

            R.id.btnJobCardEdit -> { //                popBackStackAndHideKeyboard()
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.ORDER_ID, orderId)
                            putInt(AppENUM.IntentKeysENUM.SELECT_TAB, 1)
                        }),
                    target = R.id.fragment_container_2)
            }

            R.id.btnOrderEstEdit, R.id.llOrderEstimate -> { //                popBackStackAndHideKeyboard()
                if (::orderId.isInitialized) {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                            Bundle().apply {
                                putString(AppENUM.IntentKeysENUM.ORDER_ID, orderId)
                                putInt(AppENUM.IntentKeysENUM.SELECT_TAB, 1)
                            }))
                }
            }

            R.id.btnPaymentsEdit, R.id.llPayment -> { //                popBackStackAndHideKeyboard()
                if (::orderId.isInitialized) {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                            Bundle().apply {
                                putString(AppENUM.IntentKeysENUM.ORDER_ID, orderId)
                                putInt(AppENUM.IntentKeysENUM.SELECT_TAB, 2)
                            }))
                }
            }

            R.id.btnMarkComplete -> {
                responseData?.let { firebaseMarkCompleteOrderEvent(it) }
                showLoader()
                viewModel.markComplete(orderId)
            }
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is RefreshOrderSummaryEvent -> {
                if (isResumed) refreshItself(event.str)
            }
        }
    }

    private fun refreshItself(ordId: String) {
        showLoader()
        if (::orderId.isInitialized) {
            viewModel.getOrderSummary(orderId)
        }else viewModel.getOrderSummary(ordId)
    }

    private fun firebaseMarkCompleteOrderEvent(data: OrderSummaryResponse) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ORDER_SUMMERY)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_NAME, data.customerName)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_PHONE, data.phone)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_MARK_COMPLETE,
                this)
        }
    }
}