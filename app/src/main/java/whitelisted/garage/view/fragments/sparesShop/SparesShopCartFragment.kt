package whitelisted.garage.view.fragments.sparesShop

import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.AddressData
import whitelisted.garage.api.response.AddressResponseModel
import whitelisted.garage.api.response.Cart
import whitelisted.garage.api.response.GetSSCartResponse
import whitelisted.garage.api.response.SSItemModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentSparesShopCartBinding
import whitelisted.garage.databinding.ItemPreviouslyOrderedBinding
import whitelisted.garage.eventBus.LocationSelectedEvent
import whitelisted.garage.eventBus.MembershipPurchasedEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.isNotZero
import whitelisted.garage.utils.CommonUtils.isVisible
import whitelisted.garage.utils.CommonUtils.linearLayoutManager
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.CartItemsAdapter
import whitelisted.garage.view.adapter.OOSItemsAdapter
import whitelisted.garage.view.adapter.SSItemsAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.SelectAddressViewModel
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import kotlin.math.roundToLong

class SparesShopCartFragment : BaseFragment() {

    private val binding by viewBinding(FragmentSparesShopCartBinding::inflate)
    private val viewModel: SparesShopFragmentViewModel by sharedViewModel()
    private val addressViewModel: SelectAddressViewModel by sharedViewModel()
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var isAccCart = false
    private lateinit var cart: Cart
    private var sparesPopularItems: MutableList<SSItemModel>? = null
    private var accPopularItems: MutableList<SSItemModel>? = null
    private var isAddedFromMoreItems = false
    private lateinit var selectedAddress: AddressResponseModel
    private var proDiscount: Double = 0.0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListeners()
        setObservers()

        isAccCart = try {
            viewModel.provideGetCartResponse().cartList[1].lineItems.isEmpty()
        } catch (e: IndexOutOfBoundsException) {
            false
        }

        if (isAccCart) {
            binding.tvSparesTab.isSelected = false
            binding.tvAccTab.isSelected = true
        } else {
            binding.tvSparesTab.isSelected = true
            binding.tvAccTab.isSelected = false
        }
        setSelectedTab(50)
        setFragmentResultListener(AppENUM.SELECT_ADDRESS) { _, bundle -> // We use a String here, but any type that can be put in a Bundle is supported
            val result: Parcelable? = bundle.getParcelable(AppENUM.ADDRESS_POST)
            (result as? AddressResponseModel)?.let {
                proceedToPaymentPage(it)
            }
        }

        setFragmentResultListener(AppENUM.SELECT_ADDRESS_CART) { _, bundle -> // We use a String here, but any type that can be put in a Bundle is supported
            val result: Parcelable? = bundle.getParcelable(AppENUM.ADDRESS_POST)
            (result as? AddressResponseModel)?.let {
                selectedAddress = it
                binding.tvDefAddress.text = it.address
                binding.tvDeliverTo.text = "${it.address_type}:"
            }
        }

        setFragmentResultListener(AppENUM.ADD_ADDRESS) { _, bundle -> // We use a String here, but any type that can be put in a Bundle is supported
            val addAddressSuccess = bundle.getBoolean(AppENUM.ADD_ADDRESS, false)
            if (addAddressSuccess) {
                showSelectAddressDialog()
            }
        }

        binding.rvMoreItems.layoutManager =
            object : LinearLayoutManager(requireContext(), HORIZONTAL, false) {
                override fun checkLayoutParams(lp: RecyclerView.LayoutParams): Boolean {
                    lp.width = (width / 2.2).toInt()
                    return true
                }
            }
        binding.rvPOIItems.layoutManager = linearLayoutManager(requireContext(), 2.5f)
        binding.srlFSSC.setOnRefreshListener {
            binding.srlFSSC.isRefreshing = false
            showLoader()
            viewModel.getSSCartAPI()
        }
        showLoader()
        viewModel.getSSCartAPI()

    }

    private fun proceedToPaymentPage(address: AddressResponseModel) {
        Bundle().apply {
            putString(AppENUM.CART_ID, cart.id) //            val totalAmount =
            //                cart.lineItems.filter { item -> (item.inventory ?: 0) > 0 }.sumOf { ci ->
            //                    (ci.gomTotalAmount ?: 0.0)
            //                }
            //            val totalDiscount = (cart.goCoinsDiscount ?: 0.0) + (cart.discount ?: 0.0)
            //            val totalDiscountedAmount = totalAmount.minus(totalDiscount)
            putString(AppENUM.AMOUNT_TO_BE_PURCHASED,
                (cart.discountedAmount.minus(proDiscount)).toString())
            putParcelable(AppENUM.ADDRESS_POST, address)
            putBoolean(AppENUM.IS_ACCESSORIES_CART, isAccCart)
            CommonUtils.addFragmentUtil(requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.SS_PAYMENT_TYPE, this))
        }
        fireCheckoutEvent(address)
    }

    private fun getPopularItems() = viewLifecycleOwner.lifecycleScope.launch {

        val symbol = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL)

        if (isAccCart && accPopularItems == null) accPopularItems = viewModel.getPopularItems("acc")
            .filter { (it.inventory ?: 0) > 0 } as MutableList<SSItemModel>
        else if (sparesPopularItems == null) sparesPopularItems =
            viewModel.getPopularItems("spares")
                .filter { (it.inventory ?: 0) > 0 } as MutableList<SSItemModel>

        (if (isAccCart) accPopularItems else sparesPopularItems)?.let {
            binding.clMoreItems.makeVisible(::cart.isInitialized && cart.lineItems.isNotEmpty())
            binding.rvMoreItems.adapter =
                SSItemsAdapter(symbol, it, this@SparesShopCartFragment, showBorder = true)
        } ?: kotlin.run {
            binding.clMoreItems.makeVisible(false)
        }
        checkChangesInMoreItems()
    }

    private fun checkChangesInMoreItems() {
        (binding.rvMoreItems.adapter as? SSItemsAdapter)?.dataList?.forEachIndexed { index, ssItemModel ->
            val item = cart.lineItems.find { ssItemModel.productId == it.productId }
            item?.let {
                ssItemModel.quantity = it.quantity ?: 0
                binding.rvMoreItems.adapter?.notifyItemChanged(index)
            } ?: kotlin.run {
                ssItemModel.quantity = 0
                binding.rvMoreItems.adapter?.notifyItemChanged(index)
            }
        }
    }

    private fun setObservers() {
        observeAddToCartAPI()
        observeGetCartAPI()
        observeCheckoutSuccessEvent()
        observeCheckoutFailureEvent()
        observeRetryPaymentEvent()
        getAllAddressObserver()
    }

    private fun observeRetryPaymentEvent() {
        viewModel.provideRetryPaymentEvent().observe(viewLifecycleOwner) {
            hideLoader()
            retryPayment()
        }
    }

    private fun retryPayment() {
        Bundle().apply {
            putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
            putString(AppENUM.IS_FROM, AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP)
            putString(AppENUM.CART_ID, cart.id)/*val totalAmount = cart.discountedAmount
                cart.lineItems.filter { item -> item.inventory ?: 0 > 0 }.sumOf { ci ->
                    (ci.gomTotalAmount ?: 0.0)
                }
            val totalDiscount = (cart.goCoinsDiscount ?: 0.0) + (cart.discount ?: 0.0)
            val totalDiscountedAmount = totalAmount.minus(totalDiscount)*/
            putString(AppENUM.AMOUNT_TO_BE_PURCHASED, cart.discountedAmount.toString())
            CommonUtils.addFragmentUtil(activity,
                FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, this))
        }
    }

    private fun observeCheckoutFailureEvent() {
        viewModel.provideCheckoutFailureEvent().observe(viewLifecycleOwner) {
            hideLoader()
            showPaymentFailedBottomSheet()
            firePlaceOrder(false)
        }
    }

    private fun showPaymentFailedBottomSheet() {
        requireActivity().let {
            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PAYMENT_FAILED,
                params = Bundle().apply {
                    putBoolean(AppENUM.IS_ACCESSORIES_CART, isAccCart)
                })?.let { baseFragment ->
                baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
            }
        }
    }

    private fun observeCheckoutSuccessEvent() {
        viewModel.provideCheckoutSuccessEvent().observe(viewLifecycleOwner) {
            hideLoader()
            viewModel.getSSCartAPI()
            CommonUtils.showToast(requireContext(), getString(R.string.order_placed_successfully))
            removeAllFragmentsTill(SparesShopFragment::class.java.simpleName)
            CommonUtils.addFragmentUtil(requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_ORDER_HISTORY))
            firePlaceOrder(true)
        }
    }

    private fun observeGetCartAPI() {
        viewModel.provideGetCartResponseAPI().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    viewModel.setGetCartResponse(it.body.data)
                    if (isAdded) {
                        setData()
                        checkChangesInMoreItems()
                        checkIfAddedFromMoreItems()

                        addressViewModel.getAllAddresses()
                    }
                    sendFirebaseEvents(it.body.data)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun getAllAddressObserver() {
        addressViewModel.provideAddressesResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        lifecycleScope.launch {
                            val isPresent = it.body.data.any { add -> add.isDefault == true }
                            if (isPresent && viewModel.provideGetCartResponse().addressVisible == true) {
                                it.body.data.find { add -> add.isDefault == true }?.let { add ->
                                    selectedAddress = add
                                    binding.cgDefAddress.makeVisible(true)
                                    binding.tvDefAddress.text = add.address
                                    binding.btnCheckout.text = getString(R.string.checkout)
                                    binding.tvDeliverTo.text = "${add.address_type}:"
                                }
                            }
                        }
                    }
                }
                is Result.Failure -> { //                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun checkIfAddedFromMoreItems() {
        if (isAddedFromMoreItems) binding.nsvMain.post { //binding.nsvMain.smoothScrollTo(binding.nsvMain.x.toInt(), binding.rvMoreItems.y.toInt())
            binding.nsvMain.fullScroll(View.FOCUS_DOWN)
            isAddedFromMoreItems = false
        }
    }

    private fun observeAddToCartAPI() {
        viewModel.provideAddToCartResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    if (isVisible) { //                        viewModel.cartItemChanged = null
                    }
                }
                is Result.Failure -> {}
            }
        }
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    private fun setData() = binding.apply {
        if (viewModel.provideGetCartResponse().cartList.isNotEmpty()) {
            binding.tvAccTab.text =
                resources.getString(R.string.accessories) + " (" + viewModel.provideGetCartResponse().cartList[0].lineItems.size + ")"

            binding.tvSparesTab.text =
                resources.getString(R.string.spares) + " (" + viewModel.provideGetCartResponse().cartList[1].lineItems.size + ")"

            cart = if (isAccCart) viewModel.provideGetCartResponse().cartList[0]
            else viewModel.provideGetCartResponse().cartList[1]
        }
        val shippingCharges = if (isAccCart) viewModel.provideGetCartResponse().accShippingCharges
        else viewModel.provideGetCartResponse().sparesShippingCharges

        if (cart.lineItems.isNotEmpty()) {
            getPopularItems()
            clEmptyList.makeVisible(false)
            cartGroup.makeVisible(true)
            clSavings.makeVisible(true)
            clPOI.makeVisible(false)
            tvApplyCouponGoCoins.makeVisible(true)
            clCheckout.makeVisible(true)
            cart.let {
                binding.clPro.makeVisible(true)
                if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                        false)) {
                    binding.tvEGPM.text = getString(R.string.priority_shipping_activated)
                    binding.tvProDesc.text = viewModel.provideGetCartResponse().proDiscountText
                    binding.tvAddPro.makeVisible(false)
                    binding.ivProTick.makeVisible(true)
                    binding.lblProDiscount.makeVisible(true)
                    binding.tvProDiscount.makeVisible(true)
                } else {
                    binding.tvEGPM.text = getString(R.string.easy_garage_pro_membership)
                    binding.tvProDesc.text = viewModel.provideGetCartResponse().basicDiscountText
                    binding.tvAddPro.makeVisible(true)
                    binding.ivProTick.makeVisible(false)
                    binding.lblProDiscount.makeVisible(false)
                    binding.tvProDiscount.makeVisible(false)
                }

                val totalDiscount = (it.goCoinsDiscount ?: 0.0) + (it.discount ?: 0.0)
                val list = it.lineItems.filter { item -> item.inventory ?: 0 > 0 }
                it.discountedAmount =
                    list.sumOf { item -> item.gomTotalAmount ?: 0.0 } + (shippingCharges
                        ?: 0.0) - totalDiscount
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,

                    AppENUM.RefactoredStrings.defaultCurrencySymbol).apply {

                    if (rvAddedParts.adapter == null) {
                        rvAddedParts.adapter =
                            CartItemsAdapter(it.estimatedDelivery?.estimatedDelivery,
                                list.sortedByDescending { item -> item.bulkOrderQuantity ?: 0 > 0 },
                                this,
                                this@SparesShopCartFragment)
                    }
                    (rvAddedParts.adapter as? CartItemsAdapter)?.setNewList(it.estimatedDelivery?.estimatedDelivery,
                        list.sortedByDescending { item -> item.bulkOrderQuantity ?: 0 > 0 })
                    rvAddedParts.adapter?.notifyDataSetChanged()
                    val oosList = it.lineItems.filter { item -> item.inventory ?: 0 == 0 }
                    if (oosList.isNotEmpty()) {
                        clOOS.makeVisible(true)
                        setOOSAdapter(oosList, this)
                    } else {
                        clOOS.makeVisible(false)
                    }

                    tvTotalBeforeDiscount.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

                    val totalMrp =
                        cart.lineItems.filter { item -> item.inventory ?: 0 > 0 }.sumOf { ci ->
                            (ci.totalItemAmount ?: 0.0)
                        }
                    "$this${totalMrp.roundToLong().toDouble()}".apply {
                        tvTotalBeforeDiscount.text = this
                    }

                    val totalAmount =
                        cart.lineItems.filter { item -> item.inventory ?: 0 > 0 }.sumOf { ci ->
                            (ci.gomTotalAmount ?: 0.0)
                        }
                    "$this${totalAmount.roundToLong().toDouble()}".apply {
                        tvItemTotal.text = this
                    }
                    if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                            false)) {
                        proDiscount =
                            totalAmount.times((viewModel.provideGetCartResponse().proDiscountPercent
                                ?: 0)).div(100)
                        "$this${proDiscount.roundToLong().toDouble()}".apply {
                            tvProDiscount.text = this
                        }
                    }

                    "$this${shippingCharges}".let { sc ->
                        tvShippingCharges.text = sc
                    }

                    val isFreeShipping = shippingCharges == 0.0
                    tvShippingChargesFree.makeVisible(isFreeShipping)
                    tvShippingCharges.makeVisible(!isFreeShipping)


                    tvDiscount.makeVisible(totalDiscount.isNotZero())
                    lblDiscount.makeVisible(totalDiscount.isNotZero())

                    "$this$totalDiscount".apply { tvDiscount.text = this }

                    //val totalDiscountedAmount = totalAmount.minus(totalDiscount)
                    "$this${
                        (it.discountedAmount.minus(proDiscount)).roundToLong().toDouble()
                    }".apply {
                        tvTotalPrice.text = this
                        tvTotalPrice2.text = this
                    }
                    "$this${
                        CommonUtils.convertDoubleTo2DecimalPlaces(it.totalAmount?.minus(it.gomPriceTotalAmount ?: 0.0) ?: 0.0)
                    }".apply {
                        tvSaving.text = this
                    }

                    val totalBulkDiscount =
                        cart.lineItems.filter { item -> (item.inventory ?: 0) > 0 }.sumOf { ci ->
                            if ((ci.quantity ?: 0) >= (ci.bulkOrderQuantity ?: 0)) (ci.gomPrice
                                ?: 0.0).times(ci.quantity ?: 0).minus(ci.gomTotalAmount ?: 0.0)
                                .toInt()
                            else 0
                        }

                    if (totalBulkDiscount > 0) {
                        clBulkDiscount.makeVisible(true)
                        tvBulkAmount.text = resources.getString(R.string.extra_bulk_discount,
                            "$this$totalBulkDiscount")

                    } else clBulkDiscount.makeVisible(false)

                }

                when {
                    (it.goCoinsDiscount ?: 0.0) > 0 -> {
                        val conversionValue =
                            viewModel.getIntSharedPreference(AppENUM.GO_COINS_VALUE)
                        val goCoinsApplied = ((it.goCoinsDiscount ?: 0.0) * conversionValue).toInt()
                        tvApplyCouponGoCoins.makeVisible(false)
                        tvCouponApplied.makeVisible(true)
                        ivAppliedIcon.setImageResource(R.drawable.ic_go_coin_medium_)
                        ("$goCoinsApplied ${resources.getString(R.string.gocoins)} ${
                            resources.getString(R.string.applied)
                        }").apply {
                            tvAppliedHeader.text = this
                        }

                        (resources.getString(R.string.currency_symbol_default) + it.goCoinsDiscount.toString()).apply {
                            tvAppliedAmount.text = this
                        }
                        tvSavings2.text = requireContext().getString(R.string.savings)
                        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                            AppENUM.RefactoredStrings.defaultCurrencySymbol).apply {
                            "$this${
                                CommonUtils.convertDoubleTo2DecimalPlaces(it.totalAmount?.minus(it.gomPriceTotalAmount ?: 0.0)
                                    ?.plus(it.goCoinsDiscount ?: 0.0) ?: 0.0)
                            }".apply {
                                tvSaving.text = this
                            }
                        }
                    }
                    (it.discount ?: 0.0) > 0 -> {
                        tvApplyCouponGoCoins.makeVisible(false)
                        tvCouponApplied.makeVisible(true)
                        ivAppliedIcon.setImageResource(R.drawable.ic_savings_icon)
                        ("'${it.discountCoupon}' ${resources.getString(R.string.applied)}").apply {
                            tvAppliedHeader.text = this
                        }
                        (resources.getString(R.string.currency_symbol_default) + it.discount.toString()).apply {
                            tvAppliedAmount.text = this
                        }
                        tvSavings2.text = requireContext().getString(R.string.coupon_savings)
                        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                            AppENUM.RefactoredStrings.defaultCurrencySymbol).apply {
                            "$this${
                                CommonUtils.convertDoubleTo2DecimalPlaces(it.totalAmount?.minus(it.gomPriceTotalAmount ?: 0.0)
                                    ?.plus(it.discount ?: 0.0) ?: 0.0)
                            }".apply {
                                tvSaving.text = this
                            }
                        }
                    }
                    else -> {
                        tvApplyCouponGoCoins.makeVisible(true)
                        tvCouponApplied.makeVisible(false)
                    }
                }
            }
        } else {
            clEmptyList.makeVisible(true)
            clCheckout.makeVisible(false)
            cartGroup.makeVisible(false)
            clMoreItems.makeVisible(false)
            clPro.makeVisible(false)
            showPreviouslyOrderedItems()
        }
    }

    private fun showPreviouslyOrderedItems() = lifecycleScope.launch {
        showLoader()
        val previouslyOrderedItems = viewModel.getPreviouslyOrderedItems().filter {
            if (isAccCart) it.isAccessories ?: false
            else !(it.isAccessories ?: false)
        }
        previouslyOrderedItems.onEach { it.isSelected = false }
        binding.tvItemCount.text = "0"
        binding.prevOrderGrp.makeVisible(false)
        hideLoader()
        if (previouslyOrderedItems.isNotEmpty()) {

            binding.clPOI.makeVisible(true)
            binding.rvPOIItems.clear().addViews(previouslyOrderedItems,
                ItemPreviouslyOrderedBinding::inflate) { binding, model, pos ->
                ImageLoader.loadImage(binding.ivProduct, model.imageUrl)
                binding.tvProductName.text = model.brand
                binding.tvProductDesc.text = model.title

                binding.clItemBaseISSC.setOnClickListener {
                    it.setTag(R.id.model, model)
                    onClick(it)
                }
                binding.btnSelect.setOnClickListener {
                    model.isSelected = !model.isSelected
                    this@SparesShopCartFragment.binding.rvPOIItems.adapter?.notifyItemChanged(pos)
                    changeItemCount(model.isSelected)
                }

                binding.btnSelect.setViewBackgroundColor(if (model.isSelected) R.color.colorAccent else R.color.white)
                binding.btnSelect.text =
                    resources.getString(if (model.isSelected) R.string.selected else R.string.select)
                binding.btnSelect.setTextColor(resources.getColor(if (model.isSelected) R.color.white else R.color.colorAccent,
                    resources.newTheme()))
            }

            binding.tvSeeAllPOI.setOnClickListener { gotoPLP(true) }
            binding.btnAddAllToCart.setOnClickListener {
                addPreviouslyOrderedItems(previouslyOrderedItems.filter { it.isSelected }
                    .onEach { it.quantity = 1 })
            }

        } else binding.clPOI.makeVisible(false)
    }

    private fun addPreviouslyOrderedItems(items: List<SSItemModel>) = lifecycleScope.launch {
        showLoader()
        if (!viewModel.addPreviouslyOrdered(items)) {
            CommonUtils.showToast(requireActivity(),
                resources.getString(R.string.unable_to_add_items))
            hideLoader()
        } else firePreviouslyOrderedEvent(items)
    }

    private fun changeItemCount(isSelected: Boolean) {

        var count = binding.tvItemCount.text.toString().toInt()
        if (isSelected) count++ else count--
        binding.tvItemCount.text = count.toString()

        binding.prevOrderGrp.makeVisible(count > 0)

    }

    private fun setOOSAdapter(oosList: List<SSItemModel>, currency: String) {
        binding.rvOOSItems.adapter = OOSItemsAdapter(currency, this)
        (binding.rvOOSItems.adapter as? OOSItemsAdapter)?.setNewList(oosList)
    }

    private fun setSelectedTab(delay: Long = 0) = lifecycleScope.launch {
        delay(delay)
        binding.tvAccTab.isSelected = isAccCart
        binding.tvSparesTab.isSelected = !isAccCart
        binding.cvTabBg.animate()
            .translationX(if (isAccCart) binding.tvAccTab.x - 10 else binding.tvSparesTab.x - 10)
            .setDuration(150).start()
    }

    private fun clickListeners() {
        binding.imgBackFSSC.setOnClickListener(this)
        binding.btnCheckout.setOnClickListener(this)
        binding.tvApplyCouponGoCoins.setOnClickListener(this)
        binding.tvRemoveCoupon.setOnClickListener(this)
        binding.btnEmptyAction.setOnClickListener(this)
        binding.tvAccTab.setOnClickListener(this)
        binding.tvSparesTab.setOnClickListener(this)
        binding.tvRemoveOOS.setOnClickListener(this)
        binding.tvChange.setOnClickListener(this)
        binding.tvAddPro.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBackFSSC -> {
                popBackStackAndHideKeyboard()
            }
            R.id.imgPlusCart -> {
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                    it.apply {
                        type = 1
                        showLoader()
                        viewModel.cartItemChanged = this
                        viewModel.addToCart(productId, this)
                    }
                }
            }

            R.id.imgMinusCart -> {
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                    it.apply {
                        type = 0
                        showLoader()
                        viewModel.cartItemChanged = this
                        viewModel.addToCart(productId, this)
                    }
                }
            }
            R.id.ivDelete -> {
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                    showConfirmationDialog(it)
                }
            }
            R.id.btnCheckout -> {
                if (binding.clOOS.isVisible()) {
                    lifecycleScope.launch {
                        if (::cart.isInitialized) {
                            showLoader()
                            val result = viewModel.removeOOSItems(cart.id)
                            hideLoader()
                            if (result) {
                                CommonUtils.showToast(requireContext(),
                                    getString(R.string.alert_remove_oos))
                                checkoutClick()
                                viewModel.getSSCartAPI()
                            }
                        }
                    }
                } else {
                    checkoutClick()
                }
            }
            R.id.tvApplyCouponGoCoins -> showCouponDialog()

            R.id.tvRemoveCoupon -> removeCoupon()

            R.id.btnEmptyAction -> {
                viewModel.setBrandsSelected(!isAccCart)
                gotoPLP()
            }

            R.id.clItemBaseISSC -> {
                viewModel.provideGetCartResponseAPI().postValue(null)
                viewModel.provideAddToCartResponse().postValue(null)
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_PRODUCT_DETAILS,
                            Bundle().apply {
                                putString(AppENUM.SS_PRODUCT_ID, it.skuCode)
                            }))
                }
            }

            R.id.tvAccTab -> {
                isAccCart = true
                setSelectedTab()
                fireInitAccCart()
                setData()
            }
            R.id.tvSparesTab -> {
                isAccCart = false
                setSelectedTab()
                setData()
            }

            R.id.btnAdd -> CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.apply {
                type = 1
                showLoader()
                viewModel.addToCart(productId, this)
                fireAddCartEvent(this)
                isAddedFromMoreItems = true
            }

            R.id.imgPlus, R.id.btnAddIBI -> CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))
                ?.apply {
                    type = 1
                    showLoader()
                    viewModel.addToCart(productId, this)
                    fireBundleEvent(this)
                }

            R.id.imgMinus -> CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))
                ?.apply {
                    type = 0
                    showLoader()
                    viewModel.addToCart(productId, this)
                }


            R.id.clISI -> {
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_PRODUCT_DETAILS,
                            Bundle().apply {
                                putParcelable(AppENUM.SSITEM, it)
                            }))
                }
            }

            R.id.tvTextView -> CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))
                ?.apply {
                    type = 1
                    showLoader()
                    viewModel.addToCart(productId, this)
                    sendQtyDropdownEvent(this)
                }

            R.id.tvRemoveOOS -> {
                lifecycleScope.launch {
                    if (::cart.isInitialized) {
                        showLoader()
                        val result = viewModel.removeOOSItems(cart.id)
                        hideLoader()
                        if (result) {
                            showLoader()
                            viewModel.getSSCartAPI()
                        }
                    }
                }
            }

            R.id.tvChange -> {
                showSelectAddressDialog(true)
            }

            R.id.tvAddPro->{
                fireAddCartProEvent()
                dashboardViewModel.screensConfigResponse.value?.membership?.membershipPrice?.let { price->
                    Bundle().apply {
                        putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                        putString(AppENUM.IS_FROM,
                            AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_PRO)
                        putString(AppENUM.AMOUNT_TO_BE_PURCHASED, price)
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, this))
                    }
                }
            }
        }
    }

    private fun showConfirmationDialog(ssItemModel: SSItemModel) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
            Bundle().apply {
                putBoolean(AppENUM.TITLE_COLOR_ACCENT, true)
                putString(AppENUM.TITLE, getString(R.string.delete_item))
                putString(AppENUM.DESCRIPTION, getString(R.string.alert_item_delete))
                putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.yes))
                putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.no))
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    if (extra as? Boolean == true) {
                        fireCartDeletionEvent(ssItemModel)
                        ssItemModel.apply {
                            type = 2
                            showLoader()
                            viewModel.cartItemChanged = this
                            viewModel.addToCart(productId, this)
                        }
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun checkoutClick() {
        if (::cart.isInitialized) {
            if ((cart.gomPriceTotalAmount ?: 0.0) < (viewModel.provideGetCartResponse().paymentLimit
                    ?: 0.0)) {
                CommonUtils.showToast(requireContext(),
                    "${getString(R.string.minimum_order_value_should_be)} ${
                        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                            AppENUM.RefactoredStrings.defaultCurrencySymbol)
                    }${viewModel.provideGetCartResponse().paymentLimit}")
            } else {
                if (::selectedAddress.isInitialized) proceedToPaymentPage(selectedAddress)
                else showSelectAddressDialog()
            }
        }
    }


    private fun gotoPLP(showPreviouslyOrdered: Boolean = false) {
        if (!showPreviouslyOrdered) popBackStackAndHideKeyboard()
        val isPlpAdded = if (showPreviouslyOrdered) false
        else requireActivity().supportFragmentManager.fragments.any {
            it is ShopWithFilterFragment
        }
        if (!isPlpAdded) {
            CommonUtils.addFragmentUtil(
                requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                    Bundle().apply {
                        putBoolean(AppENUM.SHOW_PREVIOUSLY_ORDERED, showPreviouslyOrdered)
                    }),
            )
        } else viewModel.updateFilters()
    }

    private fun removeCoupon() = lifecycleScope.launchWhenStarted {
        showLoader()
        val removed = viewModel.removeMarketPlaceCoupon(cart.id)
        if (removed) viewModel.getSSCartAPI()
        else {
            hideLoader()
            CommonUtils.showToast(requireContext(),
                resources.getString(R.string.unable_to_remove_coupon))
        }
    }

    private fun showCouponDialog() {
        val isBulkApplied = cart.lineItems.any { i -> i.quantity == i.bulkOrderQuantity }
        val bundle = Bundle()
        bundle.putBoolean(AppENUM.IS_ACCESSORIES_CART, isAccCart)
        bundle.putBoolean(AppENUM.IS_BULK_APPLIED, isBulkApplied)
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.APPLY_MARKETPLACE_COUPON_DIALOG,
            bundle,
            null)?.let {
            it.show(parentFragmentManager, it.javaClass.name)
        }
    }


    private fun showSelectAddressDialog(isChange: Boolean = false) {
        requireActivity().let {
            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.SELECT_ADDRESS,
                params = if (isChange) Bundle().apply {
                    putBoolean(AppENUM.SELECT_ADDRESS_CART, true)
                    putParcelable(AppENUM.ADDRESS_POST, selectedAddress)
                } else null)?.let { baseFragment ->
                baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
            }
        }
    }

    override fun onDestroyView() {
        viewModel.provideAddToCartResponse().postValue(null)
        super.onDestroyView()
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is LocationSelectedEvent -> {
                (event.extra as? AddressData)?.let { addressData ->
                    requireActivity().let {
                        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.ADD_EDIT_ADDRESS,
                            Bundle().apply {
                                putParcelable(AppENUM.ADDRESS_POST, addressData)
                            })?.let { baseFragment ->
                            baseFragment.show(it.supportFragmentManager,
                                baseFragment.javaClass.name)
                        }
                    }
                }
            }

            is MembershipPurchasedEvent -> {
                showLoader()
                viewModel.getSSCartAPI()
            }
        }
    }


    private fun sendFirebaseEvents(data: GetSSCartResponse) {
        data.cartList[0].lineItems.filter { i -> i.quantity == i.bulkOrderQuantity }
            .forEach { i -> sendBulkDscAppliedEvent(i, data.cartList[0]) }
        data.cartList[1].lineItems.filter { i -> i.quantity == i.bulkOrderQuantity }
            .forEach { i -> sendBulkDscAppliedEvent(i, data.cartList[1]) }
    }

    private fun fireInitAccCart() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_CART)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_ACC_CART,
            bundle)
    }

    private fun firePlaceOrder(paymentStatus: Boolean) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.TOTAL_AMOUNT,
            cart.discountedAmount.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PAYMENT_MODE,
            AppENUM.MODE_ONLINE)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PAYMENT_STATUS,
            paymentStatus.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_PAYMENT_TYPE)
        FirebaseAnalyticsLog.trackFireBaseEventLog(if (isAccCart) FirebaseAnalyticsLog.FirebaseEventNameENUM.PURCHASE_ACC
        else FirebaseAnalyticsLog.FirebaseEventNameENUM.PLACE_ORDER, bundle)
    }

    private fun fireCheckoutEvent(selectedAddress: AddressResponseModel) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CITY, selectedAddress.city)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.STATE, selectedAddress.state)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PIN, selectedAddress.pin)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.ITEM_DETAILS,
            cart.lineItems.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_CART)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.SPARES_CHECKOUT,
            bundle)
    }


    private fun fireAddCartEvent(item: SSItemModel) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, item.title)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NUMBER, item.productId)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_SKU_CODE, item.skuCode)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.POPULAR_TAG, item.tag?.tagName)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_ENQUIRY, item.isEnquiry)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_FASTMOVING,
            item.isfastmoving)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_ID, item.id?.oid ?: "")
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_BRAND, item.brand)
        bundle.putDouble(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
            item.gomPrice ?: 0.0)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_TYPE, item.skuCategory)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_CART)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.POPULAR_PARTS,
            bundle)
    }

    private fun fireBundleEvent(item: SSItemModel) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, item.title)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NUMBER, item.productId)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_SKU_CODE, item.skuCode)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.POPULAR_TAG, item.tag?.tagName)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_ENQUIRY, item.isEnquiry)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_FASTMOVING,
            item.isfastmoving)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_ID, item.id?.oid ?: "")
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_BRAND, item.brand)
        bundle.putDouble(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
            item.gomPrice ?: 0.0)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_TYPE, item.skuCategory)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_CART)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.BUNDLE_PRODUCT,
            bundle)
    }


    private fun sendBulkDscAppliedEvent(item: SSItemModel, cart: Cart) {
        val bundle = Bundle()

        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, item.title)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NUMBER, item.productId)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_SKU_CODE, item.skuCode)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_ENQUIRY, item.isEnquiry)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_FASTMOVING,
            item.isfastmoving)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.POPULAR_TAG, item.tag?.tagName)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_ID, item.id?.oid ?: "")

        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.BRAND, item.brand)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.DISCOUNT,
            item.bulkDiscountRate.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.QUANTITY,
            item.quantity.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
            item.gomPrice.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_CATEGORY, item.skuCategory)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CART_VALUE,
            cart.discountedAmount.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CART_ITEMS,
            cart.lineItems.toString())


        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_CART)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.BULK_PURC_APPLIED,
            bundle)

    }


    private fun sendQtyDropdownEvent(item: SSItemModel) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, item.title)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NUMBER, item.productId)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_SKU_CODE, item.skuCode)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_ENQUIRY, item.isEnquiry)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_FASTMOVING,
            item.isfastmoving)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.POPULAR_TAG, item.tag?.tagName)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_ID, item.id?.oid ?: "")
        bundle.putDouble(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
            item.gomPrice ?: 0.0)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.BRAND, item.brand)
        bundle.putDouble(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
            item.gomPrice ?: 0.0)
        bundle.putInt(FirebaseAnalyticsLog.FirebaseEventNameENUM.QTY_SELECTED, item.quantity ?: 0)

        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_CART)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.QTY_DROP_DOWN,
            bundle)


    }


    private fun firePreviouslyOrderedEvent(items: List<SSItemModel>) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PREVIOUSLY_ORDERED_ITEMS,
            items.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_CART)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.PREVIOUSLY_ORDERED,
            bundle)
    }

    private fun fireAddCartProEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_CART)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.ADD_TO_CART_PRO,
            bundle)
    }

    private fun fireCartDeletionEvent(item: SSItemModel) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, item.title)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NUMBER, item.productId)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_SKU_CODE, item.skuCode)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.POPULAR_TAG, item.tag?.tagName)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_ENQUIRY, item.isEnquiry)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_FASTMOVING,
            item.isfastmoving)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_ID, item.id?.oid ?: "")
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_BRAND, item.brand)
        bundle.putDouble(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
            item.gomPrice ?: 0.0)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_TYPE, item.skuCategory)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_CART)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.CART_DELETION,
            bundle)
    }


}