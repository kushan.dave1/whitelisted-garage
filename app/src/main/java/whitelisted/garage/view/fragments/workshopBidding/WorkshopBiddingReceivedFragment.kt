package whitelisted.garage.view.fragments.workshopBidding

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.AccRetailBiding
import whitelisted.garage.api.response.GetWsBidReceivedResponse
import whitelisted.garage.api.response.GetWsOngoingBidResponse
import whitelisted.garage.api.response.WorkshopBiding
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentWorkshopBidReceivedBinding
import whitelisted.garage.databinding.LayoutCancelBidBinding
import whitelisted.garage.eventBus.GoBackEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.getLocationPointOnScreen
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.WorkShopBidReceivedAdapter
import whitelisted.garage.viewmodels.WorkShopBiddingViewModel

class WorkshopBiddingReceivedFragment : BaseFragment() {

    private val binding by viewBinding(FragmentWorkshopBidReceivedBinding::inflate)
    private val viewModel: WorkShopBiddingViewModel by sharedViewModel()
    private var wsBidData: GetWsOngoingBidResponse? = null
    private var wsBidRecResponse: GetWsBidReceivedResponse? = null
    private var isCompletedSection = false


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListener()
        observeData()
        setUnlockPhoneObserver()
        getData()
    }

    private fun getData() = binding.apply {
        wsBidData = arguments?.getParcelable(AppENUM.IntentKeysENUM.WORKSHOP_BID_DATA)
        when (wsBidData?.statusId) {
            AppENUM.WSBiddingStatusConstant.COMPLETED -> {
                llPendingBids.makeVisible(false)
                llOnnGoingBids.makeVisible(true)
                isCompletedSection = true
            }
            AppENUM.WSBiddingStatusConstant.NEW -> {
                llPendingBids.makeVisible(true)
                llOnnGoingBids.makeVisible(false)
            }
            else -> {
                llPendingBids.makeVisible(false)
                llOnnGoingBids.makeVisible(true)
            }
        }
        showLoader()
        viewModel.getWsReceivedBids(wsBidData?.id ?: "")
    }

    private fun setUnlockPhoneObserver() {
        viewModel.provideUnlockContactResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_UNLOCK_CONTACT,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_WORKSHOP_BI_RECEIVE)
                    showLoader()
                    viewModel.getWsReceivedBids(wsBidData?.id ?: "")
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    private fun observeData() {
        viewModel.provideGetWsBidReceived().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.let { getWsBidRecResponse ->
                        this.wsBidRecResponse = getWsBidRecResponse
                        setAdapterData(wsBidRecResponse?.accRetailBiding)
                        setDataToPendingBids(wsBidRecResponse?.workshopBiding)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
        viewModel.provideCancelWsBid().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.let {
                        viewModel.setRefreshReceivedBidList(true)
                        popBackStackAndHideKeyboard()
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
        viewModel.provideRefreshReceivedBidList().observe(viewLifecycleOwner) {
            if (it == true) {
                showLoader()
                viewModel.getWsReceivedBids(wsBidData?.id ?: "")
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setDataToPendingBids(workshopBiding: WorkshopBiding?) {
        binding.tvTotalItems.text =
            "${getString(R.string.total)} ${(workshopBiding?.items?.size ?: 0)} ${getString(R.string.items)}"
        binding.tvTotalItemsNotNew.text =
            "${getString(R.string.total)} ${(workshopBiding?.items?.size ?: 0)} ${getString(R.string.items)}"
    }

    private fun setAdapterData(accRetailBiding: List<AccRetailBiding>?) {
        if (accRetailBiding?.isNotEmpty() == true) {
            binding.tvTotalBidReceived.text = (accRetailBiding.size).toString()
            binding.rvBidsDetails.adapter = WorkShopBidReceivedAdapter(accRetailBiding,
                this,
                isCompletedSection,
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID, ""))
        }
    }

    private fun clickListener() {
        binding.btnProceed.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)
        binding.llViewItems.setOnClickListener(this)
        binding.llViewItemsNotNew.setOnClickListener(this)
        binding.imgMore.setOnClickListener(this)
        binding.swipeRefresh.setOnRefreshListener {
        binding.swipeRefresh.isRefreshing = false
            showLoader()
            viewModel.getWsReceivedBids(wsBidData?.id ?: "")
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnProceed, R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
            R.id.imgMore -> {
                cancelBidPopUp(v)

            }
            R.id.imgCall -> {
                CommonUtils.genericCastOrNull<String>(v.getTag(R.id.model))?.let {
                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.data = Uri.parse("tel:${it}")
                    startActivity(intent)
                }
            }
            R.id.tvItems -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.tvItems))?.let {
                    (binding.rvBidsDetails.adapter as? WorkShopBidReceivedAdapter)?.accRetailBiding?.get(it)?.isItemTextExpanded =
                        true
                    binding.rvBidsDetails.adapter?.notifyItemChanged(it)
                }
            }
            R.id.llViewItems, R.id.llViewItemsNotNew -> {
                viewModel.brandWiseItems =
                    viewModel.getAllMapOfBrandBidsList((wsBidRecResponse?.workshopBiding?.brandWiseItems)?.toMutableList()
                        ?: mutableListOf())
                viewModel.genericItems =
                    (wsBidRecResponse?.workshopBiding?.genericItems)?.toMutableList()
                        ?: mutableListOf()
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_BIDDING_CART_DETAILS_FRAGMENT,
                        Bundle().apply {
                            this.putString(AppENUM.ID, wsBidData?.id)
                            this.putBoolean(AppENUM.IS_VIEW_ONLY, true)
                        }))
            }
            R.id.imgDirection -> {
                CommonUtils.genericCastOrNull<AccRetailBiding>(v.getTag(R.id.model))?.let {
                    val mapLik = "${AppENUM.MAP_URL}${it.latitude},${it.longitude}"
                    CommonUtils.openWebPage(requireContext(), mapLik)
                }
            }
            R.id.llViewDetails -> {
                CommonUtils.genericCastOrNull<AccRetailBiding>(v.getTag(R.id.model))?.let {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_BID_ENQUIRY_FRAG,
                            Bundle().apply {
                                putBoolean(AppENUM.IS_VIEW_ONLY, false)
                                putString(AppENUM.ID, it.id)
                            }))
                }
            }
            R.id.llUnlockMobile -> {
                CommonUtils.genericCastOrNull<AccRetailBiding>(v.getTag(R.id.model))?.let {
                    showConfirmationDialogForUnlockMobile(it)
                }
            }
        }
    }

    private fun showConfirmationDialogForUnlockMobile(bidDetail: AccRetailBiding) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
            Bundle().apply {
                putString(AppENUM.TITLE, getString(R.string.unlock_contact_number))
                putString(AppENUM.DESCRIPTION,
                    getString(R.string.unlock_this_contact_number_using_10_gocoins))
                putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.okay))
                putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.cancel))
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    if (extra as? Boolean == true) {
                        showLoader()
                        viewModel.unlockPhoneWorkshop(bidDetail.id.toString())
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun cancelBidPopUp(view: View?) {
        view?.let {
            val infoView = LayoutCancelBidBinding.inflate(layoutInflater)
            val popupWindow = PopupWindow(infoView.root,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                true)
            popupWindow.showAtLocation(view,
                Gravity.NO_GRAVITY,
                view.getLocationPointOnScreen().x - 110,
                view.getLocationPointOnScreen().y + 40)
            infoView.parentLayout.setOnClickListener {
                popupWindow.dismiss()
                openCancelBidDialog()
            }
        }
    }

    private fun openCancelBidDialog() {
        activity?.let {
            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.CANCEL_WS_BID,
                null,
                object : ActionListener {
                    override fun onActionItem(extra: Any?, extra2: Any?) {
                        if (extra == true) {
                            showLoader()
                            viewModel.cancelWorkshopBid(wsBidData?.id ?: "")
                        }
                    }
                })?.let { baseFragment ->
                baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
            }
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is GoBackEvent -> {
                popBackStackAndHideKeyboard()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.provideUnlockContactResponse().postValue(null)
    }
}