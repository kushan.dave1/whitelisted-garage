package whitelisted.garage.view.fragments.payment

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.zxing.WriterException
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.data.SetOptionsImgStatusObject
import whitelisted.garage.api.request.AddPaymentRequest
import whitelisted.garage.api.request.OrderInclusionsItem
import whitelisted.garage.api.request.SaveUPIIdRequest
import whitelisted.garage.api.response.GetPaymentResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentPaymentBinding
import whitelisted.garage.eventBus.*
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.PaymentInclusionsItemsAdapter
import whitelisted.garage.view.adapter.PaymentsAdapter
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment
import whitelisted.garage.viewmodels.PaymentViewModel
import whitelisted.garage.viewmodels.PaymentsSharedViewModel

class PaymentFragment : BaseFragment() {

    private val binding by viewBinding(FragmentPaymentBinding::inflate)
    private val viewModel: PaymentViewModel by viewModel()
    private val paymentsSharedViewModel: PaymentsSharedViewModel by sharedViewModel()

    private lateinit var getPaymentResponse: MutableList<GetPaymentResponseModel>
    private lateinit var paymentsAdapter: PaymentsAdapter
    private lateinit var completePaymentResponse: JSONObject
    private var isEditUpiOpen = false
    private var isUpiAsked = false
    private lateinit var upiId: String
    private var shopType = ""
    private var isRetailerItemsSet = false
    private var orderInclusionItems = mutableListOf<OrderInclusionsItem>()

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun setupScreen() {
        setPageEvent(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_PAYMENT,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_PAYMENT,
        )
        setObservers()
        setOnClickListener()
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                shopType = AppENUM.RefactoredStrings.WORKSHOP_CONSTANT
            }
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                shopType = AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT
            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                shopType = AppENUM.RefactoredStrings.RETAILER_CONSTANT
                binding.clCarDetails.visibility = View.GONE
                binding.lblInvoiceId.visibility = View.GONE
                binding.tvInvoiceId.visibility = View.GONE
                binding.llTotalItems.visibility = View.VISIBLE
                binding.lblDueAmount.visibility = View.VISIBLE
                binding.tvTotalDueAmount.visibility = View.VISIBLE
                binding.llButtons2.visibility = View.VISIBLE
                binding.llButtons.visibility = View.GONE
            }
        }

        showLoader()
        viewModel.getPaymentDetails()
    }

    private fun setOnClickListener() {
        binding.btnAddPayment.setOnClickListener(this)
        binding.btnMarkComplete.setOnClickListener(this)
        binding.llViewMore.setOnClickListener(this)
        binding.btnAddPayment2.setOnClickListener(this)
        binding.btnGiveCredit.setOnClickListener(this)
        binding.btnSetupNow.setOnClickListener(this)
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun setObservers() {
        observeGetPaymentResponse()
        observeAddPaymentResponse()
        observeCompletePaymentResponse()
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                AppENUM.RefactoredStrings.defaultCurrency) == AppENUM.RefactoredStrings.defaultCurrency) {
            observeGetUpiResponse()
        }
        observeSaveUiResponse()
    }

    private fun observeSaveUiResponse() {
        viewModel.provideSaveUPIResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(),
                        it.body.message,
                        true) //                    showLoader()
                    //                    viewModel.getUPIId()
                    //                    if (isFirstTime)
                    //                        isFirstTime = false
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun observeGetUpiResponse() {
        viewModel.provideGetUPIResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (isVisible && isResumed) {
                        if (it.body.data.upi_id != null) {
                            upiId = it.body.data.upi_id
                            setupQRCode(upiId)
                        } else {
                            binding.cvQRNotAvailable.makeVisible(true)
                            if (!isUpiAsked) {
                                showEnterUpiDialog()
                            }
                        }
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeCompletePaymentResponse() {
        viewModel.provideCompletePaymentResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    completePaymentResponse = it.body.data
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    EventBus.getDefault().post(UpdateOrderStatusEvent(
                        AppENUM.RefactoredStrings.COMPLETED_ORDER_STATUS.toString(),
                    ))
                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                    EventBus.getDefault()
                        .post(RefreshOrderSummaryEvent(OrderDetailWrapperFragment.orderId))
                    showLoader()
                    viewModel.getPaymentDetails()

                    CommonUtils.showReviewDialog(requireActivity())
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeAddPaymentResponse() {
        viewModel.provideAddPaymentResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setTransactionDetails()
                    showLoader()
                    EventBus.getDefault().post(UpdateOrderStatusEvent(
                        AppENUM.RefactoredStrings.READY_ORDER_STATUS.toString(),
                    ))
                    viewModel.getPaymentDetails()
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                    EventBus.getDefault()
                        .post(RefreshOrderSummaryEvent(OrderDetailWrapperFragment.orderId))
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun observeGetPaymentResponse() {
        viewModel.provideGetPaymentResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    getPaymentResponse = it.body.data.toMutableList()
                    if (isVisible) updateScreenDetails()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setTransactionDetails() {
        binding.cvTransactionDetails.visibility = View.VISIBLE
        setPaymentsAdapter()
    }

    private fun setPaymentsAdapter() {
        binding.rvPayments.layoutManager = LinearLayoutManager(requireContext())
        paymentsAdapter =
            PaymentsAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default)),
                getPaymentResponse.subList(1, getPaymentResponse.size))
        binding.rvPayments.adapter = paymentsAdapter
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun updateScreenDetails() = binding.apply {
        when (shopType) {
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> { //                if (viewModel.provideOrderEstimateItemsResponse().value?.isNotEmpty() == true) {
                //                    setPaymentItemsAdapterEstimate()
                //                    llItems.visibility = View.VISIBLE
                //                } else llItems.visibility = View.GONE
            }
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> { //                if (viewModel.provideOrderEstimateItemsResponse().value?.isNotEmpty() == true) {
                //                    setPaymentItemsAdapterEstimate()
                //                    llItems.visibility = View.VISIBLE
                //                } else llItems.visibility = View.GONE
            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> { //                isRetailerItemsSet = false
                setRetailerItems()
                if (getPaymentResponse.size > 0) {
                    if (getPaymentResponse[0].paymentDetails?.billAmount ?: 0.0 > 0) {
                        if (getPaymentResponse[0].paymentDetails?.dueAmount ?: 0.0 == 0.0) {
                            btnGiveCredit.text = getString(R.string.mark_complete)
                            btnGiveCredit.setTextColor(requireContext().getColor(R.color.white))
                            btnGiveCredit.background = ContextCompat.getDrawable(requireContext(),
                                R.drawable.bg_default_button)
                        }

                        "${
                            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                                getString(R.string.currency_symbol_default))
                        }${getPaymentResponse[0].paymentDetails?.creditValue}".apply {
                            tvTotalDueAmount.text = this
                        }
                    }
                    tvInvoiceId2.text = getPaymentResponse[0].invoiceId ?: "NA"
                }
            }
        }

        if (getPaymentResponse.size > 1) {
            setTransactionDetails()
        }

        if (getPaymentResponse.size > 0) {
            getPaymentResponse[0].let { paymentModel ->
                ImageLoader.loadImage(imgCarImage, paymentModel.carIcon)
                tvCarName.text = paymentModel.carDetails?.car
                tvRegNumber.text = paymentModel.carDetails?.registrationNumber
                tvFuelType.text = paymentModel.carDetails?.fuelType ?: ""
                tvInvoiceId.text = paymentModel.invoiceId ?: "NA"
                val billAmt = "${
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default))
                }${
                    String.format("%.2f", paymentModel.paymentDetails?.billAmount ?: 0.0)
                }"
                tvBillAmount.text = billAmt

                val totalPaid = "${
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default))
                }${
                    String.format("%.2f", paymentModel.paymentDetails?.paidAmount ?: 0.0)
                }"
                tvTotalPaid.text = totalPaid

                val totalDiscount = "${
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default))
                }${
                    String.format("%.2f", paymentModel.paymentDetails?.discountValue ?: 0.0)
                }"
                tvTotalDiscount.text = totalDiscount

                val totalDueAmt = if (paymentModel.paymentDetails?.dueAmount ?: 0.0 < 0) {
                    "${
                        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                            getString(R.string.currency_symbol_default))
                    }0.0"
                } else if (paymentModel.paymentDetails?.dueAmount ?: 0.0 > 0 && paymentModel.paymentDetails?.dueAmount ?: 0.0 < 1) {
                    "${
                        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                            getString(R.string.currency_symbol_default))
                    }0.0"
                } else {
                    "${
                        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                            getString(R.string.currency_symbol_default))
                    }${
                        String.format("%.2f", paymentModel.paymentDetails?.dueAmount ?: 0.0)
                    }"
                }
                tvBalanceAmount.text = totalDueAmt

                var status = getString(R.string.na)
                if (paymentModel.paymentDetails?.dueAmount ?: 0.0 > 0) {
                    if (paymentModel.paymentDetails?.dueAmount ?: 0.0 <= 0.0) {
                        status = getString(R.string.paid)
                        tvPaymentStatus.setTextColor(ContextCompat.getColor(requireContext(),
                            R.color.green_new))
                        btnMarkComplete.isEnabled = true

                        cvScanAndPay.visibility = View.GONE
                        cvQRNotAvailable.makeVisible(false)
                        cvPay.visibility = View.GONE
                    } else {
                        status = getString(R.string.unpaid)
                        tvPaymentStatus.setTextColor(ContextCompat.getColor(requireContext(),
                            R.color.penalty_yellow))
                        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                                AppENUM.RefactoredStrings.defaultCurrency) != AppENUM.RefactoredStrings.defaultCurrency) {
                            cvPay.visibility = View.VISIBLE
                        } else {
                            if (::upiId.isInitialized && upiId.isNotEmpty()) {
                                cvScanAndPay.visibility = View.VISIBLE
                            }
//                            else {
//                                cvQRNotAvailable.makeVisible(true)
//                            }
                        }
                        val dueAmt = "${
                            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                                getString(R.string.currency_symbol_default))
                        }${
                            String.format("%.2f", paymentModel.paymentDetails?.dueAmount)
                        }"
                        tvDueAmount.text = dueAmt
                        tvDueAmount2.text = dueAmt

                        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                                AppENUM.RefactoredStrings.defaultCurrency) == AppENUM.RefactoredStrings.defaultCurrency) {
                            if (!::upiId.isInitialized) {
                                showLoader()
                                viewModel.getUPIId()
                            } else setupQRCode(upiId)
                        }
                    }
                } else {
                    if (paymentModel.paymentDetails?.billAmount ?: 0.0 > 0) {
                        status = getString(R.string.paid)
                        tvPaymentStatus.setTextColor(ContextCompat.getColor(requireContext(),
                            R.color.green_new))
                    } else {
                        tvPaymentStatus.setTextColor(ContextCompat.getColor(requireContext(),
                            R.color.blacktextA2))
                    }
                    btnMarkComplete.isEnabled = true

                    cvScanAndPay.visibility = View.GONE
                    cvQRNotAvailable.makeVisible(false)
                    cvPay.visibility = View.GONE
                    if (paymentModel.paymentDetails?.billAmount ?: 0.0 < 1) {
                        btnMarkComplete.isEnabled =
                            false //                btnAddPayment.background =
                        //                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_button_disabled)
                    }
                }
                tvPaymentStatus.text = status

                if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                        AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.RETAILER_CONSTANT) {
                    if (paymentModel.paymentDetails?.pdfStatus?.status == true) {
                        EventBus.getDefault().post(SetOptionsImageStatusEvent("1",
                            SetOptionsImgStatusObject(tabPosition = 2,
                                pdfFile = paymentModel.paymentDetails.taxPdfStatus?.pdfFile ?: "",
                                pdfFile2 = paymentModel.paymentDetails.pdfStatus?.pdfFile ?: "")))
                    } else {
                        EventBus.getDefault().post(SetOptionsImageStatusEvent("1",
                            SetOptionsImgStatusObject(tabPosition = 2)))
                    }
                } else {
                    if (paymentModel.paymentDetails?.pdfStatus?.status == true) {
                        EventBus.getDefault().post(SetOptionsImageStatusEvent("0",
                            SetOptionsImgStatusObject(tabPosition = 2,
                                pdfFile = paymentModel.paymentDetails.taxPdfStatus?.pdfFile ?: "",
                                pdfFile2 = paymentModel.paymentDetails.pdfStatus?.pdfFile ?: "")))
                    } else {
                        EventBus.getDefault().post(SetOptionsImageStatusEvent("0",
                            SetOptionsImgStatusObject(tabPosition = 2)))
                    }
                }
            }
        }
    }

    private fun setRetailerItems() = binding.apply {
        if (!isRetailerItemsSet) {
            if (paymentsSharedViewModel.provideOrderInclusionItemsResponse().isNotEmpty()) {
                setPaymentItemsAdapterInclusive()
                llItems.visibility = View.VISIBLE
            } else llItems.visibility = View.GONE
            var total = 0
            paymentsSharedViewModel.provideOrderInclusionItemsResponse().forEach {
                total += it.quantity ?: 0
            }
            tvTotalItems.text = total.toString()
        }
    }

    //    private fun setPaymentItemsAdapterEstimate() {
    //        if (rvPaymentItems.layoutManager == null) rvPaymentItems.layoutManager =
    //            LinearLayoutManager(requireContext())
    //
    //        if ((paymentsSharedViewModel.provideOrderEstimateItemsResponse().value?.size ?: 0) > 3) {
    //            llViewMore.visibility = View.VISIBLE
    //            paymentsSharedViewModel.provideOrderEstimateItemsResponse().value?.toMutableList()
    //                ?.subList(0, 2)?.let {
    //                    rvPaymentItems.adapter =
    //                        PaymentEstimateItemsAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
    //                            getString(R.string.currency_symbol_default)), it)
    //                }
    //        } else {
    //            llViewMore.visibility = View.GONE
    //            paymentsSharedViewModel.provideOrderEstimateItemsResponse().value?.toMutableList()
    //                ?.let {
    //                    rvPaymentItems.adapter =
    //                        PaymentEstimateItemsAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
    //                            getString(R.string.currency_symbol_default)), it)
    //                }
    //        }
    //    }

    private fun setPaymentItemsAdapterInclusive() = binding.apply {
        if (rvPaymentItems.layoutManager == null) rvPaymentItems.layoutManager =
            LinearLayoutManager(requireContext())

        if (paymentsSharedViewModel.provideOrderInclusionItemsResponse().size > 3) {
            llViewMore.visibility = View.VISIBLE
            rvPaymentItems.adapter =
                paymentsSharedViewModel.provideOrderInclusionItemsResponse().toMutableList()
                    .subList(0, 2).let {
                        PaymentInclusionsItemsAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                            getString(R.string.currency_symbol_default)), it)
                    }
            orderInclusionItems =
                paymentsSharedViewModel.provideOrderInclusionItemsResponse().toMutableList()
        } else {
            llViewMore.visibility = View.GONE
            rvPaymentItems.adapter =
                paymentsSharedViewModel.provideOrderInclusionItemsResponse().toMutableList().let {
                    PaymentInclusionsItemsAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default)), it)
                }
        }
        isRetailerItemsSet = true
    }

    private fun showEnterUpiDialog() {
        if (isVisible) {
            if (!isUpiAsked) {
                if (!isEditUpiOpen) {
                    activity?.let {
                        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.EDIT_UPI_ID,
                            Bundle().apply {
                                putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_PAYMENT)
                            },
                            object : ActionListener {
                                @RequiresApi(Build.VERSION_CODES.P)
                                override fun onActionItem(extra: Any?, extra2: Any?) {
                                    isEditUpiOpen = false
                                    upiId = (extra as? String) ?: ""
                                    setupQRCode(upiId)
                                    showLoader()
                                    viewModel.saveUPIId(SaveUPIIdRequest(upiId), true)
                                }
                            })?.let { baseFragment ->
                            baseFragment.show(it.supportFragmentManager,
                                baseFragment.javaClass.name)
                        }
                    }
                    isEditUpiOpen = true
                }
                isUpiAsked = true
            } else {
                binding.cvScanAndPay.visibility = View.GONE
                binding.cvPay.visibility = View.GONE
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun setupQRCode(UPI_ID: String) = binding.apply {
        cvScanAndPay.makeVisible(true)
        cvQRNotAvailable.makeVisible(false) // Sending side
        val data: ByteArray = BuildConfig.SHA_256.encodeToByteArray()
        val base64: String = java.util.Base64.getEncoder().encodeToString(data)
        val url = "upi://pay?pa=" +   // payment method.
                UPI_ID +         // VPA number.
                "&am=" + getPaymentResponse[0].paymentDetails?.dueAmount +       // this param is for fixed amount (non editable).
                "&pn=${viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME)}" +      // to showing your name in app.
                "&cu=INR" +                  // Currency code.
                "&mode=02" +                 // mode O2 for Secure QR Code.
                "&orgid=189999" +            //If the transaction is initiated by any PSP app then the respective orgID needs to be passed.
                "&sign=" + base64

        try {
            val bitmap = CommonUtils.textToImageEncode(url)
            imgQRCode.setImageBitmap(bitmap)
            imgQRCode.invalidate()
        } catch (e: WriterException) {
            e.printStackTrace()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnAddPayment, R.id.btnAddPayment2 -> {
                onAddPaymentClick(false)
            }

            R.id.btnMarkComplete -> {
                onMarkCompleteClick()
            }

            R.id.llViewMore -> {
                if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                        AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.RETAILER_CONSTANT) {
                    binding.llViewMore.visibility = View.GONE
                    binding.rvPaymentItems.adapter =
                        PaymentInclusionsItemsAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                            getString(R.string.currency_symbol_default)), orderInclusionItems)
                    (binding.rvPayments.adapter as? PaymentInclusionsItemsAdapter)?.notifyDataSetChanged()
                }
            }

            R.id.btnGiveCredit -> {
                if (getPaymentResponse[0].paymentDetails?.dueAmount ?: 0.0 == 0.0) {
                    onMarkCompleteClick()
                } else {
                    onAddPaymentClick(true)
                }
            }
            R.id.btnSetupNow->{
                isUpiAsked = false
                isEditUpiOpen = false
                showEnterUpiDialog()
            }
        }
    }

    private fun onMarkCompleteClick() {
        if (::getPaymentResponse.isInitialized && getPaymentResponse.size > 0) {
            if (getPaymentResponse[0].paymentDetails?.dueAmount ?: 0.0 < 1) {
                showLoader()
                viewModel.markComplete()
                firebaseMarkCompleteOrderEvent()
            } else CommonUtils.showToast(requireContext(),
                getString(R.string.alert_complete_payment),
                true)

        }
    }

    private fun onAddPaymentClick(isCredit: Boolean) {
        if (::getPaymentResponse.isInitialized) {
            var paymentModel = AddPaymentRequest()
            paymentModel.orderId = OrderDetailWrapperFragment.orderId
            activity?.let {
                FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.ADD_PAYMENT_FRAGMENT,
                    Bundle().apply {
                        putParcelable(AppENUM.IntentKeysENUM.PAYMENT_MODEL, paymentModel)
                        putString(AppENUM.DUE_AMOUNT,
                            getPaymentResponse[0].paymentDetails?.dueAmount.toString())
                        if (shopType == AppENUM.RefactoredStrings.RETAILER_CONSTANT) {
                            if (isCredit) {
                                putBoolean(AppENUM.IS_CREDIT, true)
                            }
                        }
                    },
                    object : ActionListener {
                        override fun onActionItem(extra: Any?, extra2: Any?) {
                            paymentModel = (extra as AddPaymentRequest)
                            firebaseEventAddPayment(paymentModel)
                            showLoader()
                            viewModel.addPaymentAPI(paymentModel)
                        }

                    })?.let { baseFragment ->
                    baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
                }
            }
        }
    }

    private fun firebaseEventAddPayment(paymentModel: AddPaymentRequest) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_PAYMENT)
            putDouble(FirebaseAnalyticsLog.FirebaseEventNameENUM.PAYMENT_AMOUNT,
                paymentModel.amount ?: 0.0)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.MOBILE_NUMBER, paymentModel.mobile)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.MODE_OF_PAYMENT,
                paymentModel.transactionMode)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_ADD_PAYMENT,
                this)
        }
    }

    private fun firebaseMarkCompleteOrderEvent() { // firebase event
        val data = OrderDetailWrapperFragment.provideOrderDetails()
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_PAYMENT)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_NAME,
                data?.customerDetails?.name)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_PHONE,
                data?.customerDetails?.mobile)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_EMAIL,
                data?.customerDetails?.email)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_ADDRESS,
                data?.customerDetails?.address)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_CAR_ODO,
                data?.carDetails?.odometer)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_CAR_BRAND,
                data?.carDetails?.car)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_CAR_REG,
                data?.carDetails?.registrationNumber)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.BILL_TYPE, data?.billType)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.TAX_TYPE, data?.taxType)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.GSTIN_NUM,
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_GSTIN))

            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_MARK_COMPLETE,
                this)
        }
    }

    fun onEditUpiDismiss() {
        isEditUpiOpen = false
        binding.cvScanAndPay.visibility = View.GONE
        binding.cvPay.visibility = View.GONE
    }

    fun updateScreen() {
        showLoader()
        viewModel.getPaymentDetails()
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is RetailerInclusionsAddedEvent -> {
                isRetailerItemsSet = false
                setRetailerItems()
            }
        }
    }

}