package whitelisted.garage.view.fragments.manageEmployee

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.EmployeePerformanceResponse
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentEmployeePerformanceBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.ChartSetup
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.viewmodels.ManageEmployeeFragmentViewModel
import java.text.SimpleDateFormat
import java.util.*

class EmployeePerformanceFragment : BaseFragment() {

    private var employeeId = ""
    private val viewModel: ManageEmployeeFragmentViewModel by viewModel()
    private val binding by dataBinding<FragmentEmployeePerformanceBinding>(R.layout.fragment_employee_performance)
    private var month = 0;
    private var year = 0
    private var chartSetup: ChartSetup? = null
    private lateinit var yearsPopupWindow: PopupWindow
    private lateinit var monthsPopupWindow: PopupWindow
    private val years by lazy { arrayOfYears() }
    private var isInitialized = false


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        chartSetup =
            ChartSetup(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol))
        employeeId = arguments?.getString("employee_id", "") ?: ""
        initializeViews()

    }

    private fun getEmployeeResponse() = viewLifecycleOwner.lifecycleScope.launch {
        showLoader()
        val response = viewModel.getEmployeePerformance(employeeId, month, year)
        response?.let { fillUpCharts(it) }
        hideLoader()
    }

    private fun fillUpCharts(it: EmployeePerformanceResponse) {
        setPieData(arrayListOf(it.monthData.wipOrder,
            it.monthData.openOrder,
            it.monthData.completeOrder,
            it.monthData.readyForDelivery))

        binding.tvOrdersCompleted.text = it.monthData.completeOrder.toString()
        binding.tvOrdersDelivered.text = it.monthData.readyForDelivery.toString()
        binding.tvOrdersOpen.text = it.monthData.openOrder.toString()
        binding.tvOrdersWip.text = it.monthData.wipOrder.toString()

        if (!isInitialized) {

            val monthsList = if (month > 5) arrayOfMonths().slice(IntRange(month - 5,
                month)) else arrayOfMonths().toList()

            val countsList = if (month > 5) it.yearCount.subList(month - 5, month) else it.yearCount
            chartSetup?.setUpBarChart(binding.bcNoOfOrders, countsList, monthsList)

            val amountList =
                if (month > 5) it.yearAmount.subList(month - 5, month) else it.yearAmount
            chartSetup?.setupLineChart(binding.lineChartOrderValue, amountList, monthsList)

            isInitialized = true
        }

    }

    private fun initializeViews() {
        setupYearsSpinner(); setupMonthsSpinner()

        binding.yearsSpinner.setOnClickListener {
            yearsPopupWindow.showAsDropDown(it)
            yearsPopupWindow.update()
        }

        binding.monthsSpinner.setOnClickListener {
            monthsPopupWindow.showAsDropDown(it)
            monthsPopupWindow.update()
        }
        getEmployeeResponse()
    }


    private fun setPieData(data: List<Int>) {
        chartSetup?.setUpPieChart(binding.pieOrdersFEP, data)
    }


    private fun setYearSelection(it: Int) {
        binding.yearsSpinner.text = it.toString()
        year = it
        getEmployeeResponse()
        yearsPopupWindow.dismiss()
    }

    private fun setMonthSelection(it: Int) {
        binding.monthsSpinner.text = arrayOfMonths()[it - 1]
        month = it; getEmployeeResponse()
        monthsPopupWindow.dismiss()
    }

    private fun setupYearsSpinner() {

        val inflater = requireContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_spinner, null)
        val recyclerView = view.findViewById<RecyclerView>(R.id.rvWorkDone)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        val adapter = CommonUtils.spinnerRecyclerAdapter(years) {
            setYearSelection(years[it].toInt())
        }
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged() //        view.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        yearsPopupWindow = PopupWindow(view,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            true).apply {
            isOutsideTouchable = true
            isFocusable = true
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        binding.yearsSpinner.text = years[0]
        year = years[0].toInt()
    }

    private fun setupMonthsSpinner() {

        val currentMonth = SimpleDateFormat("MM").format(Date()).toInt() - 1
        val inflater =
            requireContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_spinner, null)
        val recyclerView = view.findViewById<RecyclerView>(R.id.rvWorkDone)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        val adapter =
            CommonUtils.spinnerRecyclerAdapter(arrayOfMonths().slice(IntRange(0, currentMonth))) {
                setMonthSelection(it + 1)
            }
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged() //        view.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        monthsPopupWindow = PopupWindow(view,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            true).apply {
            isOutsideTouchable = true
            isFocusable = true
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        binding.monthsSpinner.text = arrayOfMonths()[currentMonth]
        month = currentMonth + 1
    }

    private fun arrayOfMonths() =
        arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")

    private fun arrayOfYears(): List<String> {
        val currentYear = SimpleDateFormat("YYYY").format(Date()).toInt()
        val years = arrayListOf<String>().apply {
            (2022..currentYear).forEach {
                add(0, it.toString())
            }
        }
        return years
    }
}