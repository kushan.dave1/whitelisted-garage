package whitelisted.garage.view.fragments.sparesShop

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import whitelisted.garage.R
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.CustomProductFragmentTabBinding
import whitelisted.garage.databinding.FragmentSelectCategoryOrBrandBinding
import whitelisted.garage.utils.CommonUtils.makeVisible

class SelectCategoryOrBrandFragment : BaseFragment() {

    private val binding by viewBinding2(FragmentSelectCategoryOrBrandBinding::inflate)
    private lateinit var titles: Array<String>
    private lateinit var fragments: Array<Fragment>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titles = arrayOf(
            requireActivity().getString(R.string.categories),
            requireActivity().getString(R.string.brands)
        )
        fragments = arrayOf(
            SelectCategoryFragment(false).apply {
                arguments = this@SelectCategoryOrBrandFragment.arguments
            },
            SelectBrandFragment()
        )
        setupViews()
    }

    private fun setupViews() = binding?.apply {
        imgBackFSC.setOnClickListener { popBackStackAndHideKeyboard() }
        vpPF.adapter = ProductViewPagerAdapter(this@SelectCategoryOrBrandFragment)
        vpPF.isUserInputEnabled = true
        vpPF.offscreenPageLimit = 2

        TabLayoutMediator(tlPF, vpPF) { tab, position ->

            (vpPF.adapter as? ProductViewPagerAdapter)?.let {
                tab.customView = CustomProductFragmentTabBinding.inflate(
                    LayoutInflater.from(context),
                    tlPF,
                    false
                ).apply {
                    tabTitleProduct.text = titles[position]
                    setSelected(position == 0)
                }.root
            }
        }.attach()

        tlPF.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.customView?.let { CustomProductFragmentTabBinding.bind(it) }?.setSelected()
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {}
            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab?.customView?.let { CustomProductFragmentTabBinding.bind(it) }
                    ?.setSelected(false)
            }
        })
        //setTabLayoutHeight()
    }

    private fun setTabLayoutHeight() = lifecycleScope.launch {
        binding?.tlPF?.let {
            delay(300)
            it.layoutParams = ViewGroup.LayoutParams(it.width, it[0].height)
        }
    }

    internal inner class ProductViewPagerAdapter(fragment: BaseFragment) :
        FragmentStateAdapter(fragment) {

        override fun getItemCount() = titles.size

        override fun createFragment(position: Int) = fragments[position]
    }

    private fun CustomProductFragmentTabBinding.setSelected(isSelected: Boolean = true) {

        productFragmentTabIndicator.setBackgroundColor(
            resources.getColor(
                if (isSelected) R.color.colorAccent else R.color.screen_bg, resources.newTheme()
            )
        )
        tabTitleProduct.setTextColor(
            resources.getColor(
                if (isSelected) R.color.colorAccent else R.color.black_trans,
                resources.newTheme()
            )
        )
        tabTitleProduct.typeface = ResourcesCompat.getFont(
            requireContext(),
            if (isSelected) R.font.gilroy_semibold else
                R.font.gilroy_medium
        )
    }

}