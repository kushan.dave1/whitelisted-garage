package whitelisted.garage.view.fragments.myBusiness

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import whitelisted.garage.R
import whitelisted.garage.api.response.Preview
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentGmbbPreviewBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.view.adapter.GmbPreviewAdapter
import whitelisted.garage.view.adapter.RecyclerProgressAdpater
import kotlin.math.abs

class GMBPreviewFragment : BaseFragment() {

    private var preview: Preview? = null
    private var isGMB = true
    private val binding by viewBinding(FragmentGmbbPreviewBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_G_COMBO_PREVIEW,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_G_COMBO_INFO)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickLester()
        getData()
    }

    private fun getData() {
        arguments?.let {
            preview = it.getParcelable(AppENUM.GMB)
            isGMB = it.getBoolean(AppENUM.IS_FROM_GMB, true)
        }
        if (!isGMB) {
            binding.tvHeader.text = getString(R.string.webiste_preview)
            binding.tvBus.text = getString(R.string.busness_will_look)
            binding.tvUnLockText.text = getString(R.string.unlock_webbsite_cre)
        }
        binding.rvGMBPreview.apply {
            clipToPadding = false
            clipChildren = false
            offscreenPageLimit = 3
            getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        }

        CompositePageTransformer().apply {
            addTransformer(MarginPageTransformer(64))
            addTransformer { page, position ->
                val r = 1 - abs(position)
                page.scaleY = .85f + r * .15f
            }
            binding.rvGMBPreview.setPageTransformer(this)
        }
        binding.rvGMBPreview.adapter = GmbPreviewAdapter()
        (binding.rvGMBPreview.adapter as? GmbPreviewAdapter)?.isGMB = isGMB
        preview?.let {
            it.previesImages?.let { (binding.rvGMBPreview.adapter as? GmbPreviewAdapter)?.setData(it) }
            val data = mutableListOf<Boolean>()
            for (i in 0 until (it.previesImages?.size ?: 0)) {
                data.add(false)
            }
            data[0] = true
            binding.progressRecycler.adapter = RecyclerProgressAdpater(data)
        }

        binding.rvGMBPreview.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            // This method is triggered when there is any scrolling activity for the current page
            override fun onPageScrolled(position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                val newData = mutableListOf<Boolean>()
                for (i in 0 until (preview?.previesImages?.size ?: 0)) {
                    if (i == position) {
                        newData.add(i, true)
                    } else {
                        newData.add(i, false)
                    }
                }
                (binding.progressRecycler.adapter as RecyclerProgressAdpater).setData(newData)
            }
        })
    }

    private fun clickLester() {
        binding.btnLockGMB.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnLockGMB -> {
                popBackStackAndHideKeyboard()
            }
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
        }
    }

}