package whitelisted.garage.view.fragments.referAndEarn

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.style.ImageSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import androidx.core.content.ContextCompat
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.AvailReferralRequest
import whitelisted.garage.api.response.GetReferralCodeResponse
import whitelisted.garage.api.response.ReferralText
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentReferAndEarnBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.viewmodels.ReferAndEarnFragmentViewModel


class ReferAndEarnFragment : BaseFragment() {

    private val binding by viewBinding2(FragmentReferAndEarnBinding::inflate)
    private val viewModel: ReferAndEarnFragmentViewModel by viewModel()
    var response: GetReferralCodeResponse? = null

    override fun onCreateView(inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        return super.onCreateView(inflater, container, savedInstanceState)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
        observeResponses()
        showLoader()
        viewModel.getReferralCode()
    }

    private fun observeResponses() {
        viewModel.provideReferralCode().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    response = it.body.data
                    setUpData(it.body.data)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
        viewModel.provideAvailReferralCode().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    showLoader()
                    viewModel.getReferralCode()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }

            requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        }
    }

    private fun setUpData(data: GetReferralCodeResponse) = binding?.apply {
        tvRefCode.text = data.refCode
        if (data.availReleralCode.isNullOrEmpty()) {
            tvHrAvailRef.visibility = View.VISIBLE
            llAvailRef.visibility = View.VISIBLE
        } else {
            tvHrAvailRef.visibility = View.GONE
            llAvailRef.visibility = View.GONE
        }

        headerText(tvInfoHeader, data.referral_text)
    }


    private fun setupScreen() = binding?.apply {

        imgBack.setOnClickListener(this@ReferAndEarnFragment)
        imgQNA.setOnClickListener(this@ReferAndEarnFragment)
        tvSubmit.setOnClickListener(this@ReferAndEarnFragment)
        imgShareRef.setOnClickListener(this@ReferAndEarnFragment)
        etRefCode.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().isNotEmpty()) {
                    tvSubmit.setTextColor(ContextCompat.getColor(requireContext(),
                        R.color.colorAccent))
                } else {
                    tvSubmit.setTextColor(ContextCompat.getColor(requireContext(),
                        R.color.order_inclusions_accent))
                }
            }
        })

        //        etRefCode.setOnTouchListener { view, motionEvent ->
        //            etRefCode.requestLayout()
        //            requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_UNSPECIFIED)
        //            false
        //        }

    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
            R.id.imgShareRef -> {
                shareReferral()
            }
            R.id.tvSubmit -> {

                if (binding?.etRefCode?.text.toString().isNotEmpty()) {
                    showLoader()
                    viewModel.getReferralCode(AvailReferralRequest().apply {
                        this.ref_code = binding?.etRefCode?.text.toString()
                    })
                } else {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.referal_code_nnot_empty))
                }
            }
            R.id.imgQNA -> {
                val bundle = Bundle().apply {
                    putParcelableArrayList(AppENUM.FAQ, response?.faq as ArrayList)
                }
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.FQA_S, bundle),
                    target = R.id.fragment_container_2)
            }
        }
    }

    private fun shareReferral() {
        if (!response?.refral_msg.isNullOrEmpty()) {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, response?.refral_msg)
                type = "text/plain"
            }
            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

    }

    private fun headerText(textView: TextView, referralText: ReferralText?) {
        binding?.tvDesc?.text = referralText?.desc ?: ""
        val ssb = SpannableStringBuilder()
        ssb.append(getString(R.string.earn))
        ssb.append(" ")
        ssb.append(" ")
        val length = ssb.length
        val d1 = ContextCompat.getDrawable(requireContext(), R.drawable.ic_ref_coin)
        d1?.setBounds(0, 0, d1.intrinsicWidth, d1.intrinsicHeight)
        val span1 = ImageSpan(d1!!, ImageSpan.ALIGN_BASELINE)
        ssb.setSpan(span1, length - 1, length, 0)
        ssb.append(" ")
        ssb.append(referralText?.header?.replace(getString(R.string.earn), ""))
        textView.text = ssb
    }
}