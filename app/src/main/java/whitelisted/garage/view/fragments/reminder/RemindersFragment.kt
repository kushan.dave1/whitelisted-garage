package whitelisted.garage.view.fragments.reminder

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.RemindersItem
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentReminderBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.RemindersAdapter
import whitelisted.garage.viewmodels.RemindersViewModel

class RemindersFragment : BaseFragment(), AdapterView.OnItemSelectedListener, TextWatcher {

    private val binding by viewBinding2(FragmentReminderBinding::inflate)
    private val viewModel: RemindersViewModel by viewModel()
//    private val dashboardSharedViewModel: DashboardSharedViewModel by sharedViewModel()
    private lateinit var remindersList: MutableList<RemindersItem>
    private lateinit var remindersAdapter: RemindersAdapter
    private val filterList = AppENUM.FILTER_LIST_REMINDER
    private var selectedType = 0
    private var selectedTypeString = AppENUM.FilterString.ALL
    private var periodType = filterList[0]
    private var restrictionDate = ""
    private var isSpinnerActive = false
    private var alteredPosition = 0
    private var isUpdated = false


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
        setUpData()

    }

    private fun setUpData() {
        ArrayAdapter(requireContext(),
            R.layout.layout_drop_down,
            resources.getStringArray(R.array.rem_filter_list)).apply {
            binding?.filterSpinner?.adapter = this
        }
    }

    private fun setupScreen() {
        clickListener()
        observeReminderListResponse()
        observeSendReminderResponse()
        observeGetGoCOinResponse()
        getData()
    }

    private fun getData() {
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                binding?.etSearch?.hint = getString(R.string.search_for_phone_name)
            }
        }
        showLoader()
        viewModel.getGoCoinBalance()
        viewModel.getRemindersList(selectedTypeString, periodType)
        firebaseEvent(AppENUM.View_Reminder_Data, periodType, selectedTypeString)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun clickListener() = binding?.apply {
        imgBack.setOnClickListener(this@RemindersFragment)
        llViewGOCOIN.setOnClickListener(this@RemindersFragment)
        tvAll.setOnClickListener(this@RemindersFragment)
        tvSent.setOnClickListener(this@RemindersFragment)
        tvScheduled.setOnClickListener(this@RemindersFragment)
        llCreateOrder.setOnClickListener(this@RemindersFragment)
        etSearch.addTextChangedListener(this@RemindersFragment)
        filterSpinner.onItemSelectedListener = this@RemindersFragment
        filterSpinner.setOnTouchListener { _, _ ->
            isSpinnerActive = true
            false
        }
    }

    private fun observeGetGoCOinResponse() {
        viewModel.provideGetGoCoinResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.let { data ->
                        binding?.tvGoCoinBalance?.text = (data.gocoinBalance ?: 0).toString()
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeSendReminderResponse() {
        viewModel.provideSendReminderResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    isUpdated = true
                    showLoader()
                    viewModel.getRemindersList(selectedTypeString, periodType)
                    viewModel.getGoCoinBalance()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeReminderListResponse() {
        viewModel.provideRemindersListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.reminders != null) {
                        restrictionDate = it.body.data.reminder_restriction ?: ""
                        remindersList = it.body.data.reminders.toMutableList()
                        if (remindersList.isEmpty()) {
                            binding?.rvReminders.makeVisible(false)
                            binding?.llNoDataScreen.makeVisible(true)
                        } else {
                            binding?.rvReminders?.makeVisible(true)
                            binding?.llNoDataScreen?.makeVisible(false)
                        }
                        setRemindersAdapter()
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setRemindersAdapter() {
        binding?.rvReminders?.layoutManager = LinearLayoutManager(requireContext())
        remindersAdapter = RemindersAdapter(remindersList, this)
        remindersAdapter.restrictionDate = restrictionDate
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                remindersAdapter.viewType = 1
            }
            else -> {
                remindersAdapter.viewType = 0
            }
        }
        binding?.rvReminders?.adapter = remindersAdapter
        if (remindersList.size == 1) binding?.tvTotleCustomer?.text =
            "${remindersList.size}  ${getString(R.string.customer)}"
        else binding?.tvTotleCustomer?.text = "${remindersList.size}  ${getString(R.string.customers)}"
        if (isUpdated) {
            binding?.rvReminders?.scrollToPosition(alteredPosition)
        }
        isUpdated = false
    }


    private fun showSelectDateDialog(startDate: String?, orderId: String?) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_DATE_REMINDER,
            Bundle().apply {
                putString(AppENUM.START_DATE, startDate)
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    firebaseEvent(AppENUM.SCHEDULE_REMINDER, periodType, selectedTypeString)
                    showLoader()
                    viewModel.scheduleReminder(orderId, extra as? String)
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnSendReminder -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    showLoader()
                    viewModel.sendReminder(remindersList[position].orderId)
                    firebaseEvent(AppENUM.SEND_REMINDER, periodType, selectedTypeString)
                }
            }
            R.id.btnScheduled -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    showSelectDateDialog(remindersList[position].createdAt,
                        remindersList[position].orderId)
                }
            }
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.llViewGOCOIN -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, null))
            }
            R.id.tvSent -> updateSelectorAndGetList(1)
            R.id.tvScheduled -> updateSelectorAndGetList(2)
            R.id.tvAll -> updateSelectorAndGetList(0)
            R.id.llCreateOrder -> {
                handleCreateOrder()
            }

        }
    }

    private fun handleCreateOrder() {
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                        }),
                    target = R.id.fragment_container_2)
            }
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                        }),
                    target = R.id.fragment_container_2)
            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_NEW_ORDER,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                        }),
                    target = R.id.fragment_container_2)
            }
            else -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                        }),
                    target = R.id.fragment_container_2)
            }
        }
    }

    private fun updateSelectorAndGetList(type: Int)  = binding?.apply {
        when (type) {
            1 -> {
                if (selectedType != 1) {
                    selectedType = 1
                    selectedTypeString = AppENUM.FilterString.SENT
                    tvSent.background =
                        CommonUtils.getDrawable(requireContext(), R.drawable.bg_selected_gray_2)
                    tvScheduled.setBackgroundColor(CommonUtils.getColor(requireContext(),
                        R.color.white))
                    tvAll.setBackgroundColor(CommonUtils.getColor(requireContext(), R.color.white))
                }
            }
            2 -> {
                if (selectedType != 2) {
                    selectedType = 2
                    selectedTypeString = AppENUM.FilterString.SCHEDULE
                    tvScheduled.background =
                        CommonUtils.getDrawable(requireContext(), R.drawable.bg_selected_gray_2)
                    tvSent.setBackgroundColor(CommonUtils.getColor(requireContext(), R.color.white))
                    tvAll.setBackgroundColor(CommonUtils.getColor(requireContext(), R.color.white))
                }
            }
            0 -> {
                if (selectedType != 0) {
                    selectedType = 0
                    selectedTypeString = AppENUM.FilterString.ALL
                    tvAll.background =
                        CommonUtils.getDrawable(requireContext(), R.drawable.bg_selected_gray_2)
                    tvScheduled.setBackgroundColor(CommonUtils.getColor(requireContext(),
                        R.color.white))
                    tvSent.setBackgroundColor(CommonUtils.getColor(requireContext(), R.color.white))
                }
            }
        }
        showLoader()
        viewModel.getRemindersList(selectedTypeString, periodType)
        firebaseEvent(AppENUM.View_Reminder_Data, periodType, selectedTypeString)
    }


    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (isSpinnerActive) {
            isSpinnerActive = false
            periodType = filterList[position]
            showLoader()
            viewModel.getRemindersList(selectedTypeString, periodType)
            firebaseEvent(AppENUM.View_Reminder_Data, periodType, selectedTypeString)
        }
    }


    override fun onNothingSelected(parent: AdapterView<*>?) { //
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { //
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { //
    }

    override fun afterTextChanged(s: Editable?) {
        if (s.toString().length > 1) {
            showLoader()
            viewModel.searchRemindersList(s.toString())
        } else if (s.toString().isEmpty()) {
            showLoader()
            viewModel.getRemindersList(selectedTypeString, periodType)
        }
    }

    private fun firebaseEvent(actionType: String, filterType: String, selectionType: String) {
        var selection = ""
        when (selectionType) {
            AppENUM.FilterString.ALL -> {
                selection = AppENUM.ALL
            }
            AppENUM.FilterString.SCHEDULE -> {
                selection = AppENUM.SCHEDULE
            }
            AppENUM.FilterString.SENT -> {
                selection = AppENUM.SENT
            }
        }
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_REMINDER)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.ACTION_TYPE, actionType)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FILTER_TYPE, filterType)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.SELECTION_TYPE, selection)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_TAP_REMINDER,
                this)
        }
    }
}