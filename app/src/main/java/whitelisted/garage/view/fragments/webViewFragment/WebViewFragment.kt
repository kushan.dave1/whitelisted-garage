package whitelisted.garage.view.fragments.webViewFragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentWebViewBinding
import whitelisted.garage.utils.AppENUM

class WebViewFragment : BaseFragment() {

    private val binding by viewBinding(FragmentWebViewBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupScreen() {
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.settings.allowContentAccess = true
        binding.webView.settings.domStorageEnabled = true
        binding.webView.settings.mixedContentMode = 0;
        binding.webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        binding.webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url ?: "")
                return true
            }
        }
        binding.webView.loadUrl(arguments?.getString(AppENUM.IntentKeysENUM.WEB_VIEW_URL) ?: "")
    }

}