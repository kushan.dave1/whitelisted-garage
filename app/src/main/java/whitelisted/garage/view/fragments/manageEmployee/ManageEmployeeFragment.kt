package whitelisted.garage.view.fragments.manageEmployee

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.PopupWindow
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewbinding.ViewBinding
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.EmployeeListResponseModel
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.base.ViewBindingInflater
import whitelisted.garage.databinding.FragmentManageEmployeeBinding
import whitelisted.garage.eventBus.EscalationPopup
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.CommonUtils.spinnerRecyclerAdapter
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.EmployeesAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.ManageEmployeeFragmentViewModel
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.*

class ManageEmployeeFragment : BaseFragment(), SwipeRefreshLayout.OnRefreshListener {

    private val binding by viewBinding(FragmentManageEmployeeBinding::inflate)

    private var deleteEmployeeCardPosition = -1
    private val viewModel: ManageEmployeeFragmentViewModel by viewModel()
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var workShopId: String = ""
    private var workShopType: String = ""
    private lateinit var employeeList: MutableList<EmployeeListResponseModel>
    private lateinit var employeeAdapter: EmployeesAdapter
    private var workShopList: List<WorkshopListResponseModel>? = null
    private lateinit var workshopsPopupWindow: PopupWindow

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
        setGarageName()
        getManageEmployee()
        showLoader()
        viewModel.getWorkshopsListFromRoom()
        observeManageEmployee()

    }

    private fun setGarageName() {
        viewModel.provideWorkshopListResponseFromRoom().observe(viewLifecycleOwner) {
            workShopList = it
            val workshopName =
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME, "")
            employeeAdapter.setWorkshopName(workshopName)
            setupWorkshopFilter()
        }

    }

    private fun onSpinnerItemSelected(position: Int) {
        workShopId = workShopList?.get(position)?.workshopId.toString()
        workShopType = workShopList?.get(position)?.types.toString()
        employeeAdapter.setWorkshopName(workShopList?.get(position)?.name)
        showLoader()
        getManageEmployee()
    }


    private fun getManageEmployee() {
        viewModel.getManageEmployeeData(workShopId)
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        if (event is EscalationPopup && event.isRefresh) {
            refreshScreen()
        }
    }


    private fun observeManageEmployee() {
        viewModel.observeManageEmployee().observe(viewLifecycleOwner) {
            binding.pbManageEmployee.makeVisible(false)
            binding.swipeRefresh.isRefreshing = false
            hideLoader()
            when (it) {
                is Result.Success -> {
                    manageEmployeeSuccess(it)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

        viewModel.provideDeleteWorkshopResponse().observe(viewLifecycleOwner) {
            binding.pbManageEmployee.makeVisible(false)
            binding.swipeRefresh.isRefreshing = false
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (deleteEmployeeCardPosition != -1) {
                        employeeList.removeAt(deleteEmployeeCardPosition)
                        employeeAdapter.notifyItemRemoved(deleteEmployeeCardPosition)

                        if (employeeList.size == 0) binding.tvNoDataFound.visibility = View.VISIBLE
                    }
                    showLoader()
                    getManageEmployee()

                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun manageEmployeeSuccess(it: Result.Success<ServerResponse<List<EmployeeListResponseModel>>>) {
        if (::employeeList.isInitialized && employeeList.size == it.body.data.size) { //
        } else {
            employeeList.clear()
            employeeAdapter.notifyDataSetChanged()
            employeeList.addAll(it.body.data.toMutableList())
            if (employeeList.size > 0) {
                binding.tvNoDataFound.makeVisible(false)
                employeeAdapter.notifyDataSetChanged()
            } else {
                binding.tvNoDataFound.makeVisible(true)
            }

        }
    }

    private fun refreshScreen() {
        binding.swipeRefresh.isRefreshing = true
        onRefresh()
    }


    private fun setupScreen() {
        binding.imgBack.setOnClickListener(this)
        binding.swipeRefresh.setOnRefreshListener(this)
        setEmployeesAdapter()

        workShopId = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID, "")
        workShopType = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
    }

    private fun setEmployeesAdapter() {
        employeeList = mutableListOf()
        employeeAdapter = EmployeesAdapter(employeeList, this)
        binding.rvEmployees.adapter = employeeAdapter
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.llAddEmployee -> {
                if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                        false)) {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.ADD_EDIT_EMPLOYEE,
                            Bundle().apply {
                                putString(AppENUM.IntentKeysENUM.WORKSHOP_ID, workShopId)
                                putString(AppENUM.IntentKeysENUM.SHOP_TYPE, workShopType)
                            }))
                } else {
                    if (::employeeList.isInitialized && (employeeList.size - 1) < (dashboardViewModel.screensConfigResponse.value?.membershipConfig?.empAccount
                            ?: 0)) {
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.ADD_EDIT_EMPLOYEE,
                                Bundle().apply {
                                    putString(AppENUM.IntentKeysENUM.WORKSHOP_ID, workShopId)
                                    putString(AppENUM.IntentKeysENUM.SHOP_TYPE, workShopType)
                                }))
                    } else {
                        fireFeatureLockedEvent()
                        requireActivity().let {
                            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT, params = Bundle().apply {
                                putString(AppENUM.PRO_LOCKED_ALERT_MSG, dashboardViewModel.screensConfigResponse.value?.membershipConfig?.empAccountMsg)
                            })
                                ?.let { baseFragment ->
                                    baseFragment.show(it.supportFragmentManager,
                                        baseFragment.javaClass.name)
                                }
                        }
                    }
                }
            }
            R.id.btnMore -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    showPopupMenu(v, position)
                    firebaseEmpEvent(employeeList[position],
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_EMP_MAN)
                }
            }

            R.id.workshopAutotv -> showWorkDoneWindow(v)

            R.id.btnViewPerformance -> {
                if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                        false)) {
                    Bundle().apply {
                        putString("employee_id", v.tag as String)
                        CommonUtils.addFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.EMPLOYEE_PERFORMANCE,
                                this))
                    }
                }else {
                    fireFeatureLockedEvent()
                    requireActivity().let {
                        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT, params = Bundle().apply {
                            putString(AppENUM.PRO_LOCKED_ALERT_MSG, dashboardViewModel.screensConfigResponse.value?.membershipConfig?.employeePerformanceMsg)
                        })
                            ?.let { baseFragment ->
                                baseFragment.show(it.supportFragmentManager,
                                    baseFragment.javaClass.name)
                            }
                    }
                }
            }
        }
    }

    private fun showPopupMenu(view: View, position: Int) {
        val data = employeeList[position]
        val popupMenu =
            PopupMenu(ContextThemeWrapper(requireContext(), R.style.BasePopupMenu), view)
        if (data.userRole.equals(AppENUM.OWNER)) {
            popupMenu.menuInflater.inflate(R.menu.edit_menu, popupMenu.menu)
        } else popupMenu.menuInflater.inflate(R.menu.edit_del_menu, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.edit -> {
                    if (data.userRole.equals(AppENUM.OWNER)) {
                        CommonUtils.addFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.PROFILE, null))
                    } else {
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.ADD_EDIT_EMPLOYEE,
                                Bundle().apply {
                                    putParcelable(AppENUM.IntentKeysENUM.SINGLE_PARCELABLE_DATA,
                                        employeeList[position])
                                    putString(AppENUM.IntentKeysENUM.SHOP_TYPE, workShopType

                                    )
                                }))
                    }
                }
                R.id.delete -> {
                    deleteEmployee(position)
                }
            }
            true
        }
        popupMenu.show()
    }

    private fun deleteEmployee(position: Int) {
        lateinit var dialog: AlertDialog
        requireActivity().let {
            AlertDialog.Builder(it).apply {
                setIcon(android.R.drawable.ic_dialog_alert)
                setTitle(getString(R.string.delete))
                setMessage(getString(R.string.are_you_sure_you_want_to_delete_this_nemployee))
                val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                    when (which) {
                        DialogInterface.BUTTON_POSITIVE -> {
                            employeeList[position].userId?.let { it1 ->
                                showLoader()
                                val id = it1.toString()
                                deleteEmployeeCardPosition = position
                                viewModel.deleteEmployee(id)
                                firebaseEmpEvent(employeeList[position],
                                    FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_DELETE_EMPLOYEE)
                            }
                        }
                    }
                }

                setPositiveButton(getString(R.string.delete), dialogClickListener)
                setNeutralButton(getString(R.string.cancel), dialogClickListener)

                dialog = this.create()
                dialog.show()
            }
        }
    }

    private fun firebaseEmpEvent(param: EmployeeListResponseModel, eventName: String) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_MANAGE_EMP)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.EMP_NAME, param.name)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.EMP_EMAIL, param.email)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.EMP_PHONE, param.mobile)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.EMP_ROLE, param.userRole)
            FirebaseAnalyticsLog.trackFireBaseEventLog(eventName, this)
        }
    }

    override fun onRefresh() {
        getManageEmployee()
    }


    private fun showWorkDoneWindow(view: View) {
        workshopsPopupWindow.showAsDropDown(view)
        workshopsPopupWindow.update()
    }

    private fun setupWorkshopFilter() {
        val inflater =
            requireContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_spinner, null)
        val recyclerView = view.findViewById<RecyclerView>(R.id.rvWorkDone)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        val adapter = spinnerRecyclerAdapter(workShopList?.map { it.name }) {
            onSpinnerItemSelected(it)
            workshopsPopupWindow.dismiss()
        }
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged() //        view.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        workshopsPopupWindow = PopupWindow(view,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            true).apply {
            isOutsideTouchable = true
            isFocusable = true
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }


    private fun fireFeatureLockedEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_MANAGE_EMP)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.ERROR_HEADER_PRO,
            bundle)

    }


}