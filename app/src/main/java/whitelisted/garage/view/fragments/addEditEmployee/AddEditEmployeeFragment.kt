package whitelisted.garage.view.fragments.addEditEmployee

import DateUtil
import android.Manifest
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import gun0912.tedimagepicker.builder.TedImagePicker
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.request.AddEditEmployee
import whitelisted.garage.api.response.EmployeeListResponseModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentAddEditEmployeeBinding
import whitelisted.garage.eventBus.EscalationPopup
import whitelisted.garage.eventBus.UpdateHomeScreenEvent
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeBackgroundDrawable
import whitelisted.garage.utils.CommonUtils.isValidEmail
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.DummyResponse
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.viewmodels.AddEditEmployeeViewModel
import java.text.DecimalFormat
import java.util.*


class AddEditEmployeeFragment : BaseFragment(), TextWatcher {

    private val binding by viewBinding(FragmentAddEditEmployeeBinding::inflate)

    private var isEditEmployee = false
    private var editEmployeeId: String? = null
    private var employeeImage: Uri? = null
    private var employeeImageFirebaseUrl = ""
    private var workShopId: String? = null
    private var shopType: String? = null
    private var isImageUploaded = false
    private var storageRef: StorageReference? = null
    private var storage: FirebaseStorage? =
        FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE)

    private val viewModel: AddEditEmployeeViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        setTextWatchers()
        clickListeners()
        observeAddEditEmployee()
        getEditScreenData()
        setFieldAdapters()
    }

    private fun getEditScreenData() {
        arguments?.getParcelable<EmployeeListResponseModel>(AppENUM.IntentKeysENUM.SINGLE_PARCELABLE_DATA)
            ?.let { it ->
                editEmployeeId = it.userId.toString()
                isEditEmployee = true
                binding.etName.setText(it.name)
                binding.atvRole.setText(it.userRole)
                binding.etPhoneNumber.setText(it.mobile)
                storageRef = storage?.reference
                it.image?.let { image ->
                    if (image.isNotEmpty()) {
                        storageRef?.child(image)?.let { sigRef ->
                            try {
                                sigRef.downloadUrl.addOnSuccessListener {
                                    loadCircularImage(it.toString(), binding.imgProfilePicture)
                                }

                            } catch (e: Exception) { //do nothing
                            }
                        }
                    }

                }
                binding.etEmail.setText(it.email)
                val date: String? = it.joiningDate?.let {
                    DateUtil.getFormattedDateUtcToGmt(AppENUM.DateUtilKey.TIME_FORMAT_NZ,
                        AppENUM.DDMMYYYY,
                        it)
                }
                binding.etJoiningDate.setText(date)
                binding.btnAdd.setText(R.string.update)
                binding.tvHeader.setText(R.string.edit_employee)
            }

        workShopId = arguments?.getString(AppENUM.IntentKeysENUM.WORKSHOP_ID, "")
        shopType = arguments?.getString(AppENUM.IntentKeysENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
    }

    private fun observeAddEditEmployee() {
        viewModel.provideAddEmployeeObserver().observe(viewLifecycleOwner) { result ->
            hideLoader()
            when (result) {
                is Result.Success -> {
                    result.body.let {
                        CommonUtils.genericCastOrNull<ServerResponse<DummyResponse>>(it)
                            ?.let { res ->
                                CommonUtils.showToast(requireContext(), res.message, true)
                                popBackStackAndHideKeyboard()
                                EventBus.getDefault().post(EscalationPopup(true))
                            }
                    }

                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                    if (!isEditEmployee) CommonUtils.showReviewDialog(requireActivity())
                }
                is Result.Failure -> {
                    setClickable(true)
                    CommonUtils.showToast(requireContext(), result.errorMessage, true)
                }
            }
        }

        viewModel.provideUploadEmployeeImageResponse()
            .observe(viewLifecycleOwner) { sendSuccessUrl ->
                sendSuccessUrl.isSuccessFailure?.let { isSuccess ->
                    if (isSuccess) {
                        sendSuccessUrl.url?.let {
                            employeeImageFirebaseUrl = it
                        }
                        hitAddEditEmployeeApi()
                    } else {
                        setClickable(true)
                        sendSuccessUrl.url?.let { it1 ->
                            CommonUtils.showToast(requireContext(), it1, true)
                        }
                    }
                }
            }
    }

    private fun clickListeners() {
        binding.imgBack.setOnClickListener(this)
        binding.imgProfilePicture.setOnClickListener(this)
        binding.viewJoiningDateClick.setOnClickListener(this)
        binding.btnAdd.setOnClickListener(this)
    }

    private fun setTextWatchers() {
        binding.tilName.editText?.addTextChangedListener(this)
        binding.tilEmployeeRole.editText?.addTextChangedListener(this)
        binding.tilPhoneNumber.editText?.addTextChangedListener(this)
        binding.tilPassword.editText?.addTextChangedListener(this)
        binding.tilJoiningDate.editText?.addTextChangedListener(this)
    }

    private fun setFieldAdapters() {
        val employeeRolesList = ArrayList<String>()
        when (shopType) {
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                employeeRolesList.add(AppENUM.EMPROLE.SUPERVISOR)
                employeeRolesList.add(AppENUM.EMPROLE.MECHANIC)
            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                employeeRolesList.add(AppENUM.EMPROLE.SUPERVISOR) // employeeRolesList.add(getString(R.string.executive))
            }
            else -> {
                employeeRolesList.add(AppENUM.EMPROLE.SUPERVISOR)
                employeeRolesList.add(AppENUM.EMPROLE.MECHANIC)
                employeeRolesList.add(AppENUM.EMPROLE.DRIVER)
            }
        }

        val adapter = ArrayAdapter(requireContext(),
            R.layout.support_simple_spinner_dropdown_item,
            employeeRolesList)
        (binding.tilEmployeeRole.editText as? AutoCompleteTextView)?.setAdapter(adapter)
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
    }

    override fun afterTextChanged(p0: Editable?) {
        if (shouldEnableButton()) setClickable(true)
        else setClickable(false)
    }

    private fun setClickable(isClickable: Boolean) {
        if (isClickable) binding.btnAdd.changeBackgroundDrawable(requireContext(),
            R.drawable.bg_default_button)
        else binding.btnAdd.changeBackgroundDrawable(requireContext(),
            R.drawable.bg_button_disabled)
        binding.btnAdd.isEnabled = isClickable
        binding.btnAdd.isClickable = isClickable
    }

    private fun shouldEnableButton(): Boolean {
        return if (binding.etName.text.toString().trim().isNotEmpty()) {
            if (binding.atvRole.text.toString().trim().isNotEmpty()) {
                if (binding.etPhoneNumber.text.toString().trim().isNotEmpty()) {
                    return true
                } else false
            } else false
        } else false
    }

    private fun isValidate(): Boolean {
        if (binding.etEmail.text.toString().trim().isNotEmpty() && !binding.etEmail.text.toString()
                .trim().isValidEmail()) {
            CommonUtils.showToast(requireContext(),
                requireContext().getString(R.string.alert_invalid_email))
            return false
        }
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                AppENUM.RefactoredStrings.defaultCurrency) == AppENUM.RefactoredStrings.defaultCurrency && binding.etPhoneNumber.text.toString().length < 10) {
            CommonUtils.showToast(requireContext(),
                requireContext().getString(R.string.alert_invalid_mobile))
            return false
        }
        return true
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.viewJoiningDateClick -> {
                binding.datePicker.makeVisible(true)
                val today = Calendar.getInstance()
                binding.datePicker.init(today.get(Calendar.YEAR),
                    today.get(Calendar.MONTH),
                    today.get(Calendar.DAY_OF_MONTH)

                ) { _, year, month, day ->
                    val f = DecimalFormat("00")
                    val incrementMonth = month + 1
                    ("${f.format(day)}-${f.format(incrementMonth)}-$year").apply {
                        binding.tilJoiningDate.editText?.setText(this)
                    }
                    binding.datePicker.makeVisible(false)
                }
                binding.datePicker.maxDate = today.timeInMillis
            }
            R.id.btnAdd -> {
                if (isValidate() && !isLoading()) {
                    if (!isImageUploaded) {
                        hitAddEditEmployeeApi()
                    } else {
                        setClickable(false)
                        employeeImage?.let {
                            showLoader()
                            viewModel.uploadProfilePicture(requireContext(),
                                binding.etPhoneNumber.text.toString(),
                                it)
                        } ?: run {
                            hitAddEditEmployeeApi()
                        }
                    }
                }
            }
            R.id.imgProfilePicture -> {
                TedPermission.create().setPermissionListener(object : PermissionListener {
                    override fun onPermissionGranted() {
                        TedImagePicker.with(requireContext()).showCameraTile(true).start {
                            employeeImage = it
                            isImageUploaded = true
                            loadCircularImage(employeeImage.toString(), binding.imgProfilePicture)
                        }
                    }

                    override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
                        CommonUtils.showToast(context,
                            resources.getString(R.string.permission_denied))
                    }

                }).setPermissions(Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE).check()

            }
        }
    }

    private fun hitAddEditEmployeeApi() {
        AddEditEmployee().apply {
            name = binding.etName.text.toString().trim()
            userRole = binding.atvRole.text.toString().trim()
            mobile = binding.etPhoneNumber.text.toString().trim()
            email = binding.etEmail.text.toString().trim()
            password = binding.etPassword.text.toString().trim()
            joiningDate = binding.etJoiningDate.text.toString().trim()
            if (employeeImageFirebaseUrl.isNotBlank()) {
                image = employeeImageFirebaseUrl
            }
            showLoader()
            if (isEditEmployee) viewModel.editEmployee(this, editEmployeeId)
            else workShopId?.let { viewModel.addEmployee(it, this) }

            Bundle().apply {
                putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ADD_EMP)
                putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.EMP_NAME, name)
                putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.EMP_EMAIL, email)
                putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.EMP_PHONE, mobile)
                putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.EMP_ROLE, userRole)
                FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_ADD_EMPLOYEE,
                    this)
            }
        }
    }

}