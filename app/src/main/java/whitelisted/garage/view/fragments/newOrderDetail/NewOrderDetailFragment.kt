package whitelisted.garage.view.fragments.newOrderDetail

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.CompoundButton
import android.widget.SeekBar
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.flexbox.*
import com.google.android.gms.location.LocationListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.data.CarNameOrderIdStatusModel
import whitelisted.garage.api.data.CustomerDetailsModel
import whitelisted.garage.api.data.SetOptionsImgStatusObject
import whitelisted.garage.api.request.MyCustomerModel
import whitelisted.garage.api.request.NewOrderRequest
import whitelisted.garage.api.request.UploadItem
import whitelisted.garage.api.response.*
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseRecordFragment
import whitelisted.garage.databinding.FragmentNewOrderDetailBinding
import whitelisted.garage.eventBus.*
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.activities.DashboardActivity
import whitelisted.garage.view.adapter.CarDocsAdapter
import whitelisted.garage.view.adapter.CarImagesAdapterExt
import whitelisted.garage.view.adapter.OrderInventoryCheckListAdapter
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment
import whitelisted.garage.view.fragments.orderInventory.InventoryCheckListItem
import whitelisted.garage.view.fragments.orderInventory.OrderInventoryRequest
import whitelisted.garage.viewmodels.NewOrderFragmentViewModel
import whitelisted.garage.viewmodels.SelectCarDialogViewModel
import java.io.ByteArrayOutputStream
import java.util.*

class NewOrderDetailFragment : BaseRecordFragment(), LocationListener,
    SeekBar.OnSeekBarChangeListener, CompoundButton.OnCheckedChangeListener, View.OnKeyListener {

    private val binding by viewBinding(FragmentNewOrderDetailBinding::inflate)
    private val viewModel: NewOrderFragmentViewModel by viewModel()
    private val selectCarViewModel: SelectCarDialogViewModel by sharedViewModel()
    private var isNewOrder = true

    private lateinit var employeeListResponse: List<EmployeeListResponseModel>
    private var city: String = ""
    private val apiRequest = NewOrderRequest()
    private lateinit var placedOrderId: String

    //------inventory----------
    private val readStoragePermission = 200
    private lateinit var inventoryCheckList: MutableList<OrderInventoryCheckListItemModel>
    private lateinit var orderInventoryCheckListAdapter: OrderInventoryCheckListAdapter
    private lateinit var carDocsList: ArrayList<OrderInventoryCheckListItemModel>
    private lateinit var carDocsAdapter: CarDocsAdapter
    private lateinit var signatureBitmap: Bitmap
    private var signatureTaken = false
    private var fuelPercentage: Int = 0

    private lateinit var exteriorImagesAdapter: CarImagesAdapterExt
    private lateinit var interiorImagesAdapter: CarImagesAdapterExt
    private lateinit var exteriorImagesList: MutableList<Uri>
    private lateinit var interiorImagesList: MutableList<Uri>
    private lateinit var newExteriorImagesList: MutableList<String>
    private lateinit var newInteriorImagesList: MutableList<String>

    private lateinit var uploadList: MutableList<UploadItem>
    private var getInventoryResponse = OrderInventoryRequest()
    private var mediaToBeUploaded = false
    private lateinit var uploadMediaRef: StorageReference
    private var isInventorySet = false
    private var isCustomerDetailUpdated = false
    private var isCarDetailUpdated = false
    private var isInventoryChanged = false
    private var transmissionManual = true
    private var breakingSystemABS = false
    private var twoWheelerType = false
    private var backHandled = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        view?.isFocusableInTouchMode = true
        view?.requestFocus()
        view?.setOnKeyListener(this)
        CommonUtils.hideKeyboard(requireActivity())
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        setObservers()
        setTextWatcher()
        setClickListeners()
        setSwitchListeners()
        showLoader()
        viewModel.getEmployess()
        if (arguments?.getBoolean(AppENUM.IntentKeysENUM.IS_UNDER_TAB, false) == true) {
            isNewOrder = false
            binding.llHeader.visibility = View.GONE
            binding.lblOrderDetails.visibility = View.GONE
            binding.btnCancel.visibility = View.GONE
            binding.btnSave.text = getString(R.string.update_and_notify)
            binding.llAddCustomerDetails.makeVisible(false)
            binding.llAddCarDetails.makeVisible(false)
            binding.clCustomerDetails.makeVisible(true)
            binding.clCarDetails.makeVisible(true)

            viewModel.getOrderDetail(OrderDetailWrapperFragment.orderId)
            observeOrderDetailResponse()
            observeUpdateOrderResponse() //            viewCarClick.visibility = View.VISIBLE
        } else { //            prefillData()
            setupScreenInventory()
            observePlaceOrderResponse()

            //get user location for city
            if (isAdded) {
                lifecycleScope.launch(Dispatchers.Main) {
                    delay(300)
                    (activity as? DashboardActivity)?.getLastLocation(this@NewOrderDetailFragment,
                        false)
                }
            }
            (arguments?.getParcelable<MyCustomerModel>(AppENUM.MY_CUSOMTER_MODEL))?.let {
                setCustomerAndCarDetails(it)
            }

            (arguments?.getParcelable<SearchOrderResponseModel>(AppENUM.SEARCH_ORDER_MODEL))?.let {
                setCustomerAndCarDetails(it)
            }
        }
    }

    private fun setCustomerAndCarDetails(model: SearchOrderResponseModel) {
        isCustomerDetailUpdated = true
        binding.llAddCustomerDetails.makeVisible(false)
        binding.clCustomerDetails.makeVisible(true)
        binding.tvCustomerName.text = model.customerDetails?.name
        binding.tvPhoneNumber.text = model.customerDetails?.mobile
        if (model.customerDetails?.address?.trim()
                ?.isNotEmpty() == true) binding.tvCustomerAddress.text =
            model.customerDetails.address
        else binding.tvCustomerAddress.text = getString(R.string.hyphen)
        if (!model.customerDetails?.email.isNullOrEmpty()) binding.tvEmail.text =
            model.customerDetails?.email
        else binding.tvEmail.text = getString(R.string.hyphen)

        twoWheelerType = model.isTwoWheeler != 2
        changeVehicleType()
        isCarDetailUpdated = true
        binding.llAddCarDetails.makeVisible(false)
        binding.clCarDetails.makeVisible(true)
        model.carDetails?.car?.let {
            binding.tvCarName.text = it
            selectCarViewModel.provideSelectedModel().value?.carName = it
        }
        model.carDetails?.modelId?.let {
            selectCarViewModel.provideSelectedModel().value?.modelId = it
        }
        model.carDetails?.brandId?.let {
            selectCarViewModel.provideSelectedBrand().value?.id = it
        }
        model.carDetails?.carTypeId?.let {
            selectCarViewModel.provideSelectedModel().value?.carTypeId = it
        }
        model.carDetails?.brandName?.let {
            selectCarViewModel.provideSelectedBrand().value?.name = it
        }
        model.carDetails?.car_icon?.let {
            selectCarViewModel.provideSelectedModel().value?.carIcon = it
            ImageLoader.loadImage(binding.imgCarImage, it)
        }
        model.carDetails?.fuelType?.let {
            selectCarViewModel.provideSelectedFuelType().value?.type = it
            binding.tvFuelType.text = it
        }

        binding.tvOdometer.text = model.carDetails?.odometer.toString()
        binding.tvRegNumber.text = model.carDetails?.registrationNumber
    }

    private fun setCustomerAndCarDetails(myCustomerModel: MyCustomerModel) {
        isCustomerDetailUpdated = true
        binding.llAddCustomerDetails.makeVisible(false)
        binding.clCustomerDetails.makeVisible(true)
        binding.tvCustomerName.text = myCustomerModel.name
        binding.tvPhoneNumber.text = myCustomerModel.mobile
        if (myCustomerModel.address?.trim()?.isNotEmpty() == true) binding.tvCustomerAddress.text =
            myCustomerModel.address
        else binding.tvCustomerAddress.text = getString(R.string.hyphen)
        if (!myCustomerModel.email.isNullOrEmpty()) binding.tvEmail.text = myCustomerModel.email
        else binding.tvEmail.text = getString(R.string.hyphen)

        twoWheelerType = myCustomerModel.isTwoWheeler != 2
        changeVehicleType()
        isCarDetailUpdated = true
        binding.llAddCarDetails.makeVisible(false)
        binding.clCarDetails.makeVisible(true)
        myCustomerModel.carName?.let {
            binding.tvCarName.text = it
            selectCarViewModel.provideSelectedModel().value?.carName = it
        }
        myCustomerModel.car_model?.let {
            selectCarViewModel.provideSelectedModel().value?.modelId = it
        }
        myCustomerModel.car_brand?.let {
            selectCarViewModel.provideSelectedBrand().value?.id = it
        }
        myCustomerModel.carTypeId?.let {
            selectCarViewModel.provideSelectedModel().value?.carTypeId = it
        }
        myCustomerModel.brandName?.let {
            selectCarViewModel.provideSelectedBrand().value?.name = it
        }
        myCustomerModel.car_icon?.let {
            selectCarViewModel.provideSelectedModel().value?.carIcon = it
            ImageLoader.loadImage(binding.imgCarImage, it)
        }
        myCustomerModel.fuelType?.let {
            selectCarViewModel.provideSelectedFuelType().value?.type = it
            binding.tvFuelType.text = it
        }

        binding.tvOdometer.text = myCustomerModel.odometerReading
        binding.tvRegNumber.text = myCustomerModel.registration_number
    }

    private fun setSwitchListeners() {
        binding.switchAccCount.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.llAccessoriesList.visibility = View.VISIBLE
            } else {
                binding.tvCountHeadRest.text = getString(R.string.zero)
                binding.tvCountFloorMats.text = getString(R.string.zero)
                binding.tvCountMudFlap.text = getString(R.string.zero)
                binding.tvCountWheelCap.text = getString(R.string.zero)
                binding.llAccessoriesList.visibility = View.GONE
            }
        }
        binding.switchCarDocuments.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.rvCarDocs.visibility = View.VISIBLE
            } else {
                if (::carDocsList.isInitialized) {
                    var i = 0
                    carDocsList.forEach {
                        it.isSelected = false
                        carDocsAdapter.notifyItemChanged(i)
                        i++
                    }
                }
                binding.rvCarDocs.visibility = View.GONE
            }
        }
        binding.switchInventoryCheck.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.llInventoryCheckList.visibility = View.VISIBLE
            } else {
                if (::inventoryCheckList.isInitialized) {
                    var i = 0
                    inventoryCheckList.forEach {
                        it.isSelected = false
                        orderInventoryCheckListAdapter.notifyItemChanged(i)
                        i++
                    }
                }
                binding.llInventoryCheckList.visibility = View.GONE
            }
        }
        binding.switchRemarks.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.llRemarksET.visibility = View.VISIBLE
            } else {
                binding.etInventoryRemarks.setText("")
                binding.llRemarksET.visibility = View.GONE
            }
        }
        binding.switchSelectMechanic.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.llSelectMech.visibility = View.VISIBLE
            } else {
                binding.llSelectMech.visibility = View.GONE
            }
        }
    }

    private fun setupScreenInventory() {
        if (signatureTaken) {
            binding.btnUpdateSignature.text = getString(R.string.update_signature)
        } else binding.btnUpdateSignature.text = getString(R.string.take_sign)

        setImagesAdapter()

        showLoader()
        viewModel.getInventoryCheckListAndCarDocsAPI(twoWheelerType)
        setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_ADD_INVENTORY,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ORDER_INVENTORY)
    }

    private fun setTextWatcher() {
        binding.etAddInventory.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?,
                p1: Int,
                p2: Int,
                p3: Int) { //do nothing
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                isInventoryChanged = true
            }

            override fun afterTextChanged(p0: Editable?) {
                binding.btnAddInventory.isEnabled = p0?.isEmpty() != true
            }
        })

        binding.etInventoryRemarks.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?,
                p1: Int,
                p2: Int,
                p3: Int) { //do nothing
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                isInventoryChanged = true
            }

            override fun afterTextChanged(p0: Editable?) { //do nothing
            }
        })

        binding.etMechanic.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?,
                p1: Int,
                p2: Int,
                p3: Int) { //do nothing
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                isCustomerDetailUpdated = true
            }

            override fun afterTextChanged(p0: Editable?) { //do nothing
            }
        })
    }

    private fun observeOrderDetailResponse() {
        viewModel.provideOrderDetailResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    OrderDetailWrapperFragment.orderStatusId = it.body.data.statusId
                    val carNameOrderIdStatusModel = CarNameOrderIdStatusModel().apply {
                        carName = it.body.data.carDetails?.car ?: ""
                        orderId = it.body.data.orderId
                    }

                    when (it.body.data.statusId) {
                        AppENUM.RefactoredStrings.OPEN_ORDER_STATUS, AppENUM.RefactoredStrings.OPEN_ORDER_STATUS_2 -> EventBus.getDefault()
                            .post(SetOrderDetailsEvent(carNameOrderIdStatusModel.apply {
                                status = getString(R.string.open_order)
                            }))

                        AppENUM.RefactoredStrings.WIP_ORDER_STATUS -> EventBus.getDefault()
                            .post(SetOrderDetailsEvent(carNameOrderIdStatusModel.apply {
                                status = getString(R.string.wip_order)
                            }))
                        AppENUM.RefactoredStrings.READY_ORDER_STATUS -> EventBus.getDefault()
                            .post(SetOrderDetailsEvent(carNameOrderIdStatusModel.apply {
                                status = getString(R.string.ready_order)
                            }))
                        AppENUM.RefactoredStrings.COMPLETED_ORDER_STATUS -> EventBus.getDefault()
                            .post(SetOrderDetailsEvent(carNameOrderIdStatusModel.apply {
                                status = getString(R.string.completed)
                            }))
                    }
                    setData(it.body.data)
                    setupScreenInventory()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setData(data: OrderDetailResponse) {
        binding.tvCustomerName.text = data.customerDetails.name
        binding.tvPhoneNumber.text = data.customerDetails.mobile
        binding.tvEmail.text = getString(R.string.hyphen)
        data.customerDetails.email?.let {
            if (it.trim().isNotEmpty()) binding.tvEmail.text = data.customerDetails.email
        }
        binding.tvCustomerAddress.text = getString(R.string.hyphen)
        data.customerDetails.address.let {
            if (it.trim().isNotEmpty()) binding.tvCustomerAddress.text =
                data.customerDetails.address
        }

        twoWheelerType = data.carDetails?.isTwoWheeler != 2
        changeVehicleType()
        ImageLoader.loadImage(binding.imgCarImage, data.carDetails?.carIcon)
        binding.tvRegNumber.text = data.carDetails?.registrationNumber
        binding.tvOdometer.text = data.carDetails?.odometer.toString()
        binding.tvCarName.text = data.carDetails?.car
        binding.tvFuelType.text = data.carDetails?.fuelType

        if (data.mechanicName.isNotEmpty()) {
            binding.switchSelectMechanic.isChecked = true
            binding.tilMechanic.editText?.setText(data.mechanicName)
        }

        EventBus.getDefault()
            .post(SetOptionsImageStatusEvent("0", SetOptionsImgStatusObject(tabPosition = 0)))

        selectCarViewModel.provideSelectedBrand().value = BrandsResponseModel().apply {
            id = data.carDetails?.brandId
            name = data.carDetails?.brand_name
        }
        selectCarViewModel.provideSelectedModel().value = (CarModelResponseModel().apply {
            modelId = data.carDetails?.modelId
            carIcon = data.carDetails?.carIcon
            carTypeId = data.carDetails?.carTypeId
            carName = data.carDetails?.car
        })
        selectCarViewModel.provideSelectedFuelType().value = TypeItem().apply {
            type = data.carDetails?.fuelType
        }
        transmissionManual = (data.carDetails?.transmission == 0)
        breakingSystemABS = (data.carDetails?.breakingSystem == 1)
        setBreakingSystem()
        setTransmissionMode()

        isCustomerDetailUpdated = false
        isCarDetailUpdated = false
    }

    private fun observeUpdateOrderResponse() {
        viewModel.provideUpdateOrderDetailResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(activity, it.body.message, true)
                    EventBus.getDefault().post(UpdateOrderStatusEvent(
                        "10",
                    ))
                    isCustomerDetailUpdated = false
                    isCarDetailUpdated = false

                    if (isInventoryChanged) {
                        showLoader()
                        updateInventory()
                    }
                }
                is Result.Failure -> CommonUtils.showToast(activity, it.errorMessage, true)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun setObservers() {
        observeEmployeeListResponse()
        observeSignatureLiveData()
        observeInventoryCheckListResponse()
        observeCarDocsListResponse()
        observeInventoryDetailResponse()
        observeAddInventoryResponse()
    }

    private fun observeEmployeeListResponse() {
        viewModel.provideManageEmployeeResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isEmpty()) {
                        binding.llSelectMechanic.makeVisible(false)
                    } else {
                        employeeListResponse = it.body.data
                        setEmployeeListAdapter()
                    }
                }
                is Result.Failure -> { //do nothing
                }
            }
        }
    }

    private fun setEmployeeListAdapter() {
        val mechanicList = ArrayList<String>()
        employeeListResponse.forEach {
            if (!it.name.isNullOrEmpty()) {
                mechanicList.add(it.name)
            }
        }

        if (mechanicList.isNotEmpty()) {
            val adapter = ArrayAdapter(requireContext(),
                R.layout.support_simple_spinner_dropdown_item,
                mechanicList)
            (binding.tilMechanic.editText as? AutoCompleteTextView)?.setAdapter(adapter)
        } else binding.llSelectMechanic.makeVisible(false)
    }

    private fun observePlaceOrderResponse() {
        viewModel.providePlaceOrderResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(activity, getString(R.string.new_order_created), true)
                    placedOrderId = it.body.data.orderId ?: ""

                    if (isInventoryChanged) {
                        showLoader()
                        updateInventory()
                    } else {
                        placeOrderSuccess()
                    }
                }
                is Result.Failure -> CommonUtils.showToast(activity, it.errorMessage, true)
            }
        }
    }

    private fun setClickListeners() {
        binding.imgBack.setOnClickListener(this)
        binding.btnCancel.setOnClickListener(this)
        binding.btnSave.setOnClickListener(this)
        binding.tilMechanic.setOnClickListener(this)
        binding.imgSwitchBreakingSys.setOnClickListener(this)
        binding.imgSwitchVehicleType.setOnClickListener(this)
        binding.imgSwitchTransmission.setOnClickListener(this)
        binding.llAddCustomerDetails.setOnClickListener(this)
        binding.llAddCarDetails.setOnClickListener(this)
        binding.imgCustomerEdit.setOnClickListener(this)
        binding.imgCarEdit.setOnClickListener(this)

        //--------inventory---------

        binding.imgPlayPause.setOnClickListener(this)
        binding.btnRecord.setOnClickListener(this)
        binding.sbVoiceRecord.setOnSeekBarChangeListener(this)
        setFuelMeterSeekListener()
        binding.btnMinusHeadRest.setOnClickListener(this)
        binding.btnMinusFloorMats.setOnClickListener(this)
        binding.btnMinusMudFlap.setOnClickListener(this)
        binding.btnMinusWheelCap.setOnClickListener(this)
        binding.btnPlusHeadRest.setOnClickListener(this)
        binding.btnPlusFloorMats.setOnClickListener(this)
        binding.btnPlusMudFlap.setOnClickListener(this)
        binding.btnPlusWheelCap.setOnClickListener(this)
        binding.btnAddInventory.setOnClickListener(this)
        binding.btnUpdateSignature.setOnClickListener(this)
    }

    private fun setFuelMeterSeekListener() {
        binding.sbFuelMeter.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, position: Int, p2: Boolean) {
                fuelPercentage = position
                "$fuelPercentage%".apply {
                    binding.tvFuelPercentage.text = this
                }

                isInventoryChanged = true
            }

            override fun onStartTrackingTouch(p0: SeekBar?) { //do nothing
            }

            override fun onStopTrackingTouch(p0: SeekBar?) { //do nothing
            }
        })
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnCancel -> {
                popBackStackAndHideKeyboard()
            }
            R.id.btnSave -> {
                makeNewOrderRequest()
                if (isNewOrder) {
                    if (checkForm()) {
                        showLoader()
                        viewModel.createNewOrder()
                    }
                } else {
                    if (isCustomerDetailUpdated || isCarDetailUpdated) {
                        showLoader()
                        viewModel.updateOrderDetail(OrderDetailWrapperFragment.orderId)
                    } else {
                        if (isInventoryChanged) {
                            showLoader()
                            updateInventory()
                        } else {
                            CommonUtils.showToast(requireContext(),
                                getString(R.string.order_already_saved))
                        }
                    }
                }
            }
            R.id.imgBack -> {
                if (checkForBack()) {
                    showConfirmationDialog()
                } else {
                    popBackStackAndHideKeyboard()
                }
            }

            R.id.tilMechanic -> CommonUtils.hideKeyboard(requireActivity())

            R.id.imgCarEdit, R.id.llAddCarDetails -> showBrandsDialog()

            //----------inventory-----------
            R.id.imgPlayPause -> {
                if (player?.isPlaying != true) startPlaying()
                else pausePlaying()
            }
            R.id.btnRecord -> {
                startRecording()
            }

            R.id.btnMinusHeadRest -> handleAccessoriesCounts(0, true)
            R.id.btnMinusFloorMats -> handleAccessoriesCounts(1, true)
            R.id.btnMinusWheelCap -> handleAccessoriesCounts(2, true)
            R.id.btnMinusMudFlap -> handleAccessoriesCounts(3, true)
            R.id.btnPlusHeadRest -> handleAccessoriesCounts(0, false)
            R.id.btnPlusFloorMats -> handleAccessoriesCounts(1, false)
            R.id.btnPlusWheelCap -> handleAccessoriesCounts(2, false)
            R.id.btnPlusMudFlap -> handleAccessoriesCounts(3, false)
            R.id.clItemBase -> {
                isInventoryChanged = true
            }
            R.id.btnAddInventory -> addToInventoryCheckList()
            R.id.clItemBaseCD -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    if (position == carDocsAdapter.dataListItemModel.size - 1) {
                        carDocsAdapter.dataListItemModel.forEach {
                            it.isSelected = false
                        }
                        carDocsAdapter.dataListItemModel[position].isSelected = true
                        carDocsAdapter.notifyItemRangeChanged(0,
                            carDocsAdapter.dataListItemModel.size)
                    } else {
                        carDocsAdapter.dataListItemModel[carDocsAdapter.dataListItemModel.size - 1].isSelected =
                            false
                        carDocsAdapter.notifyItemChanged(carDocsAdapter.dataListItemModel.size - 1)
                    }

                    isInventoryChanged = true
                }
            }

            R.id.btnUpdateSignature -> {
                CommonUtils.addFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.TAKE_SIGNATURE, null),
                    target = R.id.fragment_container_2)
            }
            R.id.llDelImage, R.id.rlAddImage -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    CommonUtils.genericCastOrNull<Boolean>(v.getTag(R.id.model))?.let { extra ->
                        if (extra) {
                            if (position == exteriorImagesAdapter.getAdapterList().size - 1) {
                                TedPermission.create()
                                    .setPermissionListener(object : PermissionListener {
                                        override fun onPermissionGranted() {
                                            TedImagePicker.with(requireContext())
                                                .showCameraTile(true).startMultiImage { uris ->
                                                    onExteriorImagesPicked(uris)
                                                }
                                        }

                                        override fun onPermissionDenied(deniedPermissions: MutableList<String>?) { //do nothing
                                        }
                                    }).setPermissions(Manifest.permission.CAMERA,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE).check()
                            } else {
                                exteriorImagesAdapter.getAdapterList().removeAt(position)
                                if (::newExteriorImagesList.isInitialized && newExteriorImagesList.contains(
                                        exteriorImagesList[position])) {
                                    newExteriorImagesList.remove(exteriorImagesList[position])
                                }
                                exteriorImagesAdapter.notifyItemRemoved(position)
                            }
                        } else {
                            if (position == interiorImagesAdapter.getAdapterList().size - 1) {
                                TedPermission.create()
                                    .setPermissionListener(object : PermissionListener {
                                        override fun onPermissionGranted() {
                                            TedImagePicker.with(requireContext())
                                                .showCameraTile(true).startMultiImage { uris ->
                                                    onInteriorImagesPicked(uris)
                                                }
                                        }

                                        override fun onPermissionDenied(deniedPermissions: MutableList<String>?) { //do nothing
                                        }
                                    }).setPermissions(Manifest.permission.CAMERA,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE).check()

                            } else {
                                interiorImagesAdapter.getAdapterList().removeAt(position)
                                if (::newInteriorImagesList.isInitialized && newInteriorImagesList.contains(
                                        interiorImagesList[position])) newInteriorImagesList.remove(
                                    interiorImagesList[position])
                                interiorImagesAdapter.notifyItemRemoved(position)
                            }
                        }

                        isInventoryChanged = true
                    }
                }
            }

            R.id.imgCarImage -> {
                CommonUtils.genericCastOrNull<Uri>(v.getTag(R.id.model))?.let { uri ->
                    imagePreview(v, uri)
                }
            }

            R.id.imgSwitchTransmission -> {
                changeTransmissionMode()
            }

            R.id.imgSwitchBreakingSys -> {
                changeBreakingSystem()
            }

            R.id.imgSwitchVehicleType -> {
                if (!isNewOrder) {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.cannot_change_vehicle_type_in_existinig_order))
                } else changeVehicleType(true)
            }

            R.id.llAddCustomerDetails -> {
                showCustomerDetailsDialog(false)
            }

            R.id.imgCustomerEdit -> {
                showCustomerDetailsDialog(true)
            }
        }
    }

    private fun changeVehicleType(isChange: Boolean = false) {
        if (twoWheelerType) {
            twoWheelerType = false
            binding.lblFourWheeler.changeTextColor(requireContext(), R.color.colorAccent)
            binding.lblTwoWheeler.changeTextColor(requireContext(), R.color.black_text_new)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_double_switch_off), binding.imgSwitchVehicleType)
            binding.tvAddVehicleDetails.text = getString(R.string.add_car_details)
            binding.tvAddVehicleDetails.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_add_car_details),
                null,
                null,
                null)
            binding.llCarAccCount.makeVisible(true)
            binding.llInteriorImages.makeVisible(true)
            binding.lblCarDocuments.text = getString(R.string.car_documents)
            binding.lblInventoryCheck.text = getString(R.string.car_inventory_check)
        } else {
            twoWheelerType = true
            binding.lblFourWheeler.changeTextColor(requireContext(), R.color.black_text_new)
            binding.lblTwoWheeler.changeTextColor(requireContext(), R.color.colorAccent)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_double_switch_on), binding.imgSwitchVehicleType)
            binding.tvAddVehicleDetails.text = getString(R.string.add_bike_details)
            binding.tvAddVehicleDetails.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_two_wheeler),
                null,
                null,
                null)
            binding.llCarAccCount.makeVisible(false)
            binding.llInteriorImages.makeVisible(false)
            binding.lblCarDocuments.text = getString(R.string.bike_documents)
            binding.lblInventoryCheck.text = getString(R.string.bike_inventory_check)
        }

        if (isChange) {
            isCarDetailUpdated = false
            binding.llAddCarDetails.makeVisible(true)
            binding.clCarDetails.makeVisible(false)
            showLoader()
            viewModel.getInventoryCheckListAndCarDocsAPI(twoWheelerType)
        }
    }

    private fun showConfirmationDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
            Bundle().apply {
                putString(AppENUM.TITLE, getString(R.string.are_you_sure_you_want_to_go_back))
                putString(AppENUM.DESCRIPTION, getString(R.string.unsaved_data_will_be_lost))
                putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.confirm))
                putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.cancel))
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    backHandled = false
                    if (extra as? Boolean == true) {
                        popBackStackAndHideKeyboard()
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun checkForBack(): Boolean {
        return isCarDetailUpdated || isCustomerDetailUpdated || isInventoryChanged
    }

    private fun showCustomerDetailsDialog(isEdit: Boolean) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.ADD_EDIT_CUSTOMER_DETAILS,
            if (!isEdit) null else Bundle().apply {
                putParcelable(AppENUM.CUSTOMER_DETAILS,
                    CustomerDetailsModel(name = binding.tvCustomerName.text.toString(),
                        phoneNumber = binding.tvPhoneNumber.text.toString(),
                        address = binding.tvCustomerAddress.text.toString(),
                        email = binding.tvEmail.text.toString()))
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    isCustomerDetailUpdated = true
                    binding.llAddCustomerDetails.makeVisible(false)
                    binding.clCustomerDetails.makeVisible(true)
                    (extra as? CustomerDetailsModel)?.let {
                        binding.tvCustomerName.text = it.name
                        binding.tvPhoneNumber.text = it.phoneNumber
                        if (it.address?.trim()
                                ?.isNotEmpty() == true) binding.tvCustomerAddress.text = it.address
                        else binding.tvCustomerAddress.text = getString(R.string.hyphen)
                        if (it.email?.trim()?.isNotEmpty() == true) binding.tvEmail.text = it.email
                        else binding.tvEmail.text = getString(R.string.hyphen)
                    }
                }
            })?.let { dialog ->
            dialog.show(parentFragmentManager, dialog.javaClass.name)
        }
    }

    private fun changeBreakingSystem() {
        if (breakingSystemABS) {
            breakingSystemABS = false
            binding.lblABS.changeTextColor(requireContext(), R.color.black_text_new)
            binding.lblNonABS.changeTextColor(requireContext(), R.color.colorAccent)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_double_switch_on), binding.imgSwitchBreakingSys)
        } else {
            breakingSystemABS = true
            binding.lblABS.changeTextColor(requireContext(), R.color.colorAccent)
            binding.lblNonABS.changeTextColor(requireContext(), R.color.black_text_new)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_double_switch_off), binding.imgSwitchBreakingSys)
        }
    }

    private fun setBreakingSystem() {
        if (breakingSystemABS) {
            binding.lblABS.changeTextColor(requireContext(), R.color.colorAccent)
            binding.lblNonABS.changeTextColor(requireContext(), R.color.black_text_new)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_double_switch_off), binding.imgSwitchBreakingSys)
        } else {
            binding.lblABS.changeTextColor(requireContext(), R.color.black_text_new)
            binding.lblNonABS.changeTextColor(requireContext(), R.color.colorAccent)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_double_switch_on), binding.imgSwitchBreakingSys)
        }
    }

    private fun changeTransmissionMode() {
        if (transmissionManual) {
            transmissionManual = false
            binding.lblManual.changeTextColor(requireContext(), R.color.black_text_new)
            binding.lblAuto.changeTextColor(requireContext(), R.color.colorAccent)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_double_switch_on), binding.imgSwitchTransmission)
        } else {
            transmissionManual = true
            binding.lblManual.changeTextColor(requireContext(), R.color.colorAccent)
            binding.lblAuto.changeTextColor(requireContext(), R.color.black_text_new)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_double_switch_off), binding.imgSwitchTransmission)
        }
        isInventoryChanged = true
    }

    private fun setTransmissionMode() {
        if (transmissionManual) {
            binding.lblManual.changeTextColor(requireContext(), R.color.colorAccent)
            binding.lblAuto.changeTextColor(requireContext(), R.color.black_text_new)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_double_switch_off), binding.imgSwitchTransmission)
        } else {
            binding.lblManual.changeTextColor(requireContext(), R.color.black_text_new)
            binding.lblAuto.changeTextColor(requireContext(), R.color.colorAccent)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_double_switch_on), binding.imgSwitchTransmission)
        }
        isInventoryChanged = true
    }

    private fun updateInventory() {
        setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_SAVE_INVENTORY,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ORDER_INVENTORY)

        CommonUtils.hideKeyboard(requireActivity())
        showLoader()
        makeInventoryRequest()
        prepareUploadList()
    }

    private fun showBrandsDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_CAR, Bundle().apply {
            putInt(AppENUM.CAR_SEARCH_TYPE, 0)
            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, twoWheelerType)
            putBoolean(AppENUM.IS_REG, true)
            putString(AppENUM.REG_NO, binding.tvRegNumber.text.toString())
            putString(AppENUM.ODO_READING, binding.tvOdometer.text.toString())
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                when (extra as? Int) {
                    0 -> { //do nothing
                    }
                    1 -> { //do nothing
                    }
                    2 -> {
                        isCarDetailUpdated = true
                        binding.llAddCarDetails.makeVisible(false)
                        binding.clCarDetails.makeVisible(true)
                        "${selectCarViewModel.provideSelectedBrand().value?.name} ${selectCarViewModel.provideSelectedModel().value?.carName}".apply {
                            binding.tvCarName.text = this
                        }
                        binding.tvFuelType.text =
                            selectCarViewModel.provideSelectedFuelType().value?.type
                        binding.tvOdometer.text = selectCarViewModel.provideOdometerReading().value
                        binding.tvRegNumber.text =
                            selectCarViewModel.provideRegistrationNumber().value
                        ImageLoader.loadImage(binding.imgCarImage,
                            selectCarViewModel.provideSelectedModel().value?.carIcon)
                    }
                }
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun checkForm(): Boolean {
        return when {
            !isCustomerDetailUpdated -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.please_add_customer_details))
                false
            }
            !isCarDetailUpdated -> {
                CommonUtils.showToast(requireContext(), getString(R.string.please_add_vehicle_details))
                false
            }
            else -> {
                true
            }
        }
    }

    private fun makeNewOrderRequest() {
        apiRequest.name = binding.tvCustomerName.text.toString().trim()
        apiRequest.phoneNumber = binding.tvPhoneNumber.text.toString().trim()
        apiRequest.email = binding.tvEmail.text.toString().trim()
        apiRequest.address = binding.tvCustomerAddress.text.toString().trim()
        apiRequest.regNumber = binding.tvRegNumber.text.toString().trim()
        apiRequest.odometerReading = binding.tvOdometer.text.toString().trim()
        apiRequest.city = city
        apiRequest.isTwoWheeler = if (twoWheelerType) 2 else 4

        apiRequest.car =
            "${selectCarViewModel.provideSelectedBrand().value?.name} ${selectCarViewModel.provideSelectedModel().value?.carName}"
        apiRequest.modelId = selectCarViewModel.provideSelectedModel().value?.modelId
        apiRequest.brandId = selectCarViewModel.provideSelectedBrand().value?.id
        apiRequest.brandName = selectCarViewModel.provideSelectedBrand().value?.name
        apiRequest.carTypeId = selectCarViewModel.provideSelectedModel().value?.carTypeId
        apiRequest.carIcon = selectCarViewModel.provideSelectedModel().value?.carIcon
        apiRequest.fuelType = selectCarViewModel.provideSelectedFuelType().value?.type
        if (transmissionManual) apiRequest.transmission = 0
        else apiRequest.transmission = 1
        if (breakingSystemABS) apiRequest.breakingSystem = 1
        else apiRequest.breakingSystem = 0

        if (::employeeListResponse.isInitialized) {
            employeeListResponse.find { it.name == binding.tilMechanic.editText?.text.toString() }
                ?.let {
                    apiRequest.mechanicId = it.userId.toString()
                    apiRequest.mechanicName = it.name.toString()
                }
        }
        // firebase event
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_NEW_ORDER_DETAILS)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_NAME, apiRequest.name)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_PHONE,
                apiRequest.phoneNumber)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_EMAIL, apiRequest.email)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_ADDRESS,
                apiRequest.address)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_CAR_ODO,
                apiRequest.odometerReading)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_CAR_BRAND, apiRequest.car)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_CAR_REG,
                apiRequest.regNumber)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.BILL_TYPE, apiRequest.billType)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.TAX_TYPE, apiRequest.taxType)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.GSTIN_NUM,
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_GSTIN))

            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_SAVE_ORDER,
                this)
        }

        viewModel.provideNewOrderRequest().value = apiRequest
    }

    override fun onLocationChanged(location: Location) {
        try {
            val gcd = Geocoder(requireContext(), Locale.getDefault())
            val addresses: List<Address>? =
                gcd.getFromLocation(location.latitude, location.longitude, 1)
            if (addresses?.isNotEmpty() == true) {
                city = addresses[0].locality
            } else { // not found
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray) {
        Log.d(AppENUM.TAG_PERMISSION, requestCode.toString())
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is RequestLocationEvent -> {
                (activity as DashboardActivity).getLastLocation(this,
                    false) //                (activity as DashboardActivity).isLocationEventSent = true
            }
            is UpdateSignatureEvent -> {
                mediaToBeUploaded = true
                signatureBitmap = event.extra as Bitmap
                signatureTaken = true
                binding.btnUpdateSignature.text = getString(R.string.update_signature)
                binding.imgSignature.setImageBitmap(signatureBitmap)
            }
        }
    }


    //-----------inventory methods-------------
    private fun setCarDocsListAdapter(data: List<String>) {
        carDocsList = ArrayList()
        data.forEach {
            val inventoryCheckItem = OrderInventoryCheckListItemModel()
            inventoryCheckItem.name = it
            inventoryCheckItem.isSelected = false
            carDocsList.add(inventoryCheckItem)
        }

        val layoutManager = FlexboxLayoutManager(requireContext())
        layoutManager.flexWrap = FlexWrap.WRAP
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.FLEX_START
        layoutManager.alignItems = AlignItems.FLEX_START
        binding.rvCarDocs.layoutManager = layoutManager

        carDocsAdapter = CarDocsAdapter(carDocsList, this)
        binding.rvCarDocs.adapter = carDocsAdapter
    }

    private fun observeCarDocsListResponse() {
        viewModel.provideCarDocumentsListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setCarDocsListAdapter(it.body.data)
                    if (isResumed) {
                        if (!isInventorySet) {
                            if (!isNewOrder) viewModel.getInventoryDetail()
                        }
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeInventoryCheckListResponse() {
        viewModel.provideInventoryCheckListResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    setInventoryCheckListAdapter(it.body.data)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun onExteriorImagesPicked(images: List<Uri>) { //        exteriorImagesList.clear()
        //        for (image in images!!) {
        //            val imageModel = CarImageModel()
        //            imageModel.carImage = image
        //            exteriorImagesList.add(imageModel)
        //        }
        mediaToBeUploaded = true
        if ((exteriorImagesList.size + images.size) > 21) {
            CommonUtils.showToast(activity,
                getString(R.string.pre_car_image_validation) + " " + (21 - exteriorImagesList.size) + " " + getString(
                    R.string.more_images))
            return
        } //        exteriorImagesAdapter.getAdapterList().removeLast()
        repeat(images.size) { position ->
            if (images[position].toString().isNotEmpty()) {
                exteriorImagesAdapter.getAdapterList().add(0,
                    (images[position])) //                exteriorImagesList.add(images[position] as String)
            }
        } //        if (exteriorImagesAdapter.getAdapterList().size != 20)
        //            exteriorImagesAdapter.getAdapterList().add("")
        exteriorImagesAdapter.notifyDataSetChanged()

        isInventoryChanged = true
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun onInteriorImagesPicked(images: List<Uri>) {
        mediaToBeUploaded = true
        if ((interiorImagesList.size + images.size) > 21) {
            CommonUtils.showToast(activity,
                getString(R.string.pre_car_image_validation) + (21 - interiorImagesList.size) + getString(
                    R.string.more_images))
            return
        } //        interiorImagesAdapter.getAdapterList().removeLast()
        repeat(images.size) { position ->
            if (images[position].toString().isNotEmpty()) {
                interiorImagesAdapter.getAdapterList().add(0,
                    images[position]) //                interiorImagesList.add(images[position] as String)
            }
        }

        interiorImagesAdapter.notifyDataSetChanged()

        isInventoryChanged = true
    }

    private fun setImagesAdapter() {
        exteriorImagesList = ArrayList()
        exteriorImagesList.add(Uri.EMPTY)
        exteriorImagesAdapter = CarImagesAdapterExt(exteriorImagesList, true, this)
        binding.rvExteriorPhotos.layoutManager =
            GridLayoutManager(requireContext(), 3, GridLayoutManager.VERTICAL, false)
        binding.rvExteriorPhotos.adapter = exteriorImagesAdapter

        interiorImagesList = ArrayList()
        interiorImagesList.add(Uri.EMPTY)
        interiorImagesAdapter = CarImagesAdapterExt(interiorImagesList, false, this)
        val layoutManager =
            GridLayoutManager(requireContext(), 3, GridLayoutManager.VERTICAL, false)

        binding.rvInteriorPhotos.layoutManager = layoutManager

        binding.rvInteriorPhotos.adapter = interiorImagesAdapter
    }

    private fun observeAddInventoryResponse() {
        viewModel.provideAddInventoryResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (isResumed) {
                        binding.tvInventoryRemarks.text = binding.etInventoryRemarks.text.toString()
                        CommonUtils.showToast(requireContext(), it.body.message, true)
                        if (!isNewOrder) {
                            EventBus.getDefault().post(UpdateOrderStatusEvent(
                                "10",
                            ))

                            //                    if (OrderDetailWrapperActivity.isNewOrder)
                            EventBus.getDefault().post(SelectTabEvent(
                                "1",
                            ))
                        }

                        EventBus.getDefault().post(UpdateHomeScreenEvent())
                        EventBus.getDefault()
                            .post(RefreshOrderSummaryEvent(OrderDetailWrapperFragment.orderId))

                        if (isNewOrder) {
                            placeOrderSuccess()
                        }
                    }
                    isInventoryChanged = false
                }
                is Result.Failure -> {
                    if (isNewOrder) {
                        placeOrderSuccess()
                    }
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun placeOrderSuccess() {
        EventBus.getDefault().post(UpdateHomeScreenEvent())
        parentFragmentManager.popBackStack(0, 0)
        if (::placedOrderId.isInitialized) {
            CommonUtils.showReviewDialog(requireActivity())
            CommonUtils.addFragmentUtil(activity,
                FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                    Bundle().apply {
                        putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, twoWheelerType)
                        putString(AppENUM.IntentKeysENUM.ORDER_ID, placedOrderId)
                        putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER,
                            true) //                        putInt(
                        //                            AppENUM.IntentKeysENUM.SELECT_TAB,
                        //                            1
                        //                        )
                    }))
        }
    }

    private fun uploadAllMedia() {
        if (uploadList.size > 0) {
            if (isAdded) {
                uploadMedia(uploadList[0])
            }
        } else {
            viewModel.addInventory()
        }
    }

    private fun uploadMedia(uploadItem: UploadItem) { // 0->exteriorImage, 1->interiorImage 2->sign 3->audio
        var tryCount = 0
        lifecycleScope.launch {
            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
            var orderRef: StorageReference = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
                .child(viewModel.getStringSharedPreference(AppENUM.USER_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                .child(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID) + "/")

            orderRef = if (isNewOrder) {
                orderRef.child(AppENUM.RefactoredStrings.PATH_ORDERS).child("$placedOrderId/")
                    .child(AppENUM.RefactoredStrings.PATH_ORDER_INVENTORY)
            } else {
                orderRef.child(AppENUM.RefactoredStrings.PATH_ORDERS)
                    .child("${OrderDetailWrapperFragment.orderId}/")
                    .child(AppENUM.RefactoredStrings.PATH_ORDER_INVENTORY)
            }

            val fileNameRand: String = (Calendar.getInstance().timeInMillis.toString())
            when (uploadItem.fileType) {
                0 -> uploadMediaRef = orderRef.child(AppENUM.RefactoredStrings.PATH_EXTERIOR_IMAGES)
                    .child(fileNameRand)
                1 -> uploadMediaRef = orderRef.child(AppENUM.RefactoredStrings.PATH_INTERIOR_IMAGES)
                    .child(fileNameRand)
                2 -> {
                    uploadMediaRef = orderRef.child(AppENUM.RefactoredStrings.PATH_SIGNATURE_IMAGE)
                        .child(fileNameRand)
                    orderRef.child(AppENUM.RefactoredStrings.PATH_SIGNATURE_IMAGE).listAll()
                        .addOnSuccessListener { res ->
                            res.items.forEach {
                                it.delete()
                            }
                        }
                }
            }

            val uploadTask: UploadTask
            val file: Uri
            val emptyBitmap = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(emptyBitmap);
            canvas.drawColor(ContextCompat.getColor(requireContext(), R.color.white))
            if (uploadItem.fileType.toString().toInt() <= 1) {
                var bmp = try {
                    MediaStore.Images.Media.getBitmap(requireContext().contentResolver,
                        uploadItem.uri) ?: emptyBitmap
                } catch (e: Exception) {
                    emptyBitmap
                }

                val byteArrayOutputStream = ByteArrayOutputStream()
                bmp = CommonUtils.scaleBitmap(bmp)
                bmp.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream)
                val data = byteArrayOutputStream.toByteArray()
                uploadTask = uploadMediaRef.putBytes(data)
            } else {
                file = Uri.fromFile(uploadItem.file)
                uploadTask = uploadMediaRef.putFile(file)
            }

            // Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener {
                val result = it.message
                tryCount++
                if (tryCount > 1) {
                    tryCount = 0
                    if (uploadList.size > 0) uploadList.removeAt(0)
                }
                uploadAllMedia() // Handle unsuccessful uploads
            }.addOnSuccessListener { taskSnapshot ->
                val result = ""
                tryCount = 0
                if (uploadList.size > 0) uploadList.removeAt(0)
                uploadAllMedia() // taskSnapshot.metadata contains file metadata such as size, content-type, etc.
                // ...
            }.addOnCompleteListener {
                if (it.isSuccessful) {
                    val downloadUri = it.result
                }
            }
        }
    }

    private fun prepareUploadList() {
        uploadList = mutableListOf()
        if (::exteriorImagesList.isInitialized) {
            exteriorImagesList.forEach {
                if (it.toString().isNotEmpty() && !it.toString().contains("whitelist")) {
                    val uploadItem = UploadItem()
                    uploadItem.fileType = 0
                    val uri = it
                    uploadItem.uri = uri
                    uploadList.add(uploadItem)
                }
            }
        }

        if (::interiorImagesList.isInitialized) {
            interiorImagesList.forEach {
                if (it.toString().isNotEmpty() && !it.toString().contains("whitelist")) {
                    val uploadItem = UploadItem()
                    uploadItem.fileType = 1
                    val uri = it
                    uploadItem.uri = uri
                    uploadList.add(uploadItem)
                }
            }
        }

        if (signatureTaken) {
            val uploadItem = UploadItem()
            uploadItem.fileType = 2
            uploadItem.file = CommonUtils.bitmapToFile(requireContext(), signatureBitmap)
            uploadList.add(uploadItem)
        }

        Log.d("files:", "$uploadList")
        uploadAllMedia()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun observeInventoryDetailResponse() {
        viewModel.provideInventoryDetailResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (isVisible) {
                        getInventoryResponse = it.body.data
                        setInventoryData(it.body.data)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeSignatureLiveData() {
        viewModel.provideSignatureBitmap().observe(viewLifecycleOwner) {
            mediaToBeUploaded = true
            signatureBitmap = it
            signatureTaken = true
            binding.btnUpdateSignature.text = getString(R.string.update_signature)
            ImageLoader.loadImage(binding.imgSignature, signatureBitmap)
        }
    }

    private fun setInventoryCheckListAdapter(data: List<String>) {
        inventoryCheckList = mutableListOf()
        data.forEach {
            val inventoryCheckItem = OrderInventoryCheckListItemModel()
            inventoryCheckItem.name = it
            inventoryCheckItem.isSelected = false
            inventoryCheckList.add(inventoryCheckItem)
        }
        orderInventoryCheckListAdapter = OrderInventoryCheckListAdapter(inventoryCheckList, this)

        val layoutManager = FlexboxLayoutManager(requireContext())
        layoutManager.flexWrap = FlexWrap.WRAP
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.FLEX_START
        layoutManager.alignItems = AlignItems.FLEX_START
        binding.rvInventoryCheck.layoutManager = layoutManager
        binding.rvInventoryCheck.adapter = orderInventoryCheckListAdapter
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun setInventoryData(data: OrderInventoryRequest) {
        if (data.inventoryItems?.size.toString().toInt() > 0) {
            var flag = false
            data.inventoryItems?.forEach {
                if (it.qty?.toInt() ?: 0 > 0) {
                    flag = true
                }
            }
            if (flag) {
                binding.switchAccCount.isChecked = true
            }
            data.inventoryItems?.forEach {
                when (it.name) {
                    getString(R.string.head_rest) -> {
                        binding.tvCountHeadRest.text = it.qty
                    }
                    getString(R.string.floor_mats) -> {
                        binding.tvCountFloorMats.text = it.qty
                    }
                    getString(R.string.wheel_cap) -> {
                        binding.tvCountWheelCap.text = it.qty
                    }
                    getString(R.string.mud_flap) -> {
                        binding.tvCountMudFlap.text = it.qty
                    }
                }
            }
        }
        if (data.inventoryChecks?.size.toString().toInt() > 0) {
            binding.switchInventoryCheck.isChecked = true
            data.inventoryChecks?.forEach { responseItem ->
                var flag = false
                inventoryCheckList.forEach {
                    if (it.name == responseItem) {
                        it.isSelected = true
                        flag = true
                    }
                }
                if (!flag) inventoryCheckList.add(OrderInventoryCheckListItemModel(responseItem,
                    true))
            }
            orderInventoryCheckListAdapter =
                OrderInventoryCheckListAdapter(inventoryCheckList, this)
            binding.rvInventoryCheck.adapter = orderInventoryCheckListAdapter
        }

        if (data.carDocuments?.size.toString().toInt() > 0) {
            binding.switchCarDocuments.isChecked = true
            data.carDocuments?.forEach { responseItem ->
                carDocsList.forEach {
                    if (it.name == responseItem) {
                        it.isSelected = true
                    }
                }
            }
            carDocsAdapter = CarDocsAdapter(carDocsList, this)
            binding.rvCarDocs.adapter = carDocsAdapter
        }

        if (!data.inventoryRemarks.isNullOrEmpty()) {
            binding.switchRemarks.isChecked = true
            binding.etInventoryRemarks.setText(data.inventoryRemarks)
            binding.tvInventoryRemarks.text = data.inventoryRemarks
        }

        if (data.fuelMeter?.isNotEmpty() == true) {
            fuelPercentage = data.fuelMeter?.toInt() ?: 0
            binding.sbFuelMeter.setProgress(data.fuelMeter?.toInt() ?: 0, true)
            binding.tvFuelPercentage.text = "$fuelPercentage%"
        }

        if (data.pdfStatus?.status == true) {
            EventBus.getDefault().post(SetOptionsImageStatusEvent("0",
                SetOptionsImgStatusObject(tabPosition = 1,
                    pdfFile = data.pdfStatus?.pdfFile ?: "")))

        } else {
            EventBus.getDefault()
                .post(SetOptionsImageStatusEvent("0", SetOptionsImgStatusObject(tabPosition = 0)))
        }

        setImages()

        isInventorySet = true
        isInventoryChanged = false
    }

    private fun setImages() {
        if (isVisible) {
            if (::exteriorImagesAdapter.isInitialized) {
                exteriorImagesAdapter.getAdapterList().clear()
                exteriorImagesAdapter.getAdapterList().add(Uri.EMPTY)
                exteriorImagesAdapter.notifyDataSetChanged()
            }
            if (::interiorImagesAdapter.isInitialized) {
                interiorImagesAdapter.getAdapterList().clear()
                interiorImagesAdapter.getAdapterList().add(Uri.EMPTY)
                interiorImagesAdapter.notifyDataSetChanged()
            }
            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
            var orderRef: StorageReference = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
                .child(viewModel.getStringSharedPreference(AppENUM.USER_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                .child(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID) + "/")
            orderRef = if (isNewOrder) {
                orderRef.child(AppENUM.RefactoredStrings.PATH_ORDERS).child("$placedOrderId/")
                    .child(AppENUM.RefactoredStrings.PATH_ORDER_INVENTORY)
            } else {
                orderRef.child(AppENUM.RefactoredStrings.PATH_ORDERS)
                    .child("${OrderDetailWrapperFragment.orderId}/")
                    .child(AppENUM.RefactoredStrings.PATH_ORDER_INVENTORY)
            }
            orderRef.child(AppENUM.RefactoredStrings.PATH_EXTERIOR_IMAGES).listAll()
                .addOnSuccessListener {
                    if (it.items.size > 0) { //                        switchCarPhotos.isChecked = true
                        it.items.forEach { item ->
                            item.downloadUrl.addOnSuccessListener { uri ->
                                exteriorImagesAdapter.getAdapterList().add(0, uri)
                                exteriorImagesAdapter.notifyItemInserted(0)
                            }
                        }
                    }
                }
            orderRef.child(AppENUM.RefactoredStrings.PATH_INTERIOR_IMAGES).listAll()
                .addOnSuccessListener {
                    if (it.items.size > 0) { //                        switchCarPhotos.isChecked = true
                        it.items.forEach { item ->
                            item.downloadUrl.addOnSuccessListener { uri ->
                                interiorImagesAdapter.getAdapterList().add(0, uri)
                                interiorImagesAdapter.notifyItemInserted(0)
                            }
                        }
                    }
                }
            orderRef.child(AppENUM.RefactoredStrings.PATH_SIGNATURE_IMAGE).listAll()
                .addOnSuccessListener {
                    if (it.items.size > 0) {
                        kotlin.run {
                            if (isAdded) {
                                binding.btnUpdateSignature.text =
                                    getString(R.string.update_signature)
                                it.items[0].downloadUrl.addOnSuccessListener { uri ->
                                    if (isResumed) ImageLoader.loadImage(binding.imgSignature, uri)
                                }
                            }
                        }
                    }
                }
        }
    }

    private fun makeInventoryRequest() {
        if (isNewOrder) {
            viewModel.provideInventoryDetailRequest().orderId = placedOrderId
        } else viewModel.provideInventoryDetailRequest().orderId =
            OrderDetailWrapperFragment.orderId
        viewModel.provideInventoryDetailRequest().inventoryItems?.add(InventoryCheckListItem().apply {
            name = getString(R.string.head_rest)
            qty = binding.tvCountHeadRest.text.toString()
        })
        viewModel.provideInventoryDetailRequest().inventoryItems?.add(InventoryCheckListItem().apply {
            name = getString(R.string.floor_mats)
            qty = binding.tvCountFloorMats.text.toString()
        })
        viewModel.provideInventoryDetailRequest().inventoryItems?.add(InventoryCheckListItem().apply {
            name = getString(R.string.wheel_cap)
            qty = binding.tvCountWheelCap.text.toString()
        })
        viewModel.provideInventoryDetailRequest().inventoryItems?.add(InventoryCheckListItem().apply {
            name = getString(R.string.mud_flap)
            qty = binding.tvCountMudFlap.text.toString()
        })
        if (::inventoryCheckList.isInitialized) {
            inventoryCheckList.forEach {
                if (it.isSelected == true) viewModel.provideInventoryDetailRequest().inventoryChecks?.add(
                    it.name ?: "")
            }
        }
        if (::carDocsList.isInitialized) {
            carDocsList.forEach {
                if (it.isSelected == true) viewModel.provideInventoryDetailRequest().carDocuments?.add(
                    it.name ?: "")
            }
        }
        viewModel.provideInventoryDetailRequest().inventoryRemarks =
            binding.etInventoryRemarks.text.toString()
        viewModel.provideInventoryDetailRequest().fuelMeter = fuelPercentage.toString()
        viewModel.provideInventoryDetailRequest().id = getInventoryResponse.id
    }

    private fun addToInventoryCheckList() {
        CommonUtils.hideKeyboardFragment(this)
        if (::orderInventoryCheckListAdapter.isInitialized) {
            val item = OrderInventoryCheckListItemModel()
            item.name = binding.etAddInventory.text.toString().trim()
            item.isSelected = true
            orderInventoryCheckListAdapter.addItemToAdapterList(item)
            binding.etAddInventory.setText("")
            binding.etAddInventory.clearFocus()

            isInventoryChanged = true
        }
    }

    private fun handleAccessoriesCounts(position: Int, isMinus: Boolean) {
        when (position) {
            0 -> {
                if (isMinus) {
                    var count = binding.tvCountHeadRest.text.toString().toInt()
                    if (count > 0) {
                        count--
                        binding.tvCountHeadRest.text = count.toString()
                    }
                } else {
                    var count = binding.tvCountHeadRest.text.toString().toInt()
                    if (count < 10) {
                        count++
                        binding.tvCountHeadRest.text = count.toString()
                    }
                }
            }
            1 -> {
                if (isMinus) {
                    var count = binding.tvCountFloorMats.text.toString().toInt()
                    if (count > 0) {
                        count--
                        binding.tvCountFloorMats.text = count.toString()
                    }
                } else {
                    var count = binding.tvCountFloorMats.text.toString().toInt()
                    if (count < 8) {
                        count++
                        binding.tvCountFloorMats.text = count.toString()
                    }
                }
            }
            2 -> {
                if (isMinus) {
                    var count = binding.tvCountWheelCap.text.toString().toInt()
                    if (count > 0) {
                        count--
                        binding.tvCountWheelCap.text = count.toString()
                    }
                } else {
                    var count = binding.tvCountWheelCap.text.toString().toInt()
                    if (count < 8) {
                        count++
                        binding.tvCountWheelCap.text = count.toString()
                    }
                }
            }
            3 -> {
                if (isMinus) {
                    var count = binding.tvCountMudFlap.text.toString().toInt()
                    if (count > 0) {
                        count--
                        binding.tvCountMudFlap.text = count.toString()
                    }
                } else {
                    var count = binding.tvCountMudFlap.text.toString().toInt()
                    if (count < 4) {
                        count++
                        binding.tvCountMudFlap.text = count.toString()
                    }
                }
            }
        }

        isInventoryChanged = true
    }

    private fun startRecording() {
        if (ActivityCompat.checkSelfPermission(requireActivity(),
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.RECORD_AUDIO),
                readStoragePermission)
        } else {
            if (!isStart) {
                val vib =
                    requireActivity().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator // Vibrate for 500 milliseconds
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vib.vibrate(VibrationEffect.createOneShot(200,
                        VibrationEffect.DEFAULT_AMPLITUDE))
                } else {
                    vib.vibrate(200)
                }
                deleteAnyPreviousRecording()
                isStart = true
            } else {
                val vib =
                    requireActivity().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator // Vibrate for 500 milliseconds
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vib.vibrate(VibrationEffect.createOneShot(200,
                        VibrationEffect.DEFAULT_AMPLITUDE))
                } else {
                    vib.vibrate(200)
                }
                stopRecording()
                isStart = false
            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, p1: Int, p2: Boolean) { //do nothing
    }

    override fun onStartTrackingTouch(p0: SeekBar?) {
        pausePlaying()
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        onSeekBarChanged(seekBar?.progress ?: 0)
    }

    override fun onCheckedChanged(checkBox: CompoundButton?, isChecked: Boolean) { //do nothing
    }

    override fun onKey(p0: View?, keyCode: Int, p2: KeyEvent?): Boolean {
        return if (!backHandled) {
            backHandled = true
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (checkForBack()) {
                    showConfirmationDialog()
                } else {
                    popBackStackAndHideKeyboard()
                }
            }
            true
        } else true
    }

    fun updateScreen() {
        if (!isInventorySet) {
            showLoader()
            viewModel.getOrderDetail(OrderDetailWrapperFragment.orderId)
            if (::carDocsList.isInitialized) {
                if (!isNewOrder) viewModel.getInventoryDetail()
            } else viewModel.getInventoryCheckListAndCarDocsAPI(twoWheelerType)
        }
    }

}