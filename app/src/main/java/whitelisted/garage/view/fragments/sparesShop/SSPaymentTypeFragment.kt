package whitelisted.garage.view.fragments.sparesShop

import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.GetPaymentModesRequest
import whitelisted.garage.api.request.SSCheckoutRequest
import whitelisted.garage.api.response.AddressResponseModel
import whitelisted.garage.api.response.OptionsItem
import whitelisted.garage.api.response.PaymentTypesResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentSsPaymentTypeBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.PaymentOptionsAdapter
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel

class SSPaymentTypeFragment : BaseFragment() {

    private val binding by viewBinding(FragmentSsPaymentTypeBinding::inflate)
    private val viewModel: SparesShopFragmentViewModel by sharedViewModel()

    private lateinit var selectedAddress: AddressResponseModel
    private var isAccCart = false
    private var selectedPaymentType: Int = -1
    private var codOtpAmount: Double = -1.0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupScreen()
    }

    private fun setupScreen() {
        setObservers()
        clickListeners()

        if (arguments?.getBoolean(AppENUM.IS_EMI, false) == true) {
            binding.clBaseFSSP.makeVisible(false)
        }

        isAccCart = arguments?.getBoolean(AppENUM.IS_ACCESSORIES_CART, false) ?: false

        (arguments?.getParcelable(AppENUM.ADDRESS_POST) as? AddressResponseModel)?.let {
            selectedAddress = it
            binding.tvDelAdd.text = selectedAddress.address
        }

        //        if ((viewModel.providePaymentTypesResponse().value?.size ?: 0) > 0) {
        //            setData(viewModel.providePaymentTypesResponse().value ?: mutableListOf())
        //        } else
        getPaymentTypes()

        setFragmentResultListener(AppENUM.SELECT_ADDRESS_PAYMENT) { _, bundle -> // We use a String here, but any type that can be put in a Bundle is supported
            val result: Parcelable? = bundle.getParcelable(AppENUM.ADDRESS_POST)
            (result as? AddressResponseModel)?.let {
                selectedAddress = it
                binding.tvDelAdd.text = it.address
            }
        }
    }

    private fun getPaymentTypes() {
        if (::selectedAddress.isInitialized) {
            showLoader()
            val req = GetPaymentModesRequest()
            req.cartId = arguments?.getString(AppENUM.CART_ID, "")
            req.cus_address = selectedAddress
            lifecycleScope.launch {
                val paymentTypes = viewModel.getPaymentTypes(req)
                viewModel.setPaymentTypes(paymentTypes)
                hideLoader()
                setData(paymentTypes)
            }
        }
    }

    private fun setData(paymentTypes: List<PaymentTypesResponseModel>) {
        paymentTypes.find { it.enumName == AppENUM.PaymentTypeENUM.ONLINE }?.let {
            if (it.isEnabled == true) {
                binding.clPayNow.makeVisible(true)
            }
        }
        paymentTypes.find { it.enumName == AppENUM.PaymentTypeENUM.CASH }?.let {
            if (it.isEnabled == true) {
                binding.clPayOnDel.makeVisible(true)
                if (it.text.isNullOrEmpty()) {
                    binding.lblUpfront.makeVisible(false)
                } else {
                    binding.lblUpfront.makeVisible(true)
                    binding.lblUpfront.text = it.text
                }
                it.extraCashback?.let { extraCb ->
                    binding.clExtraCashback.makeVisible(true)
                    ImageLoader.loadImage(binding.imgBgEC, extraCb.bgImage, R.drawable.ic_placeholder_go_coins_banner)
                    ImageLoader.loadImage(binding.imgEC, extraCb.icon)
                    binding.tvECHeading.text = extraCb.heading
                    binding.tvECSubHeading.text = extraCb.subHeading
                } ?: run {
                    binding.clExtraCashback.makeVisible(false)
                }
                codOtpAmount = it.codOtp ?: 0.0
            }
        }
        paymentTypes.find { it.enumName == AppENUM.PaymentTypeENUM.UPI }?.let {
            if (it.isEnabled == true) {
                binding.llUPI.makeVisible(true)
                setUpiOptionsAdapter(it)
            }
        }
        paymentTypes.find { it.enumName == AppENUM.PaymentTypeENUM.WALLET }?.let {
            if (it.isEnabled == true) {
                binding.llWallets.makeVisible(true)
                setWalletOptionsAdapter(it)
            }
        }
        paymentTypes.find { it.enumName == AppENUM.PaymentTypeENUM.EMI }?.let {
            if (it.isEnabled == true) {
                binding.clEMI.makeVisible(true)
            }
        }

        proceedToSetupScreen()
    }

    private fun proceedToSetupScreen() {
        val totalDiscountedAmount = arguments?.getString(AppENUM.AMOUNT_TO_BE_PURCHASED, "")
        "${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol)
        }$totalDiscountedAmount".apply {
            binding.tvTotalPriceFSSPT.text = this
        }

        if (arguments?.getBoolean(AppENUM.IS_EMI, false) == true) {
            binding.clBaseFSSP.makeVisible(false)
            binding.rbEmi.callOnClick()
            binding.btnPlaceOrder.callOnClick()
        } /*else {
            if (isAccCart) {
                val totalAmount =
                    viewModel.provideGetCartResponse().cartList[0].lineItems.filter { item -> item.inventory ?: 0 > 0 }.sumOf { ci ->
                        (ci.gomTotalAmount ?: 0.0)
                    }
                val totalDiscount = (viewModel.provideGetCartResponse().cartList[0].goCoinsDiscount ?: 0.0) + (viewModel.provideGetCartResponse().cartList[0].discount ?: 0.0)
                val totalDiscountedAmount = viewModel.provideGetCartResponse().cartList[0].discountedAmount
                "${
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        AppENUM.RefactoredStrings.defaultCurrencySymbol)
                }${CommonUtils.convertDoubleTo2DecimalPlaces(totalDiscountedAmount)}".apply {
                    binding.tvTotalPriceFSSPT.text = this
                }
            } else {
                val totalAmount =
                    viewModel.provideGetCartResponse().cartList[1].lineItems.filter { item -> item.inventory ?: 0 > 0 }.sumOf { ci ->
                        (ci.gomTotalAmount ?: 0.0)
                    }
                val totalDiscount = (viewModel.provideGetCartResponse().cartList[1].goCoinsDiscount ?: 0.0) + (viewModel.provideGetCartResponse().cartList[1].discount ?: 0.0)
                val totalDiscountedAmount = totalAmount.minus(totalDiscount)
                "${
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        AppENUM.RefactoredStrings.defaultCurrencySymbol)
                }${CommonUtils.convertDoubleTo2DecimalPlaces(totalDiscountedAmount)}".apply {
                    binding.tvTotalPriceFSSPT.text = this
                }
            }
        }*/
    }

    private fun setWalletOptionsAdapter(paymentTypesResponseModel: PaymentTypesResponseModel) {
        paymentTypesResponseModel.options?.filter { it.isEnabled == true }?.let {
            it.forEach { item -> item.isSelected = false }
            binding.rvWallets.adapter = PaymentOptionsAdapter(it, this)
        } ?: binding.llWallets.makeVisible(false)
    }

    private fun setUpiOptionsAdapter(paymentTypesResponseModel: PaymentTypesResponseModel) {
        paymentTypesResponseModel.options?.filter { it.isEnabled == true }?.let {
            it.forEach { item -> item.isSelected = false }
            binding.rvUPI.adapter = PaymentOptionsAdapter(it, this)
        } ?: binding.llUPI.makeVisible(false)
    }

    private fun setObservers() {
        observeOfflineCheckout()
        observeCheckoutFailureEvent()
        observeCheckoutSuccessEvent()
    }

    private fun observeCheckoutSuccessEvent() {
        viewModel.provideCheckoutSuccessEvent().observe(viewLifecycleOwner) {
            hideLoader()
            viewModel.getSSCartAPI()
            CommonUtils.showToast(requireContext(), getString(R.string.order_placed_successfully))
            removeAllFragmentsTill(SparesShopFragment::class.java.simpleName)
            CommonUtils.addFragmentUtil(requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_ORDER_HISTORY))
            firePlaceOrder(true)
        }
    }

    private fun observeCheckoutFailureEvent() {
        viewModel.provideCheckoutFailureEvent().observe(viewLifecycleOwner) {
            binding.clBaseFSSP.makeVisible(true)
            hideLoader()
            showPaymentFailedBottomSheet()
            firePlaceOrder(false)
        }
    }

    private fun showPaymentFailedBottomSheet() {
        requireActivity().let {
            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PAYMENT_FAILED,
                params = Bundle().apply {
                    putBoolean(AppENUM.IS_ACCESSORIES_CART, isAccCart)
                })?.let { baseFragment ->
                baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
            }
        }
    }

    private fun observeOfflineCheckout() {
        viewModel.provideOfflineCheckoutResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    viewModel.provideCheckoutSuccessEvent()
                        .postValue(it.body.data) //                    popBackStackAndHideKeyboard()
                    firePlaceOrder(true)
                }
                is Result.Failure -> {
                    firePlaceOrder(false)
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun clickListeners() {
        binding.imgBackFSSPT.setOnClickListener(this)
        binding.clPayNow.setOnClickListener(this) //        clPayOnline.setOnClickListener(this)
        binding.clPayCash.setOnClickListener(this)
        binding.rbPayNow.setOnClickListener(this) //        rbPayOnline.setOnClickListener(this)
        binding.rbPayCash.setOnClickListener(this)
        binding.rbEmi.setOnClickListener(this)
        binding.btnPlaceOrder.setOnClickListener(this)
        binding.clDelAddress.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBackFSSPT -> popBackStackAndHideKeyboard()

            R.id.clPayNow, R.id.rbPayNow -> binding.apply {
                selectedPaymentType = 0
                rbPayNow.isChecked = true
                rbPayCash.isChecked = false
                rbEmi.isChecked = false
                deSelectUpiWalletOptions()
                fireSelectModeEvent(AppENUM.MODE_ONLINE)
            }

            R.id.clPayCash, R.id.rbPayCash -> binding.apply {
                selectedPaymentType = 1
                rbPayNow.isChecked = false
                rbPayCash.isChecked = true
                rbEmi.isChecked = false
                deSelectUpiWalletOptions()
                fireSelectModeEvent(AppENUM.MODE_OFFLINE)
            }

            R.id.rbEmi -> binding.apply {
                selectedPaymentType = 4
                rbPayNow.isChecked = false
                rbPayCash.isChecked = false
                rbEmi.isChecked = true
                deSelectUpiWalletOptions()
                fireSelectModeEvent(AppENUM.MODE_EMI)
            }

            R.id.btnPlaceOrder -> {
                if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.OWNER_NAME, "")
                        .isNotEmpty()) {
                    checkPaymentTypeAndRedirect()
                } else {
                    showCompleteRegisterFragmentForName()
                }
            }

            R.id.clDelAddress -> {
                requireActivity().let {
                    FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.SELECT_ADDRESS,
                        Bundle().apply {
                            putBoolean(AppENUM.FROM_SS_PAYMENT, true)
                            if (::selectedAddress.isInitialized) putParcelable(AppENUM.ADDRESS_POST,
                                selectedAddress)
                        })?.let { baseFragment ->
                            baseFragment.show(it.supportFragmentManager,
                                baseFragment.javaClass.name)
                        }
                }
            }

            R.id.clBaseIPO -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let {
                    CommonUtils.genericCastOrNull<OptionsItem>(v.getTag(R.id.model))
                        ?.let { optionsItem ->
                            binding.apply {
                                if (optionsItem.enumName == "upi") {
                                    selectedPaymentType = 2
                                    (rvUPI.adapter as? PaymentOptionsAdapter)?.let { adapter ->
                                        adapter.currentSelectedPosition.let { currentPos ->
                                            if (currentPos != -1) {
                                                adapter.dataList[currentPos].isSelected = false
                                                adapter.notifyItemChanged(currentPos)
                                            }
                                            adapter.currentSelectedPosition = it
                                            adapter.dataList[it].isSelected = true
                                            adapter.notifyItemChanged(it)
                                        }
                                    }

                                    (rvWallets.adapter as? PaymentOptionsAdapter)?.let { adapter ->
                                        if (adapter.currentSelectedPosition != -1) {
                                            adapter.dataList[adapter.currentSelectedPosition].isSelected =
                                                false
                                            adapter.notifyItemChanged(adapter.currentSelectedPosition)
                                            adapter.currentSelectedPosition = -1
                                        }
                                    }
                                } else {
                                    selectedPaymentType = 3
                                    (rvWallets.adapter as? PaymentOptionsAdapter)?.let { adapter ->
                                        adapter.currentSelectedPosition.let { currentPos ->
                                            if (currentPos != -1) {
                                                adapter.dataList[currentPos].isSelected = false
                                                adapter.notifyItemChanged(currentPos)
                                            }
                                            adapter.currentSelectedPosition = it
                                            adapter.dataList[it].isSelected = true
                                            adapter.notifyItemChanged(it)
                                        }
                                    }

                                    (rvUPI.adapter as? PaymentOptionsAdapter)?.let { adapter ->
                                        if (adapter.currentSelectedPosition != -1) {
                                            adapter.dataList[adapter.currentSelectedPosition].isSelected =
                                                false
                                            adapter.notifyItemChanged(adapter.currentSelectedPosition)
                                            adapter.currentSelectedPosition = -1
                                        }
                                    }
                                }

                                binding.rbEmi.isChecked = false
                                binding.rbPayCash.isChecked = false
                                binding.rbPayNow.isChecked = false
                            }
                        }
                }
            }
        }
    }

    private fun showCompleteRegisterFragmentForName() {
        requireActivity().let {
            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.COMPLETE_REGISTRATION,
                Bundle().apply {
                    this.putBoolean(AppENUM.IS_ONLY_NAME, true)
                },
                object : ActionListener {
                    override fun onActionItem(extra: Any?, extra2: Any?) {
                        if (extra as? Boolean? == true) {
                            if (::selectedAddress.isInitialized) {
                                selectedAddress.name = extra2 as? String
                            }
                            checkPaymentTypeAndRedirect()

                        }
                    }
                })?.let { baseFragment ->
                baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
            }
        }
    }

    private fun checkPaymentTypeAndRedirect() {
        when (selectedPaymentType) {
            0 -> {
                arguments?.let {
                    Bundle().apply {
                        putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                        putString(AppENUM.IS_FROM,
                            AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP)
                        putString(AppENUM.CART_ID, it.getString(AppENUM.CART_ID, ""))
                        putString(AppENUM.AMOUNT_TO_BE_PURCHASED,
                            it.getString(AppENUM.AMOUNT_TO_BE_PURCHASED, ""))
                        putParcelable(AppENUM.ADDRESS_POST, selectedAddress)
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, this))
                    }
                }
            }
            1 -> lifecycleScope.launch { //                        (viewModel.provideGetCartResponse().upfrontPercent ?: 0.0).let { upfront ->
                //                            if (upfront > 0) {
                //                                arguments?.let {
                //                                    Bundle().apply {
                //                                        putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                //                                        putString(AppENUM.IS_FROM,
                //                                            AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP)
                //                                        putString(AppENUM.CART_ID,
                //                                            it.getString(AppENUM.CART_ID, ""))
                //                                        putString(AppENUM.AMOUNT_TO_BE_PURCHASED,
                //                                            it.getString(AppENUM.AMOUNT_TO_BE_PURCHASED, ""))
                //                                        putParcelable(AppENUM.ADDRESS_POST,
                //                                            if (::changedAddress.isInitialized) changedAddress else it.getParcelable(
                //                                                AppENUM.ADDRESS_POST))
                //                                        CommonUtils.addFragmentUtil(activity,
                //                                            FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS,
                //                                                this))
                //                                    }
                //                                }
                //                            } else {
                //                            }
                //                        }

                if (codOtpAmount > 0) {
                    if ((arguments?.getString(AppENUM.AMOUNT_TO_BE_PURCHASED, "0")?.toDouble()
                            ?: 0.0) >= codOtpAmount) verifyOTP()
                    else startOfflineCheckout()
                } else {
                    startOfflineCheckout()
                }
            }
            2 -> {
                (binding.rvUPI.adapter as? PaymentOptionsAdapter)?.let { adapter ->
                    if (adapter.currentSelectedPosition != -1) {
                        arguments?.let {
                            Bundle().apply {
                                putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                                putString(AppENUM.IS_FROM,
                                    AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP)
                                putString(AppENUM.CART_ID, it.getString(AppENUM.CART_ID, ""))
                                putString(AppENUM.AMOUNT_TO_BE_PURCHASED,
                                    it.getString(AppENUM.AMOUNT_TO_BE_PURCHASED, ""))
                                putParcelable(AppENUM.ADDRESS_POST, selectedAddress)
                                putString(AppENUM.RazorPayENUM.RAZORPAY_METHOD,
                                    adapter.dataList[adapter.currentSelectedPosition].enumMethod)
                                putString(AppENUM.RazorPayENUM.RAZORPAY_MODE,
                                    adapter.dataList[adapter.currentSelectedPosition].enumMode)
                                CommonUtils.addFragmentUtil(activity,
                                    FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS,
                                        this))
                            }
                        }
                    }
                }
            }
            3 -> {
                (binding.rvWallets.adapter as? PaymentOptionsAdapter)?.let { adapter ->
                    if (adapter.currentSelectedPosition != -1) {
                        arguments?.let {
                            Bundle().apply {
                                putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                                putString(AppENUM.IS_FROM,
                                    AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP)
                                putString(AppENUM.CART_ID, it.getString(AppENUM.CART_ID, ""))
                                putString(AppENUM.AMOUNT_TO_BE_PURCHASED,
                                    it.getString(AppENUM.AMOUNT_TO_BE_PURCHASED, ""))
                                putParcelable(AppENUM.ADDRESS_POST, selectedAddress)
                                putString(AppENUM.RazorPayENUM.RAZORPAY_METHOD,
                                    adapter.dataList[adapter.currentSelectedPosition].enumMethod)
                                putString(AppENUM.RazorPayENUM.RAZORPAY_MODE,
                                    adapter.dataList[adapter.currentSelectedPosition].enumMode)
                                CommonUtils.addFragmentUtil(activity,
                                    FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS,
                                        this))
                            }
                        }
                    }
                }
            }
            4 -> {
                arguments?.let {
                    Bundle().apply {
                        putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                        putString(AppENUM.IS_FROM,
                            AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_SPARES_SHOP)
                        putString(AppENUM.CART_ID, it.getString(AppENUM.CART_ID, ""))
                        putString(AppENUM.AMOUNT_TO_BE_PURCHASED,
                            it.getString(AppENUM.AMOUNT_TO_BE_PURCHASED, ""))
                        putParcelable(AppENUM.ADDRESS_POST, selectedAddress)
                        putString(AppENUM.RazorPayENUM.RAZORPAY_METHOD, "emi")
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, this))
                    }
                }
            }
            else -> CommonUtils.showToast(requireContext(),
                getString(R.string.please_select_a_payment_type))
        }
    }

    private fun deSelectUpiWalletOptions() {
        (binding.rvUPI.adapter as? PaymentOptionsAdapter)?.let {
            if (it.currentSelectedPosition != -1) {
                it.dataList[it.currentSelectedPosition].isSelected = false
                it.notifyItemChanged(it.currentSelectedPosition)
                it.currentSelectedPosition = -1
            }
        }

        (binding.rvWallets.adapter as? PaymentOptionsAdapter)?.let {
            if (it.currentSelectedPosition != -1) {
                it.dataList[it.currentSelectedPosition].isSelected = false
                it.notifyItemChanged(it.currentSelectedPosition)
                it.currentSelectedPosition = -1
            }
        }
    }

    private suspend fun verifyOTP() {
        showLoader()
        startSMSListener()
        val mobile = viewModel.getStringSharedPreference(AppENUM.MOBILE_NUMBER)
        val hashCode = AppSignatureHelper(requireContext()).getAppSignatures()[0]
        val countryCode = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
            getString(R.string.ind_country_code))
        val otp = viewModel.sendOTP(hashCode, countryCode, mobile)
        hideLoader()
        if (otp != null) {
            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.VERIFY_OTP_SHEET,
                Bundle().apply {
                    putString(AppENUM.MOBILE_NUMBER, mobile)
                    putString(AppENUM.OTP, otp)
                },
                object : ActionListener {
                    override fun onActionItem(extra: Any?, extra2: Any?) {
                        showLoader()
                        startOfflineCheckout()
                    }
                })?.let { baseFragment ->
                baseFragment.show(requireActivity().supportFragmentManager,
                    baseFragment.javaClass.name)
            }
        }
    }

    private fun startOfflineCheckout() {
        arguments?.let {
            showLoader()
            viewModel.checkoutOffline(SSCheckoutRequest(paymentAmount = it.getString(AppENUM.AMOUNT_TO_BE_PURCHASED,
                ""),
                paymentType = "offline",
                paymentStatus = "later",
                paymentCurrency = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                    AppENUM.RefactoredStrings.defaultCurrency),
                cartId = it.getString(AppENUM.CART_ID, ""),
                clientAddressDetails = selectedAddress))
        }
    }

    private fun firePlaceOrder(paymentStatus: Boolean) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.TOTAL_AMOUNT,
            arguments?.getString(AppENUM.AMOUNT_TO_BE_PURCHASED, ""))
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PAYMENT_MODE,
            AppENUM.MODE_OFFLINE)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PAYMENT_STATUS, paymentStatus.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_PAYMENT_TYPE)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.PLACE_ORDER,
            bundle)
    }

    private fun fireSelectModeEvent(mode: String) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PAYMENT_MODE, mode)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_PAYMENT_TYPE)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.SELECT_MODE,
            bundle)
    }

}