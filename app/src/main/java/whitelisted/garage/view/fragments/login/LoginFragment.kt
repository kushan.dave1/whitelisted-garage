package whitelisted.garage.view.fragments.login

import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Intent
import android.content.IntentSender
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.Nullable
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.google.android.gms.auth.api.identity.GetPhoneNumberHintIntentRequest
import com.google.android.gms.auth.api.identity.Identity
import com.truecaller.android.sdk.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.App
import whitelisted.garage.R
import whitelisted.garage.api.data.IllustrationImage
import whitelisted.garage.api.request.TrueCallerLoginRequest
import whitelisted.garage.api.response.CountryCodeResponseModel
import whitelisted.garage.api.response.LocationDetailsResponse
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.broadcast_receivers.MySMSBroadcastReceiver
import whitelisted.garage.databinding.FragmentLoginBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.view.activities.DashboardActivity
import whitelisted.garage.view.adapter.IllustrationsAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.LoginViewModel
import kotlin.math.abs

class LoginFragment : BaseFragment(), ITrueCallback {


    private val binding by viewBinding(FragmentLoginBinding::inflate)

    private var languageValue = "en"
    private val viewModel: LoginViewModel by sharedViewModel()
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var smsReceiver: MySMSBroadcastReceiver? = null
    private var sliderHandler = Handler(Looper.getMainLooper())
    private var sliderRunnable = Runnable {
        if (isResumed) binding.vpIllustration.currentItem = binding.vpIllustration.currentItem + 1
    }
    private var sliderWidth = 0
    private lateinit var animationSet: AnimatorSet
    private var countryCodes = mutableListOf<CountryCodeResponseModel>()
    private var selectedCountryFlag = AppENUM.RefactoredStrings.defaultFlag

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
        setUpLanguage()
        observeCheckBackPressEvent()

        binding.imgCountryFlag.let { imgView ->
            ImageLoader.loadImage(imgView, selectedCountryFlag)
        }

        showLoader()
        viewModel.getCountryCodes()

        if (sliderWidth == 0) {
            binding.viewSlider1.viewTreeObserver.addOnWindowFocusChangeListener {
                if (isResumed) {
                    sliderWidth = binding.viewSlider1.measuredWidth
                    updateSliderIndicator(0)
                }
            }
        }
    }

    private fun observeCheckBackPressEvent() {
        dashboardViewModel.provideCheckBackPressEvent().observe(viewLifecycleOwner) {
            it?.let { value ->
                if (value) {
                    (requireActivity() as? DashboardActivity)?.backPressEvent()
                }
            }
        }
    }

    private fun setUpLanguage() {
        languageValue = viewModel.getStringSharedPreference(AppENUM.RefactoredStrings.LANGUAGE_CODE,
            AppENUM.LanguageConstants.LAN_DEFAULT)

        when (languageValue) {
            AppENUM.LanguageConstants.LAN_HINGLISH -> {
                binding.tvLanguage.text = AppENUM.LanguageConstants.HINGLISH
            }
            AppENUM.LanguageConstants.LAN_HINDI -> {
                binding.tvLanguage.text = AppENUM.LanguageConstants.HINDI
            }
            AppENUM.LanguageConstants.LAN_ENGLISH -> {
                binding.tvLanguage.text = AppENUM.LanguageConstants.ENGLISH
            }
            AppENUM.LanguageConstants.LAN_BANGLA -> {
                binding.tvLanguage.text = AppENUM.LanguageConstants.BANGLA
            }
            AppENUM.LanguageConstants.LAN_GUJARAT -> {
                binding.tvLanguage.text = AppENUM.LanguageConstants.GUJARAT
            }
            AppENUM.LanguageConstants.LAN_KANNADA -> {
                binding.tvLanguage.text = AppENUM.LanguageConstants.KANNADA
            }
            AppENUM.LanguageConstants.LAN_MARATHI -> {
                binding.tvLanguage.text = AppENUM.LanguageConstants.MARATHI
            }
            AppENUM.LanguageConstants.LAN_TAMIL -> {
                binding.tvLanguage.text = AppENUM.LanguageConstants.TAMIL
            }
            AppENUM.LanguageConstants.LAN_TELEAGU -> {
                binding.tvLanguage.text = AppENUM.LanguageConstants.TELEAGU
            }
        }
    }

    private fun setupScreen() {
        setClickListeners()
        setObservers()
        observeTruecallerLoginResponse()
        setUpTrueCaller()

        binding.viewSlider1Progress.layoutParams.width = 0
        binding.viewSlider2Progress.layoutParams.width = 0
        binding.viewSlider3Progress.layoutParams.width = 0
        setUpViewPager() //        startSMSListener()
    }

    private fun setUpViewPager() {
        mutableListOf<IllustrationImage>().apply {
            add(IllustrationImage(R.drawable.ic_illustration_image_1))
            add(IllustrationImage(R.drawable.ic_illustration_image_2))
            add(IllustrationImage(R.drawable.ic_illustration_image_3))
            binding.vpIllustration.adapter = IllustrationsAdapter(this, binding.vpIllustration)
        }

        binding.vpIllustration.clipToPadding = false
        binding.vpIllustration.clipChildren =
            false //        binding.vpIllustration.offscreenPageLimit = 3;
        binding.vpIllustration.getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER

        val compositePageTransformer = CompositePageTransformer()
        compositePageTransformer.addTransformer(MarginPageTransformer(40))
        compositePageTransformer.addTransformer { page, position ->
            val r = "1".toInt().minus(abs(position))
            page.scaleY = 0.85f + (r * 0.15f)
        }

        binding.vpIllustration.setPageTransformer(compositePageTransformer)

        binding.vpIllustration.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                updateSliderIndicator(position)
                sliderHandler.removeCallbacks(sliderRunnable)
                sliderHandler.postDelayed(sliderRunnable, 4000) // slide duration 2 seconds
            }
        })
    }

    private fun updateSliderIndicator(position: Int) {
        if (::animationSet.isInitialized) {
            animationSet.cancel()
        }
        var flag = 0
        if (position > 2) {
            flag = (position % 3)
        }
        when (position) {
            0 -> {
                binding.viewSlider1Progress.layoutParams.width = 0
                binding.viewSlider1Progress.requestLayout()
                binding.viewSlider2Progress.layoutParams.width = 0
                binding.viewSlider2Progress.requestLayout()
                binding.viewSlider3Progress.layoutParams.width = 0
                binding.viewSlider3Progress.requestLayout()
                startAnimationForView(binding.viewSlider1Progress,
                    binding.viewSlider1Progress.layoutParams.width,
                    sliderWidth)
            }
            1 -> {
                binding.viewSlider1Progress.layoutParams.width = sliderWidth
                binding.viewSlider1Progress.requestLayout()
                binding.viewSlider3Progress.layoutParams.width = 0
                binding.viewSlider3Progress.requestLayout()
                binding.viewSlider2Progress.layoutParams.width = 0
                binding.viewSlider2Progress.requestLayout()
                startAnimationForView(binding.viewSlider2Progress,
                    binding.viewSlider2Progress.layoutParams.width,
                    sliderWidth)
            }
            2 -> {
                binding.viewSlider1Progress.layoutParams.width = sliderWidth
                binding.viewSlider1Progress.requestLayout()
                binding.viewSlider2Progress.layoutParams.width = sliderWidth
                binding.viewSlider2Progress.requestLayout()
                binding.viewSlider3Progress.layoutParams.width = 0
                binding.viewSlider3Progress.requestLayout()
                startAnimationForView(binding.viewSlider3Progress,
                    binding.viewSlider3Progress.layoutParams.width,
                    sliderWidth)
            }
            (position) -> {
                when (flag) {
                    0 -> {
                        binding.viewSlider1Progress.layoutParams.width = 0
                        binding.viewSlider1Progress.requestLayout()
                        binding.viewSlider2Progress.layoutParams.width = 0
                        binding.viewSlider2Progress.requestLayout()
                        binding.viewSlider3Progress.layoutParams.width = 0
                        binding.viewSlider3Progress.requestLayout()
                        startAnimationForView(binding.viewSlider1Progress,
                            binding.viewSlider1Progress.layoutParams.width,
                            sliderWidth)
                    }
                    1 -> {
                        binding.viewSlider1Progress.layoutParams.width = sliderWidth
                        binding.viewSlider1Progress.requestLayout()
                        binding.viewSlider3Progress.layoutParams.width = 0
                        binding.viewSlider3Progress.requestLayout()
                        binding.viewSlider2Progress.layoutParams.width = 0
                        binding.viewSlider2Progress.requestLayout()
                        startAnimationForView(binding.viewSlider2Progress,
                            binding.viewSlider2Progress.layoutParams.width,
                            sliderWidth)
                    }
                    2 -> {
                        binding.viewSlider1Progress.layoutParams.width = sliderWidth
                        binding.viewSlider1Progress.requestLayout()
                        binding.viewSlider2Progress.layoutParams.width = sliderWidth
                        binding.viewSlider2Progress.requestLayout()
                        binding.viewSlider3Progress.layoutParams.width = 0
                        binding.viewSlider3Progress.requestLayout()
                        startAnimationForView(binding.viewSlider3Progress,
                            binding.viewSlider3Progress.layoutParams.width,
                            sliderWidth)
                    }
                }
            }
        }
    }

    @SuppressLint("Recycle")
    private fun startAnimationForView(view: View, currentWidth: Int, newWidth: Int) {
        (ValueAnimator.ofInt(currentWidth, newWidth).setDuration(4000)).apply {/* We use an update listener which listens to each tick
           * and manually updates the height of the view  */
            addUpdateListener { anim ->
                val value = anim.animatedValue.toString().toInt()
                val params = view.layoutParams
                params.width = value
                view.layoutParams = params
                view.requestLayout()
            }

            /*  We use an animationSet to play the animation  */
            animationSet = AnimatorSet()
            animationSet.interpolator = DecelerateInterpolator()
            animationSet.play(this)
            animationSet.start()
        }
    }

    override fun onStart() {
        super.onStart()

        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_LOGIN)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_LOGIN,
                this)
        }
    }

    private fun observeTruecallerLoginResponse() {
        viewModel.provideTrueCallerLoginResponse().observe(requireActivity()) {
            hideLoader()
            if (isAdded) {
                when (it) {
                    is Result.Success -> {
                        (requireActivity().application as? App)?.reStartModule()
                        CommonUtils.replaceFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.BOTTOM_NAV, null))
                    }
                    is Result.Failure -> {
                        CommonUtils.showToast(requireActivity(), it.errorMessage, true)
                        if (it.code == 404) {
                            CommonUtils.showToast(requireActivity(), it.errorMessage, true)
                            CommonUtils.addFragmentUtil(requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.SIGN_UP,
                                    Bundle().apply {
                                        putString(AppENUM.IntentKeysENUM.MOBILE_NUMBER,
                                            binding.etPhoneNumber.text.toString())
                                        putString(AppENUM.IntentKeysENUM.COUNTRY_CODE,
                                            binding.tvCountryCode.text.toString())
                                        putString(AppENUM.IntentKeysENUM.COUNTRY_FLAG,
                                            selectedCountryFlag)
                                    }))
                        }
                    }
                }
            }
        }
    }

    private fun setObservers() {
        getOtpForLoginObserver()
        viewModel.provideLocationDetailResponse().observe(requireActivity()) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setCurrentLocationParams(it.body.data)
                }
                is Result.Failure -> { // do nothing
                }
            }
        }

        observerCountryCodesResponse()
        viewModel.provideIPResponse().observe(requireActivity()) {
            hideLoader()
            viewModel.getLocationDetails(ipAddress = it.ip ?: "")
        }
    }

    private fun observerCountryCodesResponse() {
        viewModel.provideCountryCodesResponse().observe(requireActivity()) {
            hideLoader()
            if (isAdded) {
                when (it) {
                    is Result.Success -> {
                        countryCodes = it.body.data
                        ccList = countryCodes

                        if (countryCodes.size > 0) {
                            selectedCountryFlag = countryCodes[0].image ?: ""
                            binding.imgCountryFlag.let { imgView ->
                                ImageLoader.loadImage(imgView, selectedCountryFlag)
                            }
                            binding.tvCountryCode.text = countryCodes[0].dialCode

                            viewModel.saveUserCountryCodeDetails(countryCodes[0].image,
                                countryCodes[0].dialCode,
                                countryCodes[0].currency,
                                countryCodes[0].currencySymbol,
                                countryCodes[0].id)
                            firebaseEventForCountrySelection(countryCodes[0].dialCode,
                                countryCodes[0].currency,
                                countryCodes[0].currencySymbol) //                    showLoader()
                            //                    viewModel.getIp("https://api64.ipify.org/?format=json")
                            val countryCode =
                                CommonUtils.getCountryCodeFromTelephonyM(requireContext())
                            countryCodes.forEach { countryCodeModel ->
                                if (countryCodeModel.code == countryCode) {
                                    selectedCountryFlag =
                                        countryCodeModel.image ?: selectedCountryFlag
                                    binding.imgCountryFlag.let { imgView ->
                                        ImageLoader.loadImage(imgView, selectedCountryFlag)
                                    }

                                    binding.tvCountryCode.text = countryCodeModel.dialCode
                                }
                            }
                        }
                    }
                    is Result.Failure -> {
                        CommonUtils.showToast(requireActivity(), it.errorMessage, true)
                    }
                }
            }
        }
    }

    private fun getOtpForLoginObserver() {
        viewModel.provideOTPForLoginResponse().observe(requireActivity()) {
            hideLoader()
            when (it) {
                is Result.Success<*> -> {
                    getOtpSuccess(it)
                }
                is Result.Failure -> {
                    if (isAdded) {
                        if (it.code == 404) {
                            CommonUtils.showToast(requireActivity(), it.errorMessage, true)
                            CommonUtils.addFragmentUtil(requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.SIGN_UP,
                                    Bundle().apply {
                                        putString(AppENUM.IntentKeysENUM.MOBILE_NUMBER,
                                            binding.etPhoneNumber.text.toString())
                                        putString(AppENUM.IntentKeysENUM.COUNTRY_CODE,
                                            binding.tvCountryCode.text.toString())
                                        putString(AppENUM.IntentKeysENUM.COUNTRY_FLAG,
                                            selectedCountryFlag)
                                    }))
                        } else {
                            CommonUtils.showToast(requireActivity(), it.errorMessage)
                        }
                    }
                }
            }
        }
    }

    private fun getOtpSuccess(it: Result.Success<*>) {
        if (isAdded) {
            startSMSListener()
            if (isAdded && isVisible) {
                CommonUtils.addFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.OTP_FRAGMENT,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.MOBILE_NUMBER,
                                binding.etPhoneNumber.text.toString())
                            putString(AppENUM.IntentKeysENUM.COUNTRY_CODE,
                                binding.tvCountryCode.text.toString())
                            putBoolean(AppENUM.IntentKeysENUM.IS_LOGIN, true)
                        }))
            }
            if (BuildConfig.BUILD_TYPE == "debug") {
                CommonUtils.showToast(requireContext(), it.body.toString())
            }
        }
    }

    companion object {
        var ccList: MutableList<CountryCodeResponseModel>? = mutableListOf()
    }

    private fun setCurrentLocationParams(data: LocationDetailsResponse) {
        if (countryCodes.size > 0) {
            countryCodes.forEach {
                if (it.code == data.countryCode) {
                    binding.imgCountryFlag.let { imgView ->
                        ImageLoader.loadImage(imgView, it.image)
                    }
                    binding.tvCountryCode.text = it.dialCode
                }
            }
        }
    }

    private fun setClickListeners() {
        binding.btnLogin.setOnClickListener(this)
        binding.btnRegister.setOnClickListener(this)
        binding.tvLanguage.setOnClickListener(this)
        binding.llCountryCode.setOnClickListener(this)
    }

    private fun showLanguageSheet() {
        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.LANGUAGE_BOTTOM,
            null,
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) { //do nothing
                }
            })?.let { baseFragment ->
            baseFragment.show(parentFragmentManager, baseFragment.javaClass.name)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnRegister -> {
                CommonUtils.addFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.SIGN_UP, null))
            }
            R.id.tvLanguage -> {
                showLanguageSheet()
            }
            R.id.btnLogin -> {
                CommonUtils.hideKeyboard(requireActivity())
                if (checkForm()) {
                    showLoader()
                    viewModel.loginUserAPI(AppSignatureHelper(requireContext()).getAppSignatures()[0],
                        binding.tvCountryCode.text.toString(),
                        binding.etPhoneNumber.text.toString())
                }
            }

            R.id.llCountryCode -> {
                FragmentFactory.fragmentDialog(FragmentFactory.Fragments.SELECT_COUNTRY_CODE,
                    Bundle().apply {
                        putString(AppENUM.COUNTRY_CODE, binding.tvCountryCode.text.toString())
                        putParcelableArrayList("", countryCodes as ArrayList)
                    },
                    object : ActionListener {
                        override fun onActionItem(extra: Any?, extra2: Any?) {
                            (extra as? CountryCodeResponseModel)?.let {
                                selectedCountryFlag = it.image ?: selectedCountryFlag
                                ImageLoader.loadImage(binding.imgCountryFlag, selectedCountryFlag)

                                binding.tvCountryCode.text = it.dialCode
                                viewModel.saveUserCountryCodeDetails(it.image,
                                    it.dialCode,
                                    it.currency,
                                    it.currencySymbol,
                                    it.id)
                                firebaseEventForCountrySelection(it.dialCode,
                                    it.currency,
                                    it.currencySymbol)
                            }

                        }
                    })?.let { dialog ->
                    dialog.show(parentFragmentManager, dialog.javaClass.name)
                }
            }
        }
    }

    private fun checkForm(): Boolean {
        if (binding.etPhoneNumber.text.toString().trim().isEmpty()) {
            CommonUtils.showToast(requireActivity(), getString(R.string.alert_empty_mobile), true)
            binding.etPhoneNumber.requestFocus()
            return false
        } else if (binding.tvCountryCode.text.toString() == getString(R.string.ind_country_code) && binding.etPhoneNumber.text.toString()
                .trim().length != 10) {
            CommonUtils.showToast(requireActivity(), getString(R.string.alert_invalid_mobile), true)
            binding.etPhoneNumber.requestFocus()
            return false
        }
        return true
    }

    override fun onStop() {
        super.onStop()
        try {
            requireActivity().unregisterReceiver(smsReceiver)
        } catch (e: Exception) { //Do nothing
        }
    }

    private fun setUpTrueCaller() {
        val trueScope = TruecallerSdkScope.Builder(requireActivity(), this)
            .consentMode(TruecallerSdkScope.CONSENT_MODE_BOTTOMSHEET)
            .loginTextPrefix(TruecallerSdkScope.LOGIN_TEXT_PREFIX_TO_GET_STARTED)
            .loginTextSuffix(TruecallerSdkScope.LOGIN_TEXT_SUFFIX_PLEASE_VERIFY_MOBILE_NO)
            .ctaTextPrefix(TruecallerSdkScope.CTA_TEXT_PREFIX_USE)
            .buttonShapeOptions(TruecallerSdkScope.BUTTON_SHAPE_ROUNDED)
            .footerType(TruecallerSdkScope.FOOTER_TYPE_CONTINUE)
            .consentTitleOption(TruecallerSdkScope.SDK_CONSENT_TITLE_LOG_IN)
            .sdkOptions(TruecallerSdkScope.SDK_OPTION_WITHOUT_OTP).build()
        TruecallerSDK.init(trueScope)

        if (TruecallerSDK.getInstance().isUsable) {
            TruecallerSDK.getInstance().getUserProfile(requireActivity())
        } else {
            getPhoneNumber()
        }
    }

    private fun getPhoneNumber() {
        try {
            val request: GetPhoneNumberHintIntentRequest =
                GetPhoneNumberHintIntentRequest.builder().build()

            Identity.getSignInClient(requireActivity()).getPhoneNumberHintIntent(request)
                .addOnSuccessListener { pendingIntent ->
                    try {
                        phoneNumberHintIntentResultLauncher.launch(IntentSenderRequest.Builder(
                            pendingIntent.intentSender).build())
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                .addOnFailureListener { //                    CommonUtils.showToast(requireContext(), it.localizedMessage ?: "")
                }
        } catch (e: IntentSender.SendIntentException) {
            e.printStackTrace()

        } catch (e: Exception) {
            e.printStackTrace()

        }
    }

    private val phoneNumberHintIntentResultLauncher = registerForActivityResult(
        ActivityResultContracts.StartIntentSenderForResult(),
    ) {
        try {
            (Identity.getSignInClient(requireContext()).getPhoneNumberFromIntent(it.data)).apply {
                binding.etPhoneNumber.setText(this)
            }
        } catch (e: Exception) {
            println("Phone Number Hint failed")
            e.printStackTrace()
        }
    }

    override fun onSuccessProfileShared(p0: TrueProfile) {
        (p0.phoneNumber.replace("+91", "")).apply {
            showLoader()
            viewModel.callTrueCallerLogin(TrueCallerLoginRequest(this, true, "login"))
        }
    }

    override fun onFailureProfileShared(p0: TrueError) {
        if (p0.errorType == 14) {
            getPhoneNumber()
        }
    }

    override fun onVerificationRequired(p0: TrueError?) { //
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        if (requestCode == TruecallerSDK.SHARE_PROFILE_REQUEST_CODE) {
            TruecallerSDK.getInstance()
                .onActivityResultObtained(requireActivity(), requestCode, resultCode, data)
        }
    }
}