package whitelisted.garage.view.fragments.bottomNavFragment

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.gms.location.LocationListener
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.nightonke.boommenu.BoomButtons.BoomButton
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum
import com.nightonke.boommenu.BoomButtons.HamButton
import com.nightonke.boommenu.OnBoomListener
import com.nightonke.boommenu.Piece.PiecePlaceEnum
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.data.SelectTabParams
import whitelisted.garage.api.data.SelectWorkshopDialogParams
import whitelisted.garage.api.request.UpdateLocationRequest
import whitelisted.garage.api.response.HomePopupItem
import whitelisted.garage.api.response.SSCategoryResponseModel
import whitelisted.garage.api.response.ScreensConfigResponse
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseBottomFragmentModel
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentBottomNavBinding
import whitelisted.garage.eventBus.NavigateToInventoryEventModel
import whitelisted.garage.eventBus.RackUpdateEvent
import whitelisted.garage.eventBus.WorkshopUpdateEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeViewCompoundDrawablesWithInterinsicBounds
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.activities.DashboardActivity
import whitelisted.garage.view.fragments.account.AccountFragment
import whitelisted.garage.view.fragments.billing.BillingFragment
import whitelisted.garage.view.fragments.home.HomeFragment
import whitelisted.garage.view.fragments.inventory.InventoryFragment
import whitelisted.garage.view.fragments.sparesShop.ShopWithFilterFragment
import whitelisted.garage.view.fragments.sparesShop.SparesShopFragment
import whitelisted.garage.viewmodels.BottomNavViewModel
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import java.util.*

class BottomNavFragment : BaseFragment(), LocationListener {

    private val viewModel: DashboardSharedViewModel by sharedViewModel()
    private val sparesShopFragmentViewModel: SparesShopFragmentViewModel by sharedViewModel()
    private val bottomNavViewModel: BottomNavViewModel by viewModel()
    private val binding by viewBinding(FragmentBottomNavBinding::inflate)
    private val myRequestCode: Int = 111
    private var bottomFragmentTreeMap: TreeMap<Int, BaseBottomFragmentModel>? = null
    private var isHomeWorkshopSelect = true
    private var newDb: DatabaseReference? = null
    private var postListener: ValueEventListener? = null
    private var database: DatabaseReference? = null
    private var isAdapterSet = false
    private var isFirebaseRewardToastEnable = false
    private var isHomePopupSet = false
    private var installStateUpdatedListener: InstallStateUpdatedListener? = null
    private var appUpdateManager: AppUpdateManager? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupScreen()
    }

    override fun onResume() {
        observeDeeplink()
        super.onResume()
    }

    private fun setupScreen() { //        initializeBMB()
        setClickListeners()
        observerGoCoinsValue()
        observeConfigAPI()
        observeSelectWorkshopValue()
        observeCheckBackPressEvent()
        observeSelectTabEvent()
        observeUpdateWorkshopVal()

        //        showLoader()
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE, "").isEmpty()) {
            showSelectWorkshopDialog(SelectWorkshopDialogParams(isStart = true, isHome = true))
        } else { //            bottomNavigationView.visibility = View.GONE
            sparesInternationalTabChanges()
            setupViewPager()

            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CONFIG_API_RESPONSE, "")
                .let {
                    if (it.isNotEmpty()) {
                        (Gson().fromJson(it, ScreensConfigResponse::class.java))?.let { config ->
                            viewModel.screensConfigResponse.value = config
                            updateTabs() //                            updateBMB(config.homePopup ?: listOf())
                        }
                    }
                }

            bottomNavViewModel.getConfigAPI()
        }

        if (viewModel.getStringSharedPreference(AppENUM.ACCESS_TOKEN_, "").isNotEmpty()) {
            if (!walkThroughInProgress) {
                (activity as? DashboardActivity)?.getLastLocation(this, false)
            }
        }

        setSoftUpdateUI()
        isFirebaseRewardToastEnable = false
        database =
            Firebase.database(BuildConfig.FirebaseDatabaseUrl).reference // set unique user id
        FirebaseAnalyticsLog.setUniqueUserOrAttribute(FirebaseAnalyticsLog.FirebaseEventNameENUM.UNIQUE_ID,
            viewModel.getStringSharedPreference(AppENUM.USER_ID))
        FirebaseAnalyticsLog.setUniqueUserOrAttribute(FirebaseAnalyticsLog.FirebaseEventNameENUM.MOBILE_NUMBER,
            viewModel.getStringSharedPreference(AppENUM.MOBILE_NUMBER))
        firebaseForRewards() //        handleBackPressTabIndicator()

        getToken()
    }

    private fun observeUpdateWorkshopVal() {
//        viewModel.provideIsWorkshopRefresh().observe(viewLifecycleOwner){
//            if (it){
//                showLoader()
//                bottomNavViewModel.getWorkshopListAPI()
//            }
//        }
    }

    private fun initializeBMB() {
        binding.imgCreateNew.onBoomListener = object : OnBoomListener {
            override fun onClicked(index: Int, boomButton: BoomButton?) {
                val intent = Intent(requireContext(), DashboardActivity::class.java)
                when (index) {
                    0 -> {
                        intent.data = Uri.parse("http://freegarage.in/?pagename=neworder")
                        startActivity(intent)
                    }
                    1 -> {
                        intent.data = Uri.parse("http://freegarage.in/?pagename=allspares")
                        startActivity(intent)
                    }
                    2 -> {
                        intent.data = Uri.parse("http://freegarage.in/?pagename=gbc")
                        startActivity(intent)
                    }
                }
            }

            override fun onBackgroundClick() {

            }

            override fun onBoomWillHide() {
                binding.imgOptions.animate().rotation(0f).setDuration(500).start()
            }

            override fun onBoomDidHide() {

            }

            override fun onBoomWillShow() {
                binding.imgOptions.animate().rotation(-135f).setDuration(500).start()
            }

            override fun onBoomDidShow() {

            }
        } //        binding.imgCreateNew.addBuilder(HamButton.Builder().normalText("Create Garage Order")
        //            .typeface(CommonUtils.getCustomFontTypeface(requireContext(), R.font.gilroy_semibold))
        //            .textSize(14).subNormalText("Click to open a new job card").subTextSize(12)
        //            .subMaxLines(2).buttonCornerRadius(10)
        //            .normalImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_hp_1))
        //            .normalColor(ContextCompat.getColor(requireContext(), R.color.hp_1))
        //            .imagePadding(Rect(20, 20, 20, 20))
        //            .highlightedColor(ContextCompat.getColor(requireContext(), R.color.black_color_new))
        //            .subTypeface(CommonUtils.getCustomFontTypeface(requireContext(), R.font.gilroy_medium))
        //            .subTextSize(12).subMaxLines(2))
        //        binding.imgCreateNew.addBuilder(HamButton.Builder().normalText("Buy Spares & Accessories")
        //            .typeface(CommonUtils.getCustomFontTypeface(requireContext(), R.font.gilroy_semibold))
        //            .textSize(14).subNormalText("Click to buy parts at upto 50% off").subTextSize(12)
        //            .subMaxLines(2)
        //            .normalImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_hp_2))
        //            .imagePadding(Rect(20, 20, 20, 20))
        //            .normalColor(ContextCompat.getColor(requireContext(), R.color.hp_2))
        //            .highlightedColor(ContextCompat.getColor(requireContext(), R.color.black_color_new))
        //            .subTypeface(CommonUtils.getCustomFontTypeface(requireContext(), R.font.gilroy_medium)))
        //        binding.imgCreateNew.addBuilder(HamButton.Builder().normalText("Create your Google website")
        //            .normalImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_hp_3))
        //            .typeface(CommonUtils.getCustomFontTypeface(requireContext(), R.font.gilroy_semibold))
        //            .imagePadding(Rect(20, 20, 20, 20)).textSize(14)
        //            .subNormalText("Click to get your personalised website").subTextSize(12).subMaxLines(2)
        //            .normalColor(ContextCompat.getColor(requireContext(), R.color.hp_3))
        //            .highlightedColor(ContextCompat.getColor(requireContext(), R.color.black_color_new))
        //            .subTypeface(CommonUtils.getCustomFontTypeface(requireContext(), R.font.gilroy_medium))
        //            .subTextSize(12).subMaxLines(2))
    }

    private fun getToken() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.d(AppENUM.FCM_TOKEN, "getInstanceId failed", task.exception)
                return@OnCompleteListener
            } // Get new Instance ID token
            val token = task.result
            Log.d(AppENUM.FCM_TOKEN, token)
            bottomNavViewModel.updateDeviceTokenAPI(token)
        })
    }

    private fun sparesInternationalTabChanges() {
        if (isInternationalUser) {
            binding.tvTab2Name.text = getString(R.string.inventory)
            binding.tvTab2Name.changeViewCompoundDrawablesWithInterinsicBounds(drawableTop = ContextCompat.getDrawable(
                requireContext(),
                R.drawable.ic_inventory_tab))

            binding.tvTab3Name.text = getString(R.string.billing)
            binding.tvTab3Name.changeViewCompoundDrawablesWithInterinsicBounds(drawableTop = ContextCompat.getDrawable(
                requireContext(),
                R.drawable.ic_billing_tab))
        }
    }

    private fun observeSelectTabEvent() {
        viewModel.provideTabSelectedEvent().observe(viewLifecycleOwner) {
            it?.let { params -> selectTab(params) }
        }
    }

    private fun observeCheckBackPressEvent() {
        viewModel.provideCheckBackPressEvent().observe(viewLifecycleOwner) {
            it?.let { value ->
                if (value) {
                    onBackPressed()
                }
            }
        }
    }

    private fun observerGoCoinsValue() {
        bottomNavViewModel.provideGoCoinsValueResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    val value = it.body.data.gocoinsValue
                    viewModel.putIntSharedPreference(AppENUM.GO_COINS_VALUE, value ?: 1)
                }
                is Result.Failure -> { //                    CommonUtils.showToast(this, it.errorMessage, true)
                }
            }
        }
    }

    private fun observeSelectWorkshopValue() {
        viewModel.provideSelectWSDialogParams().observe(viewLifecycleOwner) {
            it?.let { params ->
                lifecycleScope.launch {
                    delay(700)
                    showSelectWorkshopDialogActual(params)
                }
            }
        }
    }

    private fun observeDeeplink() {
        viewModel.provideDeeplinkPageNameResponse().observe(viewLifecycleOwner) {
            it?.let { pageName ->
                checkPageNameAndRedirect(pageName)
            }
        }
    }

    private fun setClickListeners() {
        binding.btnDownload.setOnClickListener(this)
        binding.rlTab1.setOnClickListener(this)
        binding.rlTab2.setOnClickListener(this)
        binding.rlTab3.setOnClickListener(this)
        binding.rlTab4.setOnClickListener(this)
        binding.imgCreateNew.setOnClickListener(this)
        binding.imgOptions.setOnClickListener(this)
        binding.imgCancelUpdate.setOnClickListener(this)
    }

    class ViewPagerAdapter(fragmentActivity: FragmentActivity) :
        FragmentStateAdapter(fragmentActivity) {
        var mBottomFragmentList: ArrayList<BaseBottomFragmentModel> = arrayListOf()
        private var positionChanged: Int = 0
        override fun getItemCount(): Int {
            return mBottomFragmentList.size
        }

        override fun getItemId(position: Int): Long {
            return mBottomFragmentList[position].fragmentId.toLong()
        }

        override fun containsItem(itemId: Long): Boolean {
            return mBottomFragmentList.find { itemId.toInt() == it.fragmentId }?.let { true }
                ?: false
        }

        override fun createFragment(position: Int): Fragment {
            return mBottomFragmentList[position].fragment
        }

        @SuppressLint("NotifyDataSetChanged") // here notifyDataSetChanged will not notify all the tabs rather only the tabs that are changed
        fun addFragment(position: Int? = null, baseBottomFragmentModel: BaseBottomFragmentModel?) {
            position?.let { pos ->
                baseBottomFragmentModel?.let {
                    mBottomFragmentList.add(pos, it)
                    positionChanged = pos
                    notifyDataSetChanged()
                }
            } ?: run {
                baseBottomFragmentModel?.let {
                    mBottomFragmentList.add(it)
                }
            }
        }

        @SuppressLint("NotifyDataSetChanged")
        fun removeFragment(fragmentId: Int) {
            val posToRemove = getFragmentPosition(fragmentId)
            mBottomFragmentList.removeAt(posToRemove)
            positionChanged = posToRemove
            notifyDataSetChanged()
        }

        fun getFragmentPosition(id: Int): Int {
            return mBottomFragmentList.indexOf(mBottomFragmentList.find { it.fragmentId == id })
        }
    }

    private fun showSelectWorkshopDialogActual(params: SelectWorkshopDialogParams) {
        val bundle = Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE)
            if (params.isStart == true) putBoolean(AppENUM.IS_START, true)
        }
        isHomeWorkshopSelect = params.isHome ?: true

        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_WORKSHOP_LIST,
            bundle,
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    if ((extra as? String) == "add_workshop") {
                        CommonUtils.addFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.ADD_WORKSHOP, null),
                            target = R.id.fragment_container_2)
                    } else {
                        val data =
                            extra as WorkshopListResponseModel //                        tvWorkshopName.text = data.name
                        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME,
                            data.name.toString())
                        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_ADDRESS,
                            data.address.toString())
                        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                            data.workshopId.toString())

                        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                            data.types ?: AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)

                        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.OWNER_NAME,
                            data.ownerName.toString())
                        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.OWNER_ID,
                            data.ownerId.toString())
                        viewModel.putStringSharedPreference(AppENUM.USER_ROLE,
                            data.workshopRole ?: "")
                        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_GSTIN,
                            data.gstin.toString()) //                        (requireActivity().application as? App)?.reStartModule()
                        viewModel.putBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                            data.membershipStatus ?: false)
                        sparesInternationalTabChanges()
                        setupViewPager()
                        showLoader()
                        bottomNavViewModel.getConfigAPI()
                        isFirebaseRewardToastEnable = false
                        database = Firebase.database(BuildConfig.FirebaseDatabaseUrl).reference
                        firebaseForRewards()
                        EventBus.getDefault().post(WorkshopUpdateEvent())
                        EventBus.getDefault().post(RackUpdateEvent())
                    }
                }
            })?.let { dialog ->
            dialog.show(requireActivity().supportFragmentManager, dialog.javaClass.name)
        }
    }

    private fun checkPageNameAndRedirect(pageName: MutableList<String>) {
        CommonUtils.hideKeyboard(requireActivity())
        if (pageName.isNotEmpty()) {
            when (pageName[0]) {
                AppENUM.ConfigConstants.DEEPLINK_INVENTORY -> {
                    if (isInternationalUser) {
                        selectTab(SelectTabParams(FragmentFactory.Fragments.INVENTORY_TABS_INTERNATIONAL,
                            true))
                    } else selectTab(SelectTabParams(FragmentFactory.Fragments.INVENTORY_TABS,
                        true))
                }
                AppENUM.ConfigConstants.DEEPLINK_BILLING -> {
                    if (isInternationalUser) {
                        selectTab(SelectTabParams(FragmentFactory.Fragments.BILLING_TABS, true))
                    } else {
                        CommonUtils.addFragmentUtil(
                            requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.BILLING, null),
                        )
                    }
                }
                AppENUM.ConfigConstants.DEEPLINK_ACCOUNT -> {
                    selectTab(SelectTabParams(FragmentFactory.Fragments.ACCOUNT_TABS, true))
                }
                AppENUM.ConfigConstants.DEEPLINK_ONGOING_ORDERS -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.ORDERS_LIST,
                            Bundle().apply { putInt(AppENUM.ORDER_TYPE, 0) }),
                    )
                }
                AppENUM.ConfigConstants.DEEPLINK_GO_COINS_WALLET -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, null),
                    )
                }
                AppENUM.ConfigConstants.DEEPLINK_QR -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.QR_CODE, null),
                    )
                }
                AppENUM.ConfigConstants.DEEPLINK_REMINDERS -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.REMINDERS, null),
                    )
                }
                AppENUM.ConfigConstants.DEEPLINK_LEDGER -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.LEDGER, null),
                    )
                }
                AppENUM.ConfigConstants.DEEPLINK_MY_CUSTOMERS -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.MY_CUSTOMER, null),
                    )
                }
                AppENUM.ConfigConstants.DEEPLINK_MANAGE_EMPLOYEE -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.MANAGE_EMPLOYEE, null),
                    )
                }
                AppENUM.ConfigConstants.DEEPLINK_MANAGE_PACKAGE -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.MANAGE_PACKAGES, null),
                    )
                }
                AppENUM.ConfigConstants.DEEPLINK_ORDER_HISTORY -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_HISTORY, null),
                    )
                }
                AppENUM.ConfigConstants.DEEPLINK_GMB, AppENUM.ConfigConstants.DEEPLINK_GMWEB, AppENUM.ConfigConstants.DEEPLINK_GMB_COMBO -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.GMB_INFO, null),
                    )
                }
                AppENUM.ConfigConstants.DEEPLINK_HOME -> { // do nothing
                }
                AppENUM.ConfigConstants.DEEPLINK_PROFILE -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.PROFILE, null),
                    )
                }
                AppENUM.ConfigConstants.DEEPLINK_REFERRAL, AppENUM.ConfigConstants.DEEPLINK_REFER -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.REFER_AND_EARN, null),
                    )
                }
                AppENUM.ConfigConstants.DEEPLINK_INSTANT_REMINDER -> {
                    viewModel.putBooleanSharedPreference(AppENUM.DEEPLINK_INSTANT_REMINDER, true)
                    selectTab(SelectTabParams(FragmentFactory.Fragments.ACCOUNT_TABS, true))
                }
                AppENUM.ConfigConstants.DEEPLINK_BIDING_HOME -> {
                    when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                        AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
                        AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                            CommonUtils.addFragmentUtil(requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_BIDDING_ADD_PART_FRAGMENT,
                                    null))
                        }
                        else -> {
                            CommonUtils.addFragmentUtil(requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.ENQUIRY_ORDER_REQUESTS,
                                    Bundle().apply {
                                        putInt(AppENUM.POSITION,
                                            0) //                                    putBoolean(AppENUM.IS_COMBO_PURCHASED, isComboPurchased)
                                    }))
                        }
                    }
                }
                AppENUM.ConfigConstants.DEEPLINK_BIDING_LIST -> {
                    when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                        AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
                        AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                            CommonUtils.addFragmentUtil(requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_BIDDING_LIST_FRAGMENT,
                                    null))
                        }
                        else -> {
                            CommonUtils.addFragmentUtil(requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.ENQUIRY_ORDER_REQUESTS,
                                    Bundle().apply {
                                        putInt(AppENUM.POSITION,
                                            0) //                                    putBoolean(AppENUM.IS_COMBO_PURCHASED, isComboPurchased)
                                    }))
                        }
                    }
                }
                AppENUM.ConfigConstants.DEEPLINK_CREATE_ORDER -> {
                    when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                        "")) {
                        AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                            CommonUtils.addFragmentUtil(
                                requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_NEW_ORDER,
                                    Bundle().apply {
                                        putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                                    }),
                            )
                        }
                        else -> {
                            CommonUtils.addFragmentUtil(
                                requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                                    Bundle().apply {
                                        putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                                    }),
                            )
                        }
                    }
                }
                AppENUM.ConfigConstants.DEEPLINK_SS_MARKETPLACE -> {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_SHOP, null))
                }
                AppENUM.ConfigConstants.DEEPLINK_SPARES_CART -> {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SS_CART, null))
                }
                AppENUM.ConfigConstants.DEEPLINK_SS_BRAND_LIST -> {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                            Bundle().apply {
                                putString(AppENUM.IntentKeysENUM.TYPE,
                                    AppENUM.RefactoredStrings.CATEGORY)
                                putBoolean(AppENUM.IS_SEE_ALL, true)
                            }))
                }
                AppENUM.ConfigConstants.DEEPLINK_SS_CATEGORY_LIST -> {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                            Bundle().apply {
                                putString(AppENUM.IntentKeysENUM.TYPE,
                                    AppENUM.RefactoredStrings.CATEGORY)
                                putBoolean(AppENUM.IS_SEE_ALL, true)
                            }))
                }
                AppENUM.ConfigConstants.DEEPLINK_SS_ORDER_HISTORY -> {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_ORDER_HISTORY,
                            null))
                }
                AppENUM.ConfigConstants.DEEPLINK_BULK_INVENTORY -> {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.INVENTORY_BULK_UPLOAD,
                            null))
                }
                AppENUM.ConfigConstants.DEEPLINK_BUSINESS_CARDS -> {
                    fireInitBusinessCard()
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.FRAGMENT_BUSINESS_CARD,
                            null))
                }
                AppENUM.ConfigConstants.DEEPLINK_SS_BRAND -> {
                    popPLPIfOnTop()
                    lifecycleScope.launch {
                        waitForFiltersToGetLoaded()
                        if (pageName.size > 1) {
                            CommonUtils.addFragmentUtil(requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                                    Bundle().apply {
                                        putString(AppENUM.IntentKeysENUM.TYPE,
                                            AppENUM.RefactoredStrings.BRAND)
                                        putString(AppENUM.IntentKeysENUM.BRAND_NAME, pageName[1])
                                    }))
                        }
                    }
                }
                AppENUM.ConfigConstants.DEEPLINK_SS_PRODUCT_DETAIL -> {
                    if (pageName.size > 1) {
                        CommonUtils.addFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_PRODUCT_DETAILS,
                                Bundle().apply {
                                    putString(AppENUM.SS_PRODUCT_ID, pageName[1])
                                }))
                    }
                }

                AppENUM.ConfigConstants.DEEPLINK_SS_CATEGORY -> {
                    lifecycleScope.launch {
                        waitForFiltersToGetLoaded()
                        if (pageName.size > 1) {
                            if (pageName[1] == "all") {
                                CommonUtils.addFragmentUtil(requireActivity(),
                                    FragmentFactory.fragment(FragmentFactory.Fragments.SELECT_CATEGORY_OR_BRAND,
                                        Bundle().apply {
                                            putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                                            putInt(AppENUM.POSITION, 0)
                                        }))
                            } else {
                                CommonUtils.addFragmentUtil(requireActivity(),
                                    FragmentFactory.fragment(FragmentFactory.Fragments.SELECT_CATEGORY_OR_BRAND,
                                        Bundle().apply { //                                putString(AppENUM.IntentKeysENUM.TYPE,
                                            //                                    AppENUM.RefactoredStrings.CATEGORY)
                                            putParcelable(AppENUM.CATEGORY_DATA,
                                                SSCategoryResponseModel().apply {
                                                    categoriesName = pageName[1]
                                                })
                                        }))
                            }
                        }
                    }
                }

                AppENUM.ConfigConstants.DEEPLINK_AS_CATEGORY -> {
                    popPLPIfOnTop()
                    lifecycleScope.launch {
                        waitForFiltersToGetLoaded()
                        if (pageName.size > 1) {
                            if (pageName[1] == "all") {
                                CommonUtils.addFragmentUtil(requireActivity(),
                                    FragmentFactory.fragment(FragmentFactory.Fragments.SELECT_CATEGORY_OR_BRAND,
                                        Bundle().apply {
                                            putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                                            putInt(AppENUM.POSITION, -1)
                                        }))
                            } else {
                                CommonUtils.addFragmentUtil(requireActivity(),
                                    FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                                        Bundle().apply {
                                            putString(AppENUM.IntentKeysENUM.CATEGORY_NAME,
                                                pageName[1])
                                        }))
                            }
                        }
                    }
                }

                AppENUM.ConfigConstants.DEEPLINK_ALL_ACC -> {
                    popPLPIfOnTop()
                    lifecycleScope.launch {
                        waitForFiltersToGetLoaded()
                        sparesShopFragmentViewModel.setBrandsSelected(false)
                        CommonUtils.addFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER))
                    }
                }

                AppENUM.ConfigConstants.DEEPLINK_ALL_SPARES -> {
                    popPLPIfOnTop()
                    lifecycleScope.launch {
                        waitForFiltersToGetLoaded()
                        sparesShopFragmentViewModel.setBrandsSelected(true)
                        CommonUtils.addFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER))
                    }
                }

                AppENUM.ConfigConstants.DEEPLINK_BUSINESS_CARD -> {
                    lifecycleScope.launch {
                        CommonUtils.addFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.FRAGMENT_BUSINESS_CARD))
                    }
                }

                AppENUM.ConfigConstants.DEEPLINK_MEMBERSHIP -> {
                    lifecycleScope.launch {
                        CommonUtils.addFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.SUBSCRIPTION_INFO_FRAGMENT))
                    }
                }

            }
        }
    }

    private fun popPLPIfOnTop() {
        if (parentFragmentManager.fragments.asReversed().size > 0 && (parentFragmentManager.fragments.asReversed()[0] as? ShopWithFilterFragment) != null) {
            popBackStackAndHideKeyboard()
        }
    }

    private fun firebaseForRewards() {
        if (newDb != null && postListener != null) {
            postListener?.let {
                newDb?.removeEventListener(it)
            }
        }
        newDb = database?.child(BuildConfig.Enviroment)?.child(AppENUM.SaaSGarage)
            ?.child(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID))
        postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (isFirebaseRewardToastEnable && dataSnapshot.child("type").value != null) {
                    val isCredit = when (dataSnapshot.child("type").value.toString()) {
                        AppENUM.DEBIT -> {
                            false
                        }
                        AppENUM.CREDIT -> {
                            true
                        }
                        else -> {
                            false
                        }
                    }
                    if (dataSnapshot.child("text").value != null) {
                        CommonUtils.showRewardToast(requireActivity(),
                            dataSnapshot.child("text").value.toString(),
                            isCredit)
                    }
                }

                isFirebaseRewardToastEnable = true
            }

            override fun onCancelled(databaseError: DatabaseError) {
                databaseError.message
            }
        }
        postListener?.let {
            newDb?.addValueEventListener(it)
        }
    }

    private fun setSoftUpdateUI() {
        if (viewModel.getBooleanSharedPreference(AppENUM.IS_SOFT_UPDATE_AVAILABLE)) {
            binding.rlUpdateView.visibility = View.VISIBLE
        } else {
            binding.rlUpdateView.visibility = View.GONE
        }
    }

    private fun observeConfigAPI() {
        bottomNavViewModel.provideConfigResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> { //                    configResponse = it.body.data
                    viewModel.screensConfigResponse.value = (it.body.data)
                    viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.CONFIG_API_RESPONSE,
                        Gson().toJson(it.body.data))

                    updateTabs() //                    updateBMB(it.body.data.homePopup ?: listOf())
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    //    private fun updateBMB(homePopupList: List<HomePopupItem>) {
    //        if (!isHomePopupSet) {
    //            if (homePopupList.isNotEmpty()) {
    //                isHomePopupSet = true
    //
    //                binding.imgCreateNew.onBoomListener = object : OnBoomListener {
    //                    override fun onClicked(index: Int, boomButton: BoomButton?) {
    //                        val intent = Intent(requireContext(), DashboardActivity::class.java)
    //                        intent.data = Uri.parse(homePopupList[index].deeplink)
    //                        startActivity(intent)
    //                    }
    //
    //                    override fun onBackgroundClick() {
    //
    //                    }
    //
    //                    override fun onBoomWillHide() {
    //                        binding.imgOptions.animate().rotation(0f).setDuration(500).start()
    //                    }
    //
    //                    override fun onBoomDidHide() {
    //
    //                    }
    //
    //                    override fun onBoomWillShow() {
    //                        binding.imgOptions.animate().rotation(-135f).setDuration(500).start()
    //                    }
    //
    //                    override fun onBoomDidShow() {
    //
    //                    }
    //                }
    //
    //                lifecycleScope.launchWhenResumed {
    //                    loadBMBBuilder(homePopupList)
    //
    //                    when (homePopupList.size) {
    //                        3 -> {
    //                            binding.imgCreateNew.piecePlaceEnum = PiecePlaceEnum.HAM_3
    //                            binding.imgCreateNew.buttonPlaceEnum = ButtonPlaceEnum.HAM_3
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    private suspend fun loadBMBBuilder(homePopupList: List<HomePopupItem>) {
    //        homePopupList.forEach {
    //            val drawable = bottomNavViewModel.getDrawableFromUrl(requireContext(), it.icon)
    //            drawable?.let { drawableValue ->
    //                binding.imgCreateNew.addBuilder(HamButton.Builder().normalText(it.heading)
    //                    .typeface(CommonUtils.getCustomFontTypeface(requireContext(),
    //                        R.font.gilroy_semibold)).textSize(14).subNormalText(it.subHeading)
    //                    .subTextSize(12).subMaxLines(2).buttonCornerRadius(10)
    //                    .normalImageDrawable(drawableValue).normalColor(Color.parseColor(it.bgColor))
    //                    .imagePadding(Rect(20, 20, 20, 20)).highlightedColor(ContextCompat.getColor(
    //                        requireContext(),
    //                        R.color.black_color_new)).subTypeface(CommonUtils.getCustomFontTypeface(
    //                        requireContext(),
    //                        R.font.gilroy_medium)).subTextSize(12).subMaxLines(2))
    //            }
    //        }
    //    }

    private fun updateTabs() {
        kotlin.run breaking@{
            for (fragment in requireActivity().supportFragmentManager.fragments.asReversed()) {
                (fragment as? HomeFragment)?.let {
                    if (it.isAdded) fragment.updateScreen()
                    if (walkThroughInProgress) return@breaking
                }
                (fragment as? AccountFragment)?.let {
//                    fragment.updateScreen()
                    if (it.isAdded) fragment.updateScreen()
                }
            }
        }
    }

    private suspend fun waitForFiltersToGetLoaded() {
        showLoader()
        sparesShopFragmentViewModel.getSearchAndFilterAttributes()
        hideLoader()
    }

    private fun setupViewPager() {
        if (!isAdapterSet) {
            isAdapterSet = true
            binding.viewPager.layoutTransition?.setAnimateParentHierarchy(false)
            binding.viewPager.isUserInputEnabled = false
            pagerAdapter = ViewPagerAdapter(requireActivity())

            if (isInternationalUser) {
                pagerAdapter?.let { pAdapter ->
                    bottomFragmentTreeMap = TreeMap()
                    getFragmentTab(FragmentFactory.Fragments.HOME_TABS, requireContext())?.let {
                        bottomFragmentTreeMap?.put(0, it)
                    }
                    getFragmentTab(FragmentFactory.Fragments.INVENTORY_TABS_INTERNATIONAL,
                        requireContext())?.let {
                        bottomFragmentTreeMap?.put(1, it)
                    }
                    getFragmentTab(FragmentFactory.Fragments.BILLING_TABS, requireContext())?.let {
                        bottomFragmentTreeMap?.put(2, it)
                    }
                    getFragmentTab(FragmentFactory.Fragments.ACCOUNT_TABS, requireContext())?.let {
                        bottomFragmentTreeMap?.put(3, it)
                    }
                    bottomFragmentTreeMap?.values?.forEach { fragmentModel ->
                        pAdapter.addFragment(baseBottomFragmentModel = fragmentModel)
                    }
                    binding.viewPager.adapter = pAdapter
                }
            } else {
                pagerAdapter?.let { pAdapter ->
                    bottomFragmentTreeMap = TreeMap()
                    getFragmentTab(FragmentFactory.Fragments.HOME_TABS, requireContext())?.let {
                        bottomFragmentTreeMap?.put(0, it)
                    }
                    getFragmentTab(FragmentFactory.Fragments.SPARES_TABS, requireContext())?.let {
                        bottomFragmentTreeMap?.put(1, it)
                    }
                    getFragmentTab(FragmentFactory.Fragments.INVENTORY_TABS,
                        requireContext())?.let {
                        bottomFragmentTreeMap?.put(2, it)
                    }
                    getFragmentTab(FragmentFactory.Fragments.ACCOUNT_TABS, requireContext())?.let {
                        bottomFragmentTreeMap?.put(3, it)
                    }
                    bottomFragmentTreeMap?.values?.forEach { fragmentModel ->
                        pAdapter.addFragment(baseBottomFragmentModel = fragmentModel)
                    }
                    binding.viewPager.adapter = pAdapter
                }
            }
            binding.viewPager.offscreenPageLimit = pagerAdapter?.itemCount ?: 0
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnDownload -> inAppUpdate() //openPlayStore()
            R.id.rlTab1 -> {
                selectTab(SelectTabParams(FragmentFactory.Fragments.HOME_TABS, true))
            }
            R.id.rlTab2 -> {
                if (isInternationalUser) {
                    selectTab(SelectTabParams(FragmentFactory.Fragments.INVENTORY_TABS_INTERNATIONAL,
                        true))
                } else selectTab(SelectTabParams(FragmentFactory.Fragments.SPARES_TABS, true))
            }
            R.id.rlTab3 -> {
                if (isInternationalUser) {
                    selectTab(SelectTabParams(FragmentFactory.Fragments.BILLING_TABS, true))
                } else selectTab(SelectTabParams(FragmentFactory.Fragments.INVENTORY_TABS, true))
            }
            R.id.rlTab4 -> {
                selectTab(SelectTabParams(FragmentFactory.Fragments.ACCOUNT_TABS, true))
            }
            R.id.imgCreateNew -> {
                for (fragment in requireActivity().supportFragmentManager.fragments) {
                    (fragment as? HomeFragment)?.let {
                        fragment.createNewOrder()
                        return
                    }
                }
            }
            R.id.imgOptions -> {
                for (fragment in requireActivity().supportFragmentManager.fragments) {
                    (fragment as? HomeFragment)?.let {
                        fragment.createNewOrder()
                        return
                    }
                } //                if (binding.imgCreateNew.isBoomed) {
                //                    binding.imgCreateNew.reboom()
                //                } else {
                //                    binding.imgCreateNew.boom()
                //                }
            }
            R.id.imgCancelUpdate -> {
                binding.rlUpdateView.makeVisible(false)
            }
        }
    }

    private fun inAppUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(requireContext())
        appUpdateManager?.appUpdateInfo?.addOnSuccessListener {
            it?.let { it1 ->
                appUpdateManager?.startUpdateFlowForResult(it1,
                    AppUpdateType.IMMEDIATE,
                    requireActivity(),
                    myRequestCode)
            }
        }
        installStateUpdatedListener = InstallStateUpdatedListener { installState ->
            if (installState.installStatus() == InstallStatus.DOWNLOADED) completeUpdateAndUnregister()
        }.also { appUpdateManager?.registerListener(it) }
    }

    private fun completeUpdateAndUnregister() {
        appUpdateManager?.completeUpdate()
        CommonUtils.showToast(requireContext(), resources.getString(R.string.update_sucessfull))
        installStateUpdatedListener?.let {
            appUpdateManager?.unregisterListener(it)
        }
    }


    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is NavigateToInventoryEventModel -> {
                if (isInternationalUser) {
                    selectTab(SelectTabParams(FragmentFactory.Fragments.INVENTORY_TABS_INTERNATIONAL,
                        true))
                } else selectTab(SelectTabParams(FragmentFactory.Fragments.INVENTORY_TABS, true))
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        postListener?.let {
            newDb?.removeEventListener(it)
        }
    }

    private fun onBackPressed() {
        if (requireActivity().supportFragmentManager.fragments.size <= 6) {
            when (binding.viewPager.currentItem) {
                1, 2, 3 -> {
                    binding.rlTab1.callOnClick()
                }
                else -> {
                    (requireActivity() as? DashboardActivity)?.backPressEvent()
                }
            }
        } else {
            if (!walkThroughInProgress) (requireActivity() as? DashboardActivity)?.backPressEvent()
        }
    }

    private fun selectTab(params: SelectTabParams) {
        binding.tvTab1Name.isEnabled = false
        binding.tvTab2Name.isEnabled = false
        binding.tvTab3Name.isEnabled = false
        binding.tvTab4Name.isEnabled = false
        binding.viewIndicatorTab1.makeVisible(false)
        binding.viewIndicatorTab2.makeVisible(false)
        binding.viewIndicatorTab3.makeVisible(false)
        binding.viewIndicatorTab4.makeVisible(false)
        when (params.position) {
            FragmentFactory.Fragments.HOME_TABS -> {
                binding.tvTab1Name.isEnabled = true
                binding.viewIndicatorTab1.makeVisible(true)
                if (params.navigate == true) {
                    binding.viewPager.setCurrentItem(pagerAdapter?.getFragmentPosition(
                        FragmentFactory.Fragments.HOME_TABS) ?: 0, true)
                    tabFirebaseEvents(FirebaseAnalyticsLog.FirebaseEventNameENUM.TAB_HOME)
                }
            }
            FragmentFactory.Fragments.SPARES_TABS -> {
                binding.tvTab2Name.isEnabled = true
                binding.viewIndicatorTab2.makeVisible(true)
                if (params.navigate == true) {
                    binding.viewPager.setCurrentItem(pagerAdapter?.getFragmentPosition(
                        FragmentFactory.Fragments.SPARES_TABS) ?: 0, true)
                    try {
                        val baseFragment =
                            pagerAdapter?.mBottomFragmentList?.get(pagerAdapter?.getFragmentPosition(
                                FragmentFactory.Fragments.SPARES_TABS) ?: 0)?.fragment
                        if (baseFragment is SparesShopFragment) {
                            baseFragment.apiCall()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    tabFirebaseEvents(FirebaseAnalyticsLog.FirebaseEventNameENUM.TAB_SPARES)
                }
            }
            FragmentFactory.Fragments.INVENTORY_TABS -> {
                binding.tvTab3Name.isEnabled = true
                binding.viewIndicatorTab3.makeVisible(true)
                if (params.navigate == true) {
                    binding.viewPager.setCurrentItem(pagerAdapter?.getFragmentPosition(
                        FragmentFactory.Fragments.INVENTORY_TABS) ?: 0, true)
                    try {
                        val baseFragment =
                            pagerAdapter?.mBottomFragmentList?.get(pagerAdapter?.getFragmentPosition(
                                FragmentFactory.Fragments.INVENTORY_TABS) ?: 0)?.fragment
                        if (baseFragment is InventoryFragment) {
                            baseFragment.apiCall()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    tabFirebaseEvents(FirebaseAnalyticsLog.FirebaseEventNameENUM.TAB_INVENTORY)
                }
            }
            FragmentFactory.Fragments.INVENTORY_TABS_INTERNATIONAL -> {
                binding.tvTab2Name.isEnabled = true
                binding.viewIndicatorTab2.makeVisible(true)
                if (params.navigate == true) {
                    binding.viewPager.setCurrentItem(pagerAdapter?.getFragmentPosition(
                        FragmentFactory.Fragments.INVENTORY_TABS_INTERNATIONAL) ?: 0, true)
                    try {
                        val baseFragment =
                            pagerAdapter?.mBottomFragmentList?.get(pagerAdapter?.getFragmentPosition(
                                FragmentFactory.Fragments.INVENTORY_TABS_INTERNATIONAL)
                                ?: 0)?.fragment
                        if (baseFragment is InventoryFragment) {
                            baseFragment.apiCall()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    tabFirebaseEvents(FirebaseAnalyticsLog.FirebaseEventNameENUM.TAB_INVENTORY)
                }
            }
            FragmentFactory.Fragments.BILLING_TABS -> {
                binding.tvTab3Name.isEnabled = true
                binding.viewIndicatorTab3.makeVisible(true)
                if (params.navigate == true) {
                    binding.viewPager.setCurrentItem(pagerAdapter?.getFragmentPosition(
                        FragmentFactory.Fragments.BILLING_TABS) ?: 0, true)
                    try {
                        val baseFragment =
                            pagerAdapter?.mBottomFragmentList?.get(pagerAdapter?.getFragmentPosition(
                                FragmentFactory.Fragments.BILLING_TABS) ?: 0)?.fragment
                        if (baseFragment is BillingFragment) {
                            baseFragment.apiCall()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    tabFirebaseEvents(FirebaseAnalyticsLog.FirebaseEventNameENUM.TAB_BILLING)
                }
            }
            FragmentFactory.Fragments.ACCOUNT_TABS -> {
                binding.tvTab4Name.isEnabled = true
                binding.viewIndicatorTab4.makeVisible(true)
                if (params.navigate == true) {
                    binding.viewPager.setCurrentItem(pagerAdapter?.getFragmentPosition(
                        FragmentFactory.Fragments.ACCOUNT_TABS) ?: 0, true)
                    try {
                        val baseFragment =
                            pagerAdapter?.mBottomFragmentList?.get(pagerAdapter?.getFragmentPosition(
                                FragmentFactory.Fragments.ACCOUNT_TABS) ?: 0)?.fragment
                        if (baseFragment is AccountFragment) {
                            baseFragment.apiCall()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    tabFirebaseEvents(FirebaseAnalyticsLog.FirebaseEventNameENUM.TAB_ACCOUNT)
                }
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        try {
            val gcd = Geocoder(requireContext(), Locale.getDefault())
            val addresses: List<Address>? =
                gcd.getFromLocation(location.latitude, location.longitude, 1)
            if (addresses?.isNotEmpty() == true) {
                addresses[0].locality //city
                addresses[0].adminArea //state
                addresses[0].countryName //country
                UpdateLocationRequest().apply {
                    latitude = location.latitude
                    longitude = location.longitude
                    city = addresses[0].locality
                    state = addresses[0].adminArea
                    country = addresses[0].countryName
                    bottomNavViewModel.sendLocationToServer(this)
                }
            } else { // not found
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun fireInitBusinessCard() {
        val bundle = Bundle()
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_BCARD_PRO,
            bundle)
    }


}