package whitelisted.garage.view.fragments.orderInclusions

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.data.SetOptionsImgStatusObject
import whitelisted.garage.api.request.OrderInclusionsItem
import whitelisted.garage.api.request.OrderInclusionsRequestResponse
import whitelisted.garage.api.request.RackItem
import whitelisted.garage.api.request.UpdateInventoryItemRequest
import whitelisted.garage.api.response.MostUsedPartsResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentRetailOrderInclusionsBinding
import whitelisted.garage.eventBus.*
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.AddedInclusionsAdapter
import whitelisted.garage.view.adapter.MostUsedPartsAdapter
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment
import whitelisted.garage.viewmodels.OrderInclusionsViewModel
import whitelisted.garage.viewmodels.PaymentsSharedViewModel

class OrderInclusionsFragment : BaseFragment() {

    private val binding by viewBinding(FragmentRetailOrderInclusionsBinding::inflate)
    private var totalBillAmount: Double = 0.0
    private val viewModel: OrderInclusionsViewModel by viewModel()
    private val paymentViewModel: PaymentsSharedViewModel by sharedViewModel()

    private lateinit var addedPartListDuplicate: MutableList<OrderInclusionsItem>
    private lateinit var inventoryItemsList: MutableList<RackItem>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        setClickListeners()
        setInclusionsAdaper()
        setObservers()
        updateScreen()

        showLoader()
        viewModel.getOrderInclusionsAPI()

        binding.lblMostUsedParts.text = getString(R.string.most_used_spares)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setObservers() {
        viewModel.provideUpdateOrderInclusionsResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    EventBus.getDefault().post(SelectTabEvent(
                        "2",
                    ))

                    CommonUtils.showToast(activity, it.body.message, true)

                    OrderDetailWrapperFragment.orderStatusId =
                        AppENUM.RefactoredStrings.WIP_ORDER_STATUS

                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                    EventBus.getDefault().post(GetOrderDetailsEvent())
                    paymentViewModel.setOrderInclusionItems((binding.rvInclusions.adapter as? AddedInclusionsAdapter)?.dataList
                        ?: mutableListOf())
                    EventBus.getDefault().post(RetailerInclusionsAddedEvent())
                    if (::addedPartListDuplicate.isInitialized && addedPartListDuplicate.size > 0) {
                        updateAddedItemsValue()
                    }
                }
                is Result.Failure -> CommonUtils.showToast(activity, it.errorMessage, true)

            }
        }

        viewModel.provideGetOrderInclusionsResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        paymentViewModel.setOrderInclusionItems(it.body.data[0].services
                            ?: mutableListOf())
                        if (isAdded) {
                            if (it.body.data[0].services?.isNotEmpty() == true) binding.apply {
                                llPartsInventory.visibility = View.VISIBLE
                                rlTotal.visibility = View.VISIBLE
                                (rvInclusions.adapter as AddedInclusionsAdapter).dataList =
                                    it.body.data[0].services?.toMutableList() ?: mutableListOf()
                                (rvInclusions.adapter as? AddedInclusionsAdapter)?.notifyDataSetChanged()
                                calculateTotals()
                            }
                            if (it.body.data[0].pdfStatus?.status == true) {
                                EventBus.getDefault().post(SetOptionsImageStatusEvent("1",
                                    SetOptionsImgStatusObject(tabPosition = 1,
                                        pdfFile = it.body.data[0].pdfStatus?.pdfFile ?: "")))
                            } else {
                                EventBus.getDefault().post(SetOptionsImageStatusEvent("1",
                                    SetOptionsImgStatusObject(tabPosition = 1)))

                            }
                        }
                    } else {
                        showLoader()
                        viewModel.getMostUsedParts(false)
                        EventBus.getDefault().post(SetOptionsImageStatusEvent("1",
                            SetOptionsImgStatusObject(tabPosition = 1)))
                    }
                }
                is Result.Failure -> {

                }
            }
        }

        viewModel.provideMostUsedPartsResponse()
            .observe(viewLifecycleOwner) { mostUsedPartsResult ->
                hideLoader()
                when (mostUsedPartsResult) {
                    is Result.Success -> {
                        if (mostUsedPartsResult.body.data.isNotEmpty()) {
                            if ((binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList.size == 0) {
                                binding.rlMostUsedParts.visibility = View.VISIBLE
                                setMostUsedPartsAdapter(mostUsedPartsResult.body.data)
                            }
                        }
                    }
                    is Result.Failure -> { //                        CommonUtils.showToast(
                        //                            activity,
                        //                            mostUsedPartsResult.errorMessage,
                        //                            true
                        //                        )
                    }
                }
            }

        viewModel.provideUpdateInventoryPriceResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    if (::addedPartListDuplicate.isInitialized) {
                        addedPartListDuplicate.removeAt(0)
                        if (addedPartListDuplicate.size > 0) {
                            updateAddedItemsValue()
                        } else hideLoader()
                    } else hideLoader()
                }
                is Result.Failure -> {
                    hideLoader()
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

    }

    private fun setMostUsedPartsAdapter(data: MutableList<MostUsedPartsResponseModel>) {
        binding.rvMostUsedParts.layoutManager = LinearLayoutManager(requireContext())
        binding.rvMostUsedParts.adapter = MostUsedPartsAdapter(true, this)
        (binding.rvMostUsedParts.adapter as MostUsedPartsAdapter).setData(data)
    }

    private fun updateAddedItemsValue() {
        if (::addedPartListDuplicate.isInitialized && addedPartListDuplicate.size > 0) {
            showLoader()
            val req = UpdateInventoryItemRequest()
            req.selling_price = addedPartListDuplicate[0].pricePerQuantity
            req.buying_price = 0.0
            req.sku_id = addedPartListDuplicate[0].skuId
            viewModel.updatePriceForAddedParts(req)
        }
    }

    private fun setInclusionsAdaper() {
        binding.rvInclusions.layoutManager = LinearLayoutManager(requireContext())
        binding.rvInclusions.adapter = AddedInclusionsAdapter(viewModel.getStringSharedPreference(
            AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            getString(R.string.currency_symbol_default)), mutableListOf(), this)
    }

    private fun setClickListeners() {
        binding.btnAddParts.setOnClickListener(this)
        binding.btnAddInventory.setOnClickListener(this)
        binding.btnConfirmOrder.setOnClickListener(this)
        binding.btnAddSelectedParts.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnAddParts -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_ADD_PART,
                        Bundle().apply { putBoolean(AppENUM.IS_INVENTORY, false) }),
                    target = R.id.fragment_container_2)
            }

            R.id.btnAddInventory -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_ADD_PART,
                        Bundle().apply {
                            putBoolean(AppENUM.IS_INVENTORY, true)
                            putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                        }),
                    target = R.id.fragment_container_2)
            }

            R.id.btnConfirmOrder -> {
                if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_ADDRESS, "")
                        .isEmpty()) {
                    activity?.let {
                        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.COMPLETE_REGISTRATION,
                            null,
                            object : ActionListener {
                                @RequiresApi(Build.VERSION_CODES.P)
                                override fun onActionItem(extra: Any?, extra2: Any?) {
                                    if (extra as Boolean? == true) {
                                        updateOrderInclusions()
                                    }
                                }
                            })?.let { baseFragment ->
                            baseFragment.show(it.supportFragmentManager,
                                baseFragment.javaClass.name)
                        }
                    }
                } else updateOrderInclusions()
            }

            R.id.btnAddSelectedParts -> {
                checkAndAddSelectedParts(true)
            }
            R.id.imgEdit -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.model))?.let { position ->
                    activity?.let {
                        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.ORDER_ESTIMATES_EDIT,
                            Bundle().apply {
                                putParcelable(AppENUM.ORDER_MODEL,
                                    (binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList[position])
                            },
                            object : ActionListener {
                                @RequiresApi(Build.VERSION_CODES.P)
                                override fun onActionItem(extra: Any?, extra2: Any?) {
                                    (binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList[position] =
                                        extra as OrderInclusionsItem
                                    (binding.rvInclusions.adapter as AddedInclusionsAdapter).notifyItemChanged(
                                        position)
                                    calculateTotals()
                                }
                            })?.let { baseFragment ->
                            baseFragment.show(it.supportFragmentManager,
                                baseFragment.javaClass.name)
                        }
                    }
                }
            }
            R.id.imgDel -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.model))?.let { position ->
                    (binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList.removeAt(
                        position)
                    (binding.rvInclusions.adapter as AddedInclusionsAdapter).notifyItemRemoved(
                        position)
                    if ((binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList.size == 0) {
                        binding.llPartsInventory.visibility = View.GONE
                        binding.rlTotal.visibility = View.GONE
                        if (binding.rvMostUsedParts.adapter != null) {
                            binding.rlMostUsedParts.visibility = View.VISIBLE
                            (binding.rvMostUsedParts.adapter as MostUsedPartsAdapter).apply {
                                this.notifyItemRangeChanged(0, this.getAdapterData().size)
                            }
                        } else {
                            showLoader()
                            viewModel.getMostUsedParts(false)
                        }
                    } else calculateTotals()
                }
            }
            R.id.rlPlusMinus -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.model))?.let {
                    calculateTotals()
                }
            }


        }
    }

    private fun checkAndAddSelectedParts(isButtonClicked: Boolean) {
        var flag = false
        if (binding.rvMostUsedParts.adapter != null) {
            (binding.rvMostUsedParts.adapter as MostUsedPartsAdapter).getAdapterData().forEach {
                if (it.isSelected) {
                    flag = true
                    val item = OrderInclusionsItem()
                    item.skuId = it.skuId
                    item.pricePerQuantity = it.sellingPrice
                    item.serviceName = it.categoryName
                    item.type = "part"
                    item.total = it.sellingPrice?.times(item.quantity ?: 0)
                    (binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList.add(item)
                }
            }
            if (flag) {
                binding.rlMostUsedParts.visibility = View.GONE
                binding.llPartsInventory.visibility = View.VISIBLE
                binding.rlTotal.visibility = View.VISIBLE
                (binding.rvInclusions.adapter as AddedInclusionsAdapter).notifyItemRangeInserted(0,
                    (binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList.size)
                calculateTotals()

                (binding.rvMostUsedParts.adapter as MostUsedPartsAdapter).getAdapterData().forEach {
                    it.isSelected = false
                }
            } else {
                if (isButtonClicked) CommonUtils.showToast(requireContext(),
                    getString(R.string.error_select_parts),
                    true)
            }
        }
    }

    private fun updateOrderInclusions() {
        var flag = false
        (binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList.forEach {
            if (it.pricePerQuantity ?: 0.0 < 1) {
                flag = true
            }
        }
        if (!flag) {
            addedPartListDuplicate =
                (binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList

            val req = OrderInclusionsRequestResponse()
            req.billAmount = totalBillAmount
            req.services = (binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList
            req.orderId = OrderDetailWrapperFragment.orderId
            showLoader()
            viewModel.updateOrderInclusionsAPI(req)
        } else {
            CommonUtils.showToast(requireContext(), getString(R.string.alert_part_inventory_price))
        }
    }

    fun updateScreen() {
        binding.tvShopName.text = OrderDetailWrapperFragment.retailWorkshopName
        showLoader()
        viewModel.getOrderInclusionsAPI()
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is AddRetailInclusionPartEventModel -> {
                binding.rlMostUsedParts.visibility = View.GONE
                if (binding.llPartsInventory.visibility == View.GONE) {
                    binding.llPartsInventory.visibility = View.VISIBLE
                    binding.rlTotal.visibility = View.VISIBLE
                }
                val selectedItem = event.extra as OrderInclusionsItem
                if (selectedItem.type == "part") {
                    if (::inventoryItemsList.isInitialized) {
                        inventoryItemsList.forEach {
                            if (it.skuId == selectedItem.skuId) {
                                selectedItem.partAlreadyAdded = true
                                return@forEach
                            }
                        }
                    }
                }
                (binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList.add(selectedItem)
                (binding.rvInclusions.adapter as AddedInclusionsAdapter).notifyItemInserted((binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList.size - 1)
                (binding.rvInclusions.adapter as AddedInclusionsAdapter).notifyItemRangeChanged(0,
                    (binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList.size)
                calculateTotals()

                checkAndAddSelectedParts(false)
            }

            is CompleteRegLocUpdateEvent -> {
                val bundle = Bundle()
                bundle.putParcelable(AppENUM.COMPLETE_REG_DATA, event.addressData)
                activity?.let {
                    FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.COMPLETE_REGISTRATION,
                        bundle,
                        object : ActionListener {
                            @RequiresApi(Build.VERSION_CODES.P)
                            override fun onActionItem(extra: Any?, extra2: Any?) {
                                if (extra as Boolean? == true) {
                                    updateOrderInclusions()
                                }
                            }
                        })?.let { baseFragment ->
                        baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
                    }
                }
            }

            is InventoryItemsEvent -> {
                inventoryItemsList = event.extra as? MutableList<RackItem> ?: mutableListOf()
            }
        }
    }

    private fun calculateTotals() {
        totalBillAmount = 0.0
        var totalItems = 0
        (binding.rvInclusions.adapter as AddedInclusionsAdapter).dataList.forEach {
            totalItems = totalItems.plus(it.quantity ?: 0)
            totalBillAmount += it.total ?: 0.0
        }
        binding.tvTotalParts.text = totalItems.toString()
        ("${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }${String.format("%.2f", totalBillAmount)}").apply { binding.tvTotalAmount.text = this }
    }


}