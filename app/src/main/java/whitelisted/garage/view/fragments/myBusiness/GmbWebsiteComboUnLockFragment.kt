package whitelisted.garage.view.fragments.myBusiness

import android.os.Bundle
import android.view.View
import whitelisted.garage.R
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentGmbUnlockBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FragmentFactory

class GmbWebsiteComboUnLockFragment : BaseFragment() {

    private val binding by viewBinding(FragmentGmbUnlockBinding::inflate)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setData()
    }

    private fun setData() {
        binding.btnVisitGooglePage.setOnClickListener(this)
        CommonUtils.showReviewDialog(requireActivity())

    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
            R.id.btnVisitGooglePage -> {
                popBackStackAndHideKeyboard()
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.EDIT_GMB_FRAGMENT, null))
            }
        }
    }

}