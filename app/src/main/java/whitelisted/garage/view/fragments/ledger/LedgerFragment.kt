package whitelisted.garage.view.fragments.ledger

import android.os.Bundle
import android.view.View
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.LedgerIdRequest
import whitelisted.garage.api.response.Ledger
import whitelisted.garage.api.response.LedgerResponseModel
import whitelisted.garage.api.response.OrderIdResponse
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentLedgerBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeBackgroundDrawable
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.LedgerAdapter
import whitelisted.garage.view.adapter.LedgerDuesAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.LedgerFragmentViewModel

class LedgerFragment : BaseFragment() {

    private val binding by viewBinding(FragmentLedgerBinding::inflate)
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private val viewModel: LedgerFragmentViewModel by viewModel()
    private lateinit var ledgerList: MutableList<Ledger>
    private lateinit var ledgerListDue: MutableList<Ledger>
    private var isDue = false
    private var selectedType = "today"
    private var isWhatsappMsg = false
    private var clickedMobile = ""

    private lateinit var filterList: List<String>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListeners()
        filterList = resources.getStringArray(R.array.ledger_filter_list).toList()

        observeData()
        showLoader()
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.RETAILER_CONSTANT) {
            binding.rlSelectType.makeVisible(true)
        }
        viewModel.getLedger(filterList[1], null)

        binding.srlFL.setOnRefreshListener {
            binding.srlFL.isRefreshing = false
            refreshScreen()
        }
    }

    private fun refreshScreen() {
        showLoader()
        if (isDue) {
            viewModel.getLedger(selectedType, AppENUM.RefactoredStrings.DUE)
        } else {
            viewModel.getLedger(selectedType, null)
        }
    }

    private fun clickListeners() {
        binding.imgBack.setOnClickListener(this)
        binding.btnGive.setOnClickListener(this)
        binding.btnTake.setOnClickListener(this)
        binding.tvYesterday.setOnClickListener(this)
        binding.tvToday.setOnClickListener(this)
        binding.tv1month.setOnClickListener(this)
        binding.tv1Week.setOnClickListener(this)
        binding.tv1Year.setOnClickListener(this)
        binding.tvDownloadSheet.setOnClickListener(this)
        binding.viewAll.setOnClickListener(this)
        binding.viewDue.setOnClickListener(this)
    }

    private fun observeData() {
        viewModel.provideLedgerResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setUpData(it.body.data)
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }

        dashboardViewModel.provideIsNewLedgerAdded().observe(viewLifecycleOwner) {
            if (it) {
                dashboardViewModel.isNewLedgerAdded.value = false
                setBacGroundAccordingToFilter(R.id.tvToday)
                showLoader()

                if (isDue) viewModel.getLedger(filterList[1], AppENUM.RefactoredStrings.DUE)
                else viewModel.getLedger(filterList[1], null)
            }
        }

        viewModel.provideDownloadCSVResponse().observe(viewLifecycleOwner) {
            CommonUtils.showToast(requireContext(), getString(R.string.downloading), true)
            CommonUtils.saveToDisk(requireActivity(), it, "${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME, "")
            }-LedgerCSV.csv", "${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME, "")
            }-LedgerCSV.csv", getString(R.string.download_ledger_csv))
            CommonUtils.showReviewDialog(requireActivity())
        }

        viewModel.provideReminderMsgResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    handleSMSOrWhatsapp(it.body.data.msg)
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }

        viewModel.provideCollectDuesResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    showLoader()
                    viewModel.getLedger(selectedType, AppENUM.RefactoredStrings.DUE)
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun handleSMSOrWhatsapp(msg: String?) {
        if (isWhatsappMsg) {
            openWhatsApp(clickedMobile, msg)
        } else {
            CommonUtils.sendSMSTEXT(requireContext(), clickedMobile, msg ?: "")
        }
    }

    private fun setUpData(data: LedgerResponseModel) {
        setDebitCreditProgress(data)
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.RETAILER_CONSTANT) {
            data.due?.let {
                binding.pbDue.max = data.due_upper?.toInt() ?: 0
                binding.pbDue.progress = it.toInt()
                "${
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default))
                }$it".apply {
                    binding.tvDueAmount.text = this
                }
            }

            if (!data.ledger.isNullOrEmpty()) {
                binding.tvDuesCount.text = data.ledger.size.toString()
            } else binding.tvDuesCount.text = getString(R.string.zero)
        }

        checkDue(data)
        setCreditDebitAmounts(data)
    }

    private fun setCreditDebitAmounts(data: LedgerResponseModel) {
        if (data.creadit != null && data.debit != null) {
            if (data.creadit > data.debit) {
                binding.tvTotalAmount.changeTextColor(requireContext(), R.color.green_text_color)
                "${
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default))
                }${CommonUtils.convertDoubleTo2DecimalPlaces((data.creadit - data.debit))}".apply {
                    binding.tvTotalAmount.text = this
                }
            } else {
                binding.tvTotalAmount.changeTextColor(requireContext(), R.color.red_text_color)
                "${
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default))
                }${CommonUtils.convertDoubleTo2DecimalPlaces((data.debit - data.creadit))}".apply {
                    binding.tvTotalAmount.text = this
                }
            }
        }
    }

    private fun checkDue(data: LedgerResponseModel) {
        if (isDue) {
            if (!::ledgerListDue.isInitialized) {
                ledgerListDue = mutableListOf()
            } else {
                ledgerListDue.clear()
            }
            data.ledger?.let {
                ledgerListDue = it.toMutableList()
                setDuesAdapter()
            }
        } else {
            if (!::ledgerList.isInitialized) {
                ledgerList = mutableListOf()
            } else {
                ledgerList.clear()
            }
            data.ledger?.let {
                ledgerList = it.toMutableList()
                setAdapter()
            }
            if (ledgerList.size > 0) binding.tvDownloadSheet.makeVisible(true)
            else binding.tvDownloadSheet.makeVisible(false)
        }
    }

    private fun setDebitCreditProgress(data: LedgerResponseModel) {
        data.creadit?.let {
            binding.creditProgress.max = data.credit_upper?.toInt() ?: 0
            binding.creditProgress.progress = it.toInt()
            "+ ${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    AppENUM.RefactoredStrings.defaultCurrency)
            }${it}".apply {
                binding.tvCreditAmount.text = this
            }
        }
        data.debit?.let {
            binding.debitProgress.max = data.debit_upper?.toInt() ?: 0
            binding.debitProgress.progress = it.toInt()
            "- ${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    AppENUM.RefactoredStrings.defaultCurrency)
            }${it}".apply {
                binding.tvDebitAmount.text = this
            }
        }
    }

    private fun setDuesAdapter() {
        val ledgerAdapter =
            LedgerDuesAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default)), this)
        binding.rvDues.adapter = ledgerAdapter
        ledgerAdapter.setData(ledgerListDue)
    }

    private fun setAdapter() {
        (LedgerAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            getString(R.string.currency_symbol_default)), ledgerList)).apply {
            binding.rlLedger.adapter = this
        }
        binding.rlLedger.makeVisible(ledgerList.isNotEmpty())
        binding.clNoEntries.makeVisible(ledgerList.isEmpty())
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.tvYesterday -> {
                selectedType = filterList[0]
                getFilteredData(selectedType, R.id.tvYesterday)
            }
            R.id.tvToday -> {
                selectedType = filterList[1]
                getFilteredData(selectedType, R.id.tvToday)
            }
            R.id.tv1Week -> {
                selectedType = filterList[2]
                getFilteredData(selectedType, R.id.tv1Week)
            }
            R.id.tv1month -> {
                selectedType = filterList[3]
                getFilteredData(selectedType, R.id.tv1month)
            }
            R.id.tv1Year -> {
                selectedType = filterList[4]
                getFilteredData(selectedType, R.id.tv1Year)
            }

            R.id.btnGive -> {
                Bundle().apply {
                    putString(AppENUM.HEADING, AppENUM.GIVE)
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.GIVE_OR_TAKE, this))
                }
            }
            R.id.btnTake -> {
                Bundle().apply {
                    putString(AppENUM.HEADING, AppENUM.TAKE)
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.GIVE_OR_TAKE, this))
                }
            }

            R.id.tvDownloadSheet -> {
                if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                        false)) {
                    viewModel.getDownloadCSV(selectedType)
                } else {
                    fireFeatureLockedEvent()
                    requireActivity().let {
                        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT, Bundle().apply {
                            putString(AppENUM.PRO_LOCKED_ALERT_MSG,  dashboardViewModel.screensConfigResponse.value?.membershipConfig?.ledgerCSVMsg)
                        })
                            ?.let { baseFragment ->
                                baseFragment.show(it.supportFragmentManager,
                                    baseFragment.javaClass.name)
                            }
                    }
                }
            }

            R.id.viewAll, R.id.viewDue -> {
                switchDueType()
            }

            R.id.imgCall -> {
                CommonUtils.genericCastOrNull<Ledger>(v.getTag(R.id.imgCall)).let {
                    CommonUtils.openPhoneCallApp("${it?.customerDetails?.country_code} ${it?.customerDetails?.mobile ?: ""}",
                        requireContext())
                }
            }

            R.id.imgWhatsapp -> {
                isWhatsappMsg = true
                CommonUtils.genericCastOrNull<Ledger>(v.getTag(R.id.imgWhatsapp)).let {
                    clickedMobile =
                        "${it?.customerDetails?.country_code}${it?.customerDetails?.mobile ?: ""}"
                    showLoader()
                    viewModel.getReminderSMSAPI(OrderIdResponse().apply { orderId = it?.orderId })
                }
            }

            R.id.llSendReminder -> {
                isWhatsappMsg = false
                CommonUtils.genericCastOrNull<Ledger>(v.getTag(R.id.llSendReminder)).let {
                    clickedMobile =
                        "${it?.customerDetails?.country_code} ${it?.customerDetails?.mobile ?: ""}"
                    showLoader()
                    viewModel.getReminderSMSAPI(OrderIdResponse().apply { orderId = it?.orderId })
                }
            }

            R.id.llCollectDue -> {
                CommonUtils.genericCastOrNull<Ledger>(v.getTag(R.id.llCollectDue)).let {
                    FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
                        Bundle().apply {
                            putString(AppENUM.TITLE, getString(R.string.collect_due))
                            putString(AppENUM.DESCRIPTION,
                                getString(R.string.credit_due_clear_description))
                            putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.yes))
                            putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.no))
                        },
                        object : ActionListener {
                            override fun onActionItem(extra: Any?, extra2: Any?) {
                                if (extra as Boolean) {
                                    showLoader()
                                    viewModel.collectDueAPI(LedgerIdRequest().apply {
                                        ledgerId = it?.id
                                    })
                                }
                            }
                        })?.let { dialog ->
                        dialog.show(childFragmentManager, dialog.javaClass.name)
                    }
                }
            }

            R.id.clBase -> {
                CommonUtils.genericCastOrNull<Ledger>(v.getTag(R.id.clBase)).let {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                            Bundle().apply {
                                putString(AppENUM.IntentKeysENUM.ORDER_ID, it?.orderId)
                                putInt(AppENUM.IntentKeysENUM.SELECT_TAB, 2)
                            }))
                }
            }
        }
    }

    private fun switchDueType() {
        if (binding.tvDue.isClickable) {
            binding.tvDue.isClickable = false
            binding.tvAll.isClickable = true
            binding.tvDue.isEnabled = false
            binding.tvAll.isEnabled = true
            isDue = false
            binding.clAll.makeVisible(true)
            binding.clDue.makeVisible(false)
        } else {
            binding.tvDue.isClickable = true
            binding.tvAll.isClickable = false
            binding.tvDue.isEnabled = true
            binding.tvAll.isEnabled = false
            isDue = true
            binding.clAll.makeVisible(false)
            binding.clDue.makeVisible(true)
            if (!::ledgerListDue.isInitialized) {
                showLoader()
                viewModel.getLedger(selectedType, AppENUM.RefactoredStrings.DUE)
            }
        }
    }

    //         this method gets the filtered data based on selection
    private fun getFilteredData(type: String, textViewId: Int) {
        setBacGroundAccordingToFilter(textViewId)
        showLoader()

        if (isDue) viewModel.getLedger(type, AppENUM.RefactoredStrings.DUE)
        else viewModel.getLedger(type, null)
    }

    private fun setBacGroundAccordingToFilter(id: Int) {
        binding.tvYesterday.background = null
        binding.tvToday.background = null
        binding.tv1Week.background = null
        binding.tv1month.background = null
        binding.tv1Year.background = null
        when (id) {
            R.id.tvYesterday -> binding.tvYesterday.changeBackgroundDrawable(requireContext(),
                R.drawable.bg_selected_gray_2)

            R.id.tvToday -> binding.tvToday.changeBackgroundDrawable(requireContext(),
                R.drawable.bg_selected_gray_2)

            R.id.tv1Week -> binding.tv1Week.changeBackgroundDrawable(requireContext(),
                R.drawable.bg_selected_gray_2)

            R.id.tv1month -> binding.tv1month.changeBackgroundDrawable(requireContext(),
                R.drawable.bg_selected_gray_2)

            R.id.tv1Year -> binding.tv1Year.changeBackgroundDrawable(requireContext(),
                R.drawable.bg_selected_gray_2)
        }
    }


    private fun fireFeatureLockedEvent() {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_LEDGER_GVE_OR_TAKE)
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.ERROR_HEADER_PRO,
            bundle)

    }

}