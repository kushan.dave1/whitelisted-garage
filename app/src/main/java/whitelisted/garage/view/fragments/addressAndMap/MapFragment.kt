package whitelisted.garage.view.fragments.addressAndMap

import android.Manifest
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.view.View
import com.google.android.gms.location.LocationListener
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import kotlinx.android.synthetic.main.fragment_manage_packages.*
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.AddressData
import whitelisted.garage.api.request.CompleteRegData
import whitelisted.garage.base.BaseActivity
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentMapBinding
import whitelisted.garage.eventBus.CompleteRegLocUpdateEvent
import whitelisted.garage.eventBus.LocationSelectedEvent
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.viewmodels.AddressViewModel
import java.util.*

class MapFragment : BaseFragment(), OnMapReadyCallback, GoogleMap.OnCameraIdleListener,
    LocationListener, GoogleMap.OnCameraMoveStartedListener {

    private val binding by viewBinding(FragmentMapBinding::inflate)
    private val defaultZoom = 16.5f
    private var isMyLocation: Boolean? = null
    private var latitude: Double? = null
    private var longitude: Double? = null
    private var sheetBehavior: BottomSheetBehavior<*>? = null
    private var name: String? = null
    private var address: String? = null
    private var isFromComplete: Boolean? = null

    private val viewModel: AddressViewModel by sharedViewModel()
    private var mMap: GoogleMap? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding
        getData()
        setupScreen()
        readyMap()
    }

    private fun getData() {
        arguments?.let {
            isMyLocation = arguments?.getBoolean(AppENUM.IS_MY_CURRENT_LOCATION, false)
            longitude = arguments?.getDouble(AppENUM.LONGITUDE, 0.0)
            latitude = arguments?.getDouble(AppENUM.LATITUDE, 0.0)
            isFromComplete = it.getBoolean(AppENUM.IS_FROM_COMPLETE_REG, false)
            name = it.getString(AppENUM.NAME, "")
            address = it.getString(AppENUM.ADDRESS_POST, "")
        }
    }

    private fun setupScreen() {
        sheetBehavior = BottomSheetBehavior.from(binding.bsFragment.cardViewBottomSheet)
        imgBack.setOnClickListener(this)
        binding.bsFragment.confirmLocationBtn.setOnClickListener(this)
        binding.locateMeFab.setOnClickListener(this)
    }

    private fun readyMap() {
        (childFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment)?.getMapAsync(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
            R.id.locateMeFab -> {
                TedPermission.create().setPermissionListener(object : PermissionListener {
                    override fun onPermissionGranted() {
                        if (isAdded) {
                            (requireActivity() as? BaseActivity<*>)?.requestLocation(this@MapFragment,
                                false)
                        }
                    }

                    override fun onPermissionDenied(deniedPermissions: MutableList<String>?) { //do nothing
                    }
                }).setPermissions(Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION).check()
            }
            R.id.confirmLocationBtn -> {
                if (binding.bsFragment.addressTv.text.isNotEmpty()) {
                    val address = AddressData(
                        latitude,
                        longitude,
                        binding.bsFragment.addressTv.text.toString(),
                    )
                    viewModel.setAddressData(address)
                    if (isFromComplete == true) {
                        val completeRegData = CompleteRegData(name = name,
                            address = binding.bsFragment.addressTv.text.toString(),
                            addressData = address)
                        EventBus.getDefault().post(CompleteRegLocUpdateEvent(completeRegData))
                    } else {
                        EventBus.getDefault().post(LocationSelectedEvent(address))
                    }

                    popBackStackAndHideKeyboard()
                } else {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.address_not_fetched_yet))
                }
            }
        }
    }

    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        mMap?.uiSettings?.isMyLocationButtonEnabled = false
        mMap?.setOnCameraIdleListener(this)
        mMap?.setOnCameraMoveStartedListener(this)
        mMap?.setOnMyLocationButtonClickListener { false }

        if (isMyLocation == true) {
            TedPermission.create().setPermissionListener(object : PermissionListener {
                override fun onPermissionGranted() {
                    if (isAdded) {
                        (requireActivity() as? BaseActivity<*>)?.requestLocation(this@MapFragment,
                            false)
                    }
                }

                override fun onPermissionDenied(deniedPermissions: MutableList<String>?) { //do nothing
                }
            }).setPermissions(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION).check()
        } else {
            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude ?: 0.0,
                longitude ?: 0.0), defaultZoom))
        }
    }

    override fun onCameraIdle() {
        sheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
        latitude = mMap?.cameraPosition?.target?.latitude
        longitude = mMap?.cameraPosition?.target?.longitude
        setAddressFromLocation(latitude ?: 0.0, longitude ?: 0.0)
    }

    override fun onCameraMoveStarted(p0: Int) { //
    }

    private fun setAddressFromLocation(latitude: Double, longitude: Double) {
        try {
            val geocoder = Geocoder(requireContext(), Locale.getDefault())
            val addresses: List<Address>? = geocoder.getFromLocation(latitude, longitude, 1)
            if (addresses?.isNotEmpty() == true) {
                val address: String = addresses[0].getAddressLine(0)
                binding.bsFragment.addressTv.text = address
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onLocationChanged(p0: Location) {
        longitude = p0.longitude
        latitude = p0.latitude
        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(p0.latitude, p0.longitude),
            defaultZoom))
        setAddressFromLocation(latitude ?: 0.0, longitude ?: 0.0)
    }

}