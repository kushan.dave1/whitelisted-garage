package whitelisted.garage.view.fragments.sparesShop

import android.content.ClipData
import android.content.ClipboardManager
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Parcelable
import android.provider.MediaStore
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import coil.ImageLoader
import coil.load
import coil.request.ImageRequest
import coil.request.SuccessResult
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.item_bundle_item.view.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.request.AddressData
import whitelisted.garage.api.request.Enquiry
import whitelisted.garage.api.response.AddressResponseModel
import whitelisted.garage.api.response.InstallationSteps
import whitelisted.garage.api.response.MediaUrl
import whitelisted.garage.api.response.SSItemModel
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.CardHowToInstallBinding
import whitelisted.garage.databinding.DiscountOfferCardBinding
import whitelisted.garage.databinding.FragmentSparesProductDetailBinding
import whitelisted.garage.databinding.ItemBundleItemBinding
import whitelisted.garage.eventBus.LocationSelectedEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.checkNull
import whitelisted.garage.utils.CommonUtils.ifNotNullOrZero
import whitelisted.garage.utils.CommonUtils.isVisible
import whitelisted.garage.utils.CommonUtils.lower
import whitelisted.garage.utils.CommonUtils.makeInVisible
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.CommonUtils.roundOff
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.activities.DashboardActivity
import whitelisted.garage.view.adapter.*
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.SelectAddressViewModel
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import kotlin.math.roundToLong

class SparesProductDetails : BaseFragment() {

    private val storageRef by lazy { FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference }
    private var product: SSItemModel? = null
    private val binding by viewBinding(FragmentSparesProductDetailBinding::inflate) //dataBinding<FragmentSparesProductDetailBinding>(R.layout.fragment_spares_product_detail)
    private val viewModel: SparesShopFragmentViewModel by sharedViewModel()
    private val addressViewModel: SelectAddressViewModel by viewModel()
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var buyNowCartId: String? = null
    private var selectedAddress: AddressResponseModel? = null
    private lateinit var bundleItems: MutableList<SSItemModel>
    private var enquiry: Enquiry? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        product = arguments?.getParcelable(AppENUM.SSITEM)

        getAllAddressObserver()
        observeAddToCartResponse()
        setCartItemsCount()
        clickListeners()
        setupViews()
        getPdpBanner()
        getPdpFAQs()
        getOffers()
        setSelectAddressFragmentResult()
        setViewPagerScrollListener()
        getAddressList()

        binding.srlFSPD.setOnRefreshListener {
            binding.srlFSPD.isRefreshing = false
            product?.productId?.let {
                showLoader()
                getProductDetail(it)
            }
        }
        setFragmentResultListener(AppENUM.ADD_ADDRESS) { _, _ ->
            sendEnquiry()
        }
    }

    private fun getAddressList() {
        addressViewModel.getAllAddresses()
    }

    private fun getAllAddressObserver() {
        addressViewModel.provideAddressesResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        lifecycleScope.launch {
                            var pin = ""
                            val isPresent = it.body.data.any { add -> add.isDefault == true }
                            if (isPresent) {
                                it.body.data.find { add -> add.isDefault == true }?.let { pinAdd ->
                                    pin = pinAdd.pin ?: ""
                                }
                            } else pin = it.body.data[0].pin ?: ""
                            if(product?.isEnquiry != true && product?.inventory != 0) {
                                val estDel = addressViewModel.getEstimatedDelivery(pin)
                                estDel?.let { estimatedDelRes ->
                                    binding.clEstDel.makeVisible(true)
                                    if (estimatedDelRes.estimatedDelivery.isNullOrEmpty()) {
                                        binding.clEstDel.makeVisible(false)
                                    } else binding.tvEstDelivery.text =
                                        estimatedDelRes.estimatedDelivery
                                }
                            }

                        }
                    } else {
                        lifecycleScope.launch {
                            val estDel = addressViewModel.getEstimatedDelivery("")
                            estDel?.let { estimatedDelRes ->
                                binding.clEstDel.makeVisible(true)
                                binding.tvEstDelivery.text = estimatedDelRes.estimatedDelivery
                            }
                        }
                    }
                }
                is Result.Failure -> { //                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setSelectAddressFragmentResult() {
        setFragmentResultListener(AppENUM.SELECT_ADDRESS_PDP) { _, bundle -> // We use a String here, but any type that can be put in a Bundle is supported
            val result: Parcelable? = bundle.getParcelable(AppENUM.ADDRESS_POST)
            (result as? AddressResponseModel)?.let {
                lifecycleScope.launch {
                    showLoader()
                    val res = viewModel.buyNowProduct(product?.productId, product)
                    hideLoader()
                    res?.let { cartIdResponse ->
                        buyNowCartId = cartIdResponse.id
                        selectedAddress = it
                        Bundle().apply {
                            putString(AppENUM.CART_ID, cartIdResponse.id)
                            putString(AppENUM.AMOUNT_TO_BE_PURCHASED, product?.gomPrice.toString())
                            putParcelable(AppENUM.ADDRESS_POST, it)
                            putBoolean(AppENUM.IS_ACCESSORIES_CART, product?.isAccessories ?: false)
                            putBoolean(AppENUM.IS_EMI, true)
                            CommonUtils.addFragmentUtil(requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.SS_PAYMENT_TYPE,
                                    this))
                        }
                        fireCheckoutEvent(it)
                    }
                }
            }
        }
        setFragmentResultListener(AppENUM.SELECT_ADDRESS_ENQUIRY) { _, _ ->
            sendEnquiry()
        }
    }

    private fun setCartItemsCount() {
        if (viewModel.provideGetCartResponse().cartList.isNotEmpty()) {
            viewModel.provideGetCartResponse().cartList[0].lineItems.size.plus(viewModel.provideGetCartResponse().cartList[1].lineItems.size)
                .let { itemCount ->
                    if (itemCount > 0) {
                        binding.tvCartCount.makeVisible(true)
                        binding.tvCartCount.text = "$itemCount"
                    } else binding.tvCartCount.makeVisible(false)
                }
            viewModel.provideGetCartResponseAPI().observe(viewLifecycleOwner) { result ->
                (result as? Result.Success)?.body?.data?.let {
                    checkChangesInMoreItems(it.cartList.map { c -> c.lineItems }.flatten())
                    val itemsCount = it.cartList[0].lineItems.size
                    itemsCount.plus(it.cartList[1].lineItems.size).let { itemCount ->
                        if (itemCount > 0) {
                            binding.tvCartCount.makeVisible(true)
                            binding.tvCartCount.text = "$itemCount"
                        } else binding.tvCartCount.makeVisible(false)
                    }
                }
            }
        }
    }

    private fun getPdpBanner() = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
        viewModel.getPDPBannerImage()?.let { banner ->
            binding.bannerImage.load(banner.bannerImage?.image)
            binding.bannerImage.makeVisible(true)
            binding.bannerImage.setOnClickListener {
                if (banner.bannerImage?.type == "website") {
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(banner.bannerImage.rediract)
                    startActivity(i)
                } else {
                    val intent = Intent(requireContext(), DashboardActivity::class.java)
                    intent.data = Uri.parse(banner.bannerImage?.rediract)
                    startActivity(intent)
                }
                sendAssuranceBannerClickEvent(banner.bannerImage?.rediract)
            }
        }
    }

    private fun observeAddToCartResponse() {
        viewModel.provideAddToCartResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> { //                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    viewModel.cartItemChanged?.let { changedItemModel ->
                        if (product?.skuCode == changedItemModel.skuCode) {
                            product?.quantity = changedItemModel.quantity
                            binding.tvQuantity.text = product?.quantity.toString()
                            if ((product?.quantity ?: 0) > 0) {
                                binding.cgAddedToCart.makeVisible(true)
                                binding.btnAddToCart.makeVisible(false)
                            } else {
                                binding.cgAddedToCart.makeVisible(false)
                                binding.btnAddToCart.makeVisible(true)
                            }
                        }
                        if (binding.cgBundleItems.isVisible()) {
                            if (::bundleItems.isInitialized) {
                                bundleItems.forEachIndexed { index, ssItemModel ->
                                    if (ssItemModel.productId == changedItemModel.productId) {
                                        ssItemModel.quantity = changedItemModel.quantity
                                        binding.rvBundleItems.adapter?.notifyItemChanged(index)
                                    }
                                }
                            }
                        }
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun clickListeners() {
        binding.imgBack.setOnClickListener(this)
        binding.btnAddToCart.setOnClickListener(this)
        binding.imgCart.setOnClickListener(this)
        binding.imgShare.setOnClickListener(this)
        binding.imgPlusFSPD.setOnClickListener(this)
        binding.imgMinusFSPD.setOnClickListener(this)
        binding.btnGoToCart.setOnClickListener(this)
        binding.clEMIFSPD.setOnClickListener(this)
        binding.ivRemoveBundleItems.setOnClickListener(this)
    }

    private fun setupViews() = product?.let {
        binding.nsvMain.makeVisible(true)
        binding.vpFSPD.transitionName = it.skuCode
        if (it.tag?.tagName.isNullOrEmpty()) {
            binding.clTagFSPD.makeInVisible(true)
            binding.tvTagFSPD.text = binding.tvTagFSPD.context.getString(R.string.best_price)
        } else {
            binding.clTagFSPD.makeInVisible(false)
            it.tag?.let { tag ->
                binding.tvTagFSPD.text = tag.tagName
                val wrappedDrawable =
                    DrawableCompat.wrap(AppCompatResources.getDrawable(binding.tvTagFSPD.context,
                        R.drawable.ic_ss_tag_bg_inverted)!!)
                wrappedDrawable.setTint(Color.parseColor(tag.bgColor))
                binding.imgTagBg.setImageDrawable(wrappedDrawable)
                binding.tvTagFSPD.setTextColor(Color.parseColor(tag.textColor))
            }
        }

        val currencySymbol =
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol)
        if ((it.quantity ?: 0) > 0) {
            binding.cgAddedToCart.makeVisible(true)
            binding.btnAddToCart.makeVisible(false)
            if ((it.quantity ?: 0) > 0) binding.tvQuantity.text = "${it.quantity}"
            else binding.tvQuantity.text = "${it.quantity}"
        } //        if (it.quantity?:0 > 0) {
        //            it.quantity = it.quantity
        //        }

        if ((it.inventory ?: 0) < 1) {
            binding.clTagFSPD.makeInVisible(true)
            if(product?.isEnquiry != true) {
                binding.clAddToCart.makeVisible(false)
            }

            binding.clNotifyMe.makeVisible(true)
            binding.btnNotifyMe.setOnClickListener(::setReminder)
            if (it.isNotified == true) {
                binding.btnNotifyMe.makeVisible(false)
                binding.tvReminderOn.makeVisible(true)
            }
        }

        (resources.getString(R.string.mrp) + currencySymbol + it.mrp?.roundToLong()
            ?.toDouble()).apply {
            binding.tvMrpPDFTXT.text = this
        }
        binding.tvMrpPDFTXT.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        ("$currencySymbol${
            it.gomPrice?.roundToLong()?.toDouble()
        }").apply { binding.tvPricePDF.text = this }

        checkForImages()

        it.brand?.let { brandName ->
            val name = "$brandName ${it.title}"
            val spannableString = SpannableString(name)
            spannableString.setSpan(RelativeSizeSpan(1.1f), 0, brandName.length, 0)
            spannableString.setSpan(StyleSpan(Typeface.BOLD), 0, brandName.length, 0)
            spannableString.setSpan(ForegroundColorSpan(ContextCompat.getColor(requireContext(),
                R.color.black_trans_new)), 0, brandName.length, 0)
            binding.tvNamePDF.text = spannableString
        } ?: run {
            binding.tvNamePDF.text = it.title
        }
        binding.tvPartNumber.text = it.productId

        if (!it.specifications.isNullOrEmpty()) {
            binding.llSpecifications.makeVisible(true)
            binding.rvSpecifications.layoutManager = LinearLayoutManager(requireContext())
            binding.rvSpecifications.adapter = ProductSpecificationAdapter(it.specifications)
        }

        if (it.highlights.isNotEmpty()) {
            binding.llHighlights.makeVisible(true)
            binding.rvHighlights.layoutManager = LinearLayoutManager(requireContext())
            binding.rvHighlights.adapter = ProductHighlightsAdapter(it.highlights)
        }

        if (!it.description.isNullOrEmpty()) {
            binding.llDesc.makeVisible(true)
            binding.tvProductDescription.text = it.description
        }

        binding.tvProductDescription.post {
            binding.tvProductDescription.layout?.let { l ->
                val lines = l.lineCount
                if (lines > 0 && l.getEllipsisCount(lines - 1) > 0) {
                    binding.tvViewMoreDetail.setOnClickListener(this)
                    binding.tvViewMoreDetail.makeVisible(true)
                }
            }
        }

        if (it.isEnquiry) setupEnquiryView()

        if (it.bulkOrderQuantity.ifNotNullOrZero() && (it.inventory ?: 0) >= (it.bulkOrderQuantity
                ?: 0)) setupBulkDiscountView()

        if (!it.compatibility.isNullOrEmpty()) {
            binding.compatibilityView.setData(it.compatibility)
            binding.clCompCars.makeVisible(true)
        } else getCompatibility(it.productId)

        val moreItems = it.egAlternative + it.categoryAlternative

        if (moreItems.isNotEmpty()) {
            binding.lblMoreItems.text = it.alternativeTitle
            showAlternativeProducts(moreItems)
        } else binding.clMoreItems.makeVisible(false)


        it.installationSteps?.let { ins ->
            if (!ins.steps.isNullOrEmpty()) showInstallationSteps(ins)
        }

        if ((product?.gomPrice ?: 0.0) >= (viewModel.provideGetCartResponse().minEmiAmount
                ?: 1000.0)) {
            getEmiAmount()
        }

        saveToRecentItems(it)

        if ((it.bundleItems?.size ?: 0) > 0) {
            if (viewModel.provideGetCartResponse().cartList.isNotEmpty()) {
                var isPresent = false
                bundleItems = mutableListOf()
                it.bundleItems?.forEach { itemModel ->
                    isPresent = if (itemModel.isAccessories == true) {
                        viewModel.provideGetCartResponse().cartList[0].lineItems.any { cartItem -> cartItem.productId == itemModel.productId }
                    } else {
                        viewModel.provideGetCartResponse().cartList[1].lineItems.any { cartItem -> cartItem.productId == itemModel.productId }
                    }
                    if (!isPresent) {
                        bundleItems.add(itemModel)
                    }
                }
                if (!isPresent) {
                    binding.cgBundleItems.makeVisible(true)

                    binding.rvBundleItems.clear()
                        .addViews(bundleItems, ItemBundleItemBinding::inflate) { binding, item, _ ->
                            whitelisted.garage.utils.ImageLoader.loadImage(binding.ivPartImage,
                                item.imageUrl,
                                placeholderImage = R.drawable.ic_placeholder_square)
                            binding.tvPartName.text = item.title
                            "${binding.tvPartId.context.getString(R.string.part_no_colon)} ${item.skuCode}".apply {
                                binding.tvPartId.text = this
                            }
                            "$currencySymbol${item.gomPrice.roundOff()}".apply {
                                binding.tvPrice.text = this
                            }
                            "$currencySymbol${((item.mrp ?: 0.0).times(item.quantity ?: 1)).roundOff()}".apply {
                                binding.tvMRP.text = this
                            }
                            binding.tvMRP.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

                            if ((item.quantity ?: 0) > 0) {
                                binding.rlPlusMinus.makeVisible(true)
                                binding.btnAddIBI.makeVisible(false)
                                binding.tvQuantity.text = "${item.quantity}"
                            } else {
                                binding.rlPlusMinus.makeVisible(false)
                                binding.btnAddIBI.makeVisible(true)
                            }

                            binding.btnAddIBI.setOnClickListener {
                                item.quantity = 1
                                showLoader()
                                item.type = 1
                                binding.tvQuantity.text = "${item.quantity}"
                                binding.btnAddIBI.makeVisible(false)
                                binding.rlPlusMinus.makeVisible(true)
                                viewModel.cartItemChanged = item
                                viewModel.addToCart(item.productId, item)
                            }

                            binding.imgPlusCart.setOnClickListener {
                                item.apply {
                                    if (inventory.checkNull() > quantity.checkNull()) {
                                        showLoader()
                                        type = 1
                                        this.quantity = (this.quantity)?.plus(1)
                                        binding.tvQuantity.text = "${this.quantity}"
                                        viewModel.cartItemChanged = this
                                        viewModel.addToCart(productId, this)
                                        fireAddCartEvent(this)
                                    } else CommonUtils.showToast(requireContext(),
                                        resources.getString(R.string.only_items_in_stock,
                                            inventory))
                                }
                            }

                            binding.imgMinusCart.setOnClickListener {
                                showLoader()
                                item.apply {
                                    type = 0
                                    this.quantity = (this.quantity)?.minus(1)
                                    binding.tvQuantity.text = "${this.quantity}"
                                    viewModel.cartItemChanged = this
                                    if (quantity == 0) {
                                        binding.rlPlusMinus.makeVisible(false)
                                        binding.btnAddIBI.makeVisible(true)
                                    }
                                    viewModel.addToCart(productId, this) //                    }
                                }
                            }
                        }
                }
            }
        }
    } ?: checkDeeplink()

    private fun checkForImages() = product?.let {
        binding.vpFSPD.adapter = ProductDetailImagesAdapter(it.mediaUrl.ifEmpty {
            listOf(MediaUrl(it.imageUrl ?: "", 1))
        }, this)
        if (it.mediaUrl.size <= 1) binding.dotsIndicatorFSPD.makeVisible(false)
        else {
            binding.dotsIndicatorFSPD.attachTo(binding.vpFSPD)
            binding.dotsIndicatorFSPD.makeVisible(true)
        }
    }

    private fun getCompatibility(productId: String?) = lifecycleScope.launch {
        val quantity = product?.quantity
        product = viewModel.getProductDetails(productId ?: "")
        product?.quantity = quantity
        checkForImages()
        product?.let {

            if (!it.compatibility.isNullOrEmpty()) {
                binding.compatibilityView.setData(it.compatibility)
                binding.clCompCars.makeVisible(true)
            }

            val moreItems = it.egAlternative + it.categoryAlternative
            if (moreItems.isNotEmpty()) {
                binding.lblMoreItems.text = it.alternativeTitle
                showAlternativeProducts(moreItems)
            }

            it.installationSteps?.let { ins ->
                if (!ins.steps.isNullOrEmpty()) showInstallationSteps(ins)
            }
        }
    }

    private fun showInstallationSteps(installationSteps: InstallationSteps) = binding.apply {

        clHTI.makeVisible(true)
        lblMarketplaceHTI.text = installationSteps.title
        val steps = installationSteps.steps ?: listOf()
        rvMarketplaceHTI.setLayoutManagerAsLinear()
        rvMarketplaceHTI.clear()
            .addViews(steps, CardHowToInstallBinding::inflate) { cardBinding, step, pos ->
                cardBinding.tvPointNumber.text = "${pos + 1}"
                cardBinding.tvText.text = step.text
                cardBinding.vLine.makeVisible(pos != steps.size - 1)
            }

        imgToggleHTI.setOnClickListener {
            val isExpanded = rvMarketplaceHTI.isVisible()
            rvMarketplaceHTI.makeVisible(!isExpanded)
            imgToggleHTI.animate().rotation(if (isExpanded) 0f else 180f).setDuration(500).start()
        }

        imgToggleHTI.callOnClick()
    }

    private fun showAlternativeProducts(moreItems: List<SSItemModel>) {
        val symbol = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL)
        binding.rvMoreItems.layoutManager =
            object : LinearLayoutManager(requireContext(), HORIZONTAL, false) {
                override fun checkLayoutParams(lp: RecyclerView.LayoutParams): Boolean {
                    lp.width = (width / 2.2).toInt()
                    return true
                }
            }
        binding.clMoreItems.makeVisible(true)
        binding.rvMoreItems.adapter = SSItemsAdapter(symbol,
            mutableListOf<SSItemModel>().apply { addAll(moreItems.distinctBy { it.skuCode }) },
            this,
            showBorder = true)

        if (viewModel.provideGetCartResponse().cartList.isNotEmpty()) {
            (binding.rvMoreItems.adapter as? SSItemsAdapter)?.dataList?.forEachIndexed { index, ssItemModel ->
                val item =
                    (viewModel.provideGetCartResponse().cartList[0].lineItems + viewModel.provideGetCartResponse().cartList[1].lineItems).find { ssItemModel.productId == it.productId }
                item?.let {
                    ssItemModel.quantity = it.quantity
                    binding.rvMoreItems.adapter?.notifyItemChanged(index)
                }
            }
        }
    }

    private fun checkChangesInMoreItems(lineItems: List<SSItemModel>) {
        (binding.rvMoreItems.adapter as? SSItemsAdapter)?.dataList?.forEachIndexed { index, ssItemModel ->
            val item = lineItems.find { ssItemModel.productId == it.productId }
            item?.let {
                ssItemModel.quantity = it.quantity ?: 0
                binding.rvMoreItems.adapter?.notifyItemChanged(index)
            } ?: kotlin.run {
                ssItemModel.quantity = 0
                binding.rvMoreItems.adapter?.notifyItemChanged(index)
            }
        }
    }


    private fun getEmiAmount() = lifecycleScope.launch {
        product?.gomPrice?.let {
            val emi = viewModel.getEmiAmount(it)
            if (emi != null) {
                binding.emiFrame.makeVisible(true)
                binding.tvEmiDesc.text = resources.getString(R.string.emi_amount, emi)
            } else binding.emiFrame.makeVisible(false)
        }
    }

    private fun checkDeeplink() {
        val productId = arguments?.getString(AppENUM.SS_PRODUCT_ID, "") ?: ""
        if (productId.isNotEmpty()) {
            binding.nsvMain.makeVisible(false)
            binding.shimmerPDP.makeVisible(true)
            binding.shimmerPDP.startShimmer()
            getProductDetail(productId)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun getProductDetail(productId: String) = lifecycleScope.launch {
        val productDetail = viewModel.getProductDetails(productId)
        if (viewModel.provideGetCartResponse().cartList.isNotEmpty()) {
            val bothList =
                viewModel.provideGetCartResponse().cartList[0].lineItems + viewModel.provideGetCartResponse().cartList[1].lineItems
            productDetail.quantity =
                bothList.find { it.skuCode == productDetail.skuCode }?.quantity ?: 0
        }
        hideLoader()
        binding.shimmerPDP.makeVisible(false)
        product = productDetail
        setupViews()
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.tvViewMoreDetail -> if (v.tag == null || v.tag == false) {
                binding.tvProductDescription.maxLines = 30
                v.tag = true
                binding.tvViewMoreDetail.text = getString(R.string.view_less)
            } else {
                binding.tvProductDescription.maxLines = 3
                v.tag = false
                binding.tvViewMoreDetail.text = getString(R.string.view_more)
            }

            R.id.imgBack -> popBackStackAndHideKeyboard()

            R.id.imgCart, R.id.btnGoToCart -> {
                fireInitCartEvent()
                val isCartPresent =
                    requireActivity().supportFragmentManager.fragments.any { it is SparesShopCartFragment }
                if (!isCartPresent) CommonUtils.addFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.SS_CART)) else popBackStackAndHideKeyboard()

            }
            R.id.imgShare -> shareProduct()

            R.id.btnAddToCart -> {
                product?.apply {

                    if(isEnquiry) {
                        binding.nsvMain.smoothScrollTo(binding.nsvMain.x.toInt(), binding.evEnquiry.y.toInt())
                        binding.evEnquiry.onSendEnquiryClickedFromPDP()
                    }

                    else {
                        showLoader()
                        type = 1
                        this.quantity = (this.quantity)?.plus(1)
                        binding.tvQuantity.text = "${this.quantity}"
                        viewModel.cartItemChanged = this
                        viewModel.addToCart(productId, this)
                    }

                }
            }

            R.id.imgPlusFSPD -> {
                product?.apply {
                    if (inventory.checkNull() > quantity.checkNull()) {
                        showLoader()
                        type = 1
                        this.quantity = (this.quantity)?.plus(1)
                        binding.tvQuantity.text = "${this.quantity}"
                        viewModel.cartItemChanged = this
                        viewModel.addToCart(productId, this)
                        fireAddCartEvent(this)
                    } else CommonUtils.showToast(requireContext(),
                        resources.getString(R.string.only_items_in_stock, inventory))
                }
            }

            R.id.imgMinusFSPD -> {
                showLoader()
                product?.apply {
                    type = 0
                    this.quantity = (this.quantity)?.minus(1)
                    binding.tvQuantity.text = "${this.quantity}"
                    viewModel.cartItemChanged = this
                    viewModel.addToCart(productId, this) //                    }
                }
            }

            R.id.clEMIFSPD -> {
                showSelectAddressDialog()
            }

            R.id.btnAdd -> {
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.apply {
                    type = 1
                    showLoader()
                    viewModel.addToCart(productId, this)
                    fireAddCartEvent(this)
                }
            }

            R.id.imgPlus -> {
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.apply {
                    type = 1
                    showLoader()
                    viewModel.addToCart(productId, this)
                }
            }

            R.id.imgMinus -> {
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.apply {
                    type = 0
                    showLoader()
                    viewModel.addToCart(productId, this)
                }
            }

            R.id.clISI -> {
                viewModel.searchAndFilterAttrs.textSearch = ""
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                    sendClickSimilarEvent(it)
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_PRODUCT_DETAILS,
                            Bundle().apply {
                                putParcelable(AppENUM.SSITEM, it)
                            }))
                }
            }

            R.id.ivRemoveBundleItems -> {
                binding.cgBundleItems.makeVisible(false)
            }

            R.id.ivPartImage -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_PRODUCT_ZOOM,
                        Bundle().apply {
                            putParcelable(AppENUM.SSITEM, product)
                        }))
            }

            R.id.btnSendEnquiry -> {
                binding.evEnquiry.onSendEnquiryClickedFromPDP()
                binding.nsvMain.smoothScrollTo(0,binding.evEnquiry.y.toInt())
            }

        }
    }

    private fun showSelectAddressDialog() {
        hideLoader()
        requireActivity().let {
            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.SELECT_ADDRESS,
                Bundle().apply {
                    putBoolean(AppENUM.IS_FROM_PDP, true)
                })?.let { baseFragment ->
                baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
            }
        }
    }

    private fun getPdpFAQs() = lifecycleScope.launch {
        val faqs = viewModel.getMarketplacePdpFAQ()
        if (isAdded) {
            if (faqs.isNotEmpty()) binding.clFAQ.makeVisible(true)
            binding.rvMarketplaceFAQ.layoutManager = LinearLayoutManager(requireContext())
            binding.rvMarketplaceFAQ.adapter = GoCoinsFAQAdapter(faqs.toMutableList())
            binding.imgToggle.setOnClickListener {
                expandCollapseTrack()
            }
            expandCollapseTrack()
        }
    }

    private fun expandCollapseTrack() = binding.apply {
        val isExpanded = rvMarketplaceFAQ.isVisible()
        rvMarketplaceFAQ.makeVisible(!isExpanded)
        imgToggle.animate().rotation(if (isExpanded) 0f else 180f).setDuration(500).start()
    }


    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is LocationSelectedEvent -> {
                if (isAdded) {
                    (event.extra as? AddressData)?.let { addressData ->
                        requireActivity().let {
                            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.ADD_EDIT_ADDRESS,
                                Bundle().apply {
                                    putParcelable(AppENUM.ADDRESS_POST, addressData)
                                })?.let { baseFragment ->
                                baseFragment.show(it.supportFragmentManager,
                                    baseFragment.javaClass.name)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun shareProduct() = lifecycleScope.launchWhenStarted {
        val uri: Uri?
        val request = ImageRequest.Builder(requireContext()).data(product?.imageUrl ?: "")
            .allowHardware(false).build()
        val bitmap =
            ((ImageLoader(requireContext()).execute(request) as? SuccessResult)?.drawable as? BitmapDrawable)?.bitmap
        uri = saveMediaToStorage(bitmap)

        val link = "http://freegarage.in/?pagename=skuid&product_id=" + product?.skuCode
        val message =
            "Hi, checkout this product on Easy Garage Marketplace.\n\n" + "Product Name - ${product?.title} \n\n" + "Link: $link\n\n" + "Thanks"
        val intentBuilder =
            ShareCompat.IntentBuilder(requireActivity()).setType("text/plain").setText(message)
        uri?.let { intentBuilder.setStream(it) }
        val shareIntent = intentBuilder.createChooserIntent()
            .addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)

        startActivity(shareIntent)
    }


    private fun saveMediaToStorage(bitmap: Bitmap?): Uri? {
        try {
            val filename = "${System.currentTimeMillis()}.jpg"
            var fos: OutputStream? = null
            var imageUri: Uri? = null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                requireActivity().contentResolver?.also { resolver ->
                    val contentValues = ContentValues().apply {
                        put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                        put(MediaStore.MediaColumns.MIME_TYPE, "image/*")
                        put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
                    }
                    imageUri =
                        resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                    fos = imageUri?.let { resolver.openOutputStream(it) }
                }
            } else {
                val imagesDir =
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                val image = File(imagesDir, filename)
                imageUri = Uri.fromFile(image)
                fos = FileOutputStream(image)
            }
            fos?.use {
                bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, it)
            } ?: kotlin.run {
                return null
            }
            return imageUri
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    private fun setReminder(v: View) = lifecycleScope.launchWhenStarted {
        showLoader()
        product?.let { fireRemindMeEvent(it) }
        val result = viewModel.notifyForProduct(product?.skuCode, product?.title, product?.imageUrl)
        if (!(result == null || result.isJsonNull)) {
            binding.btnNotifyMe.makeVisible(false)
            binding.tvReminderOn.makeVisible(true)
            product?.isNotified = true
            viewModel.provideNotifyMeLiveEvent().postValue(product)
        } else CommonUtils.showToast(requireContext(),
            resources.getString(R.string.failed_to_set_reminder))
        hideLoader()
    }

    private fun sendEnquiry() = lifecycleScope.launch {
        val isSuccessfullySent = viewModel.sendEnquiry(enquiry ?: Enquiry())
        hideLoader()
        if (isSuccessfullySent) {
            binding.evEnquiry.setProductEnquired(product).showEnquirySuccessDialog()
            CommonUtils.showReviewDialog(requireActivity())
            CommonUtils.showFakeReviewDialog(requireActivity())
            fireSendEnquiryEvent(enquiry ?: Enquiry())
        } else CommonUtils.showToast(requireContext(),
            resources.getString(R.string.unable_to_send_enquiry))
    }

    private fun setupBulkDiscountView() {
        binding.bulkFrame.makeVisible(true)
        val isProMember = viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS, false)
        val discountRate = if (isProMember)
            dashboardViewModel.screensConfigResponse.value?.membershipConfig?.membershipBulkDiscount ?: 0
            else dashboardViewModel.screensConfigResponse.value?.membershipConfig?.basicBulkDiscount ?: 0
            //(product?.bulkDiscountRate ?: 0).toInt().toString()
        val qty = product?.bulkOrderQuantity ?: 0

        // Order 5 items & get extra 5% discount

        val stringBuilder = StringBuilder()

        val index1 = stringBuilder.append(getString(R.string.order)).length
        val index2 = stringBuilder.append(" $qty ").length
        stringBuilder.append(getString(R.string.items_sc)).length
        stringBuilder.append(" & ").length
        val index3 = stringBuilder.append(getString(R.string.get_extra)).length
        val index4 = stringBuilder.append(" $discountRate% ").length
        stringBuilder.append(getString(R.string.discount_sc))


        val bulkString = stringBuilder.toString()
        val spannableString = SpannableString(bulkString)

        spannableString.setSpan(RelativeSizeSpan(1.1f), index1, index2, 0)
        spannableString.setSpan(StyleSpan(Typeface.BOLD), index1, index2, 0)
        spannableString.setSpan(ForegroundColorSpan(ContextCompat.getColor(requireContext(),
            R.color.bulk_color)), index1, index2, 0)

        spannableString.setSpan(RelativeSizeSpan(1.1f), index3, index4, 0)
        spannableString.setSpan(StyleSpan(Typeface.BOLD), index3, index4, 0)
        spannableString.setSpan(ForegroundColorSpan(ContextCompat.getColor(requireContext(),
            R.color.bulk_color)), index3, index4, 0)

        binding.tvBulkDesc.text = spannableString
    }

    private fun setupEnquiryView() = lifecycleScope.launch {

        binding.evEnquiry.makeVisible(true)
        binding.evEnquiry.hideSendEnquiryBtn()
        //binding.clAddToCart.makeVisible(false)
        binding.clNotifyMe.makeVisible(false)
        binding.tvBuyNow.text = resources.getString(R.string.send_enquiry)

        val cost = viewModel.getEnquiryCost()
        binding.llEnquiryCost.makeVisible( cost > 0)
        binding.tvEnquiryCost.text = cost.toString()

        binding.evEnquiry.setEnquiryCost(viewModel.getEnquiryCost(), viewModel.getGoCoinsBalance())

        val enquiries = viewModel.getAllEnquiries()
        val isAlreadyEnquired = enquiries.any { it.productID == product?.productId }
        if (isAlreadyEnquired) binding.evEnquiry.setProductEnquired(product).showShared()

        val productEnqImagesRef = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
            .child(viewModel.getStringSharedPreference(AppENUM.USER_ID) + "/")
            .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
            .child(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID) + "/")
            .child(AppENUM.RefactoredStrings.PATH_SS_PRODUCT_ENQUIRY)
            .child("${product?.productId}/")

        val wid = viewModel.getSharedPreference()
            .getString(requireContext(), AppENUM.UserKeySaveENUM.WORKSHOP_ID, "")
        binding.evEnquiry.setUploadingPath(productEnqImagesRef).setWorkshopId(wid)
            .setOnUploadingStartListener { showLoader() }
            .setOnUploadingFinishedListener { enquiry ->
                enquiry.apply {
                    productName = product?.title
                    productBrand = product?.brand
                    productImage = product?.imageUrl
                    productID = product?.skuCode
                    price = product?.gomPrice
                }
                checkAddressAndSendEnquiry(enquiry)
            }

    }

    private fun checkAddressAndSendEnquiry(enquiry: Enquiry) = lifecycleScope.launch {
        if (isAdded) {
            this@SparesProductDetails.enquiry = enquiry
            if (addressViewModel.getAddresses().isEmpty()) showSelectAddressFragment()
            else sendEnquiry()
        }
    }

    private fun showSelectAddressFragment() {
        hideLoader()
        CommonUtils.addFragmentUtil(activity,
            FragmentFactory.fragment(FragmentFactory.Fragments.ADDRESS_FRAGMENT, Bundle().apply {
                putBoolean(AppENUM.SELECT_ADDRESS_ENQUIRY, true)
            }))

    }

    private fun setViewPagerScrollListener() {
        binding.vpFSPD.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrolled(position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                (binding.vpFSPD.adapter as? ProductDetailImagesAdapter)?.onPageScrolled(position)
            }
        })
    }

    private fun getOffers() = lifecycleScope.launch {
        val offers = viewModel.getPaymentTypesGet().asSequence().mapNotNull { it.options }.flatten()
            .mapNotNull { op ->
                op.offers?.onEach {
                    it.name = op.name
                    it.icon = op.imageName
                }
            }.flatten().filter { it.isVisible == true }.toList()

        if (offers.isNotEmpty()) {
            binding.clDiscountOffers.makeVisible(true)
            binding.rvDiscountOffers.setLayoutManagerAsLinearHorizontal()
            binding.rvDiscountOffers.addViews(offers,
                DiscountOfferCardBinding::inflate) { binding, offer, _ ->

                if (offer.type.lower() == AppENUM.COUPON) {
                    binding.tvOfferName.text = offer.text
                    binding.tvOfferDesc.text = resources.getString(R.string.use_code, offer.code)
                    binding.root.setOnClickListener {
                        val clip = ClipData.newPlainText("code", offer.code)
                        ContextCompat.getSystemService(binding.root.context,
                            ClipboardManager::class.java)?.setPrimaryClip(clip)
                        CommonUtils.showToast(binding.root.context,
                            resources.getString(R.string.copied_to_clipboard),
                            false)
                    }

                } else {
                    binding.tvOfferName.text = offer.name
                    binding.tvOfferDesc.text = offer.text
                    binding.root.setOnClickListener(null)
                }
            }
        }
    }

    private fun saveToRecentItems(item: SSItemModel) {
        if (viewModel.searchAndFilterAttrs.textSearch.isNotEmpty()) viewModel.insertRecentItem(item)
    }

    private fun fireAddCartEvent(item: SSItemModel) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, item.title)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_ENQUIRY, item.isEnquiry)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_FASTMOVING,
            item.isfastmoving)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.POPULAR_TAG, item.tag?.tagName)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_ID, item.id?.oid ?: "")
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_BRAND, item.brand ?: "")
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
            item.gomPrice.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_TYPE, item.skuCategory)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_PDP)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.ADD_TO_CART,
            bundle)
    }


    private fun fireInitCartEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_PDP)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_CART,
            bundle)
    }

    private fun fireRemindMeEvent(ssItemModel: SSItemModel, isClickSimilar: Boolean = false) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, ssItemModel.title)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_ID, ssItemModel.skuCode)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_ENQUIRY,
            ssItemModel.isEnquiry)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_FASTMOVING,
            ssItemModel.isfastmoving)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.POPULAR_TAG,
            ssItemModel.tag?.tagName)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
            ssItemModel.gomPrice.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_CATEGORY,
            ssItemModel.skuCategory)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_PDP)

        FirebaseAnalyticsLog.trackFireBaseEventLog(if (isClickSimilar) FirebaseAnalyticsLog.FirebaseEventNameENUM.CLICK_SIMILAR
        else FirebaseAnalyticsLog.FirebaseEventNameENUM.REMIND_ME, bundle)
    }

    private fun firePlaceOrder(paymentStatus: Boolean) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.TOTAL_AMOUNT,
            product?.gomPrice.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PAYMENT_MODE,
            AppENUM.MODE_ONLINE)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PAYMENT_STATUS, paymentStatus.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_PAYMENT_TYPE)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.PLACE_ORDER,
            bundle)
    }

    private fun fireCheckoutEvent(selectedAddress: AddressResponseModel) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CITY, selectedAddress.city)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.STATE, selectedAddress.state)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PIN, selectedAddress.pin)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.ITEM_DETAILS,
            product.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_CART)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.SPARES_CHECKOUT,
            bundle)
    }


    private fun fireSendEnquiryEvent(enquiry: Enquiry) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, enquiry.productName)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.QUANTITY,
            enquiry.quantity.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.REMARK, enquiry.remarks)

        product?.let { ssItemModel ->
            bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_ID,
                ssItemModel.skuCode)
            bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.BRAND, ssItemModel.brand)
            bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
                ssItemModel.gomPrice.toString())
            bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_CATEGORY,
                ssItemModel.skuCategory)
        }
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_PDP)

        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.ENQUIRE_NOW_STOCK,
            bundle)
    }

    private fun sendAssuranceBannerClickEvent(redirectionUrl: String?) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.REDIRECTION_URL, redirectionUrl)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_CART)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.PDP_BANNER_CLICK,
            bundle)

    }

    private fun sendClickSimilarEvent(ssItemModel: SSItemModel) {
        fireRemindMeEvent(ssItemModel, true)
    }

}