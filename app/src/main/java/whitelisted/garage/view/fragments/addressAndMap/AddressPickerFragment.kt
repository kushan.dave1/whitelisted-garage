package whitelisted.garage.view.fragments.addressAndMap

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.maps.android.SphericalUtil
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.request.CompleteRegData
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentAddressPickerBinding
import whitelisted.garage.eventBus.CompleteRegLocUpdateEvent
import whitelisted.garage.eventBus.CompleteRegLocUpdateEventHome
import whitelisted.garage.eventBus.LocationSelectedEvent
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.GooglePlacesAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import kotlin.math.sqrt

class AddressPickerFragment : BaseFragment() {

    private val binding by viewBinding(FragmentAddressPickerBinding::inflate)
    private var name: String? = null
    private var address: String? = null
    private var isFromComplete: Boolean? = null
    private var token: AutocompleteSessionToken? = null
    private var placesClient: PlacesClient? = null
    private var placesAdapter: GooglePlacesAdapter? = null
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var goingBack = false
    private var isFromHome = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPlaces()
        setupScreen()
    }

    private fun initPlaces() {
        if (isAdded) {
            Places.initialize(requireContext(), BuildConfig.GOOGLE_MAPS_KEY)
            placesClient = Places.createClient(requireContext())
            token = AutocompleteSessionToken.newInstance()
        }
    }

    private fun setupScreen() {
        binding.imgBack.setOnClickListener(this)
        binding.currentLocLayout.setOnClickListener(this)
        placesAdapter = GooglePlacesAdapter(this)
        binding.revSearchResult.adapter = placesAdapter
        binding.etSearch.addTextChangedListener(textWatcher)
        arguments?.let {
            isFromComplete = it.getBoolean(AppENUM.IS_FROM_COMPLETE_REG, false)
            name = it.getString(AppENUM.NAME, "")
            address = it.getString(AppENUM.ADDRESS_POST, "")
            isFromHome = it.getBoolean("home", false)
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is LocationSelectedEvent -> {
                popBackStackAndHideKeyboard()
            }
            is CompleteRegLocUpdateEvent -> {
                if (!goingBack) popBackStackAndHideKeyboard()
            }
            is CompleteRegLocUpdateEventHome -> {
                if (!goingBack) popBackStackAndHideKeyboard()
            }
        }
    }

    private fun getLatLngBounds(center: LatLng, meters: Double = 1000.00): LatLngBounds {
        val distanceFromCenterToCorner = meters * sqrt(2.0)
        val southwestCorner = SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 225.0)
        val northeastCorner = SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 45.0)
        return LatLngBounds(southwestCorner, northeastCorner)
    }

    fun getPlaces(string: String) {
        val currency =
            dashboardViewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                AppENUM.LanguageConstants.LAN_DEFAULT_CURRENCY)
        val request = FindAutocompletePredictionsRequest.builder().apply {
            this.sessionToken = token
            this.query = string
            if (currency == AppENUM.LanguageConstants.LAN_DEFAULT_CURRENCY) {
                this.setCountry("IN")
                this.locationBias =
                    RectangularBounds.newInstance(getLatLngBounds(LatLng(28.4705960, 77.094450)))
            }
        }.build()

        placesClient?.findAutocompletePredictions(request)
            ?.addOnSuccessListener { response: FindAutocompletePredictionsResponse ->
                placesAdapter?.setData(response.autocompletePredictions) //     }
            }?.addOnFailureListener { exception: Exception? ->
                if (exception is ApiException) {
                    initPlaces()
                }
            }
    }

    //              text watcher object for search purposes
    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(text: Editable?) {
            text?.let {
                when {
                    it.length > 2 -> {
                        getPlaces(it.toString())
                    }
                    it.trim().isNotEmpty() -> {
                        placesAdapter?.clearData()
                    }
                    else -> {
                        placesAdapter?.clearData()

                    }
                }
            }

        }

        override fun beforeTextChanged(s: CharSequence?,
            start: Int,
            count: Int,
            after: Int) { // do nothing
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { //
        }
    }

    override fun onStart() {
        super.onStart()
        onBackPress()
    }

    private fun onBackPress() {
        view?.isFocusableInTouchMode = true
        view?.requestFocus()
        view?.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                if (isFromComplete == true) {
                    val completeRegData = CompleteRegData(
                        name = name,
                        address = address,
                    )
                    goingBack = true
                    if (isFromHome) EventBus.getDefault()
                        .post(CompleteRegLocUpdateEventHome(completeRegData))
                    else EventBus.getDefault().post(CompleteRegLocUpdateEvent(completeRegData))
                }
                popBackStackAndHideKeyboard() //                requireActivity().onBackPressed()
                true
            } else false
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.rvParent -> {
                CommonUtils.genericCastOrNull<AutocompletePrediction>(v.getTag(R.id.model))
                    ?.let { data ->
                        showLoader()
                        getPlaceById(data.placeId, placesClient)
                    }
            }
            R.id.imgBack -> {
                if (isFromComplete == true) {
                    val completeRegData = CompleteRegData(
                        name = name,
                        address = address,
                    )
                    goingBack = true
                    if (isFromHome) EventBus.getDefault()
                        .post(CompleteRegLocUpdateEventHome(completeRegData))
                    else EventBus.getDefault().post(CompleteRegLocUpdateEvent(completeRegData))
                }
                popBackStackAndHideKeyboard()
            }
            R.id.currentLocLayout -> {
                Bundle().apply {
                    putBoolean(AppENUM.IS_MY_CURRENT_LOCATION, true)
                    if (isFromComplete == true) {
                        putBoolean(AppENUM.IS_FROM_COMPLETE_REG, true)
                        putString(AppENUM.NAME, name)
                        putString(AppENUM.ADDRESS_POST, address)
                    }
                    CommonUtils.addFragmentUtil(
                        activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.MAP_FRAGMENT, this),
                    )
                }
            }
        }
    }

    private fun getPlaceById(placeId: String, placesClient: PlacesClient?) {
        val place: Place? = null
        val request = FetchPlaceRequest.builder(placeId,
            listOf(Place.Field.ID,
                Place.Field.NAME,
                Place.Field.ADDRESS,
                Place.Field.LAT_LNG,
                Place.Field.ADDRESS_COMPONENTS)).build()

        placesClient?.fetchPlace(request)?.addOnSuccessListener { response ->
            hideLoader()
            Bundle().apply {
                putBoolean(AppENUM.IS_MY_CURRENT_LOCATION, false)
                putDouble(AppENUM.LONGITUDE, response?.place?.latLng?.longitude ?: 0.0)
                putDouble(AppENUM.LATITUDE, response?.place?.latLng?.latitude ?: 0.0)
                if (isFromComplete == true) {
                    putBoolean(AppENUM.IS_FROM_COMPLETE_REG, true)
                    putString(AppENUM.NAME, name)
                    putString(AppENUM.ADDRESS_POST, address)
                }
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.MAP_FRAGMENT, this),
                )
            }
            Log.d("PlaceData", place?.name.toString())
        }?.addOnFailureListener { exception ->
            hideLoader()
            if (exception is ApiException) {
                Log.d("PlaceError", "Place not found: ${exception.message}")
            }
        }
    }

}

