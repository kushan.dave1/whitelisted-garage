package whitelisted.garage.view.fragments.sparesShop

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import whitelisted.garage.R
import whitelisted.garage.api.response.SparesPurchaseItem
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentSparesPurchaseSummaryBinding
import whitelisted.garage.utils.AppENUM

class PurchaseSummaryFragment: BaseFragment(),View.OnClickListener {

    private val purchaseItem by lazy { arguments?.getParcelable<SparesPurchaseItem>("purchase_item") }
    private val binding by dataBinding<FragmentSparesPurchaseSummaryBinding>(R.layout.fragment_spares_purchase_summary)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()

    }

    private fun initViews() = purchaseItem?.let { item ->

        binding.tvItemCountCSPH.text = resources.getString(R.string.qty_items,item.items.size)
        binding.tvOrderIdFSPS.text = item.orderId
        binding.tvItemTotal.text = AppENUM.RefactoredStrings.defaultCurrencySymbol + item.totalAmount
        binding.tvDiscount.text = AppENUM.RefactoredStrings.defaultCurrencySymbol + item.discount
        binding.tvTotalPrice.text = AppENUM.RefactoredStrings.defaultCurrencySymbol + (item.totalAmount - item.discount)

        binding.rvPurchaseSummary.layoutManager = LinearLayoutManager(requireContext())


        binding.imgBack.setOnClickListener(this)

    }

    override fun onClick(v: View) {
        super.onClick(v)

        when(v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
        }

    }

}