package whitelisted.garage.view.fragments.workshops.viewEditWorkshop

import android.Manifest
import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.storage.FirebaseStorage
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.request.AddWorkshopRequest
import whitelisted.garage.api.request.AddressData
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentViewEditWorkshopBinding
import whitelisted.garage.eventBus.LocationSelectedEvent
import whitelisted.garage.eventBus.UpdateHomeScreenEvent
import whitelisted.garage.eventBus.WorkshopUpdateEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.viewmodels.ViewEditWorkshopViewModel
import java.util.*

class ViewEditWorkshopFragment : BaseFragment() {

    private val binding by viewBinding(FragmentViewEditWorkshopBinding::inflate)
    private val viewModel: ViewEditWorkshopViewModel by viewModel()
    private var isEdit = false
    private lateinit var addWorkshopRequest: AddWorkshopRequest
    private lateinit var profilePicture: Uri
    private lateinit var selectedLocation: LatLng


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        getDataFromArgs()
        setData()
        clickListeners()
        observeUpdateWorkshopResponse()
        observeUploadImageResponse()
    }

    private fun getDataFromArgs() {
        (arguments?.getSerializable(AppENUM.IntentKeysENUM.WORKSHOP_MODEL) as? WorkshopListResponseModel)?.let {
            viewModel.workshopModel = it
        }
        if (arguments?.getBoolean(AppENUM.IntentKeysENUM.IS_EDIT, false) == true) isEdit = true
        if (isEdit) binding.btnSave.makeVisible(true)
        else disableFields()
    }

    private fun observeUploadImageResponse() {
        viewModel.provideUploadWorkshopImageResponse().observe(viewLifecycleOwner) {
            hideLoader()
            if (it == "") {
                popBackStackAndHideKeyboard()
                EventBus.getDefault().post(WorkshopUpdateEvent())
            } else CommonUtils.showToast(requireContext(), it, true)
        }
    }

    private fun disableFields() = binding.apply {
        etGarageName.isEnabled = false
        etOwnerName.isEnabled = false
        etGSTNumber.isEnabled = false
        etGarageAddress.isEnabled = false
        etGoogleMapLink.isEnabled = false
        etInvoiceName.isEnabled = false
        etCity.isEnabled = false
        etState.isEnabled = false
        etPincode.isEnabled = false
        lblUploadLogo.makeVisible(false)
    }

    private fun observeUpdateWorkshopResponse() {
        viewModel.provideUpdateWorkshopResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    if (::profilePicture.isInitialized) {
                        if (::profilePicture.isInitialized) {
                            viewModel.uploadProfilePicture(requireContext(),
                                profilePicture,
                                viewModel.workshopModel.workshopId)
                        } else {
                            hideLoader()
                            CommonUtils.showToast(requireContext(), it.body.message, true)
                            EventBus.getDefault().post(WorkshopUpdateEvent())
                            popBackStackAndHideKeyboard()
                        }
                    } else {
                        hideLoader()
                        CommonUtils.showToast(requireContext(), it.body.message, true)
                        EventBus.getDefault().post(WorkshopUpdateEvent())
                        popBackStackAndHideKeyboard()
                    }
                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                    saveToPrefs()
                }
                is Result.Failure -> {
                    hideLoader()
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun saveToPrefs() = binding.apply {
        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME,
            etGarageName.text.toString())
        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_ADDRESS,
            etGarageAddress.text.toString())
        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.OWNER_NAME,
            etOwnerName.text.toString())
        viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_GSTIN,
            etGSTNumber.text.toString())
    }

    private fun clickListeners() {
        binding.imgBack.setOnClickListener(this)
        binding.imageContextCopy.setOnClickListener(this)
        binding.btnSave.setOnClickListener(this)
        binding.viewMapClick.setOnClickListener(this)
        binding.etGarageAddress.setOnClickListener(this)
        if (isEdit) binding.imgWorkshopImage.setOnClickListener(this)
    }

    private fun setData() = binding.apply {
        selectedLocation = LatLng(viewModel.workshopModel.latitude ?: 0.0,
            viewModel.workshopModel.longitude ?: 0.0)
        tvHeader.text = viewModel.workshopModel.name
        etGarageName.setText(viewModel.workshopModel.name)
        etOwnerName.setText(viewModel.workshopModel.ownerName)
        etGSTNumber.setText(viewModel.workshopModel.gstin)
        etGarageAddress.setText(viewModel.workshopModel.address)
        etGoogleMapLink.setText(viewModel.workshopModel.mapLink)
        etInvoiceName.setText(viewModel.workshopModel.invoiceName)
        etState.setText(viewModel.workshopModel.state)
        etCity.setText(viewModel.workshopModel.city)
        etPincode.setText(viewModel.workshopModel.pin)
        tvWorkShopID.text = viewModel.workshopModel.workshopId
        viewModel.storageRef =
            FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
        viewModel.workshopImageRef =
            viewModel.storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
                .child(viewModel.getStringSharedPreference(AppENUM.USER_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                .child("${viewModel.workshopModel.workshopId}/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOP_IMAGE)
                .child("${viewModel.workshopModel.workshopId}")
        viewModel.workshopImageRef.downloadUrl.addOnSuccessListener {
            loadCircularImage(it.toString(), imgWorkshopImage)
        }

        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                AppENUM.RefactoredStrings.defaultCountryCode) == AppENUM.RefactoredStrings.defaultCountryCode) {
            etGSTNumber.makeVisible(true)
            lblGSTNumber.makeVisible(true)
        } else {
            etGSTNumber.makeVisible(false)
            lblGSTNumber.makeVisible(false)
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.btnSave -> {
                if (formCheck() && !isLoading()) {
                    lifecycleScope.launch {
                        showLoader()
                        if (binding.etGSTNumber.text.toString().isNotEmpty()) {
                            val verifyGSTResponse =
                                viewModel.checkGST(binding.etGSTNumber.text.toString())
                            verifyGSTResponse?.let {
                                if ((it.gstinStatus ?: 0) == 1) {
                                    showLoader()
                                    viewModel.updateWorkshop(addWorkshopRequest)
                                } else {
                                    hideLoader()
                                    CommonUtils.showToast(requireContext(), it.message ?: "")
                                }
                            } ?: run {
                                hideLoader()
                                CommonUtils.showToast(requireContext(),
                                    getString(R.string.error_invalid_gstt))
                            }
                        } else {
                            showLoader()
                            viewModel.updateWorkshop(addWorkshopRequest)
                        }
                    }
                }
            }
            R.id.imageContextCopy -> {
                copyIdOnClick()
            }

            R.id.imgWorkshopImage -> {
                openImagePicker()
            }
            R.id.viewMapClick -> {
                if (binding.etGoogleMapLink.text.toString().isNotEmpty()) {
                    val clipboardManager =
                        requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    val clipData =
                        ClipData.newPlainText("text", binding.etGoogleMapLink.text.toString())
                    clipboardManager.setPrimaryClip(clipData)
                    CommonUtils.showToast(requireContext(), getString(R.string.text_coppied))
                } else {
                    CommonUtils.showToast(requireContext(), getString(R.string.link_not_available))
                }
            }
            R.id.etGarageAddress -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ADDRESS_FRAGMENT, null),
                    target = R.id.fragment_container_2)
            }
        }
    }

    private fun openImagePicker() {
        TedPermission.create().setPermissionListener(object : PermissionListener {
            override fun onPermissionGranted() {
                TedImagePicker.with(requireContext()).showCameraTile(true).start {
                    onImagePicked(it)
                }
            }

            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
                CommonUtils.showToast(context, resources.getString(R.string.permission_denied))
            }

        }).setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .check()

    }

    private fun copyIdOnClick() {
        if (binding.tvWorkShopID.text.toString().isNotEmpty()) {
            val clipboardManager =
                requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText("text", binding.tvWorkShopID.text.toString())
            clipboardManager.setPrimaryClip(clipData)
            CommonUtils.showToast(requireContext(), getString(R.string.text_coppied))
        } else {
            CommonUtils.showToast(requireContext(), getString(R.string.id_not_available))
        }
    }

    private fun formCheck(): Boolean {
        addWorkshopRequest = AddWorkshopRequest()
        addWorkshopRequest.ownerId =
            viewModel.getStringSharedPreference(AppENUM.USER_ID, "").toLong()
        return when {
            binding.etGarageName.text?.isEmpty() == true -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.empty_workshop_name),
                    true)
                false
            }
            binding.etInvoiceName.text?.isEmpty() == true -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.empty_invoice_name),
                    true)
                false
            }
            binding.etOwnerName.text?.isEmpty() == true -> {
                CommonUtils.showToast(requireContext(), getString(R.string.empty_owner_name), true)
                false
            }
            binding.etGarageAddress.text?.isEmpty() == true -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.empty_workshop_address),
                    true)
                false
            }
            else -> {
                addWorkshopRequest.apply {
                    workshopName = binding.etGarageName.text.toString()
                    ownerName = binding.etOwnerName.text.toString()
                    address = binding.etGarageAddress.text.toString()
                    mapLink = binding.etGoogleMapLink.text.toString()
                    gstIn = binding.etGSTNumber.text.toString()
                    invoiceName = binding.etInvoiceName.text.toString()
                    types =
                        viewModel.workshopModel.types ?: AppENUM.RefactoredStrings.WORKSHOP_CONSTANT
                    if (binding.etCity.text.toString().trim().isEmpty()) {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.please_enter_city),
                            true)
                        binding.etCity.requestFocus()
                        return false
                    }
                    if (binding.etState.text.toString().trim().isEmpty()) {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.please_enter_state),
                            true)
                        binding.etState.requestFocus()
                        return false
                    }
                    if (binding.etPincode.text.toString().trim().isEmpty()) {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.please_enter_pincode),
                            true)
                        binding.etPincode.requestFocus()
                        return false
                    }
                    if (::selectedLocation.isInitialized) {
                        latitude = selectedLocation.latitude
                        longitude = selectedLocation.longitude
                        try {
                            val gcd = Geocoder(requireContext(), Locale.getDefault())
                            val addresses: List<Address>? =
                                gcd.getFromLocation(selectedLocation.latitude,
                                    selectedLocation.longitude,
                                    1)
                            if (addresses?.isNotEmpty() == true) {
                                this.city = binding.etCity.text.toString()
                                this.state = binding.etState.text.toString()
                                this.country = addresses[0].countryName
                                this.pin = binding.etPincode.text.toString()
                            } else { // not found
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
                true
            }
        }
    }

    private fun onImagePicked(imageUri: Uri) {
        profilePicture = imageUri
        loadCircularImage(profilePicture.toString(), binding.imgWorkshopImage)
    }


    @SuppressLint("SetTextI18n")
    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is LocationSelectedEvent -> {
                (event.extra as? AddressData)?.let {
                    binding.etGarageAddress.setText(it.fullAddress)
                    binding.etGoogleMapLink.setText("${AppENUM.MAP_URL}${it.latitude},${it.longitude}")
                    binding.etState.setText(CommonUtils.getStateFromLocation(requireContext(),
                        it.latitude ?: 0.0,
                        it.longitude ?: 0.0))
                    binding.etCity.setText(CommonUtils.getCityFromLocation(requireContext(),
                        it.latitude ?: 0.0,
                        it.longitude ?: 0.0))
                    binding.etPincode.setText(CommonUtils.getPinCodeFromLocation(requireContext(),
                        it.latitude ?: 0.0,
                        it.longitude ?: 0.0))
                    selectedLocation = LatLng(it.latitude ?: 0.0, it.longitude ?: 0.0)
                }
            }
        }
    }
}