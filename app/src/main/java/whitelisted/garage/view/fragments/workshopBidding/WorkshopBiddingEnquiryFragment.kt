package whitelisted.garage.view.fragments.workshopBidding

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.BrandWiseItem
import whitelisted.garage.api.request.GenericItem
import whitelisted.garage.api.response.GetWsBidEnquiryResponse
import whitelisted.garage.api.response.WsAccRejBid
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentWorkshopBidEnquiryBinding
import whitelisted.garage.eventBus.RefreshWsBidList
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.WorkBidEnquireBrandListAdapter
import whitelisted.garage.view.adapter.WorkBidEnquireGenericListAdapter
import whitelisted.garage.viewmodels.WorkShopBiddingViewModel

class WorkshopBiddingEnquiryFragment : BaseFragment() {

    private val binding by viewBinding(FragmentWorkshopBidEnquiryBinding::inflate)
    private val viewModel: WorkShopBiddingViewModel by sharedViewModel()
    private var isViewOly = false
    private var bidId = ""
    private var isBidRejected = false
    private var wsBidEnquiryResponse: GetWsBidEnquiryResponse? = null
    private val partsAvailabilityList = mutableListOf<String>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()

    }

    private fun setupScreen() {
        clickListener()
        observeResponses()
        getDataFromArgs()
    }

    private fun getDataFromArgs() {
        partsAvailabilityList.addAll(resources.getStringArray(R.array.part_availability_list))
        arguments?.let {
            bidId = it.getString(AppENUM.ID, "")
            isViewOly = it.getBoolean(AppENUM.IS_VIEW_ONLY, false)
        }
        showLoader()
        viewModel.getWsBidEnquiry(bidId)
    }

    private fun observeResponses() {
        observeGetWsBidEnquiry()
        observeChangedStatus()
        setUnlockPhoneObserver()
    }

    private fun observeChangedStatus() {
        viewModel.provideChangeBidStatusResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.let {
                        if (isBidRejected) {
                            firebaseAcceptRejBidEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_REJECT_BID)
                            viewModel.setRefreshReceivedBidList(true)
                            showLoader()
                            viewModel.getWsBidEnquiry(bidId)

                        } else {
                            firebaseAcceptRejBidEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_ACCEPT_BID)
                            EventBus.getDefault().post(RefreshWsBidList(wsBidEnquiryResponse))
                        }
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
    }

    private fun observeGetWsBidEnquiry() {
        viewModel.provideWsBidEnquiry().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.let { data ->
                        setDataToGenericAdapter(data.genericItems)
                        setDataToBrandAdapter(data.brandWiseItems)
                        wsBidEnquiryResponse = data
                        setDataToScreen()
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setDataToScreen() = binding.apply {
        wsBidEnquiryResponse?.let {
            tvRetailerName.text = it.workshopName
            tvDistance.text = "${it.distance} ${getString(R.string.km_away)}"
            if (wsBidEnquiryResponse?.isAccepted == true) {
                llAcceptedBid.makeVisible(true)
                llRejectedBid.makeVisible(false)
                llAcceptRejectLayout.makeVisible(false)
            } else if (wsBidEnquiryResponse?.isCanceled == true) {
                llAcceptedBid.makeVisible(false)
                llRejectedBid.makeVisible(true)
                llAcceptRejectLayout.makeVisible(false)
            } else {
                llAcceptedBid.makeVisible(false)
                llRejectedBid.makeVisible(false)
                llAcceptRejectLayout.makeVisible(true)
            }
            if (it.isContactNumber?.contains(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                    "")) == true) {
                clgMobileMasked.makeVisible(false)
                clgDirection.makeVisible(true)
            } else {
                clgMobileMasked.makeVisible(true)
                clgDirection.makeVisible(false)
                var maskedPhone: String?
                val initialFour = it.contactNumber?.substring(0, 4)
                val noOfStars = (it.contactNumber?.length ?: 10).minus(4)
                maskedPhone = initialFour
                if (noOfStars > 0) {
                    for (i in 1..(noOfStars)) {
                        maskedPhone += "*"
                    }
                }
                tvPhoneNumberMasked.text = maskedPhone
            }
        }
    }

    private fun setDataToBrandAdapter(brandWiseItems: List<BrandWiseItem>?) {
        if (brandWiseItems?.isNotEmpty() == true) {
            binding.rvEnquiryBrand.adapter =
                WorkBidEnquireBrandListAdapter(brandWiseItems, this, partsAvailabilityList)
        }
    }

    private fun setDataToGenericAdapter(genericItems: List<GenericItem>?) {
        if (genericItems?.isNotEmpty() == true) {

            binding.rvEnquiryGeneric.adapter =
                WorkBidEnquireGenericListAdapter(genericItems, this, partsAvailabilityList)
        }
    }

    private fun clickListener() {
        binding.imgBack.setOnClickListener(this)
        binding.imgDirection.setOnClickListener(this)
        binding.imgCall.setOnClickListener(this)
        binding.btnReject.setOnClickListener(this)
        binding.btnAccept.setOnClickListener(this)
        binding.llUnlockMobile.setOnClickListener(this)
    }

    private fun setUnlockPhoneObserver() {
        viewModel.provideUnlockContactResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_UNLOCK_CONTACT,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_WORKSHOP_BID_ENQUIRY)
                    viewModel.setRefreshReceivedBidList(true)
                    showLoader()
                    viewModel.getWsBidEnquiry(bidId)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
            R.id.imgCall -> {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:${wsBidEnquiryResponse?.contactNumber}")
                startActivity(intent)
            }
            R.id.imgDirection -> {
                val mapLik =
                    "${AppENUM.MAP_URL}${wsBidEnquiryResponse?.latitude},${wsBidEnquiryResponse?.longitude}"
                openWebPage(mapLik)
            }
            R.id.btnAccept -> {
                onAcceptWsBid()
            }
            R.id.btnReject -> {
                onRejectWsBid()
            }
            R.id.llUnlockMobile -> {
                showConfirmationDialogForUnlockMobile()
            }
            R.id.img1, R.id.img2, R.id.img3, R.id.img4 -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    CommonUtils.genericCastOrNull<List<String>>(v.getTag(R.id.model))?.let { list ->
                        imagePreview(v, Uri.parse(list[position]))
                    }
                }
            }
            R.id.tvMorePhotos -> {
                CommonUtils.genericCastOrNull<List<String>>(v.getTag(R.id.model))?.let { list ->
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.INVENTORY_IMAGE_PREVIEW,
                            Bundle().apply {
                                this.putStringArrayList(AppENUM.IMAGE_URL, ArrayList(list))
                            }))
                }
            }

        }
    }

    private fun showConfirmationDialogForUnlockMobile() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
            Bundle().apply {
                putString(AppENUM.TITLE, getString(R.string.unlock_contact_number))
                putString(AppENUM.DESCRIPTION,
                    getString(R.string.unlock_this_contact_number_using_10_gocoins))
                putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.okay))
                putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.cancel))
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    if (extra as? Boolean == true) {
                        showLoader()
                        viewModel.unlockPhoneWorkshop(wsBidEnquiryResponse?.id ?: "")
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun onRejectWsBid() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
            Bundle().apply {
                putString(AppENUM.TITLE, getString(R.string.reject))
                putString(AppENUM.DESCRIPTION, getString(R.string.sure_wannt_to_reject_bid))
                putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.yes))
                putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.no))
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    if (extra as Boolean) {
                        isBidRejected = true
                        showLoader()
                        viewModel.changeStatusOfWsBid(bidId,
                            WsAccRejBid(AppENUM.WSBiddingStatusConstant.REJECTED))
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun firebaseAcceptRejBidEvent(eventName: String) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_WORKSHOP_BID_ENQUIRY)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.RETAILER_NAME,
                wsBidEnquiryResponse?.workshopName)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.RETAILER_LOC,
                wsBidEnquiryResponse?.workshopAddress)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.RETAILER_NUMBER,
                wsBidEnquiryResponse?.contactNumber)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.TOTAL_COST,
                wsBidEnquiryResponse?.totalAmount)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.AVL_ITEMS,
                wsBidEnquiryResponse?.totalAvailableAmount.toString())
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.UN_AVL_ITEMS,
                (((wsBidEnquiryResponse?.totalItem
                    ?: 0) - (wsBidEnquiryResponse?.totalAvailableAmount ?: 0)).toString()))
            FirebaseAnalyticsLog.trackFireBaseEventLog(eventName, this)
        }
    }

    private fun onAcceptWsBid() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
            Bundle().apply {
                putString(AppENUM.TITLE, getString(R.string.accept))
                putString(AppENUM.DESCRIPTION, getString(R.string.sure_wannt_to_acc_bid))
                putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.yes))
                putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.no))
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    if (extra as Boolean) {
                        isBidRejected = false
                        showLoader()
                        viewModel.changeStatusOfWsBid(bidId,
                            WsAccRejBid(AppENUM.WSBiddingStatusConstant.ACCEPTED))
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun openWebPage(url: String?) {
        val webpage = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, webpage)
        if (intent.resolveActivity(requireActivity().packageManager) != null) {
            startActivity(intent)
        }
    }
}