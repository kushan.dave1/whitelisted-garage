package whitelisted.garage.view.fragments.orderDetail

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.PopupMenu
import androidx.appcompat.view.ContextThemeWrapper
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.data.CarNameOrderIdStatusModel
import whitelisted.garage.api.data.SetOptionsImgStatusObject
import whitelisted.garage.api.response.OrderDetailResponse
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentOrderDetailWrapperBinding
import whitelisted.garage.eventBus.*
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.view.adapter.OrderDetailTabLayoutAdapter
import whitelisted.garage.view.adapter.RetailerOrderDetailTabLayoutAdapter
import whitelisted.garage.viewmodels.OrderDetailWrapperViewModel

class OrderDetailWrapperFragment : BaseFragment() {

    private val binding by viewBinding(FragmentOrderDetailWrapperBinding::inflate)
    private val viewModel: OrderDetailWrapperViewModel by viewModel()
    private lateinit var tlAdapter: OrderDetailTabLayoutAdapter
    private lateinit var tlRetailAdapter: RetailerOrderDetailTabLayoutAdapter
    private var pdfFile: String? = ""
    private var pdfFile2: String? = ""
    private var selectedTabPosition = 0
    private var selectTab = -1

    companion object {
        fun provideOrderDetails(): OrderDetailResponse? {
            return if (Companion::orderDetails.isInitialized) {
                orderDetails
            } else null
        }

        var isNewOrder: Boolean = false
        var orderId: String = ""
        var orderStatusId = 0
        var retailWorkshopName = ""
        lateinit var orderDetails: OrderDetailResponse
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        binding.imgMore.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)

        isNewOrder = arguments?.getBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, false) ?: false
        orderId = arguments?.getString(AppENUM.IntentKeysENUM.ORDER_ID) ?: ""

        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                selectTab = arguments?.getInt(AppENUM.IntentKeysENUM.SELECT_TAB, -1) ?: -1
                setupTabs()

            }
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                selectTab = arguments?.getInt(AppENUM.IntentKeysENUM.SELECT_TAB, -1) ?: -1
                setupTabs()

            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                selectTab = arguments?.getInt(AppENUM.IntentKeysENUM.SELECT_TAB, -1) ?: -1
                setupTabsForRetailer()

                //                binding.tvOrderStatus.visibility = View.GONE
                //                binding.imgMore.visibility = View.GONE
            }
        }
        if (orderId.isNotEmpty()) {
            viewModel.getOrderDetail(orderId)
            observeOrderDetailResponse()
        }

        observeCancelOrder()
    }

    @SuppressLint("WrongConstant")
    private fun setupTabsForRetailer() {
        tlRetailAdapter = RetailerOrderDetailTabLayoutAdapter(requireActivity())
        binding.vpOrderStatus.adapter = tlRetailAdapter
        binding.vpOrderStatus.offscreenPageLimit = ViewPager2.SCREEN_STATE_ON
        TabLayoutMediator(binding.tlOrderStatus, binding.vpOrderStatus) { tab, position ->
            run {
                when (position) {
                    0 -> tab.text = getString(R.string.order_details)
                    1 -> tab.text = getString(R.string.order_inclusions)
                    2 -> tab.text = getString(R.string.payment)
                    else -> Log.d("FragmentError", "Not found")
                }
            }
        }.attach() //        binding.tlOrderStatus.setupWithViewPager(binding.vpOrderStatus)
        //        binding.tlOrderStatus.addOnTabSelectedListener(requireActivity())

        binding.vpOrderStatus.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                tlRetailAdapter.updateFragment(position)
            }
        })

        if (isNewOrder) {
            Handler(Looper.getMainLooper()).postDelayed({
                binding.tlOrderStatus.selectTab(binding.tlOrderStatus.getTabAt(1))
            }, 200)
        }

        Handler(Looper.getMainLooper()).postDelayed({
            if (selectTab >= 0) {
                binding.tlOrderStatus.selectTab(binding.tlOrderStatus.getTabAt(selectTab))
            }
        }, 500)

    }

    private fun observeOrderDetailResponse() {
        viewModel.provideOrderDetailResponse().observe(requireActivity()) {
            when (it) {
                is Result.Success -> {
                    orderDetails = it.body.data
                    orderStatusId = it.body.data.statusId
                    if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.RETAILER_CONSTANT) {
                        when (it.body.data.statusId) {
                            AppENUM.RefactoredStrings.OPEN_ORDER_STATUS, AppENUM.RefactoredStrings.OPEN_ORDER_STATUS_2 -> setDetails(
                                it.body.data.customerDetails.name,
                                it.body.data.orderId,
                                getString(R.string.open_order))
                            AppENUM.RefactoredStrings.WIP_ORDER_STATUS -> setDetails(it.body.data.customerDetails.name,
                                it.body.data.orderId,
                                getString(R.string.wip_order))
                            AppENUM.RefactoredStrings.READY_ORDER_STATUS -> setDetails(it.body.data.customerDetails.name,
                                it.body.data.orderId,
                                getString(R.string.ready_order))
                            AppENUM.RefactoredStrings.COMPLETED_ORDER_STATUS -> setDetails(it.body.data.customerDetails.name,
                                it.body.data.orderId,
                                getString(R.string.completed))
                        }
                    } else {
                        if (it.body.data.carDetails != null) {
                            when (it.body.data.statusId) {
                                AppENUM.RefactoredStrings.OPEN_ORDER_STATUS, AppENUM.RefactoredStrings.OPEN_ORDER_STATUS_2 -> setDetails(
                                    it.body.data.carDetails.car ?: "",
                                    it.body.data.orderId,
                                    getString(R.string.open_order))
                                AppENUM.RefactoredStrings.WIP_ORDER_STATUS -> setDetails(it.body.data.carDetails.car
                                    ?: "", it.body.data.orderId, getString(R.string.wip_order))
                                AppENUM.RefactoredStrings.READY_ORDER_STATUS -> setDetails(it.body.data.carDetails.car
                                    ?: "", it.body.data.orderId, getString(R.string.ready_order))
                                AppENUM.RefactoredStrings.COMPLETED_ORDER_STATUS -> setDetails(it.body.data.carDetails.car
                                    ?: "", it.body.data.orderId, getString(R.string.completed))
                            }
                        }
                    }
                    if (selectTab == 1) {
                        if (::tlAdapter.isInitialized) {
                            tlAdapter.updateFragment(selectTab)
                        }
                    }
                }
                is Result.Failure -> { //                    CommonUtils.showToast(requireActivity(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeCancelOrder() {
        viewModel.provideCancelOrderResponse().observe(requireActivity()) {
            showLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireActivity(), it.body.message, true)
                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                    popBackStackAndHideKeyboard()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireActivity(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setDetails(carName: String, orderId: String, status: String) {
        binding.tvCarBrand.text = carName
        "${getString(R.string.order_id_hyphen)} $orderId".apply {
            binding.tvOrderId.text = this
        }
        binding.tvOrderStatus.text = status
    }

    private fun setOptionsImgStatus(tabPosition: Int, pdfFile: String = "", pdfFile2: String = "") {
        this.pdfFile = pdfFile
        this.pdfFile2 = pdfFile2
        selectedTabPosition = tabPosition
        if (selectedTabPosition == 1 && pdfFile.isEmpty()) {
            binding.imgMore.visibility = View.INVISIBLE
        } else {
            if (selectedTabPosition == 0) {
                if (orderStatusId < AppENUM.RefactoredStrings.WIP_ORDER_STATUS) binding.imgMore.visibility =
                    View.VISIBLE
                else binding.imgMore.visibility = View.INVISIBLE
            } else {
                if (pdfFile.isNotEmpty()) binding.imgMore.visibility = View.VISIBLE
                else binding.imgMore.visibility = View.INVISIBLE
            }
        }
    }

    private fun setOptionsImgStatusForRetailer(tabPosition: Int,
        pdfFile: String = "",
        pdfFile2: String = "") {
        this.pdfFile = pdfFile
        this.pdfFile2 = pdfFile2
        selectedTabPosition = tabPosition
        if (selectedTabPosition == 2 && pdfFile.isEmpty()) binding.imgMore.visibility =
            View.INVISIBLE
        else if (selectedTabPosition == 1) {
            binding.imgMore.visibility = View.VISIBLE
        } else {
            if (selectedTabPosition == 0) {
                if (orderStatusId < AppENUM.RefactoredStrings.WIP_ORDER_STATUS) binding.imgMore.visibility =
                    View.VISIBLE
                else binding.imgMore.visibility = View.INVISIBLE
            } else binding.imgMore.visibility = View.VISIBLE
        }
    }

    private fun updateOrderStatus(status: Int) {
        when (status) {
            AppENUM.RefactoredStrings.OPEN_ORDER_STATUS, AppENUM.RefactoredStrings.OPEN_ORDER_STATUS_2 -> binding.tvOrderStatus.text =
                getString(R.string.open_order)
            AppENUM.RefactoredStrings.READY_ORDER_STATUS -> binding.tvOrderStatus.text =
                getString(R.string.ready_order)
            AppENUM.RefactoredStrings.WIP_ORDER_STATUS -> binding.tvOrderStatus.text =
                getString(R.string.wip_order)
            AppENUM.RefactoredStrings.COMPLETED_ORDER_STATUS -> binding.tvOrderStatus.text =
                getString(R.string.completed)
        }

    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()

            R.id.imgMore -> {
                showPopupMenu(binding.imgMore, selectedTabPosition)
            }

            //            R.id.btnDownload -> {
            //                val url = pdfFile
            //                val i = Intent(Intent.ACTION_VIEW)
            //                i.data = Uri.parse(url)
            //                startActivity(i)
            //            }
        }
    }

    private fun showPopupMenu(view: View, tabPosition: Int) {
        var option1 = ""
        var option2 = ""
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.RETAILER_CONSTANT) {
            when (tabPosition) {
                0 -> {
                    if (orderStatusId < AppENUM.RefactoredStrings.WIP_ORDER_STATUS) option1 =
                        getString(R.string.cancel_order)
                }
                1 -> {
                    if (pdfFile?.isNotEmpty() == true) {
                        option1 = getString(R.string.download_order_estimates)
                    } else {
                        if (orderStatusId < AppENUM.RefactoredStrings.WIP_ORDER_STATUS) option1 =
                            getString(R.string.cancel_order)
                    }
                }
                2 -> {
                    if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                            AppENUM.RefactoredStrings.defaultCurrency) == AppENUM.RefactoredStrings.defaultCurrency) {
                        option1 = if (pdfFile?.isNotEmpty() == true) {
                            getString(R.string.download_gst_invoice)
                        } else ""
                        option2 = if (pdfFile2?.isNotEmpty() == true) {
                            getString(R.string.download_non_gst_invoice)
                        } else ""
                    } else {
                        option1 = if (pdfFile?.isNotEmpty() == true) {
                            getString(R.string.download_invoice)
                        } else "" //                        option2 = if (pdfFile2?.isNotEmpty() == true) {
                        //                            getString(R.string.download_non_gst_invoice)
                        //                        } else ""
                    }
                }
            }
        } else {
            when (tabPosition) {
                0 -> {
                    if (orderStatusId < AppENUM.RefactoredStrings.WIP_ORDER_STATUS) option1 =
                        getString(R.string.cancel_order)
                }
                1 -> { //                    if (pdfFile?.isNotEmpty() == true) {
                    //                        option1 = getString(R.string.download_job_card)
                    //                        if (orderStatusId < AppENUM.RefactoredStrings.WIP_ORDER_STATUS)
                    //                            option2 = getString(R.string.cancel_order)
                    //                    } else {
                    //                        if (orderStatusId < AppENUM.RefactoredStrings.WIP_ORDER_STATUS)
                    //                            option1 = getString(R.string.cancel_order)
                    //                    }
                    if (pdfFile?.isNotEmpty() == true) {
                        option1 = getString(R.string.download_job_card)
                        if (pdfFile2?.isNotEmpty() == true) {
                            option2 = getString(R.string.download_order_estimates)
                        }
                    } else {
                        if (orderStatusId < AppENUM.RefactoredStrings.WIP_ORDER_STATUS) option1 =
                            getString(R.string.cancel_order)
                    }
                }
                2 -> {
                    if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                            AppENUM.RefactoredStrings.defaultCurrency) == AppENUM.RefactoredStrings.defaultCurrency) {
                        option1 = if (pdfFile?.isNotEmpty() == true) {
                            getString(R.string.download_gst_invoice)
                        } else ""
                        option2 = if (pdfFile2?.isNotEmpty() == true) {
                            getString(R.string.download_non_gst_invoice)
                        } else ""
                    } else {
                        option1 = if (pdfFile?.isNotEmpty() == true) {
                            getString(R.string.download_invoice)
                        } else "" //                        option2 = if (pdfFile2?.isNotEmpty() == true) {
                        //                            getString(R.string.download_non_gst_invoice)
                        //                        } else ""
                    }
                } //                4 -> {
                //                    option1 = if (pdfFile?.isNotEmpty() == true) {
                //                        getString(R.string.download_invoice)
                //                    } else ""
                //                }
            }
        }
        val wrapper = ContextThemeWrapper(requireActivity(), R.style.BasePopupMenu)
        val popupMenu = PopupMenu(wrapper, view)
        popupMenu.menuInflater.inflate(R.menu.empty_menu, popupMenu.menu)
        if (option1.isNotEmpty()) {
            popupMenu.menu.add(option1)
        }
        if (option2.isNotEmpty()) popupMenu.menu.add(option2)
        if (popupMenu.menu.size() > 0) {
            popupMenu.setOnMenuItemClickListener { item ->
                when (item.title) {
                    getString(R.string.cancel_order) -> {
                        showLoader()
                        viewModel.cancelOrder(orderId)
                    } //                    getString(R.string.download_job_card) -> {
                    //                        if (!pdfFile.isNullOrEmpty()) {
                    //                            val url = pdfFile
                    //                            val i = Intent(Intent.ACTION_VIEW)
                    //                            i.data = Uri.parse(url)
                    //                            startActivity(i)
                    //                        }
                    //                    }
                    getString(R.string.download_order_estimates) -> {
                        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.RETAILER_CONSTANT) {
                            if (!pdfFile.isNullOrEmpty()) {
                                val url = pdfFile
                                val i = Intent(Intent.ACTION_VIEW)
                                i.data = Uri.parse(url)
                                startActivity(i)
                            }
                        } else {
                            if (!pdfFile2.isNullOrEmpty()) {
                                val url = pdfFile2
                                val i = Intent(Intent.ACTION_VIEW)
                                i.data = Uri.parse(url)
                                startActivity(i)
                            }
                        }
                    }
                    getString(R.string.download_non_gst_invoice) -> {
                        if (!pdfFile2.isNullOrEmpty()) {
                            val url = pdfFile2
                            val i = Intent(Intent.ACTION_VIEW)
                            i.data = Uri.parse(url)
                            startActivity(i)
                        }
                    }
                    else -> {
                        if (!pdfFile.isNullOrEmpty()) {
                            val url = pdfFile
                            val i = Intent(Intent.ACTION_VIEW)
                            i.data = Uri.parse(url)
                            startActivity(i)
                        }
                    }
                }
                true
            }
            popupMenu.show()
        }
    }

    @SuppressLint("WrongConstant")
    private fun setupTabs() {
        tlAdapter = OrderDetailTabLayoutAdapter(requireActivity())
        binding.vpOrderStatus.adapter = tlAdapter
        binding.vpOrderStatus.offscreenPageLimit = ViewPager2.SCREEN_STATE_ON
        TabLayoutMediator(binding.tlOrderStatus, binding.vpOrderStatus) { tab, position ->
            run {
                when (position) {
                    0 -> tab.text = getString(R.string.order_details)
                    1 -> tab.text =
                        getString(R.string.order_estimates) //                    2 -> tab.text = getString(R.string.job_card)
                    2 -> tab.text =
                        getString(R.string.payment) //                    3 -> tab.text = getString(R.string.order_estimates)
                    //                    3 -> tab.text = getString(R.string.payment)
                    //                    4 -> tab.text = getString(R.string.payment)
                    else -> Log.d("FragmentError", "Not found")
                }
            }
        }.attach() //        binding.tlOrderStatus.setupWithViewPager(binding.vpOrderStatus)
        //        binding.tlOrderStatus.addOnTabSelectedListener(requireActivity())

        binding.vpOrderStatus.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (position == 2) { //2 when job card is hidden
                    Handler(Looper.getMainLooper()).postDelayed({
                        binding.tlOrderStatus.selectTab(binding.tlOrderStatus.getTabAt(2)) //2 when job card is hidden
                    }, 500)
                }
                tlAdapter.updateFragment(position)
            }
        })

        if (isNewOrder) {
            Handler(Looper.getMainLooper()).postDelayed({
                binding.tlOrderStatus.selectTab(binding.tlOrderStatus.getTabAt(1))
            }, 200)
        }

        Handler(Looper.getMainLooper()).postDelayed({
            if (selectTab >= 0) {
                binding.tlOrderStatus.selectTab(binding.tlOrderStatus.getTabAt(selectTab))
            }
        }, 500)
    }

    private fun selectTab(position: Int) {
        binding.tlOrderStatus.selectTab(binding.tlOrderStatus.getTabAt(position))
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is SetOrderDetailsEvent -> {
                val carNameOrderIdStatusModel: CarNameOrderIdStatusModel? =
                    event.extra as? CarNameOrderIdStatusModel
                carNameOrderIdStatusModel.let {
                    setDetails(it?.carName ?: "", it?.orderId ?: "", it?.status ?: "")
                }
            }

            is GetOrderDetailsEvent -> {
                viewModel.getOrderDetail(orderId)
            }

            is UpdateOrderStatusEvent -> {
                updateOrderStatus(event.str.toInt())
            }

            is SetOptionsImageStatusEvent -> {
                if (event.str == "0") {
                    (event.extra as? SetOptionsImgStatusObject).let {
                        setOptionsImgStatus(it?.tabPosition ?: 0,
                            it?.pdfFile ?: "",
                            it?.pdfFile2 ?: "")
                    }
                } else {
                    (event.extra as? SetOptionsImgStatusObject).let {
                        setOptionsImgStatusForRetailer(it?.tabPosition ?: 0,
                            it?.pdfFile ?: "",
                            it?.pdfFile2 ?: "")
                    }
                }
            }

            is SelectTabEvent -> {
                selectTab(event.str.toInt())
            }
        }
    }

}