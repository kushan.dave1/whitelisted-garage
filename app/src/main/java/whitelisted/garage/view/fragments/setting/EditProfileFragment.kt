package whitelisted.garage.view.fragments.setting

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.AddressData
import whitelisted.garage.api.request.ProfileNavigationData
import whitelisted.garage.api.request.SaveProfileCompletionRequest
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentEditProfileBinding
import whitelisted.garage.eventBus.LocationSelectedEvent
import whitelisted.garage.eventBus.ProfileUpdateEvent
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.viewmodels.ProfileFragmentViewModel
import java.util.*

class EditProfileFragment : BaseFragment() {

    private val binding by viewBinding(FragmentEditProfileBinding::inflate)
    private val viewModel: ProfileFragmentViewModel by viewModel()
    private var profileImage = ""
    private var profileImageFirebaseUrl = ""
    private var isImageUploaded = false
    private val today: Calendar = Calendar.getInstance()
    private var data: ProfileNavigationData? = null
    private var googleMapLink = ""
    private var selectedLocation = LatLng(0.0, 0.0)

    private var startDate = "${today.get(Calendar.DAY_OF_MONTH)}-${today.get(Calendar.MONTH) + 1}-${
        today.get(Calendar.YEAR)
    }"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListeners()
        getDataFromArgs()
        setUpUi()
        observeData()
    }

    private fun getDataFromArgs() {
        arguments?.let {
            data = it.getParcelable(AppENUM.Profile.ROLE)
        }
    }

    private fun clickListeners() {
        binding.backArrowImg.setOnClickListener(this)
        binding.saveProfileBtn.setOnClickListener(this)
        binding.llJoiningDate.setOnClickListener(this)
        binding.etJoiningDate.setOnClickListener(this)
        binding.etGarageAddress.setOnClickListener(this)
    }

    private fun setUpUi() = binding.apply {
        data?.let { data ->
            tvHeader.text = data.header
            when (data.constant) {
                AppENUM.Profile.OWNER_NAME, AppENUM.Profile.EMAIL, AppENUM.Profile.WORKSHOP_NAME, AppENUM.Profile.GSTN -> {
                    tvName.makeVisible(true)
                    llName.makeVisible(true)
                    tvName.text = data.header
                    etName.hint = data.header
                    if (data.value?.length == 1) {
                        if (data.value.toString().contains("-")) etName.setText("")
                        else etName.setText(data.value)
                    } else {
                        etName.setText(data.value)
                    }

                }
                AppENUM.Profile.JOINING_DATE -> {
                    tvJoining.makeVisible(true)
                    llJoiningDate.makeVisible(true)
                    tvJoining.text = data.header
                    if (data.value?.length == 1) {
                        if (data.value.toString().contains("-")) etJoiningDate.setText("")
                        else etJoiningDate.setText(data.value)
                    } else {
                        etJoiningDate.setText(data.value)
                    }
                }
                AppENUM.Profile.PHONE_NUMBER -> {
                    tvPhone.makeVisible(true)
                    llMobile.makeVisible(true)
                    tvPhone.text = data.header
                    if (data.value?.length == 1) {
                        if (data.value.toString().contains("-")) etMobile.setText("")
                        else etMobile.setText(data.value)
                    } else {
                        etMobile.setText(data.value)
                    }
                }
                AppENUM.Profile.WORKSHOP_ADD -> {
                    llAddress.makeVisible(true)
                    lblGarageAddress.text = data.header
                    if (data.value?.length == 1) {
                        if (data.value.toString().contains("-")) etGarageAddress.setText("")
                        else etGarageAddress.setText(data.value)
                    } else {
                        etGarageAddress.setText(data.value)
                    }
                }
            }
        }
    }

    private fun observeData() {
        viewModel.provideGetOtpResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success<*> -> {
                    it.body.let { data ->
                        val result = CommonUtils.genericCastOrNull<ServerResponse<String>>(data)
                        showVerifyOTPDialog(binding.etMobile.text.toString(), result?.data)
                    }
                }
                is Result.Failure<*> -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
        viewModel.provideUpdateProfileResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success<*> -> {
                    it.body.let {
                        EventBus.getDefault().post(ProfileUpdateEvent())
                        popBackStackAndHideKeyboard()
                    }
                }
                is Result.Failure<*> -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
        viewModel.provideUploadWorkshopImageResponse()
            .observe(viewLifecycleOwner) { sendSuccessUrl ->
                sendSuccessUrl.isSuccessFailure?.let { it ->
                    isImageUploaded = false
                    if (it) {
                        sendSuccessUrl.url?.let {
                            profileImageFirebaseUrl = it
                        }
                    } else {
                        sendSuccessUrl.url?.let { it1 ->
                            CommonUtils.showToast(requireContext(), it1, true)
                        }
                    }
                }
            }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.backArrowImg -> popBackStackAndHideKeyboard()
            R.id.saveProfileBtn -> {
                saveProfile()
            }
            R.id.llJoiningDate, R.id.etJoiningDate -> showSelectDateDialog()
            R.id.imgProfilePicture -> {
                ImagePicker.with(this)
                    .compress(512)         //Final image size will be less than 1 MB(Optional)
                    .maxResultSize(512, 512).galleryMimeTypes(  //Exclude gif images
                        mimeTypes = arrayOf("image/png", "image/jpg", "image/jpeg"))
                    .setImageProviderInterceptor {
                        showLoader()
                    }.createIntent { intent ->
                        startForProfileImageResult.launch(intent)
                    }
            }
            R.id.etGarageAddress -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ADDRESS_FRAGMENT, null),
                    target = R.id.fragment_container_2)
            }
        }
    }

    private fun saveProfile() = binding.apply {
        when (data?.constant) {
            AppENUM.Profile.OWNER_NAME -> {
                if (validateFieldsName()) {
                    showLoader()
                    viewModel.updateProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID),
                        SaveProfileCompletionRequest().apply {
                            this.name = etName.text.toString()
                        })
                }
            }
            AppENUM.Profile.WORKSHOP_NAME -> {
                if (validateFieldsName()) {
                    showLoader()
                    viewModel.updateProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID),
                        SaveProfileCompletionRequest().apply {
                            this.workshopName = etName.text.toString()
                        })
                }
            }
            AppENUM.Profile.EMAIL -> {
                if (validateFieldsName()) {
                    showLoader()
                    viewModel.updateProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID),
                        SaveProfileCompletionRequest().apply {
                            this.email = etName.text.toString()
                        })
                }
            }
            AppENUM.Profile.GSTN -> {
                lifecycleScope.launch {
                    showLoader()
                    if (binding.etName.text.toString().isNotEmpty()) {
                        val verifyGSTResponse =
                            viewModel.checkGST(binding.etName.text.toString())
                        verifyGSTResponse?.let {
                            if ((it.gstinStatus ?: 0) == 1) {
                                showLoader()
                                viewModel.updateProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID),
                                    SaveProfileCompletionRequest().apply {
                                        this.gstin = etName.text.toString()
                                    })
                            } else {
                                hideLoader()
                                CommonUtils.showToast(requireContext(), it.message ?: "")
                            }
                        } ?: run {
                            hideLoader()
                            CommonUtils.showToast(requireContext(),
                                getString(R.string.error_invalid_gstt))
                        }
                    }
                }
            }
            AppENUM.Profile.JOINING_DATE -> {
                if (validateJoiningDate()) {
                    showLoader()
                    viewModel.updateProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID),
                        SaveProfileCompletionRequest().apply {
                            this.joiningDate = etJoiningDate.text.toString()
                        })
                }
            }
            AppENUM.Profile.PHONE_NUMBER -> {
                if (validateMobileFieldsName()) {
                    showLoader()
                    viewModel.callGetOTPAPI(etMobile.text.toString())
                }
            }
            AppENUM.Profile.WORKSHOP_ADD -> {
                if (validateFieldsAddress()) {
                    showLoader()
                    viewModel.updateProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID),
                        SaveProfileCompletionRequest().apply {
                            this.address = etGarageAddress.text.toString()
                            this.mapLink = googleMapLink
                            this.latitude = selectedLocation.latitude
                            this.longitude = selectedLocation.longitude
                        })
                    viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_ADDRESS,
                        etGarageAddress.text.toString())
                }
            }
        }
    }

    fun hitAPIAfterVerifyOtp() {
        showLoader()
        viewModel.updateProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID),
            SaveProfileCompletionRequest().apply {
                this.mobile = binding.etMobile.text.toString()
            })
    }

    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            hideLoader()
            val resultCode = result.resultCode
            val data = result.data
            if (resultCode == Activity.RESULT_OK) { //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data
                profileImage = fileUri?.path ?: ""
                isImageUploaded = true
                binding.saveProfileBtn.makeVisible(true)
            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(requireContext(), ImagePicker.getError(data), Toast.LENGTH_SHORT)
                    .show()
            }
        }

    private fun validateFieldsName(): Boolean {
        return if (binding.etName.text.toString().isNotEmpty()) {
            true
        } else {
            CommonUtils.showToast(requireContext(), getString(R.string.not_empty))
            false
        }
    }

    private fun validateFieldsAddress(): Boolean {
        return if (binding.etGarageAddress.text.toString().isNotEmpty()) {
            true
        } else {
            CommonUtils.showToast(requireContext(), getString(R.string.not_empty))
            false
        }
    }

    private fun validateJoiningDate(): Boolean {
        return if (binding.etJoiningDate.text.toString().isNotEmpty()) {
            true
        } else {
            CommonUtils.showToast(requireContext(), getString(R.string.not_empty))
            false
        }
    }

    private fun validateMobileFieldsName(): Boolean {
        return if (binding.etMobile.text.toString().isNotEmpty()) {
            true
        } else {
            CommonUtils.showToast(requireContext(), getString(R.string.not_empty))
            false
        }
    }

    private fun showVerifyOTPDialog(mobile: String, otp: String?) {
        activity?.let {
            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.VERIFY_OTP_SHEET,
                Bundle().apply {
                    putString(AppENUM.MOBILE_NUMBER, mobile)
                    putString(AppENUM.OTP, otp)
                },
                object : ActionListener {
                    override fun onActionItem(extra: Any?, extra2: Any?) {
                        showLoader()
                        hitAPIAfterVerifyOtp()
                    }
                })?.let { baseFragment ->
                baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
            }
        }
    }


    private fun showSelectDateDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_DATE, Bundle().apply {
            putBoolean(AppENUM.IS_START, true)
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                startDate = extra as String
                binding.etJoiningDate.setText(startDate)
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is LocationSelectedEvent -> {
                val it = event.extra as AddressData
                binding.etGarageAddress.setText(it.fullAddress)
                googleMapLink = "${AppENUM.MAP_URL}${it.latitude},${it.longitude}"
                selectedLocation = LatLng(it.latitude ?: 0.0, it.longitude ?: 0.0)
            }
        }
    }
}