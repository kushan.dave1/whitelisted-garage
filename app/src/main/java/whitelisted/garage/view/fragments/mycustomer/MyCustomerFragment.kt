package whitelisted.garage.view.fragments.mycustomer

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.PopupMenu
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_manage_packages.imgBack
import kotlinx.android.synthetic.main.item_mycustomer.*
import kotlinx.android.synthetic.main.layout_no_data.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.MyCustomerModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentMyCustomerBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.MyCustomerAdapter
import whitelisted.garage.viewmodels.MyCustomerViewModel

@SuppressLint("NotifyDataSetChanged")
class MyCustomerFragment : BaseFragment(), View.OnTouchListener, TextView.OnEditorActionListener {
    private val binding by viewBinding(FragmentMyCustomerBinding::inflate)
    private val viewModel: MyCustomerViewModel by viewModel()
    private lateinit var myCustomerAdapter: MyCustomerAdapter
    private lateinit var customPackagesList: MutableList<MyCustomerModel>
    private var mobileNumber: String? = ""
    private var countryCode: String? = ""
    private var isWhatsAppMessage = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
        Log.d("Token", viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID, ""))
        Log.d("Token", viewModel.getStringSharedPreference(AppENUM.ACCESS_TOKEN_, ""))
        Log.d("Token",
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT))
    }

    private fun setupScreen() {
        imgBack.setOnClickListener(this)
        binding.rvMyCustomer.setOnTouchListener(this)
        binding.etSearch.addTextChangedListener(textWatcher)
        binding.etSearch.setOnEditorActionListener(this)
        showLoader()
        setMyCustomerAdapter()
        observeGetMessageFromApi()
        observeMyCustomerTabList()
        viewModel.getMyCustomerList()
        tvNoDataFound.text = getString(R.string.no_cus_found)
    }


    private fun observeMyCustomerTabList() {
        viewModel.provideMyCustomerList().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    customPackagesList.clear()
                    customPackagesList.addAll(it.body.data.toMutableList())
                    if (customPackagesList.size > 0) {
                        binding.noDataScreen.rlBaseLND.makeVisible(false)
                        binding.rvMyCustomer.makeVisible(true)
                        myCustomerAdapter.notifyDataSetChanged()

                    } else {
                        binding.noDataScreen.rlBaseLND.makeVisible(true)
                        binding.rvMyCustomer.makeVisible(false)
                    }
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun observeGetMessageFromApi() {
        viewModel.provideMessageFromApi().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (isWhatsAppMessage) {
                        openWhatsAppText(true,
                            (countryCode + mobileNumber),
                            it.body.data.whatsappText)

                    } else {
                        openWhatsAppText(false, countryCode + mobileNumber, it.body.data.smsText)
                    }
                    isWhatsAppMessage = false
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun setMyCustomerAdapter() {
        customPackagesList = ArrayList()
        binding.rvMyCustomer.layoutManager = LinearLayoutManager(requireContext())
        myCustomerAdapter = MyCustomerAdapter(customPackagesList, this)
        binding.rvMyCustomer.adapter = myCustomerAdapter
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                myCustomerAdapter.viewType = 0
            }
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                myCustomerAdapter.viewType = 0
            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                myCustomerAdapter.viewType = 1
                binding.etSearch.hint = getString(R.string.search_reg_phone_or_customer_name)
            }
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.tvSMS -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    isWhatsAppMessage = false
                    mobileNumber = customPackagesList[position].mobile
                    countryCode = customPackagesList[position].country_code
                    when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
                        AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                            if (!isLoading()) {
                                showLoader()
                                viewModel.getMessageFromApi(customPackagesList[position].userId)
                            }
                        }
                        AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                            if (!isLoading()) {
                                showLoader()
                                viewModel.getMessageFromApi(customPackagesList[position].userId)
                            }
                        }
                        AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                            if (!isLoading()) {
                                showLoader()
                                viewModel.getMessageFromAPIRetailer(customPackagesList[position].mobile)
                            }
                        }
                        else -> {
                            if (!isLoading()) {
                                showLoader()
                                viewModel.getMessageFromApi(customPackagesList[position].userId)
                            }
                        }
                    }
                }
            }
            R.id.tvWhatsApp -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    isWhatsAppMessage = true
                    mobileNumber = customPackagesList[position].mobile
                    countryCode = customPackagesList[position].country_code
                    when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
                        AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                            if (!isLoading()) {
                                showLoader()
                                viewModel.getMessageFromApi(customPackagesList[position].userId)
                            }
                        }
                        AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                            if (!isLoading()) {
                                showLoader()
                                viewModel.getMessageFromApi(customPackagesList[position].userId)
                            }
                        }
                        AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                            if (!isLoading()) {
                                showLoader()
                                viewModel.getMessageFromAPIRetailer(customPackagesList[position].mobile)
                            }
                        }
                        else -> {
                            if (!isLoading()) {
                                showLoader()
                                viewModel.getMessageFromApi(customPackagesList[position].userId)
                            }
                        }
                    }
                }
            }
            R.id.tvCall -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    if (!customPackagesList[position].mobile.isNullOrEmpty()) {
                        CommonUtils.openPhoneCallApp("${(customPackagesList[position].country_code)}${customPackagesList[position].mobile ?: ""}",
                            requireContext())
                    } else {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.model_can_not_be_empty))
                    }

                }
            }

            R.id.btnMore -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    addNewOrder(position)
                }
            }

            R.id.btnSeeMore, R.id.llSeeMore -> CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->

                val order = customPackagesList[position]
                fireLastOrderBillEvent(order)

            }
        }
    }
    /*
          text watcher object for search purposes
     */
    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) { // do nothing
        }

        override fun beforeTextChanged(s: CharSequence?,
            start: Int,
            count: Int,
            after: Int) { // do nothing
        }


        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            customPackagesList.clear()
            myCustomerAdapter.notifyDataSetChanged()

            if (s.toString().length > 1) {
                showLoader()
                viewModel.getMyCustomerListWithSearch(s.toString())
            } else if (s.toString().isEmpty()) {
                showLoader()
                viewModel.getMyCustomerList()
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
        CommonUtils.hideKeyboard(requireActivity())
        return false
    }

    override fun onEditorAction(textView: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            CommonUtils.hideKeyboard(requireActivity())
            showLoader()
            viewModel.getMyCustomerListWithSearch(binding.etSearch.text.toString())
            return true
        }
        return false
    }


    private fun addPopupMenu(btn: View, layoutPosition: Int) {
        val popupMenu = PopupMenu(btn.context, btn)
        popupMenu.menuInflater.inflate(R.menu.my_customers_menu, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener {
            if (it.itemId == R.id.add_new_order) addNewOrder(layoutPosition)
            true
        }
        popupMenu.show()
    }

    private fun addNewOrder(position: Int) {
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.RETAILER_CONSTANT) {
            CommonUtils.addFragmentUtil(requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_NEW_ORDER,
                    Bundle().apply {
                        putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                        if (customPackagesList.size > position) putParcelable(AppENUM.MY_CUSOMTER_MODEL,
                            customPackagesList[position])
                    }))
        } else {
            CommonUtils.addFragmentUtil(requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.NEW_ORDER_DETAIL,
                    Bundle().apply {
                        if (customPackagesList.size > position) putParcelable(AppENUM.MY_CUSOMTER_MODEL,
                            customPackagesList[position])
                    }))
        }
    }

    private fun fireLastOrderBillEvent(order: MyCustomerModel) {

        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.ORDER_DETAILS,
            order.orderDetails.toString())
        bundle.putDouble(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.AMOUNT_PAID,
            order.orderDetails?.billAmount ?: 0.0)
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_MY_CUSTOMERS)
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_ORDER_HISTORY,
            bundle)
    }


}