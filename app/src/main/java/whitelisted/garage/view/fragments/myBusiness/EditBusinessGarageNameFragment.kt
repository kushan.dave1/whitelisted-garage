package whitelisted.garage.view.fragments.myBusiness

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_edit_bus_garage_name.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.BusinessPageData
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentEditBusGarageNameBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.ImageLoader
import whitelisted.garage.viewmodels.MyBusinessViewModel

class EditBusinessGarageNameFragment : BaseFragment() {


    private val viewModel: MyBusinessViewModel by sharedViewModel()
    private val binding by viewBinding(FragmentEditBusGarageNameBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
        setData()
    }

    private fun setData() {
        arguments?.let {
            binding.etName.setText(it.getString(AppENUM.NAME, ""))
            binding.etPhone.setText(it.getString(AppENUM.MOBILE, ""))
        }
        ImageLoader.loadImage(binding.imageFlag,
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_FLAG,
                AppENUM.RefactoredStrings.defaultFlag))
        binding.tv91.text = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
            AppENUM.RefactoredStrings.defaultCountryCode)
    }

    private fun setupScreen() {
        binding.btnApply.setOnClickListener(this)
        binding.btnCancel.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnApply -> {
                viewModel.setBusinessPageData(BusinessPageData(binding.etName.text.toString(),
                    binding.etPhone.text.toString()))
                popBackStackAndHideKeyboard()
            }
            R.id.imgBack, R.id.btnCancel -> {
                popBackStackAndHideKeyboard()
            }
        }
    }
}