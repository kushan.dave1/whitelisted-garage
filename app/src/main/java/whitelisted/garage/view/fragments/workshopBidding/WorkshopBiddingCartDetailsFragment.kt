package whitelisted.garage.view.fragments.workshopBidding

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.BrandWiseItem
import whitelisted.garage.api.request.GenericItem
import whitelisted.garage.api.request.WorkShopBiddingRequest
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentWorkshopBidCartDetailsBinding
import whitelisted.garage.databinding.LayoutCancelBidBinding
import whitelisted.garage.eventBus.GoBackEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.getLocationPointOnScreen
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.WorkshopBidBrandAdapter
import whitelisted.garage.view.adapter.WorkshopBidGenericAdapter
import whitelisted.garage.viewmodels.WorkShopBiddingViewModel

class WorkshopBiddingCartDetailsFragment : BaseFragment() {

    private val binding by viewBinding(FragmentWorkshopBidCartDetailsBinding::inflate)
    private lateinit var bidBrandAdapter: WorkshopBidBrandAdapter
    private lateinit var bidGenericAdapter: WorkshopBidGenericAdapter
    private val viewModel: WorkShopBiddingViewModel by sharedViewModel()
    private var isViewONLY = false
    private var bidId: String? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        getDataFromArgs()
        clickListener()
        observe()
        setData()
        setUpAdapters()
    }

    private fun getDataFromArgs() = binding.apply {
        arguments?.let {
            isViewONLY = it.getBoolean(AppENUM.IS_VIEW_ONLY, false)
            bidId = it.getString(AppENUM.ID, "")
        }
        if (isViewONLY)  {
            llProceed.makeVisible(false)
            btnProceedToOrderList.makeVisible(true)
            imgMore.makeVisible(true)
        } else {
            llProceed.makeVisible(true)
            btnProceedToOrderList.makeVisible(false)
            imgMore.makeVisible(false)
        }
    }

    private fun setData() = binding.apply {
        if (viewModel.brandWiseItems.isEmpty() && viewModel.genericItems.size == 0) {
            llNoDataScreen.makeVisible(true)
            nestedScrollView.makeVisible(false)
            llProceed.makeVisible(false)
        } else {
            llNoDataScreen.makeVisible(false)
            nestedScrollView.makeVisible(true)
            llProceed.makeVisible(true)
        }
    }

    private fun setUpAdapters() = binding.apply {
        if (!::bidBrandAdapter.isInitialized) {
            bidBrandAdapter = WorkshopBidBrandAdapter(viewModel.brandWiseItems,
                (viewModel.brandWiseItems.keys).toMutableList(),
                isViewONLY, this@WorkshopBiddingCartDetailsFragment)
        }
        rvBrandPart.adapter = bidBrandAdapter
        if (!::bidGenericAdapter.isInitialized) {
            bidGenericAdapter = WorkshopBidGenericAdapter(viewModel.genericItems,
                this@WorkshopBiddingCartDetailsFragment, isViewONLY)
        }
        rvGenericPart.adapter = bidGenericAdapter
    }

    private fun observe() { //
        observeSaveWFCart()
        observeBidCancelled()
    }

    private fun observeBidCancelled() {
        viewModel.provideCancelWsBid().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.let {
                        popBackStackAndHideKeyboard()
                        EventBus.getDefault().post(GoBackEvent())
                        viewModel.setRefreshReceivedBidList(true)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
    }

    private fun observeSaveWFCart() {
        viewModel.provideCreateWorkshopBidCartResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success<*> -> {
                    it.body.let {
                        firebaseEventProceed()
                        popBackStackAndHideKeyboard()
                        EventBus.getDefault().post(GoBackEvent())
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_BIDDING_STATUS_FRAGMENT,
                                Bundle().apply {
                                    putBoolean(AppENUM.IS_VIEW_ONLY, false)
                                }))
                    }
                }
                is Result.Failure<*> -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
    }

    private fun clickListener() {
        binding.imgBack.setOnClickListener(this)
        binding.btnAddPart.setOnClickListener(this)
        binding.btnProceed.setOnClickListener(this)
        binding.btnProceedToOrderList.setOnClickListener(this)
        binding.imgMore.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
            R.id.btnAddPart -> {
                popBackStackAndHideKeyboard()
            }
            R.id.imgMore -> {
                cancelBidPopUp(v)
            }
            R.id.tvAddRemark -> {
                CommonUtils.genericCastOrNull<Boolean>(v.getTag(R.id.IsGeneric))?.let {
                    if (it) {
                        CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))
                            ?.let { position ->
                                showRemarkDialog(position, true, null)
                            }
                    } else {
                        CommonUtils.genericCastOrNull<BrandWiseItem>(v.getTag(R.id.model))
                            ?.let { brandWise ->
                                showRemarkDialog(0, false, brandWise)
                            }
                    }
                }
            }
            R.id.btnProceedToOrderList -> {
                popBackStackAndHideKeyboard()
                EventBus.getDefault().post(GoBackEvent())
            }
            R.id.btnProceed -> { //
                showLoader()
                val request = WorkShopBiddingRequest().apply {
                    genericItems = viewModel.genericItems
                    brandWiseItems = viewModel.getAllBrandBidsList(viewModel.brandWiseItems)
                    totalItem = viewModel.getCompleteCartSize(viewModel.brandWiseItems,
                        viewModel.genericItems).toLong()
                }
                viewModel.createWorkshopBiddingAsync(request)

            }
            R.id.tvAddPart -> {
                CommonUtils.genericCastOrNull<MutableList<BrandWiseItem>>(v.getTag(R.id.model))
                    ?.let {
                        if (it.isNotEmpty()) {
                            popBackStackAndHideKeyboard()
                            viewModel.setCarBrand(it[0])
                        }
                    }
            }
            R.id.imgWpMinuse -> {
                CommonUtils.genericCastOrNull<GenericItem>(v.getTag(R.id.model))?.let { data ->
                    viewModel.setGenericItemRemovedFromCart(data)
                }
            }
            R.id.img1, R.id.img2, R.id.img3, R.id.img4 -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    CommonUtils.genericCastOrNull<List<String>>(v.getTag(R.id.model))?.let { list ->
                        imagePreview(v, Uri.parse(list[position]))
                    }
                }
            }
            R.id.tvMorePhotos -> {
                CommonUtils.genericCastOrNull<List<String>>(v.getTag(R.id.model))?.let { list ->
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.INVENTORY_IMAGE_PREVIEW,
                            Bundle().apply {
                                this.putStringArrayList(AppENUM.IMAGE_URL, ArrayList(list))
                            }))
                }
            }
        }
    }

    private fun cancelBidPopUp(view: View?) {
        view?.let {
            val infoView = LayoutCancelBidBinding.inflate(layoutInflater)
            val popupWindow = PopupWindow(infoView.root,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                true)
            popupWindow.showAtLocation(view,
                Gravity.NO_GRAVITY,
                view.getLocationPointOnScreen().x - 110,
                view.getLocationPointOnScreen().y + 40)
            infoView.parentLayout.setOnClickListener {
                popupWindow.dismiss()
                openCancelBidDialog()
            }
        }
    }

    private fun openCancelBidDialog() {
        requireActivity().let {
            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.CANCEL_WS_BID,
                null,
                object : ActionListener {
                    override fun onActionItem(extra: Any?, extra2: Any?) {
                        (extra as? Boolean)?.let {
                            if (it) {
                                showLoader()
                                viewModel.cancelWorkshopBid(bidId ?: "")
                            }
                        }
                    }
                })?.let { baseFragment ->
                baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
            }
        }
    }

    private fun showRemarkDialog(position: Int,
        isGeneric: Boolean,
        brandWiseItemData: BrandWiseItem?) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.ADD_REMARK_PHOTO, Bundle().apply {
            if (isGeneric) {
                val data = viewModel.genericItems[position]
                putParcelable(AppENUM.DATA,
                    BrandWiseItem(remark = data.remark, images = data.images))
            } else {
                putParcelable(AppENUM.DATA, brandWiseItemData)
            }
        }, object : ActionListener {
            @SuppressLint("NotifyDataSetChanged")
            override fun onActionItem(extra: Any?, extra2: Any?) {
                val brandWiseItem = extra as BrandWiseItem
                if (isGeneric) {
                    viewModel.genericItems[position].remark = brandWiseItem.remark
                    viewModel.genericItems[position].images = brandWiseItem.images
                    bidGenericAdapter.notifyItemChanged(position)
                } else {
                    brandWiseItemData?.remark = brandWiseItem.remark
                    brandWiseItemData?.images = brandWiseItem.images
                    bidBrandAdapter.notifyDataSetChanged()
                }
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun firebaseEventProceed() {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_BIDDING_CART)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.ITEM_COUNT,
                viewModel.getCompleteCartSize(viewModel.brandWiseItems, viewModel.genericItems)
                    .toString())
            putParcelableArrayList(FirebaseAnalyticsLog.FirebaseEventNameENUM.ITEM_NAME,
                viewModel.getAllGenericWise())
            putParcelableArrayList(FirebaseAnalyticsLog.FirebaseEventNameENUM.ITEM_NAME,
                viewModel.getAllBrandWise())
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_BID_ENQ_SUCC,
                this)
        }
    }
}