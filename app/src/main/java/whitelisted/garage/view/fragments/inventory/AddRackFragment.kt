package whitelisted.garage.view.fragments.inventory

import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.CreateInventoryRackRequest
import whitelisted.garage.api.request.RackItem
import whitelisted.garage.api.response.GetInventoryRackResponse
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentAddRackBinding
import whitelisted.garage.eventBus.InventoryItemAddedEvent
import whitelisted.garage.eventBus.RackUpdateEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.InventoryRackItemAdapter
import whitelisted.garage.viewmodels.AddRackFragmentViewModel
import java.util.*

class AddRackFragment : BaseFragment() {

    private val binding by viewBinding(FragmentAddRackBinding::inflate)
    private lateinit var inventoryRackAdapter: InventoryRackItemAdapter
    private val viewModel: AddRackFragmentViewModel by viewModel()
    private var inventoryData: GetInventoryRackResponse? = null
    private var singleInventoryResponse: GetInventoryRackResponse? = null
    private lateinit var filterList: MutableList<RackItem>
    private var isTwoWheeler = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun addTextWatcher() {
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?,
                p1: Int,
                p2: Int,
                p3: Int) { // nothing
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                setAdapter(p0.toString())
            }

            override fun afterTextChanged(p0: Editable?) { // nothing
            }
        })
    }

    private fun getData() {
        arguments?.let { bundle ->
            inventoryData = bundle.getParcelable(AppENUM.INVENTORY_DATA)
            binding.tvHeader.text =
                String.format(getString(R.string.rack_placeholder), inventoryData?.name)
            isTwoWheeler = bundle.getBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, false)
        }
    }

    private fun observeData() {
        observeGetInventory()
        observeUpdateInventory()
    }

    private fun setAdapter(searchString: String) {
        if (searchString.trim().isNotEmpty()) {
            if (::filterList.isInitialized) filterList.clear()
            else filterList = mutableListOf()
            viewModel.provideListOfRacksItems().forEach {
                if (it.partName.toString().toLowerCase(Locale.getDefault())
                        .contains(searchString.toLowerCase(Locale.getDefault())) || it.brandName.toString()
                        .toLowerCase(Locale.getDefault())
                        .contains(searchString.toLowerCase(Locale.getDefault()))) {
                    filterList.add(it)
                }
            }
            inventoryRackAdapter.setData(filterList)

        } else {
            binding.tvTotalItemCount.text = viewModel.provideListOfRacksItems().size.toString()
            if (viewModel.provideListOfRacksItems().size == 0) {
                binding.etSearch.makeVisible(false)
            } else {
                binding.etSearch.makeVisible(true)
            }
            inventoryRackAdapter.setData(viewModel.provideListOfRacksItems())
        }
    }

    private fun observeUpdateInventory() {
        viewModel.provideUpdateInventoryResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    showLoader()
                    viewModel.getIndividualInventoryRack(inventoryData?.id.toString())
                    EventBus.getDefault().post(RackUpdateEvent())
                    EventBus.getDefault().post(InventoryItemAddedEvent())
                }
                is Result.Failure -> CommonUtils.showToast(activity, it.errorMessage, true)
            }
        }
    }

    private fun observeGetInventory() {
        viewModel.provideIndividualInventoryRackResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    viewModel.setListOfRackItem(it.body.data.rack_items ?: mutableListOf())
                    singleInventoryResponse = it.body.data
                    setAdapter("")
                }
                is Result.Failure -> CommonUtils.showToast(activity, it.errorMessage, true)
            }
        }
    }

    private fun setupScreen() {
        clickListener()
        getData()
        observeData()
        addTextWatcher()
        setUpRV()
        if (inventoryData?.items_count ?: 0 > 0) {
            binding.clRackList.makeVisible(true)
            showLoader()
            viewModel.getIndividualInventoryRack(inventoryData?.id.toString())
        } //        else {
        //            clEmptyList.makeVisible(true)
        //        }
    }

    private fun clickListener() {
        binding.llAddRack.setOnClickListener(this)
        binding.llAddRack2.setOnClickListener(this)
        binding.imgMenu.setOnClickListener(this)
    }

    private fun setUpRV() {
        if (!::inventoryRackAdapter.isInitialized) {
            inventoryRackAdapter = InventoryRackItemAdapter(viewModel.getStringSharedPreference(
                AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default)), this)
            binding.rvAddRackItem.adapter = inventoryRackAdapter
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgMenu -> {
                popBackStackAndHideKeyboard() //                EventBus.getDefault().post(RackUpdateEvent())
            }
            R.id.llAddRack -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ADD_RACK_ITEM_FRAGMENT,
                        Bundle().apply {
                            putParcelable(AppENUM.INVENTORY_DATA, singleInventoryResponse)
                            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, isTwoWheeler)
                        }),
                    target = R.id.fragment_container_2)
                setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_ITEM_INV,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ADD_INVENTORY_RACK)
            }
            R.id.imageDeleteRackItem -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.imageDeleteRackItem))?.let {
                    deleteRackItem(it)
                }
            }

            R.id.llPhotoLayout -> {
                CommonUtils.genericCastOrNull<List<String>>(v.getTag(R.id.llPhotoLayout))?.let {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.INVENTORY_IMAGE_PREVIEW,
                            Bundle().apply {
                                this.putStringArrayList(AppENUM.IMAGE_URL, ArrayList(it))
                            }),
                        target = R.id.fragment_container_2)
                }
            }

            R.id.imageEditRackItem -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.imageEditRackItem))?.let {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.Edit_RACK_ITEM_FRAGMENT,
                            Bundle().apply {
                                putParcelable(AppENUM.INVENTORY_DATA, singleInventoryResponse)
                                putInt(AppENUM.POSITION, it)
                                putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, isTwoWheeler)
                            }),
                        target = R.id.fragment_container_2)
                }
            }

            R.id.btnSaveIR -> {
                val data = v.tag as RackItem
                singleInventoryResponse?.rack_items?.forEachIndexed { index, rackItem ->
                    if (rackItem.rackItemId == data.rackItemId) singleInventoryResponse?.rack_items!![index] =
                        data
                }

                CreateInventoryRackRequest().apply {
                    rackItems = singleInventoryResponse?.rack_items
                    id = singleInventoryResponse?.id
                    images = singleInventoryResponse?.images
                    rackName = singleInventoryResponse?.name
                    showLoader()
                    id?.let { it1 ->
                        viewModel.updateInventoryRack(it1, this)
                    }
                }
            }

            R.id.llAddRack2 -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ADD_RACK_ITEM_FRAGMENT,
                        Bundle().apply {
                            putParcelable(AppENUM.INVENTORY_DATA, inventoryData)
                        }),
                    target = R.id.fragment_container_2)
                setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_ITEM_INV,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ADD_INVENTORY_RACK)
            }
        }
    }

    private fun deleteRackItem(position: Int) {
        lateinit var dialog: AlertDialog
        activity?.let {
            AlertDialog.Builder(it).apply {
                setTitle(getString(R.string.delete))
                setMessage(getString(R.string.rack_item_delete_confirm))
                val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                    when (which) {
                        DialogInterface.BUTTON_POSITIVE -> {
                            val list = singleInventoryResponse?.rack_items
                            if ((list?.size ?: 0) > position) {
                                list?.removeAt(position)
                                CreateInventoryRackRequest().apply {
                                    rackItems = list
                                    id = singleInventoryResponse?.id
                                    images = singleInventoryResponse?.images
                                    rackName = singleInventoryResponse?.name
                                    showLoader()
                                    id?.let { it1 ->
                                        viewModel.updateInventoryRack(it1, this)
                                    }
                                }
                            }
                        }
                    }
                }
                setPositiveButton(getString(R.string.delete), dialogClickListener)
                setNeutralButton(getString(R.string.cancel), dialogClickListener)
                dialog = create()
                dialog.show()
            }
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is InventoryItemAddedEvent -> {
                showLoader()
                viewModel.getIndividualInventoryRack(inventoryData?.id.toString())
            }
        }
    }

    override fun onResume() {
        super.onResume()
        onBackPress()
    }

    private fun onBackPress() {
        view?.isFocusableInTouchMode = true
        view?.requestFocus()
        view?.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) { //                EventBus.getDefault().post(RackUpdateEvent())
                requireActivity().onBackPressed()
                true
            } else false
        }
    }

}