package whitelisted.garage.view.fragments.sparesShop

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager2.widget.ViewPager2
import whitelisted.garage.api.response.MediaUrl
import whitelisted.garage.api.response.SSItemModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentZoomImageBinding
import whitelisted.garage.databinding.ItemProductImageZoomableBinding
import whitelisted.garage.databinding.ProductDetailVideoItemBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils.setOnPageChangedListener
import whitelisted.garage.utils.ImageLoader
import whitelisted.garage.view.adapter.ProductDetailImagesAdapter

class ZoomImageFragment : BaseFragment() {

    private val binding by viewBinding(FragmentZoomImageBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        val images =
            arguments?.getParcelable<SSItemModel>(AppENUM.SSITEM)?.mediaUrl ?: arrayListOf()
        val page = arguments?.getInt("page", 0) ?: 0
        //binding.vpImages.adapter = ProductDetailImagesAdapter(images, isZoomableImages =  true)
        binding.vpImages2.adapter = ProductZoomImageAdapter(images)
        binding.vpImages2.offscreenPageLimit = images.size

        binding.ivClose.setOnClickListener { popBackStackAndHideKeyboard() }
        binding.tvImageCount.text = "1 of ${images.size}"
        onSelected(page, images.size)
    }

    private fun onSelected(position: Int, totalImages: Int) {
        binding.vpImages.currentItem = position
        binding.tvImageCount.text = "${position + 1} of $totalImages"
    }

    private fun setViewPagerScrollListener() {
        binding.vpImages.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                (binding.vpImages.adapter as? ProductDetailImagesAdapter)?.onPageScrolled(position)
            }
        })
    }

    inner class ProductZoomImageAdapter(private val media: List<MediaUrl>) : PagerAdapter() {

        override fun getCount() = media.size

        override fun isViewFromObject(view: View, obj: Any) = view == obj

        override fun instantiateItem(container: ViewGroup, position: Int): Any {

            return if (media[position].type == 1) {
                val card = ItemProductImageZoomableBinding.inflate(
                    LayoutInflater.from(requireContext()), container, false)
                ImageLoader.loadImage(card.imgProductImageSDF, media[position].url)
                container.addView(card.root)
                card.root
            } else {
                val card = ProductDetailVideoItemBinding.inflate(
                    LayoutInflater.from( requireContext()), container, false)
                val videoUri = Uri.parse(media[position].url)
                card.ivPartVideo.setVideoURI(videoUri)
                card.ivPartVideo.start()
                container.addView(card.root)
                card.root
            }
        }

    }


}