package whitelisted.garage.view.fragments.splash

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import com.an.biometric.BiometricCallback
import com.an.biometric.BiometricManager
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.ActivityResult
import com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE
import com.google.android.play.core.install.model.UpdateAvailability
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentSplashBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.AppSignatureHelper
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.viewmodels.LoginViewModel

class SplashFragment : BaseFragment(), TextWatcher {

    private val binding by viewBinding(FragmentSplashBinding::inflate)
    private var updateInfoObject: AppUpdateInfo? = null
    private val myRequestCode: Int = 111
    private var appUpdateManager: AppUpdateManager? = null
    private lateinit var passCode: String

    private val viewModel: LoginViewModel by viewModel()
    private var isHardUpdate = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.putBooleanSharedPreference(AppENUM.IS_SOFT_UPDATE_AVAILABLE, false)
        checkForAppUpdate()
        observeResponse()

        setClickListeners()
    }

    private fun setClickListeners() {
        binding.etPasscode.addTextChangedListener(this)
        binding.tvForgotPin.setOnClickListener(this)
    }

    private fun observeResponse() = binding.apply {
        viewModel.provideGetUpdateInfoResponse().observe(viewLifecycleOwner) {
            showLoader()
            when (it) {
                is Result.Success -> {
                    if (BuildConfig.VERSION_CODE >= it.body.data.backwardCompatibleTill?.toInt() ?: 0) {
                        viewModel.putBooleanSharedPreference(AppENUM.IS_SOFT_UPDATE_AVAILABLE, true)
                        checkFingerPrint()
                    } else {
                        setHardUpdateScreen()
                        updateInfoObject?.let { it1 ->
                            appUpdateManager?.startUpdateFlowForResult(it1,
                                IMMEDIATE,
                                requireActivity(),
                                myRequestCode)
                        }
                        Log.d(tag, "Update available")
                    }
                }
                is Result.Failure -> {
                    setHardUpdateScreen()
                }
            }
        }

        viewModel.provideGetPinResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (etPasscode.text.toString() == it.body.data.pin) {
                        setupScreen()
                    } else {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.alert_error_passcode))
                        CommonUtils.showKeyboard(requireActivity(), etPasscode)
                        etPasscode.text?.length?.let { it1 -> etPasscode.setSelection(it1) }
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

        viewModel.provideSetPinResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeOTPResponse() {
        viewModel.provideOTPForLoginResponse().observe(this) {
            hideLoader()
            when (it) {
                is Result.Success<*> -> {
                    showVerifyOTPDialog(viewModel.getStringSharedPreference(AppENUM.MOBILE_NUMBER))
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun showVerifyOTPDialog(mobile: String) {
        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.VERIFY_OTP_SHEET,
            Bundle().apply {
                putString(AppENUM.MOBILE_NUMBER, mobile)
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    showResetPinSheet()
                }
            })?.let { baseFragment ->
            baseFragment.show(requireActivity().supportFragmentManager, baseFragment.javaClass.name)
        }

    }

    private fun showResetPinSheet() {
        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.RESET_PIN,
            null,
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    showLoader()
                    viewModel.setPin(extra as String)
                }
            })?.let { baseFragment ->
            baseFragment.show(requireActivity().supportFragmentManager, baseFragment.javaClass.name)
        }
    }

    private fun checkForAppUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(requireContext())

        if (BuildConfig.DEBUG) {
            isHardUpdate = false
        }

        checkUpdate()
    }

    private fun setupScreen() { //        viewModel.putBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN,
        //            true)
        binding.clSplash.makeVisible(false)
        if (isAdded) {
            if (viewModel.getStringSharedPreference(AppENUM.ACCESS_TOKEN_, "").isNotEmpty()) {
                CommonUtils.replaceFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.BOTTOM_NAV, null))
            } else { //Basically since we are not logged in, do nothing but launch the login activity
                CommonUtils.replaceFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.LOGIN, null))
            }
        }
    }

    private fun checkFingerPrint() {
        Handler(Looper.getMainLooper()).postDelayed({
            if (viewModel.getBooleanSharedPreference(AppENUM.BIOMETRIC_ENABLED, false)) {
                binding.clSplash.makeVisible(false)
                binding.clPasscode.makeVisible(true)
                BiometricManager.BiometricBuilder(requireContext())
                    .setTitle(getString(R.string.app_name))
                    .setSubtitle(getString(R.string.enter_fingerprint_to_securely_login)) //            .setDescription("Add a description")
                    .setNegativeButtonText(getString(R.string.use_pin)).build()
                    .authenticate(object : BiometricCallback {
                        override fun onSdkVersionNotSupported() {
                            CommonUtils.showToast(requireContext(),
                                "Android version not supported",
                                true) //switchFingerprint.isChecked = false
                        }

                        override fun onBiometricAuthenticationNotSupported() {
                            CommonUtils.showToast(requireContext(),
                                "Biometric not supported",
                                true) //switchFingerprint.isChecked = false
                        }

                        override fun onBiometricAuthenticationNotAvailable() {
                            CommonUtils.showToast(requireContext(),
                                "Biometric not registered",
                                true) //switchFingerprint.isChecked = false
                        }

                        override fun onBiometricAuthenticationPermissionNotGranted() {
                            CommonUtils.showToast(requireContext(),
                                "Biometric auth not granted",
                                true) //switchFingerprint.isChecked = false
                        }

                        override fun onBiometricAuthenticationInternalError(error: String?) { //switchFingerprint.isChecked = false
                        }

                        override fun onAuthenticationFailed() { //                    CommonUtils.showToast(this@SplashActivity, "Biometric auth failed", true)
                            //switchFingerprint.isChecked = false
                        }

                        override fun onAuthenticationCancelled() { //                    CommonUtils.showToast(this@SplashActivity, "Cancel", true)
                            //switchFingerprint.isChecked = false
                            CommonUtils.showKeyboard(requireActivity(), binding.etPasscode)
                            binding.etPasscode.text?.length?.let {
                                binding.etPasscode.setSelection(it)
                            }
                        }

                        override fun onAuthenticationSuccessful() { //                    CommonUtils.showToast(this@SplashActivity, "Biometric auth Success", true)
                            setupScreen()
                        }

                        override fun onAuthenticationHelp(helpCode: Int,
                            helpString: CharSequence?) {
                            Log.d(helpString.toString(),
                                helpCode.toString()) //switchFingerprint.isChecked = false
                        }

                        override fun onAuthenticationError(errorCode: Int,
                            errString: CharSequence?) {
                            Log.d(errString.toString(),
                                errorCode.toString()) //switchFingerprint.isChecked = false
                        }
                    })
            } else setupScreen()
        }, 1000)
    }

    private fun setHardUpdateScreen() {
        binding.tvHardUpdateText.visibility = View.VISIBLE
    }

    private fun checkUpdate() {
        val appUpdateInfoTask = appUpdateManager?.appUpdateInfo
        Log.d(tag, "Checking for updates")
        appUpdateInfoTask?.addOnSuccessListener { appUpdateInfo ->
            updateInfoObject = appUpdateInfo
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                showLoader()
                viewModel.getUpdateInfo()
            } else {
                checkFingerPrint()
            }

        }
        appUpdateInfoTask?.addOnFailureListener {
            Log.d("Update_fail", it.toString())
            if (isHardUpdate) setHardUpdateScreen()
            else {
                checkFingerPrint()
            }
            if (isAdded) CommonUtils.showToast(requireContext(),
                getString(R.string.play_store_error),
                true)
        }
    }

    override fun onResume() {
        super.onResume()
        appUpdateManager?.appUpdateInfo?.addOnSuccessListener { appUpdateInfo ->

            if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                appUpdateManager?.startUpdateFlowForResult(appUpdateInfo,
                    IMMEDIATE,
                    requireActivity(),
                    myRequestCode)
            }
        }

    }

    @Deprecated("Deprecated in Java")
    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == myRequestCode) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    Log.d(tag, "Result Ok")
                    appUpdateManager?.completeUpdate()
                }
                Activity.RESULT_CANCELED -> {
                    Log.d(tag, "Result Cancelled")

                }
                ActivityResult.RESULT_IN_APP_UPDATE_FAILED -> {
                    Log.d(tag, "Update Failure")
                    checkForAppUpdate()
                }
            }
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        setPasscodeUI()
        if (binding.etPasscode.text?.length == 4) {
            CommonUtils.hideKeyboard(requireActivity())
            verifyPasscode()
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.tvForgotPin -> {
                showLoader()
                val signHelper = AppSignatureHelper(requireContext())
                signHelper.getAppSignatures()
                observeOTPResponse()
                viewModel.loginUserAPI(signHelper.getAppSignatures()[0],
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                        getString(R.string.ind_country_code)),
                    viewModel.getStringSharedPreference(AppENUM.MOBILE_NUMBER))
            }
        }
    }

    private fun setPasscodeUI() = binding.apply {
        when (etPasscode.text?.length) {
            0 -> {
                llPasscode1.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_passcode_unfilled)
                imgPasscode1.setImageResource(R.drawable.bg_white_round)
                llPasscode2.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_passcode_unfilled)
                imgPasscode2.setImageResource(R.drawable.bg_white_round)
                llPasscode3.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_passcode_unfilled)
                imgPasscode3.setImageResource(R.drawable.bg_white_round)
                llPasscode4.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_passcode_unfilled)
                imgPasscode4.setImageResource(R.drawable.bg_white_round)
            }
            1 -> {
                llPasscode1.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_accent_round)
                imgPasscode1.setImageResource(R.drawable.bg_passcode_filled)
                llPasscode2.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_passcode_unfilled)
                imgPasscode2.setImageResource(R.drawable.bg_white_round)
                llPasscode3.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_passcode_unfilled)
                imgPasscode3.setImageResource(R.drawable.bg_white_round)
                llPasscode4.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_passcode_unfilled)
                imgPasscode4.setImageResource(R.drawable.bg_white_round)
            }
            2 -> {
                llPasscode1.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_accent_round)
                imgPasscode1.setImageResource(R.drawable.bg_passcode_filled)
                llPasscode2.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_accent_round)
                imgPasscode2.setImageResource(R.drawable.bg_passcode_filled)
                llPasscode3.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_passcode_unfilled)
                imgPasscode3.setImageResource(R.drawable.bg_white_round)
                llPasscode4.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_passcode_unfilled)
                imgPasscode4.setImageResource(R.drawable.bg_white_round)
            }
            3 -> {
                llPasscode1.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_accent_round)
                imgPasscode1.setImageResource(R.drawable.bg_passcode_filled)
                llPasscode2.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_accent_round)
                imgPasscode2.setImageResource(R.drawable.bg_passcode_filled)
                llPasscode3.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_accent_round)
                imgPasscode3.setImageResource(R.drawable.bg_passcode_filled)
                llPasscode4.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_passcode_unfilled)
                imgPasscode4.setImageResource(R.drawable.bg_white_round)
            }
            4 -> {
                llPasscode1.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_accent_round)
                imgPasscode1.setImageResource(R.drawable.bg_passcode_filled)
                llPasscode2.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_accent_round)
                imgPasscode2.setImageResource(R.drawable.bg_passcode_filled)
                llPasscode3.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_accent_round)
                imgPasscode3.setImageResource(R.drawable.bg_passcode_filled)
                llPasscode4.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_accent_round)
                imgPasscode4.setImageResource(R.drawable.bg_passcode_filled)
            }
        }
    }

    private fun verifyPasscode() = binding.apply {
        if (::passCode.isInitialized) {
            if (passCode == etPasscode.text.toString()) {
                setupScreen()
            } else {
                CommonUtils.showToast(requireContext(), getString(R.string.alert_error_passcode))
                CommonUtils.showKeyboard(requireActivity(), etPasscode)
                etPasscode.text?.length?.let { etPasscode.setSelection(it) }
            }
        } else {
            showLoader()
            viewModel.getPin()
        }
    }

    override fun afterTextChanged(p0: Editable?) { //do nothing
    }

}