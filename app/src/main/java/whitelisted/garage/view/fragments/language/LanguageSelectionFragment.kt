package whitelisted.garage.view.fragments.language

import android.os.Bundle
import android.view.View
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.LanguageData
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentLanguageSelectionBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.view.activities.DashboardActivity
import whitelisted.garage.view.adapter.LanguageAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel

class LanguageSelectionFragment : BaseFragment() {

    private val binding by viewBinding(FragmentLanguageSelectionBinding::inflate)
    private val viewModel: DashboardSharedViewModel by sharedViewModel()
    private var languageValue = AppENUM.LanguageConstants.LAN_DEFAULT

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
        setUpDate()
    }

    private fun setUpDate() {
        val enableList = mutableListOf(
            false,
            false,
            false,
            false,
            )
        languageValue = viewModel.getStringSharedPreference(AppENUM.RefactoredStrings.LANGUAGE_CODE,
            AppENUM.LanguageConstants.LAN_DEFAULT)

        when (languageValue) { //            AppENUM.LanguageConstants.LAN_HINGLISH -> {
            //                enableList[0] = true
            //            }
            AppENUM.LanguageConstants.LAN_ENGLISH -> {
                enableList[1] = true
            }
            AppENUM.LanguageConstants.LAN_HINDI -> {
                enableList[0] = true
            }
            AppENUM.LanguageConstants.LAN_GUJARAT -> {
                enableList[2] = true
            }
            AppENUM.LanguageConstants.LAN_TELEAGU -> {
                enableList[3] = true
            }
            AppENUM.LanguageConstants.LAN_KANNADA -> { // enableList[4] = true
            }
            AppENUM.LanguageConstants.LAN_TAMIL -> { //  enableList[5] = true
            } //            AppENUM.LanguageConstants.LAN_BANGLA -> {
            //                enableList[6] = true
            //            }

            //            AppENUM.LanguageConstants.LAN_TAMIL -> {
            //                enableList[8] = true
            //            }
        }
        binding.rvLanguage.adapter = LanguageAdapter(this, enableList)
    }


    private fun setupScreen() {
        binding.backArrowImg.setOnClickListener(this)
        binding.btnSave.setOnClickListener(this)
        setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_LANGUAGE_SELECTION,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_LANGUAGE_SELECTION)
    }

    private fun setResource(language: String) {
        viewModel.putStringSharedPreference(AppENUM.RefactoredStrings.LANGUAGE_CODE, language)
        (activity as DashboardActivity).restartApp()
    }

    private fun setLanguage(s: String) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_LANGUAGE_SELECTION)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.LANGUAGE, s)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_LANGUAGE_SELECTION,
                this)
        }
        when (s) {
            AppENUM.LanguageConstants.HINDI -> {
                setResource(AppENUM.LanguageConstants.LAN_HINDI)
            }
            AppENUM.LanguageConstants.HINGLISH -> {
                setResource(AppENUM.LanguageConstants.LAN_HINGLISH)
            }
            AppENUM.LanguageConstants.ENGLISH -> {
                setResource(AppENUM.LanguageConstants.LAN_ENGLISH)
            }
            AppENUM.LanguageConstants.BANGLA -> {
                setResource(AppENUM.LanguageConstants.LAN_BANGLA)
            }
            AppENUM.LanguageConstants.GUJARAT -> {
                setResource(AppENUM.LanguageConstants.LAN_GUJARAT)
            }
            AppENUM.LanguageConstants.KANNADA -> {
                setResource(AppENUM.LanguageConstants.LAN_KANNADA)
            }
            AppENUM.LanguageConstants.MARATHI -> {
                setResource(AppENUM.LanguageConstants.LAN_MARATHI)
            }
            AppENUM.LanguageConstants.TAMIL -> {
                setResource(AppENUM.LanguageConstants.LAN_TAMIL)
            }
            AppENUM.LanguageConstants.TELEAGU -> {
                setResource(AppENUM.LanguageConstants.LAN_TELEAGU)
            }
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.backArrowImg -> popBackStackAndHideKeyboard()
            R.id.btnSave -> { //
            }
            R.id.parenLayout -> {
                whitelisted.garage.utils.CommonUtils.genericCastOrNull<LanguageData>(v.getTag(R.id.parenLayout))
                    ?.let {
                        setLanguage(it.string)
                    }
            }
        }
    }

}