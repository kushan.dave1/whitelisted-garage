package whitelisted.garage.view.fragments.retailAddPart

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.CompoundButton
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.JobCardItem
import whitelisted.garage.api.request.OEServicesItem
import whitelisted.garage.api.request.OrderInclusionsItem
import whitelisted.garage.api.response.GetInventoryPartItemResponse
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentRetailOrderAddPartBinding
import whitelisted.garage.eventBus.*
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.changeViewCompoundDrawablesWithInterinsicBounds
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.utils.ImageLoader
import whitelisted.garage.view.adapter.InventoryItemsAdapter
import whitelisted.garage.viewmodels.RetailOrderAddPartViewModel
import whitelisted.garage.viewmodels.SelectCarDialogViewModel
import java.util.*

class RetailOrderAddPartFragment : BaseFragment(), CompoundButton.OnCheckedChangeListener {

    private val binding by viewBinding2(FragmentRetailOrderAddPartBinding::inflate)
    private val viewModel: RetailOrderAddPartViewModel by viewModel()
    private val selectCarViewModel: SelectCarDialogViewModel by sharedViewModel()
    private var yearRange = arrayListOf<String>()
    private var brandId = 0
    private var carModelId = 0
    private var sellingPrice = 0.0
    private var skuId = "0"
    private var isAddPart = true
    private var isAcc = false
    private var isJobCard = false
    private var isTwoWheeler = false
    private var isFromOutside = false
    private var isInventory = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        getDataFromArgs()
        clickListeners()
        observeData()
        getData()
    }

    private fun getData() {
        viewModel.getDateYearRange()
        getInventory()
    }

    private fun observeData() {
        observeYearRange()
        observeGetInventoryRack()
    }

    private fun observeYearRange() {
        viewModel.provideDateYearRange().observe(viewLifecycleOwner) {
            yearRange = it ?: arrayListOf()
            ArrayAdapter(requireContext(),
                R.layout.support_simple_spinner_dropdown_item,
                yearRange).apply {
                (binding?.autoYearMF?.editText as? AutoCompleteTextView)?.setAdapter(this)
            }
        }
    }

    private fun clickListeners() = binding?.apply {
        imgBack.setOnClickListener(this@RetailOrderAddPartFragment)
        tvSelectBrand.setOnClickListener(this@RetailOrderAddPartFragment)
        tvSearchPart.setOnClickListener(this@RetailOrderAddPartFragment)
        btnInventoryItems.setOnClickListener(this@RetailOrderAddPartFragment)
        btnAdd.setOnClickListener(this@RetailOrderAddPartFragment)
        btnAddInventory.setOnClickListener(this@RetailOrderAddPartFragment)
        tvEnterFuelType.setOnClickListener(this@RetailOrderAddPartFragment)
        switchCarDetails.setOnCheckedChangeListener(this@RetailOrderAddPartFragment)
        imgSwitchVehicleType.setOnClickListener(this@RetailOrderAddPartFragment)
    }

    private fun getDataFromArgs() = binding?.apply {
        arguments?.let {
            if (it.getBoolean(AppENUM.IS_INVENTORY, false)) {
                isInventory = true
                srlAddPart.makeVisible(false)
                clAddInventoryItems.makeVisible(true)
                isAddPart = false
                btnAdd.text = getString(R.string.add_selected)
            }
            if (it.getBoolean(AppENUM.IS_ACCESSORIES, false)) {
                llBrand.makeVisible(false)
                llModel.makeVisible(false)
                llFuleType.makeVisible(false)
                llYearMF.makeVisible(false)
                llCarDetails.makeVisible(false)
                isAcc = true
                if (it.getBoolean(AppENUM.IS_JOB_CARD, false)) {
                    isJobCard = true
                }
            }
            if (it.getBoolean(AppENUM.SHOW_INVENTORY_LIST, false)) {
                srlAddPart.makeVisible(false)
                clAddInventoryItems.makeVisible(true)
                isAddPart = false
                btnAdd.text = getString(R.string.add_selected)
            }
            isTwoWheeler = it.getBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, false)
            isFromOutside = it.getBoolean(AppENUM.IS_FROM_OUTSIDE, false)
            if (isTwoWheeler) {
                isTwoWheeler = true
                lblFourWheeler.changeTextColor(requireContext(), R.color.black_text_new)
                lblTwoWheeler.changeTextColor(requireContext(), R.color.colorAccent)
                lblFourWheeler.changeViewCompoundDrawablesWithInterinsicBounds(ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.ic_car_type_unselectetd))
                lblTwoWheeler.changeViewCompoundDrawablesWithInterinsicBounds(ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.ic_two_wheeler))
                ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                    R.drawable.ic_double_switch_on), imgSwitchVehicleType)
            }
        }
    }

    private fun observeGetInventoryRack() {
        viewModel.provideInventoryRackResponse().observe(viewLifecycleOwner) { result ->
            hideLoader()
            when (result) {
                is Result.Success -> {
                    viewModel.getInventoryItems()?.clear()
                    val data = result.body.data
                    if (data.isNotEmpty()) {
                        data.forEach { rack ->
                            rack.rack_items?.forEach { rackItem ->
                                rackItem.apply {
                                    rackName = rack.name
                                    if (this.quantity ?: 0 > 0) viewModel.getInventoryItems()
                                        ?.add(this)
                                }
                            }
                        }

                        var list =
                            viewModel.getInventoryItems() //                        if (!isFromOutside) {
                        list = if (isTwoWheeler) {
                            list?.filter { it.vehicleType == 2 }?.toMutableList()
                        } else list?.filter { it.vehicleType != 2 }
                            ?.toMutableList() //                        }
                        if (list?.isNotEmpty() == true) binding?.apply {
                            llNoInventoryItems.makeVisible(false)
                            rvInventoryItems.makeVisible(true)
                            llAdd.makeVisible(true)
                            rvInventoryItems.layoutManager = LinearLayoutManager(requireContext())
                            rvInventoryItems.adapter =
                                InventoryItemsAdapter(list, this@RetailOrderAddPartFragment)
                            EventBus.getDefault()
                                .post(InventoryItemsEvent(viewModel.getInventoryItems()
                                    ?: mutableListOf()))
                        } else binding?.apply {
                            if (isInventory) llAdd.makeVisible(false)
                            llNoInventoryItems.makeVisible(true)
                        }
                    } else { //                        btnAdd.makeVisible(false)
                        binding?.llNoInventoryItems?.makeVisible(true)
                    }
                }
                is Result.Failure -> CommonUtils.showToast(activity, result.errorMessage, true)
            }
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
            R.id.tvSearchPart -> showPartSelectDialog()
            R.id.tvSelectBrand -> showBrandsDialog()
            R.id.btnInventoryItems -> binding?.apply {
                srlAddPart.makeVisible(false)
                clAddInventoryItems.makeVisible(true)
                isAddPart = false
                btnAdd.text =
                    getString(R.string.add_selected) //                viewModel.getInventoryItems().let {
                //                    if ((it?.size ?: 0) < 1) {
                //                        llAdd.makeVisible(false)
                //                    }
                //                }
                var list = viewModel.getInventoryItems()
                if (!isFromOutside) {
                    list = if (isTwoWheeler) {
                        list?.filter { it.vehicleType == 2 }?.toMutableList()
                    } else list?.filter { it.vehicleType != 2 }?.toMutableList()
                }
                if (list?.isNotEmpty() == true) {
                    rvInventoryItems.makeVisible(true)
                    llNoInventoryItems.makeVisible(false)
                    llAdd.makeVisible(true)
                    rvInventoryItems.layoutManager = LinearLayoutManager(requireContext())
                    rvInventoryItems.adapter =
                        InventoryItemsAdapter(list, this@RetailOrderAddPartFragment)
                    EventBus.getDefault()
                        .post(InventoryItemsEvent(viewModel.getInventoryItems() ?: mutableListOf()))
                } else {
                    btnAdd.makeVisible(false)
                    llNoInventoryItems.makeVisible(true)
                    rvInventoryItems.makeVisible(false)
                }
            }
            R.id.btnAdd -> {
                handleAddButtonClick()
            }
            R.id.btnAddInventory -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.INVENTORY_FRAGMENT,
                        Bundle().apply {
                            putBoolean(AppENUM.INNER_FRAGMENT, true)
                            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, isTwoWheeler)
                        }),
                    target = R.id.fragment_container_2)
            }
            R.id.tvEnterFuelType -> {
                if (binding?.tvSelectBrand?.text == getString(R.string.select_brand)) {
                    CommonUtils.showToast(requireContext(), getString(R.string.select_brand_frist))
                } else showFuelTypeDialog()
            }

            R.id.imgSwitchVehicleType -> binding?.apply {
                if (isTwoWheeler) {
                    isTwoWheeler = false
                    lblFourWheeler.changeTextColor(requireContext(), R.color.colorAccent)
                    lblTwoWheeler.changeTextColor(requireContext(), R.color.black_text_new)
                    lblFourWheeler.changeViewCompoundDrawablesWithInterinsicBounds(ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_add_car_details))
                    lblTwoWheeler.changeViewCompoundDrawablesWithInterinsicBounds(ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_bike_unselected))
                    ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                        R.drawable.ic_double_switch_off), imgSwitchVehicleType)
                } else {
                    isTwoWheeler = true
                    lblFourWheeler.changeTextColor(requireContext(), R.color.black_text_new)
                    lblTwoWheeler.changeTextColor(requireContext(), R.color.colorAccent)
                    lblFourWheeler.changeViewCompoundDrawablesWithInterinsicBounds(ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_car_type_unselectetd))
                    lblTwoWheeler.changeViewCompoundDrawablesWithInterinsicBounds(ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_two_wheeler))
                    ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                        R.drawable.ic_double_switch_on), imgSwitchVehicleType)
                }
                clearValues()
            }
        }

    }

    private fun clearValues() {

    }

    private fun handleAddButtonClick() {
        if (isAddPart) {
            if (validateMandatoryFields()) {
                if (isAcc) {
                    if (isJobCard) {
                        val selectedService = JobCardItem()
                        selectedService.skuId = skuId
                        selectedService.serviceName = binding?.tvSearchPart?.text.toString()
                        selectedService.type = AppENUM.AdaptersConstant.PART
                        selectedService.pricePerQuantity = sellingPrice
                        EventBus.getDefault()
                            .post(AddJobCardPartEventModel(AppENUM.AdaptersConstant.PART,
                                selectedService))
                    } else {
                        val selectedService = OEServicesItem()
                        selectedService.skuId = skuId
                        selectedService.serviceName = binding?.tvSearchPart?.text.toString()
                        selectedService.type = AppENUM.AdaptersConstant.PART
                        selectedService.pricePerQuantity = sellingPrice
                        selectedService.total =
                            selectedService.pricePerQuantity?.times(selectedService.quantity ?: 0)
                        EventBus.getDefault()
                            .post(AddOrderEstPartEventModel(AppENUM.AdaptersConstant.PART,
                                selectedService))
                    }
                } else {
                    val inclusionsItem = OrderInclusionsItem()
                    inclusionsItem.type = AppENUM.AdaptersConstant.PART
                    inclusionsItem.skuId = skuId
                    inclusionsItem.serviceName = binding?.tvSearchPart?.text.toString()
                    inclusionsItem.brandId = brandId
                    inclusionsItem.modelId = carModelId
                    inclusionsItem.brandName = binding?.tvSelectBrand?.text.toString()
                    inclusionsItem.modelname = binding?.tvSelectModel?.text.toString()
                    inclusionsItem.fuelType = binding?.tvEnterFuelType?.text.toString()
                    inclusionsItem.pricePerQuantity = sellingPrice
                    inclusionsItem.total =
                        inclusionsItem.pricePerQuantity?.times(inclusionsItem.quantity ?: 0)
                    EventBus.getDefault()
                        .post(AddRetailInclusionPartEventModel(AppENUM.AdaptersConstant.PART,
                            inclusionsItem))
                }
                popBackStackAndHideKeyboard()
            }
        } else {
            if (binding?.rvInventoryItems?.adapter != null) {
                (binding?.rvInventoryItems?.adapter as InventoryItemsAdapter).dataList.forEach {
                    if ((it.selectedQty ?: 0) > 0) {
                        if (isAcc) {
                            if (isJobCard) {
                                JobCardItem().apply {
                                    skuId = it.skuId
                                    serviceName = it.partName
                                    type = AppENUM.AdaptersConstant.INVENTORY
                                    rackItemId = it.rackItemId
                                    totalQuantity = it.quantity
                                    pricePerQuantity = it.selling_price ?: 0.0
                                    quantity = it.selectedQty
                                    total = it.selling_price?.times(it.selectedQty ?: 0) ?: 0.0
                                    EventBus.getDefault()
                                        .post(AddJobCardPartEventModel(AppENUM.AdaptersConstant.INVENTORY,
                                            this))
                                }

                            } else {
                                OEServicesItem().apply {
                                    skuId = it.skuId
                                    serviceName = it.partName
                                    type = AppENUM.AdaptersConstant.INVENTORY
                                    rackItemId = it.rackItemId
                                    totalQuantity = it.quantity
                                    quantity = it.selectedQty
                                    pricePerQuantity = it.selling_price ?: 0.0
                                    total =
                                        it.selling_price?.times(it.selectedQty?.toDouble() ?: 0.0)
                                    EventBus.getDefault()
                                        .post(AddOrderEstPartEventModel(AppENUM.AdaptersConstant.INVENTORY,
                                            this))
                                }

                            }
                        } else {
                            OrderInclusionsItem().apply {
                                type = AppENUM.AdaptersConstant.INVENTORY
                                skuId = it.skuId
                                serviceName = it.partName
                                brandId = it.brandId
                                modelId = it.carModelId
                                brandName = it.brandName
                                modelname = it.carModel
                                fuelType = it.fuel_type
                                quantity = it.selectedQty
                                rackItemId = it.rackItemId
                                totalQuantity = it.quantity
                                totalStockLeft = it.quantity?.minus(it.selectedQty ?: 0)
                                pricePerQuantity = it.selling_price ?: 0.0
                                total = it.selling_price?.times(it.selectedQty?.toDouble() ?: 0.0)
                                EventBus.getDefault()
                                    .post(AddRetailInclusionPartEventModel(AppENUM.AdaptersConstant.INVENTORY,
                                        this))
                            }
                        }
                    }
                }
                popBackStackAndHideKeyboard()
            } else { //
            }
        }
    }

    private fun showFuelTypeDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_CAR, Bundle().apply {
            putInt(AppENUM.CAR_SEARCH_TYPE, 2)
            putString(AppENUM.MODEL_ID,
                selectCarViewModel.provideSelectedModel().value?.modelId.toString())
            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, isTwoWheeler)
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                when (extra as Int) {
                    0 -> { // nothing
                    }
                    1 -> { //nothing
                    }
                    2 -> {
                        binding?.tvEnterFuelType?.text =
                            selectCarViewModel.provideSelectedFuelType().value?.type
                    }
                }
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun validateMandatoryFields(): Boolean {
        if (isAcc) {
            if (binding?.tvSearchPart?.text.toString().trim().isEmpty()) {
                CommonUtils.showToast(requireContext(), getString(R.string.part_can_not_be_empty))
                return false
            }
        } else {
            if (binding?.switchCarDetails?.isChecked == true) {
                if (binding?.tvSelectBrand?.text.toString().trim().isEmpty()) {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.brand_can_not_be_empty))
                    return false
                }
                if (binding?.tvSelectModel?.text.toString().trim().isEmpty()) {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.model_can_not_be_empty))
                    return false
                }
            }
            if (binding?.tvSearchPart?.text.toString().trim().isEmpty()) {
                CommonUtils.showToast(requireContext(), getString(R.string.part_can_not_be_empty))
                return false
            }
        }
        return true
    }

    private fun showBrandsDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_CAR, Bundle().apply {
            putInt(AppENUM.CAR_SEARCH_TYPE, 0)
            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, isTwoWheeler)
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                when (extra as Int) {
                    0 -> { // nothing
                    }
                    1 -> { // nothing
                    }
                    2 -> binding?.apply {
                        tvSelectModel.text =
                            selectCarViewModel.provideSelectedModel().value?.carName
                        tvSelectBrand.text = selectCarViewModel.provideSelectedBrand().value?.name
                        brandId = selectCarViewModel.provideSelectedModel().value?.brandId ?: 0
                        carModelId = selectCarViewModel.provideSelectedModel().value?.modelId ?: 0
                        tvEnterFuelType.text = ""
                        tvEnterFuelType.text =
                            selectCarViewModel.provideSelectedFuelType().value?.type
                    }
                }
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun showPartSelectDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_RACK_ITEM, Bundle().apply {
            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, isTwoWheeler)
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                (extra as? GetInventoryPartItemResponse)?.let {
                    skuId = it.skuId ?: "0"
                    sellingPrice = it.sellingPrice ?: 0.0
                    binding?.tvSearchPart?.setText(it.subCategoryName)

                    addPartToRecentSearch(it.subCategoryName ?: "")
                }
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun addPartToRecentSearch(serviceName: String) {
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT) {
            var recentSearchesList =
                viewModel.getStringSharedPreference(AppENUM.RECENT_SEARCHES_ACCESSORIES, "")
            recentSearchesList = if (recentSearchesList.isNotEmpty()) {
                if (recentSearchesList.contains(serviceName.toUpperCase(Locale.getDefault()))) {
                    recentSearchesList
                } else "$serviceName,$recentSearchesList"
            } else serviceName
            viewModel.putStringSharedPreference(AppENUM.RECENT_SEARCHES_ACCESSORIES,
                recentSearchesList.toUpperCase(Locale.getDefault()))
        } else if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.RETAILER_CONSTANT) {
            var recentSearchesList =
                viewModel.getStringSharedPreference(AppENUM.RECENT_SEARCHES_RETAILER, "")
            recentSearchesList = if (recentSearchesList.isNotEmpty()) {
                if (recentSearchesList.contains(serviceName.toUpperCase(Locale.getDefault()))) {
                    recentSearchesList
                } else "$serviceName,$recentSearchesList"
            } else serviceName
            viewModel.putStringSharedPreference(AppENUM.RECENT_SEARCHES_RETAILER,
                recentSearchesList.toUpperCase(Locale.getDefault()))
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is InventoryItemAddedEvent -> {
                getInventory()
            }
            is RackUpdateEvent -> {
                getInventory()
            }
        }
    }

    private fun getInventory() {
        showLoader()
        viewModel.getInventoryRack()
    }

    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
        if (p1) {
            binding?.cvCarDetails?.makeVisible(true)
        } else {
            binding?.cvCarDetails?.makeVisible(false)
        }
    }

}