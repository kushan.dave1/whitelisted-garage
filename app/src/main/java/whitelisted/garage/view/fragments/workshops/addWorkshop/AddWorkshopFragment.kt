package whitelisted.garage.view.fragments.workshops.addWorkshop

import android.Manifest
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Typeface
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.maps.model.LatLng
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.AddWorkshopRequest
import whitelisted.garage.api.request.AddressData
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentAddWorkshopBinding
import whitelisted.garage.eventBus.LocationSelectedEvent
import whitelisted.garage.eventBus.WorkshopUpdateEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.changeViewCompoundDrawablesWithInterinsicBounds
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.viewmodels.AddWorkshopViewModel
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import java.util.*

class AddWorkshopFragment : BaseFragment(), TextWatcher {

    private val binding by viewBinding(FragmentAddWorkshopBinding::inflate)
    private val viewModel: AddWorkshopViewModel by viewModel()
    private val dashboardSharedViewModel: DashboardSharedViewModel by sharedViewModel()
    private lateinit var addWorkshopRequest: AddWorkshopRequest
    private lateinit var profilePicture: Uri
    private var fontMedium: Typeface? = null
    private var fontSemiBold: Typeface? = null
    private var selectedShopsList = mutableListOf(AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
    private lateinit var selectedLocation: LatLng
    private var isDistributor = false


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setupScreen() {
        try {
            fontMedium = ResourcesCompat.getFont(requireContext(), R.font.gilroy_medium)
            fontSemiBold = ResourcesCompat.getFont(requireContext(), R.font.gilroy_semibold)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        setData()
        setClickListeners()
        addObservers()
        setPageEvent(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_SHOP_CREATION,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ADD_SHOP,
        )
    }

    private fun setData() {
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                AppENUM.RefactoredStrings.defaultCountryCode) == AppENUM.RefactoredStrings.defaultCountryCode) {
            binding.etGSTNumber.makeVisible(true)
        } else {
            binding.etGSTNumber.makeVisible(false)
        }
    }

    private fun setClickListeners() {
        binding.imgBack.setOnClickListener(this)
        binding.btnAdd.setOnClickListener(this)
        binding.imgWorkshopImage.setOnClickListener(this)
        binding.viewMapClick.setOnClickListener(this)
        binding.etGarageAddress.setOnClickListener(this)
        binding.etWorkshopName.addTextChangedListener(this)
        binding.viewWorkshop.setOnClickListener(this)
        binding.viewAccessories.setOnClickListener(this)
        binding.viewRetailer.setOnClickListener(this)
        binding.clDistributorCheck.setOnClickListener(this)
    }

    private fun addObservers() {
        observeAddWorkshopResponse()
        observeUploadImageResponse()
    }

    private fun observeUploadImageResponse() {
        viewModel.provideUploadWorkshopImageResponse().observe(viewLifecycleOwner) {
            hideLoader()
            if (it == "") {
                popBackStackAndHideKeyboard()
                EventBus.getDefault().post(WorkshopUpdateEvent())
            } else CommonUtils.showToast(requireContext(), it, true)
        }
    }

    private fun observeAddWorkshopResponse() {
        viewModel.provideAddWorkshopResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (::profilePicture.isInitialized) {
                        showLoader()
                        viewModel.uploadProfilePicture(requireContext(),
                            profilePicture,
                            it.body.data.workshopId)
                    } else {
                        popBackStackAndHideKeyboard()
                    }
                    EventBus.getDefault().post(WorkshopUpdateEvent())
                    dashboardSharedViewModel.provideIsWorkshopRefresh().postValue(true)
                }
                is Result.Failure -> {
                    hideLoader()
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.btnAdd -> {
                if (checkForm() && !isLoading()) {
                    lifecycleScope.launch {
                        showLoader()
                        if (binding.etGSTNumber.text.toString().isNotEmpty()) {
                            val verifyGSTResponse =
                                viewModel.checkGST(binding.etGSTNumber.text.toString())
                            verifyGSTResponse?.let {
                                if ((it.gstinStatus ?: 0) == 1) {
                                    firebaseAddWorkshopEvent()
                                    showLoader()
                                    viewModel.addWorkshopAPI(addWorkshopRequest)
                                } else {
                                    hideLoader()
                                    CommonUtils.showToast(requireContext(), it.message ?: "")
                                }
                            } ?: run {
                                hideLoader()
                                CommonUtils.showToast(requireContext(),
                                    getString(R.string.error_invalid_gstt))
                            }
                        } else {
                            firebaseAddWorkshopEvent()
                            showLoader()
                            viewModel.addWorkshopAPI(addWorkshopRequest)
                        }
                    }
                }
            }
            R.id.imgWorkshopImage -> {
                openImagePicker()
            }
            R.id.viewMapClick -> {
                copyIdOnnClick()
            }
            R.id.etGarageAddress -> {
                openAddressPickerFrag()
            }
            R.id.viewWorkshop -> {
                viewWorkshopType()
            }
            R.id.viewAccessories -> {
                vieAccType()
            }
            R.id.viewRetailer -> {
                viewRetailerType()
            }
            R.id.clDistributorCheck -> {
                if (isDistributor) {
                    isDistributor = false
                    binding.tvDistributor.changeViewCompoundDrawablesWithInterinsicBounds(
                        drawableEnd = ContextCompat.getDrawable(requireContext(),
                            R.drawable.ic_circle_unchecked))
                } else {
                    isDistributor = true
                    binding.tvDistributor.changeViewCompoundDrawablesWithInterinsicBounds(
                        drawableEnd = ContextCompat.getDrawable(requireContext(),
                            R.drawable.ic_check_circle))
                }
            }
        }
    }

    private fun viewRetailerType() {
        binding.tvRetailer.isEnabled = true
        if (fontSemiBold != null) binding.tvRetailer.typeface = fontSemiBold
        if (!selectedShopsList.contains(AppENUM.RefactoredStrings.RETAILER_CONSTANT)) selectedShopsList.add(
            AppENUM.RefactoredStrings.RETAILER_CONSTANT)

        binding.tvWorkshop.isEnabled = false
        if (fontMedium != null) binding.tvWorkshop.typeface = fontMedium
        selectedShopsList.remove(AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
        binding.tvAccessories.isEnabled = false
        if (fontMedium != null) binding.tvAccessories.typeface = fontMedium
        selectedShopsList.remove(AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT)
        binding.clDistributorCheck.makeVisible(true)
    }

    private fun vieAccType() {
        binding.tvAccessories.isEnabled = true
        if (fontSemiBold != null) binding.tvAccessories.typeface = fontSemiBold
        if (!selectedShopsList.contains(AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT)) selectedShopsList.add(
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT)

        binding.tvWorkshop.isEnabled = false
        if (fontMedium != null) binding.tvWorkshop.typeface = fontMedium
        selectedShopsList.remove(AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
        binding.tvRetailer.isEnabled = false
        if (fontMedium != null) binding.tvRetailer.typeface = fontMedium
        selectedShopsList.remove(AppENUM.RefactoredStrings.RETAILER_CONSTANT)
        binding.clDistributorCheck.makeVisible(true)
    }

    private fun viewWorkshopType() {
        binding.tvWorkshop.isEnabled = true
        fontSemiBold?.let {
            binding.tvWorkshop.typeface = it
        }
        if (!selectedShopsList.contains(AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) selectedShopsList.add(
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
        binding.tvAccessories.isEnabled = false
        fontMedium?.let {
            binding.tvAccessories.typeface = it
            binding.tvRetailer.typeface = it
        }
        selectedShopsList.remove(AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT)
        binding.tvRetailer.isEnabled = false
        selectedShopsList.remove(AppENUM.RefactoredStrings.RETAILER_CONSTANT)
        binding.clDistributorCheck.makeVisible(false)
    }

    private fun openAddressPickerFrag() {
        CommonUtils.addFragmentUtil(activity,
            FragmentFactory.fragment(FragmentFactory.Fragments.ADDRESS_FRAGMENT, null),
            target = R.id.fragment_container_2)
    }

    private fun copyIdOnnClick() {
        if (binding.etGarageMapLink.text.toString().isNotEmpty()) {
            val clipboardManager =
                requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText("text", binding.etGarageMapLink.text.toString())
            clipboardManager.setPrimaryClip(clipData)
            CommonUtils.showToast(requireContext(), getString(R.string.text_coppied))
        } else {
            CommonUtils.showToast(requireContext(), getString(R.string.link_not_available))
        }
    }

    private fun openImagePicker() {
        TedPermission.create().setPermissionListener(object : PermissionListener {
            override fun onPermissionGranted() {
                TedImagePicker.with(requireContext()).showCameraTile(true).start {
                    onImagePicked(it)
                }
            }

            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
                CommonUtils.showToast(context, resources.getString(R.string.permission_denied))
            }

        }).setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .check()
    }

    private fun firebaseAddWorkshopEvent() {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ADD_SHOP)
            putStringArrayList(FirebaseAnalyticsLog.FirebaseEventNameENUM.SHOP_TYPE,
                selectedShopsList as ArrayList<String>)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_SHOP_CREATION_SAVE,
                this)
        }
    }

    private fun checkForm(): Boolean {
        addWorkshopRequest = AddWorkshopRequest()
        addWorkshopRequest.ownerId =
            viewModel.getStringSharedPreference(AppENUM.USER_ID, "").toLong()
        return when {
            binding.etWorkshopName.text?.isEmpty() == true -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.empty_workshop_name),
                    true)
                false
            }
            binding.etOwnerName.text?.isEmpty() == true -> {
                CommonUtils.showToast(requireContext(), getString(R.string.empty_owner_name), true)
                false
            }
            binding.etInvoiceName.text?.isEmpty() == true -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.empty_invoice_name),
                    true)
                false
            }
            binding.etGarageAddress.text?.isEmpty() == true -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.empty_workshop_address),
                    true)
                false
            }
            selectedShopsList.isEmpty() -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.alert_empty_select_shop),
                    true)
                false
            }
            else -> {
                addWorkshopRequest.apply {
                    this.types = selectedShopsList[0]
                    this.workshopName = binding.etWorkshopName.text.toString()
                    this.ownerName = binding.etOwnerName.text.toString()
                    this.address = binding.etGarageAddress.text.toString()
                    this.mapLink = binding.etGarageMapLink.text.toString()
                    this.gstIn = binding.etGSTNumber.text.toString()
                    this.invoiceName = binding.etInvoiceName.text.toString()
                    if (isDistributor) ownerType = AppENUM.OwnerTypeENUM.DISTRIBUTOR
                    if (binding.etCity.text.toString().trim().isEmpty()){
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.please_enter_city),
                            true)
                        binding.etCity.requestFocus()
                        return false
                    }
                    if (binding.etState.text.toString().trim().isEmpty()){
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.please_enter_state),
                            true)
                        binding.etState.requestFocus()
                        return false
                    }
                    if (binding.etPincode.text.toString().trim().isEmpty()){
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.please_enter_pincode),
                            true)
                        binding.etPincode.requestFocus()
                        return false
                    }

                    if (::selectedLocation.isInitialized) {
                        this.latitude = selectedLocation.latitude
                        this.longitude = selectedLocation.longitude
                        try {
                            val gcd = Geocoder(requireContext(), Locale.getDefault())
                            val addresses: List<Address>? =
                                gcd.getFromLocation(selectedLocation.latitude,
                                    selectedLocation.longitude,
                                    1)
                            if (addresses?.isNotEmpty() == true) {
                                this.city = binding.etCity.text.toString()
                                this.state = binding.etState.text.toString()
                                this.country = addresses[0].countryName
                                this.pin = binding.etPincode.text.toString()
                            } else { // not found
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
                true
            }
        }
    }

    private fun onImagePicked(imageUri: Uri) {
        profilePicture = imageUri
        loadCircularImage(profilePicture.toString(), binding.imgWorkshopImage)
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is LocationSelectedEvent -> {
                (event.extra as? AddressData)?.let {
                    binding.etGarageAddress.setText(it.fullAddress)
                    "${AppENUM.MAP_URL}${it.latitude},${it.longitude}".apply {
                        binding.etGarageMapLink.setText(this)
                    }
                    binding.etState.setText( CommonUtils.getStateFromLocation(requireContext(),
                        it.latitude ?: 0.0,
                        it.longitude ?: 0.0))
                    binding.etCity.setText( CommonUtils.getCityFromLocation(requireContext(),
                        it.latitude ?: 0.0,
                        it.longitude ?: 0.0))
                    binding.etPincode.setText( CommonUtils.getPinCodeFromLocation(requireContext(),
                        it.latitude ?: 0.0,
                        it.longitude ?: 0.0))
                    selectedLocation = LatLng(it.latitude ?: 0.0, it.longitude ?: 0.0)
                }
            }
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { //
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { // nothing
    }

    override fun afterTextChanged(s: Editable?) {
        binding.etInvoiceName.setText("")
        binding.etInvoiceName.setText(s.toString())
    }
}