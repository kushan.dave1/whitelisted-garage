package whitelisted.garage.view.fragments.inventory

import android.app.Activity
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.CompoundButton
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import com.github.dhaval2404.imagepicker.ImagePicker
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.CreateInventoryRackRequest
import whitelisted.garage.api.request.RackItem
import whitelisted.garage.api.request.UpdateInventoryItemRequest
import whitelisted.garage.api.response.*
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentAddRackItemBinding
import whitelisted.garage.eventBus.InventoryItemAddedEvent
import whitelisted.garage.eventBus.RackUpdateEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeBackgroundDrawable
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.changeViewCompoundDrawablesWithInterinsicBounds
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.utils.ImageLoader
import whitelisted.garage.view.adapter.RackItemPartImageAdapter
import whitelisted.garage.viewmodels.AddRackItemFragmentViewModel
import whitelisted.garage.viewmodels.SelectCarDialogViewModel

class EditRackItemFragment : BaseFragment(), CompoundButton.OnCheckedChangeListener {

    private val binding by viewBinding(FragmentAddRackItemBinding::inflate)
    private val viewModel: AddRackItemFragmentViewModel by viewModel()
    private val selectCarViewModel: SelectCarDialogViewModel by sharedViewModel()
    private var yearRange = arrayListOf<String>()
    private var quantityCount = 0
    private var rackPartImageAdapter: RackItemPartImageAdapter? = null
    private var inventoryData: GetInventoryRackResponse? = null

    //    private var isBrandSelected = false
    //    private var isModelSelected = false
    private var brandID = 0
    private var carModelID = 0
    private var skuID = ""
    private var positionTobeEdited = 0
    private var rackData: RackItem? = null
    private var lisPfUri = mutableListOf<Uri>()
    private var isTwoWheeler = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListener()
        getData()
        setUpData()
        observeData()
        viewModel.getDateYearRange()
    }

    private fun clickListener() {
        binding.imgMenu.setOnClickListener(this)
        binding.tvSelectBrand.setOnClickListener(this)
        binding.imgMinuse.setOnClickListener(this)
        binding.imgPluse.setOnClickListener(this)
        binding.llAddRackItem.setOnClickListener(this)
        binding.autoTvPartItem.setOnClickListener(this)
        binding.tvSelectModel.setOnClickListener(this)
        binding.tvEnterFuelType.setOnClickListener(this)
        binding.checkBox.setOnCheckedChangeListener(this)
        binding.imgSwitchVehicleType.setOnClickListener(this)
    }

    private fun getData() {
        arguments?.let { bundle ->
            inventoryData = bundle.getParcelable(AppENUM.INVENTORY_DATA)
            positionTobeEdited = bundle.getInt(AppENUM.POSITION, 0)
            binding.tvHeader.text =
                String.format(getString(R.string.rack_placeholder), inventoryData?.name)
        }
    }

    private fun setUpData() {
        rackPartImageAdapter = RackItemPartImageAdapter(this)
        binding.rvRackImage.adapter = rackPartImageAdapter
        lisPfUri.add(Uri.EMPTY)
        rackPartImageAdapter?.setData(lisPfUri)
        binding.tvAddItem.text = getString(R.string.update_item)
    }

    private fun observeData() {
        observeYearRange()
        observeUpdateInventory()
    }

    private fun observeUpdateInventory() {
        viewModel.provideUpdateInventoryResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message)
                    EventBus.getDefault().post(InventoryItemAddedEvent())
                    EventBus.getDefault().post(RackUpdateEvent())
                    popBackStackAndHideKeyboard()
                }
                is Result.Failure -> CommonUtils.showToast(activity, it.errorMessage, true)
            }
        }
        viewModel.provideUpdateInventoryPriceResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> { //
                }
                is Result.Failure -> CommonUtils.showToast(activity, it.errorMessage, true)
            }
        }
    }


    private fun observeYearRange() {
        viewModel.provideDateYearRange().observe(viewLifecycleOwner) {
            yearRange = it ?: arrayListOf()
            ArrayAdapter(requireContext(),
                R.layout.support_simple_spinner_dropdown_item,
                yearRange).apply {
                (binding.autoYearMF.editText as? AutoCompleteTextView)?.setAdapter(this)
            }
        }
        setUpDataToView()
    }

    private fun setUpDataToView() {
        rackData = inventoryData?.rack_items?.get(positionTobeEdited)
        rackData?.let { data ->
            setDataToRack(data)
        }
    }

    private fun setDataToRack(data: RackItem) {
        if (data.brandName != AppENUM.GENERIC) {
            binding.checkBox.isChecked = true
            if (!data.brandName.isNullOrEmpty() && data.brandName != AppENUM.NULL) {
                binding.tvSelectBrand.text = data.brandName
                selectCarViewModel.provideSelectedBrand()
                    .postValue(BrandsResponseModel(name = data.brandName, id = data.brandId))
            }
            if (!data.carModel.isNullOrEmpty() && data.carModel != AppENUM.NULL) {
                binding.tvSelectModel.text = data.carModel
                selectCarViewModel.provideSelectedModel()
                    .postValue(CarModelResponseModel(carName = data.brandName,
                        modelId = data.brandId))
            }
            if (!data.fuel_type.isNullOrEmpty() && data.fuel_type != AppENUM.NULL) {
                binding.tvEnterFuelType.text = data.fuel_type
                selectCarViewModel.provideSelectedFuelType()
                    .postValue(TypeItem(type = data.fuel_type))
            }
            if (!data.mf_year.isNullOrEmpty() && data.mf_year != AppENUM.NULL) binding.autoTvYearMF.setText(
                data.mf_year)
        } else {
            binding.checkBox.isChecked = false
        }
        carModelID = data.carModelId ?: 0
        brandID = data.brandId ?: 0
        binding.autoTvPartItem.setText(data.partName)
        binding.tvQuantity.setText(data.quantity.toString())
        quantityCount = data.quantity ?: 0
        binding.etBuyingPrice.setText((data.buyingPrice ?: 0.0).toString())
        binding.etSellingPrice.setText((data.selling_price ?: 0.0).toString())
        skuID = data.skuId ?: "0"
        for (v in data.images ?: mutableListOf()) {
            lisPfUri.add(Uri.parse(v))
        }
        rackPartImageAdapter?.setData(lisPfUri)

        isTwoWheeler = data.vehicleType == 2
        if (isTwoWheeler) {
            binding.lblFourWheeler.changeTextColor(requireContext(), R.color.black_text_new)
            binding.lblTwoWheeler.changeTextColor(requireContext(), R.color.colorAccent)
            binding.lblFourWheeler.changeViewCompoundDrawablesWithInterinsicBounds(
                ContextCompat.getDrawable(requireContext(),
                    R.drawable.ic_car_type_unselectetd))
            binding.lblTwoWheeler.changeViewCompoundDrawablesWithInterinsicBounds(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_two_wheeler))
            ImageLoader.loadDrawable(ContextCompat.getDrawable(requireContext(),
                R.drawable.ic_double_switch_on), binding.imgSwitchVehicleType)
        }
    }

    private fun openImage() {
        if (binding.autoTvPartItem.text.toString().trim().isEmpty()) {
            CommonUtils.showToast(requireContext(), getString(R.string.select_car_part_item))
        } else {
            openAddImageView(binding.autoTvPartItem.text.toString())
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.llDelImage -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.llDelImage))?.let {
                    rackPartImageAdapter?.dataList?.removeAt(it)
                    rackPartImageAdapter?.notifyItemRemoved(it)
                }
            }
            R.id.rlAddImage -> openImage()
            R.id.autoTvPartItem -> showPartSelectDialog()
            R.id.imgMenu -> popBackStackAndHideKeyboard()
            R.id.tvSelectBrand -> showBrandsDialog(0)
            R.id.tvSelectModel -> {
                handleModelSelection()
            }
            R.id.tvEnterFuelType -> {
                handleFuelTypeSelection()
            }
            R.id.imgPluse -> {
                quantityCount++
                binding.tvQuantity.setText(quantityCount.toString())
                binding.imgMinuse.setImageResource(R.drawable.ic_rack_minuse_no_alpha)
            }
            R.id.imgMinuse -> {
                if (binding.tvQuantity.text.toString() != "0") {
                    quantityCount--
                    if (quantityCount == 0) {
                        binding.imgMinuse.setImageResource(R.drawable.ic_rack_minuse)
                    }
                }
                binding.tvQuantity.setText(quantityCount.toString())
            }
            R.id.llAddRackItem -> {
                handleAddRackItem()
            }
            R.id.imgSwitchVehicleType -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.cannot_change_vehicle_type_in_existinig_item))
            }
        }
    }

    private fun handleAddRackItem() {
        if (validateMandatoryFields()) {
            inventoryData?.rack_items?.set(positionTobeEdited, getRackItem())
            updateGenericPriceOfRackItem()
            showLoader()
            viewModel.updateInventoryRack(inventoryData?.id.toString(),
                CreateInventoryRackRequest().apply {
                    rackItems = inventoryData?.rack_items
                    rackName = inventoryData?.name
                    images = inventoryData?.images
                    isNewRack = false
                })
        }
    }

    private fun getRackItem(): RackItem {
        return RackItem().apply {
            buyingPrice = binding.etBuyingPrice.text.toString().toDoubleOrNull() ?: 0.0
            partName = binding.autoTvPartItem.text.toString()
            quantity = binding.tvQuantity.text.toString().toIntOrNull() ?: 0
            mf_year = binding.autoTvYearMF.text.toString()
            selling_price = binding.etSellingPrice.text.toString().toDoubleOrNull() ?: 0.0
            skuId = skuID
            images = rackPartImageAdapter?.getAdapterList()
            if (binding.checkBox.isChecked) {
                brandName = binding.tvSelectBrand.text.toString()
                fuel_type = binding.tvEnterFuelType.text.toString()
                carModel = binding.tvSelectModel.text.toString()
                carModelId = carModelID
                brandId = brandID
            } else {
                brandName = AppENUM.GENERIC
                fuel_type = AppENUM.GENERIC
                carModel = AppENUM.GENERIC
                carModelId = -999999
                brandId = -999999
            }
        }
    }

    private fun updateGenericPriceOfRackItem() {
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) {
            viewModel.updateInventoryItemValueForWorkshop(UpdateInventoryItemRequest().apply {
                id = skuID
                selling_price = binding.etSellingPrice.text.toString().toDoubleOrNull() ?: 0.0
                buying_price = binding.etBuyingPrice.text.toString().toDoubleOrNull() ?: 0.0
            })
        } else {
            viewModel.updateInventoryItemValue(UpdateInventoryItemRequest().apply {
                sku_id = skuID
                selling_price = binding.etSellingPrice.text.toString().toDoubleOrNull() ?: 0.0
                buying_price = binding.etBuyingPrice.text.toString().toDoubleOrNull() ?: 0.0
            })
        }
    }

    private fun handleFuelTypeSelection() {
        if (binding.tvSelectModel.text.toString().trim().isEmpty()) {
            CommonUtils.showToast(requireContext(), getString(R.string.select_model_frist))
        } else {
            showBrandsDialog(2)
        }
    }

    private fun handleModelSelection() {
        if (binding.tvSelectBrand.text.toString().trim().isEmpty()) {
            CommonUtils.showToast(requireContext(), getString(R.string.select_brand_frist))
        } else {
            showBrandsDialog(1) //  showCarModelDialog()
        }
    }

    private fun validateMandatoryFields(): Boolean {
        if (binding.checkBox.isChecked) {
            if (binding.tvSelectBrand.text.toString().trim()
                    .isEmpty() || binding.tvSelectBrand.text == getString(R.string.select_brand)) {
                CommonUtils.showToast(requireContext(), getString(R.string.brand_can_not_be_empty))
                return false
            }
            if (binding.tvSelectModel.text.toString().trim()
                    .isEmpty() || binding.tvSelectModel.text == getString(R.string.select_model)) {
                CommonUtils.showToast(requireContext(), getString(R.string.model_can_not_be_empty))
                return false
            } //            if (binding.tvEnterFuelType.text.toString().trim()
            //                    .isEmpty() || binding.tvEnterFuelType.text == getString(R.string.enter_fuel_type)) {
            //                CommonUtils.showToast(requireContext(), getString(R.string.fule_can_not_be_empty))
            //                return false
            //            }
        }
        if (binding.autoTvPartItem.text.toString().trim().isEmpty()) {
            CommonUtils.showToast(requireContext(), getString(R.string.part_can_not_be_empty))
            return false
        }
        if (binding.etBuyingPrice.text.toString().trim()
                .isEmpty() || binding.etBuyingPrice.text.toString() == "0") {
            CommonUtils.showToast(requireContext(),
                getString(R.string.bying_price_can_not_be_empty))
            return false
        }
        if (binding.etSellingPrice.text.toString().trim()
                .isEmpty() || binding.etBuyingPrice.text.toString() == "0") {
            CommonUtils.showToast(requireContext(),
                getString(R.string.selling_price_not_empty))
            return false
        }
        if (binding.tvQuantity.text.toString().trim()
                .isEmpty() || binding.tvQuantity.text.toString() == "0") {
            CommonUtils.showToast(requireContext(), getString(R.string.quantity_can_not_be_empty))
            return false
        }
        return true
    }

    private fun showBrandsDialog(type: Int) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_CAR, Bundle().apply {
            putInt(AppENUM.CAR_SEARCH_TYPE, type)
            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, isTwoWheeler)
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                when (extra as Int) {
                    0 -> { //showCarModelDialog()
                    }
                    1 -> { //
                    }
                    2 -> {
                        selectCarViewModel.provideSelectedModel().value?.let {
                            binding.tvSelectModel.text = it.carName
                            brandID = it.brandId ?: 0
                            carModelID = it.modelId ?: 0
                        }
                        binding.tvSelectBrand.text =
                            selectCarViewModel.provideSelectedBrand().value?.name
                        binding.tvEnterFuelType.text = ""
                        binding.tvEnterFuelType.text =
                            selectCarViewModel.provideSelectedFuelType().value?.type
                    }
                }
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun showPartSelectDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_RACK_ITEM, Bundle().apply {
            putBoolean(AppENUM.IS_INVENTORY, true)
            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, isTwoWheeler)
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                (extra as? GetInventoryPartItemResponse)?.let {
                    skuID = it.skuId ?: "0"
                    binding.autoTvPartItem.setText(it.subCategoryName)
                    openAddImageView(it.subCategoryName.toString())
                }
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }


    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            hideLoader()
            if (result.resultCode == Activity.RESULT_OK) result.data?.data?.let { //Image Uri will not be null for RESULT_OK

                lisPfUri.add(it)
                rackPartImageAdapter?.setData(lisPfUri)

            } else if (result.resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(requireContext(),
                    ImagePicker.getError(result.data),
                    Toast.LENGTH_SHORT).show()
            }
        }


    private fun openAddImageView(searchString: String) {
        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.CHOOSE_IMAGE, Bundle().apply {
            putString(AppENUM.SEARCH_STRING, searchString)
            putBoolean(AppENUM.SINGLE_IMAGE, false)
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                val uri = extra as? Uri?
                if (uri == Uri.EMPTY) {
                    ImagePicker.with(requireActivity()).compress(512).maxResultSize(512, 512)
                        .setImageProviderInterceptor {
                            showLoader()
                        }.createIntent { intent ->
                            startForProfileImageResult.launch(intent)
                        }
                } else CommonUtils.genericCastOrNull<MutableList<Uri>>(extra)?.let {
                    if ((rackPartImageAdapter?.getDataSetSize() ?: 0) > 9 || it.size > 9) {
                        CommonUtils.showToast(requireContext(), getString(R.string.max_8_image))
                    } else {
                        lisPfUri.addAll(it)
                        rackPartImageAdapter?.setData(lisPfUri)
                    }
                }
            }
        })?.let { baseFragment ->
            baseFragment.show(requireActivity().supportFragmentManager, baseFragment.javaClass.name)
        }
    }

    override fun onCheckedChanged(button: CompoundButton, p1: Boolean) {
        if (binding.checkBox.isChecked) {
            binding.llCarDetails.makeVisible(true)
            binding.lineOrange.makeVisible(true)
            binding.llAddCarDetails.changeBackgroundDrawable(requireContext(),
                R.drawable.bg_add_rack_fields)
        } else {
            binding.llAddCarDetails.changeBackgroundDrawable(requireContext(),
                R.drawable.bg_add_car_details)
            binding.llCarDetails.makeVisible(false)
            binding.lineOrange.makeVisible(false)
        }
    }
}