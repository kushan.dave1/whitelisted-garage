package whitelisted.garage.view.fragments.sparesShop

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.Category
import whitelisted.garage.api.response.SSCategoryResponseModel
import whitelisted.garage.api.response.SubArray
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentSelectCategoryBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.CategoryFilterAdapter
import whitelisted.garage.view.adapter.SSSubCategoriesAdapter
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import java.util.*

class SelectCategoryFragment(val showToolbar: Boolean = true) : BaseFragment() {

    private val viewModel: SparesShopFragmentViewModel by sharedViewModel()
    private val binding by viewBinding(FragmentSelectCategoryBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupScreen()
    }

    private fun setupScreen() {
        binding.clHeaderFSC.makeVisible(showToolbar)
        binding.imgBackFSC.setOnClickListener { popBackStackAndHideKeyboard() }

        viewModel.provideSearchAndFilterAttributes().categories.let { catList ->
            if (catList.isNotEmpty()) {
                setScreenData(catList)
            } else {
                showLoader()
                observeSearchAndFilters()
            }
        }
    }

    private fun observeSearchAndFilters() {
        viewModel.provideSearchAndFiltersResponse().observe(viewLifecycleOwner) {
            hideLoader()
            setScreenData(it.categories)
        }
    }

    private fun setScreenData(catList: List<Category>) {
        arguments?.let { bundle ->
            setCategoriesAdapter(catList)
            if (bundle.getBoolean(AppENUM.IS_FROM_OUTSIDE, false)) {
                bundle.getInt(AppENUM.POSITION, 0)
                    .let { selPosition -> //                    binding.tvCategoryName.text = catList[selPosition].categoryName
                        catList.forEach {
                            it.isSelected = false
                        }
                        if (selPosition >= 0) {
                            catList[selPosition].isSelected = true
                            setRecyclerForCategory(catList[selPosition], selPosition)
                        } else if (selPosition==-1){
                            var i = 0
                            kotlin.run breaking@{
                                catList.forEach {
                                    if (it.categoryName?.toLowerCase(Locale.getDefault()) == "accessories") {
                                        catList[i].isSelected = true
                                        setRecyclerForCategory(catList[i], i)
                                        return@breaking
                                    }
                                    i++
                                }
                            }
                        }else{
                            var i = 0
                            kotlin.run breaking@{
                                catList.forEach {
                                    if (it.categoryName?.toLowerCase(Locale.getDefault()) == "bike categories") {
                                        catList[i].isSelected = true
                                        setRecyclerForCategory(catList[i], i)
                                        return@breaking
                                    }
                                    i++
                                }
                            }
                        }
                    }
            } else {
                bundle.getParcelable<SSCategoryResponseModel>(AppENUM.CATEGORY_DATA)
                    ?.let { selectedCategory ->
                        var i = 0
                        catList.forEach {
                            it.isSelected = false
                            if (it.categoryName == selectedCategory.categoriesName) {
                                it.isSelected = true
                                setRecyclerForCategory(it, i)
                            }
                            i++
                        } //                        binding.tvCategoryName.text = selectedCategory.name
                    }
            }
        }
    }

    private fun setCategoriesAdapter(catList: List<Category>) {
        binding.rvCategoryFSC.adapter = CategoryFilterAdapter(catList, this)
    }

    private fun setRecyclerForCategory(category: Category?, position: Int) {
        lifecycleScope.launch {
            delay(200)
            (binding.rvCategoryFSC.adapter as? CategoryFilterAdapter)?.let { adapter ->
                adapter.notifyDataSetChanged()
                binding.rvCategoryFSC.scrollToPosition(position)
            }
        }
        setSubCategoryAdapter(category)
    }

    private fun setSubCategoryAdapter(category: Category?) {
        binding.tvCategoryName.text = category?.categoryName
        category?.let {
            if (category.subCategory.isNotEmpty()) {
                (SSSubCategoriesAdapter(category.subCategory, this)).apply {
                    binding.rvSubCategories.adapter = this
                    notifyDataSetChanged()
                }
            }
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.clBaseSSISSC -> {
                CommonUtils.genericCastOrNull<SubArray>(v.getTag(R.id.model))?.let { subSubCat ->
                    if (subSubCat.isEnable == true) {
                        fireSparesSubCatEvent(subSubCat.name)
                        CommonUtils.addFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                                Bundle().apply { //                                putString(AppENUM.IntentKeysENUM.TYPE,
                                    //                                    AppENUM.RefactoredStrings.CATEGORY)
                                    putString(AppENUM.IntentKeysENUM.CATEGORY_NAME, subSubCat.name)
                                    putInt(AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION,
                                        arguments?.getInt(AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION,
                                            4) ?: 4)
                                })) //                    fireSelectCatEvent(it.categoriesName ?: "")
                    } else {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.all_products_sold_out))
                    }
                }
            }

            R.id.clBrandFilter -> {
                CommonUtils.genericCastOrNull<Category>(v.getTag(R.id.model))?.let { category ->
                    (binding.rvCategoryFSC.adapter as? CategoryFilterAdapter)?.let { catAdapter ->
                        catAdapter.dataList.forEach {
                            it.isSelected = false
                        }
                        catAdapter.dataList.find { it.categoryName == category.categoryName }?.isSelected =
                            true
                        catAdapter.notifyItemRangeChanged(0, catAdapter.dataList.size)
                    }

                    setSubCategoryAdapter(category)
                }
            }
        }
    }

    private fun fireSparesSubCatEvent(name: String) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CATEGORY_SELECTED, name)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SORT_AND_FILTER)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.SPARES_SUBCAT,
            bundle)

    }

}