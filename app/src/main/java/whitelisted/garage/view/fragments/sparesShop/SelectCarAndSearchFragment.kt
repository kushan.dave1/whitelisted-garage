package whitelisted.garage.view.fragments.sparesShop

import android.Manifest
import android.app.AlertDialog
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.textclassifier.TextClassifier
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.content.res.ResourcesCompat.getFont
import androidx.core.view.isEmpty
import androidx.core.view.setPadding
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.*
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.*
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.checkNull
import whitelisted.garage.utils.CommonUtils.lower
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.CommonUtils.px
import whitelisted.garage.utils.customViews.CustomRecyclerView
import whitelisted.garage.view.adapter.SSItemsAdapter
import whitelisted.garage.viewmodels.SelectCarDialogViewModel
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import java.io.File

class SelectCarAndSearchFragment : BaseFragment() {

    private lateinit var searches: List<SSItemModel>
    private val binding by viewBinding(FragmentSelectCarAndSearchBinding::inflate)
    private val viewModel: SparesShopFragmentViewModel by sharedViewModel()
    private val selectCarViewModel: SelectCarDialogViewModel by sharedViewModel()
    private val searchAttrs: MutableMap<String, List<Any>> = mutableMapOf()
    private val popularMap = mutableMapOf<String, List<String>>()
    private var recentAdapter: RecyclerView.Adapter<*>? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch(Dispatchers.Main) {
            delay(200)
            binding.etSearchFSSSI.requestFocus()
            CommonUtils.showKeyboard(requireActivity(), binding.etSearchFSSSI)
        }
        setupViews()
        getFilterAttributes()
    }

    private fun getRecentSearches() = lifecycleScope.launch {

        val partIds = viewModel.getRecentItems().joinToString { it.productId }
        searches = viewModel.getMultipleParts(partIds)
            .filter { !it.isEnquiry && it.inventory.checkNull() > 0 }
        addRecentSearchesAtTop()
    }

    private fun addRecentSearchesAtTop() {
        val symbol = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL)
        if (::searches.isInitialized && searches.isNotEmpty()) {
            val cartItems =
                viewModel.provideGetCartResponse().cartList.map { it.lineItems }.flatten()
            searches.onEach {
                it.quantity = cartItems.find { i -> i.productId == it.productId }?.quantity
            }

            binding.rvSuggestions.addSingleView(0, RecentSearchesCardBinding::inflate) { binding ->

                binding.rvRecentSearches.layoutManager =
                    object : LinearLayoutManager(requireContext(), HORIZONTAL, false) {
                        override fun checkLayoutParams(lp: RecyclerView.LayoutParams): Boolean {
                            lp.width = (width / 2.2).toInt()
                            return true
                        }
                    }
                recentAdapter = SSItemsAdapter(symbol,
                    searches.toMutableList(),
                    this@SelectCarAndSearchFragment,
                    showBorder = true,
                    isRecentSearchItem = true)
                binding.rvRecentSearches.adapter = recentAdapter
            }
            observeGetCartAPI()
            binding.rvSuggestions.scrollToPosition(0)
        }

    }

    private fun getFilterAttributes() = viewLifecycleOwner.lifecycleScope.launch {
        showLoader()
        val attrs = viewModel.getSearchAndFilterAttributes()
        viewModel.getSSCart()
        hideLoader()

        if (attrs != null) searchAttrs.apply {

            val allSubCategories =
                attrs.categories.asSequence().map { it.subCategory }.flatten().map { it.subArray }
                    .flatten().toList()

            /*for (i in 1 until allSubCategories.size) {

            }*/
            put(resources.getString(R.string.select_category), allSubCategories)
            put(resources.getString(R.string.select_part_brand), attrs.brands)
            put(resources.getString(R.string.select_car_model), attrs.car)
            put(resources.getString(R.string.select_part_name),
                attrs.partName) //put(resources.getString(R.string.select_part_id), attrs.part)

            popularMap[resources.getString(R.string.popular_searches)] = attrs.popularSearch
            updateRecycler(popularMap)

            getRecentSearches()
        }
    }

    private fun headerBinder(binding: ItemSearchHeaderBinding, header: String) {
        binding.tvSearchHeader.text = header
        val isPopular = header.lower() == AppENUM.POPULAR_SEARCHES.lower()
        binding.tvSearchHeader.typeface =
            getFont(requireContext(), if (isPopular) R.font.gilroy_semibold
            else R.font.gilroy_medium)
        binding.tvSearchHeader.setTextColor(resources.getColor(if (isPopular) R.color.black
        else R.color.black_trans_new_2, resources.newTheme()))
        binding.tvSearchHeader.textSize = if (isPopular) 16f else 10f
        binding.tvSearchHeader.setBackgroundColor(resources.getColor(if (isPopular) R.color.white
        else R.color.bg_color_gray_alt, resources.newTheme()))

    }

    private fun itemBinder(binding: ItemRecentSearchBinding, itemModel: Any, headerModel: String) {

        val itemName: String = getItemName(itemModel)

        val isPopular = headerModel == resources.getString(R.string.popular_searches)
        binding.tvItemNameIRS.text = itemName
        binding.ivRSD.makeVisible(isPopular)

        binding.root.setOnClickListener {
            viewModel.searchAndFilterAttrs.isImageSearch = false
            if (itemModel is String) gotoPLP(itemModel)
            else addChip(itemModel)
        }
    }

    private fun addChip(model: Any) {
        setSelected(model)
        binding.cgSelectedAttrs.makeVisible(true)

        val chip = Chip(requireContext())
        chip.text = getItemName(model)
        chip.isCloseIconVisible = true
        chip.closeIcon =
            ResourcesCompat.getDrawable(resources, R.drawable.ic_cross_black, resources.newTheme())
        chip.closeIconTint =
            ColorStateList.valueOf(ContextCompat.getColor(requireActivity(), R.color.close_tint))
        chip.closeIconSize = 32.0f
        chip.isClickable = false
        chip.elevation = 0.0f
        chip.setTextAppearanceResource(R.style.ChipTextStyle)
        chip.setPadding(10.px)
        chip.chipBackgroundColor =
            ColorStateList.valueOf(ContextCompat.getColor(requireActivity(), R.color.chip_color))
        chip.setOnCloseIconClickListener {
            setSelected(model, false)
            removeChip(chip)
        }
        binding.cgSelectedAttrs.addView(chip)
        clearSearchSuggestions()
    }

    private fun addRecentSearchChip(searchText: String) = with(Chip(requireContext())) {

        chipIcon = ResourcesCompat.getDrawable(resources,
            R.drawable.ic_recent_searches,
            resources.newTheme())
        text = searchText
        setTextAppearanceResource(R.style.ChipTextStyle2)
        iconStartPadding = 20f;
        isClickable = true; elevation = 0.0f
        chipStrokeColor =
            ColorStateList.valueOf(ContextCompat.getColor(requireActivity(), R.color.new_gray))
        chipStrokeWidth = 4f
        chipBackgroundColor =
            ColorStateList.valueOf(ContextCompat.getColor(requireActivity(), R.color.white))
        setOnClickListener { gotoPLP(searchText) } //binding.cgRecentSearches.addView(this)
    }

    private fun removeChip(chip: Chip) {

        binding.cgSelectedAttrs.removeView(chip)

        if (binding.cgSelectedAttrs.isEmpty()) {

            binding.cgSelectedAttrs.makeVisible(false)
            binding.clAutoSuggestBottom.makeVisible(false)

            if (binding.etSearchFSSSI.text.isNullOrEmpty()) {
                binding.lblSelectCar.makeVisible(true)
                updateRecycler(popularMap)
                addRecentSearchesAtTop()
            }
        }

    }

    private fun setupViews() = binding.apply {

        rvSuggestions.setLayoutManagerAsLinear()
        lblSelectCar.setOnClickListener { showSelectCarDialog() }
        imgBackFSSSI.setOnClickListener { popBackStackAndHideKeyboard() }
        btnClearText.setOnClickListener { clearSearchSuggestions() }
        tvSearchSRF.setOnClickListener { gotoPLP() }
        etSearchFSSSI.doOnTextChanged { text, _, _, _ ->
            onSearchTextChanged(text.toString())
            viewModel.searchAndFilterAttrs.isImageSearch = false
        }
        etSearchFSSSI.setOnEditorActionListener { v, _, _ ->
            val searchText = v.text.toString()
            if (searchText.isNotEmpty()) gotoPLP(searchText)
            return@setOnEditorActionListener false
        }

        val imageSearch = arguments?.getString(AppENUM.IMAGE_SEARCH,null)
        if(!imageSearch.isNullOrEmpty()) {
            setTextASImageSearch(imageSearch)
        }

        btnSearchByImage.setOnClickListener { openSelectImageView() }

    }

    private fun setTextASImageSearch(text: String) {
        viewModel.searchAndFilterAttrs.isImageSearch = true
        binding.etSearchFSSSI.setText(text)
        binding.etSearchFSSSI.setSelection(text.length)
        binding.etSearchFSSSI.requestFocus()
    }


    private fun openSelectImageView() {
        TedPermission.create().setPermissionListener(object : PermissionListener {
            override fun onPermissionGranted() {
                TedImagePicker.with(requireContext()).showCameraTile(true).start(::openTextRecognizer)
            }

            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
                CommonUtils.showToast(context, resources.getString(R.string.permission_denied))
            }

        }).setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .check()


    }


    private fun openTextRecognizer(uri: Uri) {

        val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)
        val image = InputImage.fromFilePath(requireContext(), uri)
        recognizer.process(image).addOnSuccessListener { text ->
            showRecognizedTextsDialog(text.textBlocks.map { it.text })
        }

    }


    private fun showRecognizedTextsDialog(texts: List<String>) {

        val dialogBinding = DialogImageToTextBinding.inflate(LayoutInflater.from(requireContext()))
        val builder = AlertDialog.Builder(context)
        val dialog = builder.setView(dialogBinding.root).create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogBinding.searchesRv.setLayoutManagerAsLinear()
        dialogBinding.searchesRv.addViews(texts, ItemRecentSearchBinding::inflate) { bind,text, _ ->
            bind.ivRSD.scaleX = 0.8f
            bind.ivRSD.scaleY = 0.8f
            bind.ivRSD.setImageDrawable(ResourcesCompat.getDrawable(resources,R.drawable.ic_search_gray,resources.newTheme()))
            bind.tvItemNameIRS.text = text
            bind.tvItemNameIRS.setOnClickListener {
                fireImageToTextSearchEvent(text)
                setTextASImageSearch(text)
                dialog.dismiss()
            }
        }
        dialogBinding.tvClose.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }

    private fun gotoPLP(search: String = "") {
        popBackStackAndHideKeyboard()
        viewModel.searchAndFilterAttrs.textSearch = search
        val isPlpAdded = requireActivity().supportFragmentManager.fragments.any {
            it is ShopWithFilterFragment
        }
        if (!isPlpAdded) {
            CommonUtils.addFragmentUtil(requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                    params = Bundle().apply {
                        putInt(AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION,
                            arguments?.getInt(AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION, 4) ?: 4)
                    }))
        } else viewModel.updateFilters()
        if (search.isNotEmpty()) fireSearchEvent(search)
    }

    private fun clearSearchSuggestions() {
        binding.etSearchFSSSI.setText("")
    }

    private fun onSearchTextChanged(searchText: String? = "") {
        if (searchText.isNullOrEmpty()) {

            if (binding.cgSelectedAttrs.isEmpty()) {
                binding.clAutoSuggestBottom.makeVisible(false)
                binding.lblSelectCar.makeVisible(true)
                updateRecycler(popularMap)
                addRecentSearchesAtTop()
            } else {
                binding.rvSuggestions.clear()
                binding.lblSelectCar.makeVisible(false)
                binding.clAutoSuggestBottom.makeVisible(true)
            }
        } else {
            binding.lblSelectCar.makeVisible(false)
            search(searchText)
        }

    }

    private fun search(searchText: String) {
        val searchMap = mutableMapOf<String, List<Any>>()
        searchAttrs.forEach { (t, u) ->
            if (!u.any { checkIfSelected(it) }) {
                val list = u.filter {
                    getItemName(it).lower().contains(searchText.lower())
                }
                if (list.isNotEmpty()) searchMap[t] = list
            }
        }
        if (searchMap.isEmpty()) updateRecycler(popularMap)
        else updateRecycler(searchMap)
    }

    private fun updateRecycler(map: Map<String, List<Any>>) {
        binding.rvSuggestions.clear().addViewsWithHeader(map,
            ItemSearchHeaderBinding::inflate,
            ItemRecentSearchBinding::inflate,
            ::headerBinder,
            ::itemBinder)
    }

    private fun showSelectCarDialog() = binding.apply {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_CAR, Bundle().apply {
            putInt(AppENUM.CAR_SEARCH_TYPE, 0)
            putBoolean(AppENUM.IS_FROM_SS_FILTER, true)
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                val carName = selectCarViewModel.provideSelectedModel().value?.carName
                if (carName != null) {
                    val car =
                        viewModel.searchAndFilterAttrs.car.find { it.carName.contains(carName) }
                    car?.let { addChip(it) }
                }
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }


    private fun getItemName(itemModel: Any): String {
        return when (itemModel) {
            is SubArray -> itemModel.name
            is Brand -> itemModel.name
            is CarAttr -> itemModel.carName
            is String -> itemModel
            else -> ""
        }
    }

    private fun checkIfSelected(itemModel: Any): Boolean {
        return when (itemModel) {
            is SubArray -> itemModel.isSelected
            is Brand -> itemModel.isSelected
            is CarAttr -> itemModel.isSelected
            else -> false
        }
    }

    private fun setSelected(itemModel: Any, selected: Boolean = true) {
        when (itemModel) {
            is SubArray -> {
                viewModel.searchAndFilterAttrs.categoryCount = if (selected) 1 else 0
                itemModel.isSelected = selected
            }
            is Brand -> {
                viewModel.searchAndFilterAttrs.brandCount = if (selected) 1 else 0
                itemModel.isSelected = selected
            }
            is CarAttr -> itemModel.isSelected = selected
        }
    }

    private fun observeGetCartAPI() {
        viewModel.provideGetCartResponseAPI().observe(viewLifecycleOwner) { result ->
            hideLoader()
            (result as? Result.Success)?.body?.data?.let {
                checkChangesInRecentItems(it.cartList.map { c -> c.lineItems }.flatten())
            }
        }
    }

    private fun checkChangesInRecentItems(lineItems: List<SSItemModel>) {
        if (::searches.isInitialized) {
            searches.forEachIndexed { index, ssItemModel ->
                val item = lineItems.find { ssItemModel.productId == it.productId }
                item?.let {
                    if (ssItemModel.quantity != it.quantity) {
                        ssItemModel.quantity = it.quantity ?: 0
                        recentAdapter?.notifyItemChanged(index)
                    }
                } ?: kotlin.run {
                    ssItemModel.quantity = 0
                    recentAdapter?.notifyItemChanged(index)
                }
            }
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnAdd -> {
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.apply {
                    type = 1
                    showLoader()
                    viewModel.addToCart(productId, this)
                }
            }

            R.id.imgPlus -> {
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.apply {
                    type = 1
                    showLoader()
                    viewModel.addToCart(productId, this)
                }
            }

            R.id.imgMinus -> {
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.apply {
                    type = 0
                    showLoader()
                    viewModel.addToCart(productId, this)
                }
            }

            R.id.clISI -> {
                CommonUtils.genericCastOrNull<SSItemModel>(v.getTag(R.id.model))?.let {
                    fireRecentSearchesEvent(it)
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_PRODUCT_DETAILS,
                            Bundle().apply {
                                putParcelable(AppENUM.SSITEM, it)
                            }))
                }
            }
        }
    }


    private fun fireRecentSearchesEvent(item: SSItemModel) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, item.title)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_ENQUIRY, item.isEnquiry)
        bundle.putBoolean(FirebaseAnalyticsLog.FirebaseEventNameENUM.IS_FASTMOVING,
            item.isfastmoving)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.POPULAR_TAG, item.tag?.tagName)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_ID, item.id?.oid ?: "")
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_BRAND, item.brand ?: "")
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_PRICE,
            item.gomPrice.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_TYPE, item.skuCategory)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.MARKETPLACE_SEARCH)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.RECENT_SEARCHES,
            bundle)
    }

    private fun fireSearchEvent(text: String) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FBE_SEARCH_TERM, text)

        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_MARKETPLACE_SEARCH)

        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.MARKETPLACE_SEARCH,
            bundle)
    }


    private fun fireImageToTextSearchEvent(text: String) {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_MARKETPLACE_SEARCH
        )
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.SEARCH_TEXT, text)
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.SERIAL_NO_SCANNER,
            bundle
        )
    }
}