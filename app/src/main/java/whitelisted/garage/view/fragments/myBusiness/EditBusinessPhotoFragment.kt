package whitelisted.garage.view.fragments.myBusiness

import android.Manifest
import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import gun0912.tedimagepicker.builder.TedImagePicker
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentEditBusPhotoBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.view.adapter.PhotoAdapter
import whitelisted.garage.viewmodels.MyBusinessViewModel

class EditBusinessPhotoFragment : BaseFragment() {

    private val viewModel: MyBusinessViewModel by sharedViewModel()
    private lateinit var photoList: MutableList<Uri>
    private lateinit var photoAdapter: PhotoAdapter
    private val binding by viewBinding(FragmentEditBusPhotoBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setData() {
        photoList = ArrayList()
        photoList.add(Uri.EMPTY)
        photoAdapter = PhotoAdapter(this)
        binding.rvPhoto.layoutManager =
            GridLayoutManager(requireContext(), 4, GridLayoutManager.VERTICAL, false)
        binding.rvPhoto.adapter = photoAdapter
        photoAdapter.setData(photoList)
    }


    private fun setupScreen() {
        binding.btnApply.setOnClickListener(this)
        binding.btnCancel.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)
        setData()
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnApply -> {
                photoAdapter.getAdapterList().let { viewModel.setPhotoUriFromAdd(it) }
                popBackStackAndHideKeyboard()
            }
            R.id.imgBack, R.id.btnCancel -> {
                popBackStackAndHideKeyboard()
            }
            R.id.llDelImage -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    CommonUtils.genericCastOrNull<Uri>(v.getTag(R.id.model))?.let {
                        photoAdapter.removeItemFromList(position)
                    }
                }
            }
            R.id.rlAddImage -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { _ ->
                    CommonUtils.genericCastOrNull<Uri>(v.getTag(R.id.model))?.let {
                        TedPermission.create().setPermissionListener(object : PermissionListener {
                            override fun onPermissionGranted() {
                                TedImagePicker.with(requireContext()).showCameraTile(true)
                                    .startMultiImage { uris ->
                                        onImagesPicked(uris)
                                    }
                            }

                            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) { //
                            }
                        }).setPermissions(Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE).check()
                    }
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun onImagesPicked(uris: List<Uri>) {
        repeat(uris.size) { position ->
            if (uris[position].toString().isNotEmpty()) {
                photoAdapter.getAdapterList().add(0, (uris[position]))
            }
            photoAdapter.notifyDataSetChanged()
        }
    }

}