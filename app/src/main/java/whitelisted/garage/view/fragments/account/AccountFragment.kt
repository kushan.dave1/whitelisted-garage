package whitelisted.garage.view.fragments.account

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.data.SelectWorkshopDialogParams
import whitelisted.garage.api.response.GetGMDResponse
import whitelisted.garage.api.response.ScreensConfigResponse
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.api.response.login.GetProfileCompletion
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentAccountBinding
import whitelisted.garage.eventBus.*
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.AccountAdapter
import whitelisted.garage.viewmodels.AccountFragmentViewModel
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.WalkthroughSharedViewModel

@SuppressLint("SetTextI18n")
class AccountFragment : BaseFragment() {

    private val binding by viewBinding2(FragmentAccountBinding::inflate)

    private var isWorkShopAltered = false
    private var workShopId = ""
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private val walkThroughViewModel: WalkthroughSharedViewModel by sharedViewModel()
    private val viewModel: AccountFragmentViewModel by viewModel()
    private var isProgressSet = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
        handleBackPress()
    }

    fun apiCall() { //        if ((activity as BaseActivity<*>).isLoadAccountFirstTime) {
        setupScreen()
        showLoader()
        viewModel.getProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID, ""))
        setWorkshopNameAndCount() //        }
        checkDeeplink()
    }

    private fun setupScreen() {
        clickListeners()
        setObservers()
        observeGMB()
        setUpData(dashboardViewModel.screensConfigResponse.value)
    }

    private fun setUpData(value: ScreensConfigResponse?) = binding?.apply {
        rvAccountTiles.layoutManager = GridLayoutManager(requireContext(), 2)

        rvAccountTiles.adapter =
            value?.account?.let { AccountAdapter(it.toMutableList(), this@AccountFragment) }
        versionTV.text = AppENUM.V + BuildConfig.VERSION_NAME
        tvGoCoinBalance.text =
            viewModel.getStringSharedPreference(AppENUM.GO_COIN_BALANCE_AMOUNT, "")
        ImageLoader.loadImageWithCallback(imgCreateGooglePage,
            value?.screen_images?.accountProMembership,
            object : ImageLoader.ImageLoadCallback {
                override fun onSuccess() { //                    tvExtraText.makeVisible(true)
                }

                override fun onError(throwable: Throwable) { // do nothing
                }
            },
            placeholderImage = R.drawable.ic_placeholder_combo,
            defaultImage = R.drawable.ic_placeholder_combo)
        ImageLoader.loadImage(accounntReferEnd,
            value?.screen_images?.account_refer_earn,
            placeholderImage = R.drawable.ic_placeholder_refer,
            defaultImage = R.drawable.ic_placeholder_refer)
    }

    private fun checkDeeplink() {
        if (viewModel.getBooleanSharedPreference(AppENUM.DEEPLINK_INSTANT_REMINDER, false)) {
            viewModel.putBooleanSharedPreference(AppENUM.DEEPLINK_INSTANT_REMINDER, false)
            Handler(Looper.getMainLooper()).postDelayed({
                binding?.llInstantReminder?.callOnClick()
            }, 300)
        }
    }

    private fun observeGMB() {
        viewModel.provideGMD().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> { //                    gmbResultSuccess(it.body.data)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

        dashboardViewModel.provideOpenInstantReminder().observe(viewLifecycleOwner) {
            if (it == true) {
                setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_CUS_REMINDER,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                requireActivity().let {
                    FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.INSTANT_REMINDER,
                        null,
                        object : ActionListener {
                            @RequiresApi(Build.VERSION_CODES.P)
                            override fun onActionItem(extra: Any?, extra2: Any?) {
                                if (extra as? String == AppENUM.OPEN_DIALOG) {
                                    openSuccessDialog()
                                    CommonUtils.showReviewDialog(requireActivity())
                                } else if (extra == AppENUM.OPEN_FRAG) {
                                    openCustomerReminderList()
                                }
                            }
                        })?.let { baseFragment ->
                        baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
                    }
                }
            }
        }
    }

    private fun gmbResultSuccess(response: GetGMDResponse) {
        response.gocoin_unlocked_features?.let { list ->
            if (list.isNotEmpty() && list.contains(AppENUM.GMB)) {
                if (response.gmb_updated == null || response.gmb_updated == false) {
                    binding?.llRequestGMBParent.makeVisible(true)
                } else {
                    binding?.llRequestGMBParent.makeVisible(false)
                }
            } else {
                binding?.llRequestGMBParent.makeVisible(false)
            }
        }
            ?: binding?.llRequestGMBParent.makeVisible(false) //        if (viewModel.getIntSharedPreference(AppENUM.GO_COINS_VALUE) == 0) {
        binding?.tvExtraText?.text = "${getString(R.string.only)} ${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }${
            (dashboardViewModel.screensConfigResponse.value?.membership?.membershipPrice ?: "699")
        }" //        } else binding?.tvExtraText?.text = "${getString(R.string.only)} ${
        //            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
        //                getString(R.string.currency_symbol_default))
        //        }${
        //            (response.gmbPrice ?: "0").toInt()
        //                .div(viewModel.getIntSharedPreference(AppENUM.GO_COINS_VALUE))
        //        }"
    }

    private fun setObservers() {
        viewModel.provideWorkshopListResponseFromRoom().observe(viewLifecycleOwner) {
            hideLoader()
            checkWorkShopNameStatus(it)
        }

        viewModel.provideProfileResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    binding?.perProgress?.progress = 0
                    setUpDataProfileData(it.body.data)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

        observeWalkThroughEvent()
        observeMembershipPurchaseEvent()
    }

    private fun observeWalkThroughEvent() = binding?.apply {
        walkThroughViewModel.provideAccountWalkThroughEvent().observe(viewLifecycleOwner) {
            when (it) {
                R.id.llUploadBulkInventory -> {
                    lifecycleScope.launchWhenResumed {
                        if (isVisible && isAdded) {
                            startWalkthrough(llInstantReminder)
                        }
                    }
                }
                R.id.llInstantReminder -> {
                    val scrollTo = rvAccountTiles.bottom
                    nsvAccount.smoothScrollTo(0, scrollTo)
                    lifecycleScope.launch {
                        delay(500)
                        if (isAdded) {
                            rvAccountTiles.getChildAt(1)?.let { view ->
                                if (isAdded) startWalkthrough(view)
                            }
                        }
                    }
                }
                1 -> {
                    if (rvAccountTiles.childCount > 2) {
                        startWalkthrough(rvAccountTiles.getChildAt(2))
                    }
                }

                2 -> {
                    if (rvAccountTiles.childCount > 3) {
                        startWalkthrough(rvAccountTiles.getChildAt(3))
                    }
                }

                3 -> {
                    if (rvAccountTiles.childCount > 4) {
                        startWalkthrough(rvAccountTiles.getChildAt(4))
                    }
                }
                4 -> {
                    if (rvAccountTiles.childCount > 5) {
                        startWalkthrough(rvAccountTiles.getChildAt(5))
                    }
                }
                5 -> {
                    if (rvAccountTiles.childCount > 6) {
                        startWalkthrough(rvAccountTiles.getChildAt(6))
                    }
                }
                6 -> {
                    if (isInternationalUser) {
                        if (isRetailer) {
                            if (rvAccountTiles.childCount > 7) {
                                CommonUtils.addFragmentUtil(
                                    requireActivity(),
                                    FragmentFactory.fragment(FragmentFactory.Fragments.SETTINGS,
                                        null),
                                )
                                lifecycleScope.launch {
                                    delay(500)
                                    walkThroughViewModel.setSettingsWalkThrough(9)
                                }
                            }
                        } else {
                            if (rvAccountTiles.childCount > 7) {
                                startWalkthrough(rvAccountTiles.getChildAt(7))
                            }
                        }
                    } else {
                        if (rvAccountTiles.childCount > 7) {
                            startWalkthrough(rvAccountTiles.getChildAt(7))
                        }
                    }
                }
                7 -> {
                    if (isInternationalUser) {
                        if (rvAccountTiles.childCount > 8) {
                            nsvAccount.smoothScrollTo(0, 0)
                            CommonUtils.addFragmentUtil(
                                requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.SETTINGS, null),
                            )
                            lifecycleScope.launch {
                                delay(500)
                                walkThroughViewModel.setSettingsWalkThrough(9)
                            }
                        }
                    } else {
                        if (isRetailer) {
                            if (rvAccountTiles.childCount > 8) {
                                nsvAccount.smoothScrollTo(0, 0)
                                CommonUtils.addFragmentUtil(
                                    requireActivity(),
                                    FragmentFactory.fragment(FragmentFactory.Fragments.SETTINGS,
                                        null),
                                )
                                lifecycleScope.launch {
                                    delay(500)
                                    walkThroughViewModel.setSettingsWalkThrough(9)
                                }
                            }
                        } else {
                            if (rvAccountTiles.childCount > 8) {
                                startWalkthrough(rvAccountTiles.getChildAt(8))
                            }
                        }
                    }
                }
                8 -> {
                    if (rvAccountTiles.childCount > 9) {
                        nsvAccount.smoothScrollTo(0, 0)
                        CommonUtils.addFragmentUtil(
                            requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.SETTINGS, null),
                        )
                        lifecycleScope.launch {
                            delay(500)
                            walkThroughViewModel.setSettingsWalkThrough(9)
                        }
                    }
                }
                9 -> {
                    nsvAccount.smoothScrollTo(0, 0)
                }
            }
        }
    }

    private fun setUpDataProfileData(data: GetProfileCompletion) = binding?.apply {
        tvWorkShopName.text = data.userData?.workshopName
        tvName.text = data.userData?.name
        dashboardViewModel.putStringSharedPreference(AppENUM.USER_ROLE, data.userData?.role ?: "")
        if (isAdded) {
            loadCircularImage(data.userData?.image ?: "",
                imgWorkshop,
                defaultImage = R.drawable.ic_apartment_gray)
            if (!isProgressSet) {
                data.profileScore?.let {
                    when {
                        data.profileScore == 100 -> {
                            perProgress.progressTintList =
                                ColorStateList.valueOf(CommonUtils.getColor(requireContext(),
                                    R.color.status_complete))
                            tvStatus.text = getString(R.string.good)
                            tvPer.changeTextColor(requireContext(), R.color.status_complete)
                            tvStatus.changeTextColor(requireContext(), R.color.status_complete)
                            tvCompleteNow.text = getString(R.string.manage)
                        }
                        (data.profileScore ?: 0) <= 30 -> {
                            perProgress.progressTintList =
                                ColorStateList.valueOf(CommonUtils.getColor(requireContext(),
                                    R.color.status_low))
                            tvStatus.text = getString(R.string.weak)
                            tvPer.changeTextColor(requireContext(), R.color.status_low)
                            tvStatus.changeTextColor(requireContext(), R.color.status_low)
                            tvCompleteNow.text = getString(R.string.complete_now)
                        }
                        else -> {
                            perProgress.progressTintList =
                                ColorStateList.valueOf(CommonUtils.getColor(requireContext(),
                                    R.color.status_average))
                            tvStatus.text = getString(R.string.avg)
                            tvPer.changeTextColor(requireContext(), R.color.status_average)
                            tvStatus.changeTextColor(requireContext(), R.color.status_average)
                            tvCompleteNow.text = getString(R.string.complete_now)
                        }
                    }
                    perProgress.max = 100
                    startProgress(data)
                }
            }
        }

//        if (data.userData?.membershipStatus == true) {
//            viewModel.putBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS, true)
//        } else {
//            viewModel.putBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS, false)
//        }
        if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                false)) {
            binding?.clProActive.makeVisible(true)
        } else {
            binding?.clProActive.makeVisible(false)
        }
        binding?.tvExtraText?.text = "${getString(R.string.only)} ${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }${
            (dashboardViewModel.screensConfigResponse.value?.membership?.membershipPrice ?: "699")
        }"
    }

    private fun startProgress(data: GetProfileCompletion) {
        var progressStatus = 0
        viewModel.launch(Dispatchers.Default) {
            while (progressStatus < (data.profileScore ?: 0)) {
                progressStatus += 1
                delay(10)
                viewModel.launch(Dispatchers.Main) {
                    binding?.perProgress?.progress = progressStatus
                    binding?.tvPer?.text =
                        String.format(getString(R.string.text_percntage), progressStatus)
                }
            }
        }
//        isProgressSet = true
    }

    private fun checkWorkShopNameStatus(list: List<WorkshopListResponseModel>) {
        if (isWorkShopAltered) {
            isWorkShopAltered = false
            var isWorkShopPresent = true
            for (data in list) {
                if (data.workshopId.equals(workShopId)) {
                    isWorkShopPresent = true
                    break
                } else {
                    isWorkShopPresent = false
                }
            }
            if (!isWorkShopPresent) {
                viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME,
                    list[0].name.toString())
                viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                    list[0].workshopId.toString())
                binding?.tvWorkShopName?.text =
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME, "")
                when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                    AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
                    AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                        binding?.tvShopType?.text = getString(R.string.car_workshop)
                    }
                    AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                        binding?.tvShopType?.text = getString(R.string.accessories_shop)
                    }
                    AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                        binding?.tvShopType?.text = getString(R.string.spares_shop)
                    }
                    else -> {
                        binding?.tvShopType?.text = getString(R.string.car_workshop)
                    }
                }

                EventBus.getDefault().post(UpdateHomeScreenEvent())
            }
            workShopId = ""
        }
    }

    private fun clickListeners() = binding?.apply {
        imgMenu.setOnClickListener(this@AccountFragment)
        llWorkshops.setOnClickListener(this@AccountFragment)
        llViewGOCOIN.setOnClickListener(this@AccountFragment)
        tvWorkshopName.setOnClickListener(this@AccountFragment)
        tvShopType.setOnClickListener(this@AccountFragment)
        tvLogout.setOnClickListener(this@AccountFragment)
        ppTv.setOnClickListener(this@AccountFragment)
        imgReferAndEarn.setOnClickListener(this@AccountFragment)
        llProfileProgress.setOnClickListener(this@AccountFragment)
        imgCreateGooglePage.setOnClickListener(this@AccountFragment)
        imgWebsitePage.setOnClickListener(this@AccountFragment)
        llRequestGMBParent.setOnClickListener(this@AccountFragment)
        llInstantReminder.setOnClickListener(this@AccountFragment)

        srl.setOnRefreshListener {
            srl.isRefreshing = false
            showLoader()
            isProgressSet = false
            viewModel.getProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID, ""))
            viewModel.getWorkshopsListFromRoom()
        }
    }

    override fun onClick(v: View) {
        if (dashboardViewModel.getBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN, true)) {
            super.onClick(v)
            when (v.id) {
                R.id.llProfileProgress -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.PROFILE, null),
                    )
                }
                R.id.llRequestGMBParent -> {
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_TAP_G_COMBO_REMINDER,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.GMB_INFO, null),
                    )
                }
                R.id.ppTv -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.WEB_VIEW,
                            Bundle().apply {
                                putString(AppENUM.IntentKeysENUM.WEB_VIEW_URL, BuildConfig.PP_URL)
                            }),
                    )
                }
                R.id.imgReferAndEarn -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.REFER_AND_EARN, null),
                    )
                }
                R.id.llWorkshops -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_LIST, null),
                    )

                    Bundle().apply {
                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)

                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_WORKSHOP,
                            this)
                    }
                }
                R.id.llViewGOCOIN -> {
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_WALLET,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, null),
                    )
                }
                R.id.tvShopType, R.id.tvWorkshopName -> {
                    Bundle().apply {
                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_TAP_WORKSHOP,
                            this)
                    }
                    showSelectWorkshopDialog(SelectWorkshopDialogParams(isStart = false,
                        isHome = false))
                }
                R.id.tvLogout -> {
                    Bundle().apply {
                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_LOG_OUT,
                            this)
                    }

                    dashboardViewModel.logoutUser()
                    restartTheApp()
                }
                R.id.imgWebsitePage -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.WEBSITE_INFO, null),
                    )
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_WEBSITE,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }
                R.id.imgCreateGooglePage -> {
                    fireInitMembershipEvent()
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SUBSCRIPTION_INFO_FRAGMENT))
                }
                R.id.llInstantReminder -> {
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_CUS_REMINDER,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                    requireActivity().let {
                        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.INSTANT_REMINDER,
                            null,
                            object : ActionListener {
                                @RequiresApi(Build.VERSION_CODES.P)
                                override fun onActionItem(extra: Any?, extra2: Any?) {
                                    if (extra as? String == AppENUM.OPEN_DIALOG) {
                                        openSuccessDialog()
                                        CommonUtils.showReviewDialog(requireActivity())
                                    } else if (extra == AppENUM.OPEN_FRAG) {
                                        openCustomerReminderList()
                                    }
                                }
                            })?.let { baseFragment ->
                            baseFragment.show(it.supportFragmentManager,
                                baseFragment.javaClass.name)
                        }
                    }
                }
                R.id.viewClickIA -> {
                    CommonUtils.genericCastOrNull<String>(v.getTag(R.id.model))
                        ?.let { redirection ->
                            accountAdapterRedirect(redirection)
                        }
                }
            }
        }
    }

    private fun accountAdapterRedirect(redirection: String) {
        if (isAdded && isResumed) {
            when (redirection) {
                AppENUM.ConfigConstants.ACCOUNT_MANAGE_EMPLOYEES -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.MANAGE_EMPLOYEE, null),
                    )
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_MANAGE_EMP,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }
                AppENUM.ConfigConstants.ACCOUNT_MANAGE_WORKSHOP -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_LIST, null),
                    )

                    Bundle().apply {
                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)

                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_WORKSHOP,
                            this)
                    }
                }
                AppENUM.ConfigConstants.ACCOUNT_GMB -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.GMB_INFO, null),
                    )
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_G_COMBO,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }
                AppENUM.ConfigConstants.ACCOUNT_REMINDER -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.REMINDERS, null),
                    )
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_REMINDER,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }
                AppENUM.ConfigConstants.ACCOUNT_QR_CODE -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.QR_CODE, null),
                    )
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_QR,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }
                AppENUM.ConfigConstants.GMB_WEBSITE -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.WEBSITE_INFO, null),
                    )
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_WEBSITE,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }
                AppENUM.ConfigConstants.ACCOUNT_MY_CUSTOMERS -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.MY_CUSTOMER, null),
                    )
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_MY_CUSTOMER,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }
                AppENUM.ConfigConstants.ACCOUNT_MANAGE_PACKAGES -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.MANAGE_PACKAGES, null),
                    )
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_MANAGE_PACK,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }
                AppENUM.ConfigConstants.ACCOUNT_LEDGER -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.LEDGER, null),
                    )
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_LEDGER,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }
                AppENUM.ConfigConstants.ACCOUNT_ORDER_HISTORY -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_HISTORY, null),
                    )
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_ORDER_HISTORY,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }
                AppENUM.ConfigConstants.ACCOUNT_SETTINGS -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SETTINGS, null),
                    )
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_SETTING,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }
                AppENUM.ConfigConstants.ACCOUNT_FEEDBACK -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.FEEDBACK_FRAGMENT, null),
                    )
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_FEEDBACK,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }
                AppENUM.ConfigConstants.ACCOUNT_SHARE_APP -> {
                    val sendIntent = Intent()
                    sendIntent.action = Intent.ACTION_SEND
                    sendIntent.putExtra(Intent.EXTRA_TEXT,
                        getString(R.string.share_app_text) + AppENUM.APP_PLAY_STORE_URL)
                    sendIntent.type = "text/plain"
                    startActivity(sendIntent)
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_FEEDBACK,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
                }

                AppENUM.ConfigConstants.BUSINESS_CARD -> {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.FRAGMENT_BUSINESS_CARD))
                }

                AppENUM.ConfigConstants.BILLING -> {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.BILLING))
                }
            }
        }
    }

    private fun openCustomerReminderList() {
        CommonUtils.addFragmentUtil(activity,
            FragmentFactory.fragment(FragmentFactory.Fragments.CUSTOMER_REMINDER_FRAGMENT, null))
    }

    private fun openSuccessDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.REMINDER_SENT_SUCCESS,
            null,
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) { //
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun setWorkshopNameAndCount() = binding?.apply {
        (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME, "")).apply {
            tvWorkshopName.text = this
            tvWorkShopName.text = this
        }
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE, "")) {
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                tvShopType.text = getString(R.string.accessories_shop)
            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                tvShopType.text = getString(R.string.spares_shop)
            }
            else -> {
                tvShopType.text = getString(R.string.car_workshop)
            }
        }

        viewModel.getWorkshopsListFromRoom()
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is WorkshopUpdateEvent -> { //                (activity as BaseActivity<*>).loaderAgainFirstTime()
                setWorkshopNameAndCount()
            }
            is WorkshopAddedEvent -> { //                (activity as BaseActivity<*>).loaderAgainFirstTime()
                setWorkshopNameAndCount()
            }
            is WorkshopDeletedEvent -> {
                isWorkShopAltered = true
                workShopId = event.id
                setWorkshopNameAndCount()
            }
            is GoCoinBalanceReceivedEvent -> {
                binding?.tvGoCoinBalance?.text = (event.gocoinBalance).toString()
            }
            is ProfileUpdateEvent -> {
                showLoader()
                isProgressSet = false
                viewModel.getProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID,
                    ""))
            }
        }
    }

    fun updateScreen() {
        if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                false)) {
            binding?.clProActive.makeVisible(true)
        } else {
            binding?.clProActive.makeVisible(false)
        }
        binding?.tvExtraText?.text = "${getString(R.string.only)} ${
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }${
            (dashboardViewModel.screensConfigResponse.value?.membership?.membershipPrice ?: "499")
        }"
        setWorkshopNameAndCount()
        setUpData(dashboardViewModel.screensConfigResponse.value)
        showLoader()
        isProgressSet = false
        viewModel.getProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID, ""))
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                false)) {
            binding?.clProActive.makeVisible(true)
        } else {
            binding?.clProActive.makeVisible(false)
        }
    }

    private fun observeMembershipPurchaseEvent() {
        dashboardViewModel.provideMembershipPurchasedEvent().observe(viewLifecycleOwner) {
            if (it) {
                binding?.clProActive.makeVisible(true)
                dashboardViewModel.provideIsWorkshopRefresh().postValue(true)
            }
        }
    }


    private fun fireInitMembershipEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_MEMBERSHIP,
            bundle)
    }
}