package whitelisted.garage.view.fragments.retailNewOrderDetail

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.core.content.ContextCompat
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.data.SetOptionsImgStatusObject
import whitelisted.garage.api.request.MyCustomerModel
import whitelisted.garage.api.response.EmployeeListResponseModel
import whitelisted.garage.api.response.RetailOrderDetailResponse
import whitelisted.garage.api.response.SavedWorkshopResponseModel
import whitelisted.garage.api.response.SearchOrderResponseModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentRetailNewOrderDetailBinding
import whitelisted.garage.eventBus.SetOptionsImageStatusEvent
import whitelisted.garage.eventBus.UpdateHomeScreenEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.SavedWorkshopsAdapter
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment
import whitelisted.garage.viewmodels.RetailNewOrderDetailFragmentViewModel

class RetailNewOrderDetailFragment : BaseFragment(), TextWatcher {

    private val binding by viewBinding2(FragmentRetailNewOrderDetailBinding::inflate)
    private val viewModel: RetailNewOrderDetailFragmentViewModel by viewModel()
    private lateinit var savedWorkshopsList: MutableList<SavedWorkshopResponseModel>
    private var isNewOrder = false
    private lateinit var employeeListResponse: List<EmployeeListResponseModel>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        getDataFromArgs()
        setClickListeners()
        setTextWatchers()
        setFieldAdapters()
        setObservers()
        setSwitchListeners()
        showLoader()
        viewModel.getSavedWorkshopsAPI()
        viewModel.getEmployess()
    }

    private fun setSwitchListeners() {
        binding?.switchSelectMechanic?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding?.llSelectMech?.makeVisible(true)
            } else {
                binding?.llSelectMech?.makeVisible(false)
            }
        }
    }

    private fun getDataFromArgs() {
        arguments?.let {
            isNewOrder = it.getBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, false)
            if (it.getBoolean(AppENUM.IntentKeysENUM.IS_UNDER_TAB, false)) {
                isNewOrder = false
                binding?.llHeader?.makeVisible(false)
                binding?.lblOrderDetail?.makeVisible(false)
                binding?.btnCancel?.makeVisible(false)
                binding?.rlSaveForFuture?.makeVisible(false)
                viewModel.getOrderDetail()
            }
        }
    }

    private fun setOrderDetails(model: SearchOrderResponseModel) = binding?.apply {
        tilBillTo.editText?.setText(model.customerDetails?.name ?: "")
        etWorkshopName.setText(model.customerDetails?.name ?: "")
        etBillingName.setText(model.bill_name ?: "")
        etPhoneNumber.setText(model.customerDetails?.mobile ?: "")
        etAddress.setText(model.customerDetails?.address ?: "") //        etGstin.setText(model.gst)
        //        tilBillType.editText?.setText(model.billType ?: "")
        //        if (data.billType == getString(R.string.non_tax_invoice)) llTaxType.makeVisible(false)
        //        tilTaxType.editText?.setText(data.taxType ?: "")

        if (!model.mechanicName.isNullOrEmpty()) {
            (employeeListResponse.find { it.name == model.mechanicName })?.let {
                switchSelectMechanic.isChecked = true
                tilMechanic.editText?.setText(model.mechanicName)
            }
        }
    }

    private fun setOrderDetails(model: MyCustomerModel) = binding?.apply {
        tilBillTo.editText?.setText(model.name ?: "")
        etWorkshopName.setText(model.name ?: "")
        etBillingName.setText(model.bill_name ?: "")
        etPhoneNumber.setText(model.mobile ?: "")
        etAddress.setText(model.address ?: "") //        etGstin.setText(model.gst)
        //        tilBillType.editText?.setText(model.billType ?: "")
        //        if (data.billType == getString(R.string.non_tax_invoice)) llTaxType.makeVisible(false)
        //        tilTaxType.editText?.setText(data.taxType ?: "")

        if (!model.mechanicName.isNullOrEmpty()) {
            (employeeListResponse.find { it.name == model.mechanicName })?.let {
                switchSelectMechanic.isChecked = true
                tilMechanic.editText?.setText(model.mechanicName)
            }
        }
    }

    private fun setTextWatchers() {
        binding?.etWorkshopName?.addTextChangedListener(this)
    }

    private fun setObservers() {
        observePlaceOrderResponse()
        observeSaveWsListResponse()
        observeOrderDetailsResponse()
        observeUpdateOrderResponse()
        observeEmployeeListResponse()
    }

    private fun observeEmployeeListResponse() {
        viewModel.provideManageEmployeeResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isEmpty()) {
                        binding?.llSelectMechanic?.makeVisible(false)
                    } else {
                        employeeListResponse = it.body.data
                        setEmployeeListAdapter()
                    }
                }
                is Result.Failure -> { //do nothing
                }
            }
        }
    }

    private fun setEmployeeListAdapter() {
        val mechanicList = java.util.ArrayList<String>()
        employeeListResponse.forEach {
            if (!it.name.isNullOrEmpty()) mechanicList.add(it.name)
        }

        if (mechanicList.isNotEmpty()) {
            val adapter = ArrayAdapter(requireContext(),
                R.layout.support_simple_spinner_dropdown_item,
                mechanicList)
            (binding?.tilMechanic?.editText as? AutoCompleteTextView)?.setAdapter(adapter)
        } else binding?.llSelectMechanic?.makeVisible(false)

        if (isNewOrder) {
            checkForMyCustomerRedirection()
        }
    }

    private fun checkForMyCustomerRedirection() {
        (arguments?.getParcelable<MyCustomerModel>(AppENUM.MY_CUSOMTER_MODEL))?.let { model ->
            setOrderDetails(model)
        }

        (arguments?.getParcelable<SearchOrderResponseModel>(AppENUM.SEARCH_ORDER_MODEL))?.let { model ->
            setOrderDetails(model)
        }
    }

    private fun observeUpdateOrderResponse() {
        viewModel.provideUpdateOrderResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    showLoader()
                    viewModel.getOrderDetail()
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun observeOrderDetailsResponse() {
        viewModel.provideOrderDetailResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setData(it.body.data)
                    OrderDetailWrapperFragment.retailWorkshopName =
                        it.body.data.customerDetails?.name.toString()
                    if (!isNewOrder && isResumed) {
                        EventBus.getDefault().post(SetOptionsImageStatusEvent("1",
                            SetOptionsImgStatusObject(tabPosition = 0)))
                    }
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun observeSaveWsListResponse() {
        viewModel.provideSavedWorkshopListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        savedWorkshopsList = mutableListOf()
                        savedWorkshopsList.add(SavedWorkshopResponseModel().apply {
                            name = getString(R.string.create_new)
                        })
                        savedWorkshopsList.addAll(it.body.data)
                        setSavedWorkshopsAdapter()
                    } else {
                        binding?.tilBillTo?.editText?.hint = getString(R.string.enter_workshop_name)
                    }
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun observePlaceOrderResponse() {
        viewModel.providePlaceOrderResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {

                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    OrderDetailWrapperFragment.retailWorkshopName = binding?.etWorkshopName?.text.toString()
                    popBackStackAndHideKeyboard()
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                            Bundle().apply {
                                putString(AppENUM.IntentKeysENUM.ORDER_ID, it.body.data.orderId)
                                putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                            }),
                        target = R.id.fragment_container_2)
                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun setData(data: RetailOrderDetailResponse) = binding?.apply {
        tilBillTo.editText?.setText(data.customerDetails?.name ?: "")
        etWorkshopName.setText(data.customerDetails?.name ?: "")
        etBillingName.setText(data.billName ?: "")
        etPhoneNumber.setText(data.customerDetails?.mobile ?: "")
        etAddress.setText(data.customerDetails?.address ?: "")
        etGstin.setText(data.gstin ?: "")
        tilBillType.editText?.setText(data.billType ?: "")
        if (data.billType == getString(R.string.non_tax_invoice)) llTaxType.makeVisible(false)
        tilTaxType.editText?.setText(data.taxType ?: "")

        if (!data.mechanicName.isNullOrEmpty()) {
            switchSelectMechanic.isChecked = true
            tilMechanic.editText?.setText(data.mechanicName)
        }

        switchSaveForFuture.isEnabled = data.customerDetails?.save ?: false
    }

    private fun setSavedWorkshopsAdapter() {
        val workshopsList = ArrayList<String>()
        savedWorkshopsList.forEach {
            workshopsList.add(it.name.toString())
        }
        SavedWorkshopsAdapter(requireContext(), workshopsList).apply {
            (binding?.tilBillTo?.editText as? AutoCompleteTextView)?.setAdapter(this)
        }
        (binding?.tilBillTo?.editText as? AutoCompleteTextView)?.setOnItemClickListener { _, _, _, _ ->
            CommonUtils.hideKeyboard(requireActivity())
            if (binding?.tilBillTo?.editText?.text.toString() == getString(R.string.create_new)) {
                setEmptyAndEnableFields()
            } else {
                savedWorkshopsList.forEach {
                    if (it.name == binding?.tilBillTo?.editText?.text.toString()) {
                        setAndLockFields(it)
                        return@forEach
                    }
                }
            }
        }
    }

    private fun setEmptyAndEnableFields() = binding?.apply {
        etWorkshopName.setText("")
        tilBillTo.editText?.setText(getString(R.string.create_new))
        etBillingName.setText("")
        etPhoneNumber.setText("")
        etAddress.setText("")
        switchSaveForFuture.isChecked = false
        etWorkshopName.isEnabled = true
        etBillingName.isEnabled = true
        etPhoneNumber.isEnabled = true
        etAddress.isEnabled = true
        etGstin.isEnabled = true
        tilTaxType.isEnabled = true
        rlSaveForFuture.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.bg_border_e3e3e3_4)
        switchSaveForFuture.isEnabled = true
        etWorkshopName.requestFocus()
    }

    private fun setAndLockFields(savedWorkshopResponseModel: SavedWorkshopResponseModel) = binding?.apply {
        etWorkshopName.setText(savedWorkshopResponseModel.name)
        etBillingName.setText(savedWorkshopResponseModel.name)
        etPhoneNumber.setText(savedWorkshopResponseModel.mobile)
        etAddress.setText(savedWorkshopResponseModel.address)
        switchSaveForFuture.isChecked = savedWorkshopResponseModel.save ?: false
        etWorkshopName.isEnabled = false
        etBillingName.isEnabled = false
        etPhoneNumber.isEnabled = false
        etAddress.isEnabled = false
        rlSaveForFuture.background =
            ContextCompat.getDrawable(requireContext(), R.drawable.bg_lock_field)
        switchSaveForFuture.isEnabled = false
        etGstin.requestFocus()
    }

    private fun setClickListeners() = binding?.apply {
        imgBack.setOnClickListener(this@RetailNewOrderDetailFragment)
        btnCancel.setOnClickListener(this@RetailNewOrderDetailFragment)
        btnSave.setOnClickListener(this@RetailNewOrderDetailFragment)
        tvClear.setOnClickListener(this@RetailNewOrderDetailFragment)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.btnCancel -> popBackStackAndHideKeyboard()
            R.id.btnSave -> {
                if (formCheck()) {
                    makeOrderRequest()
                    showLoader()
                    if (isNewOrder) viewModel.createNewOrder()
                    else viewModel.updateOrder()
                }
            }

            R.id.tvClear -> {
                setEmptyAndEnableFields()
                binding?.tilBillTo?.editText?.setText("")
            }
        }
    }

    private fun makeOrderRequest() = binding?.apply {
        viewModel.provideCreateOrderRequest().name = etWorkshopName.text.toString()
        viewModel.provideCreateOrderRequest().billName = etBillingName.text.toString()
        viewModel.provideCreateOrderRequest().mobile = etPhoneNumber.text.toString()
        viewModel.provideCreateOrderRequest().address = etAddress.text.toString()
        viewModel.provideCreateOrderRequest().save = switchSaveForFuture.isChecked
        if (switchSelectMechanic.isChecked) {
            if (::employeeListResponse.isInitialized) employeeListResponse.forEach {
                if (it.name == tilMechanic.editText?.text.toString()) {
                    viewModel.provideCreateOrderRequest().mechanicId = it.userId.toString()
                    viewModel.provideCreateOrderRequest().mechanicName = it.name.toString()
                    return@forEach
                }
            }
        }

        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_NEW_ORDER_DETAILS_SPARE)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_NAME,
                etWorkshopName.text.toString())
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_PHONE,
                etPhoneNumber.text.toString())
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_ADDRESS,
                etAddress.text.toString())
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_SAVE_ORDER,
                this)
        }
    }

    private fun formCheck(): Boolean {

        if (binding?.etWorkshopName?.text.toString().trim().isEmpty() || binding?.etPhoneNumber?.text.toString().trim()
                .isEmpty() || binding?.etBillingName?.text.toString().trim().isEmpty()) {
            CommonUtils.showToast(requireContext(), getString(R.string.maindatory_not_blanck), true)
            when {
                binding?.etWorkshopName?.text.toString().trim().isEmpty() -> binding?.etWorkshopName?.requestFocus()
                binding?.etPhoneNumber?.text.toString().trim().isEmpty() -> binding?.etPhoneNumber?.requestFocus()
                binding?.etBillingName?.text.toString().trim().isEmpty() -> binding?.etBillingName?.requestFocus()
            }
            return false
        } else if (binding?.etPhoneNumber?.text.toString().trim().length < 10) {
            return if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                    AppENUM.RefactoredStrings.defaultCurrency) == AppENUM.RefactoredStrings.defaultCurrency) {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.alert_invalid_mobile),
                    true)
                binding?.etPhoneNumber?.requestFocus()
                binding?.etPhoneNumber?.setSelection(binding?.etPhoneNumber?.text.toString().length)
                false
            } else true
        }
        return true
    }

    private fun setFieldAdapters()  = binding?.apply {
        val billTypeList = ArrayList<String>()
        billTypeList.add(getString(R.string.tax_invoice))
        billTypeList.add(getString(R.string.non_tax_invoice))
        ArrayAdapter(requireContext(),
            R.layout.support_simple_spinner_dropdown_item,
            billTypeList).apply {
            (tilBillType.editText as? AutoCompleteTextView)?.setAdapter(this)
        }
        (tilBillType.editText as? AutoCompleteTextView)?.setOnItemClickListener { _, _, position, _ ->
            if (position == 1) {
                llTaxType.makeVisible(false)
            } else {
                llTaxType.makeVisible(true)
            }
        }
        val taxTypeList = ArrayList<String>()
        taxTypeList.add(getString(R.string.gst))
        taxTypeList.add(getString(R.string.igst))
        val adapter2 = ArrayAdapter(requireContext(),
            R.layout.support_simple_spinner_dropdown_item,
            taxTypeList)
        (tilTaxType.editText as? AutoCompleteTextView)?.setAdapter(adapter2)
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { // nothing
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        binding?.etBillingName?.setText(binding?.etWorkshopName?.text.toString())
        binding?.tilBillTo?.editText?.setText(binding?.etWorkshopName?.text.toString())
    }

    override fun afterTextChanged(p0: Editable?) { // nothing
    }

    fun updateScreen() {
        if (!isNewOrder) {
            EventBus.getDefault()
                .post(SetOptionsImageStatusEvent("1", SetOptionsImgStatusObject(tabPosition = 0)))
        }
    }
}