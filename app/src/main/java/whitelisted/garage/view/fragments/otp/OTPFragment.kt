package whitelisted.garage.view.fragments.otp

import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.moengage.core.analytics.MoEAnalyticsHelper
import com.moengage.core.model.AppStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.App
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentOtpBinding
import whitelisted.garage.eventBus.OTPDetectedEvent
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.AppSignatureHelper
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.viewmodels.LoginViewModel

class OTPFragment : BaseFragment(), TextWatcher, View.OnKeyListener {

    private val binding by viewBinding(FragmentOtpBinding::inflate)
    private val viewModel: LoginViewModel by sharedViewModel()

    private lateinit var countDownTimerOTP: CountDownTimer
    private var secondsLeft: Int = 30

    private var isForgot = false
    private var isLogin = false
    private lateinit var mobile: String
    private lateinit var countryCode: String
    private var flag = false
    private var isSubmitted = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() = binding.apply {
        checkForForgotOrLogin()
        when {
            isForgot -> {
                tvMobileNumber.text = mobile
                btnRegister.text = getString(R.string.confirm)
                tvHeader.text = getString(R.string.forgot_password)
            }
            isLogin -> {
                tvMobileNumber.text = mobile
                btnRegister.text = getString(R.string.confirm)
                tvHeader.text = getString(R.string.login)
            }
            else -> tvMobileNumber.text = viewModel.provideSignUpRequest().value?.mobile
        }

        addTextWatchers()
        setClickListeners()
        startOtpTimer()
        setObservers()
        etOtp1.requestFocus()
        CommonUtils.showKeyboard(requireActivity(), etOtp1)
    }

    private fun addTextWatchers() {
        binding.etOtp1.addTextChangedListener(this)
        binding.etOtp2.addTextChangedListener(this)
        binding.etOtp3.addTextChangedListener(this)
        binding.etOtp4.addTextChangedListener(this)
        binding.etOtp1.setOnKeyListener(this)
        binding.etOtp2.setOnKeyListener(this)
        binding.etOtp3.setOnKeyListener(this)
        binding.etOtp4.setOnKeyListener(this)
    }

    private fun checkForForgotOrLogin() {
        if (arguments?.getBoolean(AppENUM.IntentKeysENUM.IS_FORGOT) == true) {
            isForgot = true
        } else if (arguments?.getBoolean(AppENUM.IntentKeysENUM.IS_LOGIN) == true) isLogin = true
        mobile = arguments?.getString(AppENUM.IntentKeysENUM.MOBILE_NUMBER).toString()
        countryCode = arguments?.getString(AppENUM.IntentKeysENUM.COUNTRY_CODE,
            getString(R.string.ind_country_code)) ?: getString(R.string.ind_country_code)
    }

    private fun setObservers() {
        getOtpResponseObserver()
        signUpResponseObserver()
        verifyOtpObserver()
        getOtpForLoginObserver()
    }

    private fun getOtpForLoginObserver() {
        viewModel.provideOTPForLoginResponse().observe(viewLifecycleOwner) {
            if (flag) {
                hideLoader()
                when (it) {
                    is Result.Success<*> -> {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.otp_resent),
                            true)
                    }
                    is Result.Failure -> {
                        CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    }
                }
            }
            flag = true
        }
    }

    private fun verifyOtpObserver() {
        viewModel.provideVerifyOtpResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    when {
                        isForgot -> {
                            Bundle().apply {
                                putString(AppENUM.IntentKeysENUM.MOBILE_NUMBER, mobile)
                                putBoolean(AppENUM.IntentKeysENUM.IS_FORGOT, true)
                                CommonUtils.addFragmentUtil(activity,
                                    FragmentFactory.fragment(FragmentFactory.Fragments.RESET_PASSWORD,
                                        this))
                            }
                        }
                        isLogin -> {
                            (requireActivity().application as App).reStartModule()
                            lifecycleScope.launch {
                                delay(200)
                                CommonUtils.replaceFragmentUtil(requireActivity(),
                                    FragmentFactory.fragment(FragmentFactory.Fragments.BOTTOM_NAV,
                                        null))
                            }

                            // For Existing user who has updated the app
                            MoEAnalyticsHelper.setAppStatus(requireContext(), AppStatus.UPDATE)
                        }
                        else -> {
                            (requireActivity().application as App).reStartModule()
                            CommonUtils.replaceFragmentUtil(requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.BOTTOM_NAV,
                                    null))

                            // For Existing user who has updated the app
                            MoEAnalyticsHelper.setAppStatus(requireContext(), AppStatus.UPDATE)
                        }
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage, true)
                }
            }
        }
    }


    private fun setClickListeners() {
        binding.imgBack.setOnClickListener(this)
        binding.btnRegister.setOnClickListener(this)
        binding.tvSendAgain.setOnClickListener(this)
    }

    private fun signUpResponseObserver() {
        viewModel.provideSignUpResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    (requireActivity().application as App).reStartModule()
                    CommonUtils.replaceFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.BOTTOM_NAV, null)
                    )

                    //For Fresh Install of App
                    MoEAnalyticsHelper.setAppStatus(requireContext(), AppStatus.INSTALL)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage, true)
                }
            }
        }
    }

    private fun getOtpResponseObserver() {
        viewModel.provideGetOtpResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success<*> -> {
                    it.body.let { data ->
                        if (BuildConfig.DEBUG) {
                            val result =
                                CommonUtils.genericCastOrNull<ServerResponse<String>>(data)
                            viewModel.provideSignUpRequest().value?.otp = result?.data
                            if (isAdded) {
                                startSMSListener()
                            }
                        }
                    }
                }
                is Result.Failure<*> -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
    }

    private fun startOtpTimer() {
        countDownTimerOTP = object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                secondsLeft = (millisUntilFinished / 1000).toInt()
                binding.tvExpectOtp.text = getString(R.string.expect_otp_in,
                    secondsLeft.toString() + " " + getString(R.string.seconds))
            }

            override fun onFinish() {
                binding.tvExpectOtp.visibility = View.GONE
                binding.clgSendAgain.visibility = View.VISIBLE
            }

        }

        binding.tvExpectOtp.text = getString(R.string.expect_otp_in, "30 " + getString(R.string.seconds))
        countDownTimerOTP.start()
    }


    override fun onDestroyView() {
        countDownTimerOTP.cancel()
        super.onDestroyView()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.btnRegister -> with(binding) {
                CommonUtils.hideKeyboard(activity) //                secondsLeft = 30
                //                countDownTimerOTP.cancel()
                //                tvExpectOtp.visibility = View.GONE
                //                clgSendAgain.visibility = View.VISIBLE
                showLoader()
                if (checkForm()) {
                    if (!isSubmitted) {
                        isSubmitted = true
                        when {
                            isForgot -> viewModel.callVerifyOtpAPI(mobile,
                                "verify",
                                etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString())
                            isLogin -> viewModel.callVerifyOtpAPIForLogin(countryCode,
                                mobile,
                                "login",
                                etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString())
                            else -> viewModel.callSignUpAPI(countryCode,
                                etOtp1.text.toString() + etOtp2.text.toString() + etOtp3.text.toString() + etOtp4.text.toString())
                        }
                        viewModel.launch(Dispatchers.Main) {
                            delay(2000)
                            isSubmitted = false
                        }
                    }
                }
            }
            R.id.tvSendAgain -> {
                CommonUtils.hideKeyboard(activity)
                showLoader()
                val signHelper = AppSignatureHelper(requireContext())
                signHelper.getAppSignatures()
                when {
                    isForgot -> viewModel.callGetOTPAPI(signHelper.getAppSignatures()[0],
                        countryCode,
                        mobile)
                    isLogin -> viewModel.loginUserAPI(signHelper.getAppSignatures()[0],
                        countryCode,
                        mobile)
                    else -> viewModel.callGetOTPAPI(signHelper.getAppSignatures()[0],
                        countryCode,
                        viewModel.provideSignUpRequest().value?.mobile ?: "")
                }
            }
        }
    }

    private fun checkForm(): Boolean = with(binding) {
        if (etOtp1.text.toString().isEmpty() || etOtp2.text.toString()
                .isEmpty() || etOtp3.text.toString().isEmpty() || etOtp4.text.toString()
                .isEmpty()) {
            CommonUtils.showToast(activity, getString(R.string.alert_invalid_otp), true)
            when {
                etOtp1.text.toString().isEmpty() -> etOtp1.requestFocus()
                etOtp2.text.toString().isEmpty() -> etOtp2.requestFocus()
                etOtp3.text.toString().isEmpty() -> etOtp3.requestFocus()
                etOtp4.text.toString().isEmpty() -> etOtp4.requestFocus()
            }
            hideLoader()
            return false
        }
        return true
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun afterTextChanged(p0: Editable?) {
        when {
            binding.etOtp1.text.toString().isEmpty() -> binding.etOtp1.requestFocus()
            binding.etOtp2.text.toString().isEmpty() -> binding.etOtp2.requestFocus()
            binding.etOtp3.text.toString().isEmpty() -> binding.etOtp3.requestFocus()
            binding.etOtp4.text.toString().isEmpty() -> binding.etOtp4.requestFocus()
            binding.etOtp4.text?.isNotEmpty() == true -> {
                CommonUtils.hideKeyboard(requireActivity())
                binding.btnRegister.callOnClick()
            }
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        when (event) {
            is OTPDetectedEvent -> {
                if (event.otpString != null) {
                    binding.etOtp1.setText(event.otpString[0].toString())
                    binding.etOtp2.setText(event.otpString[1].toString())
                    binding.etOtp3.setText(event.otpString[2].toString())
                    binding.etOtp4.setText(event.otpString[3].toString()) //                    if (event.otpString.isNotEmpty()) {
                    //                        btnRegister.callOnClick()
                    //                    }
                }
            }
        }
    }

    override fun onKey(p0: View?, keyCode: Int, p2: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_DEL) { //this is for backspace
            if (binding.etOtp4.text.toString().isEmpty()) binding.etOtp3.requestFocus()
            if (binding.etOtp3.text.toString().isEmpty()) binding.etOtp2.requestFocus()
            if (binding.etOtp2.text.toString().isEmpty()) binding.etOtp1.requestFocus()

        }
        return false
    }

    override fun onDestroy() {
        viewModel.provideSignUpResponse().value = null
        viewModel.provideVerifyOtpResponse().value = null
        super.onDestroy()
    }

}