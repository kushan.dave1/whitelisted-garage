package whitelisted.garage.view.fragments.inventory

import android.os.Bundle
import android.view.View
import whitelisted.garage.R
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentInventoryImagePreviewBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils.makeInVisible
import whitelisted.garage.utils.ImageLoader

class InventoryImagePreviewFragment : BaseFragment() {
    
    private val binding by viewBinding(FragmentInventoryImagePreviewBinding::inflate)
    private var list: ArrayList<String>? = null
    private var count = 0
    private var sizeOfList = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getData()
        setupScreen()
    }

    private fun setupScreen() {
        if (!list.isNullOrEmpty() && list?.size == 1) {
            binding.imageLeftArrow.makeInVisible(true)
            binding.imageRightArrow.makeInVisible(true)
        }
        if (!list.isNullOrEmpty() && (list?.size ?: 0) >= 1) {
            setDataToView(list?.get(count))
            count = 0
        }
    }

    private fun getData() {
        arguments?.let { bundle ->
            list = bundle.getStringArrayList(AppENUM.IMAGE_URL)
            sizeOfList = list?.size ?: 0
        }
        clickListener()
    }

    private fun clickListener() {
        binding.imageLeftArrow.setOnClickListener(this)
        binding.imageRightArrow.setOnClickListener(this)
        binding.lblPhotoContainer.setOnClickListener(this)
        binding.clBase.setOnClickListener(this)
    }

    private fun setDataToView(url: String?) {
        ImageLoader.loadImage(binding.imageView, url)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imageLeftArrow -> {
                if (count > 0) {
                    count--
                    setDataToView(list?.get(count) ?: "")
                }
            }
            R.id.imageRightArrow -> {
                if (count < (sizeOfList - 1)) {
                    count++
                    setDataToView(list?.get(count) ?: "")
                }
            }
            R.id.clBase -> {
                popBackStackAndHideKeyboard()
            }
        }
    }
}