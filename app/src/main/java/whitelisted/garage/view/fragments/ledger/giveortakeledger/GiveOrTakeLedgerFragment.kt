package whitelisted.garage.view.fragments.ledger.giveortakeledger

import DateUtil
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.AddToLedgerRequest
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentGiveTakeLedgerBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.GiveOrTakeFragmentViewModel
import java.util.*

class GiveOrTakeLedgerFragment : BaseFragment() {

    private val binding by viewBinding(FragmentGiveTakeLedgerBinding::inflate)
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private val viewModel: GiveOrTakeFragmentViewModel by viewModel()
    private lateinit var heading: String
    val today: Calendar = Calendar.getInstance()

    private var startDate = "${today.get(Calendar.DAY_OF_MONTH)}-${today.get(Calendar.MONTH) + 1}-${
        today.get(Calendar.YEAR)
    }"

    private lateinit var type: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        heading = arguments?.getString(AppENUM.HEADING).toString()

        type = if (heading == AppENUM.GIVE) {
            AppENUM.DEBIT
        } else {
            AppENUM.CREDIT
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
        observeData()

        binding.etAmount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().trim().isNotEmpty() && p0.toString()
                        .contains(".") && CommonUtils.countOccurrences(p0.toString(), '.') == 2) {
                    binding.etAmount.setText(p0.toString()
                        .subSequence(0, binding.etAmount.text.toString().length - 1))
                    binding.etAmount.setSelection(binding.etAmount.text.toString().length)
                }
            }
        })
    }

    private fun observeData() {
        viewModel.provideAddLedgerResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    popBackStackAndHideKeyboard()
                    dashboardViewModel.isNewLedgerAdded.value = true
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun setupScreen() {
        binding.etDate.setText(startDate)
        binding.tvHeaderTv.text = heading
        binding.imgBack.setOnClickListener(this)
        binding.btnSave.setOnClickListener(this)
        binding.etDate.setOnClickListener(this)

        binding.etAmount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().trim().isNotEmpty() && p0.toString()
                        .contains(".") && CommonUtils.countOccurrences(p0.toString(), '.') == 2) {
                    binding.etAmount.setText(p0.toString()
                        .subSequence(0, binding.etAmount.text.toString().length - 1))
                    binding.etAmount.setSelection(binding.etAmount.text.toString().length)
                }
            }
        })
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.etDate -> showSelectDateDialog()
            R.id.btnSave -> {
                if (!isLoading()) {
                    validateMandatoryFields()
                }
            }
        }
    }

    private fun validateMandatoryFields() {
        if (binding.etAmount.text.toString().isNotEmpty() && binding.etRemark.text.toString()
                .isNotEmpty() && binding.etName.text.toString()
                .isNotEmpty() && binding.etDate.text.toString().isNotEmpty()) {
            try {
                val request = AddToLedgerRequest(binding.etAmount.text.toString().toDouble(),
                    type,
                    binding.etRemark.text.toString(),
                    binding.etName.text.toString(),
                    DateUtil.getFormattedDateUtcToGmt(AppENUM.DDMMYYYY,
                        AppENUM.DateUtilKey.TIME_FORMAT,
                        startDate))

                // event
                Bundle().apply {
                    val eventName = if (heading == AppENUM.GIVE) {
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_LEDGER_GIVE
                    } else {
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_LEDGER_TAKE
                    }
                    putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_LEDGER_GVE_OR_TAKE)
                    putDouble(FirebaseAnalyticsLog.FirebaseEventNameENUM.PAYMENT_AMOUNT,
                        binding.etAmount.text.toString().toDouble())
                    FirebaseAnalyticsLog.trackFireBaseEventLog(eventName, this)
                }

                showLoader()
                viewModel.callToAddLedgerApi(request)
            } catch (e: NumberFormatException) { //
            }
        } else {
            CommonUtils.showToast(requireContext(), getString(R.string.not_empty_fileds), true)
        }
    }

    private fun showSelectDateDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_DATE, Bundle().apply {
            putBoolean(AppENUM.IS_START, true)
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                (extra as? String)?.let {
                    startDate = it
                }
                binding.etDate.setText(startDate)
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

}