package whitelisted.garage.view.fragments.signup

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.request.SignUpRequest
import whitelisted.garage.api.response.CountryCodeResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentSignupBinding
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.changeViewCompoundDrawablesWithInterinsicBounds
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.fragments.login.LoginFragment
import whitelisted.garage.viewmodels.AddressViewModel
import whitelisted.garage.viewmodels.LoginViewModel

class SignUpFragment : BaseFragment(), CompoundButton.OnCheckedChangeListener {

    private val binding by viewBinding(FragmentSignupBinding::inflate)
    private val viewModel: LoginViewModel by sharedViewModel()
    private val addressViewModel: AddressViewModel by sharedViewModel()
    private lateinit var fontMedium: Typeface
    private lateinit var fontSemiBold: Typeface
    private var selectedShopsList = mutableListOf(AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
    private var isDistributor = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    override fun onStart() {
        super.onStart()
        firebaseInItEvent()
    }

    private fun firebaseInItEvent() {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_REGISTER)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_REGISTER,
                this)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_SHOP_CREATION,
                this)
        }
    }

    private fun setupScreen() {
        getData()
        setObservers()
        setClickListeners()
    }

    private fun getData() = binding.apply {
        ImageLoader.loadImage(imgCountryFlag, AppENUM.RefactoredStrings.defaultFlag)
        fontMedium = Typeface.createFromAsset(requireActivity().assets, "gilroy_medium.ttf")
        fontSemiBold = Typeface.createFromAsset(requireActivity().assets, "gilroy_semibold.otf")
        arguments?.let {
            it.getString(AppENUM.IntentKeysENUM.MOBILE_NUMBER, "")?.let { number ->
                etPhoneNumber.setText(number)
            }
            it.getString(AppENUM.IntentKeysENUM.COUNTRY_CODE,
                AppENUM.RefactoredStrings.defaultCountryCode)?.let { code ->
                tvCountryCode.text = code
                if (code != AppENUM.RefactoredStrings.defaultCountryCode) {
                    clGst.makeVisible(false)
                }
            }
            it.getString(AppENUM.IntentKeysENUM.COUNTRY_FLAG)?.let { flag ->
                if (isAdded) {
                    ImageLoader.loadImage(imgCountryFlag, flag)
                }
            }
        }
    }

    private fun setObservers() {
        getOtpResponseObserver()
        getAddressDataObserver()
    }

    private fun setClickListeners() {
        binding.imgBack.setOnClickListener(this)
        binding.btnRegister.setOnClickListener(this)
        binding.btnLogin.setOnClickListener(this)
        binding.etGarageAddress.setOnClickListener(this)
        binding.copyLinkView.setOnClickListener(this)
        binding.viewWorkshop.setOnClickListener(this)
        binding.viewAccessories.setOnClickListener(this)
        binding.viewRetailer.setOnClickListener(this)
        binding.llCountryCode.setOnClickListener(this)
        binding.clDistributorCheck.setOnClickListener(this)
        binding.switchGST.setOnCheckedChangeListener(this)
    }

    private fun getAddressDataObserver() = binding.apply {
        addressViewModel.provideAddressDataResponse().observe(viewLifecycleOwner) {
            etGarageAddress.setText(it.fullAddress)
            ("${AppENUM.MAP_URL}${it.latitude},${it.longitude}").apply {
                etGarageMapLink.setText(this)
            }
        }
    }

    private fun getOtpResponseObserver() {
        viewModel.provideGetOtpResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success<*> -> {
                    it.body.let { result ->
                        if (BuildConfig.DEBUG) {
                            CommonUtils.genericCastOrNull<ServerResponse<String>>(result)
                                ?.let { data ->
                                    CommonUtils.showToast(activity, data.data)
                                }
                        }
                        makeSignUpRequest()
                        opeOtpScreen()
                    }
                }
                is Result.Failure<*> -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
    }

    private fun opeOtpScreen() {
        CommonUtils.addFragmentUtil(activity,
            FragmentFactory.fragment(FragmentFactory.Fragments.OTP_FRAGMENT, Bundle().apply {
                putString(AppENUM.IntentKeysENUM.COUNTRY_CODE,
                    binding.tvCountryCode.text.toString())
            }),
            target = R.id.fragment_container_2)
        viewModel.provideGetOtpResponse().postValue(null)
    }

    private fun makeSignUpRequest() = binding.apply {
        SignUpRequest().apply {
            types = selectedShopsList
            countryCode = tvCountryCode.text.toString()
            mobile = etPhoneNumber.text.toString()
            workshopName = etGarageName.text.toString()
            if (etOwnerName.text.toString().trim().isNotEmpty()) {
                ownerName = etOwnerName.text.toString()
            }
            if (switchGST.isChecked) {
                workshopGstNo = etGSTNumber.text.toString()
            }
            if (isDistributor){
                ownerType = AppENUM.OwnerTypeENUM.DISTRIBUTOR
            }
            viewModel.provideSignUpRequest().value = this
        }
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_REGISTER)
            putStringArrayList(FirebaseAnalyticsLog.FirebaseEventNameENUM.SHOP_TYPE,
                selectedShopsList as ArrayList<String>)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_SHOP_CREATION_SAVE,
                this)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
            R.id.btnRegister -> binding.apply {
                CommonUtils.hideKeyboard(activity)
                if (checkForm()) {
                    lifecycleScope.launch {
                        showLoader()
                        if (switchGST.isChecked) {
                            val verifyGSTResponse = viewModel.checkGST(etGSTNumber.text.toString())
                            verifyGSTResponse?.let {
                                if ((it.gstinStatus ?: 0) == 1) {
                                    viewModel.callGetOTPAPI(AppSignatureHelper(requireContext()).getAppSignatures()[0],
                                        tvCountryCode.text.toString(),
                                        etPhoneNumber.text.toString())
                                } else {
                                    hideLoader()
                                    CommonUtils.showToast(requireContext(), it.message ?: "")
                                }
                            } ?: run {
                                hideLoader()
                                CommonUtils.showToast(requireContext(),
                                    getString(R.string.error_invalid_gstt))
                            }
                        } else {
                            viewModel.callGetOTPAPI(AppSignatureHelper(requireContext()).getAppSignatures()[0],
                                tvCountryCode.text.toString(),
                                etPhoneNumber.text.toString())
                        }
                    }

                }
            }
            R.id.btnLogin -> {
                popBackStackAndHideKeyboard()
            }
            R.id.copyLinkView -> binding.apply {
                if (etGarageMapLink.text.toString().isNotEmpty()) {
                    val clipboardManager =
                        requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as? ClipboardManager
                    val clipData = ClipData.newPlainText("text", etGarageMapLink.text.toString())
                    clipboardManager?.setPrimaryClip(clipData)
                    CommonUtils.showToast(requireContext(), getString(R.string.text_coppied))
                } else {
                    CommonUtils.showToast(requireContext(), getString(R.string.link_not_available))
                }
            }
            R.id.etGarageAddress -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ADDRESS_FRAGMENT, null),
                    target = R.id.fragment_container_2)
            }
            R.id.viewWorkshop -> binding.apply {
                tvWorkshop.isEnabled = true
                tvWorkshop.typeface = fontSemiBold
                if (!selectedShopsList.contains(AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) selectedShopsList.add(
                    AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
                tvAccessories.isEnabled = false
                tvAccessories.typeface = fontMedium
                selectedShopsList.remove(AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT)
                tvRetailer.isEnabled = false
                tvRetailer.typeface = fontMedium
                selectedShopsList.remove(AppENUM.RefactoredStrings.RETAILER_CONSTANT)
                clDistributorCheck.makeVisible(false)
            }
            R.id.viewAccessories -> binding.apply {
                tvAccessories.isEnabled = true
                tvAccessories.typeface = fontSemiBold
                if (!selectedShopsList.contains(AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT)) selectedShopsList.add(
                    AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT)

                tvWorkshop.isEnabled = false
                tvWorkshop.typeface = fontMedium
                selectedShopsList.remove(AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
                tvRetailer.isEnabled = false
                tvRetailer.typeface = fontMedium
                selectedShopsList.remove(AppENUM.RefactoredStrings.RETAILER_CONSTANT)
                clDistributorCheck.makeVisible(true)
            }
            R.id.viewRetailer -> binding.apply {
                tvRetailer.isEnabled = true
                tvRetailer.typeface = fontSemiBold
                if (!selectedShopsList.contains(AppENUM.RefactoredStrings.RETAILER_CONSTANT)) selectedShopsList.add(
                    AppENUM.RefactoredStrings.RETAILER_CONSTANT)
                tvWorkshop.isEnabled = false
                tvWorkshop.typeface = fontMedium
                selectedShopsList.remove(AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
                tvAccessories.isEnabled = false
                tvAccessories.typeface = fontMedium
                selectedShopsList.remove(AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT)
                clDistributorCheck.makeVisible(true)
            }
            R.id.llCountryCode -> binding.apply {
                FragmentFactory.fragmentDialog(FragmentFactory.Fragments.SELECT_COUNTRY_CODE,
                    Bundle().apply {
                        putString(AppENUM.COUNTRY_CODE, tvCountryCode.text.toString())
                        putParcelableArrayList("", LoginFragment.ccList as ArrayList)
                    },
                    object : ActionListener {
                        override fun onActionItem(extra: Any?, extra2: Any?) {
                            (extra as? CountryCodeResponseModel)?.let {
                                ImageLoader.loadImage(imgCountryFlag, it.image)
                                tvCountryCode.text = it.dialCode
                                viewModel.saveUserCountryCodeDetails(it.image,
                                    it.dialCode,
                                    it.currency,
                                    it.currencySymbol,
                                    it.id.toString())
                                if (it.dialCode == AppENUM.RefactoredStrings.defaultCountryCode) {
                                    clGst.makeVisible(true)
                                } else {
                                    clGst.makeVisible(false)
                                    etGSTNumber.makeVisible(false)
                                    switchGST.isChecked = false
                                }
                                firebaseEventForCountrySelection(it.dialCode,
                                    it.currency,
                                    it.currencySymbol)
                            }
                        }
                    })?.let { dialog ->
                    dialog.show(childFragmentManager, dialog.javaClass.name)
                }
            }

            R.id.clDistributorCheck->{
                if (isDistributor){
                    isDistributor = false
                    binding.tvDistributor.changeViewCompoundDrawablesWithInterinsicBounds(drawableEnd =  ContextCompat.getDrawable(requireContext(), R.drawable.ic_circle_unchecked))
                }else{
                    isDistributor = true
                    binding.tvDistributor.changeViewCompoundDrawablesWithInterinsicBounds(drawableEnd =  ContextCompat.getDrawable(requireContext(), R.drawable.ic_check_circle))
                }
            }
        }
    }

    private fun checkForm(): Boolean {
        if (binding.etGarageName.text.toString().trim().isEmpty()) {
            CommonUtils.showToast(activity, getString(R.string.alert_empty_garage_name))
            binding.etGarageName.requestFocus()
            return false
        }
        if (binding.etPhoneNumber.text.toString().trim().isEmpty()) {
            CommonUtils.showToast(activity, getString(R.string.alert_empty_mobile), true)
            binding.etPhoneNumber.requestFocus()
            return false
        } else if (binding.tvCountryCode.text.toString() == getString(R.string.ind_country_code) && binding.etPhoneNumber.text.toString()
                .trim().length != 10) {
            CommonUtils.showToast(activity, getString(R.string.alert_invalid_mobile), true)
            binding.etPhoneNumber.requestFocus()
            return false
        }
        if (selectedShopsList.isEmpty()) {
            CommonUtils.showToast(activity, getString(R.string.alert_empty_select_shop), true)
            return false
        }
        if (binding.switchGST.isChecked) {
            if (binding.etGSTNumber.text.toString().trim().isEmpty()) {
                CommonUtils.showToast(activity, getString(R.string.alert_empty_gst), true)
                return false
            }
        }
        return true
    }

    override fun onCheckedChanged(p0: CompoundButton?, isChecked: Boolean) {
        if (isChecked) {
            binding.etGSTNumber.makeVisible(true)
        } else binding.etGSTNumber.makeVisible(false)
    }
}