package whitelisted.garage.view.fragments.sparesShop

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentSelectBrandBinding
import whitelisted.garage.databinding.ItemAllBrandsCardBinding
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel

class SelectBrandFragment : BaseFragment() {

    private val binding by viewBinding2(FragmentSelectBrandBinding::inflate)
    private val viewModel by sharedViewModel<SparesShopFragmentViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() = binding?.apply {

        val allBrands = viewModel.searchAndFilterAttrs.brands
        val popularBrands = allBrands.filter { it.isPopular ?: false }

        rvBrands.layoutManager = GridLayoutManager(requireContext(), 4).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when {
                        position == 0 || position == popularBrands.size + 1 -> 4
                        position >= popularBrands.size + 2 -> 2
                        else -> 1
                    }
                }
            }
        }

        if (popularBrands.isNotEmpty()) rvBrands.addSingleView {
            val tvHeader = TextView(requireContext())
            tvHeader.text = resources.getString(R.string.popular_brands)
            tvHeader.setFont()
            tvHeader
        }

        Log.i("POPULAR_BRANDS",popularBrands.toString())
        rvBrands.addViews(popularBrands, ItemAllBrandsCardBinding::inflate) { binding, brand, _ ->
            binding.ivBrand.makeVisible(true)
            ImageLoader.loadImage(
                binding.ivBrand,
                brand.brandIcon,
                placeholderImage = R.drawable.ic_placeholder_square
            )

            binding.tvBrand.makeVisible(false)

            binding.root.setOnClickListener {
                CommonUtils.addFragmentUtil(
                    requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.BRAND_NAME, brand.filterValue)
                        })
                )
            }
        }

        rvBrands.addSingleView {
            val tvHeader = TextView(requireContext())
            tvHeader.text = resources.getString(R.string.all_brands)
            tvHeader.setFont()
            tvHeader
        }

        rvBrands.addViews(allBrands, ItemAllBrandsCardBinding::inflate) { binding, brand, _ ->
            binding.tvBrand.text = brand.name
            binding.root.setOnClickListener {
                CommonUtils.addFragmentUtil(
                    requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.BRAND_NAME, brand.filterValue)
                        })
                )
            }
        }


    }

    private fun TextView.setFont() {
        textSize = 14f
        layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        ).apply {
            setMargins(32, 32, 0, 0)
        }

        typeface = ResourcesCompat.getFont(requireContext(), R.font.gilroy_semibold)
        setTextColor(resources.getColor(R.color.black_color_new, resources.newTheme()))
        setBackgroundColor(resources.getColor(R.color.white, resources.newTheme()))
    }

}