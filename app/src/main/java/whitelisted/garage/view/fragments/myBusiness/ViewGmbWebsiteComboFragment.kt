package whitelisted.garage.view.fragments.myBusiness

import DateUtil
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.GridLayoutManager
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.DayTimeData
import whitelisted.garage.api.response.GMBUnlockFeatures
import whitelisted.garage.api.response.GetGMDResponse
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentViewGmbBinding
import whitelisted.garage.eventBus.GmbRequestedEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.BusShowAttributeAdapter
import whitelisted.garage.view.adapter.BusShowCategoryAdapter
import whitelisted.garage.view.adapter.DayAndTimingAdapter
import whitelisted.garage.view.adapter.PhotoAdapter
import whitelisted.garage.viewmodels.MyBusinessViewModel

class ViewGmbWebsiteComboFragment : BaseFragment() {

    private val binding by viewBinding(FragmentViewGmbBinding::inflate)
    private var businessAttributesAdapter: BusShowAttributeAdapter? = null
    private var businessCategoryAdopter: BusShowCategoryAdapter? = null
    private var dayTimeAdapter: DayAndTimingAdapter? = null
    private var gmbData: GetGMDResponse? = null
    private val viewModel: MyBusinessViewModel by sharedViewModel()
    private lateinit var photoList: MutableList<Uri>
    private lateinit var photoAdapter: PhotoAdapter
    private lateinit var dayTimeData: DayTimeData
    private var attributeData: MutableMap<String, MutableList<String>>? = null
    private var gmbCategoriesMore = listOf<String>()
    private var gmbCategoriesLess = listOf<String>()
    private var gmbMoreAttributeData: MutableMap<String, MutableList<String>>? = null
    private var gmbLessAttributeData: MutableMap<String, MutableList<String>>? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListener()
        observeData()
        setUpRv()
        setData()
        showLoader()
        viewModel.getGMB()
    }

    private fun clickListener() {
        binding.imgBack.setOnClickListener(this)
        binding.tvViewOnSesrch.setOnClickListener(this)
        binding.tvViewOnMap.setOnClickListener(this)
        binding.tvShareProfile.setOnClickListener(this)
        binding.llContactLayout.setOnClickListener(this)
        binding.tvUnCLokBtn.setOnClickListener(this)
        binding.tvSeeMoreLessCategories.setOnClickListener(this)
        binding.tvSeeMoreLessAttributes.setOnClickListener(this)
    }

    private fun setData() {
        binding.llHeader.makeVisible(false)
        binding.scroll.makeVisible(false)
        EventBus.getDefault().post(GmbRequestedEvent(true))
    }

    private fun observeData() {
        observeGetPhoto()
        observeGetBusinessPageData()
        observeGetGMD()
    }

    private fun observeGetGMD() {
        viewModel.provideGMD().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    gmbData = it.body.data
                    setDataToUi(it.body.data)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setDataToUi(data: GetGMDResponse) {
        binding.llHeader.makeVisible(true)
        binding.scroll.makeVisible(true)
        binding.tvGarageName.text = data.name
        binding.tvAddress.text = data.address
        binding.etMobile.setText(data.gmb_mobile)
        setUiAccordingToData(data)
        setUpDayTimeData(data.openTime)
        setCategoriesData(data.gmbCategories)
        setAttributeData(data.gmbAttributes)
        setGaragePhoto(data.workshopImages)
        setGMBActiveInActive(data.unlocked_features)
    }

    private fun setGMBActiveInActive(unlockedFeatures: List<GMBUnlockFeatures>?) {
        if (unlockedFeatures.isNullOrEmpty()) {
            binding.llActiveInactiveGMB.makeVisible(false)
        } else if (unlockedFeatures.isNotEmpty()) {
            binding.llActiveInactiveGMB.makeVisible(false)
            for (data in unlockedFeatures) {
                if (data.feature_code == AppENUM.GMB) {
                    data.expire_on?.let { setDataToValidity(it) }
                    break
                }
            }
        }
    }

    private fun setDataToValidity(data: String) = binding.apply {
        llActiveInactiveGMB.makeVisible(true)
        if (DateUtil.isDateExpired(CommonUtils.convertToDate(data))) {
            llActiveGMB.makeVisible(false)
            llInActiveGMB.makeVisible(true)
            llMessageInActive.makeVisible(true)
            tvInActiveExpDate.text = CommonUtils.convertToDate(data)
        } else {
            llActiveGMB.makeVisible(true)
            llInActiveGMB.makeVisible(false)
            llMessageInActive.makeVisible(false)
            tvActiveExpDate.text = CommonUtils.convertToDate(data)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setGaragePhoto(workshopImages: List<String>?) {
        photoList.clear()
        photoAdapter.getAdapterList().clear()
        photoAdapter.notifyDataSetChanged()
        if (workshopImages?.isNotEmpty() == true) {
            photoAdapter.setIsViewOnly(true)
            for (data in workshopImages) {
                photoAdapter.getAdapterList().add(0, Uri.parse(data))
                photoAdapter.notifyItemInserted(0)
            }
        } else {
            photoList.add(Uri.EMPTY)
            photoAdapter.setIsViewOnly(false)
            photoAdapter.setData(photoList)
        }
    }

    private fun setUiAccordingToData(data: GetGMDResponse) = binding.apply {
        llGMBParent.makeVisible(true)
        scroll.makeVisible(true)
        llHeader.makeVisible(true)
        if (data.gmb_updated == true) {
            if (data.gmb_links != null) {
                tvURL.text = data.gmb_links?.web_link ?: "---"
                enableLinks()
            } else {
                enableGMBViewOnly()
            }
        }
    }

    private fun enableGMBViewOnly() = binding.apply {
        llGMBOProgress.makeVisible(true)
        llShareGoogleProfile.makeVisible(false)
        llContactLayout.makeVisible(true)
    }

    private fun enableLinks() = binding.apply {
        llGMBOProgress.makeVisible(false)
        llShareGoogleProfile.makeVisible(true)
        llContactLayout.makeVisible(true)
    }

    private fun setAttributeData(gmbAttributes: MutableMap<String, MutableList<String>>?) {
        attributeData = gmbAttributes
        if ((attributeData?.size ?: 0) > 2) {
            gmbLessAttributeData = viewModel.getSubList(attributeData)
            gmbMoreAttributeData = attributeData
        } else {
            gmbMoreAttributeData = attributeData
            gmbLessAttributeData = attributeData
        }
        businessAttributesAdapter?.setData(viewModel.getMapKeyList(gmbLessAttributeData),
            gmbLessAttributeData)
        ViewCompat.setNestedScrollingEnabled(binding.rvAttributes, false)
    }

    private fun setCategoriesData(gmbCategories: List<String>?) {
        if (!gmbCategories.isNullOrEmpty()) {
            if (gmbCategories.size > 4) {
                gmbCategoriesLess = gmbCategories.subList(0, 4)
                gmbCategoriesMore = gmbCategories
            } else {
                gmbCategoriesLess = gmbCategories
                gmbCategoriesMore = gmbCategories
            }
            businessCategoryAdopter?.setDataToAdapter(gmbCategoriesLess)
        }
        ViewCompat.setNestedScrollingEnabled(binding.rvCategory, false)
    }

    private fun setUpDayTimeData(openTime: MutableMap<String, List<String>>?) {
        dayTimeData = viewModel.getPrimaryDayTimeData(openTime)
        dayTimeAdapter?.setDataToAdapter(dayTimeData)
        ViewCompat.setNestedScrollingEnabled(binding.rvTimings, false)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun observeGetPhoto() {
        viewModel.providePhotoUriFromAdd().observe(viewLifecycleOwner) {
            it?.size?.let { it1 ->
                repeat(it1) { position ->
                    if (it[position].toString().isNotEmpty()) {
                        photoAdapter.getAdapterList().add(0, (it[position]))
                    }
                    photoAdapter.notifyDataSetChanged()
                }
            }
        }
    }

    private fun observeGetBusinessPageData() {
        viewModel.provideBusinessPageData().observe(viewLifecycleOwner) {
            binding.tvGarageName.text = it?.name
            binding.etMobile.setText(it?.mobile)
        }
    }

    private fun setUpRv() {
        photoList = ArrayList()
        photoAdapter = PhotoAdapter(this)
        binding.rvGaragePhoto.layoutManager =
            GridLayoutManager(requireContext(), 4, GridLayoutManager.VERTICAL, false)
        binding.rvGaragePhoto.adapter = photoAdapter
        photoAdapter.setData(photoList)
        dayTimeAdapter = DayAndTimingAdapter().apply {
            binding.rvTimings.adapter = this
        }
        businessCategoryAdopter = BusShowCategoryAdapter().apply {
            binding.rvCategory.adapter = this
        }
        businessAttributesAdapter = BusShowAttributeAdapter().apply {
            binding.rvAttributes.adapter = this
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBusImage -> {
                CommonUtils.genericCastOrNull<Uri>(v.getTag(R.id.model))?.let { uri ->
                    imagePreview(view, uri)
                }
            }
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.llContactLayout -> {
                sendEmail(AppENUM.HELP_SUPPORT_MAIL,
                    viewModel.getStringSharedPreference(AppENUM.USER_ID, ""))
            }
            R.id.tvViewOnMap -> {
                openWebPage(gmbData?.gmb_links?.short_map_link ?: "")
            }
            R.id.tvViewOnSesrch -> {
                openWebPage(gmbData?.gmb_links?.search_link ?: "")
            }
            R.id.tvShareProfile -> {
                shareProfile()
            }
            R.id.tvSeeMoreLessCategories -> {
                if (binding.tvSeeMoreLessCategories.tag == AppENUM.RefactoredStrings.SEE_MORE) {
                    binding.tvSeeMoreLessCategories.tag = AppENUM.RefactoredStrings.SEE_LESS
                    businessCategoryAdopter?.setDataToAdapter(gmbCategoriesLess)
                    binding.tvSeeMoreLessCategories.text = getString(R.string.see_more)
                } else {
                    binding.tvSeeMoreLessCategories.tag = AppENUM.RefactoredStrings.SEE_MORE
                    businessCategoryAdopter?.setDataToAdapter(gmbCategoriesMore)
                    binding.tvSeeMoreLessCategories.text = getString(R.string.see_less)
                }
            }
            R.id.tvSeeMoreLessAttributes -> {
                if (binding.tvSeeMoreLessAttributes.tag == AppENUM.RefactoredStrings.SEE_MORE) {
                    binding.tvSeeMoreLessAttributes.tag = AppENUM.RefactoredStrings.SEE_LESS
                    businessAttributesAdapter?.setData(viewModel.getMapKeyList(gmbLessAttributeData),
                        gmbLessAttributeData)
                    binding.tvSeeMoreLessAttributes.text = getString(R.string.see_more)
                } else {
                    binding.tvSeeMoreLessAttributes.tag = AppENUM.RefactoredStrings.SEE_MORE
                    businessAttributesAdapter?.setData(viewModel.getMapKeyList(gmbMoreAttributeData),
                        gmbMoreAttributeData)
                    binding.tvSeeMoreLessAttributes.text = getString(R.string.see_less)
                }
            }
        }
    }

    private fun shareProfile() {
        Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT,
                getString(R.string.gmb_combo_share, gmbData?.gmb_links?.web_link))
            type = "text/plain"
            Intent.createChooser(this, null)
            startActivity(this)
        }
    }

    private fun openWebPage(url: String?) {
        Uri.parse(url).apply {
            Intent(Intent.ACTION_VIEW, this).apply {
                if (this.resolveActivity(requireActivity().packageManager) != null) {
                    startActivity(this)
                }
            }
        }
    }
}
