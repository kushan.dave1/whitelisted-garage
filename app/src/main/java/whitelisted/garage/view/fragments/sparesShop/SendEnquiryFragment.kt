package whitelisted.garage.view.fragments.sparesShop

import android.os.Bundle
import android.view.View
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.lifecycleScope
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.view_enquiry.view.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.request.AddressData
import whitelisted.garage.api.request.Enquiry
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentSendEnquiryBinding
import whitelisted.garage.eventBus.GoCoinsAddedEvent
import whitelisted.garage.eventBus.LocationSelectedEvent
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.viewmodels.SelectAddressViewModel
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import java.util.*

class SendEnquiryFragment : BaseFragment() {

    private val binding by viewBinding(FragmentSendEnquiryBinding::inflate)
    private val storageRef by lazy { FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference }
    private val viewModel by sharedViewModel<SparesShopFragmentViewModel>()
    private val addressViewModel: SelectAddressViewModel by sharedViewModel()
    private val searchText by getString(AppENUM.SEARCH_STRING)
    private var enquiry: Enquiry? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setCartItemsCount()
        getEnquiryCost()
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is GoCoinsAddedEvent -> getEnquiryCost(true)
            is LocationSelectedEvent -> saveAddress(event.addressData)
        }


    }

    private fun saveAddress(addressData: AddressData) = lifecycleScope.launch {

        requireActivity().let {
            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.ADD_EDIT_ADDRESS,
                Bundle().apply {
                    putParcelable(AppENUM.ADDRESS_POST, addressData)
                })?.let { baseFragment ->
                baseFragment.show(
                    it.supportFragmentManager,
                    baseFragment.javaClass.name
                )
            }
        }
    }

    private fun getEnquiryCost(sendEnquiry: Boolean = false) = lifecycleScope.launch {
        showLoader()
        val balance = viewModel.getGoCoinsBalance()
        binding.evEnquiry.setEnquiryCost(viewModel.getEnquiryCost(), balance)
        if (sendEnquiry) binding.evEnquiry.btnSendEnquiry.callOnClick()
        hideLoader()
    }


    private fun initViews() {
        val timestamp = System.currentTimeMillis()
        val productEnqImagesRef = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
            .child(viewModel.getStringSharedPreference(AppENUM.USER_ID) + "/")
            .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
            .child(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID) + "/")
            .child(AppENUM.RefactoredStrings.PATH_SS_PRODUCT_ENQUIRY)
            .child("ss_product_$timestamp/")
        val wid = viewModel.getSharedPreference()
            .getString(requireContext(), AppENUM.UserKeySaveENUM.WORKSHOP_ID, "")
        binding.evEnquiry.setPartName(searchText).setWorkshopId(wid)
            .setUploadingPath(productEnqImagesRef).setOnUploadingStartListener { showLoader() }
            .setOnSentDialogDismissListener { onSentDialogDismiss() }
            .setOnUploadingFinishedListener { checkAddressAndSendEnquiry(it) }

        binding.imgCart.setOnClickListener {
            fireInitCartEvent()
            CommonUtils.addFragmentUtil(
                requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.SS_CART)
            )
        }

        binding.imgBack.setOnClickListener {
            popBackStackAndHideKeyboard()
        }
        setFragmentResultListener(AppENUM.ADD_ADDRESS) { _, _ ->
            sendEnquiry()
        }
    }

    private fun onSentDialogDismiss() {
        if (isAdded) {
            val trans = requireActivity().supportFragmentManager.beginTransaction()
            requireActivity().supportFragmentManager.fragments.forEach {
                when (it) {
                    is SelectCarAndSearchFragment -> trans.remove(it)
                    is ShopWithFilterFragment -> trans.remove(it)
                }
            }
            trans.commit()
            popBackStackAndHideKeyboard()
        }
    }

    private fun setCartItemsCount() {
        viewModel.provideGetCartResponse().let {
            if (it.cartList.size > 1) {
                val itemsCount = it.cartList[1].lineItems.size
                itemsCount.plus(it.cartList[0].lineItems.size).let { itemCount ->
                    if (itemCount > 0) {
                        binding.tvCartCount.makeVisible(true)
                        binding.tvCartCount.text = "$itemCount"
                    } else binding.tvCartCount.makeVisible(false)
                }
            }
        }
    }

    private fun checkAddressAndSendEnquiry(enquiry: Enquiry) = lifecycleScope.launch {
        this@SendEnquiryFragment.enquiry = enquiry
        if (addressViewModel.getAddresses().isEmpty()) showSelectAddressDialog()
        else sendEnquiry()
    }

    private fun sendEnquiry() = lifecycleScope.launch {
        showLoader()
        val isSuccessfullySent = viewModel.sendEnquiry(enquiry ?: Enquiry())
        hideLoader()
        if (isSuccessfullySent) {
            binding.evEnquiry.showEnquirySuccessDialog()
            CommonUtils.showReviewDialog(requireActivity())
            CommonUtils.showFakeReviewDialog(requireActivity())
            fireSendEnquiryEvent(enquiry ?: Enquiry())
        } else CommonUtils.showToast(
            requireContext(),
            resources.getString(R.string.unable_to_send_enquiry)
        )
    }


    private fun showSelectAddressDialog() {
        hideLoader()
        CommonUtils.addFragmentUtil(activity,
            FragmentFactory.fragment(FragmentFactory.Fragments.ADDRESS_FRAGMENT, Bundle().apply {
                putBoolean(AppENUM.SELECT_ADDRESS_ENQUIRY, true)
            }))

    }

    private fun fireInitCartEvent() {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_PDP
        )
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_CART,
            bundle
        )
    }


    private fun fireSendEnquiryEvent(enquiry: Enquiry) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PART_NAME, enquiry.productName)
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.QUANTITY,
            enquiry.quantity.toString()
        )
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.REMARK, enquiry.remarks)

        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP_ENQUIRY
        )

        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.ENQUIRE_NOW_OOS,
            bundle
        )
    }


}