package whitelisted.garage.view.fragments.workshopBidding

import android.os.Bundle
import android.view.View
import whitelisted.garage.R
import whitelisted.garage.api.response.GetWsBidEnquiryResponse
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentWorkshopBidStatusBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FragmentFactory

class WorkshopBiddingStatusFragment : BaseFragment() {

    private val binding by viewBinding(FragmentWorkshopBidStatusBinding::inflate)
    private var isViewOnly: Boolean = false
    //  private val viewModel: WorkShopBiddingViewModel by sharedViewModel()
    private var wsBidEnquiryResponse: GetWsBidEnquiryResponse? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListener()
        getDataFromArgs()
        binding.animationView.setAnimation(R.raw.auction)
    }

    private fun getDataFromArgs() {
        arguments?.let {
            isViewOnly = it.getBoolean(AppENUM.IS_VIEW_ONLY, false)
            wsBidEnquiryResponse = it.getParcelable(AppENUM.USER_DATA)
        }
        setUpData()
    }

    private fun setUpData() = binding.apply {
        if (isViewOnly) {
            llConfirmDeal.makeVisible(true)
            llPendingBidStatus.makeVisible(false)
            btnProceed.makeVisible(false)
            btnDone.makeVisible(true)
            wsBidEnquiryResponse?.let {
                tvWorkshopName.text = it.workshopName
                tvContact.text = it.contactNumber
                tvAddress.text = it.workshopAddress
                tvTotalItem.text = (it.totalItem ?: 0).toString()
                tvPrice.text = (it.totalAmount ?: 0).toString()

            }
        } else {
            llConfirmDeal.makeVisible(false)
            llPendingBidStatus.makeVisible(true)
            btnProceed.makeVisible(true)
            btnDone.makeVisible(false)
        }
    }


    private fun clickListener() {
        binding.btnProceed.setOnClickListener(this)
        binding.btnDone.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnProceed -> {
                popBackStackAndHideKeyboard()
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.WORKSHOP_BIDDING_LIST_FRAGMENT,
                        null))
            }
            R.id.btnDone -> {
                popBackStackAndHideKeyboard()
            }
        }
    }
}