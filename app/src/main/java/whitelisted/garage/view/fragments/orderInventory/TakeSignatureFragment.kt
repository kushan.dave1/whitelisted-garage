package whitelisted.garage.view.fragments.orderInventory

import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import org.greenrobot.eventbus.EventBus
import whitelisted.garage.R
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentTakeSignatureBinding
import whitelisted.garage.eventBus.UpdateSignatureEvent
import whitelisted.garage.utils.CommonUtils

class TakeSignatureFragment : BaseFragment() {

    private val binding by viewBinding(FragmentTakeSignatureBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnClear.setOnClickListener(this)
        binding.btnSave.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)
    }

    override fun onClick(v: View) {

        when (v.id) {
            R.id.btnClear -> {
                binding.signatureView.clearCanvas()
            }
            R.id.btnSave -> {

                val emptyBitmap = Bitmap.createBitmap(binding.signatureView.signatureBitmap.width,
                    binding.signatureView.signatureBitmap.height,
                    Bitmap.Config.ARGB_8888)

                val canvas = Canvas(emptyBitmap);
                canvas.drawColor(ContextCompat.getColor(v.context, R.color.white))

                if (emptyBitmap.sameAs(binding.signatureView.signatureBitmap)) {
                    CommonUtils.showToast(activity, getString(R.string.please_enter_a_sig))
                } else {
                    EventBus.getDefault().post(UpdateSignatureEvent(binding.signatureView.signatureBitmap))
                    popBackStackAndHideKeyboard()
                }

            }
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }
        }
    }

    override fun onResume() {
        super.onResume()

    }
}