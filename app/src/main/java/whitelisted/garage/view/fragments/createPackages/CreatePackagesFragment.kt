package whitelisted.garage.view.fragments.createPackages

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.AllPackagesResponseModel
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentCreatePackagesBinding
import whitelisted.garage.eventBus.UpdatePackageEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.ExistingPackagesAdapter
import whitelisted.garage.viewmodels.CreatePackagesFragmentViewModel
import whitelisted.garage.viewmodels.DashboardSharedViewModel

class CreatePackagesFragment : BaseFragment() {

    private val binding by viewBinding(FragmentCreatePackagesBinding::inflate)
    private val viewModel: CreatePackagesFragmentViewModel by viewModel()
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var existingPackagesList: MutableList<AllPackagesResponseModel> = mutableListOf()
    private lateinit var existingPackagesAdapter: ExistingPackagesAdapter
    private var isTwoWheeler = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        binding.imgBack.setOnClickListener(this)
        binding.btnAddPackage.setOnClickListener(this)
        binding.imgSwitchVehicleType.setOnClickListener(this)

        setTextWatcher()

        observeExistingPackagesResponse()
        observeDuplicatePackageResponse()

        when (viewModel.getStringSharedPreference(
            AppENUM.UserKeySaveENUM.SHOP_TYPE, AppENUM.RefactoredStrings.WORKSHOP_CONSTANT
        )) {
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                binding.tvExistingPackage.makeVisible(false)
            }
            else -> {
                showLoader()
                viewModel.getExistingPackages()
            }
        }
    }

    private fun setTextWatcher() {
        binding.etPackageName.doOnTextChanged { text, _, _, _ ->
            binding.btnAddPackage.isEnabled = text?.isNotEmpty() == true
        }
    }

    private fun observeDuplicatePackageResponse() {
        viewModel.provideDuplicatePackageResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    EventBus.getDefault().post(UpdatePackageEvent())
                    viewModel.getExistingPackages()
                }
                is Result.Failure -> {
                    hideLoader()
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeExistingPackagesResponse() {
        viewModel.providePackagesListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    existingPackagesList.addAll(it.body.data)
                    lifecycleScope.launch {
                        val bikeCustomPackagesList = viewModel.getCustomPackagesListBike()
                        hideLoader()
                        bikeCustomPackagesList?.let { bikePackages ->
                            if (bikePackages.isNotEmpty()) {
                                existingPackagesList.addAll(bikePackages)
                            }
                        }
                        setExistingPackagesAdapter()
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setExistingPackagesAdapter() {
        binding.rvExistingPackages.layoutManager = LinearLayoutManager(requireContext())
        existingPackagesAdapter = ExistingPackagesAdapter(existingPackagesList, this)
        binding.rvExistingPackages.adapter = existingPackagesAdapter
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.btnAddPackage -> binding.etPackageName.text.toString().let {
                if (viewModel.getBooleanSharedPreference(
                        AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS, false
                    )
                ) {
                    if (it.isNotEmpty()) {
                        addPackage(it)
                        Bundle().apply {
                            putString(
                                FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_CREATE_PACKAGE
                            )
                            FirebaseAnalyticsLog.trackFireBaseEventLog(
                                FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_Add_PACKAGE,
                                this
                            )

                        }
                    } else CommonUtils.showToast(
                        requireContext(), getString(R.string.alert_enter_package_name), true
                    )
                } else {

                    val customPackagesCount = arguments?.getInt(AppENUM.PACKAGE_COUNT) ?: 0

                    if (customPackagesCount < (dashboardViewModel.screensConfigResponse.value?.membershipConfig?.packageLimit
                            ?: 1)
                    ) {
                        if (it.isNotEmpty()) {
                            addPackage(it)
                            Bundle().apply {
                                putString(
                                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_CREATE_PACKAGE
                                )
                                FirebaseAnalyticsLog.trackFireBaseEventLog(
                                    FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_Add_PACKAGE,
                                    this
                                )

                            }
                        } else CommonUtils.showToast(
                            requireContext(), getString(R.string.alert_enter_package_name), true
                        )
                    } else {
                        fireFeatureLockedEvent()
                        requireActivity().let {
                            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT,
                                Bundle().apply {
                                    putString(
                                        AppENUM.PRO_LOCKED_ALERT_MSG,
                                        dashboardViewModel.screensConfigResponse.value?.membershipConfig?.packageLimitMsg
                                    )
                                })?.let { baseFragment ->
                                baseFragment.show(
                                    it.supportFragmentManager, baseFragment.javaClass.name
                                )
                            }
                        }
                    }
                }
            }
            R.id.btnEditPackage -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    CommonUtils.genericCastOrNull<AllPackagesResponseModel>(v.getTag(R.id.model))
                        ?.let { extra ->
                            editPackage(position, extra)
                        }
                }

            }
            R.id.btnDuplicatePackage -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    CommonUtils.genericCastOrNull<AllPackagesResponseModel>(v.getTag(R.id.model))
                        ?.let { extra ->
                            duplicatePackage(position, extra)
                        }
                }
            }

            R.id.imgSwitchVehicleType -> {
                if (isTwoWheeler) {
                    isTwoWheeler = false
                    binding.lblFourWheeler.changeTextColor(requireContext(), R.color.colorAccent)
                    binding.lblTwoWheeler.changeTextColor(requireContext(), R.color.black_text_new)
                    ImageLoader.loadDrawable(
                        ContextCompat.getDrawable(
                            requireContext(), R.drawable.ic_double_switch_off
                        ), binding.imgSwitchVehicleType
                    )
                } else {
                    isTwoWheeler = true
                    binding.lblFourWheeler.changeTextColor(requireContext(), R.color.black_text_new)
                    binding.lblTwoWheeler.changeTextColor(requireContext(), R.color.colorAccent)
                    ImageLoader.loadDrawable(
                        ContextCompat.getDrawable(
                            requireContext(), R.drawable.ic_double_switch_on
                        ), binding.imgSwitchVehicleType
                    )
                }
            }
        }
    }

    private fun addPackage(editable: String) {
        when (viewModel.getStringSharedPreference(
            AppENUM.UserKeySaveENUM.SHOP_TYPE, AppENUM.RefactoredStrings.WORKSHOP_CONSTANT
        )) {
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(
                        FragmentFactory.Fragments.CREATE_EDIT_ACCESS_PACKAGE,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.PACKAGE_NAME, editable)
                            putBoolean(AppENUM.IntentKeysENUM.IS_NEW_PACKAGE, true)
                            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, isTwoWheeler)

                        })
                )
            }
            else -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(
                        FragmentFactory.Fragments.CREATE_EDIT_PACKAGE,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.PACKAGE_NAME, editable)
                            putBoolean(AppENUM.IntentKeysENUM.IS_NEW_PACKAGE, true)
                            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, isTwoWheeler)

                        })
                )
            }
        }
    }


    private fun editPackage(position: Int, extra: AllPackagesResponseModel) {
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(
                        FragmentFactory.Fragments.CREATE_EDIT_ACCESS_PACKAGE,
                        Bundle().apply {
                            putBoolean(
                                AppENUM.IntentKeysENUM.IS_TWO_WHEELER, extra.isTwoWheeler ?: false
                            )
                            putBoolean(AppENUM.IntentKeysENUM.IS_EDIT, true)
                            putString(AppENUM.IntentKeysENUM.PACKAGE_NAME, extra.name)
                            putString(
                                AppENUM.IntentKeysENUM.PACKAGE_ID,
                                existingPackagesList[position].packageId.toString()
                            )
                        })
                )
            }
            else -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(
                        FragmentFactory.Fragments.CREATE_EDIT_PACKAGE,
                        Bundle().apply {
                            putBoolean(
                                AppENUM.IntentKeysENUM.IS_TWO_WHEELER, extra.isTwoWheeler ?: false
                            )
                            putBoolean(AppENUM.IntentKeysENUM.IS_EDIT, true)
                            putString(AppENUM.IntentKeysENUM.PACKAGE_NAME, extra.name)
                            putString(
                                AppENUM.IntentKeysENUM.PACKAGE_ID,
                                existingPackagesList[position].packageId.toString()
                            )
                        })
                )
            }
        }
    }

    private fun duplicatePackage(position: Int, extra: AllPackagesResponseModel) {
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(
                        FragmentFactory.Fragments.CREATE_EDIT_ACCESS_PACKAGE,
                        Bundle().apply {
                            putBoolean(
                                AppENUM.IntentKeysENUM.IS_TWO_WHEELER, extra.isTwoWheeler ?: false
                            )
                            putBoolean(AppENUM.IntentKeysENUM.IS_DUPLICATE, true)
                            putString(AppENUM.IntentKeysENUM.PACKAGE_NAME, extra.name)
                            putString(
                                AppENUM.IntentKeysENUM.PACKAGE_ID,
                                existingPackagesList[position].packageId.toString()
                            )
                        })
                )
            }
            else -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(
                        FragmentFactory.Fragments.CREATE_EDIT_PACKAGE,
                        Bundle().apply {
                            putBoolean(
                                AppENUM.IntentKeysENUM.IS_TWO_WHEELER, extra.isTwoWheeler ?: false
                            )
                            putBoolean(AppENUM.IntentKeysENUM.IS_DUPLICATE, true)
                            putString(AppENUM.IntentKeysENUM.PACKAGE_NAME, extra.name)
                            putString(
                                AppENUM.IntentKeysENUM.PACKAGE_ID,
                                existingPackagesList[position].packageId.toString()
                            )
                        })
                )
            }
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is UpdatePackageEvent -> {
                showLoader()
                viewModel.getExistingPackages()
            }
        }
    }


    private fun fireFeatureLockedEvent() {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_CREATE_PACKAGE
        )
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.ERROR_HEADER_PRO, bundle
        )

    }

}