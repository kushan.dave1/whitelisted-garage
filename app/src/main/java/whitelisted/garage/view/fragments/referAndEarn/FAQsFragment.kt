package whitelisted.garage.view.fragments.referAndEarn

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_refer_and_earn.imgBack
import whitelisted.garage.R
import whitelisted.garage.api.response.Faq
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentFaqSBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.view.adapter.ReferAndEarnFAQAdapter

class FAQsFragment : BaseFragment() {

    private val binding by viewBinding(FragmentFaqSBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()

    }


    private fun setupScreen() {

        imgBack.setOnClickListener(this)
        arguments?.let {
            val data = arguments?.getParcelableArrayList<Faq>(AppENUM.FAQ) as List<Faq>
            binding.rvFAQ.adapter = ReferAndEarnFAQAdapter(data, this)
        }


    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> {
                popBackStackAndHideKeyboard()
            }

            R.id.clBase -> {


                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.clBase))?.let { position ->

                    (binding.rvFAQ.adapter as ReferAndEarnFAQAdapter).dataList[position].isSelected =
                        (binding.rvFAQ.adapter as ReferAndEarnFAQAdapter).dataList[position].isSelected != true
                    (binding.rvFAQ.adapter as ReferAndEarnFAQAdapter).notifyItemChanged(position)
                }


            }
        }
    }


    override fun onResume() {
        super.onResume()


    }
}