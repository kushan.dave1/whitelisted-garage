package whitelisted.garage.view.fragments.createEditPackage

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.data.GetAccessoriesCatalogueResponse
import whitelisted.garage.api.data.RecyclerData
import whitelisted.garage.api.request.CreateCustomPackageRequest
import whitelisted.garage.api.request.PPIAndQuantity
import whitelisted.garage.api.request.ServicesItem
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentCreateEditPackageBinding
import whitelisted.garage.eventBus.UpdatePackageEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.AddAssessorsPartAdapter
import whitelisted.garage.view.adapter.AddedItemsAdapter
import whitelisted.garage.view.fragments.managePackages.ManagePackagesFragment
import whitelisted.garage.viewmodels.CreateEditAccessoriesPackageViewModel

class CreateEditAccessoriesPackageFragment : BaseFragment(), TextWatcher {

    private val binding by viewBinding(FragmentCreateEditPackageBinding::inflate)
    private val viewModel: CreateEditAccessoriesPackageViewModel by viewModel()
    private lateinit var addPartsAdapter: AddAssessorsPartAdapter
    private lateinit var addedItemsList: MutableList<ServicesItem>
    private lateinit var addedItemsAdapter: AddedItemsAdapter
    private var isEdit = false
    private var isDuplicate = false
    private var packageId: String? = null
    private var isNewPackage = false
    private var totalAmount: Double? = null
    private lateinit var packageName: String
    private lateinit var searchedServicesList: MutableList<GetAccessoriesCatalogueResponse>
    private var searchText = ""
    private var isTwoWheeler = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() = with(binding) {
        handleBackClick()
        arguments?.let {
            etPackageName.setText(it.getString(AppENUM.IntentKeysENUM.PACKAGE_NAME) ?: "")
            isTwoWheeler = it.getBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, false)
            when {
                it.getBoolean(AppENUM.IntentKeysENUM.IS_EDIT) -> {
                    packageId = it.getString(AppENUM.IntentKeysENUM.PACKAGE_ID, "")
                    isEdit = true
                    tvHeader.text = getString(R.string.edit_package)
                    btnCreateUpdatePackage.text = getString(R.string.update_package)
                    clPackage.makeVisible(true)
                    clSearchItem.makeVisible(false)
                    showLoader()
                    viewModel.getAccCustomPackageDetails(packageId ?: "", isTwoWheeler)
                }
                it.getBoolean(AppENUM.IntentKeysENUM.IS_DUPLICATE) -> {
                    packageId = it.getString(AppENUM.IntentKeysENUM.PACKAGE_ID)
                    packageName = etPackageName.text.toString()
                    isDuplicate =
                        true //                    tvHeader.text = getString(R.string.duplicate_package)
                    clPackage.makeVisible(true)
                    clSearchItem.makeVisible(false)
                    showLoader()
                    viewModel.getAccCustomPackageDetails(packageId ?: "", isTwoWheeler)
                }
                it.getBoolean(AppENUM.IntentKeysENUM.IS_NEW_PACKAGE) -> {
                    isNewPackage = true
                    clPackage.makeVisible(false)
                    clSearchItem.makeVisible(true)
                }
            }
        }
        setClickListeners()
        setObservers()
    }

    private fun handleBackClick() {
        view?.isFocusableInTouchMode = true
        view?.requestFocus()
        view?.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) with(binding) {
                if (clSearchItem.visibility == View.VISIBLE) {
                    clPackage.makeVisible(true)
                    clSearchItem.makeVisible(false)
                    false
                } else {
                    popBackStackAndHideKeyboard()
                    true
                }
            } else false
        }
    }

    private fun setObservers() {
        observeCataloguePackageResponse()
        observeCreatePackageResponse()
        observeGetAccCustomPackageDetailsResponse()
    }

    private fun observeGetAccCustomPackageDetailsResponse() {
        viewModel.provideGetAccCustomPackageDetails().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    addedItemsList = viewModel.convertIntoListOfServiceItemObject(it.body.data)
                    setAddedItemsAdapter()
                    updateTotalPrice()
                }

                is Result.Failure -> {
//                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeCataloguePackageResponse() {
        viewModel.provideSearchCatalogueResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    (it.body.data as? MutableList<GetAccessoriesCatalogueResponse>)?.let { list ->
                        searchedServicesList = list
                    }
                    setAddPartsAdapter()
                }

                is Result.Failure -> {
//                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeCreatePackageResponse() {
        viewModel.provideCreatePackageResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.package_created),
                        true)
                    removeAllFragmentsTill(ManagePackagesFragment::class.java.simpleName)
                    EventBus.getDefault().post(UpdatePackageEvent())
                }
                is Result.Failure -> { //                    removeAllFragmentsTill(ManagePackagesFragment::class.java.simpleName)
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setClickListeners() {
        binding.imgBack.setOnClickListener(this)
        binding.btnCancel.setOnClickListener(this)
        binding.etSearchPart.addTextChangedListener(this)
        binding.btnAddItems.setOnClickListener(this)
        binding.btnCreateUpdatePackage.setOnClickListener(this)
        binding.addServiceByOwn.btnAddServiceByOwn.setOnClickListener(this)
    }

    private fun setAddedItemsAdapter() {
        binding.rvAddedItems.layoutManager = LinearLayoutManager(requireContext())
        if (!::addedItemsList.isInitialized) addedItemsList = mutableListOf()
        addedItemsAdapter =
            AddedItemsAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default)), addedItemsList, this)
        binding.rvAddedItems.adapter = addedItemsAdapter
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { // not in use
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        searchText = p0.toString().trim()
        if (searchText.isNotEmpty()) {
            showLoader()
            viewModel.launch(Dispatchers.Main) {
                delay(300)
                binding.rvPartsServices.makeVisible(true)
                viewModel.getAccessoriesCatalogue(searchText, isTwoWheeler)
            }
        } else {
            binding.rvPartsServices.makeVisible(false)
            binding.rvPartsServices.adapter = null
        }
    }

    override fun afterTextChanged(p0: Editable?) { //  nothing
    }

    private fun setAddPartsAdapter() {
        if (!::searchedServicesList.isInitialized) searchedServicesList = mutableListOf()
        addPartsAdapter = AddAssessorsPartAdapter(searchedServicesList, this)
        binding.rvPartsServices.adapter = addPartsAdapter

        if (searchedServicesList.isNotEmpty()) {
            binding.noSearchView.makeVisible(false)
            binding.rvPartsServices.makeVisible(true)
        } else {
            binding.addServiceByOwn.tvSearchText.text = searchText
            binding.noSearchView.makeVisible(true)
            binding.rvPartsServices.makeVisible(false)
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> with(binding) {
                if (clSearchItem.visibility == View.VISIBLE) {
                    clSearchItem.makeVisible(false)
                    clPackage.makeVisible(true)
                } else popBackStackAndHideKeyboard()
            }

            R.id.btnCancel -> with(binding) {
                etSearchPart.setText("")
                rvPartsServices.adapter = null
                clPackage.makeVisible(true)
                clSearchItem.makeVisible(false)
            }
            R.id.btnAddItems -> with(binding) {
                etSearchPart.setText("")
                rvPartsServices.adapter = null
                clPackage.makeVisible(false)
                clSearchItem.makeVisible(true)
            }

            R.id.btnCreateUpdatePackage -> {
                createORUpdatePackage()
            }
            R.id.tvAdd -> {
                CommonUtils.genericCastOrNull<RecyclerData<GetAccessoriesCatalogueResponse>>(v.getTag(
                    R.id.model))?.let {

                    FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.ADD_ITEM,
                        Bundle().apply {
                            putString(AppENUM.SELECTED_PART, it.data.categoryName)
                            putBoolean(AppENUM.IS_ACCESSORIES, true)
                        },
                        object : ActionListener {
                            override fun onActionItem(extra: Any?, extra2: Any?) {
                                if (binding.rvAddedItems.adapter == null) setAddedItemsAdapter()
                                addItemToList(it.data, extra)
                                binding.clSearchItem.makeVisible(false)
                                binding.clPackage.makeVisible(true)
                                updateTotalPrice()
                            }
                        })?.let { dialog ->
                        dialog.show(parentFragmentManager, dialog.javaClass.name)
                    }
                }
            }
            R.id.imgEdit -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.model))?.let { position ->
                    FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.ADD_ITEM,
                        Bundle().apply {
                            putString(AppENUM.SELECTED_PART, addedItemsList[position].serviceName)
                            putString(AppENUM.SELECTED_WORK_DONE, addedItemsList[position].workDone)
                            putString(AppENUM.PRICE_PER_ITEM,
                                addedItemsList[position].pricePerItem.toString())
                            putBoolean(AppENUM.IS_ACCESSORIES, true)
                            putString(AppENUM.QUANTITY,
                                addedItemsList[position].quantity.toString())
                            putString(AppENUM.TAX_RATE, addedItemsList[position].taxRate.toString())
                            putString(AppENUM.TOTAL_AMOUNT,
                                addedItemsList[position].total.toString())
                            putBoolean(AppENUM.IntentKeysENUM.IS_EDIT, true)
                        },
                        object : ActionListener {
                            override fun onActionItem(extra: Any?, extra2: Any?) {
                                (extra as? PPIAndQuantity)?.let {
                                    addedItemsList[position].pricePerItem = it.pricePerItem
                                    addedItemsList[position].quantity = it.quantity
                                    addedItemsList[position].taxRate = it.taxRate
                                    addedItemsList[position].total = it.totalAmount
                                    addedItemsAdapter.notifyItemChanged(position)
                                    updateTotalPrice()
                                }
                            }
                        })?.let { dialog ->
                        dialog.show(parentFragmentManager, dialog.javaClass.name)
                    }
                }
            }
            R.id.imgDel -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.model))?.let { position ->
                    addedItemsList.removeAt(position)
                    addedItemsAdapter.notifyItemRemoved(position)
                    updateTotalPrice()
                }
            }

            R.id.btnAddServiceByOwn -> addServiceByOwn()
        }
    }

    private fun createORUpdatePackage() {
        if (addedItemsList.isNotEmpty()) {
            val createPackageRequest = CreateCustomPackageRequest()
            createPackageRequest.name = binding.etPackageName.text.toString()
            createPackageRequest.description = ""
            createPackageRequest.total = totalAmount
            createPackageRequest.items = addedItemsList
            createPackageRequest.package_id = packageId

            Bundle().apply {
                putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_CREATE_EDIT_PACKAGE)
                putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PACK_NAME,
                    createPackageRequest.name)
                putDouble(FirebaseAnalyticsLog.FirebaseEventNameENUM.PACK_PRICE,
                    createPackageRequest.total ?: 0.0)
                val items = ArrayList<String>()
                for (name in addedItemsList) {
                    items.add(name.serviceName ?: "")
                }

                putStringArrayList(FirebaseAnalyticsLog.FirebaseEventNameENUM.PACK_ITEMS, items)
                if (isEdit) {
                    putString(
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.ACTION_TYPE,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.ACTION_UPDATE,
                    )
                } else {
                    putString(
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.ACTION_TYPE,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.ACTION_CREATE,
                    )
                }
                FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_CREATE_PACKAGE,
                    this)
            }

            when {
                isDuplicate -> {
                    when {
                        checkForEmptyServices() -> CommonUtils.showToast(requireContext(),
                            getString(R.string.error_add_services))
                        binding.etPackageName.text.toString() == packageName -> {
                            CommonUtils.showToast(requireContext(),
                                getString(R.string.error_package_name_duplicate),
                                true)
                        }
                        else -> {
                            showLoader()
                            viewModel.createCustomPackageAPI(createPackageRequest, isTwoWheeler)
                        }
                    }
                }
                else -> {
                    showLoader()
                    viewModel.createCustomPackageAPI(createPackageRequest, isTwoWheeler)
                }
            }
        } else CommonUtils.showToast(requireContext(), getString(R.string.error_add_item))
    }

    private fun checkForEmptyServices(): Boolean {
        var flag = false
        addedItemsList.forEach {
            if (it.pricePerItem!! < 1) {
                flag = true
                return@forEach
            }
        }
        return flag
    }

    private fun addItemToList(workDonePosition: Any?, ppiAndQuantity: Any?) {
        ServicesItem().apply {
            (ppiAndQuantity as? PPIAndQuantity)?.let {
                pricePerItem = it.pricePerItem
                quantity = it.quantity
                total = it.totalAmount
                taxRate = it.taxRate
                (workDonePosition as? GetAccessoriesCatalogueResponse)?.let { getAccessoriesCatalogueResponse ->
                    part_name = getAccessoriesCatalogueResponse.variants?.name
                    serviceName = getAccessoriesCatalogueResponse.variants?.name
                    skuId = getAccessoriesCatalogueResponse.variants?.skuCode
                    addedItemsList.add(this)
                }
            }
        }
        (binding.rvAddedItems.adapter as? AddedItemsAdapter)?.notifyItemInserted(addedItemsList.size)
    }

    @SuppressLint("SetTextI18n")
    fun updateTotalPrice() {
        if (addedItemsList.isNotEmpty()) {
            binding.rlTotalCost.makeVisible(true)
            if (addedItemsList.size == 1) binding.tvItemsCount.text =
                "${addedItemsList.size} ${getString(R.string.item)}"
            else binding.tvItemsCount.text =
                "${addedItemsList.size} ${getString(R.string.items_sc)}"
            var totalCost = 0.0
            addedItemsList.forEach {
                totalCost += it.total ?: 0.0
            }
            totalAmount = totalCost
            binding.tvTotalCost.text = "${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    getString(R.string.currency_symbol_default))
            }${String.format("%.2f", totalCost)}/-"
        } else binding.rlTotalCost.makeVisible(false)
    }

    private fun addServiceByOwn() = lifecycleScope.launchWhenStarted {
        showLoader()
        val isAdded = if (isTwoWheeler) viewModel.addTwoWheelerServiceByOwn(searchText)
        else viewModel.addAccessoriesPartByOwn(searchText)
        hideLoader()
        if (isAdded) {
            viewModel.getAccessoriesCatalogue(searchText, isTwoWheeler)
        } else CommonUtils.showToast(requireContext(),
            resources.getString(R.string.failed_to_add_part))
    }

}