package whitelisted.garage.view.fragments.payment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import kotlinx.android.synthetic.main.fragment_add_payment.view.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.AddPaymentRequest
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.databinding.FragmentAddPaymentBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import kotlin.math.ceil

class AddPaymentFragment(val actionListener: ActionListener?) : BaseBottomSheetFragment(),
    TextWatcher {

    private val binding by viewBinding(FragmentAddPaymentBinding::inflate)
    private lateinit var paymentModel: AddPaymentRequest
    private var totalAmount: Double = 0.0
    private var dueAmount: String? = ""
    private var isCredit: Boolean? = false
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
        isCancelable = true
    }

    private fun setupScreen() {
        paymentModel =
            arguments?.getParcelable(AppENUM.IntentKeysENUM.PAYMENT_MODEL) ?: AddPaymentRequest()
        isCredit = arguments?.getBoolean(AppENUM.IS_CREDIT, false)
        if (paymentModel.orderId == "") paymentModel.orderId = OrderDetailWrapperFragment.orderId

        dueAmount = arguments?.getString(AppENUM.DUE_AMOUNT, "")
        val dueAmt = "${getString(R.string.total_due_amount_colon)} ${
            dashboardViewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))
        }${
            String.format("%.2f", dueAmount?.toDouble() ?: 0.0)
        }"
        binding.tvBalanceAmount.text = dueAmt

        setPaymentTypesAdapter()
        binding.btnCancel.setOnClickListener(this)
        binding.btnSave.setOnClickListener(this)
        binding.tilAmount.editText?.addTextChangedListener(this)
        binding.tilDiscount.editText?.addTextChangedListener(this)
        binding.etAmount.setOnClickListener(this)
        binding.etFlatDiscount.setOnClickListener(this)

        if (isCredit == true) {
            binding.tilPaymentType.visibility = View.GONE
            binding.lblAddPayment.text = getString(R.string.credit_payment)
        }

    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnCancel -> popBackStack()

            R.id.btnSave -> savePaymentDetails()

            R.id.etAmount -> with(binding) {
                if (etAmount.text.toString().trim().isNotEmpty()) {
                    if (etAmount.text.toString().trim().toDouble() == 0.0) {
                        etAmount.setText("")
                    }
                }
            }
            R.id.etFlatDiscount -> with(binding) {
                if (etFlatDiscount.text.toString().trim().isNotEmpty()) {
                    if (etFlatDiscount.text.toString().trim().toDouble() == 0.0) {
                        etFlatDiscount.setText("")
                    }
                }
            }
        }
    }

    private fun savePaymentDetails() = with(binding) {
        val discount: Double
        if (formCheck()) {
            val amount = etAmount.text.toString().toDouble()
            discount = etFlatDiscount.text.toString().toDouble()
            val type = tilPaymentType.editText?.text.toString()

            //            if (amount > discount) {
            paymentModel.amount = amount
            paymentModel.transactionMode = type
            paymentModel.discountValue = discount
            if (isCredit == true) {
                paymentModel.transactionType = AppENUM.RefactoredStrings.DUE
                paymentModel.transactionMode = AppENUM.EnglishStrings.Credit
            }
            actionListener?.onActionItem(paymentModel)
            dismiss() //            } else {
            //                CommonUtils.showToast(
            //                    requireContext(),
            //                    getString(R.string.error_discount_more_than_amount),
            //                    true
            //                )
            //            }
        }
    }

    private fun formCheck(): Boolean {
        var flag = true
        dashboardViewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            AppENUM.RefactoredStrings.defaultCurrencySymbol).let {
            if (binding.etAmount.text.toString()
                    .contains(it)) binding.etAmount.setText(binding.etAmount.text.toString()
                .replace(it, ""))
        }
        if (binding.etAmount.text?.isEmpty() == true) {
            flag = false
            CommonUtils.showToast(requireContext(), getString(R.string.error_enter_amount), true)
        }
        if (isCredit != true) {
            if (binding.tilPaymentType.editText?.text?.isEmpty() == true) {
                flag = false
                CommonUtils.showToast(requireContext(),
                    getString(R.string.error_select_payment_type),
                    true)
            }
        }
        if (binding.etFlatDiscount.text?.isEmpty() == true) {
            flag = false
            CommonUtils.showToast(requireContext(), getString(R.string.error_enter_discount), true)
        }
        if (totalAmount > ceil(dueAmount?.toDouble() ?: 0.0)) {
            flag = false
            CommonUtils.showToast(requireContext(),
                getString(R.string.error_amount_more_than_due),
                true)
        }
        return flag
    }

    private fun setPaymentTypesAdapter() {
        ArrayList<String>().apply {
            if (dashboardViewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                    AppENUM.RefactoredStrings.defaultCurrency) == AppENUM.RefactoredStrings.defaultCurrency) {
                add(getString(R.string.cash))
                add(getString(R.string.upi))
            } else {
                add(getString(R.string.cash))
                add(getString(R.string.card))
            }

            ArrayAdapter(requireContext(),
                R.layout.support_simple_spinner_dropdown_item,
                this).let {
                (binding.tilPaymentType.editText as? AutoCompleteTextView)?.setAdapter(it)
            }
        } //        (tilPaymentType.editText as? AutoCompleteTextView)?.setOnItemClickListener { adapterView, view, position, l ->
        //            if (position == 1) {
        //                tilPaymentType.visibility = View.GONE
        //                tilGSTINDisplay.visibility = View.GONE
        //            } else {
        //                tilTaxType.visibility = View.VISIBLE
        //                tilGSTINDisplay.visibility = View.VISIBLE
        //            }
        //        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
    }

    override fun afterTextChanged(p0: Editable?) = with(binding) {
        if (tilAmount.editText?.text.toString().isNotEmpty()) {
            if (tilAmount.editText?.text.toString().contains(".") && CommonUtils.countOccurrences(
                    tilAmount.editText?.text.toString(),
                    '.') == 2) {
                tilAmount.editText?.setText(p0.toString()
                    .subSequence(0, tilAmount.editText?.text?.length ?: 1 - 1))
                tilAmount.editText?.setSelection(tilAmount.editText?.text.toString().length)
            } else if (tilAmount.editText?.text?.length == 1 && tilAmount.editText?.text.toString()
                    .contains(".")) {
                tilAmount.editText?.setText("")
            }
        }
        if (tilDiscount.editText?.text.toString()
                .isNotEmpty() && tilDiscount.editText?.text.toString()
                .contains(".") && CommonUtils.countOccurrences(tilDiscount.editText?.text.toString(),
                '.') == 2) {
            tilDiscount.editText?.setText(p0.toString()
                .subSequence(0, tilDiscount.editText?.text?.length ?: 1 - 1))
            tilDiscount.editText?.setSelection(tilDiscount.editText?.text.toString().length)
        }
        updateTotalAmount()
    }

    private fun updateTotalAmount() = with(binding) {
        try {
            if (etAmount.text.toString().trim().isNotEmpty() && etFlatDiscount.text.toString()
                    .trim().isNotEmpty() && etAmount.text.toString()
                    .trim() != "." && etFlatDiscount.text.toString().trim() != ".") {
                totalAmount = etAmount.text.toString().toDouble()
                    .plus(etFlatDiscount.text.toString().toDouble())
                ("${getString(R.string.total_added_amount_colon)} ${
                    dashboardViewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default))
                }$totalAmount").apply {
                    tvTotalAddedAmount.text = this
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}