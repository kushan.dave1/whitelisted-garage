package whitelisted.garage.view.fragments.account

import android.content.ComponentName
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.core.app.ShareCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.BusinessCardDetails
import whitelisted.garage.api.response.BusinessCardTemplate
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentBusinessCardBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.captureView
import whitelisted.garage.utils.CommonUtils.linearLayoutManager
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.CommonUtils.setOnTextChangeListener
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.BusinessCardAdapter
import whitelisted.garage.viewmodels.BusinessCardViewModel
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

class BusinessCardFragment : BaseFragment(), View.OnClickListener, CommonUtils.OnTextChange {

    private val binding by dataBinding<FragmentBusinessCardBinding>(R.layout.fragment_business_card)
    private val viewModel: BusinessCardViewModel by viewModel()
    private val dashboardSharedViewModel: DashboardSharedViewModel by sharedViewModel()
    private val designations = mutableListOf("Owner")
    private var businessCardAdapter: BusinessCardAdapter? = null
    private lateinit var cardDetails: BusinessCardDetails
    private val snapHelper = PagerSnapHelper()
    private val snapHelper2 = PagerSnapHelper()
    private lateinit var lm: LinearLayoutManager
    private lateinit var lm2: LinearLayoutManager
    private val cardTobeEdit by lazy { arguments?.getParcelable<BusinessCardDetails>("card") }
    private lateinit var designationsPopupWindow: PopupWindow
    private var loading = 0
    private var pro = false


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()

        if (cardTobeEdit != null) {
            cardDetails = cardTobeEdit as BusinessCardDetails
            initForEditCard()
        } else {
            getTemplates(); getSavedCards()
            cardDetails = BusinessCardDetails()
        }
        checkIfNotPro()
    }

    private fun checkIfNotPro() {
        pro = viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS)
        binding.clFeatureLocked.makeVisible(!pro)
    }

    private fun initForEditCard() = with(binding) {
        svFBC.makeVisible(false)
        svFBC2.makeVisible(true)
        flFBC.makeVisible(true) //setupDesignationsSpinner()
        businessCardAdapter =
            BusinessCardAdapter(listOf(BusinessCardTemplate().apply { data = cardDetails }))
        etNameFBC.setText(cardDetails.name)
        etAddressFBC.setText(cardDetails.address)
        etMobileNumberFBC.setText(cardDetails.mobile)
        etBusinessNameFBC.setText(cardDetails.businessName)
        etEmailAddressFBC.setText(cardDetails.email)
        tvDesignationFBC.text = cardDetails.designation
        vpFBC.adapter = businessCardAdapter
    }

    private fun prefill() {

        val name = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.OWNER_NAME, "")
        val address = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_ADDRESS, "")
        val number = viewModel.getStringSharedPreference(AppENUM.MOBILE_NUMBER, "")
        with(binding) {
            etNameFBC.setText(name)
            etAddressFBC.setText(address)
            etMobileNumberFBC.setText(number)
        }
        val userRole = viewModel.getStringSharedPreference(AppENUM.USER_ROLE)
        binding.tvDesignationFBC.text = userRole
    }

    private fun initListeners() = with(binding) {
        setupViewPagers()
        setupClickListeners()
        svFBC.makeVisible(true)
        svFBC2.makeVisible(false)
        flFBC.makeVisible(false)
        etNameFBC.setOnTextChangeListener(this@BusinessCardFragment)
        etBusinessNameFBC.setOnTextChangeListener(this@BusinessCardFragment)
        etMobileNumberFBC.setOnTextChangeListener(this@BusinessCardFragment)
        etEmailAddressFBC.setOnTextChangeListener(this@BusinessCardFragment)
        etAddressFBC.setOnTextChangeListener(this@BusinessCardFragment)
        tvDesignationFBC.setOnTextChangeListener(this@BusinessCardFragment)
        svFBC.setOnRefreshListener {
            getTemplates(); getSavedCards()
        }
    }

    private fun setupViewPagers() {
        lm = linearLayoutManager(requireContext(), 1.125f)
        lm2 = linearLayoutManager(requireContext(), 1.125f)
        binding.vp2FBC.layoutManager = lm2
        binding.vpFBC.layoutManager = lm
        snapHelper.attachToRecyclerView(binding.vpFBC)
        snapHelper2.attachToRecyclerView(binding.vp2FBC)
    }

    private fun getSavedCards(): Job = viewLifecycleOwner.lifecycleScope.launch {
        loading++
        if (!binding.svFBC.isRefreshing) showLoader()
        val savedCards = viewModel.getSavedCards()
        hideLoader()
        if (savedCards.isNotEmpty()) {
            binding.btnEdit.makeVisible(true); binding.btnShare.makeVisible(true)
            binding.btnDelete.makeVisible(true)
            binding.vp2FBC.adapter = BusinessCardAdapter(savedCards.toTemplates()).apply {
                data =
                    BusinessCardDetails(
                        name = viewModel.getStringSharedPreference(
                            AppENUM.UserKeySaveENUM.OWNER_NAME,
                            ""
                        ),
                        address = viewModel.getStringSharedPreference(
                            AppENUM.UserKeySaveENUM.SHOP_ADDRESS,
                            ""
                        ),
                        mobile = viewModel.getStringSharedPreference(AppENUM.MOBILE_NUMBER, "")
                    )
            }
            binding.tvATFBC.text = CommonUtils.getString(requireContext(), R.string.saved_cards)
        } else {
            binding.btnEdit.makeVisible(false); binding.btnShare.makeVisible(false)
            binding.btnDelete.makeVisible(false)
            binding.vp2FBC.adapter = businessCardAdapter
            binding.tvATFBC.text =
                CommonUtils.getString(requireContext(), R.string.available_templates)
        }
        if (--loading == 0) binding.svFBC.isRefreshing = false
    }

    private fun shareCard(v: View) = viewLifecycleOwner.lifecycleScope.launch {
        delay(100)
        captureView(v) {
            val uri = saveMediaToStorage(it) ?: return@captureView
            shareWithImage(false, uri)
        }
    }

    private fun editBusinessCard(v: View) {
        val cardDetails =
            (binding.vp2FBC.adapter as BusinessCardAdapter).cards[lm2.getPosition(v)].data ?: return
        val bundle = Bundle().apply { putParcelable("card", cardDetails) }
        CommonUtils.addFragmentUtil(
            requireActivity(),
            FragmentFactory.fragment(FragmentFactory.Fragments.FRAGMENT_BUSINESS_CARD, bundle)
        )
    }

    private fun deleteCard(v: View): Job = viewLifecycleOwner.lifecycleScope.launch {
        val cardId =
            (binding.vp2FBC.adapter as BusinessCardAdapter).cards[lm2.getPosition(v)].data?.id
                ?: return@launch
        showLoader()
        if (viewModel.deleteCard(cardId)) {
            CommonUtils.showToast(requireContext(), "Card deleted Successfully")
            getSavedCards()
        } else {
            CommonUtils.showToast(requireContext(), "Failed to delete card")
            hideLoader()
        }

    }

    private fun List<BusinessCardDetails>.toTemplates() = map { BusinessCardTemplate(data = it) }

    private fun setupClickListeners() = with(binding) {
        btnCNFBC.setOnClickListener(this@BusinessCardFragment)
        imgBack.setOnClickListener(this@BusinessCardFragment)
        btnConfirmFIBU.setOnClickListener(this@BusinessCardFragment)
        btnShareCardFBC.setOnClickListener(this@BusinessCardFragment)
        btnEdit.setOnClickListener(this@BusinessCardFragment)
        btnShare.setOnClickListener(this@BusinessCardFragment)
        btnDelete.setOnClickListener(this@BusinessCardFragment) //tvDesignationFBC.setOnClickListener(this@BusinessCardFragment)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnCNFBC -> {
                if (pro) gotoBusinessCardScreen()
                else {
                    fireFeatureLockedEvent()
                    requireActivity().let {
                        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT, Bundle().apply {
                            putString(AppENUM.PRO_LOCKED_ALERT_MSG,  dashboardSharedViewModel.screensConfigResponse.value?.membershipConfig?.businessCardMsg)
                        })
                            ?.let { baseFragment ->
                                baseFragment.show(
                                    it.supportFragmentManager,
                                    baseFragment.javaClass.name
                                )
                            }
                    }
                }
            }
            R.id.imgBack -> handleBackPress()
            R.id.tvDesignationFBC -> designationsPopupWindow.showAsDropDown(v)
            R.id.btnConfirmFIBU -> snapHelper.findSnapView(lm)?.let { saveBusinessCard(it) }
            R.id.btnShareCardFBC -> snapHelper.findSnapView(lm)?.let { shareCard(it) }
            R.id.btn_share -> snapHelper2.findSnapView(lm2)?.let { shareCard(it) }
            R.id.btn_delete -> snapHelper2.findSnapView(lm2)?.let { deleteCard(it) }
            R.id.btn_edit -> snapHelper2.findSnapView(lm2)?.let { editBusinessCard(it) }
        }
    }

    private fun saveBusinessCard(v: View) = viewLifecycleOwner.lifecycleScope.launch {
        try {
            val position = lm.getPosition(v)
            if (!validateDetails() || businessCardAdapter == null) {
                CommonUtils.showToast(
                    requireContext(),
                    resources.getString(R.string.please_enter_mandatory_fields)
                )
                return@launch
            }
            val details = cardDetails.apply {
                businessCardAdapter?.let {
                    val card = it.cards[position]
                    image = card.image ?: card.data?.image
                    textColor = card.textColor ?: card.data?.textColor
                    businessCardIcons = card.businessCardIcons ?: card.data?.businessCardIcons
                }
            }
            showLoader()
            val saveCard = viewModel.saveCard(details)
            hideLoader()
            if (saveCard) {
                CommonUtils.showToast(requireContext(), getString(R.string.card_saved), true)
                getSavedCards()
                handleBackPress()
                CommonUtils.showReviewDialog(requireActivity())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun validateDetails() =
        cardDetails.name.isNotEmpty() && cardDetails.businessName.isNotEmpty() && cardDetails.mobile.isNotEmpty() && cardDetails.address.isNotEmpty()


    private fun gotoBusinessCardScreen() = with(binding) {
        svFBC.makeVisible(false)
        svFBC2.makeVisible(true)
        flFBC.makeVisible(true) //setupDesignationsSpinner()
    }

    private fun getTemplates() = viewLifecycleOwner.lifecycleScope.launch {
        loading++
        if (!binding.svFBC.isRefreshing) showLoader()
        val templates = viewModel.getAvailableTemplates()
        hideLoader()
        if (binding.vp2FBC.adapter == null) binding.vp2FBC.adapter = BusinessCardAdapter(templates)
        businessCardAdapter = BusinessCardAdapter(mutableListOf<BusinessCardTemplate>().apply {
            addAll(templates)
        })
        binding.vpFBC.adapter = businessCardAdapter
        prefill()
        if (--loading == 0) binding.svFBC.isRefreshing = false
    }

    override fun handleBackPress(): Boolean {
        if (binding.svFBC2.visibility == View.VISIBLE && cardTobeEdit == null) {
            binding.svFBC.makeVisible(true)
            binding.svFBC2.makeVisible(false)
            binding.flFBC.makeVisible(false)
        } else popBackStackAndHideKeyboard()
        return true
    }

    override fun onTextChange(v: View, text: String) {
        when (v.id) {
            R.id.etNameFBC -> businessCardAdapter?.data = cardDetails.apply { name = text }
            R.id.etEmailAddressFBC -> businessCardAdapter?.data = cardDetails.apply { email = text }
            R.id.etAddressFBC -> businessCardAdapter?.data = cardDetails.apply { address = text }
            R.id.etMobileNumberFBC -> businessCardAdapter?.data =
                cardDetails.apply { mobile = text }
            R.id.tvDesignationFBC -> businessCardAdapter?.data =
                cardDetails.apply { designation = text }
            R.id.etBusinessNameFBC -> businessCardAdapter?.data =
                cardDetails.apply { businessName = text }
        }
    }

    private fun saveMediaToStorage(bitmap: Bitmap?): Uri? {
        val filename = "${System.currentTimeMillis()}.jpg"
        var fos: OutputStream? = null
        var imageUri: Uri? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            requireActivity().contentResolver?.also { resolver ->
                val contentValues = ContentValues().apply {
                    put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/*")
                    put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
                }
                imageUri =
                    resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                fos = imageUri?.let { resolver.openOutputStream(it) }
            }
        } else {
            try {
                val imagesDir =
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                val image = File(imagesDir, filename)
                imageUri = Uri.fromFile(image)
                fos = FileOutputStream(image)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        try {
            fos?.use {
                bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, it)
            } ?: kotlin.run {
                return null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return imageUri
    }

    private fun setupDesignationsSpinner() = lifecycleScope.launchWhenStarted {
        delay(100)
        val inflater =
            requireContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_spinner, null)
        val recyclerView = view.findViewById<RecyclerView>(R.id.rvWorkDone)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        val adapter =
            CommonUtils.spinnerRecyclerAdapter(designations, R.layout.item_text_view2) { pos ->
                onTextChange(binding.tvDesignationFBC, designations[pos])
                designationsPopupWindow.dismiss()
            }
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
        designationsPopupWindow = PopupWindow(
            view,
            binding.tvDesignationFBC.width,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            true
        ).apply {
            isOutsideTouchable = true
            isFocusable = true
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        binding.tvDesignationFBC.text = designations[0]
        onTextChange(binding.tvDesignationFBC, designations[0])
    }

    private fun shareWithImage(whatsAppShareClick: Boolean, uri: Uri) {
        try {
            if (whatsAppShareClick) Intent().apply {
                component = ComponentName("com.whatsapp", "com.whatsapp.ContactPicker")
                requireActivity().grantUriPermission(
                    "com.whatsapp",
                    uri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
                )
                type = "image/*"
                startActivity(this)
            } else {
                try {
                    val shareIntent =
                        ShareCompat.IntentBuilder.from(requireActivity()).setType("text/plain")
                            .setStream(uri).createChooserIntent().apply {
                                addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
                            }
                    startActivity(shareIntent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } catch (se: Exception) {
            se.printStackTrace()
        }
    }


    private fun fireFeatureLockedEvent() {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_BUSINESS_CARD)
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.ERROR_HEADER_PRO,
            bundle)

    }


}