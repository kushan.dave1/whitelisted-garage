package whitelisted.garage.view.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.setFragmentResult
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.AddressData
import whitelisted.garage.api.response.AddressResponseModel
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.databinding.FragmentAddEditAddressBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.viewmodels.AddressSharedViewModel
import whitelisted.garage.viewmodels.SelectAddressViewModel

class AddEditAddressFragment : BaseBottomSheetFragment(), TextWatcher {

    private val binding by viewBinding(FragmentAddEditAddressBinding::inflate)
    private lateinit var addressData: AddressData
    private lateinit var addressType: String
    private var isEdit = false
    private var addressIdForEdit = ""
    private var addressTypeForEdit = ""
    private val viewModel: SelectAddressViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isCancelable = true
        setupScreen()
    }

    private fun setupScreen() {
        setClickListeners()

        otherAddressTypeTextWatcher()
        setAddAddressObserver()
        arguments?.let {
            addressData = it.getParcelable(AppENUM.ADDRESS_POST) ?: AddressData()
            isEdit = it.getBoolean(AppENUM.IntentKeysENUM.IS_EDIT, false)
            addressIdForEdit = it.getString(AppENUM.ADDRESS_ID, "") ?: ""
            addressTypeForEdit = it.getString(AppENUM.ADDRESS_TYPE, "") ?: ""
        }

        addressData.latitude?.let {
            setData()
        }
        etHomeTextWatcher()
    }

    private fun etHomeTextWatcher() {
        binding.etHome.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                binding.tvLocation.text = p0.toString()
            }
        })
    }

    private fun otherAddressTypeTextWatcher() {
        binding.etOtherAddressType.doOnTextChanged { text, _, _, _ ->
            addressType = text.toString().trim()
            shouldEnableButton()
        }
        binding.etHome.addTextChangedListener(this)
        binding.etApartment.addTextChangedListener(this)
        binding.etState.addTextChangedListener(this)
        binding.etCity.addTextChangedListener(this)
        binding.etPincode.addTextChangedListener(this)
    }

    private fun setAddAddressObserver() {
        viewModel.provideAddAddressResponse().observe(viewLifecycleOwner) {
            binding.loaderFAEA.llMaterialLoader.makeVisible(false)
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    setFragmentResult(AppENUM.ADD_ADDRESS, Bundle().apply {
                        putBoolean(AppENUM.ADD_ADDRESS, true)
                    })
                    dismiss()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setData() {
        binding.etHome.setText(addressData.fullAddress)
        ("${
            CommonUtils.getCityFromLocation(requireContext(),
                addressData.latitude ?: 0.0,
                addressData.longitude ?: 0.0)
        }, ${
            CommonUtils.getStateFromLocation(requireContext(),
                addressData.latitude ?: 0.0,
                addressData.longitude ?: 0.0)
        }, ${
            CommonUtils.getCountryFromLocation(requireContext(),
                addressData.latitude ?: 0.0,
                addressData.longitude ?: 0.0)
        }").apply { binding.etApartment.setText(this) }
        binding.etState.setText( CommonUtils.getStateFromLocation(requireContext(),
            addressData.latitude ?: 0.0,
            addressData.longitude ?: 0.0))
        binding.etCity.setText( CommonUtils.getCityFromLocation(requireContext(),
            addressData.latitude ?: 0.0,
            addressData.longitude ?: 0.0))
        binding.etPincode.setText( CommonUtils.getPinCodeFromLocation(requireContext(),
            addressData.latitude ?: 0.0,
            addressData.longitude ?: 0.0))
        binding.tvLocationName.text = CommonUtils.getCityFromLocation(requireContext(),
            addressData.latitude ?: 0.0,
            addressData.longitude ?: 0.0)
        binding.tvLocation.text = addressData.fullAddress
        if (isEdit) {
            setAddressType()
            binding.btnAddFAAA.text = getString(R.string.edit)
        }

        shouldEnableButton()
    }

    private fun setAddressType() {
        when (addressTypeForEdit) {
            getString(R.string.home) -> {
                changeAddressType(R.id.tvHomeType)
            }
            getString(R.string.office) -> {
                changeAddressType(R.id.tvOfficeType)
            }
            else -> {
                changeAddressType(R.id.tvOtherType)
                addressType = addressTypeForEdit
                binding.etOtherAddressType.setText(addressType)
            }
        }
    }

    private fun setClickListeners() {
        binding.tvChange.setOnClickListener(this)
        binding.btnAddFAAA.setOnClickListener(this)
        binding.etApartment.setOnClickListener(this)
        binding.tvHomeType.setOnClickListener(this)
        binding.tvOfficeType.setOnClickListener(this)
        binding.tvOtherType.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.tvChange -> {
                dismiss()
                Bundle().apply {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.ADDRESS_FRAGMENT, this))
                }
            }
            R.id.btnAddFAAA -> {
                if (isEdit) {
                    binding.loaderFAEA.llMaterialLoader.makeVisible(true)
                    AddressResponseModel().apply {
                        if (binding.switchDefAdd.isChecked) isDefault = true
                        addressId = addressIdForEdit
                        address = binding.etHome.text.toString()
                        latitude = addressData.latitude
                        longitude = addressData.longitude
                        city = CommonUtils.getCityFromLocation(requireContext(),
                            addressData.latitude ?: 0.0,
                            addressData.longitude ?: 0.0)
                        state = CommonUtils.getStateFromLocation(requireContext(),
                            addressData.latitude ?: 0.0,
                            addressData.longitude ?: 0.0)
                        pin = CommonUtils.getPinCodeFromLocation(requireContext(),
                            addressData.latitude ?: 0.0,
                            addressData.longitude ?: 0.0)
                        address_type = addressType
                        viewModel.editAddressAPI(this)
                    }
                } else {
                    binding.loaderFAEA.llMaterialLoader.makeVisible(true)
                    AddressResponseModel().apply {
                        if (binding.switchDefAdd.isChecked) isDefault = true
                        address = binding.etHome.text.toString()
                        latitude = addressData.latitude
                        longitude = addressData.longitude
                        city = CommonUtils.getCityFromLocation(requireContext(),
                            addressData.latitude ?: 0.0,
                            addressData.longitude ?: 0.0)
                        state = CommonUtils.getStateFromLocation(requireContext(),
                            addressData.latitude ?: 0.0,
                            addressData.longitude ?: 0.0)
                        pin = CommonUtils.getPinCodeFromLocation(requireContext(),
                            addressData.latitude ?: 0.0,
                            addressData.longitude ?: 0.0)
                        address_type = addressType
                        viewModel.addAddressAPI(this)
                    }
                }
            }
            R.id.etApartment -> {

            }
            R.id.tvHomeType, R.id.tvOfficeType, R.id.tvOtherType -> {
                changeAddressType(v.id)
            }
        }
    }

    private fun changeAddressType(id: Int) = with(binding) {
        when (id) {
            R.id.tvHomeType -> {
                tvHomeType.isSelected = true
                tvOfficeType.isSelected = false
                tvOtherType.isSelected = false
                addressType = getString(R.string.home)
                etOtherAddressType.makeVisible(false)
            }
            R.id.tvOfficeType -> {
                tvHomeType.isSelected = false
                tvOfficeType.isSelected = true
                tvOtherType.isSelected = false
                addressType = getString(R.string.office)
                etOtherAddressType.makeVisible(false)
            }
            R.id.tvOtherType -> {
                tvHomeType.isSelected = false
                tvOfficeType.isSelected = false
                tvOtherType.isSelected = true
                addressType = ""
                etOtherAddressType.makeVisible(true)
            }
        }
        shouldEnableButton()
    }

    private fun shouldEnableButton() {
        binding.btnAddFAAA.isEnabled = binding.etHome.text.toString().trim()
            .isNotEmpty() && binding.etApartment.text.toString().trim()
            .isNotEmpty() && binding.etState.text.toString().trim()
            .isNotEmpty() && binding.etCity.text.toString().trim()
            .isNotEmpty() && binding.etPincode.text.toString().trim()
            .isNotEmpty() && ::addressType.isInitialized && addressType.isNotEmpty()
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun afterTextChanged(p0: Editable?) {
        shouldEnableButton()
    }

}