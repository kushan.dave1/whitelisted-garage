package whitelisted.garage.view.fragments.orderEstimate

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.delay
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.data.SetOptionsImgStatusObject
import whitelisted.garage.api.request.*
import whitelisted.garage.api.response.AllPackagesResponseModel
import whitelisted.garage.api.response.MostUsedPartsResponseModel
import whitelisted.garage.api.response.ServiceResponseModel
import whitelisted.garage.api.response.WorkDoneResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentOrderEstimatesBinding
import whitelisted.garage.eventBus.*
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.*
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment
import whitelisted.garage.viewmodels.OrderEstimatesViewModel
import whitelisted.garage.viewmodels.PaymentsSharedViewModel
import java.util.*

class OrderEstimatesFragment : BaseFragment(), TextWatcher {

    private val binding by viewBinding(FragmentOrderEstimatesBinding::inflate)
    private lateinit var packagesList: MutableList<AllPackagesResponseModel>
    private lateinit var packagesAdapter: PackagesAdapter
    private lateinit var addedPackageList: MutableList<Packages>
    private lateinit var addedOEPackagesAdapter: AddedOEPackagesAdapter
    private lateinit var partsList: MutableList<ServiceResponseModel>
    private lateinit var partsAdapter: PartsAdapter
    private lateinit var selectedPart: ServiceResponseModel
    private lateinit var workDoneList: MutableList<WorkDoneResponseModel>
    private lateinit var addedPartList: MutableList<OEServicesItem>
    private lateinit var addedPartsAdapter: OrderEstimatePartsAdapter
    private lateinit var selectedService: OEServicesItem
    private lateinit var selectedPackage: Packages
    private var totalEstimatesAmount: Double = 0.0
    private var totalDiscount: Double = 0.0
    private lateinit var addedPartListDuplicate: MutableList<OEServicesItem>
    private var dontSearch = false
    private val viewModel: OrderEstimatesViewModel by viewModel()
    private val paymentViewModel: PaymentsSharedViewModel by sharedViewModel()
    private lateinit var inventoryItemsList: MutableList<RackItem>
    private var workDoneForPosition = -1
    private var workDonePopup: PopupWindow? = null
    private var isAcc = false
    private var searchText = ""
    private val shopType by lazy {
        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
    }
    private var isTwoWheeler: Boolean = false
        get() = OrderDetailWrapperFragment.provideOrderDetails()?.carDetails?.isTwoWheeler == 2

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupScreen()
    }

    private fun setupScreen() = binding.apply {
        setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_ESTIMATE,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ESTIMATE)
        showLoader()
        setClickListeners()
        setObservers()

        if (shopType == AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT) {
            isAcc = true
            lblPartsServices.text =
                getString(R.string.parts_and_inventory_items) //            viewModel.getCustomPackagesAPI(isTwoWheeler)
            rlAddInventoryItems.makeVisible(false)

            lblMostUsedParts.text = getString(R.string.most_used_items)
        } else {
            isAcc = false //            viewModel.getPackagesAndWorkDoneAPI(isTwoWheeler)
            rlAddInventoryItems.makeVisible(true)

            lblMostUsedParts.text = getString(R.string.most_used_services)
        }

        setAddedPartsRVAdapter()

        setFilter()
    }

    private fun setFilter() {
        val filter = InputFilter { source, _, _, _, _, _ ->
            if (source != null && AppENUM.blockCharacterSet.contains("" + source)) {
                ""
            } else null
        }
        binding.etSearchPart.filters = arrayOf(filter)
    }

    private fun setAddedPartsRVAdapter() {
        addedPartList = mutableListOf()
        addedPartsAdapter =
            OrderEstimatePartsAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol),
                isAcc,
                requireContext(),
                addedPartList,
                this)
        binding.rvAddedParts.adapter = addedPartsAdapter
    }

    private fun setObservers() {
        observePartsListResponse()
        observePackagesListResponse()
        observeServicesOfPackageResponse()
        observeWorkDoneList()
        observeUpdateOrderEstimatesResponse()
        observeGetOrderEstimatesResponse()
        observeCustomPackagesResponse()
        observeAddValueResponse()
        observeMostUsedPartsResponse()
    }

    private fun observeMostUsedPartsResponse() {
        viewModel.provideMostUsedPartsRetailerCatalogueResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        setMostUsedPartsAdapter(it.body.data)
                        if (addedPartList.isEmpty()) {
                            binding.rlMostUsedParts.makeVisible(true)
                        }
                    }
                }
                is Result.Failure -> {
                }
            }
        }

        viewModel.provideMostUsedPartsWorkshopCatalogueResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        setMostUsedPartsAdapter(it.body.data)
                        if (addedPartList.isEmpty()) {
                            binding.rlMostUsedParts.makeVisible(true)
                        }
                    }
                }
                is Result.Failure -> {
                }
            }
        }
    }

    private fun setMostUsedPartsAdapter(data: MutableList<MostUsedPartsResponseModel>) =
        binding.apply {
            rvMostUsedParts.layoutManager = LinearLayoutManager(requireContext())
            rvMostUsedParts.adapter = MostUsedPartsAdapter(isAcc, this@OrderEstimatesFragment)
            (rvMostUsedParts.adapter as MostUsedPartsAdapter).setData(data)
        }

    private fun observeAddValueResponse() {
        viewModel.provideUpdateInventoryPriceResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    if (::addedPartListDuplicate.isInitialized) {
                        if (addedPartListDuplicate.size > 0) addedPartListDuplicate.removeAt(0)
                        if (addedPartListDuplicate.size > 0) {
                            updateAddedItemsValue()
                        } else hideLoader()
                    }
                }
                is Result.Failure -> {
                    hideLoader() //                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeCustomPackagesResponse() {
        viewModel.provideCustomPackagesListResponse().observe(viewLifecycleOwner) { result ->
            hideLoader()
            when (result) {
                is Result.Success -> {
                    if (::packagesList.isInitialized) packagesList.clear()
                    result.body.data.forEach {
                        if (it.total != null && it.total > 0) {
                            if (::packagesList.isInitialized) packagesList.add(it)
                            else {
                                packagesList = mutableListOf()
                                packagesList.add(it)
                            }
                        }
                    }

                    if (::packagesList.isInitialized && packagesList.size > 0) {
                        setPackagesAdapter()
                        binding.llNoPackage.makeVisible(false)
                    } else { //                        llNoPackage.makeVisible(true)
                        binding.lblPackages.setCompoundDrawablesWithIntrinsicBounds(null,
                            null,
                            ContextCompat.getDrawable(requireContext(), R.drawable.ic_add_black),
                            null)
                    }
                    showLoader()
                    viewModel.getOrderEstimatesAPI(OrderDetailWrapperFragment.orderId)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), result.errorMessage, true)
                }
            }
        }
    }

    private fun observeGetOrderEstimatesResponse() {
        viewModel.provideGetOrderEstimatesResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        if (isVisible) {
                            binding.rlMostUsedParts.makeVisible(false)
                            setData(it.body.data[0])
                        }
                    } else {
                        EventBus.getDefault().post(SetOptionsImageStatusEvent("0",
                            SetOptionsImgStatusObject(tabPosition = 1)))

                        showLoader()
                        if (isAcc) {
                            viewModel.getMostUsedPartsRetailerCatalogue(isTwoWheeler)
                        } else {
                            viewModel.getMostUsedPartsWorkshopCatalogue(isTwoWheeler)
                        }
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setData(data: OrderEstimateRequestResponse) {
        if (data.packages?.size.toString().toInt() > 0) {
            binding.lblPackages.background =
                ContextCompat.getDrawable(requireActivity(), R.drawable.bg_order_detail_heading)
            var count = 0
            if (::addedPackageList.isInitialized) {
                addedOEPackagesAdapter.dataList.clear()
                addedOEPackagesAdapter.notifyDataSetChanged()
            }
            data.packages?.forEach { _ ->
                selectPackage(count, data)
                count++
            }
        }

        if ((data.orderEstimateItems?.size ?: 0) > 0) binding.apply {
            data.orderEstimateItems?.forEach {
                if (it.taxRate == null) it.taxRate = "0%"
                if (it.taxable == null) it.taxable = 0.0
            }
            lblPartsServices.background =
                ContextCompat.getDrawable(requireActivity(), R.drawable.bg_order_detail_heading)
            if (!::addedPartList.isInitialized) {
                addedPartList = mutableListOf()
            }
            if (addedPartList.size > 0) {
                addedPartList.clear()
                addedPartsAdapter.notifyDataSetChanged()
            }
            addedPartList.addAll(data.orderEstimateItems ?: mutableListOf())
            if (!::addedPartsAdapter.isInitialized) addedPartsAdapter =
                OrderEstimatePartsAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    AppENUM.RefactoredStrings.defaultCurrencySymbol),
                    isAcc,
                    requireContext(),
                    addedPartList,
                    this@OrderEstimatesFragment)
            addedPartsAdapter.notifyDataSetChanged()

            rlSelectParts.makeVisible(false)
            rvAddedParts.makeVisible(true)
            rlChoose.makeVisible(true)
            btnAdd.makeVisible(false)
            etSearchPart.isEnabled = true
            etSearchPart.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(
                requireContext(),
                R.drawable.ic_search), null, null, null)
            etSearchPart.setText("")
            dontSearch = false

            paymentViewModel.provideOrderEstimateItemsResponse().value = data.orderEstimateItems
        }
        updateTotalPrice()

        if (isResumed) {
            if (data.pdfStatus?.status == true) {
                EventBus.getDefault().post(SetOptionsImageStatusEvent("0",
                    SetOptionsImgStatusObject(tabPosition = 1,
                        pdfFile = data.jobCardPdfStatus?.pdfFile ?: "",
                        pdfFile2 = data.pdfStatus?.pdfFile ?: "")))
            } else {
                EventBus.getDefault().post(SetOptionsImageStatusEvent("0",
                    SetOptionsImgStatusObject(tabPosition = 1)))
            }
        }
    }

    private fun observeUpdateOrderEstimatesResponse() {
        viewModel.provideUpdateOrderEstimatesResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    EventBus.getDefault().post(UpdateOrderStatusEvent(
                        AppENUM.RefactoredStrings.WIP_ORDER_STATUS.toString(),
                    ))

                    OrderDetailWrapperFragment.orderStatusId =
                        AppENUM.RefactoredStrings.WIP_ORDER_STATUS

                    //                    if (OrderDetailWrapperActivity.isNewOrder)
                    EventBus.getDefault().post(SelectTabEvent(
                        "2",
                    ))

                    if (binding.rvAddedParts.adapter != null) paymentViewModel.provideOrderEstimateItemsResponse().value =
                        (binding.rvAddedParts.adapter as OrderEstimatePartsAdapter).dataList

                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                    EventBus.getDefault()
                        .post(RefreshOrderSummaryEvent(OrderDetailWrapperFragment.orderId)) //                    showLoader()
                    //                    viewModel.getOrderEstimatesAPI(OrderDetailWrapperActivity.orderId)
                    updateAddedItemsValue()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun updateAddedItemsValue() {
        if (::addedPartListDuplicate.isInitialized && addedPartListDuplicate.size > 0) {
            showLoader()
            val req = UpdateInventoryItemRequest()
            req.selling_price = addedPartListDuplicate[0].pricePerQuantity
            req.buying_price = 0.0
            if (shopType == AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) {
                req.id = addedPartListDuplicate[0].id
            } else req.sku_id = addedPartListDuplicate[0].skuId
            viewModel.updatePriceForAddedParts(req)
        }
    }

    private fun observeWorkDoneList() {
        viewModel.provideWorkDoneListResponseFromAPI().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    workDoneList = it.body.data.toMutableList()
                    showLoader()
                    viewModel.getOrderEstimatesAPI(OrderDetailWrapperFragment.orderId)
                }
                is Result.Failure -> {

                }
            }
        }
    }

    private fun observeServicesOfPackageResponse() {
        viewModel.provideServicesOfPackageResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    val list: MutableList<String> = mutableListOf()
                    it.body.data.forEach { serviceItem ->
                        list.add(serviceItem.serviceName ?: "")
                    } //                    setPackageInclusivesAdapter(list)
                }

                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observePackagesListResponse() {
        viewModel.providePackagesListResponse().observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Success -> {
                    if (::packagesList.isInitialized) packagesList.clear()
                    result.body.data.forEach {
                        if (it.total != null && it.total > 0) {
                            if (::packagesList.isInitialized) packagesList.add(it)
                            else {
                                packagesList = mutableListOf(it)
                            }
                        }
                    }
                    if (::packagesList.isInitialized && packagesList.size > 0) {
                        setPackagesAdapter()
                        binding.llNoPackage.makeVisible(false)
                    } else { //                        llNoPackage.makeVisible(true)
                        binding.lblPackages.setCompoundDrawablesWithIntrinsicBounds(null,
                            null,
                            ContextCompat.getDrawable(requireContext(), R.drawable.ic_add_black),
                            null)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), result.errorMessage, true)
                }
            }
        }
    }

    private fun setPackagesAdapter() {
        binding.rvPackages.layoutManager = LinearLayoutManager(requireContext())
        packagesAdapter = PackagesAdapter(packagesList, this)
        binding.rvPackages.adapter = packagesAdapter
    }

    private fun observePartsListResponse() {
        viewModel.providePartsListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    binding.rvPartsService.makeVisible(true)
                    partsList = it.body.data as MutableList<ServiceResponseModel>
                    setPartsAdapter()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setClickListeners() = binding.apply {
        lblPackages.setOnClickListener(this@OrderEstimatesFragment) //        imgDelSelPackage.setOnClickListener(this@OrderEstimatesFragment)
        etSearchPart.addTextChangedListener(this@OrderEstimatesFragment)
        btnDelete.setOnClickListener(this@OrderEstimatesFragment)
        btnAdd.setOnClickListener(this@OrderEstimatesFragment)
        lblPartsServices.setOnClickListener(this@OrderEstimatesFragment)
        btnCancel.setOnClickListener(this@OrderEstimatesFragment)
        btnUpdate.setOnClickListener(this@OrderEstimatesFragment)
        btnInventoryItems.setOnClickListener(this@OrderEstimatesFragment)
        btnAddSelectedParts.setOnClickListener(this@OrderEstimatesFragment)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.lblPackages -> hideOrShowPackagesList()
            R.id.tvPackageName, R.id.imgDelSelPackage, R.id.clPart, R.id.tvWorkDone, R.id.imgEditPart, R.id.imgDelPart, R.id.tvTextView -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let {
                    CommonUtils.genericCastOrNull<String>(v.getTag(R.id.model))?.let { model ->
                        handleAdapterClick(model, it, v)
                    }
                }
            }
            R.id.btnDeleteCustomPart -> CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))
                ?.let {
                    deleteCustomPart(it)
                }
            R.id.btnDelete -> clearSearchET()
            R.id.btnAdd -> {
                if (::selectedService.isInitialized) {
                    selectedService.type = "part"
                    addSelectedPart()
                } else CommonUtils.showToast(requireContext(),
                    getString(R.string.error_select_work_done),
                    true)
            }
            R.id.btnCancel -> removeSelectPartLayout()

            R.id.lblPartsServices -> {
                if (isAcc) {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_ADD_PART,
                            Bundle().apply {
                                putBoolean(AppENUM.IS_INVENTORY, false)
                                putBoolean(AppENUM.IS_ACCESSORIES, true)
                                putBoolean(AppENUM.IS_JOB_CARD, false)
                                putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, isTwoWheeler)
                            }),
                        target = R.id.fragment_container_2)
                } else showSearchPartsLayout()
            }

            R.id.btnUpdate -> {
                if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_ADDRESS, "")
                        .isEmpty()) {
                    activity?.let {
                        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.COMPLETE_REGISTRATION,
                            null,
                            object : ActionListener {
                                @RequiresApi(Build.VERSION_CODES.P)
                                override fun onActionItem(extra: Any?, extra2: Any?) {
                                    if (extra as? Boolean? == true) {
                                        updateOrderEstimates()
                                    }
                                }
                            })?.let { baseFragment ->
                            baseFragment.show(it.supportFragmentManager,
                                baseFragment.javaClass.name)
                        }
                    }
                } else updateOrderEstimates()
            }

            R.id.btnInventoryItems -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_ADD_PART,
                        Bundle().apply {
                            putBoolean(AppENUM.IS_INVENTORY, false)
                            putBoolean(AppENUM.IS_ACCESSORIES, true)
                            putBoolean(AppENUM.IS_JOB_CARD, false)
                            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, isTwoWheeler)
                            putBoolean(AppENUM.SHOW_INVENTORY_LIST, true)
                            putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                        }))
            }

            R.id.btnAddSelectedParts -> {
                checkAndAddSelectedParts(true)
            }

            R.id.btnAddServiceByOwn -> addServicebyOwn()

            R.id.tvItemNameIRS -> {
                CommonUtils.genericCastOrNull<String>(v.getTag(R.id.model))?.let { model ->
                    binding.etSearchPart.setText(model)
                    binding.etSearchPart.setSelection(model.length)
                    binding.rvPartsService.adapter = null
                }
            }
        }
    }

    private fun deleteCustomPart(position: Int) =
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            val customPart = partsAdapter.getAdapterList()[position]
            showLoader()
            val isDeleted = if (isTwoWheeler) viewModel.deleteCustomBikePart(customPart.id)
            else viewModel.deleteCustomPart(customPart.id)
            hideLoader()
            if (isDeleted) {
                CommonUtils.showToast(requireContext(),
                    resources.getString(R.string.part_deleted_successfully))
                viewModel.getServicesAPI(searchText, isTwoWheeler)
            } else CommonUtils.showToast(requireContext(),
                resources.getString(R.string.unable_to_delete_part))

        }

    private fun addServicebyOwn() = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
        fireAddCustomServiceEvent()
        showLoader()
        val isAdded = if (isTwoWheeler) viewModel.addTwoWheelerServiceByOwn(searchText)
        else viewModel.addServiceByOwn(searchText)
        hideLoader()
        if (isAdded) {
            CommonUtils.showToast(requireContext(),
                resources.getString(R.string.service_added_successfully))
            viewModel.getServicesAPI(searchText, isTwoWheeler)

        } else CommonUtils.showToast(requireContext(),
            resources.getString(R.string.failed_to_add_service))

    }

    private fun handleAdapterClick(model: String, position: Int, v: View) = binding.apply {
        when (model) {
            AppENUM.AdaptersConstant.PART -> {
                dontSearch = true
                selectedPart = partsAdapter.getAdapterList()[position]
                etSearchPart.setText(partsAdapter.getAdapterList()[position].name)
                etSearchPart.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
                etSearchPart.isEnabled = false
                selectedService = OEServicesItem()
                selectedService.type = "part"
                selectedService.id = selectedPart.id.toString()
                selectedService.serviceName = selectedPart.name
                selectedService.description = selectedPart.description
                if (::workDoneList.isInitialized) {
                    selectedService.workDone = workDoneList[0].id?.toLong()
                    selectedService.workDoneName = workDoneList[0].name
                }
                selectedService.pricePerQuantity = selectedPart.selling_price
                selectedService.total =
                    selectedService.pricePerQuantity?.times(selectedService.quantity ?: 0)

                if (::inventoryItemsList.isInitialized) {
                    inventoryItemsList.forEach {
                        if (it.skuId == selectedService.id) {
                            selectedService.partAlreadyAdded = true
                            return@forEach
                        }
                    }
                }

                //                setWorkDoneAdapter()
                btnAdd.makeVisible(true)
                btnAdd.callOnClick()

                checkAndAddSelectedParts(false)
            }
            AppENUM.AdaptersConstant.SERVICE_TYPE -> {
                if (workDoneForPosition != -1) {
                    addedPartList[workDoneForPosition].workDone =
                        workDoneList[position].id?.toLong()
                    addedPartList[workDoneForPosition].workDoneName = workDoneList[position].name
                    addedPartsAdapter.notifyItemChanged(workDoneForPosition)
                    workDoneForPosition = -1
                }

                workDonePopup.let {
                    if (it?.isShowing == true) {
                        it.dismiss()
                    }
                    workDonePopup = null
                }

                checkAndAddSelectedParts(false)

            }
            AppENUM.AdaptersConstant.ADDED_PART -> {
                when (v.id) {
                    R.id.tvWorkDone -> {
                        if (::addedPartList.isInitialized && addedPartList.size > position) {
                            showWorkDoneWindow(addedPartList[position], position, v)
                        }
                    }
                    R.id.imgEditPart -> {
                        if (::addedPartList.isInitialized && addedPartList.size > position) {
                            activity?.let {
                                FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.ORDER_ESTIMATES_EDIT,
                                    Bundle().apply {
                                        putParcelable(AppENUM.ORDER_MODEL, addedPartList[position])
                                    },
                                    object : ActionListener {
                                        @RequiresApi(Build.VERSION_CODES.P)
                                        override fun onActionItem(extra: Any?, extra2: Any?) {
                                            addedPartList[position] = extra as OEServicesItem
                                            addedPartsAdapter.notifyItemChanged(position)
                                            updateTotalPrice()
                                        }
                                    })?.let { baseFragment ->
                                    baseFragment.show(it.supportFragmentManager,
                                        baseFragment.javaClass.name)
                                }
                            }
                        }
                    }
                    else -> {
                        if (::addedPartList.isInitialized && addedPartList.size > position) {
                            addedPartList.removeAt(position)
                            addedPartsAdapter.notifyItemRemoved(position)
                            if (addedPartList.isEmpty()) {
                                lblPartsServices.background = ContextCompat.getDrawable(
                                    requireActivity(),
                                    R.drawable.bg_white_12) //                            cvPartsServices.makeVisible(false)
                                rlMostUsedParts.makeVisible(true)
                                if (rvMostUsedParts.adapter != null) {
                                    (rvMostUsedParts.adapter as MostUsedPartsAdapter).apply {
                                        this.notifyItemRangeChanged(0, this.getAdapterData().size)
                                    }
                                } else {
                                    showLoader()
                                    if (isAcc) {
                                        viewModel.getMostUsedPartsRetailerCatalogue(isTwoWheeler)
                                    } else {
                                        viewModel.getMostUsedPartsWorkshopCatalogue(isTwoWheeler)
                                    }
                                }
                                llButtons.makeVisible(false)
                            }
                            updateTotalPrice()
                        }
                    }
                }
            }
            AppENUM.AdaptersConstant.PACKAGE_SELECTED -> {
                selectPackage(position, null)
            }
            AppENUM.AdaptersConstant.DEL_PACKAGE -> {
                if (::addedOEPackagesAdapter.isInitialized && addedOEPackagesAdapter.dataList.size > position) {
                    addedOEPackagesAdapter.dataList.removeAt(position)
                    addedOEPackagesAdapter.notifyItemRemoved(position)
                    if (addedOEPackagesAdapter.dataList.size == 0) {
                        rvAddedPackages.makeVisible(false)
                        lblPackages.background =
                            ContextCompat.getDrawable(requireActivity(), R.drawable.bg_white_12)
                    }
                    updateTotalPrice()
                }
            }
        }


    }

    private fun checkAndAddSelectedParts(isButtonClicked: Boolean) = binding.apply {
        var flag = false
        if (rvMostUsedParts.adapter != null) {
            (rvMostUsedParts.adapter as MostUsedPartsAdapter).getAdapterData().forEach {
                if (it.isSelected) {
                    flag = true
                    val item = OEServicesItem()
                    if (isAcc) {
                        item.skuId = it.skuId ?: "0"
                        item.serviceName = it.categoryName
                    } else {
                        item.id = it.id.toString()
                        item.serviceName = it.name
                        if (::workDoneList.isInitialized && workDoneList.size > 0) {
                            item.workDoneName = workDoneList[0].name
                            item.workDone = workDoneList[0].id?.toLong()
                        }
                    }
                    item.pricePerQuantity = it.sellingPrice
                    item.type = "part"
                    item.total = it.sellingPrice?.times(item.quantity ?: 0)
                    if (!::addedPartList.isInitialized) {
                        addedPartList = mutableListOf()
                    }
                    addedPartList.add(item)
                }
            }
            if (flag) {
                rlMostUsedParts.makeVisible(false)
                rlDiscountTotal.makeVisible(true)
                llButtons.makeVisible(true)
                (rvAddedParts.adapter as OrderEstimatePartsAdapter).notifyItemRangeInserted(0,
                    (rvAddedParts.adapter as OrderEstimatePartsAdapter).dataList.size)
                updateTotalPrice()

                (rvMostUsedParts.adapter as MostUsedPartsAdapter).getAdapterData().forEach {
                    it.isSelected = false
                }
            } else {
                if (isButtonClicked) CommonUtils.showToast(requireContext(),
                    getString(R.string.error_select_parts),
                    true)
            }
        }
    }

    private fun updateOrderEstimates() {
        if (formCheck()) {
            firebaseSaveEstimate()
            showLoader()
            val orderEstimateRequest = OrderEstimateRequestResponse()
            orderEstimateRequest.orderId = OrderDetailWrapperFragment.orderId
            if (::addedPackageList.isInitialized && addedPackageList.size > 0) {
                addedPackageList.forEach {
                    val packageItem = Packages()
                    if (shopType == AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) packageItem.packageId =
                        it.packageId
                    else packageItem.id = it.id
                    packageItem.packageName = it.packageName
                    packageItem.packageTotal = it.packageTotal
                        ?: 0.0 //                            packageItem.packageTotal = 1000.0
                    orderEstimateRequest.packages?.add(packageItem)
                }
            }

            if (::addedPartList.isInitialized && addedPartList.size > 0) {
                orderEstimateRequest.orderEstimateItems?.addAll(addedPartList)
                addedPartListDuplicate = addedPartList
            }
            orderEstimateRequest.billAmount = totalEstimatesAmount
            viewModel.updateOrderEstimatesAPI(orderEstimateRequest)
        } else CommonUtils.showToast(requireContext(),
            getString(R.string.alert_part_price),
            true) //                (activity as OrderDetailWrapperActivity).selectTab(3)
    }

    private fun firebaseSaveEstimate() {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ESTIMATE)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.DISCOUNT,
                binding.tvDiscount.text.toString())
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.TOTAL,
                binding.tvTotalCost.text.toString())


            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_SAVE_ESTIMATE,
                this)
        }
    }

    private fun formCheck(): Boolean {
        var flag = true
        if (::addedPackageList.isInitialized && addedPackageList.size > 0) {
            addedPackageList.forEach {
                if (it.packageTotal ?: 0.0 < 1.0) {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.alert_package_price),
                        true)
                    flag = false
                    return@forEach
                }
            }
        }
        if (::addedPartList.isInitialized && addedPartList.size > 0) {
            addedPartList.forEach {
                if (it.total ?: 0.0 < 1.0) { //                    CommonUtils.showToast(
                    //                        requireContext(),
                    //                        getString(R.string.alert_part_price),
                    //                        true
                    //                    )
                    flag = false
                    return@forEach
                }
            }
        }
        return flag
    }

    private fun removeSelectPartLayout() = binding.apply {
        rlSelectParts.makeVisible(false)
        rlChoose.makeVisible(true)
        etSearchPart.setText("")
        rvPartsService.makeVisible(false)
        etSearchPart.isEnabled = true
        etSearchPart.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_search), null, null, null)
    }

    private fun showSearchPartsLayout() {
        binding.rlChoose.makeVisible(false)
        binding.rlSelectParts.makeVisible(true)

        setRecentSearchAdapter()
    }

    private fun setRecentSearchAdapter() = binding.apply {
        lblRecentSearches.makeVisible(true)
        val recentList = viewModel.getStringSharedPreference(AppENUM.RECENT_SEARCHES_WORKSHOP, "")
        val recentSearches = recentList.split(",")
        if (recentSearches.isNotEmpty() && !recentSearches.contains("")) {
            (RecentSearchesAdapter(this@OrderEstimatesFragment)).apply {
                this.setData(recentSearches.toMutableList())
                rvPartsService.adapter = this
                rvPartsService.makeVisible(true)
            }
        } else {
            (RecentSearchesAdapter(this@OrderEstimatesFragment)).apply {
                this.setData(resources.getStringArray(R.array.recent_searches_workshop)
                    .toMutableList())
                rvPartsService.adapter = this
                rvPartsService.makeVisible(true)
            }
        }
    }

    private fun addSelectedPart() = binding.apply {
        rlMostUsedParts.makeVisible(false)
        lblPartsServices.background =
            ContextCompat.getDrawable(requireActivity(), R.drawable.bg_order_detail_heading)
        if (!::addedPartList.isInitialized) {
            addedPartList = mutableListOf()
        }
        if (::selectedService.isInitialized) {
            addedPartList.add(selectedService)
        }
        if (::workDoneList.isInitialized && workDoneList.size > 0) {
            if (selectedService.type == "inventory") {
                selectedService.workDone = workDoneList[0].id?.toLong()
                selectedService.workDoneName = workDoneList[0].name
            }
        }
        if (!::addedPartsAdapter.isInitialized) addedPartsAdapter =
            OrderEstimatePartsAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol),
                isAcc,
                requireContext(),
                addedPartList,
                this@OrderEstimatesFragment) //        addedPartsAdapter.notifyItemRangeInserted(0, addedPartList.size-1)
        addedPartsAdapter.notifyItemInserted(addedPartList.size - 1)

        rlSelectParts.makeVisible(false)
        rvAddedParts.makeVisible(true)
        rlChoose.makeVisible(true)
        btnAdd.makeVisible(false)
        etSearchPart.isEnabled = true
        etSearchPart.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_search), null, null, null)
        rvPartsService.makeVisible(false)
        etSearchPart.setText("")
        dontSearch = false

        addPartToRecentSearch(selectedService.serviceName ?: "")

        updateTotalPrice()
    }

    private fun addPartToRecentSearch(serviceName: String) {
        if (shopType == AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) {
            var recentSearchesList =
                viewModel.getStringSharedPreference(AppENUM.RECENT_SEARCHES_WORKSHOP, "")
            recentSearchesList = if (recentSearchesList.isNotEmpty()) {
                if (recentSearchesList.contains(serviceName.toUpperCase(Locale.getDefault()))) {
                    recentSearchesList
                } else "${serviceName},$recentSearchesList"
            } else serviceName
            viewModel.putStringSharedPreference(AppENUM.RECENT_SEARCHES_WORKSHOP,
                recentSearchesList.toUpperCase(Locale.getDefault()))
        }
    }

    private fun clearSearchET() = binding.apply {
        etSearchPart.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(
            requireContext(),
            R.drawable.ic_search), null, null, null)
        etSearchPart.setText("")

        (rvPartsService.adapter as? PartsAdapter)?.let {
            rvPartsService.adapter = null
            rvPartsService.makeVisible(false)
        }
        btnAdd.makeVisible(false)
        etSearchPart.isEnabled = true
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun selectPackage(position: Int, data: OrderEstimateRequestResponse?) = binding.apply {
        lblPackages.setCompoundDrawablesWithIntrinsicBounds(null,
            null,
            ContextCompat.getDrawable(requireActivity(), R.drawable.ic_add_black),
            null) //        rvPackages.makeVisible(false)
        cvPackages.makeVisible(false)
        rvAddedPackages.makeVisible(true)

        if (data == null) {
            val selPackage = Packages()
            selPackage.packageName = packagesList[position].name
            if (shopType == AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) {
                selPackage.packageId = packagesList[position].packageId
            } else selPackage.id = packagesList[position].id
            selPackage.packageTotal = packagesList[position].total ?: 0.0
            selectedPackage = selPackage
        } else {
            selectedPackage = data.packages?.get(position) ?: Packages()
        }
        if (rvAddedPackages.layoutManager == null) {
            rvAddedPackages.layoutManager = LinearLayoutManager(requireContext())
            addedPackageList = mutableListOf()
            addedOEPackagesAdapter = AddedOEPackagesAdapter(viewModel.getStringSharedPreference(
                AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol),
                addedPackageList,
                this@OrderEstimatesFragment)
            rvAddedPackages.adapter = addedOEPackagesAdapter
        }
        addedOEPackagesAdapter.dataList.add(selectedPackage)
        addedOEPackagesAdapter.notifyDataSetChanged() //        viewModel.getServicesOfPackage(packagesList[position].packageId.toString())
        //        setPackageInclusivesAdapter(packagesList[position].descTags ?: listOf())
        updateTotalPrice()
    }

    private fun hideOrShowPackagesList() = binding.apply {
        if (::packagesList.isInitialized && packagesList.size > 0) {
            if (cvPackages.visibility == View.VISIBLE) {
                cvPackages.makeVisible(false)
                rvAddedPackages.makeVisible(true)
                if (::addedOEPackagesAdapter.isInitialized && addedOEPackagesAdapter.dataList.size > 0) lblPackages.background =
                    ContextCompat.getDrawable(requireActivity(), R.drawable.bg_order_detail_heading)
                else lblPackages.background =
                    ContextCompat.getDrawable(requireActivity(), R.drawable.bg_white_12)
                lblPackages.setCompoundDrawablesWithIntrinsicBounds(null,
                    null,
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_add_black),
                    null)
            } else {
                cvPackages.makeVisible(true)
                rvAddedPackages.makeVisible(false)
                lblPackages.background =
                    ContextCompat.getDrawable(requireActivity(), R.drawable.bg_order_detail_heading)
                lblPackages.setCompoundDrawablesWithIntrinsicBounds(null,
                    null,
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_arrow_up_black),
                    null)
            }
        } else {
            if (llNoPackage.visibility == View.VISIBLE) {
                lblPackages.background =
                    ContextCompat.getDrawable(requireActivity(), R.drawable.bg_white_12)
                lblPackages.setCompoundDrawablesWithIntrinsicBounds(null,
                    null,
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_add_black),
                    null)
                llNoPackage.makeVisible(false)
            } else {
                lblPackages.background =
                    ContextCompat.getDrawable(requireActivity(), R.drawable.bg_order_detail_heading)
                lblPackages.setCompoundDrawablesWithIntrinsicBounds(null,
                    null,
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_arrow_up_black),
                    null)
                llNoPackage.makeVisible(true)
            }
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        if (!dontSearch) {
            if (p0?.isNotEmpty() == true) {
                showLoader()
                lifecycleScope.launchWhenResumed {
                    delay(300)
                    binding.rvPartsService.makeVisible(true)
                    searchText = p0.toString()
                    viewModel.getServicesAPI(searchText, isTwoWheeler)
                }
            } else {
                (binding.rvPartsService.adapter as? PartsAdapter)?.let {
                    binding.rvPartsService.adapter = null
                    binding.rvPartsService.makeVisible(false)
                } //                lblRecentSearches.makeVisible(true)
            }
        }
    }

    override fun afterTextChanged(p0: Editable?) { //do nothing
    }

    private fun setPartsAdapter() = binding.apply {
        lblRecentSearches.makeVisible(false)
        partsAdapter = PartsAdapter(partsList, searchText, this@OrderEstimatesFragment)
        rvPartsService.adapter = partsAdapter
        ivNoSearchResults.makeVisible(partsList.size == 0)
        lblNoResults.makeVisible(partsList.size == 0)
    }

    private fun showWorkDoneWindow(oeServicesItem: OEServicesItem, position: Int, view: View) {
        workDoneForPosition = position
        if (::workDoneList.isInitialized) {
            workDoneList.forEach {
                it.isSelected = false
                if (it.id?.toLong() == oeServicesItem.workDone) {
                    it.isSelected = true
                    return@forEach
                }
            }
            workDonePopup = showWorkDoneFilter()
            workDonePopup?.isOutsideTouchable = true
            workDonePopup?.isFocusable = true
            workDonePopup?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            workDonePopup?.elevation = 10f
            workDonePopup?.showAsDropDown(view)
            workDonePopup?.update()
        }
    }

    private fun updateTotalPrice() {
        var flag = true
        totalEstimatesAmount = 0.0
        totalDiscount = 0.0
        if (::addedPackageList.isInitialized && addedPackageList.size > 0) {
            flag = false
            binding.rlDiscountTotal.makeVisible(true)
            binding.llButtons.makeVisible(true)
            addedPackageList.forEach {
                totalEstimatesAmount += it.packageTotal ?: 0.0
            }
        }
        if (::addedPartList.isInitialized && addedPartList.size > 0) {
            flag = false
            binding.rlDiscountTotal.makeVisible(true)
            binding.llButtons.makeVisible(true)
            addedPartList.forEach {
                totalEstimatesAmount += it.total ?: 0.0
                totalDiscount += it.discountPerQuantity?.times(it.quantity ?: 0) ?: 0.0
            }
        }
        (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            AppENUM.RefactoredStrings.defaultCurrencySymbol) + String.format("%.2f",
            totalEstimatesAmount)).apply {
            binding.tvTotalCost.text = this
        }
        (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            AppENUM.RefactoredStrings.defaultCurrencySymbol) + String.format("%.2f",
            totalDiscount)).apply {
            binding.tvDiscount.text = this
        }
        if (flag) binding.rlDiscountTotal.makeVisible(false)
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is AddOrderEstPartEventModel -> {
                selectedService = event.extra as OEServicesItem

                if (selectedService.type == "part") {
                    if (::inventoryItemsList.isInitialized) {
                        inventoryItemsList.forEach {
                            if (it.skuId == selectedService.skuId) {
                                selectedService.partAlreadyAdded = true
                                return@forEach
                            }
                        }
                    }
                }

                addSelectedPart()

                checkAndAddSelectedParts(false)
            }

            is InventoryItemsEvent -> {
                (event.extra as? MutableList<RackItem>)?.let {
                    inventoryItemsList = it
                }
            }

            is CompleteRegLocUpdateEvent -> {
                activity?.let {
                    FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.COMPLETE_REGISTRATION,
                        Bundle().apply {
                            this.putParcelable(AppENUM.COMPLETE_REG_DATA, event.addressData)
                        },
                        object : ActionListener {
                            @RequiresApi(Build.VERSION_CODES.P)
                            override fun onActionItem(extra: Any?, extra2: Any?) {
                                if (extra as Boolean? == true) updateOrderEstimates()
                            }
                        })?.let { baseFragment ->
                        baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
                    }
                }
            }
        }
    }

    fun updateScreen() {
        isTwoWheeler =
            OrderDetailWrapperFragment.provideOrderDetails()?.carDetails?.isTwoWheeler == 2
        showLoader()
        if (shopType == AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT) { //            if (::packagesList.isInitialized) viewModel.getOrderEstimatesAPI(
            //                OrderDetailWrapperFragment.orderId)
            //            else
            viewModel.getCustomPackagesAPI(isTwoWheeler)
        } else { //            if (::packagesList.isInitialized) viewModel.getOrderEstimatesAPI(
            //                OrderDetailWrapperFragment.orderId)
            //            else
            viewModel.getPackagesAndWorkDoneAPI(isTwoWheeler)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun showWorkDoneFilter(): PopupWindow {
        val inflater =
            requireContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as? LayoutInflater
        val view = inflater?.inflate(R.layout.popup_work_done, null)
        val recyclerView = view?.findViewById<RecyclerView>(R.id.rvWorkDone)
        val adapter = NewWorkDoneAdapter(workDoneList, this)
        recyclerView?.layoutManager = LinearLayoutManager(requireContext())
        recyclerView?.adapter = adapter
        adapter.notifyDataSetChanged() //        view.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        return PopupWindow(view,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            true)
    }


    private fun fireAddCustomServiceEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.NEW_SERVICE, searchText)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ORDER_ESTIMATES)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.SERVICE_ADDITION,
            bundle)

    }


}