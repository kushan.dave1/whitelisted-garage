package whitelisted.garage.view.fragments.myBusiness

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.request.AddressData
import whitelisted.garage.api.request.SaveGMBRequest
import whitelisted.garage.api.response.GetGMDResponse
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentEditGmbBinding
import whitelisted.garage.eventBus.GmbRequestedEvent
import whitelisted.garage.eventBus.LocationSelectedEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.view.adapter.PhotoAdapter
import whitelisted.garage.viewmodels.MyBusinessViewModel
import java.io.ByteArrayOutputStream
import java.util.*

class EditGmbWebsiteComboFragment : BaseFragment() {

    private var gmbData: GetGMDResponse? = null
    private val viewModel: MyBusinessViewModel by sharedViewModel()
    private val binding by viewBinding(FragmentEditGmbBinding::inflate)
    private lateinit var photoList: MutableList<Uri>
    private var imageURl = mutableListOf<String>()
    private lateinit var photoAdapter: PhotoAdapter
    private lateinit var storageRef: StorageReference
    private var uploadMediaRef: StorageReference? = null
    private var trackMediaUploadCount = 0
    private var lat: Double? = null
    private var lng: Double? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setData() {
        EventBus.getDefault().post(GmbRequestedEvent(false))
        ImageLoader.loadImage(binding.imageFlag,
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_FLAG, ""))
        binding.tv91.text = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
            AppENUM.RefactoredStrings.defaultCountryCode)
        binding.etMobile.setText(viewModel.getStringSharedPreference(AppENUM.MOBILE_NUMBER, ""))
    }

    private fun observeData() {
        observeGetPhoto()
        observeGetBusinessPageData()
        observeGetGMD()
        observeSaveGMD()
        observeGMBUnLockSuccess()
        observeIsMediaUploadedSuccess()
        observeIsMediaFetchedSuccess()
    }

    private fun observeIsMediaFetchedSuccess() {
        viewModel.provideIsMediaDataFetched().observe(viewLifecycleOwner) {
            if (it == true) {
                hitTheSaveApi()
            }
        }
    }

    private fun observeIsMediaUploadedSuccess() {
        viewModel.provideIsMediaUploadedSuccessFully().observe(viewLifecycleOwner) {
            if (it == true) {
                getAllImageUriFromFirebase()
            }
        }
    }

    private fun observeGMBUnLockSuccess() {
        viewModel.provideGMBUnlockSuccess().observe(viewLifecycleOwner) {
            if (it == true) {
                photoAdapter.getAdapterList().clear()
                showLoader()
                viewModel.getGMB()
                checkStatusData()
            }
        }
    }

    private fun observeGetGMD() {
        viewModel.provideGMD().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    gmbData = it.body.data
                    setDataToUi(it.body.data)
                    checkStatusData()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeSaveGMD() {
        viewModel.provideSaveGMD().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.succ_request_to_create_gmb))
                    showLoader()
                    viewModel.getGMB()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setDataToUi(data: GetGMDResponse) {
        if (data.gmb_updated == true) {
            popBackStackAndHideKeyboard()
            CommonUtils.addFragmentUtil(requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.VIEW_GMB_FRAGMENT, null))
        } else {
            binding.tvAddress.text = data.address // binding.etMobile.setText(data.gmb_mobile)
            lat = data.latitude
            lng = data.longitude
            setGaragePhoto(data.workshopImages)
        }
    }


    @SuppressLint("NotifyDataSetChanged")
    private fun setGaragePhoto(workshopImages: List<String>?) {
        photoList.clear()
        photoAdapter.getAdapterList().clear()
        photoAdapter.notifyDataSetChanged()
        if (workshopImages?.isNotEmpty() == true) {
            photoAdapter.setIsViewOnly(true)
            for (data in workshopImages) {
                photoAdapter.getAdapterList().add(0, Uri.parse(data))
                photoAdapter.notifyItemInserted(0)
            }
        } else {
            photoList.add(Uri.EMPTY)
            photoAdapter.setIsViewOnly(false)
            photoAdapter.setData(photoList)
        }
    }


    @SuppressLint("NotifyDataSetChanged")
    private fun observeGetPhoto() {
        viewModel.providePhotoUriFromAdd().observe(viewLifecycleOwner) {
            it?.size?.let { it1 ->
                repeat(it1) { position ->
                    if (it[position].toString().isNotEmpty()) {
                        photoAdapter.getAdapterList().add(0, (it[position]))
                    }
                    photoAdapter.notifyDataSetChanged()
                    checkStatusData()
                }
            }
        }
    }

    private fun observeGetBusinessPageData() {
        viewModel.provideBusinessPageData().observe(viewLifecycleOwner) {
            binding.etMobile.setText(it?.mobile)
            binding.etEmail.setText(it?.name)
            checkStatusData()
        }
    }

    private fun setDataToBusinessPage(): Bundle {
        val bundle = Bundle().apply {
            putString(AppENUM.MOBILE, binding.etMobile.text.toString())
            putString(AppENUM.NAME, binding.etEmail.text.toString())
        }
        return bundle
    }

    private fun setupScreen() {
        binding.imageEditPhone.setOnClickListener(this)
        binding.imagEditGarage.setOnClickListener(this)
        binding.btnRequestPage.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)
        binding.imageEmail.setOnClickListener(this)
        setUpRv()
        setData()
        observeData()
        showLoader()
        viewModel.getGMB()
    }


    private fun setUpRv() {
        photoList = ArrayList()
        photoAdapter = PhotoAdapter(this)
        binding.rvGaragePhoto.layoutManager =
            GridLayoutManager(requireContext(), 4, GridLayoutManager.VERTICAL, false)
        binding.rvGaragePhoto.adapter = photoAdapter
        photoAdapter.setData(photoList)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imagEditGarage -> {
                CommonUtils.addFragmentUtil(
                    requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.ADDRESS_FRAGMENT, null),
                )
            }
            R.id.imageEditPhone, R.id.imageEmail -> {
                CommonUtils.addFragmentUtil(
                    requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.Edit_Business_Garage_Name_Fragment,
                        setDataToBusinessPage()),
                )
            }

            R.id.btnRequestPage -> {
                if (validateGMB()) {
                    if (photoAdapter.getAdapterList()
                            .isNotEmpty() && photoAdapter.getAdapterList().size > 2) {
                        setPageEvent(
                            FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_WEBSITE_REQUEST,
                            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_GMB,
                        )
                        showLoader()
                        trackMediaUploadCount = 0
                        for (data in photoAdapter.getAdapterList()) {
                            if (data != Uri.EMPTY) {
                                uploadMedia(data)
                            }
                        }
                    } else {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.at_leat_two_image))
                    }
                }
            }
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.imgBusImage -> {
                CommonUtils.genericCastOrNull<Uri>(v.getTag(R.id.model))?.let { uri ->
                    imagePreview(view, uri)
                }
            }
            R.id.llDelImage -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    photoAdapter.removeItemFromList(position)
                    checkStatusData()

                }
            }
            R.id.rlAddImage -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { _ ->
                    CommonUtils.genericCastOrNull<Uri>(v.getTag(R.id.model))?.let {
                        CommonUtils.addFragmentUtil(
                            requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.Edit_Business_Photo_Fragment,
                                null),
                        )
                    }
                }
            }
        }
    }

    private fun validateGMB(): Boolean {
        return when {
            binding.etMobile.text.toString().isEmpty() -> {
                CommonUtils.showToast(requireContext(), getString(R.string.phone_not_valid))
                false
            }
            binding.tvAddress.text.toString().isEmpty() -> {
                CommonUtils.showToast(requireContext(), getString(R.string.address_not_valid))
                false
            }

            binding.etMobile.text.toString().length != 10 -> {
                if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                        AppENUM.RefactoredStrings.defaultCurrency) == AppENUM.RefactoredStrings.defaultCurrency) {
                    CommonUtils.showToast(requireContext(), getString(R.string.phone_not_empty))
                    false
                } else true
            }
            else -> true
        }
    }

    private fun hitTheSaveApi() {
        val saveGMBRequest = SaveGMBRequest().apply {
            this.gmbMobile = binding.etMobile.text.toString()
            this.email_id = binding.etEmail.text.toString()
            this.address = binding.tvAddress.text.toString()
            lat?.let {
                this.latitude = lat
            }
            lng?.let {
                this.longitude = lng
            }
            this.workshopImages = imageURl
            this.name = gmbData?.name
            this.gmbAttributes = gmbData?.gmbAttributes
            this.gmbCategories = gmbData?.gmbCategories
            this.openTime = gmbData?.openTime
        }

        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_GMB_EDIT)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.MOBILE_NUMBER,
                saveGMBRequest.gmbMobile)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.ADDRESS, saveGMBRequest.address)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.NAME, saveGMBRequest.name)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_REQUEST_GMB_CREATION,
                this)
        }
        showLoader()
        viewModel.saveGMB(saveGMBRequest)
    }


    private fun uploadMedia(uploadItem: Uri) {
        lifecycleScope.launch {
            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
            val orderRef: StorageReference = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
                .child(viewModel.getStringSharedPreference(AppENUM.USER_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                .child(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_GMB)

            val fileNameRand: String = (Calendar.getInstance().timeInMillis.toString())

            uploadMediaRef = orderRef.child(fileNameRand)

            val uploadTask: UploadTask?
            val emptyBitmap = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(emptyBitmap)
            canvas.drawColor(ContextCompat.getColor(requireContext(), R.color.white))
            var bmp = try {
                MediaStore.Images.Media.getBitmap(requireContext().contentResolver, uploadItem)
                    ?: emptyBitmap
            } catch (e: Exception) {
                emptyBitmap
            }
            val byteArrayOutputStream = ByteArrayOutputStream()
            bmp = CommonUtils.scaleBitmap(bmp)
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            val data = byteArrayOutputStream.toByteArray()
            uploadTask = uploadMediaRef?.putBytes(data)

            // Register observers to listen for when the download is done or if it fails
            uploadTask?.addOnFailureListener { // Log.d("",it.toString())

                // Handle unsuccessful uploads
            }?.addOnSuccessListener {

                // taskSnapshot.metadata contains file metadata such as size, content-type, etc.
                // ...
                trackMediaUploadCount++
                if (trackMediaUploadCount == (photoAdapter.getAdapterList().size - 1) || trackMediaUploadCount == (photoAdapter.getAdapterList().size) || trackMediaUploadCount > (photoAdapter.getAdapterList().size - 1)) {
                    viewModel.setIsMediaUploadedSuccess(true)
                }
            }?.addOnCompleteListener {

            }
        }
    }

    private fun getAllImageUriFromFirebase() {
        showLoader()
        lifecycleScope.launch {
            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
            val orderRef: StorageReference = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
                .child(viewModel.getStringSharedPreference(AppENUM.USER_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                .child(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_GMB)
            orderRef.listAll().addOnSuccessListener {
                if (it.items.size > 0) {
                    it.items.forEach { item ->
                        item.downloadUrl.addOnSuccessListener { uri ->
                            imageURl.add(uri.toString())
                            if (imageURl.size == it.items.size) {
                                viewModel.setIsMediaDataFetched(true)
                            }
                        }
                    }
                }
            }.addOnFailureListener {
                hideLoader()
            }
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is LocationSelectedEvent -> {
                (event.extra as? AddressData)?.let {
                    binding.tvAddress.text = it.fullAddress
                    lat = it.latitude
                    lng = it.longitude
                }
            }
        }
    }

    private fun checkStatusData() {
        if (binding.tvAddress.text.isNotEmpty()) {
            binding.tvStatusGarage.visibility = View.INVISIBLE
        } else {
            binding.tvStatusGarage.visibility = View.VISIBLE
        }
        if (binding.etMobile.text.toString().isNotEmpty()) {
            binding.tvStatusGaragePhone.visibility = View.INVISIBLE
        } else {
            binding.tvStatusGaragePhone.visibility = View.VISIBLE
        }
        if (photoAdapter.getAdapterList().size > 1) {
            binding.tvStatusGarageAddress.visibility = View.INVISIBLE
        } else {
            binding.tvStatusGarageAddress.visibility = View.VISIBLE
        }
    }
}