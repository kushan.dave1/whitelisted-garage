package whitelisted.garage.view.fragments.inventory

import android.app.Activity
import android.content.DialogInterface
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.CreateInventoryRackRequest
import whitelisted.garage.api.request.RackItem
import whitelisted.garage.api.response.GetInventoryRackResponse
import whitelisted.garage.api.response.SparesHomeResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseActivity
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentInventoryBinding
import whitelisted.garage.eventBus.RackUpdateEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.InventoryAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.InventoryTabFragmentViewModel
import whitelisted.garage.viewmodels.WalkthroughSharedViewModel
import java.util.*

class InventoryFragment : BaseFragment() {

    private val binding by viewBinding(FragmentInventoryBinding::inflate)
    private var rackListAdapter: InventoryAdapter? = null
    private val viewModelTab: InventoryTabFragmentViewModel by sharedViewModel()
    private val walkThroughViewModel: WalkthroughSharedViewModel by sharedViewModel()
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var listOfRacks = mutableListOf<GetInventoryRackResponse>()
    private lateinit var filterList: MutableList<GetInventoryRackResponse>
    private var rackList: MutableList<RackItem>? = null
    private var deletedPosition = 0
    private var positionOFImageToBeUploaded = 0
    private var isMediaUploaded = false
    private var request: CreateInventoryRackRequest? = null
    private lateinit var profilePicture: String
    private var deletedRackId = ""
    private var dataFetched = false
    private var isTwoWheeler = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() { //        viewModelTab.getStringSharedPreference(AppENUM.UserKeySaveENUM.INVENTORY_RESPONSE, "").let {
        //            if (it.isNotEmpty()) {
        //                binding.slItemsFI.makeVisible(false)
        //                hideLoader()
        //                val sparesHomeResponse = Gson().fromJson(it, SparesHomeResponseModel::class.java)
        //                sparesHomeResponse?.let { homeResponse ->
        //                    listOfRacks = it.body.data
        //                    if (listOfRacks.isNotEmpty()) {
        //                        binding.llNoRackLayout.makeVisible(false)
        //                        for (value in listOfRacks) {
        //                            value.isNewRack = false
        //                        }
        //                        binding.tvDownloadSheet.makeVisible(true)
        //                    } else {
        //                        binding.tvDownloadSheet.makeVisible(false)
        //                        binding.llNoRackLayout.makeVisible(true)
        //                    }
        //                    setAdapter("")
        //                }
        //            }
        //        }

        getDataFromArgs()
        clickListener()
        observeData()
        setUpRv()
    }

    fun apiCall() {
        if (!dataFetched) { //            showLoader()
            binding.slItemsFI.makeVisible(true)
            binding.slItemsFI.startShimmer()
            viewModelTab.getInventoryRack()
        } //        }
    }

    private fun addTextWatcher() {
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?,
                p1: Int,
                p2: Int,
                p3: Int) { //    nothing
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                setAdapter(p0.toString())
            }

            override fun afterTextChanged(p0: Editable?) { //  nothing
            }
        })
    }

    override fun handleBackPress(): Boolean {
        return false
    }

    private fun observeData() {
        observeCreateInventory()
        observeGetInventoryRack()
        observeDeleteInventory()
        observeUpdateInventoryDetails()
        observeSearchResponse()
        observeCSVDownloadResponse()
        observeImageUpload()
        observeWalkThroughEvent()
    }

    private fun observeWalkThroughEvent() {
        walkThroughViewModel.provideInventoryWalkThroughEvent().observe(viewLifecycleOwner) {
            when (it) {
                R.id.rlOrders -> {
                    if (isVisible) {
                        if (!viewModelTab.getBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN,
                                true)) {
                            viewModelTab.launch(Dispatchers.Main) {
                                delay(500)
                                startWalkthrough(binding.llAddRack)
                            }
                        }
                    }
                }
                R.id.tvMyOrdersCart -> {
                    if (isVisible) {
                        if (!viewModelTab.getBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN,
                                true)) {
                            viewModelTab.launch(Dispatchers.Main) {
                                delay(500)
                                startWalkthrough(binding.llAddRack)
                            }
                        }
                    }
                }

                R.id.llAddRack -> {
                    startWalkthrough(binding.llUploadBulkInventory)
                }
            }
        }
    }

    private fun observeImageUpload() {
        viewModelTab.provideUploadInventoryImageResponse()
            .observe(viewLifecycleOwner) { sendSuccessUrl ->
                hideLoader()
                sendSuccessUrl.isSuccessFailure?.let { it ->
                    if (it) {
                        sendSuccessUrl.url?.let {
                            isMediaUploaded = false
                            showLoader()
                            request?.images?.add(0, it)
                            request?.let { it1 ->
                                viewModelTab.updateInventoryRackDetails(request?.id ?: "", it1)
                            }
                        }
                    } else {
                        sendSuccessUrl.url?.let { it1 ->
                            CommonUtils.showToast(requireContext(), it1, true)
                        }
                    }
                }
            }
    }

    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            hideLoader()
            if (result.resultCode == Activity.RESULT_OK) { //Image Uri will not be null for RESULT_OK
                val fileUri = result.data?.data
                onImagePicked(fileUri)
            } else if (result.resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(requireContext(),
                    ImagePicker.getError(result.data),
                    Toast.LENGTH_SHORT).show()
            }
        }

    private fun onImagePicked(imageUri: Uri?) {
        profilePicture = imageUri?.path ?: ""
        isMediaUploaded = true
        rackListAdapter?.getAdapterData()?.get(positionOFImageToBeUploaded)?.isNewRack = true
        rackListAdapter?.getAdapterData()?.get(positionOFImageToBeUploaded)?.images?.add(0,
            imageUri.toString())
        rackListAdapter?.notifyItemChanged(positionOFImageToBeUploaded)
    }

    private fun observeCSVDownloadResponse() {
        viewModelTab.provideDownloadCSVResponse().observe(viewLifecycleOwner) {
            CommonUtils.showToast(requireContext(), getString(R.string.downloading), true)
            CommonUtils.saveToDisk(requireActivity(),
                it,
                "${viewModelTab.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME)}${AppENUM.INVENTORY_CSV}",
                "${viewModelTab.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME)}${AppENUM.INVENTORY_CSV}",
                getString(R.string.download_inventory__csv))
        }
    }

    private fun observeSearchResponse() {
        viewModelTab.provideSearchImageResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> { //
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun observeUpdateInventoryDetails() {
        viewModelTab.provideUpdateInventoryDetailsResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (isMediaUploaded) {
                        showLoader()
                        viewModelTab.uploadInventoryPic(profilePicture, request?.id.toString())
                    } else {
                        showLoader()
                        viewModelTab.getInventoryRack()
                    }
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)

            }
        }
    }

    private fun observeDeleteInventory() {
        viewModelTab.provideDeleteInventoryResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (rackListAdapter?.getAdapterData()?.size ?: 0 > deletedPosition) {
                        viewModelTab.deleteInventoryFromFirebase(deletedRackId)
                        rackListAdapter?.getAdapterData()?.removeAt(deletedPosition)
                        listOfRacks.removeAt(deletedPosition)
                        rackListAdapter?.notifyItemRemoved(deletedPosition)
                        if (rackListAdapter?.getAdapterData()?.size == 0) {
                            binding.llNoRackLayout.makeVisible(true)
                            binding.tvDownloadSheet.makeVisible(false)
                        } else {
                            binding.llNoRackLayout.makeVisible(false)
                        }
                    }
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)

            }
        }
    }

    private fun observeGetInventoryRack() {
        viewModelTab.provideInventoryRackResponse().observe(viewLifecycleOwner) {
            binding.slItemsFI.makeVisible(false)
            hideLoader()
            when (it) {
                is Result.Success -> {
                    dataFetched = true
                    binding.etSearch.clearFocus()
                    binding.etSearch.setText("")
                    listOfRacks.clear()
                    rackList?.clear()
                    listOfRacks = it.body.data
                    if (listOfRacks.isNotEmpty()) {
                        binding.llNoRackLayout.makeVisible(false)
                        for (value in listOfRacks) {
                            value.isNewRack = false
                        }
                        binding.tvDownloadSheet.makeVisible(true)
                    } else {
                        binding.tvDownloadSheet.makeVisible(false)
                        binding.llNoRackLayout.makeVisible(true)
                    }
                    setAdapter("")
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun setAdapter(searchString: String) {
        if (searchString.trim().isNotEmpty()) {
            if (::filterList.isInitialized) filterList.clear()
            else filterList = mutableListOf()
            listOfRacks.forEach {
                if (it.name.toString().toLowerCase(Locale.getDefault())
                        .contains(searchString.toLowerCase(Locale.getDefault())) || checkForValueOFFiltered(
                        it.rack_items ?: mutableListOf(),
                        searchString)) {
                    filterList.add(it)
                }
            }
            rackListAdapter?.setData(filterList)
        } else {
            rackListAdapter?.setData(listOfRacks)
        }
    }

    private fun checkForValueOFFiltered(rackItems: MutableList<RackItem>,
        searchString: String): Boolean {
        for (data in rackItems) {
            if (data.brandName.toString().toLowerCase(Locale.getDefault())
                    .contains(searchString.toLowerCase(Locale.getDefault())) || data.partName.toString()
                    .toLowerCase(Locale.getDefault())
                    .contains(searchString.toLowerCase(Locale.getDefault()))) {
                return true
            }
        }
        return false
    }

    private fun observeCreateInventory() {
        viewModelTab.provideCreateInventoryResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (isMediaUploaded) {
                        request?.id = it.body.data.rackId
                        showLoader()
                        viewModelTab.uploadInventoryPic(profilePicture,
                            it.body.data.rackId.toString())
                    } else {
                        showLoader()
                        viewModelTab.getInventoryRack()
                    }
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun getDataFromArgs() {
        isTwoWheeler = arguments?.getBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, false) ?: false
        if (arguments?.getBoolean(AppENUM.INNER_FRAGMENT, false) == true) {
            binding.imgBack.makeVisible(true)
            handleBackPress()
            binding.clBaseFI.setPadding(0, 0, 0, 0)
            apiCall()
        } else {
            binding.clBaseFI.setPadding(0, 0, 0, 186)
        }
        rackList = mutableListOf()
    }

    private fun clickListener() {
        addTextWatcher()
        binding.llAddRack.setOnClickListener(this) //binding.tvDownloadSheet.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)
        binding.tvDownloadSheet.setOnClickListener(this)
        binding.llUploadBulkInventory.setOnClickListener(this)
        binding.srl.setOnRefreshListener {
            binding.srl.isRefreshing = false
            showLoader()
            viewModelTab.getInventoryRack()
        }
    }

    private fun setUpRv() {
        rackListAdapter = InventoryAdapter(this)
        binding.rvAddRack.adapter = rackListAdapter
        rackListAdapter?.setData(listOfRacks)
    }


    override fun onClick(v: View) {
        if (dashboardViewModel.getBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN, true)) {
            super.onClick(v)
            when (v.id) {
                R.id.imgBack -> {
                    popBackStackAndHideKeyboard()
                }
                R.id.llAddRack -> {
                    if (viewModelTab.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                            false)) {
                        addRack()
                    } else {
                        if (listOfRacks.size < (dashboardViewModel.screensConfigResponse.value?.membershipConfig?.inventoryLimit
                                ?: 1)) {
                            addRack()
                        } else {
                            requireActivity().let {
                                FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT, Bundle().apply {
                                    putString(AppENUM.PRO_LOCKED_ALERT_MSG,  dashboardViewModel.screensConfigResponse.value?.membershipConfig?.inventoryRacksMsg)
                                })
                                    ?.let { baseFragment ->
                                        baseFragment.show(it.supportFragmentManager,
                                            baseFragment.javaClass.name)
                                    }
                            }
                        }
                    }
                }
                R.id.imgSaveRack -> {
                    CommonUtils.genericCastOrNull<CreateInventoryRackRequest>(v.getTag(R.id.imgSaveRack))
                        ?.let {
                            request = it
                            if (!it.rackName.isNullOrEmpty()) {
                                if (it.isUpdate) {
                                    showLoader()
                                    viewModelTab.createInventoryRack(it)
                                } else {
                                    showLoader()
                                    it.id?.let { it1 ->
                                        viewModelTab.updateInventoryRackDetails(it1, it)
                                    }
                                }
                            } else {
                                CommonUtils.showToast(requireContext(),
                                    getString(R.string.rack_name_empty))
                            }
                        }
                }
                R.id.llPhotoLayout -> {
                    CommonUtils.genericCastOrNull<List<String>>(v.getTag(R.id.llPhotoLayout))?.let {
                        CommonUtils.addFragmentUtil(
                            requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.INVENTORY_IMAGE_PREVIEW,
                                Bundle().apply {
                                    this.putStringArrayList(AppENUM.IMAGE_URL, ArrayList(it))
                                }),
                        )
                    }
                }
                R.id.editCard -> {
                    CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.editCard))?.let {
                        showPopupMenu(v, it)
                    }
                }
                R.id.tvDownloadSheet -> {
                    viewModelTab.downloadInventoryCSV()
                }
                R.id.imgAddRackItems -> {
                    CommonUtils.genericCastOrNull<GetInventoryRackResponse>(v.getTag(R.id.imgAddRackItems))
                        ?.let {
                            CommonUtils.addFragmentUtil(requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.ADD_RACK_ITEM_FRAGMENT,
                                    Bundle().apply {
                                        putParcelable(AppENUM.INVENTORY_DATA, it)
                                        putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER,
                                            isTwoWheeler)
                                    }),
                                target = R.id.fragment_container_2)
                            setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_ITEM_INV,
                                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ADD_INVENTORY_RACK)
                        }
                }
                R.id.llCard -> {
                    CommonUtils.genericCastOrNull<GetInventoryRackResponse>(v.getTag(R.id.llCard))
                        ?.let {
                            if (it.items_count ?: 0 > 0) {
                                CommonUtils.addFragmentUtil(
                                    requireActivity(),
                                    FragmentFactory.fragment(FragmentFactory.Fragments.ADD_RACK_FRAGMENT,
                                        Bundle().apply {
                                            putParcelable(AppENUM.INVENTORY_DATA, it)
                                            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER,
                                                isTwoWheeler)
                                        }),
                                )
                            } else {
                                CommonUtils.addFragmentUtil(requireActivity(),
                                    FragmentFactory.fragment(FragmentFactory.Fragments.ADD_RACK_ITEM_FRAGMENT,
                                        Bundle().apply {
                                            putParcelable(AppENUM.INVENTORY_DATA, it)
                                            putBoolean(AppENUM.IS_EMPTY, true)
                                            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER,
                                                isTwoWheeler)
                                        }),
                                    target = R.id.fragment_container_2)
                                setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_ITEM_INV,
                                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ADD_INVENTORY_RACK)
                            }
                        }
                }

                R.id.llUploadBulkInventory -> {
                    if (viewModelTab.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                            false)) {
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.INVENTORY_BULK_UPLOAD))
                    }else {
                        fireFeatureLockedEvent()
                        requireActivity().let {
                            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT, Bundle().apply {
                                putString(AppENUM.PRO_LOCKED_ALERT_MSG,  dashboardViewModel.screensConfigResponse.value?.membershipConfig?.bulkInventoryMsg)
                            })
                                ?.let { baseFragment ->
                                    baseFragment.show(it.supportFragmentManager,
                                        baseFragment.javaClass.name)
                                }
                        }
                    }
                }
            }
        }
    }

    private fun addRack() {
        if (listOfRacks.size > 0 && listOfRacks[0].isNewRack != true) {
            listOfRacks.add(0, GetInventoryRackResponse().apply {
                items_count = 0
                images = mutableListOf()
                isNewRack = true
            })
            rackListAdapter?.setData(listOfRacks)
            binding.rvAddRack.scrollToPosition(0)
            binding.llNoRackLayout.makeVisible(false) // firebase
            setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_RACK,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_INVENTORY)
        } else {
            listOfRacks.add(0, GetInventoryRackResponse().apply {
                items_count = 0
                images = mutableListOf()
                isNewRack = true
            })
            rackListAdapter?.setData(listOfRacks)
        }
        binding.llNoRackLayout.makeVisible(false)
    }

    private fun showPopupMenu(view: View, position: Int) {
        val popupMenu = PopupMenu(requireContext(), view)
        popupMenu.menuInflater.inflate(R.menu.edit_rack_menu, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.edit -> {
                    rackListAdapter?.getAdapterData()?.get(position)?.isNewRack = true
                    rackListAdapter?.notifyItemChanged(position)
                    CommonUtils.showKeyboard(requireContext(), binding.etSearch)
                }
                R.id.delete -> {
                    if (position != -1) {
                        deletedPosition = position
                        if (rackListAdapter?.getAdapterData()?.get(position)?.id.isNullOrEmpty()) {
                            rackListAdapter?.getAdapterData()?.removeAt(deletedPosition)
                            listOfRacks.removeAt(deletedPosition)
                            rackListAdapter?.notifyItemRemoved(deletedPosition)
                        } else {
                            deleteRack(position)
                        }
                    }
                }
                R.id.add_photo -> {
                    (binding.rvAddRack.adapter as? InventoryAdapter)?.let {
                        openAddImageView(it.getAdapterData()[position].name ?: "", position)
                    }
                }
                R.id.download_monthly_report -> {
                    viewModelTab.downloadInventoryCSV()
                }

                R.id.inventory_bulk_upload -> {
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.INVENTORY_BULK_UPLOAD))
                }

            }
            true
        }
        popupMenu.show()
    }


    private fun deleteRack(position: Int) {
        lateinit var dialog: AlertDialog
        requireActivity().let {
            val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        deletedRackId =
                            rackListAdapter?.getAdapterData()?.get(position)?.id.toString()
                        showLoader()
                        viewModelTab.deleteInventoryRack(deletedRackId)
                    }
                }
            }
            AlertDialog.Builder(it).apply {
                setIcon(android.R.drawable.ic_dialog_alert)
                setTitle(getString(R.string.delete))
                setMessage(getString(R.string.rack_delete_confirm))
                setPositiveButton(getString(R.string.delete), dialogClickListener)
                setNeutralButton(getString(R.string.cancel), dialogClickListener)
                dialog = create()
                dialog.show()
            }
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is RackUpdateEvent -> {
                showLoader()
                viewModelTab.getInventoryRack()
            }
        }
    }

    private fun openAddImageView(searchString: String, position: Int) {
        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.CHOOSE_IMAGE, Bundle().apply {
            putString(AppENUM.SEARCH_STRING, searchString)
            putBoolean(AppENUM.SINGLE_IMAGE, true)

        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                (extra as? Uri)?.let { uri ->
                    if (uri == Uri.EMPTY) {
                        positionOFImageToBeUploaded = position
                        ImagePicker.with(this@InventoryFragment).compress(512)
                            .maxResultSize(512, 512).setImageProviderInterceptor {
                                showLoader()
                            }.createIntent { intent ->
                                startForProfileImageResult.launch(intent)
                            }
                    } else {
                        rackListAdapter?.getAdapterData()?.get(position).apply {
                            this?.isNewRack = true
                            this?.images?.add(0, uri.toString())
                            rackListAdapter?.notifyItemChanged(position)
                        }
                    }
                }
            }
        })?.let { baseFragment ->
            baseFragment.show(requireActivity().supportFragmentManager, baseFragment.javaClass.name)
        }
    }


    private fun fireFeatureLockedEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_INVENTORY)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.ERROR_HEADER_PRO,
            bundle)

    }
}