package whitelisted.garage.view.fragments.home.goCoins.goCoinsTransactions

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.GoCoinTransactionsItem
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentGoCoinTransactionsBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeBackgroundColor
import whitelisted.garage.utils.CommonUtils.changeBackgroundDrawable
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.GCTransactionsAdapter
import whitelisted.garage.viewmodels.GoCoinTransactionsViewModel

class GoCoinsTransactionsFragment : BaseFragment(), AdapterView.OnItemSelectedListener {

    private val binding by viewBinding(FragmentGoCoinTransactionsBinding::inflate)
    private val viewModel: GoCoinTransactionsViewModel by viewModel()
    private val filterList = AppENUM.FILTER_LIST_REMINDER
    private var selectedType = 0
    private var selectedTypeString = AppENUM.FilterString.ALL
    private var periodType = filterList[0]
    private var isSpinnerActive = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpData()
        setupScreen()
    }

    private fun setUpData() {
        val adapter = ArrayAdapter(requireContext(),
            R.layout.layout_drop_down,
            resources.getStringArray(R.array.rem_filter_list).toMutableList())
        binding.transFilterSpinner.adapter = adapter

        showLoader()
        viewModel.getTransactions(selectedTypeString, periodType)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupScreen() {
        binding.imgBack.setOnClickListener(this)
        binding.tvAll.setOnClickListener(this)
        binding.tvDownloadSheet.setOnClickListener(this)
        binding.tvCredit.setOnClickListener(this)
        binding.tvDebit.setOnClickListener(this)
        binding.transFilterSpinner.onItemSelectedListener = this
        binding.transFilterSpinner.setOnTouchListener { _, _ ->
            isSpinnerActive = true
            false
        }

        observeTransactionsResponse()
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.tvAll -> updateSelectorAndGetList(0)
            R.id.tvCredit -> updateSelectorAndGetList(1)
            R.id.tvDebit -> updateSelectorAndGetList(2)
            R.id.tvDownloadSheet -> {
                viewModel.downloadCSV(selectedTypeString, periodType)
            }
        }
    }

    private fun updateSelectorAndGetList(type: Int) {
        when (type) {
            1 -> {
                if (selectedType != 1) {
                    selectedType = 1
                    selectedTypeString = AppENUM.FilterString.CREDIT
                    binding.tvCredit.changeBackgroundDrawable(requireContext(),
                        R.drawable.bg_selected_gray_2)
                    binding.tvDebit.changeBackgroundColor(requireContext(), R.color.white)
                    binding.tvAll.changeBackgroundColor(requireContext(), R.color.white)
                }
            }
            2 -> {
                if (selectedType != 2) {
                    selectedType = 2
                    selectedTypeString = AppENUM.FilterString.DEBIT
                    binding.tvDebit.changeBackgroundDrawable(requireContext(),
                        R.drawable.bg_selected_gray_2)
                    binding.tvCredit.changeBackgroundColor(requireContext(), R.color.white)
                    binding.tvAll.changeBackgroundColor(requireContext(), R.color.white)
                }
            }
            0 -> {
                if (selectedType != 0) {
                    selectedType = 0
                    selectedTypeString = AppENUM.FilterString.ALL
                    binding.tvAll.changeBackgroundDrawable(requireContext(),
                        R.drawable.bg_selected_gray_2)
                    binding.tvDebit.changeBackgroundColor(requireContext(), R.color.white)
                    binding.tvCredit.changeBackgroundColor(requireContext(), R.color.white)
                }
            }
        }
        showLoader()
        viewModel.getTransactions(selectedTypeString, periodType)

    }

    private fun observeTransactionsResponse() {
        viewModel.provideTransactionsResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.transactions?.let { it1 -> setAdapter(it1) }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

        viewModel.provideDownloadTransCSVResponse().observe(viewLifecycleOwner) {
            CommonUtils.showToast(requireContext(), getString(R.string.downloading), true)
            CommonUtils.saveToDisk(requireActivity(),
                it,
                "${viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME)}-GoCoinTransaction.csv",
                "${viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME)}-GoCoinTransaction.csv",
                getString(R.string.download_order_history_csv))
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setAdapter(data: List<GoCoinTransactionsItem>) {
        binding.tvDownloadSheet.makeVisible(true)
        binding.tvTotalTras.text = "${data.size}  ${getString(R.string.transactions)}"
        binding.rvTransactionHistory.adapter = GCTransactionsAdapter(data.toMutableList())
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        if (isSpinnerActive) {
            isSpinnerActive = false
            periodType = filterList[position]
            showLoader()
            viewModel.getTransactions(selectedTypeString, periodType)
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) { //
    }
}