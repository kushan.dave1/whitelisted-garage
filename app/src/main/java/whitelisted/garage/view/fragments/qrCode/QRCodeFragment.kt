package whitelisted.garage.view.fragments.qrCode

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import com.google.zxing.WriterException
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.request.SaveUPIIdRequest
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentQrCodeBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.viewmodels.QRCodeFragmentViewModel
import java.util.*

class QRCodeFragment : BaseFragment() {

    private val binding by viewBinding(FragmentQrCodeBinding::inflate)
    private val viewModel: QRCodeFragmentViewModel by viewModel()

    private var UPI_ID: String = ""

    private var isFirstTime = false

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun setupScreen() {

        binding.imgBack.setOnClickListener(this)
        binding.btnEdit.setOnClickListener(this)
        observeGetUiResponse()
        observeSaveUiResponse()
        showLoader()
        viewModel.getUPIId()

    }


    @RequiresApi(Build.VERSION_CODES.P)
    private fun observeGetUiResponse() {
        viewModel.provideGetUPIResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.upi_id != null) {
                        UPI_ID = it.body.data.upi_id
                        setupQRCode()
                    } else {
                        isFirstTime = true
                        showEnterUpiIdDialog()
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeSaveUiResponse() {
        viewModel.provideSaveUPIResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    showLoader()
                    viewModel.getUPIId()
                    if (isFirstTime) isFirstTime = false
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()

            R.id.btnEdit -> {
                showEnterUpiIdDialog()
            }


        }
    }

    private fun showEnterUpiIdDialog() {
        activity?.let {
            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.EDIT_UPI_ID,
                Bundle().apply {
                    putString(AppENUM.UPI_ID, UPI_ID)
                    putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_QRCODE)
                },
                object : ActionListener {
                    @RequiresApi(Build.VERSION_CODES.P)
                    override fun onActionItem(extra: Any?, extra2: Any?) {
                        UPI_ID = extra as String
                        showLoader()
                        firebaseEventSAVEQR(UPI_ID)
                        viewModel.saveUPIId(SaveUPIIdRequest(UPI_ID), isFirstTime)

                    }
                })?.let { baseFragment ->
                baseFragment.show(it.supportFragmentManager, baseFragment.javaClass.name)
            }
        }
    }

    private fun firebaseEventSAVEQR(qr: String) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_QRCODE)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.UPI_ID, qr)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_SAVE_QR,
                this)
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun setupQRCode() {
        binding.tvUpiId.text = UPI_ID
        binding.tvWorkshopName.text =
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME)

        // Sending side
        //        var base64Certificate = ""
        //        try {
        //            val info: PackageInfo = requireContext().packageManager.getPackageInfo(
        //                "whitelisted.garage",
        //                PackageManager.GET_SIGNING_CERTIFICATES
        //            )
        //            for (signature in info.signingInfo.apkContentsSigners) {
        //                val md = MessageDigest.getInstance("SHA")
        //                md.update(signature.toByteArray())
        //                Timber.tag(
        //                    "KeyHash:" + encodeToString(
        //                        md.digest(),
        //                        android.util.Base64.DEFAULT
        //                    )
        //                )
        //                base64Certificate = encodeToString(
        //                    md.digest(),
        //                    android.util.Base64.DEFAULT
        //                )
        //            }
        //        } catch (e: NameNotFoundException) {
        //        } catch (e: NoSuchAlgorithmException) {
        //        }
        val data: ByteArray
        if (BuildConfig.DEBUG) {
            data = BuildConfig.SHA_256.encodeToByteArray()
        } else {
            data = BuildConfig.SHA_256.encodeToByteArray()
        }
        val base64: String = Base64.getEncoder().encodeToString(data)
        val url = "upi://pay?pa=" +   // payment method.
                UPI_ID +         // VPA number.
                //                "&am="+ TOTAL_AMOUNT +       // this param is for fixed amount (non editable).
                "&pn=${binding.tvWorkshopName.text}" +      // to showing your name in app.
                "&cu=INR" +                  // Currency code.
                "&mode=02" +                 // mode O2 for Secure QR Code.
                "&orgid=189999" +            //If the transaction is initiated by any PSP app then the respective orgID needs to be passed.
                "&sign=" + base64

        try {
            val bitmap = CommonUtils.textToImageEncode(url)
            binding.imgQRCode.setImageBitmap(bitmap)
            binding.imgQRCode.invalidate()
        } catch (e: WriterException) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()


    }
}