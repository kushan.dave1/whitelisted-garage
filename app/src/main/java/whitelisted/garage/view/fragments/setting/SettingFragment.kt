package whitelisted.garage.view.fragments.setting

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import androidx.lifecycle.lifecycleScope
import com.an.biometric.BiometricCallback
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.App
import whitelisted.garage.R
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentSettingBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.activities.DashboardActivity
import whitelisted.garage.view.adapter.SettingAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.SettingsFragmentViewModel
import whitelisted.garage.viewmodels.WalkthroughSharedViewModel

class SettingFragment : BaseFragment(), CompoundButton.OnCheckedChangeListener {

    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private val walkThroughViewModel: WalkthroughSharedViewModel by sharedViewModel()
    private val viewModel: SettingsFragmentViewModel by viewModel()
    private val binding by viewBinding(FragmentSettingBinding::inflate)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setObservers()
        setupScreen()
        setClickListeners()
    }

    private fun setClickListeners() {
        binding.backArrowImg.setOnClickListener(this)
        binding.switchFingerprint.setOnCheckedChangeListener(this)
        binding.btnSave.setOnClickListener(this)
        binding.imgPasscodeEdit.setOnClickListener(this)
    }

    private fun setupScreen() = binding.apply {
        rvSetting.adapter =
            SettingAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                AppENUM.RefactoredStrings.defaultCurrency), this@SettingFragment)
        etPasscode.isEnabled = false
        if (dashboardViewModel.getBooleanSharedPreference(AppENUM.BIOMETRIC_ENABLED, false)) {
            switchFingerprint.isChecked = true
            viewModel.getSavedPin()
            llPasscode.makeVisible(true)
            imgPasscodeEdit.makeVisible(true)
        }
    }

    private fun setObservers() {
        observeWalkThrough()
        viewModel.provideGetPinResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    binding.etPasscode.setText(it.body.data.pin)
                }
                is Result.Failure -> { // nothing
                }
            }
        }
        viewModel.provideSetPinResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    dashboardViewModel.putBooleanSharedPreference(AppENUM.BIOMETRIC_ENABLED, true)
                    binding.btnSave.makeVisible(false)
                    binding.etPasscode.isEnabled = false
                    binding.imgPasscodeEdit.makeVisible(true)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun observeWalkThrough() {
        walkThroughViewModel.provideSettingsWalkThroughEvent().observe(viewLifecycleOwner) {
            when (it) {
                9 -> {
                    lifecycleScope.launch {
                        delay(200)
                        startWalkthrough(binding.rvSetting.getChildAt(0))
                    }
                }

                20 -> {
                    if (!isInternationalUser) {
                        if (binding.rvSetting.childCount > 2) {
                            startWalkthrough(binding.rvSetting.getChildAt(2))
                        }
                    } else {
                        if (binding.rvSetting.childCount > 1) {
                            startWalkthrough(binding.rvSetting.getChildAt(1))
                        }
                    }
                }

                21 -> {
                    if (binding.rvSetting.childCount > 5) {
                        startWalkthrough(binding.rvSetting.getChildAt(5))
                    }
                }

                22 -> {
                    if (binding.rvSetting.childCount > 3) {
                        startWalkthrough(binding.rvSetting.getChildAt(3))
                    }
                }

                23 -> {
                    if (binding.rvSetting.childCount > 7) {
                        startWalkthrough(binding.rvSetting.getChildAt(7))
                    }
                }
            }
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.backArrowImg -> popBackStackAndHideKeyboard()
            R.id.btnSave -> binding.apply {
                if (switchFingerprint.isChecked) {
                    if (etPasscode.text.toString().trim()
                            .isEmpty() || etPasscode.text.toString().length < 4) {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.alert_invalid_passcode))
                    } else {
                        showLoader()
                        viewModel.setPin(etPasscode.text.toString())
                    }
                }
            }
            R.id.imgPasscodeEdit -> binding.apply {
                etPasscode.isEnabled = true
                etPasscode.requestFocus()
                etPasscode.setSelection(etPasscode.text.toString().length)
                btnSave.makeVisible(true)
                imgPasscodeEdit.makeVisible(false)
            }
            R.id.settingItemParent -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let {
                    if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                            AppENUM.RefactoredStrings.defaultCurrency) != AppENUM.RefactoredStrings.defaultCurrency) {
                        if (it >= 1) {
                            settingRvNavigation(it + 2)
                        } else settingRvNavigation(it)
                    } else settingRvNavigation(it)
                }
            }
        }
    }

    private fun settingRvNavigation(position: Int) {
        when (position) {
            0 -> {
                CommonUtils.addFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.PROFILE, null),
                    target = R.id.fragment_container_2)
            }
            1 -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.LANGUAGE_SELECTION_FRAGMENT,
                        null),
                    target = R.id.fragment_container_2)
            }
            2 -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.QR_CODE, null),

                    )
                setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_QR,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SETTING)
            }
            3 -> {
                Bundle().apply {
                    putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SETTING)
                    FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_TAP_SUPPORT,
                        this)

                }
                sendEmail(AppENUM.HELP_SUPPORT_MAIL,
                    viewModel.getStringSharedPreference(AppENUM.USER_ID, ""))
            }
            4 -> {
                Bundle().apply {
                    putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SETTING)
                    FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_DOWNLOAD_LATEST,
                        this)
                }
                openPlayStore()
            }
            5 -> {
                CommonUtils.addFragmentUtil(activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.FEEDBACK_FRAGMENT, null),
                    target = R.id.fragment_container_2)
                setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_FEEDBACK,
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SETTING)
            }
            6 -> {
                Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT,
                        getString(R.string.share_app_text) + AppENUM.APP_PLAY_STORE_URL)
                    type = "text/plain"
                    startActivity(this)
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_FEEDBACK,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SETTING)
                }
            }
            7 -> {
                Intent(requireActivity(), DashboardActivity::class.java).apply {
                    this.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    dashboardViewModel.putBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN, false)
                    dashboardViewModel.isUserWalkthroughStarted = true
                    (requireActivity().application as? App)?.reStartModule()
                    requireActivity().startActivity(this)
                }
            } //            8 -> {
            //                CommonUtils.addFragmentUtil(requireActivity(), FragmentFactory.fragment(FragmentFactory.Fragments.MANAGE_EMPLOYEE))
            //                setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_MANAGE_EMP, FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
            //            }
        }
    }

    override fun onCheckedChanged(p0: CompoundButton?, isChecked: Boolean) {
        if (isChecked) {
            viewModel.launch(Dispatchers.Main) {
                delay(300)
                checkBiometric()
            }
        } else {
            viewModel.putBooleanSharedPreference(AppENUM.BIOMETRIC_ENABLED, false)
            binding.llPasscode.makeVisible(false)
        }
    }

    private fun checkBiometric() = binding.apply {
        com.an.biometric.BiometricManager.BiometricBuilder(requireContext())
            .setTitle(getString(R.string.app_name))
            .setSubtitle(getString(R.string.enter_fingerprint_to_secure_app_login))
            .setNegativeButtonText(getString(R.string.cancel)).build()
            .authenticate(object : BiometricCallback {
                override fun onSdkVersionNotSupported() {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.android_versionn_not_supp),
                        true)
                    switchFingerprint.isChecked = false
                }

                override fun onBiometricAuthenticationNotSupported() {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.bio_metric_not_sup),
                        true)
                    switchFingerprint.isChecked = false
                }

                override fun onBiometricAuthenticationNotAvailable() {
                    CommonUtils.run {
                        showToast(requireContext(), getString(R.string.bio_metriic_nnot_reg), true)
                    }
                    switchFingerprint.isChecked = false
                }

                override fun onBiometricAuthenticationPermissionNotGranted() {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.bio_metric_auth_not_granted),
                        true)
                    switchFingerprint.isChecked = false
                }

                override fun onBiometricAuthenticationInternalError(error: String?) { // nothing
                }

                override fun onAuthenticationFailed() {
                    switchFingerprint.isChecked = false
                }

                override fun onAuthenticationCancelled() {
                    switchFingerprint.isChecked = false
                }

                override fun onAuthenticationSuccessful() {
                    llPasscode.makeVisible(true)
                    btnSave.makeVisible(true)
                    etPasscode.isEnabled = true
                    etPasscode.requestFocus()
                    etPasscode.setSelection(etPasscode.text.toString().length)
                }

                override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence?) {
                    Log.d(helpString.toString(), helpCode.toString())
                    switchFingerprint.isChecked = false
                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
                    Log.d(errString.toString(), errorCode.toString())
                    switchFingerprint.isChecked = false
                }
            })
    }
}
