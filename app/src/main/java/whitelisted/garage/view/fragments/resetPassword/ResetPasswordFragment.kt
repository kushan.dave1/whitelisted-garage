package whitelisted.garage.view.fragments.resetPassword

import android.os.Bundle
import android.view.View
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentResetPasswordBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.viewmodels.LoginViewModel

class ResetPasswordFragment : BaseFragment() {

    private val binding by viewBinding2(FragmentResetPasswordBinding::inflate)
    private val viewModel: LoginViewModel by sharedViewModel()
    private var isForgot = false
    private lateinit var forgotMobile: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        if (arguments?.getBoolean(AppENUM.IntentKeysENUM.IS_FORGOT) == true) {
            binding?.tvHeader?.text = getString(R.string.forgot_password)
            isForgot = true
            forgotMobile = arguments?.getString(AppENUM.IntentKeysENUM.MOBILE_NUMBER).toString()
        }
        setObservers()
        clickListeners()
    }

    private fun clickListeners() {
        binding?.imgBack?.setOnClickListener(this)
        binding?.btnReset?.setOnClickListener(this)
    }

    private fun setObservers() {
        observeResetPasswordResponse()
    }

    private fun observeResetPasswordResponse() {
        viewModel.provideResetPasswordOtpResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(activity, it.body.message)
                    removeAllFragments()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage, true)
                }
            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()

            R.id.btnReset -> {
                CommonUtils.hideKeyboard(activity)
                if (checkForm()) {
                    showLoader()
                    viewModel.callResetPasswordAPI(forgotMobile, binding?.etPassword?.text.toString())
                }
            }
        }
    }

    private fun checkForm(): Boolean {
        if (binding?.etPassword?.text.toString().isEmpty()) {
            CommonUtils.showToast(activity, getString(R.string.alert_empty_password))
            binding?.etPassword?.requestFocus()
            return false
        } else if (!CommonUtils.isPasswordValid(binding?.etPassword?.text.toString())) {
            CommonUtils.showToast(activity, getString(R.string.alert_invalid_password))
            binding?.etPassword?.requestFocus()
            return false
        }
        if (binding?.etConfirmPassword?.text.toString().isEmpty()) {
            CommonUtils.showToast(activity, getString(R.string.alert_empty_confirm_password))
            binding?.etConfirmPassword?.requestFocus()
            return false
        } else if (binding?.etPassword?.text.toString() != binding?.etConfirmPassword?.text.toString()) {
            CommonUtils.showToast(activity, getString(R.string.alert_password_mismatch))
            binding?.etConfirmPassword?.requestFocus()
            return false
        }
        return true
    }

    override fun onResume() {
        super.onResume()


    }
}