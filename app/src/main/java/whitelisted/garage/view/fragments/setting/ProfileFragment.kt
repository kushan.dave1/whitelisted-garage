package whitelisted.garage.view.fragments.setting

import android.Manifest
import android.net.Uri
import android.os.Bundle
import android.view.View
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import gun0912.tedimagepicker.builder.TedImagePicker
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.ProfileItemData
import whitelisted.garage.api.request.ProfileNavigationData
import whitelisted.garage.api.request.SaveProfileCompletionRequest
import whitelisted.garage.api.response.login.GetProfileCompletion
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentProfileBinding
import whitelisted.garage.eventBus.ProfileUpdateEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.ProfileAdapter
import whitelisted.garage.viewmodels.ProfileFragmentViewModel

class ProfileFragment : BaseFragment() {

    private val binding by viewBinding(FragmentProfileBinding::inflate)
    private var userData: GetProfileCompletion? = null
    private val viewModel: ProfileFragmentViewModel by viewModel()
    private lateinit var profilePicture: Uri
    private val accDetailsImage = listOf(R.drawable.ic_profile_owner,
        R.drawable.ic_profile_phone,
        R.drawable.ic_profile_email,
        R.drawable.ic_profile_joninng_date,
        R.drawable.ic_profile_role)
    private val accDetailsHeader = listOf(
        R.string.owner_name,
        R.string.phone_number,
        R.string.e_mail,
        R.string.joining_date_norm,
        R.string.role,
    )
    private val workshopDetailsImage = listOf(
        R.drawable.ic_profile_workship_name,
        R.drawable.ic_profile_workshop_add,
        R.drawable.ic_profile_workshop_gst,
    )
    private val workshopDetailsHeader = listOf(
        R.string.workshop_name,
        R.string.workshop_address,
        R.string.gst_number,
    )
    private val accENUM = listOf(AppENUM.Profile.OWNER_NAME,
        AppENUM.Profile.PHONE_NUMBER,
        AppENUM.Profile.EMAIL,
        AppENUM.Profile.JOINING_DATE,
        AppENUM.Profile.ROLE)
    private val workshopENUM =
        listOf(AppENUM.Profile.WORKSHOP_NAME, AppENUM.Profile.WORKSHOP_ADD, AppENUM.Profile.GSTN)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        observeData()
        clickListener()
        setUpData()
        showLoader()
        viewModel.getProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID, ""))
    }

    private fun observeData() {
        viewModel.provideProfileResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    userData = it.body.data
                    setUpDataProfileData(it.body.data)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
        viewModel.provideUploadWorkshopImageResponse()
            .observe(viewLifecycleOwner) { sendSuccessUrl ->
                hideLoader()
                sendSuccessUrl.isSuccessFailure?.let { it ->
                    if (it) {
                        sendSuccessUrl.url?.let {
                            showLoader()
                            viewModel.updateProfileCompletion(viewModel.getStringSharedPreference(
                                AppENUM.USER_ID,
                                ""), SaveProfileCompletionRequest().apply {
                                image = it
                            })
                        }
                    } else {
                        sendSuccessUrl.url?.let { it1 ->
                            CommonUtils.showToast(requireContext(), it1, true)
                        }
                    }
                }
            }
        viewModel.provideUpdateProfileResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success<*> -> {
                    it.body.let {
                        EventBus.getDefault().post(ProfileUpdateEvent())
                    }
                }
                is Result.Failure<*> -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
    }

    private fun setUpDataProfileData(data: GetProfileCompletion) = binding.apply {
        data.userData?.let { userData ->
            if (data.userData?.image.isNullOrEmpty()) {
                imageNoProfileAlert.makeVisible(true)
                lblUploadPhoto.changeTextColor(requireContext(), R.color.red_text_color)
            } else if (isAdded) {
                lblUploadPhoto.changeTextColor(requireContext(), R.color.colorAccent)
                imageNoProfileAlert.makeVisible(false)
                loadCircularImage(userData.image ?: "", imgProfilePicture)
            }
            setUpAccountRvDetails(mutableListOf<String>().apply {
                add(userData.name ?: "")
                add((userData.mobile).toString())
                add(userData.email ?: "")
                add(CommonUtils.convertToDate(userData.joiningDate) ?: "")
                add(userData.role ?: "")
            })
            setUpWorkshopDetails(mutableListOf<String>().apply {
                add(userData.workshopName ?: "")
                add(userData.address ?: "")
                add(userData.gstin ?: "")
            })
        }
    }

    private fun setUpWorkshopDetails(apply: MutableList<String>) {
        (binding.rvWorkShopDetails.adapter as? ProfileAdapter)?.setData(ProfileItemData().apply {
            this.dataListHeader = workshopDetailsHeader
            this.image = workshopDetailsImage
            this.dataListDes = apply
            this.dataListConstant = workshopENUM
        })
    }

    private fun setUpAccountRvDetails(apply: MutableList<String>) {
        (binding.rvAccountDetails.adapter as? ProfileAdapter)?.setData(ProfileItemData().apply {
            this.dataListHeader = accDetailsHeader
            this.image = accDetailsImage
            this.dataListDes = apply
            this.dataListConstant = accENUM
        })
    }

    private fun setUpData() = binding.apply  {
        rvWorkShopDetails.adapter = ProfileAdapter(this@ProfileFragment,
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE, ""))
        rvAccountDetails.adapter = ProfileAdapter(this@ProfileFragment,
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE, ""))
        (rvAccountDetails.adapter as? ProfileAdapter)?.setData(ProfileItemData().apply {
            this.dataListHeader = accDetailsHeader
            this.image = accDetailsImage
            this.dataListConstant = accENUM
        })
        (rvWorkShopDetails.adapter as? ProfileAdapter)?.setData(ProfileItemData().apply {
            this.dataListHeader = workshopDetailsHeader
            this.image = workshopDetailsImage
            this.dataListConstant = workshopENUM
        })
    }

    private fun clickListener() {
        binding.backArrowImg.setOnClickListener(this)
        binding.imgProfilePicture.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.backArrowImg -> popBackStackAndHideKeyboard()
            R.id.llParentView -> {
                CommonUtils.genericCastOrNull<ProfileNavigationData>(v.getTag(R.id.llParentView))
                    ?.let {
                        CommonUtils.addFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.EDIT_PROFILE,
                                Bundle().apply {
                                    this.putParcelable(AppENUM.Profile.ROLE, it)
                                }))
                    }
            }
            R.id.imgProfilePicture -> {
                TedPermission.create().setPermissionListener(object : PermissionListener {
                    override fun onPermissionGranted() {
                        TedImagePicker.with(requireContext()).showCameraTile(true).start {
                            onImagePicked(it)
                        }
                    }

                    override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
                        CommonUtils.showToast(context,
                            resources.getString(R.string.permission_denied))
                    }

                }).setPermissions(Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE).check()
            }
        }
    }

    private fun onImagePicked(imageUri: Uri?) {
        imageUri?.let {
            profilePicture = it
            loadCircularImage(profilePicture.toString(), binding.imgProfilePicture)
            showLoader()
            viewModel.uploadProfilePicture(requireContext(),
                profilePicture,
                viewModel.getStringSharedPreference(AppENUM.USER_ID, ""))
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is ProfileUpdateEvent -> {
                showLoader()
                viewModel.getProfileCompletion(viewModel.getStringSharedPreference(AppENUM.USER_ID,
                    ""))
            }
        }
    }
}