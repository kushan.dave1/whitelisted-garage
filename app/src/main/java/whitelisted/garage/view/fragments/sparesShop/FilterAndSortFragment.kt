package whitelisted.garage.view.fragments.sparesShop

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.*
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.*
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.lower
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

@SuppressLint("NotifyDataSetChanged")
class FilterAndSortFragment : BaseFragment() {

    private val binding by dataBinding<FragmentSparesFilterAndSortBinding>(R.layout.fragment_spares_filter_and_sort)
    private val viewModel: SparesShopFragmentViewModel by sharedViewModel()
    private var sortedFilters = mutableListOf<MarketplaceFilter>()
    private var filters: SearchAndFilterAttrs? = null
    private var lastSelectedFilterPos = -1
    private var openCategoryPosition = -1
    private var selectedFilters: String = ""
    private var categoriesSearch = ""
    private var selectedCategories = mutableListOf<String>()
    private var isBrandsChanged = false
    private var isBrandSelectedInitially = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        getFilters()
        setupClickListeners()
    }

    private fun setupClickListeners() {
        binding.tvClearFSFS.setOnClickListener(this)
        binding.applyBottom.setOnClickListener(this)
        binding.imgBackFSFS.setOnClickListener(this)

        binding.etSearchCategories.doOnTextChanged { text, _, _, _ ->
            if (sortedFilters[lastSelectedFilterPos].filterKey == "category_name") {
                openCategoryPosition = -1
                categoriesSearch = text.toString()
                setCategoriesAdapter()
            }
        }
    }

    private fun setupViews() {
        binding.rvFilters.layoutManager = LinearLayoutManager(requireContext())
        binding.rvFilterAttrs.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun getFilters() = viewLifecycleOwner.lifecycleScope.launchWhenStarted {
        showLoader()
        saveSelectedFilters()
        filters = viewModel.getSearchAndFilterAttributes()

        filters?.let { allFilters ->

            allFilters.categories.forEach { c ->
                c.subCategory.forEach { sc ->
                    val categories = sc.subArray.filter { it.isSelected }.map { it.displayName }
                    selectedCategories += categories
                    if (categories.isNotEmpty()) sc.isSelected = true
                }
            }

            sortedFilters.add(
                MarketplaceFilter(
                    filterKey = "category_name",
                    name = "Category",
                    selectionCount = selectedCategories.size
                )
            )

            val brandTypes = getBrandsAccToSelectedCategories()

            sortedFilters.add(MarketplaceFilter(
                filterKey = "brand_name",
                name = "Brands",
                isMultiple = true,
                filterTypes = brandTypes,
                selectionCount = brandTypes.count { it.isSelected }
            ))

            isBrandSelectedInitially = allFilters.brands.any { it.isSelected }
            sortedFilters.addAll(allFilters.filters)

        }


        binding.rvFilters.adapter = CommonUtils.recyclerAdapter(
            sortedFilters, R.layout.card_filter_item, ::bindFilterItem
        )

        val isBrand = arguments?.getBoolean(AppENUM.RefactoredStrings.IS_BRAND) ?: false
        if (sortedFilters.isNotEmpty()) onFilterItemSelected(if (isBrand) 1 else 0)
        hideLoader()
    }

    override fun handleBackPress(): Boolean {
        getPreviouslySelectedFilters()
        return super.handleBackPress()
    }

    private fun saveSelectedFilters() {
        selectedFilters = Gson().toJson(viewModel.searchAndFilterAttrs)
    }

    private fun getPreviouslySelectedFilters() {
        try {
            filters = Gson().fromJson(selectedFilters, SearchAndFilterAttrs::class.java)
            viewModel.reset(filters)
        } catch (e: JsonSyntaxException) {
            e.printStackTrace()
        }

    }

    private fun bindFilterItem(cardBinding: CardFilterItemBinding, currentPos: Int) {

        val filter = sortedFilters[currentPos]
        cardBinding.filterName.text = filter.name
        cardBinding.filterName.setOnClickListener { onFilterItemSelected(currentPos) }
        val bgColor =
            if (lastSelectedFilterPos == currentPos) R.color.white else android.R.color.transparent
        cardBinding.root.setBackgroundColor(ContextCompat.getColor(requireContext(), bgColor))

        if (filter.selectionCount > 0) {
            cardBinding.filterCount.text = filter.selectionCount.toString()
            cardBinding.filterCount.makeVisible(true)
        } else cardBinding.filterCount.makeVisible(false)

    }

    private fun onFilterItemSelected(currentPos: Int) {

        val oldPos = lastSelectedFilterPos
        lastSelectedFilterPos = currentPos
        binding.rvFilters.adapter?.notifyItemChanged(oldPos)
        binding.rvFilters.adapter?.notifyItemChanged(lastSelectedFilterPos)

        val filter = sortedFilters[currentPos]

        if (filter.filterKey == "category_name") setCategoriesAdapter()
        else binding.rvFilterAttrs.clear().addViews(
            sortedFilters[currentPos].filterTypes,
            CardFilterItemAtrBinding::inflate,
            ::bindFilterItemAttribute
        )
            .also {
                binding.etSearchCategories.setText("")
                binding.etSearchCategories.makeVisible(false)
            }
    }

    private fun bindFilterItemAttribute(
        attrBinding: CardFilterItemAtrBinding,
        filterType: FilterType,
        currentPos: Int
    ) {
        val filter = sortedFilters[lastSelectedFilterPos]

        val isMultiple = filter.isMultiple ?: false
        attrBinding.tvFilterAttrName.text = filterType.name ?: ""

        attrBinding.tvFilterAttrCheckbox.makeVisible(isMultiple)
        attrBinding.tvFilterAttrRadioBtn.makeVisible(!isMultiple)

        attrBinding.flParent.setOnClickListener {
            val isSelected = filterType.isSelected
            attrBinding.tvFilterAttrCheckbox.isChecked = !isSelected
            attrBinding.tvFilterAttrRadioBtn.isChecked = !isSelected
            filterType.isSelected = !isSelected

            when {
                isSelected -> filterType.isSelected = false

                !isMultiple -> filter.filterTypes.forEachIndexed { index, filterType ->
                    filterType.isSelected = currentPos == index
                }

                else -> filterType.isSelected = true
            }

            binding.rvFilterAttrs.adapter?.notifyDataSetChanged()

            var count = 0

            filter.filterTypes.forEach { ft ->
                if (ft.isSelected) count++
            }

            if (filter.selectionCount != count) {
                filter.selectionCount = count
                binding.rvFilters.adapter?.notifyItemChanged(lastSelectedFilterPos)
            }

            if (filter.filterKey == "brand_name") {
                val brand = filters?.brands?.find { it.name == filterType.name }
                brand?.isSelected = filterType.isSelected
                if (filterType.isSelected) isBrandsChanged = true
                else isBrandSelectedInitially = false
            }

        }

        attrBinding.tvFilterAttrCheckbox.isChecked = filterType.isSelected == true
        attrBinding.tvFilterAttrRadioBtn.isChecked = filterType.isSelected == true
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.tvClearFSFS -> clearFilter()
            R.id.applyBottom -> applyFilters()
            R.id.imgBackFSFS -> popBackStackAndHideKeyboard()
        }
    }

    private fun applyFilters() {
        if (!filters?.textSearch.isNullOrEmpty()) {
            showConfirmationDialog(); return
        }
        filters?.brandCount = filters?.brands?.count { it.isSelected } ?: 0
        filters?.categoryCount = selectedCategories.size
        viewModel.updateFilters()
        fireFilterSortEvent()
        popBackStackAndHideKeyboard()
    }

    private fun showConfirmationDialog() {
        val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    filters?.textSearch = ""
                    applyFilters()
                }
                DialogInterface.BUTTON_NEGATIVE -> {
                    dialog.cancel()
                }
            }
        }

        val builder = AlertDialog.Builder(context)
        builder.setMessage(resources.getString(R.string.clear_warning))
            .setPositiveButton("Proceed", dialogClickListener)
            .setNegativeButton("Cancel", dialogClickListener)
            .show()

    }

    private fun clearFilter() {

        filters?.apply {
            categories.forEach { c ->
                c.subCategory.forEach { sc ->
                    sc.isSelected = false
                    sc.subArray.forEach { sa ->
                        sa.isSelected = false
                    }
                }
                c.isSelected = false
            }

            brandCount = 0; categoryCount = 0
            selectedCategories.clear()
        }

        sortedFilters.forEach {
            it.filterTypes.forEach { ft ->
                ft.isSelected = false
            }
            it.selectionCount = 0
        }
        resetBrands(true)
        binding.rvFilters.adapter?.notifyDataSetChanged()
        onFilterItemSelected(lastSelectedFilterPos)
    }

    private fun setCategoriesAdapter() = filters?.let {


        binding.etSearchCategories.makeVisible(true)
        binding.rvFilterAttrs.clear().addViews(
            search(),
            FilterAttrItemBinding::inflate,
            ::bindCategories
        )
    }

    private fun bindCategories(
        binding: FilterAttrItemBinding,
        model: Category, adapterPosition: Int
    ) {

        binding.tvCategoryName.text = model.categoryName
        binding.rvSubAttr.setLayoutManagerAsLinear()
        val selected =
            if (categoriesSearch.isNotEmpty()) true else openCategoryPosition == adapterPosition
        binding.btnOpen.rotation = if (selected) 180f else 0f
        //binding.btnOpen.animate().rotation().setDuration(500).start()
        binding.rvSubAttr.makeVisible(selected)
        binding.divider.makeVisible(!selected)
        binding.tvCategoryName.setTextColor(
            resources.getColor(
                if (selected) R.color.colorAccent
                else R.color.text_color_21, resources.newTheme()
            )
        )

        binding.tvCategoryName.typeface = ResourcesCompat.getFont(
            requireContext(),
            if (selected) R.font.gilroy_semibold
            else R.font.gilroy_medium
        )

        binding.vBtn.setOnClickListener {
            val oldSelectedPos = openCategoryPosition
            openCategoryPosition = if (selected) -1 else adapterPosition
            categoriesSearch = ""
            this.binding.rvFilterAttrs.notifyAt(adapterPosition)
            if (oldSelectedPos != -1) this.binding.rvFilterAttrs.notifyAt(oldSelectedPos)
        }

        val map = mutableMapOf<SubCategory, List<SubArray>>()
        model.subCategory.forEach { sc ->
            map[sc] = sc.subArray
        }

        binding.rvSubAttr.clear().addViewsWithHeader(
            map,
            FilterItemSubAttrBinding::inflate,
            CardFilterItemAtr2Binding::inflate,
            ::bindSubCategoryHeader,
            ::bindSubCategoryAttr
        )

        model.isSelected = model.subCategory.any { it.isSelected }
        binding.cvSelected.makeVisible(model.isSelected)
    }

    private fun bindSubCategoryHeader(
        binding: FilterItemSubAttrBinding,
        headerModel: SubCategory
    ) {
        binding.tvSubCategoryName.text = headerModel.subCategory
    }

    private fun bindSubCategoryAttr(
        binding: CardFilterItemAtr2Binding,
        subCategoryModel: SubArray,
        headerModel: SubCategory
    ) {
        binding.tvCategoryName.text = subCategoryModel.name

        binding.ivFilterCheck.setImageResource(
            if (subCategoryModel.isSelected) R.drawable.ic_filter_check
            else R.drawable.ic_filter_uncheck
        )

        binding.tvCategoryName.typeface = ResourcesCompat.getFont(
            requireContext(),
            if (subCategoryModel.isSelected) R.font.gilroy_semibold
            else R.font.gilroy_medium
        )

        binding.tvCategoryName.setTextColor(
            ResourcesCompat.getColor(
                resources,
                if (subCategoryModel.isSelected) R.color.colorAccent else R.color.close_tint,
                resources.newTheme()
            )
        )

        binding.root.setOnClickListener {
            onSubCategorySelectedOrUnselected(subCategoryModel) {
                val filter = sortedFilters[lastSelectedFilterPos]
                //adapter?.notifyItemChanged(position)
                headerModel.isSelected = headerModel.subArray.any { it.isSelected }
                filter.selectionCount = selectedCategories.size
                this.binding.rvFilters.adapter?.notifyItemChanged(lastSelectedFilterPos)
                this.binding.rvFilterAttrs.notifyAt(openCategoryPosition)
            }
        }
        //56852171

    }

    private fun onSubCategorySelectedOrUnselected(
        subCategoryModel: SubArray,
        cb: () -> Unit
    ) {
        if (subCategoryModel.isSelected) getConfirmationOfUnselecting {
            subCategoryModel.isSelected = false
            selectedCategories.remove(subCategoryModel.displayName)
            resetBrands(true)
            cb()
        } else lifecycleScope.launch {
            if (isBrandSelectedInitially) {
                val isProceed = getConfirmationOfSelecting()
                if (isProceed) isBrandSelectedInitially = false
                else return@launch
            }
            subCategoryModel.isSelected = true
            selectedCategories.add(subCategoryModel.displayName)
            resetBrands()
            cb()
        }
    }

    private fun resetBrands(isClearBrand: Boolean = false) {
        if (isClearBrand) {
            filters?.brands?.forEach { it.isSelected = false }
            isBrandsChanged = false
        }
        if (sortedFilters.size>1) {
            sortedFilters[1].filterTypes = getBrandsAccToSelectedCategories()
            sortedFilters[1].selectionCount = sortedFilters[1].filterTypes.count { it.isSelected }
            this.binding.rvFilters.adapter?.notifyItemChanged(1)
        }
    }

    private fun getConfirmationOfUnselecting(cb: () -> Unit) {
        if (!isBrandsChanged) cb()
        else {
            val binding = DialogFilterWarningBinding.inflate(LayoutInflater.from(requireContext()))
            val builder = AlertDialog.Builder(context)
            val dialog = builder.setView(binding.root).create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            binding.btnCancel.setOnClickListener { dialog.dismiss() }
            binding.ivClose.setOnClickListener { dialog.dismiss() }
            binding.btnProceed.setOnClickListener {
                cb(); dialog.dismiss()
            }
            dialog.show()
        }
    }

    private suspend fun getConfirmationOfSelecting(): Boolean {
        val coroutine = suspendCoroutine<Boolean> { continuation ->
            var isProceed = false

            val binding = DialogFilterWarningBinding.inflate(LayoutInflater.from(requireContext()))
            val builder = AlertDialog.Builder(context)
            val dialog = builder.setView(binding.root).create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            binding.tvWarning.text = resources.getString(R.string.category_select_warning)
            binding.btnCancel.setOnClickListener { dialog.dismiss() }
            binding.ivClose.setOnClickListener { dialog.dismiss() }
            binding.btnProceed.setOnClickListener {
                isProceed = true; dialog.dismiss()
            }
            dialog.setOnDismissListener { continuation.resume(isProceed) }
            dialog.show()

        }
        return coroutine
    }

    private fun search(): MutableList<Category> {
        val categories = mutableListOf<Category>()
        filters?.categories?.forEach { c ->
            val subCategoryList = mutableListOf<SubCategory>()
            c.subCategory.filter { it.isEnable }.forEach { sc ->
                val list = sc.subArray.filter { it.isEnable == true }.filter { sa ->
                    if (categoriesSearch.isNotEmpty())
                        sa.name.lower().contains(categoriesSearch.lower())
                    else true
                }
                if (list.isNotEmpty())
                    subCategoryList.add(sc.copy(subArray = list))
            }
            if (subCategoryList.isNotEmpty())
                categories.add(c.copy(subCategory = subCategoryList))
        }
        return categories
    }

    private fun getBrandsAccToSelectedCategories(): List<FilterType> {
        val brandTypes = filters?.brands
            ?.filter { b ->
                val isMatched =
                    if (selectedCategories.size == 0) true
                    else b.subCategories.any { selectedCategories.contains(it) }
                if (!isMatched) b.isSelected = false
                isMatched
            }
            ?.map {
                FilterType(
                    name = it.name,
                    filterValue = it.filterValue,
                    isSelected = it.isSelected
                )
            }
        return brandTypes ?: listOf()
    }


    private fun fireFilterSortEvent() {
        val bundle = Bundle()

        filters?.filters?.map { it.filterTypes.find { ft -> ft.isSelected }?.name }
            ?.let { appliedFilters ->
                bundle.putString(
                    FirebaseAnalyticsLog.FirebaseEventNameENUM.FILTERS_APPLIED,
                    appliedFilters.filter { !it.isNullOrEmpty() }.toString()
                )
            }

        filters?.brands?.filter { it.isSelected }?.map { it.name }?.let {
            bundle.putString(
                FirebaseAnalyticsLog.FirebaseEventNameENUM.BRANDS_SELECTED,
                it.toString()
            )
        }
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.CATEGORY_SELECTED,
            selectedCategories.toString()
        )
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SORT_AND_FILTER
        )
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FILTER_SORT,
            bundle
        )

    }


}