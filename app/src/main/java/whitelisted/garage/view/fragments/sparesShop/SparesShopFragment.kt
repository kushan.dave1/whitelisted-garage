package whitelisted.garage.view.fragments.sparesShop

import android.Manifest
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.lifecycleScope
import com.google.gson.Gson
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.*
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.DialogImageToTextBinding
import whitelisted.garage.databinding.FragmentSparesShopBinding
import whitelisted.garage.databinding.ItemRecentSearchBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.isVisible
import whitelisted.garage.utils.CommonUtils.makeInVisible
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.*
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import whitelisted.garage.viewmodels.WalkthroughSharedViewModel

class SparesShopFragment : BaseFragment() {

    private val binding by viewBinding(FragmentSparesShopBinding::inflate)
    private val viewModel: SparesShopFragmentViewModel by sharedViewModel()
    private val walkThroughViewModel: WalkthroughSharedViewModel by sharedViewModel()
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var isDataFetched = false
    private var plpBannerPos = 4

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fireEvent()
        setupScreen()
        handleBackPress()
    }

    private fun setupScreen() = binding.apply {
        if (arguments?.getBoolean(AppENUM.IntentKeysENUM.IS_UNDER_TAB, false) == true) {
            imgBackFSS.makeVisible(false)
            clBaseFSS.setPadding(0, 0, 0, 240)
        } else {
            clBaseFSS.setPadding(0, 0, 0, 54)
        }

        clickListeners()

        setObservers()

        if (isVisible) {
            binding.sflSSHome.makeVisible(true)
                    binding.clScreenView.makeInVisible(true)
            binding.ivOrderHistoryFSS.makeVisible(true)
            binding.ivEnquiryFSS.makeVisible(true)
            viewModel.getSparesHomeResponse()
            viewModel.getSSOriginalBrandsResponse()
            viewModel.getSSCartAPI()
            getMarketplaceFAQ()
            getNotifiedItemsList()
            getMasterFilters()
        }

        srlFSS.setOnRefreshListener {
            srlFSS.isRefreshing = false
            showLoader()
            viewModel.getSparesHomeResponse()
            viewModel.getSSOriginalBrandsResponse()
            viewModel.getSSCartAPI()
            getMarketplaceFAQ()
            getNotifiedItemsList()
        }

        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SPARES_HOME_RESPONSE, "").let {
            if (it.isNotEmpty()) {
                hideLoader()
                binding.sflSSHome.makeVisible(false)
                binding.clScreenView.makeVisible(true)
//                isDataFetched = true
                val sparesHomeResponse = Gson().fromJson(it, SparesHomeResponseModel::class.java)
                sparesHomeResponse?.let { homeResponse ->
                    setData(homeResponse)
                }
            }
        }
    }

    private fun setObservers() {
        observeSparesHomeResponse()
        observeSSSparesBrandsResponse()
        observeSSAccBrandsResponse()
        observeGetCartAPI()
        observeSSSparesCategoriesResponse()
        observeSSAccCategoriesResponse()
        observeSSHomeBannersResponse()
        observeSSOriginalSparesBrandsResponse()
        observeWalkthroughViewModel()
    }

    private fun observeWalkthroughViewModel() {
        walkThroughViewModel.provideSparesWalkThroughEvent().observe(viewLifecycleOwner) {
            when (it) {
                R.id.imgSparesBanner -> {
                    lifecycleScope.launch(Dispatchers.Main) {
                        delay(200)
                        startWalkthrough(binding.tvSeeAllBrandsFSS)
                    }
                }

                R.id.tvSeeAllBrandsFSS -> {
                    binding.tvSeeAllAccBrandsFSS.let { view ->
                        val scrollTo = (view.parent as View).top + view.top - 50
                        binding.nsvFSS.smoothScrollTo(0, scrollTo)
                        lifecycleScope.launchWhenResumed {
                            delay(300)
                            startWalkthrough(binding.tvSeeAllAccBrandsFSS)
                        }
                    }
                }

                R.id.btnFilterAndSort -> {
                    if (isAdded) popBackStackAndHideKeyboard()
                    binding.ivOrderHistoryFSS.let { view ->
                        val scrollTo = (view.parent as View).top + view.top
                        binding.nsvFSS.smoothScrollTo(0, scrollTo)
                        lifecycleScope.launchWhenResumed {
                            delay(300)
                            startWalkthrough(binding.ivOrderHistoryFSS)
                        }
                    }
                }

                R.id.ivOrderHistoryFSS -> {
                    startWalkthrough(binding.tvMyOrdersCart)
                }

                R.id.tvMyOrdersCart -> {
                    binding.nsvFSS.smoothScrollTo(0, 0)
                }
            }
        }
    }

    private fun observeSparesHomeResponse() {
        viewModel.provideSparesHomeResponse().observe(viewLifecycleOwner) {
            hideLoader()
            binding.sflSSHome.makeVisible(false)
            binding.clScreenView.makeVisible(true)
            when (it) {
                is Result.Success -> {
                    it.body.data.let { sparesHomeResponseModel ->
                        if (viewModel.getStringSharedPreference(
                                AppENUM.UserKeySaveENUM.SPARES_HOME_RESPONSE,
                                ""
                            ) != Gson().toJson(sparesHomeResponseModel)
                        ) {
                            viewModel.putStringSharedPreference(
                                AppENUM.UserKeySaveENUM.SPARES_HOME_RESPONSE,
                                Gson().toJson(sparesHomeResponseModel)
                            )

                            setData(sparesHomeResponseModel)
                        }
                        plpBannerPos = sparesHomeResponseModel.plpBannersCount ?: 4
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setData(sparesHomeResponseModel: SparesHomeResponseModel) {
        if (sparesHomeResponseModel.sparesBrands.isNotEmpty() && sparesHomeResponseModel.sparesBrandsFlag == true) {
            binding.cgSparesBrands.makeVisible(true)
            viewModel.setSSBrandsList(sparesHomeResponseModel.sparesBrands)
            setBrandsAdapter(sparesHomeResponseModel.sparesBrands)
        }

        if (sparesHomeResponseModel.accBrands.isNotEmpty() && sparesHomeResponseModel.accBrandsFlag == true) {
            binding.cgAccBrands.makeVisible(true)
            viewModel.setSSAccBrandsList(sparesHomeResponseModel.accBrands)
            setAccBrandsAdapter(sparesHomeResponseModel.accBrands)
        }

        if (sparesHomeResponseModel.bikeBrands.isNotEmpty() && sparesHomeResponseModel.bikeBrandsFlag == true) {
            binding.cgBikeBrands.makeVisible(true)
            binding.lblBikeBrands.text =
                sparesHomeResponseModel.bikeBrandsText //            viewModel.setSSAccBrandsList(sparesHomeResponseModel.accBrands)
            setBikeBrandsAdapter(sparesHomeResponseModel.bikeBrands)
        }

        if (sparesHomeResponseModel.sparesCategories.isNotEmpty() && sparesHomeResponseModel.sparesCategoriesFlag == true) {
            binding.cgSparesCategories.makeVisible(true)
            viewModel.setSSCategoriesList(sparesHomeResponseModel.sparesCategories)
            setCategoriesAdapter(sparesHomeResponseModel.sparesCategories)
        }

        if (sparesHomeResponseModel.accCategories.isNotEmpty() && sparesHomeResponseModel.accCategoriesFlag == true) {
            binding.cgAccCategories.makeVisible(true)
            viewModel.setSSAccCategoriesList(sparesHomeResponseModel.accCategories)
            setAccCategoriesAdapter(sparesHomeResponseModel.accCategories)
        }

        if (sparesHomeResponseModel.bikeCategories.isNotEmpty() && sparesHomeResponseModel.bikeCategoriesFlag == true) {
            binding.cgBikeCategories.makeVisible(true)
            binding.lblBikeCategories.text =
                sparesHomeResponseModel.bikeCategoriesText //            viewModel.setSSAccCategoriesList(sparesHomeResponseModel.accCategories)
            setBikeCategoriesAdapter(sparesHomeResponseModel.bikeCategories)
        }

        if (sparesHomeResponseModel.banners.isNotEmpty() && sparesHomeResponseModel.ssHomeBannersFlag == true) {
            binding.rvBanners.makeVisible(true)
            setBannersAdapter(sparesHomeResponseModel.banners)
        } else binding.rvBanners.makeVisible(false)
    }

    private fun setBikeCategoriesAdapter(bikeCategories: MutableList<SSCategoryResponseModel>) =
        binding.apply {
            rvBikeCategories.adapter =
                SSCategoriesAdapter(bikeCategories, true, this@SparesShopFragment)
            rvsBikeCategories.post {
                rvsBikeCategories.makeVisible(true)
                rvsBikeCategories.attachToRecyclerView(rvBikeCategories)
            }
        }

    private fun setBikeBrandsAdapter(bikeBrands: MutableList<SSBrandsResponseModel>) {
        binding.rvBikeBrandsFSS.adapter = SSBrandsAdapter(bikeBrands, this)
    }

    private fun observeSSOriginalSparesBrandsResponse() {
        viewModel.provideSSOriginalBrandsResponse().observe(viewLifecycleOwner) {
            hideLoader() //            binding.sflSSHome.makeVisible(false)
            //            binding.clScreenView.makeVisible(true)
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        binding.llOrgSparesBrands.makeVisible(true)
                        viewModel.setSSOriginalBrandsList(it.body.data)
                        setOriginalBrandsAdapter(it.body.data)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setOriginalBrandsAdapter(data: MutableList<SSBrandsResponseModel>) {
        binding.rvOriginalSparesBrands.adapter = OrgSparesBrandAdapter(data, this)
    }

    private fun setAccBrandsAdapter(data: MutableList<SSBrandsResponseModel>) {
        binding.rvAccBrandsFSS.adapter = SSBrandsAdapter(data, this)
    }

    private fun observeSSAccCategoriesResponse() {
        viewModel.provideSSAccCategoriesResponse().observe(viewLifecycleOwner) {
            hideLoader()
            binding.sflSSHome.makeVisible(false)
            binding.clScreenView.makeVisible(true)
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        viewModel.setSSAccCategoriesList(it.body.data)
                        setAccCategoriesAdapter(it.body.data)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setAccCategoriesAdapter(data: MutableList<SSCategoryResponseModel>) =
        binding.apply {
            rvAccCategories.adapter = SSCategoriesAdapter(data, true, this@SparesShopFragment)
            rvsAccCategories.post {
                rvsAccCategories.makeVisible(true)
                rvsAccCategories.attachToRecyclerView(rvAccCategories)
            }
        }

    private fun observeSSAccBrandsResponse() {
        viewModel.provideSSAccBrandsResponse().observe(viewLifecycleOwner) {
            hideLoader()
            binding.sflSSHome.makeVisible(false)
            binding.clScreenView.makeVisible(true)
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        viewModel.setSSAccBrandsList(it.body.data)
                        setAccBrandsAdapter(it.body.data)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun getNotifiedItemsList() = lifecycleScope.launchWhenCreated {
        val list = viewModel.getNotifiedItemsAPI()
        viewModel.setNotifiedItems(list)
    }

    private fun getMarketplaceFAQ() = lifecycleScope.launchWhenStarted {
        val faqs = viewModel.getMarketplaceFAQ()
        if (faqs.isEmpty()) binding.gFAQ.makeVisible(false)
        else {
            binding.gFAQ.makeVisible(true)
            binding.rvMarketplaceFAQ.adapter = GoCoinsFAQAdapter(faqs.toMutableList())
            binding.imgToggle.setOnClickListener {
                expandCollapseTrack()
            }
        }
    }

    private fun expandCollapseTrack() = binding.apply {
        val isExpanded = rvMarketplaceFAQ.isVisible()
        rvMarketplaceFAQ.makeVisible(!isExpanded)
        imgToggle.animate().rotation(if (isExpanded) 0f else 180f).setDuration(500).start()
        nsvFSS.post {
            nsvFSS.smoothScrollTo(nsvFSS.x.toInt(), rvMarketplaceFAQ.y.toInt())
        }
    }

    private fun observeSSHomeBannersResponse() {
        viewModel.provideSSHomeBannersResponse().observe(viewLifecycleOwner) {
            hideLoader()
            binding.sflSSHome.makeVisible(false)
            binding.clScreenView.makeVisible(true)
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        setBannersAdapter(it.body.data)
                    } else binding.rvBanners.makeVisible(false)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setBannersAdapter(data: MutableList<SSHomeBannersResponseModel>) {
        binding.rvBanners.adapter = SSHomeBannersAdapter(data, this)
    }

    private fun observeSSSparesCategoriesResponse() {
        viewModel.provideSSHomeCategoriesResponse().observe(viewLifecycleOwner) {
            hideLoader()
            binding.sflSSHome.makeVisible(false)
            binding.clScreenView.makeVisible(true)
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        viewModel.setSSCategoriesList(it.body.data)
                        setCategoriesAdapter(it.body.data)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setCategoriesAdapter(data: MutableList<SSCategoryResponseModel>) = binding.apply {
        rvCategories.adapter = SSCategoriesAdapter(data, false, this@SparesShopFragment)
        rvsCategories.post {
            rvsCategories.makeVisible(true)
            rvsCategories.attachToRecyclerView(rvCategories)
        }
    }

    private fun clickListeners() {
        binding.imgBackFSS.setOnClickListener(this)
        binding.tvMyOrdersCart.setOnClickListener(this)
        binding.tvSeeAllBrandsFSS.setOnClickListener(this)
        binding.tvSeeAllAccBrandsFSS.setOnClickListener(this)
        binding.ivOrderHistoryFSS.setOnClickListener(this)
        binding.ivEnquiryFSS.setOnClickListener(this)
        binding.tvSearchFSS.setOnClickListener(this)
        binding.tvSeeAllCatsFSS.setOnClickListener(this)
        binding.tvSeeAllAccCatsFSS.setOnClickListener(this)
        binding.tvSeeAllBikeBrandsFSS.setOnClickListener(this)
        binding.tvSeeAllBikeCatsFSS.setOnClickListener(this)
        binding.btnSearchByImage.setOnClickListener(this)
    }

    private fun observeGetCartAPI() {
        viewModel.provideGetCartResponseAPI().observe(viewLifecycleOwner) { result ->
            hideLoader()
            when (result) {
                is Result.Success -> {
                    isDataFetched = true
                    viewModel.setGetCartResponse(result.body.data)
                    result.body.data.cartList[0].lineItems.plus(result.body.data.cartList[1].lineItems).size.let { itemCount ->
                        if (itemCount > 0) {
                            binding.tvCartCount.makeVisible(true)
                            binding.tvCartCount.text = "$itemCount"
                        } else binding.tvCartCount.makeVisible(false)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), result.errorMessage, true)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        (binding.rvMarketplaceFAQ.adapter as? GoCoinsFAQAdapter)?.closeAll()
    }

    override fun onClick(v: View) {
        if (dashboardViewModel.getBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN, true)) {
            super.onClick(v)
            when (v.id) {
                R.id.imgBackFSS -> {
                    popBackStackAndHideKeyboard()
                }
                R.id.cvBaseISSB -> lifecycleScope.launch {
                    waitForFiltersToGetLoaded()
                    CommonUtils.genericCastOrNull<SSBrandsResponseModel>(v.getTag(R.id.model))
                        ?.let {
                            CommonUtils.addFragmentUtil(
                                requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                                    Bundle().apply { //                                putString(AppENUM.IntentKeysENUM.TYPE,
                                        //                                    AppENUM.RefactoredStrings.BRAND)
                                        putString(AppENUM.IntentKeysENUM.BRAND_NAME, it.brandName)
                                        putInt(
                                            AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION,
                                            plpBannerPos
                                        )
                                        it.banners?.let { banners ->
                                            putParcelableArrayList(
                                                AppENUM.IntentKeysENUM.PLP_BANNERS,
                                                ArrayList(banners)
                                            )
                                        }
                                    })
                            )
                            fireSelectBrandEvent(it.brandName ?: "")
                        }
                }

                R.id.clBaseISSC -> lifecycleScope.launch {
                    waitForFiltersToGetLoaded()
                    CommonUtils.genericCastOrNull<SSCategoryResponseModel>(v.getTag(R.id.model))
                        ?.let {

                            CommonUtils.genericCastOrNull<Boolean>(v.getTag(R.id.type))
                                ?.let { isAcc ->
                                    fireCategorySelectedEvent(it.name)
                                    if (isAcc) {
                                        CommonUtils.addFragmentUtil(
                                            requireActivity(),
                                            FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                                                Bundle().apply { //                                putString(AppENUM.IntentKeysENUM.TYPE,
                                                    //                                    AppENUM.RefactoredStrings.CATEGORY)
                                                    putString(
                                                        AppENUM.IntentKeysENUM.CATEGORY_NAME,
                                                        it.name
                                                    )
                                                    putInt(
                                                        AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION,
                                                        plpBannerPos
                                                    )
                                                    it.banners?.let { banners ->
                                                        putParcelableArrayList(
                                                            AppENUM.IntentKeysENUM.PLP_BANNERS,
                                                            ArrayList(banners)
                                                        )
                                                    }
                                                })
                                        )
                                        fireSelectCatEvent(it.name ?: "")
                                    } else {
                                        CommonUtils.addFragmentUtil(
                                            requireActivity(),
                                            FragmentFactory.fragment(FragmentFactory.Fragments.SELECT_CATEGORY_OR_BRAND,
                                                Bundle().apply {
                                                    putParcelable(AppENUM.CATEGORY_DATA, it)
                                                    putInt(
                                                        AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION,
                                                        plpBannerPos
                                                    )
                                                    it.banners?.let { banners ->
                                                        putParcelableArrayList(
                                                            AppENUM.IntentKeysENUM.PLP_BANNERS,
                                                            ArrayList(banners)
                                                        )
                                                    }
                                                })
                                        )
                                        fireSelectCatEvent(it.categoriesName ?: "")
                                    }
                                }
                        }
                }
                R.id.tvSeeAllBrandsFSS, R.id.tvSeeAllAccBrandsFSS -> lifecycleScope.launch {
                    waitForFiltersToGetLoaded()
                    viewModel.setBrandsSelected(v.id == R.id.tvSeeAllBrandsFSS)
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER)
                    )
                }
                R.id.tvSeeAllBikeBrandsFSS -> lifecycleScope.launch {
                    waitForFiltersToGetLoaded()
                    viewModel.setBikeBrandsSelected()
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER)
                    )
                }
                R.id.tvMyOrdersCart -> { //                if (tvCartCount.isVisible())
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SS_CART)
                    )
                }

                R.id.ivOrderHistoryFSS -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_ORDER_HISTORY,
                            params = Bundle().apply {
                                putInt(AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION, plpBannerPos)
                            })
                    )
                    fireInitOrderHistory()
                }
                R.id.ivEnquiryFSS -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SPARES_ENQUIRIES)
                    )
                    fireInitOrderHistory()
                }

                R.id.tvSearchFSS -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SELECT_CAR_AND_SEARCH,
                            params = Bundle().apply {
                                putInt(AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION, plpBannerPos)
                            })
                    )
                }

                R.id.cvSSHomeBanner -> lifecycleScope.launch {
                    waitForFiltersToGetLoaded()
                    CommonUtils.genericCastOrNull<SSHomeBannersResponseModel>(v.getTag(R.id.model))
                        ?.let {
                            CommonUtils.addFragmentUtil(
                                requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                                    Bundle().apply { //                                    putString(AppENUM.IntentKeysENUM.TYPE,
                                        //                                         AppENUM.RefactoredStrings.BRAND)
                                        putString(AppENUM.IntentKeysENUM.BRAND_NAME, it.brandName)
                                        putInt(
                                            AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION,
                                            plpBannerPos
                                        )
                                        it.banners?.let { banners ->
                                            putParcelableArrayList(
                                                AppENUM.IntentKeysENUM.PLP_BANNERS,
                                                ArrayList(banners)
                                            )
                                        }
                                    })
                            )
                        }
                }

                R.id.imgOrgBrandImage -> lifecycleScope.launch {
                    waitForFiltersToGetLoaded()
                    CommonUtils.genericCastOrNull<SSBrandsResponseModel>(v.getTag(R.id.model))
                        ?.let {
                            CommonUtils.addFragmentUtil(
                                requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.SHOP_WITH_FILTER,
                                    Bundle().apply { //                                putString(AppENUM.IntentKeysENUM.TYPE,
                                        //                                    AppENUM.RefactoredStrings.BRAND)
                                        putString(AppENUM.IntentKeysENUM.BRAND_NAME, it.brandName)
                                        putInt(
                                            AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION,
                                            plpBannerPos
                                        )
                                        it.banners?.let { banners ->
                                            putParcelableArrayList(
                                                AppENUM.IntentKeysENUM.PLP_BANNERS,
                                                ArrayList(banners)
                                            )
                                        }
                                    })
                            )
                            fireSelectBrandEvent(it.brandName ?: "")
                        }
                }

                R.id.tvSeeAllCatsFSS -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SELECT_CATEGORY_OR_BRAND,
                            Bundle().apply {
                                putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                                putInt(AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION, plpBannerPos)
                                putInt(AppENUM.POSITION, 0)
                            })
                    ) //                fireSelectCatEvent("all")
                }

                R.id.tvSeeAllAccCatsFSS -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SELECT_CATEGORY_OR_BRAND,
                            Bundle().apply {
                                putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                                putInt(AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION, plpBannerPos)
                                putInt(AppENUM.POSITION, -1)
                            })
                    ) //                fireSelectCatEvent("all")
                }

                R.id.tvSeeAllBikeCatsFSS -> {
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SELECT_CATEGORY_OR_BRAND,
                            Bundle().apply {
                                putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                                putInt(AppENUM.IntentKeysENUM.PLP_BANNERS_POSITION, plpBannerPos)
                                putInt(AppENUM.POSITION, -2)
                            })
                    ) //                fireSelectCatEvent("all")
                }

                R.id.btnSearchByImage -> openSelectImageView()
            }
        }
    }

    private fun openTextRecognizer(uri: Uri) {

        val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)
        val image = InputImage.fromFilePath(requireContext(), uri)
        recognizer.process(image).addOnSuccessListener { text ->
            showRecognizedTextsDialog(text.textBlocks.map { it.text })
        }

    }

    private fun showRecognizedTextsDialog(texts: List<String>) {

        val binding = DialogImageToTextBinding.inflate(LayoutInflater.from(requireContext()))
        val builder = AlertDialog.Builder(context)
        val dialog = builder.setView(binding.root).create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        if(texts.isEmpty()) {
            binding.tvSubText.makeVisible(false)
            binding.searchesRv.makeVisible(false)
            binding.ivEmpty.makeVisible(true)
            binding.tvNoResults.makeVisible(true)
        }

        else {
            binding.searchesRv.setLayoutManagerAsLinear()
            binding.searchesRv.addViews(texts, ItemRecentSearchBinding::inflate) { bind, text, _ ->
                bind.ivRSD.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.ic_search_gray,
                        resources.newTheme()
                    )
                )
                bind.ivRSD.scaleX = 0.8f
                bind.ivRSD.scaleY = 0.8f
                bind.tvItemNameIRS.text = text
                bind.tvItemNameIRS.setOnClickListener {
                    fireImageToTextSearchEvent(text)
                    CommonUtils.addFragmentUtil(
                        requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.SELECT_CAR_AND_SEARCH,
                            Bundle().apply
                            {
                                putString(AppENUM.IMAGE_SEARCH, text)
                            })
                    )
                    dialog.dismiss()
                }
            }
        }
        binding.tvClose.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }

    private suspend fun waitForFiltersToGetLoaded() {
        showLoader()
        while (viewModel.searchAndFilterAttrs.brands.isEmpty()) {
            delay(100)
        }
        hideLoader()
    }

    private fun observeSSSparesBrandsResponse() {
        viewModel.provideSSHomeBrandsResponse().observe(viewLifecycleOwner) {
            hideLoader()
            binding.sflSSHome.makeVisible(false)
            binding.clScreenView.makeVisible(true)
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        viewModel.setSSBrandsList(it.body.data)
                        setBrandsAdapter(it.body.data)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setBrandsAdapter(data: MutableList<SSBrandsResponseModel>) {
        binding.rvBrandsFSS.adapter = SSBrandsAdapter(data, this)
    }

    private fun openSelectImageView() {
        TedPermission.create().setPermissionListener(object : PermissionListener {
            override fun onPermissionGranted() {
                TedImagePicker.with(requireContext()).showCameraTile(true)
                    .start(::openTextRecognizer)
            }

            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
                CommonUtils.showToast(context, resources.getString(R.string.permission_denied))
            }

        }).setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .check()


    }

    private fun fireCategorySelectedEvent(name: String?) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.SELECTED_CATEGORY, name)
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP
        )
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.MARKETPLACE_HOME_CAT,
            bundle
        )
    }

    private fun fireSelectCatEvent(category: String) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.SELECTED_CATEGORY, category)
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP
        )
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.SELECT_CAT,
            bundle
        )
    }

    private fun fireSelectBrandEvent(brand: String) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.SELECTED_BRAND, brand)
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP
        )
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.SELECT_BRAND,
            bundle
        )
    }

    private fun fireEvent() {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_HOMEPAGE
        )
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_SPARES_MARKET,
            bundle
        )
    }

    private fun fireInitOrderHistory() {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP
        )
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.INIT_ORDER_HISTORY,
            bundle
        )
    }

    private fun fireImageToTextSearchEvent(text: String) {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_SHOP
        )
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.SEARCH_TEXT, text)
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.SERIAL_NO_SCANNER,
            bundle
        )
    }

    fun apiCall() {
        if (!isDataFetched) {
            binding.sflSSHome.makeVisible(true)
            binding.clScreenView.makeInVisible(type = true)
            viewModel.getSparesHomeResponse()
            viewModel.getSSOriginalBrandsResponse()
            getMarketplaceFAQ()
            getNotifiedItemsList()
            getMasterFilters()
        }
        viewModel.getSSCartAPI()
    }

    private fun getMasterFilters() = viewLifecycleOwner.lifecycleScope.launch {
        val attrs = viewModel.getSearchAndFilterAttributes()
        viewModel.setSearchAndFilterAttributes(attrs)
    }





}
