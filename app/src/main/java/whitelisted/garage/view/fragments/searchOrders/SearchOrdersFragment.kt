package whitelisted.garage.view.fragments.searchOrders

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.SearchOrderResponseModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentSearchOrdersBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.SearchOrdersAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.SearchOrdersFragmentViewModel

class SearchOrdersFragment : BaseFragment(), TextWatcher, TextView.OnEditorActionListener,
    View.OnTouchListener {

    private val binding by viewBinding(FragmentSearchOrdersBinding::inflate)
    private val viewModel: SearchOrdersFragmentViewModel by viewModel()
    private lateinit var searchedOrdersList: MutableList<SearchOrderResponseModel>
    private lateinit var searchOrdersAdapter: SearchOrdersAdapter
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var searchEmpty = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListener()
        binding.etSearch.requestFocus()
        CommonUtils.showKeyboard(requireActivity(), binding.etSearch)
        observerOrdersList()
        setAdapter()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun clickListener() {
        binding.imgBack.setOnClickListener(this)
        binding.etSearch.addTextChangedListener(this)
        binding.etSearch.setOnEditorActionListener(this)
        binding.rvSearchOrders.setOnTouchListener(this)
        binding.llCreateOrder.setOnClickListener(this)
    }

    private fun observerOrdersList() {
        viewModel.provideSearchOrdersResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (!searchEmpty) {
                        searchedOrdersList = it.body.data.toMutableList()
                        if (searchedOrdersList.isNotEmpty()) {
                            searchOrdersAdapter.addAllToAdapterList(searchedOrdersList)
                            binding.rvSearchOrders.makeVisible(true)
                            binding.noDataScreen.makeVisible(false)
                        } else {
                            searchOrdersAdapter.addAllToAdapterList(searchedOrdersList)
                            binding.noDataScreen.makeVisible(true)
                            binding.rvSearchOrders.makeVisible(false)
                        }
                    }
                }
                is Result.Failure -> {
                    binding.rvSearchOrders.makeVisible(false)
                    binding.noDataScreen.makeVisible(true)
                }
            }
        }
    }

    private fun setAdapter() {
        binding.rvSearchOrders.layoutManager = LinearLayoutManager(requireContext())
        searchedOrdersList = mutableListOf()
        searchOrdersAdapter =
            SearchOrdersAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default)), searchedOrdersList, this)
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                searchOrdersAdapter.viewType = 1
            }
            else -> {
                searchOrdersAdapter.viewType = 0
            }
        }
        binding.rvSearchOrders.adapter = searchOrdersAdapter
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> popBackStackAndHideKeyboard()
            R.id.llCreateOrder -> {
                if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                        false)) {
                    createNewOrderActual()
                } else {
                    if ((arguments?.getInt(AppENUM.ORDERS_COUNT, -1)
                            ?: -1) < (dashboardViewModel.screensConfigResponse.value?.membershipConfig?.orderCreation
                            ?: 0)) {
                        createNewOrderActual()
                    } else {
                        requireActivity().let {
                            fireFeatureLockedEvent()
                            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT,
                                Bundle().apply {
                                    putString(AppENUM.PRO_LOCKED_ALERT_MSG,
                                        dashboardViewModel.screensConfigResponse.value?.membershipConfig?.orderCreationMsg)
                                })?.let { baseFragment ->
                                    baseFragment.show(it.supportFragmentManager,
                                        baseFragment.javaClass.name)
                                }
                        }
                    }
                }

            }
            R.id.btnCall -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    Intent(Intent.ACTION_DIAL).apply {
                        this.data =
                            Uri.parse("tel:${searchedOrdersList[position].customerDetails?.country_code}${searchedOrdersList[position].customerDetails?.mobile}")
                        startActivity(this)
                    }
                }
            }
            R.id.btnCreateNewOrder -> {
                if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                        AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.RETAILER_CONSTANT) {
                    CommonUtils.genericCastOrNull<SearchOrderResponseModel>(v.getTag(R.id.model))
                        ?.let { model ->
                            CommonUtils.addFragmentUtil(requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_NEW_ORDER,
                                    Bundle().apply {
                                        putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                                        putParcelable(AppENUM.SEARCH_ORDER_MODEL, model)
                                    }))
                        }
                } else {
                    CommonUtils.genericCastOrNull<SearchOrderResponseModel>(v.getTag(R.id.model))
                        ?.let { model ->
                            CommonUtils.addFragmentUtil(requireActivity(),
                                FragmentFactory.fragment(FragmentFactory.Fragments.NEW_ORDER_DETAIL,
                                    Bundle().apply {
                                        putParcelable(AppENUM.SEARCH_ORDER_MODEL, model)
                                    }))
                        }
                }
            }
            R.id.clBaseBuyGC -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    handleBuyGcClick(position)
                }
            }
        }
    }

    private fun createNewOrderActual() {
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_NEW_ORDER,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                        }),
                )
            }
            else -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                        }),
                )
            }
        }
    }

    private fun handleBuyGcClick(position: Int) {
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                searchedOrdersList[position].orderId)
                        }),
                )
            }
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                searchedOrdersList[position].orderId)
                        }),
                )
            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                searchedOrdersList[position].orderId)
                        }),
                )
            }
            else -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.ORDER_ID,
                                searchedOrdersList[position].orderId)
                        }),
                )
            }
        }
    }


    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { // nothing
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { // nothing
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0?.isNotEmpty() == true) {
            showLoader()
            searchEmpty = false
            viewModel.getOrders(p0.toString())
            fireSearchEvent(p0.toString())
        } else {
            searchEmpty = true
            binding.rvSearchOrders.makeVisible(false)
            binding.noDataScreen.makeVisible(true)
        }
    }

    private fun fireSearchEvent(text: String) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FBE_SEARCH_TERM, text)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SEARCH_HOMEPAGE)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalytics.Event.SEARCH, this)
        }
    }

    override fun onEditorAction(textView: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            CommonUtils.hideKeyboard(requireActivity())
            showLoader()
            binding.rvSearchOrders.makeVisible(true)
            viewModel.getOrders(binding.etSearch.text.toString())
            return true
        }
        return false
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
        CommonUtils.hideKeyboard(requireActivity())
        return false
    }

    private fun fireFeatureLockedEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SEARCH_ORDERS)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.ERROR_HEADER_PRO,
            bundle)

    }
}