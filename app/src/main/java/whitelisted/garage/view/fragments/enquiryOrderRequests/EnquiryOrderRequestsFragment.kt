package whitelisted.garage.view.fragments.enquiryOrderRequests

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.response.AccRetailBidListResponseModel
import whitelisted.garage.api.response.AccRetailBidListResponseModelSubmitAccept
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentEnquiryOrdersListBinding
import whitelisted.garage.eventBus.UpdateHomeScreenEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeViewCompoundDrawablesWithInterinsicBounds
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.AcceptedBidsListAdapter
import whitelisted.garage.view.adapter.NewBidsListAdapter
import whitelisted.garage.view.adapter.SubmittedBidsListAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.EnquiryOrderRequestsFragmentVIewModel

class EnquiryOrderRequestsFragment : BaseFragment(), TabLayout.OnTabSelectedListener {

    private val binding by viewBinding(FragmentEnquiryOrdersListBinding::inflate)
    private val viewModel: EnquiryOrderRequestsFragmentVIewModel by sharedViewModel()
    private var isComboPurchased = false
    private val dashboardSharedViewModel: DashboardSharedViewModel by sharedViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListeners()
        setObservers()
        setTabLayout()
        var tabPosition = 0
        arguments?.let {
            isComboPurchased = (it.getBoolean(AppENUM.IS_COMBO_PURCHASED, false))
            tabPosition = it.getInt(AppENUM.POSITION, 0)
            binding.tlOrderRequests.selectTab(binding.tlOrderRequests.getTabAt(tabPosition))
        }
        binding.rvBidsList.layoutManager = LinearLayoutManager(requireContext())

        when (tabPosition) {
            0 -> {
                showLoader()
                viewModel.getNewBidsList()
            }
            2 -> {
                showLoader()
                viewModel.getAcceptedBidsList()
            }
        }
        setSRL()
        dashboardSharedViewModel.getGoCoinBalance()
    }

    private fun setSRL() {
        binding.srlFEOL.setOnRefreshListener {
            binding.srlFEOL.isRefreshing = false
            showLoader()
            when (binding.tlOrderRequests.selectedTabPosition) {
                0 -> {
                    viewModel.getNewBidsList()
                }
                1 -> {
                    viewModel.getSubmittedBidsList()
                }
                2 -> {
                    viewModel.getAcceptedBidsList()
                }
            }
        }
    }

    private fun clickListeners() {
        binding.backArrowImg.setOnClickListener(this)
        binding.btnAction.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.backArrowImg -> {
                popBackStackAndHideKeyboard()
            }

            R.id.btnAction -> {
                if (isComboPurchased) {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.INVENTORY_FRAGMENT,
                            null),
                        target = R.id.fragment_container_2)
                } else {
                    CommonUtils.addFragmentUtil(requireActivity(),
                        FragmentFactory.fragment(FragmentFactory.Fragments.GMB_INFO, null),
                        target = R.id.fragment_container_2)
                }
            }

            R.id.tvItems -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.tvItems))?.let {
                    (binding.rvBidsList.adapter as? NewBidsListAdapter)?.dataList?.get(it)?.isItemTextExpanded =
                        true
                    binding.rvBidsList.adapter?.notifyItemChanged(it)
                }
            }

            R.id.btnSubmitBid -> {
                CommonUtils.genericCastOrNull<AccRetailBidListResponseModel>(v.getTag(R.id.btnSubmitBid))
                    ?.let {
                        setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_SUBMIT_BID,
                            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACC_BID_ENQUIRY)
                        CommonUtils.addFragmentUtil(requireActivity(),
                            FragmentFactory.fragment(FragmentFactory.Fragments.PLACE_BID,
                                Bundle().apply {
                                    putString(AppENUM.IntentKeysENUM.ORDER_ID, it.id)
                                }),
                            target = R.id.fragment_container_2)
                    }
            }


            R.id.imgCall -> {
                CommonUtils.genericCastOrNull<AccRetailBidListResponseModelSubmitAccept>(v.getTag(R.id.imgCall))
                    ?.let {
                        CommonUtils.openPhoneCallApp("${
                            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                                AppENUM.RefactoredStrings.defaultCountryCode)
                        } ${it.bidingCart?.contactNumber ?: ""}", requireContext())
                    }
            }

            R.id.tvPhoneNumberISB -> {
                CommonUtils.genericCastOrNull<AccRetailBidListResponseModelSubmitAccept>(v.getTag(R.id.tvPhoneNumberISB))
                    ?.let {
                        CommonUtils.openPhoneCallApp("${
                            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                                AppENUM.RefactoredStrings.defaultCountryCode)
                        } ${it.bidingCart?.contactNumber ?: ""}", requireContext())
                    }
            }

            R.id.tvPhoneNumberINEO -> {
                CommonUtils.genericCastOrNull<AccRetailBidListResponseModel>(v.getTag(R.id.tvPhoneNumberINEO))
                    ?.let {
                        CommonUtils.openPhoneCallApp("${
                            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                                AppENUM.RefactoredStrings.defaultCountryCode)
                        } ${it.contactNumber ?: ""}", requireContext())
                    }
            }

            R.id.imgLocateMarker -> {
                CommonUtils.genericCastOrNull<AccRetailBidListResponseModelSubmitAccept>(v.getTag(R.id.imgLocateMarker))
                    ?.let {
                        val mapLik =
                            "https://maps.google.com/?q=${it.bidingCart?.latitude},${it.bidingCart?.longitude}&z=15"
                        CommonUtils.openWebPage(requireContext(), mapLik)
                    }
            }

            R.id.llUnlockMobile -> {
                CommonUtils.genericCastOrNull<AccRetailBidListResponseModel>(v.getTag(R.id.llUnlockMobile))
                    ?.let {
                        showConfirmationDialogForUnlockMobile(it)
                    }
            }

            R.id.llUnlockMobileISB -> {
                CommonUtils.genericCastOrNull<AccRetailBidListResponseModelSubmitAccept>(v.getTag(R.id.llUnlockMobileISB))
                    ?.let {
                        showConfirmationDialogForUnlockMobile(it)
                    }
            }

            R.id.btnMarkComplete -> {
                CommonUtils.genericCastOrNull<AccRetailBidListResponseModelSubmitAccept>(v.getTag(R.id.btnMarkComplete))
                    ?.let {
                        showConfirmationDialogForMarkComplete(it)
                    }
            }

            R.id.btnInvoice -> {
                CommonUtils.genericCastOrNull<AccRetailBidListResponseModelSubmitAccept>(v.getTag(R.id.btnInvoice))
                    ?.let {
                        (BuildConfig.BASE_URL + AppENUM.BID_PDF_END_POINT + it.id).apply {
                            Intent(Intent.ACTION_VIEW).let { intent ->
                                intent.data = Uri.parse(this)
                                startActivity(intent)
                            }
                        }
                    }
            }
        }
    }

    private fun showConfirmationDialogForMarkComplete(bidDetail: AccRetailBidListResponseModelSubmitAccept) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
            Bundle().apply {
                putString(AppENUM.TITLE, getString(R.string.mark_complete))
                putString(AppENUM.DESCRIPTION,
                    getString(R.string.are_you_sure_you_want_to_mark_this_bid_complete))
                putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.yes))
                putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.cancel))
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    if (extra as? Boolean == true) {
                        showLoader()
                        viewModel.markComplete(bidDetail.id.toString())
                        firebaseAcceptRejBidEvent(bidDetail)
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun firebaseAcceptRejBidEvent(data: AccRetailBidListResponseModelSubmitAccept) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACC_BID_ENQUIRY)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.RETAILER_NAME,
                data.bidingCart?.workshopName)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.RETAILER_NUMBER,
                data.bidingCart?.contactNumber)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.TOTAL_COST,
                data.totalAmount.toString())
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_RETAILER_MARK_COMPLETE,
                this)
        }
    }

    private fun showConfirmationDialogForUnlockMobile(bidDetail: AccRetailBidListResponseModel) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
            Bundle().apply {
                putString(AppENUM.TITLE, getString(R.string.unlock_contact_number))
                putString(AppENUM.DESCRIPTION,
                    getString(R.string.unlock_this_contact_number_using_10_gocoins))
                putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.okay))
                putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.cancel))
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    if (extra as? Boolean == true) {
                        showLoader()
                        viewModel.unlockContact(bidDetail.id.toString())
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun showConfirmationDialogForUnlockMobile(bidDetail: AccRetailBidListResponseModelSubmitAccept) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
            Bundle().apply {
                putString(AppENUM.TITLE, getString(R.string.unlock_contact_number))
                putString(AppENUM.DESCRIPTION,
                    getString(R.string.unlock_this_contact_number_using_10_gocoins))
                putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.okay))
                putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.cancel))
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    if (extra as? Boolean == true) {
                        showLoader()
                        viewModel.unlockContact(bidDetail.bidId.toString())
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun setTabLayout() {
        binding.tlOrderRequests.addTab(binding.tlOrderRequests.newTab()
            .apply { text = getString(R.string.new_) })
        binding.tlOrderRequests.addTab(binding.tlOrderRequests.newTab()
            .apply { text = getString(R.string.submitted) })
        binding.tlOrderRequests.addTab(binding.tlOrderRequests.newTab()
            .apply { text = getString(R.string.accepted) })
        binding.tlOrderRequests.addOnTabSelectedListener(this)
    }

    private fun setObservers() {
        setNewBidsListObserver()
        setSubmittedBidsListObserver()
        setAcceptedBidsListObserver()
        setIsUpdateObserver()
        setUnlockPhoneObserver()
        setMarkCompleteObserver()
        observeGetGoCOinResponse()
    }

    private fun observeGetGoCOinResponse() {
        dashboardSharedViewModel.provideGetGoCoinResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.data.let { data ->
                        binding.tvGoCoinBalance.text = (data.gocoinBalance ?: 0).toString()
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setMarkCompleteObserver() {
        viewModel.provideMarkCompleteResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    showLoader()
                    viewModel.getAcceptedBidsList()
                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    private fun setUnlockPhoneObserver() {
        viewModel.provideUnlockContactResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setPageEvent(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_UNLOCK_CONTACT,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACC_BID_ENQUIRY)

                    showLoader()
                    when (binding.tlOrderRequests.selectedTabPosition) {
                        0 -> {
                            viewModel.getNewBidsList()
                        }
                        1 -> {
                            viewModel.getSubmittedBidsList()
                        }
                        2 -> {
                            viewModel.getAcceptedBidsList()
                        }
                    }
                    dashboardSharedViewModel.getGoCoinBalance()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    private fun setAcceptedBidsListObserver() {
        viewModel.provideAcceptedBidsListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.size > 0) {
                        binding.rvBidsList.makeVisible(true)
                        binding.llEmptyList.makeVisible(false)
                        setAcceptedBidsAdapter(it.body.data)
                    } else {
                        showEmptyBidsUI()
                    }
                }
                is Result.Failure -> {
                    showEmptyBidsUI()
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setAcceptedBidsAdapter(data: MutableList<AccRetailBidListResponseModelSubmitAccept>) {
        binding.rvBidsList.adapter = null
        binding.rvBidsList.adapter = AcceptedBidsListAdapter(viewModel.getStringSharedPreference(
            AppENUM.UserKeySaveENUM.WORKSHOP_ID,
            ""),
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol),
            data,
            this)
        binding.rvBidsList.adapter?.notifyDataSetChanged()
    }

    private fun setSubmittedBidsListObserver() {
        viewModel.provideSubmittedBidsListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.size > 0) {
                        binding.rvBidsList.makeVisible(true)
                        binding.llEmptyList.makeVisible(false)
                        setSubmittedBidsAdapter(it.body.data)
                    } else {
                        showEmptyBidsUI()
                    }
                }
                is Result.Failure -> {
                    showEmptyBidsUI()
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setSubmittedBidsAdapter(data: MutableList<AccRetailBidListResponseModelSubmitAccept>) {
        binding.rvBidsList.adapter = null
        binding.rvBidsList.adapter = SubmittedBidsListAdapter(viewModel.getStringSharedPreference(
            AppENUM.UserKeySaveENUM.WORKSHOP_ID,
            ""),
            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol),
            data,
            this)
        binding.rvBidsList.adapter?.notifyDataSetChanged()
    }

    private fun setIsUpdateObserver() {
        viewModel.provideIsUpdateList().observe(viewLifecycleOwner) {
            it?.let {
                if (it) {
                    showLoader()
                    when (binding.tlOrderRequests.selectedTabPosition) {
                        0 -> {
                            viewModel.getNewBidsList()
                        }
                        1 -> {
                            viewModel.getSubmittedBidsList()
                        }
                        2 -> {
                            viewModel.getAcceptedBidsList()
                        }
                    }
                    EventBus.getDefault().post(UpdateHomeScreenEvent())
                }
            }
        }
    }

    private fun setNewBidsListObserver() {
        viewModel.provideBidsListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    if (it.body.data.size > 0) {
                        binding.rvBidsList.makeVisible(true)
                        binding.llEmptyList.makeVisible(false)
                        setNewBidsAdapter(it.body.data)
                    } else {
                        showEmptyBidsUI()
                    }
                }
                is Result.Failure -> {
                    showEmptyBidsUI()
                }
            }
        }
    }

    private fun showEmptyBidsUI() {
        binding.rvBidsList.makeVisible(false)
        binding.llEmptyList.makeVisible(true)
        when (binding.tlOrderRequests.selectedTabPosition) {
            0 -> {
                if (isComboPurchased) {
                    binding.tvNoBids.text = getString(R.string.no_new_requests)
                    binding.tvNoBidsInfo.text =
                        getString(R.string.add_more_inventory_to_get_more_orders)
                    binding.btnAction.text = getString(R.string.add_inventory)
                    binding.btnAction.changeViewCompoundDrawablesWithInterinsicBounds(ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_add_white_large), null, null, null)
                } else {
                    binding.tvNoBids.text = getString(R.string.no_new_requests)
                    binding.tvNoBidsInfo.text =
                        getString(R.string.unlock_google_business_combo_to_get_more_online_orders)
                    binding.btnAction.text = getString(R.string.unlock_now)
                    binding.btnAction.changeViewCompoundDrawablesWithInterinsicBounds(ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_lock_open), null, null, null)
                }
            }
            1 -> {
                if (isComboPurchased) {
                    binding.tvNoBids.text = getString(R.string.no_submitted_bids)
                    binding.tvNoBidsInfo.text =
                        getString(R.string.add_more_inventory_to_get_more_orders)
                    binding.btnAction.text = getString(R.string.add_inventory)
                    binding.btnAction.changeViewCompoundDrawablesWithInterinsicBounds(ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_add_white_large), null, null, null)
                } else {
                    binding.tvNoBids.text = getString(R.string.no_submitted_bids)
                    binding.tvNoBidsInfo.text =
                        getString(R.string.unlock_google_business_combo_to_get_more_online_orders)
                    binding.btnAction.text = getString(R.string.unlock_now)
                    binding.btnAction.changeViewCompoundDrawablesWithInterinsicBounds(ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_lock_open), null, null, null)
                }
            }
            2 -> {
                if (isComboPurchased) {
                    binding.tvNoBids.text = getString(R.string.no_accepted_bids)
                    binding.tvNoBidsInfo.text =
                        getString(R.string.add_more_inventory_to_get_more_orders)
                    binding.btnAction.text = getString(R.string.add_inventory)
                    binding.btnAction.changeViewCompoundDrawablesWithInterinsicBounds(ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_add_white_large), null, null, null)
                } else {
                    binding.tvNoBids.text = getString(R.string.no_accepted_bids)
                    binding.tvNoBidsInfo.text =
                        getString(R.string.unlock_google_business_combo_to_get_more_online_orders)
                    binding.btnAction.text = getString(R.string.unlock_now)
                    binding.btnAction.changeViewCompoundDrawablesWithInterinsicBounds(ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.ic_lock_open), null, null, null)
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setNewBidsAdapter(data: MutableList<AccRetailBidListResponseModel>) {
        binding.rvBidsList.adapter = null
        binding.rvBidsList.adapter =
            NewBidsListAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                ""), data, this)
        binding.rvBidsList.adapter?.notifyDataSetChanged()
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        showLoader()
        when (tab?.position) {
            0 -> {
                viewModel.getNewBidsList()
            }
            1 -> {
                viewModel.getSubmittedBidsList()
            }
            2 -> {
                viewModel.getAcceptedBidsList()
            }
        }
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) { //do nothing
    }

    override fun onTabReselected(tab: TabLayout.Tab?) { // do nothing
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.provideBidsListResponse().postValue(null)
        viewModel.provideSubmittedBidsListResponse().postValue(null)
        viewModel.provideAcceptedBidsListResponse().postValue(null)
    }
}