package whitelisted.garage.view.fragments.managePackages

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.AllPackagesResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentManagePackagesBinding
import whitelisted.garage.eventBus.UpdatePackageEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.CustomPackagesAdapter
import whitelisted.garage.view.fragments.account.AccountFragment
import whitelisted.garage.viewmodels.ManagePackagesViewModel

class ManagePackagesFragment : BaseFragment() {
    private val binding by viewBinding(FragmentManagePackagesBinding::inflate)

    private val viewModel: ManagePackagesViewModel by viewModel()

    private lateinit var customPackagesAdapter: CustomPackagesAdapter
    private var customPackagesList: MutableList<AllPackagesResponseModel> = mutableListOf()
    private var isAccessories = false
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        binding.imgBack.setOnClickListener(this)
        binding.btnCreatePackage.setOnClickListener(this)
        binding.btnCreatePackage2.setOnClickListener(this)
        showLoader()
        customPackagesList.clear()
        viewModel.getCustomPackagesList()
        observeCustomPackagesListResponse()
        observeDeleteWorkshopResponse()
        isAccessories =
            when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
                AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                    true
                }
                else -> {
                    false
                }
            }
    }

    private fun observeDeleteWorkshopResponse() {
        viewModel.provideDeletePackageResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    showLoader()
                    customPackagesList.clear()
                    viewModel.getCustomPackagesList()
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun observeCustomPackagesListResponse() {
        viewModel.provideCustomPackagesListResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    customPackagesList.addAll(it.body.data)
                    lifecycleScope.launch {
                        val bikeCustomPackagesList = viewModel.getCustomPackagesListBike()
                        hideLoader()
                        bikeCustomPackagesList?.let { bikePackages ->
                            if (bikePackages.isNotEmpty()) {
                                customPackagesList.addAll(bikePackages)
                            }
                            if (customPackagesList.isEmpty()) {
                                binding.clEmptyPackage.visibility = View.VISIBLE
                                binding.clPackages.visibility = View.GONE
                            } else {
                                binding.clEmptyPackage.visibility = View.GONE
                                binding.clPackages.visibility = View.VISIBLE
                                setCustomPackagesAdapter()
                            }
                        }
                    }
                }
                is Result.Failure -> CommonUtils.showToast(requireContext(), it.errorMessage, true)
            }
        }
    }

    private fun setCustomPackagesAdapter() {
        binding.rvCustomPackages.layoutManager = LinearLayoutManager(requireContext())
        customPackagesAdapter =
            CustomPackagesAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default)), customPackagesList, this)
        customPackagesAdapter.isAccessories = isAccessories
        binding.rvCustomPackages.adapter = customPackagesAdapter


    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgBack -> removeAllFragmentsTill(AccountFragment::class.java.simpleName)
            R.id.btnCreatePackage, R.id.btnCreatePackage2 -> {
                Bundle().apply {
                    putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                        FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_MANAGE_PACKAGE)
                    FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_CREATE_PACKAGE,
                        this)
                }
                CommonUtils.addFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.CREATE_PACKAGES, Bundle().apply {
                        putInt(AppENUM.PACKAGE_COUNT, customPackagesList.size)
                    }),
                    target = R.id.fragment_container_2)
            }
            R.id.imgEdit -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    CommonUtils.genericCastOrNull<AllPackagesResponseModel>(v.getTag(R.id.model))
                        ?.let { extra ->
                            editPackage(position, extra)
                        }
                }
            }

            R.id.imgDel -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    CommonUtils.genericCastOrNull<AllPackagesResponseModel>(v.getTag(R.id.model))
                        ?.let { model ->
                            showConfirmationDialog(position, model)
                        }
                }
            }
        }
    }

    private fun showConfirmationDialog(position: Int, model: AllPackagesResponseModel) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
            Bundle().apply {
                putString(AppENUM.TITLE, getString(R.string.delete_package))
                putString(AppENUM.DESCRIPTION,
                    getString(R.string.are_you_sure_you_want_to_delete_this_npackage))
                putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.delete))
                putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.cancel))
            },
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) {
                    if (extra as Boolean) {
                        showLoader()
                        if (isAccessories) {
                            viewModel.deletePackage(customPackagesList[position].id.toString(),
                                model.isTwoWheeler ?: false)
                        } else {
                            viewModel.deletePackage(customPackagesList[position].packageId.toString(),
                                model.isTwoWheeler ?: false)
                        }
                    }
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun editPackage(position: Int, extra: AllPackagesResponseModel) {
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                CommonUtils.addFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.CREATE_EDIT_PACKAGE,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_EDIT, true)
                            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER,
                                extra.isTwoWheeler ?: false)
                            putString(AppENUM.IntentKeysENUM.PACKAGE_NAME, extra.name)
                            putString(AppENUM.IntentKeysENUM.PACKAGE_ID,
                                customPackagesList[position].packageId.toString())
                        }),
                    target = R.id.fragment_container_2)

            }
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                CommonUtils.addFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.CREATE_EDIT_ACCESS_PACKAGE,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_EDIT, true)
                            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER,
                                extra.isTwoWheeler ?: false)
                            putString(AppENUM.IntentKeysENUM.PACKAGE_NAME, extra.name)
                            putString(AppENUM.IntentKeysENUM.PACKAGE_ID,
                                customPackagesList[position].id)
                        }),
                    target = R.id.fragment_container_2)

            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {

                CommonUtils.addFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.CREATE_EDIT_PACKAGE,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_EDIT, true)
                            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER,
                                extra.isTwoWheeler ?: false)
                            putString(AppENUM.IntentKeysENUM.PACKAGE_NAME, extra.name)
                            putString(AppENUM.IntentKeysENUM.PACKAGE_ID,
                                customPackagesList[position].packageId.toString())
                        }),
                    target = R.id.fragment_container_2)
            }
        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is UpdatePackageEvent -> {
                showLoader()
                customPackagesList.clear()
                viewModel.getCustomPackagesList()
            }
        }
    }


}