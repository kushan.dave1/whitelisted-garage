package whitelisted.garage.view.fragments.orderHistory

import DateUtil
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.OrderListResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentOrderHistoryBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.CommonUtils.onScrolledToBottom
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.OrderHistoryAdapter
import whitelisted.garage.view.fragments.sparesShop.SparesOrderHistoryFragment
import whitelisted.garage.viewmodels.OrderHistoryFragmentViewModel

class OrderHistoryFragment : BaseFragment(), AdapterView.OnItemSelectedListener {

    private val binding by viewBinding(FragmentOrderHistoryBinding::inflate)
    private val viewModel: OrderHistoryFragmentViewModel by viewModel()
    private lateinit var ordersList: MutableList<OrderListResponseModel>
    private var orderType: String? = null
    private var startDate: String? = null
    private var endDate: String? = null
    private var isSpinnerActive = false
    private lateinit var orderHistoryAdapter: OrderHistoryAdapter
    private val filterList = AppENUM.FILTER_LIST_ORDER_HISTORY
    private var isMarketplace = false
    private var limit = 10

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setData() {
        setSelectedTab(50)
        ordersList = mutableListOf()
        ArrayAdapter(requireContext(), R.layout.layout_drop_down, filterList).apply {
            binding.filterSpinner.adapter = this
        }
        if (::orderHistoryAdapter.isInitialized) {
            when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                    orderHistoryAdapter.viewType = 0
                }
                AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                    orderHistoryAdapter.viewType = 0
                }
                AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                    orderHistoryAdapter.viewType = 1
                }
            }
        }
    }

    private fun observeData() {
        viewModel.provideOrderListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    ordersList.clear()
                    ordersList = it.body.data.order_history?.toMutableList() ?: mutableListOf()
                    if (ordersList.size < limit) {
                        isLastPageRV = true
                    }
                    isLoadingRV = false

                    binding.noDataTv.makeVisible(false)
                    binding.totleOrder.text = getString(R.string.total_orders_)
                    binding.totleOrder.append(" ")
                    binding.totleOrder.append(it.body.count.toString())
                    setAdapter()
                    if (::orderHistoryAdapter.isInitialized && orderHistoryAdapter.dataList.size > 0) {
                        binding.tvDownloadSheet.makeVisible(true)
                    } else binding.tvDownloadSheet.makeVisible(false)


                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
        viewModel.provideDownloadCSVResponse().observe(viewLifecycleOwner) {
            CommonUtils.showToast(requireContext(), getString(R.string.downloading), true)
            CommonUtils.saveToDisk(requireActivity(), it, "${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME, "")
            }-OrderHistoryCSV.csv", "${
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME, "")
            }-OrderHistoryCSV.csv", getString(R.string.download_order_history_csv))
        }
    }

    private fun setAdapter() = binding.apply {
        if (ordersList.size > 0 && rvOrdersList.adapter == null) {
            orderHistoryAdapter =
                OrderHistoryAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    getString(R.string.currency_symbol_default)), this@OrderHistoryFragment)
            binding.rvOrdersList.adapter = orderHistoryAdapter
            orderHistoryAdapter.setData(ordersList)
        } else if (ordersList.size == 0 && rvOrdersList.adapter == null) {
            noDataTv.makeVisible(true)
            totleOrder.text = getString(R.string.total_orders_zwero)
        } else {
            orderHistoryAdapter.addData(ordersList)
        }
    }

    private fun setupScreen() {
        clickListener()
        observeData()
        setData()
        orderType = AppENUM.TODAY
        getOrderHistoryList(0)
        childFragmentManager.beginTransaction()
            .add(R.id.fragmentContainerFOH, SparesOrderHistoryFragment().apply {
                arguments = Bundle().apply { putBoolean(AppENUM.IS_NESTED, true) }
            }).commitAllowingStateLoss()
        if (isInternationalUser) binding.llCartTabs.makeVisible(false)

        setRecyclerPagination()
    }

    private fun setRecyclerPagination() {
        binding.rvOrdersList.onScrolledToBottom {
            if (!isLoadingRV && !isLastPageRV) {
                isLoadingRV = true
                getOrderHistoryList(orderHistoryAdapter.dataList.size)
            }
        }
    }

    private fun getOrderHistoryList(offset: Int) {
        if (offset == 0) {
            binding.rvOrdersList.adapter = null
            isLastPageRV = false
        }
        showLoader()
        viewModel.getFilteredOrdersList(orderType, startDate, endDate, limit, offset)
    }

    private fun setSelectedTab(delay: Long = 0) = lifecycleScope.launch {
        kotlinx.coroutines.delay(delay)
        binding.tvGarageTab.isSelected = !isMarketplace
        binding.tvMarketplaceTab.isSelected = isMarketplace
        binding.cvTabBg.animate()
            .translationX(if (isMarketplace) binding.tvMarketplaceTab.x - 10 else binding.tvGarageTab.x - 10)
            .setDuration(150).start()
        binding.clSearchItem.makeVisible(!isMarketplace)
        binding.fragmentContainerFOH.makeVisible(isMarketplace)

        if (::orderHistoryAdapter.isInitialized && orderHistoryAdapter.dataList.size > 0 && !isMarketplace) binding.tvDownloadSheet.makeVisible(
            true)
        else binding.tvDownloadSheet.makeVisible(false)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun clickListener() {
        binding.imgBack.setOnClickListener(this)
        binding.tvDownloadSheet.setOnClickListener(this)
        binding.llCreateOrder.setOnClickListener(this)
        binding.tvMarketplaceTab.setOnClickListener(this)
        binding.tvGarageTab.setOnClickListener(this)
        binding.filterSpinner.onItemSelectedListener = this
        binding.filterSpinner.setOnTouchListener { _, _ ->
            isSpinnerActive = true
            false
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.clBase -> CommonUtils.genericCastOrNull<OrderListResponseModel>(v.getTag(R.id.model))
                ?.let {
                    recyclerViewClick(it)
                }
            R.id.imgBack -> popBackStackAndHideKeyboard()

            R.id.tvDownloadSheet -> viewModel.downloadCSV(orderType, startDate, endDate)

            R.id.llCreateOrder -> handleCreateOrderClick()

            R.id.tvGarageTab -> {
                isMarketplace = false
                setSelectedTab()
            }

            R.id.tvMarketplaceTab -> {
                isMarketplace = true
                setSelectedTab()
            }
        }
    }

    private fun handleCreateOrderClick() {
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE, "")) {
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_NEW_ORDER,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                        }),
                )
            }
            else -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                        Bundle().apply {
                            putBoolean(AppENUM.IntentKeysENUM.IS_NEW_ORDER, true)
                        }),
                )
            }
        }
    }

    private fun recyclerViewClick(model: OrderListResponseModel) {
        when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE)) {
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_DETAIL_WRAPPER,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.ORDER_ID, model.orderId)
                        }),
                )
            }
            else -> {
                CommonUtils.addFragmentUtil(
                    activity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_SUMMARY,
                        Bundle().apply {
                            putString(AppENUM.IntentKeysENUM.ORDER_ID, model.orderId)
                        }),
                )
            }
        }

    }

    override fun onItemSelected(parent: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        if (isSpinnerActive) {
            isSpinnerActive = false
            when (position) {
                0 -> {
                    getDataForSingleDates(AppENUM.TODAY)
                    handleSpinnerAccordingToPosition(position)
                }
                1 -> {
                    getDataForSingleDates(AppENUM.YESTERDAY)
                    handleSpinnerAccordingToPosition(position)
                }
                2 -> {
                    getDataForSingleDates(AppENUM.MONTH)
                    handleSpinnerAccordingToPosition(position)
                }
                3 -> {
                    orderType = AppENUM.CUSTOM
                    showSelectDateDialog(true)
                }
                else -> {
                    getDataForSingleDates(AppENUM.TODAY)
                }
            }
        }
    }

    private fun handleSpinnerAccordingToPosition(position: Int) {
        filterList.removeAt(3)
        filterList.add(3, getString(R.string.custom))
        binding.filterSpinner.adapter =
            ArrayAdapter(requireContext(), R.layout.layout_drop_down, filterList)
        binding.filterSpinner.setSelection(position)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) { //
    }

    private fun getDataForSingleDates(type: String) {
        startDate = null
        endDate = null
        orderType = type
        getOrderHistoryList(0)
    }

    private fun showSelectDateDialog(isStart: Boolean) {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_DATE, Bundle().apply {
            putBoolean(AppENUM.IS_START, isStart)
            if (!isStart) putString(AppENUM.START_TIME,
                CommonUtils.returnMillisForDate(startDate).toString())
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                if (isStart) {
                    startDate = extra as? String
                    showSelectDateDialog(false)
                } else {
                    endDate = extra as? String
                    filterList.removeAt(3)
                    filterList.add(3,
                        DateUtil.getFormattedDateUtcToGmt(AppENUM.DateUtilKey.DD_MM_YYYY,
                            AppENUM.DateUtilKey.DD_MM,
                            startDate
                                ?: "") + "-" + DateUtil.getFormattedDateUtcToGmt(AppENUM.DateUtilKey.DD_MM_YYYY,
                            AppENUM.DateUtilKey.DD_MM,
                            endDate ?: ""))
                    binding.filterSpinner.adapter =
                        ArrayAdapter(requireContext(), R.layout.layout_drop_down, filterList)
                    binding.filterSpinner.setSelection(3)
                    getOrderHistoryList(0)
                }
            }
        })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }
}