package whitelisted.garage.view.fragments.reminder

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.SendInstantReminderRequest
import whitelisted.garage.api.response.GetCustomerReminderResponse
import whitelisted.garage.api.response.PreviousReminder
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.FragmentCusReminderBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.CustomerReminderAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.InstantReminderViewModel

class CustomerRemindersFragment : BaseFragment() {

    private val binding by viewBinding2(FragmentCusReminderBinding::inflate)
    private val viewModel: InstantReminderViewModel by viewModel()
    private val dashboardSharedViewModel: DashboardSharedViewModel by sharedViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListener()
        observeResponses()
        showLoader()
        viewModel.getAllCustomerReminder()
    }

    private fun observeResponses() {
        observeAllCustomerReminder()
        observeSendInstantReminder()
        observeDueCollectResponse()
    }

    private fun observeDueCollectResponse() {
        viewModel.provideCollectDueResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    showLoader()
                    viewModel.getAllCustomerReminder()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    private fun observeSendInstantReminder() {
        viewModel.provideSendReminderResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    showLoader()
                    viewModel.getAllCustomerReminder()
                    openSuccessDialog()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    private fun openSuccessDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.REMINDER_SENT_SUCCESS,
            null,
            object : ActionListener {
                override fun onActionItem(extra: Any?, extra2: Any?) { //
                }
            })?.let { dialog ->
            dialog.show(childFragmentManager, dialog.javaClass.name)
        }
    }

    private fun observeAllCustomerReminder() {
        viewModel.provideGetAllReminderResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    setData(it.body.data)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setData(data: GetCustomerReminderResponse) {
        data.let {
            if (!it.previousReminder.isNullOrEmpty()) {
                binding?.rvCustomerReminder?.adapter =
                    CustomerReminderAdapter(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default)), it.previousReminder, this)
                binding?.tvTotalCustomer?.text =
                    "${(data.previousReminder?.size ?: 0)} ${getString(R.string.customers)}"
            } else {
                binding?.tvTotalCustomer?.text = "0 ${getString(R.string.customers)}"
            }
        }
    }

    private fun clickListener() {
        binding?.imgMenu?.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imgMenu -> {
                dashboardSharedViewModel.provideOpenInstantReminder().postValue(true)
                popBackStackAndHideKeyboard()
            }
            R.id.llCollectDue -> {
                CommonUtils.genericCastOrNull<PreviousReminder>(v.getTag(R.id.model))?.let {
                    FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.GENERIC_CONFIRMATION,
                        Bundle().apply {
                            putString(AppENUM.TITLE, getString(R.string.collect_due))
                            putString(AppENUM.DESCRIPTION,
                                getString(R.string.credit_due_clear_description))
                            putString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.yes))
                            putString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.no))
                        },
                        object : ActionListener {
                            override fun onActionItem(extra: Any?, extra2: Any?) {
                                if (extra as Boolean) {
                                    showLoader()
                                    viewModel.collectDueOnReminder(it.apply {
                                        this.due_collected = true
                                    })

                                }
                            }
                        })?.let { dialog ->
                        dialog.show(childFragmentManager, dialog.javaClass.name)
                    }
                }
            }
            R.id.llSendReminder -> {
                CommonUtils.genericCastOrNull<PreviousReminder>(v.getTag(R.id.model))?.let {
                    showLoader()
                    viewModel.sendInstantReminder(SendInstantReminderRequest().apply {
                        this.amount = it.amount
                        this.mobile = it.mobile
                        this.customerName = it.customerName
                        this.countryCode = it.countryCode
                        this.textMsg = it.textMsg
                        this.language = it.language
                        firebaseEventSendCusReminder(it.customerName.toString(),
                            it.amount.toString(),
                            it.mobile.toString(),
                            it.textMsg.toString())
                    })
                }
            }
        }
    }

    private fun firebaseEventSendCusReminder(name: String,
        amount: String,
        mobile: String,
        message: String) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_CUS_REMINDER)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.AMOUNT, amount)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.MESSAGE, message)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_NAME, name)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.MODEL, mobile)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_PICK_COUNTRY,
                this)
        }
    }

    override fun onResume() {
        super.onResume()
        onBackPress()
    }

    private fun onBackPress() {
        view?.isFocusableInTouchMode = true
        view?.requestFocus()
        view?.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                dashboardSharedViewModel.provideOpenInstantReminder().postValue(true)
                requireActivity().onBackPressed()
                true
            } else false
        }
    }

}