package whitelisted.garage.view.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.MembershipBenefits
import whitelisted.garage.api.response.MembershipList
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.databinding.BenefitsCardBinding
import whitelisted.garage.databinding.ExtraDiscountBrandCardBinding
import whitelisted.garage.databinding.FragmentSubscriptionPageBinding
import whitelisted.garage.databinding.FreePremiumCardBinding
import whitelisted.garage.databinding.ItemProComparisionBinding
import whitelisted.garage.databinding.MembershipBenefitsCardBinding
import whitelisted.garage.eventBus.MembershipPurchasedEvent
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.dp
import whitelisted.garage.utils.CommonUtils.isVisible
import whitelisted.garage.utils.CommonUtils.makeInVisible
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.CommonUtils.toDate
import whitelisted.garage.view.activities.DashboardActivity
import whitelisted.garage.view.adapter.GoCoinsFAQAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel
import java.util.Date

class SubscriptionInfoFragment : BaseFragment() {

    private val binding by viewBinding(FragmentSubscriptionPageBinding::inflate)
    private val viewModel by sharedViewModel<SparesShopFragmentViewModel>()
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var comparisons: List<MembershipList> = listOf()
    private var workshop: WorkshopListResponseModel? = null
    private var isPaymentFailed: Boolean = false


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
        clickListeners()
    }

    private fun clickListeners() {
        binding.clBuyNow.setOnClickListener(this)
        binding.btnlMembershipStatus.setOnClickListener(this)
        binding.imgBack.setOnClickListener(this)

    }

    private fun setupScreen() = lifecycleScope.launch {
        showLoader()
        isPaymentFailed = arguments?.getBoolean(AppENUM.PAYMENT_FAILED,false) ?: false
        val id = viewModel.getSharedPreference()
            .getString(requireContext(), AppENUM.UserKeySaveENUM.WORKSHOP_ID)
        workshop = viewModel.getWorkshopStatus(id)
        getConfigs()
        setMembershipStatus()
        hideLoader()
    }

    private suspend fun getConfigs() = viewModel.getMembershipConfigs()?.let { config ->

        val symbol = viewModel.getStringSharedPreference(
            AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            getString(R.string.currency_symbol_default)
        )

        binding.lblTop.text = config.topHeader
        binding.tvSubHeader.text = config.subHeading
        binding.rvComparision.setLayoutManagerAsLinear()

        binding.lblBenefits.text = config.proBenefitsTitle
        binding.lblMembershipBenefits.text = config.membershipBenefitsTitle
        binding.lblExtraDiscount.text = config.extraDiscountTitle

        binding.tvMRP.text = resources.getString(R.string.concatenate_string_without_space, symbol, config.mrp)
        binding.tvMRP2.text = resources.getString(R.string.concatenate_string_without_space, symbol, config.mrp)

        binding.tvAmount.text =
            resources.getString(R.string.concatenate_string_without_space, symbol, config.offerPrice + " " + config.offerPriceText)
        binding.tvAmount2.text =
            resources.getString(R.string.concatenate_string_without_space, symbol, config.offerPrice + " " + config.offerPriceText)

        binding.llMembershipBenefits.makeVisible(!config.membershipBenefits.isNullOrEmpty())
        binding.rvMembershipBenefits.setLayoutManagerAsLinear()
        binding.rvMembershipBenefits.addViews(config.membershipBenefits ?: listOf(),
            MembershipBenefitsCardBinding::inflate,:: membershipBenefitsBinder)

        binding.rvBenefitsWithPro.setLayoutManagerAsGrid(3)
        binding.rvBenefitsWithPro.clear().addViews(
            config.proBenefits ?: listOf(),
            BenefitsCardBinding::inflate
        ) { card, model, _ ->
            ImageLoader.loadImage(card.ivBenefit, model.icon)
            card.tvBenefit.text = model.name

        }

        binding.llExtraDiscount.makeVisible(!config.extraDiscountBrands.isNullOrEmpty())
        binding.rvExtraDescBrands.clear().addViewsInfiniteScroll(
            config.extraDiscountBrands ?: listOf(),
            ExtraDiscountBrandCardBinding::inflate
        ) { card, model, _ ->
            card.ivBrand.load(model.icon)
            card.tvBrand.text = model.name
            card.tvDiscount.text = model.discount
        }

        binding.lblProCompare.text = config.proCompareTitle
        binding.llComparison.makeVisible(!config.proCompareList.isNullOrEmpty())
        binding.rvProCompare.setLayoutManagerAsLinearHorizontal()
        binding.rvProCompare.clear().addViews(
            config.proCompareList ?: listOf(),
            FreePremiumCardBinding::inflate
        ) { card, model, _ ->
            card.tvTitle.text = model.title
            card.tvFree.text = model.basic
            card.tvPremium.text = model.premium
            card.lblPremium.text = model.premiumHeader
            card.lblFree.text = model.basicHeader
        }

        attachRecyclerviewIndicator()

        binding.tvMembershipPrice.text =
            resources.getString(R.string.concatenate_string_without_space, symbol, config.offerPrice)

        comparisons = config.membershipList?.filter { it.isActive ?: false } ?: listOf()
        if(comparisons.isNotEmpty()) {
            binding.lblWYG.text = comparisons[0].name
            binding.lblClPro.text = comparisons[0].premium
            binding.lblBasic.text = comparisons[0].basic

            comparisons = comparisons.subList(1,comparisons.size)
        }


        showComparisionList(comparisons.take(7))

        binding.tvViewAll.setOnClickListener {
            if (binding.rvComparision.adapter?.itemCount == 7) {
                showComparisionList(comparisons)
                binding.tvViewAll.setText(resources.getString(R.string.see_less))
                binding.tvViewAll.setDrawableRotation(180f)

            } else {
                showComparisionList(comparisons.take(7))
                binding.tvViewAll.setText(resources.getString(R.string.see_more))
                binding.tvViewAll.setDrawableRotation(0f)
            }
        }

        binding.clFAQ.makeVisible(!config.faq.isNullOrEmpty())
        binding.rvFAQ.layoutManager = LinearLayoutManager(requireContext())
        binding.rvFAQ.adapter = GoCoinsFAQAdapter(config.faq ?: mutableListOf())
        binding.imgToggle.setOnClickListener {
            expandCollapseTrack()
        }
        expandCollapseTrack()

    }

    private fun attachRecyclerviewIndicator() {
        lifecycleScope.launch {
            delay(1000)
            binding.rvsBikeCategories.attachToRecyclerView(binding.rvProCompare)
        }
    }

    private fun expandCollapseTrack() = binding.apply {
        val isExpanded = rvFAQ.isVisible()
        rvFAQ.makeVisible(!isExpanded)
        imgToggle.animate().rotation(if (isExpanded) 0f else 180f).setDuration(500).start()

    }

    private fun showComparisionList(list: List<MembershipList>) {
        binding.clWhatYouGet.makeVisible(list.isNotEmpty())
        binding.rvComparision.clear().addViews(
            list,
            ItemProComparisionBinding::inflate
        ) { card, model, pos ->
            card.tvFeature.text = model.name
            card.tvBasic.text = model.basic
            card.tvPremium.makeVisible(!model.premium.toBoolean())
            card.ivTick.makeVisible(model.premium.toBoolean())
            card.tvPremium.text = model.premium
            card.vDivider.makeVisible(pos < list.size - 1)

        }
    }

    private fun membershipBenefitsBinder(card: MembershipBenefitsCardBinding, model : MembershipBenefits, pos: Int) {
        val headerColor = Color.parseColor(model.headerColor)
        card.clMain.setViewBackgroundColor(model.bgColor)
        card.tvBenefitsHeading.setTextColor(headerColor)
        card.tvDescription.setTextColor(Color.parseColor(model.subHeaderColor))
        card.tvCheckNow.setTextColor(headerColor)
        card.tvKnowMore.setTextColor(headerColor)
        card.ivDottedLine.imageTintList = ColorStateList.valueOf(headerColor)
        TextViewCompat.setCompoundDrawableTintList(
            card.tvCheckNow,
            ColorStateList.valueOf(headerColor)
        )

        ImageLoader.loadImage(card.ivIcon, model.icon, null)

        card.tvBenefitsHeading.text = model.header
        card.tvDescription.text = model.subHeader

        card.root.setOnClickListener {
            val intent = Intent(requireContext(), DashboardActivity::class.java)
            intent.data = Uri.parse(model.deeplink)
            startActivity(intent)

            if(workshop?.membershipStatus != true) {
                fireCheckNowEvent()
            } else if(model.id == "gmb") {
                fireGMBProApplied()
            }
        }

        if ((model.id == "gmb" && workshop?.gmbLinks != null)
            || (model.id != "gmb" && workshop?.membershipStatus == true)) {

            card.tvKnowMore.makeVisible(true)
            card.ivDottedLine.makeVisible(true)

            card.tvCheckNow.compoundDrawablePadding = 6f.dp.toInt()

            card.tvCheckNow.setCompoundDrawablesWithIntrinsicBounds(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_check_circle),
                null, null, null
            )

            card.tvCheckNow.text = resources.getString(R.string.activated)


        } else if (model.id == "gmb" && workshop?.openTime != null) {
            card.tvKnowMore.makeVisible(true)
            card.ivDottedLine.makeVisible(true)
            card.tvCheckNow.compoundDrawablePadding = 6f.dp.toInt()

            card.tvCheckNow.setCompoundDrawablesWithIntrinsicBounds(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_in_progress),
                null, null, null
            )

            card.tvCheckNow.text = resources.getString(R.string.in_progress)

        } else if(model.id == "gmb" && workshop?.membershipStatus == true) {
            card.tvCheckNow.text = resources.getString(R.string.setup_now)
            card.tvKnowMore.makeVisible(true)
            card.ivDottedLine.makeVisible(true)
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.clBuyNow, R.id.btnlMembershipStatus -> {
                firePurchaseMembershipEvent()
                if(workshop?.membershipStatus == true) return
                dashboardViewModel.screensConfigResponse.value?.membership?.membershipPrice?.let { price ->
                    Bundle().apply {
                        putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                        putString(
                            AppENUM.IS_FROM,
                            AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_PRO
                        )
                        putString(AppENUM.AMOUNT_TO_BE_PURCHASED, price)
                        CommonUtils.addFragmentUtil(
                            activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, this)
                        )
                    }
                }
            }
            R.id.imgBack -> popBackStackAndHideKeyboard()
        }
    }

    private fun setMembershipStatus() = workshop?.also {
        when {

            isPaymentFailed -> binding.apply {
                clBuyNow.makeVisible(false)
                tvMembershipActiviated.makeVisible(false)
                clMembershipStatus.makeVisible(true)
                retryGroup.makeVisible(true)
                tvMessageText.setTextColor(ContextCompat.getColor(requireContext(),R.color.driver_color))
                tvMessageText.text = resources.getString(R.string.your_payment_failed)
                tvRetry.text = resources.getString(R.string.retry_now)

            }

            it.membershipStatus ?: false -> binding.apply {

                clBuyNow.makeVisible(false)
                clMembershipStatus.makeVisible(true)
                tvMembershipActiviated.makeVisible(true)
                retryGroup.makeVisible(false)
                tvMessageText.setTextColor(ContextCompat.getColor(requireContext(),R.color.colorAccent))
                tvMessageText.text = resources.getString(R.string.concatenate_string,
                    resources.getString(R.string.valid_till),
                    CommonUtils.formatDateNormal3(workshop?.membershipEndAt))


            }

            (workshop?.membershipEndAt?.toDate()?.time?: Long.MAX_VALUE) < System.currentTimeMillis() -> binding.apply {

                clBuyNow.makeVisible(false)
                tvMembershipActiviated.makeVisible(false)
                clMembershipStatus.makeVisible(true)
                retryGroup.makeVisible(true)
                tvMessageText.setTextColor(ContextCompat.getColor(requireContext(),R.color.driver_color))
                tvMessageText.text = resources.getString(R.string.your_membership_has_expired)
                tvRetry.text = resources.getString(R.string.renew_now)

            }

            else -> {
                binding.clMembershipStatus.makeVisible(false)
                binding.clBuyNow.makeVisible(true)
            }


        }
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is MembershipPurchasedEvent -> {
                isPaymentFailed = !(event.extra as Boolean)
                if(isPaymentFailed) setMembershipStatus() else resfreshMembershipBenefits()
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun resfreshMembershipBenefits() = lifecycleScope.launch {
        showLoader()
        val id = viewModel.getSharedPreference()
            .getString(requireContext(), AppENUM.UserKeySaveENUM.WORKSHOP_ID)
        workshop = viewModel.getWorkshopStatus(id)
        setMembershipStatus()
        binding.rvMembershipBenefits.adapter?.notifyDataSetChanged()
        hideLoader()
    }


    private fun firePurchaseMembershipEvent() {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.SUBSCRIPTION_INFO)
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.PURCHASE_MEMBERSHIP,
            bundle)
    }


    private fun fireGMBProApplied() {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.SUBSCRIPTION_INFO)
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.GMB_PRO_APPLIED,
            bundle)
    }

    private fun fireCheckNowEvent() {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.SUBSCRIPTION_INFO)
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.CLICK_CHECK_NOW,
            bundle)
    }


}