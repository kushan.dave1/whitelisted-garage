package whitelisted.garage.view.fragments.selectAddress

import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.setFragmentResult
import kotlinx.android.synthetic.main.fragment_select_address.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.AddressData
import whitelisted.garage.api.response.AddressResponseModel
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.databinding.FragmentSelectAddressBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.AddressesAdapter
import whitelisted.garage.viewmodels.SelectAddressViewModel

class SelectAddressFragment : BaseBottomSheetFragment() {

    private val binding by viewBinding(FragmentSelectAddressBinding::inflate)
    private val viewModel: SelectAddressViewModel by viewModel()
    private lateinit var addressList: MutableList<AddressResponseModel>
    private lateinit var selectedAddress: AddressResponseModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isCancelable = true
        setupScreen()
    }

    private fun setupScreen() {
        binding.btnSave.setOnClickListener(this)
        binding.tvAddAddress.setOnClickListener(this)

        setObservers()
        arguments?.getParcelable<AddressResponseModel>(AppENUM.ADDRESS_POST)?.let {
            selectedAddress = it
        }

        binding.loaderFSA.llMaterialLoader.makeVisible(true)
        viewModel.getAllAddresses()
    }

    private fun setObservers() {
        getAllAddressObserver()
        deleteAddressObserver()
        addAddressObserver()
    }

    private fun addAddressObserver() {
        viewModel.provideAddAddressResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    selectAddressSuccess()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun deleteAddressObserver() {
        viewModel.provideDeleteAddressResponse().observe(viewLifecycleOwner) {
            binding.loaderFSA.llMaterialLoader.makeVisible(false)
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    binding.loaderFSA.llMaterialLoader.makeVisible(true)
                    viewModel.getAllAddresses()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun getAllAddressObserver() {
        viewModel.provideAddressesResponse().observe(viewLifecycleOwner) {
            binding.loaderFSA.llMaterialLoader.makeVisible(false)
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        addressList = it.body.data
                        setAddressAdapter(it.body.data)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun selectAddressSuccess() {
        if (::selectedAddress.isInitialized) {
            if (arguments?.getBoolean(AppENUM.FROM_SS_PAYMENT, false) == true) {
                setFragmentResult(AppENUM.SELECT_ADDRESS_PAYMENT, Bundle().apply {
                    putParcelable(AppENUM.ADDRESS_POST, selectedAddress)
                })
            } else if (arguments?.getBoolean(AppENUM.IS_FROM_PDP, false) == true) {
                setFragmentResult(AppENUM.SELECT_ADDRESS_PDP, Bundle().apply {
                    putParcelable(AppENUM.ADDRESS_POST, selectedAddress)
                })
            } else if (arguments?.getBoolean(AppENUM.SELECT_ADDRESS_CART, false) == true) {
                setFragmentResult(AppENUM.SELECT_ADDRESS_CART, Bundle().apply {
                    putParcelable(AppENUM.ADDRESS_POST, selectedAddress)
                })
            } else if (arguments?.getBoolean(AppENUM.SELECT_ADDRESS_ENQUIRY, false) == true) {
                setFragmentResult(AppENUM.SELECT_ADDRESS_ENQUIRY, Bundle().apply {
                    putParcelable(AppENUM.ADDRESS_POST, selectedAddress)
                })
            } else {
                setFragmentResult(AppENUM.SELECT_ADDRESS, Bundle().apply {
                    putParcelable(AppENUM.ADDRESS_POST, selectedAddress)
                })
            }
            dismiss()
        }
    }

    private fun setAddressAdapter(data: MutableList<AddressResponseModel>) {
        var selAddress = ""
        arguments?.getParcelable<AddressResponseModel>(AppENUM.ADDRESS_POST)?.let {
            selAddress = it.addressId ?: ""
            binding.cbDefaultAddress.isChecked = it.isDefault == true
        }
        binding.rvAddresses.adapter = AddressesAdapter(selAddress, data, this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnSave -> {
                if (::selectedAddress.isInitialized) {
                    if (cbDefaultAddress.isChecked) {
                        selectedAddress.isDefault = true
                        viewModel.editAddressAPI(selectedAddress)
                    } else {
                        selectedAddress.isDefault = false
                        viewModel.editAddressAPI(selectedAddress)
                    }
                } else {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.please_select_an_address))
                }
            }

            R.id.tvAddAddress -> {
                dismiss()
                Bundle().apply { //                    this.putString(AppENUM.NAME, etName?.text.toString())
                    //                    this.putString(AppENUM.ADDRESS_POST, etAddress?.text.toString())
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.ADDRESS_FRAGMENT, this))
                }
            }

            R.id.imgMore -> {
                CommonUtils.genericCastOrNull<AddressResponseModel>(v.getTag(R.id.model))?.let {
                    showPopupMenu(v, it)
                }
            }

            R.id.clBaseIA -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let {
                    selectedAddress = addressList[it]
                    binding.cbDefaultAddress.isChecked = selectedAddress.isDefault == true
                }
            }
        }
    }

    private fun showPopupMenu(view: View, addressResponseModel: AddressResponseModel) {
        val popupMenu =
            PopupMenu(ContextThemeWrapper(requireContext(), R.style.BasePopupMenu), view)
        popupMenu.menuInflater.inflate(R.menu.edit_del_menu, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.edit -> {
                    dismiss()
                    requireActivity().let {
                        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.ADD_EDIT_ADDRESS,
                            Bundle().apply {
                                putParcelable(AppENUM.ADDRESS_POST,
                                    AddressData(addressResponseModel.latitude,
                                        addressResponseModel.longitude,
                                        addressResponseModel.address))
                                putBoolean(AppENUM.IntentKeysENUM.IS_EDIT, true)
                                putString(AppENUM.ADDRESS_ID, addressResponseModel.addressId)
                                putString(AppENUM.ADDRESS_TYPE, addressResponseModel.address_type)
                            })?.let { baseFragment ->
                            baseFragment.show(it.supportFragmentManager,
                                baseFragment.javaClass.name)
                        }
                    }
                }
                R.id.delete -> {
                    binding.loaderFSA.llMaterialLoader.makeVisible(true)
                    addressResponseModel.addressId?.let {
                        viewModel.deleteAddress(it)
                    }
                }
            }
            true
        }
        popupMenu.show()
    }
}