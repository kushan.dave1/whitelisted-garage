package whitelisted.garage.view.dialog

import android.os.Bundle
import android.view.View
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogSelectDateBinding
import whitelisted.garage.utils.AppENUM
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class SelectDateForReminderDialog(private val listener: ActionListener?) : BaseDialogFragment() {

    private val binding by viewBinding(DialogSelectDateBinding::inflate)
    private var startDate: String? = null
    private var isSetUpDone = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startDate = arguments?.getString(AppENUM.START_DATE)
        isCancelable = true
        setupScreen()
        isSetUpDone = true
    }

    private fun setupScreen() {
        val today = Calendar.getInstance()

        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        val date = simpleDateFormat.parse(startDate ?: "")
        today.time = date ?: Calendar.getInstance().time
        today.add(Calendar.DAY_OF_MONTH, 30)
        binding.datePicker.init(today.get(Calendar.YEAR),
            today.get(Calendar.MONTH),
            today.get(Calendar.DAY_OF_MONTH)) { _, year, month, day ->
            if (isSetUpDone) {
                val f = DecimalFormat("00")
                val monthVal = month + 1
                val dateVal = "${f.format(day)}-${f.format(monthVal)}-$year"
                listener?.onActionItem(dateVal)
                dismiss()
            }
        }
        binding.datePicker.minDate = today.timeInMillis
    }

}