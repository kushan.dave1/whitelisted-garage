package whitelisted.garage.view.dialog

import android.os.Bundle
import android.view.View
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.Cart
import whitelisted.garage.api.response.MarketPlaceCoupon
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogApplyMarketplaceCouponBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.CommonUtils.roundOff
import whitelisted.garage.view.adapter.MarketPlaceCouponAdapter
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel

class ApplyMarketplaceCouponDialog(private val listener: ActionListener?) : BaseDialogFragment() {

    private val viewModel: SparesShopFragmentViewModel by sharedViewModel()
    private val binding by viewBinding2(DialogApplyMarketplaceCouponBinding::inflate)
    private lateinit var cart: Cart
    private var isAccCart = false
    private var isBulkApplied = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getCart()
        setupViews()
        getMarketPlaceCoupons()
        observeGetCartAPI()
    }

    private fun getCart() {
        isAccCart = arguments?.getBoolean(AppENUM.IS_ACCESSORIES_CART, false) ?: false
        isBulkApplied = arguments?.getBoolean(AppENUM.IS_BULK_APPLIED, false) ?: false

        cart = if(isAccCart) viewModel.provideGetCartResponse().cartList[0]
        else viewModel.provideGetCartResponse().cartList[1]

    }

    private fun getMarketPlaceCoupons() = lifecycleScope.launchWhenStarted {
        if(isBulkApplied) return@launchWhenStarted
        showLoader()
        val list = viewModel.getMarketPlaceCoupons()
        if (list.isEmpty()) return@launchWhenStarted
        val adapter = MarketPlaceCouponAdapter(this@ApplyMarketplaceCouponDialog)
        adapter.setUpData(list)
        binding?.couponsRecycler?.adapter = adapter
        binding?.couponsRecycler.makeVisible(true)
        binding?.tvMoreCoupons.makeVisible(true)
        binding?.rvIndicator.makeVisible(true)
        binding?.viewDivider.makeVisible(true)
        hideLoader()
    }

    private fun setupViews() = binding?.apply {
        grpCoupon.makeVisible(!isBulkApplied)
        val goCoins = viewModel.getStringSharedPreference(AppENUM.GO_COIN_BALANCE_AMOUNT)
        tvGoCoins.text = goCoins
        lblGoCoinDesc.text = resources.getString(R.string._10_of_gocoins_can_be_used_in_an_order,
            (viewModel.provideGetCartResponse().gocoinDiscount ?: 0.0).toInt().toString())
        getGoCoinsBalance()
        imageCrossClose.setOnClickListener(this@ApplyMarketplaceCouponDialog)
        goCoinsApply.setOnClickListener(this@ApplyMarketplaceCouponDialog)
        tvYay.setOnClickListener(this@ApplyMarketplaceCouponDialog)
        tvApply.setOnClickListener(this@ApplyMarketplaceCouponDialog)
        couponsRecycler.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        rvIndicator.attachToRecyclerView(couponsRecycler)
        PagerSnapHelper().attachToRecyclerView(couponsRecycler)
        /*editCouponDA.filters =
            arrayOf(InputFilter.AllCaps(Locale.US), InputFilter.LengthFilter(18))*/
        editCouponDA.doOnTextChanged { text, _, _, _ ->
            val coupon = text.toString()
            if (coupon.isNotEmpty()) {
                tvApply.setTextColor(resources.getColor(R.color.colorAccent,
                    resources.newTheme()))
                tvApply.isEnabled = true
            } else {
                tvApply.setTextColor(resources.getColor(R.color.field_outline_33,
                    resources.newTheme()))
                tvApply.isEnabled = false
            }
        }
        viewDialog.makeVisible(true)
        viewSuccess.makeVisible(false)
    }

    private fun getGoCoinsBalance() = lifecycleScope.launch {
        val balance = viewModel.getGoCoinsBalance()
        binding?.tvGoCoins?.text = balance
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id)  { 
            R.id.imageCrossClose, R.id.tvYay -> dismiss()
            R.id.rootViewParent -> applyCoupon((v.getTag(v.id) as MarketPlaceCoupon).coupon)
            R.id.goCoinsApply -> applyGoCoins()
            R.id.tvApply -> applyCoupon(binding?.editCouponDA?.text.toString())
            
            /*binding?.imageCrossClose.id, binding?.tvYay.id -> dismiss()
            binding?.goCoinsApply.id -> applyGoCoins()
            binding?.tvApply.id -> applyCoupon(binding?.editCouponDA.text.toString())*/
            
        }
    }

    private fun applyGoCoins() = lifecycleScope.launchWhenStarted {
        showLoader()
        val isApplied = viewModel.applyMarketPlaceCoupon(cart.id,type = AppENUM.GOCOINS)
        if (isApplied.toBoolean()) viewModel.getSSCartAPI()
        else CommonUtils.showToast(requireContext(),
            resources.getString(R.string.failed_to_go_coins))
        hideLoader()
    }

    private fun applyCoupon(coupon: String) = lifecycleScope.launchWhenStarted {
        showLoader()
        when (val res = viewModel.applyMarketPlaceCoupon(cart.id,coupon, type = AppENUM.COUPON)) {
            "true" -> viewModel.getSSCartAPI()
            "false" -> CommonUtils.showToast(requireContext(),
                resources.getString(R.string.failed_to_apply_coupon))
            else -> CommonUtils.showToast(requireContext(), res)
        }
        hideLoader()
    }

    private fun observeGetCartAPI() {
        viewModel.provideGetCartResponseAPI().observe(viewLifecycleOwner) {
                when (it) {
                    is Result.Success -> {
                        viewModel.setGetCartResponse(it.body.data)
                        updateViewCartCard()
                    }
                    is Result.Failure -> {
                        CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    }
                }
            }
    }

    private fun updateViewCartCard() = binding?.apply {
        hideLoader()
        cart = if(isAccCart) viewModel.provideGetCartResponse().cartList[0]
               else viewModel.provideGetCartResponse().cartList[1]
        val symbol = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            AppENUM.RefactoredStrings.defaultCurrencySymbol)
        val conversionValue = viewModel.getIntSharedPreference(AppENUM.GO_COINS_VALUE)
        if ((cart.goCoinsDiscount ?: 0.0) > 0) {
            val goCoinsApplied = ((cart.goCoinsDiscount ?: 0.0) * conversionValue).toInt()
            viewDialog.makeVisible(false)
            viewSuccess.makeVisible(true)
            ivSuccessIcon.setImageResource(R.drawable.ic_go_coin_medium_)
            tvAppliedHeader.text =
                "$goCoinsApplied ${resources.getString(R.string.gocoins)} ${resources.getString(R.string.applied)}"
            tvAppliedSubHeader.text =
                symbol + cart.goCoinsDiscount.roundOff() + " " + resources.getString(R.string.savings_with_gocoins)
        } else if ((cart.discount ?: 0.0) > 0) {
            viewDialog.makeVisible(false)
            viewSuccess.makeVisible(true)
            ivSuccessIcon.setImageResource(R.drawable.ic_savings_icon)
            tvAppliedHeader.text =
                "'${cart.discountCoupon}' ${resources.getString(R.string.applied)}"
            tvAppliedSubHeader.text =
                symbol + cart.discount.roundOff() + " " + resources.getString(R.string.savings_with_this_coupon)
        } else {
            viewDialog.makeVisible(true)
            viewSuccess.makeVisible(false)
        }
    }


}