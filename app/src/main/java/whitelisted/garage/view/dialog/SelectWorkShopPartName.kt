package whitelisted.garage.view.dialog

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import kotlinx.android.synthetic.main.dialog_select_car.etSearch
import kotlinx.android.synthetic.main.dialog_select_car.rvSelect
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.GetInventoryPartItemResponse
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogWorkshopPartNameBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.view.adapter.AddRackPartAdapter
import whitelisted.garage.viewmodels.WorkShopBiddingViewModel

class SelectWorkShopPartName(private val listener: ActionListener?) : BaseDialogFragment() {

    private val binding by viewBinding(DialogWorkshopPartNameBinding::inflate)
    private lateinit var adapter: AddRackPartAdapter
    private val viewModel: WorkShopBiddingViewModel by sharedViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        setScreen()
        setObservers()
        addTextWatcher()
        etSearch.requestFocus()
        CommonUtils.showKeyboard(requireActivity(), etSearch)
        isCancelable = true
        binding.rlBase.setOnClickListener(this)
    }

    private fun setObservers() {
        observePartItemResponse()
    }

    private fun observePartItemResponse() {
        viewModel.providePartItemResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    viewModel.launch(Dispatchers.Main) {
                        delay(200)
                        if (!it.body.data.isNullOrEmpty()) {
                            ((it.body.data) as? MutableList<GetInventoryPartItemResponse>)?.let { list ->
                                adapter.setData(list)
                            }
                        }
                    }
                }
                is Result.Failure -> CommonUtils.showToast(activity, it.errorMessage, true)

            }
        }
    }

    private fun setScreen() {
        if (!::adapter.isInitialized) {
            adapter = AddRackPartAdapter(this)
        }
        rvSelect?.adapter = adapter
        adapter.isWorkshopOnly = true
    }

    private fun addTextWatcher() {
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().trim().isNotEmpty()) {
                    showLoader()
                    viewModel.getInventoryPartItem(p0.toString())
                }
            }
        })
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.parentLL -> {
                CommonUtils.genericCastOrNull<GetInventoryPartItemResponse>(v.getTag(R.id.parentLL))
                    ?.let {
                        listener?.onActionItem(it)
                        dismiss()
                    }
            }
            R.id.BtnAdd -> {
                CommonUtils.genericCastOrNull<GetInventoryPartItemResponse>(v.getTag(R.id.model))
                    ?.let {
                        listener?.onActionItem(it)
                        dismiss()
                    }
            }

            R.id.rlBase -> {
                dismiss()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.providePartItemResponse().value = null
    }
}