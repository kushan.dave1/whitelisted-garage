package whitelisted.garage.view.dialog

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import whitelisted.garage.R
import whitelisted.garage.api.data.CustomerDetailsModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogAddEditCustomerDetailsBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils

class AddEditCustomerDetailsDialog(private val actionListener: ActionListener?) :
    BaseDialogFragment() {

    private lateinit var customerDetailsModel: CustomerDetailsModel
    private var resultLauncher: ActivityResultLauncher<Intent>? = null
    private val binding by viewBinding(DialogAddEditCustomerDetailsBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        initializeResultLauncher()
        clickListeners()
        arguments?.getParcelable<CustomerDetailsModel>(AppENUM.CUSTOMER_DETAILS)?.let {
            customerDetailsModel = it
        }

        if (::customerDetailsModel.isInitialized) {
            setData()
        }
    }

    private fun clickListeners() {
        binding.btnSave.setOnClickListener(this)
        binding.imgCross.setOnClickListener(this)
        binding.cgAddFromContacts.setOnClickListener(this)
        binding.lblAddToContacts.setOnClickListener(this)
    }

    private fun setData() {
        customerDetailsModel.apply {
            binding.etCustomerName.setText(name)
            binding.etPhoneNumber.setText(phoneNumber)
            binding.etAddress.setText(address)
            binding.etEmail.setText(email)
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnSave -> {
                CommonUtils.hideKeyboardDialog(this)
                if (formCheck()) {
                    actionListener?.onActionItem(customerDetailsModel)
                    dismiss()
                }
            }
            R.id.imgCross -> {
                dismiss()
            }
            R.id.lblAddToContacts -> {
                if (binding.etPhoneNumber.text.toString().trim().isNotEmpty()) {
                    addContact()
                }
            }
            R.id.cgAddFromContacts -> {
                getContactsFromAddressBook()
            }
        }
    }

    private fun addContact() {
        TedPermission.create().setPermissionListener(object : PermissionListener {
            override fun onPermissionGranted() {
                Intent(ContactsContract.Intents.Insert.ACTION).apply {
                    type = ContactsContract.RawContacts.CONTENT_TYPE
                    putExtra(ContactsContract.Intents.Insert.NAME, binding.etCustomerName.text.toString())
                    putExtra(ContactsContract.Intents.Insert.PHONE, binding.etPhoneNumber.text.toString())
                    putExtra(ContactsContract.Intents.Insert.EMAIL, binding.etEmail.text.toString())
                    startActivity(this)
                }
            }

            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) { // do nothing
            }
        }).setPermissions(Manifest.permission.WRITE_CONTACTS).check()
    }

    @SuppressLint("Recycle", "Range")
    private fun getContactsFromAddressBook() {
        TedPermission.create().setPermissionListener(object : PermissionListener {
            override fun onPermissionGranted() {
                val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
                resultLauncher?.launch(intent)
            }

            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) { // do nothing
            }
        }).setPermissions(Manifest.permission.READ_CONTACTS).check()

    }

    private fun formCheck(): Boolean {
        return when {
            binding.etCustomerName?.text.toString().trim().isEmpty() -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.customer_name_is_required))
                binding.etCustomerName.requestFocus()
                CommonUtils.showKeyboard(requireContext(), binding.etCustomerName)
                false
            }
            binding.etPhoneNumber?.text.toString().trim().isEmpty() -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.phone_number_is_required))
                binding.etPhoneNumber.requestFocus()
                CommonUtils.showKeyboard(requireContext(), binding.etPhoneNumber)
                false
            }
            else -> {
                customerDetailsModel = CustomerDetailsModel().apply {
                    name = binding.etCustomerName.text.toString().trim()
                    phoneNumber = binding.etPhoneNumber.text.toString().trim()
                    address = binding.etAddress.text.toString().trim()
                    email = binding.etEmail.text.toString().trim()
                }
                true
            }
        }
    }

    private fun initializeResultLauncher() {
        resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == Activity.RESULT_OK) { // There are no request codes
                    CommonUtils.fetchContactFromCursor(requireActivity(), result).apply {
                        if (name?.isNotEmpty() == true) {
                            binding.etCustomerName.setText(name)
                            binding.etPhoneNumber.setText(phoneNumber)
                        } else {
                            CommonUtils.showToast(requireContext(),
                                getString(R.string.no_number_found))
                        }
                    }
                }
            }
    }

}