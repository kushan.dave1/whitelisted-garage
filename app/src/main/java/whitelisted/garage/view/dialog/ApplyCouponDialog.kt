package whitelisted.garage.view.dialog

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.GetAllCouponsResponse
import whitelisted.garage.api.response.Value
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogApplyCouponBinding
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.view.adapter.CouponAdapter
import whitelisted.garage.view.adapter.RecyclerProgressAdpater
import whitelisted.garage.viewmodels.ApplyCouponViewModel

class ApplyCouponDialog(private val listener: ActionListener?) : BaseDialogFragment() {

    private val binding by viewBinding(DialogApplyCouponBinding::inflate)
    private var type = ""
    private val viewModel: ApplyCouponViewModel by viewModel()
    private var valueData: List<Value>? = null
    private var couponCode = ""


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        getData()
        clickListeners()
        observeResponse()
        setDataToUi()
        showLoader()
        viewModel.getAllCoupons(type)
    }

    private fun setDataToUi() {
        setUpViewPager()
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_COUPON)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_COUPON,
                this)
        }
    }

    private fun clickListeners() {
        binding.imageCrossClose.setOnClickListener(this)
        binding.tvApply.setOnClickListener(this)
        addTextWatcher()
    }

    private fun getData() {
        arguments?.let {
            type = it.getString(AppENUM.IS_GMB).toString()
        }
    }

    private fun addTextWatcher() {
        binding.editCoupon.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?,
                p1: Int,
                p2: Int,
                p3: Int) { //do nothing
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.toString().isNotEmpty()) {
                    binding.tvApply.setTextColor(CommonUtils.getColor(requireContext(),
                        R.color.colorAccent))
                } else {
                    binding.tvApply.setTextColor(CommonUtils.getColor(requireContext(),
                        R.color.field_outline_33))

                }
            }
            override fun afterTextChanged(p0: Editable?) { //do nothing
            }
        })
    }

    private fun setUpViewPager() = with(binding) {
        rvMoreCoupons.apply {
            adapter = CouponAdapter(this@ApplyCouponDialog)
            clipToPadding = false
            clipChildren = false
            offscreenPageLimit = 3
            orientation = ViewPager2.ORIENTATION_HORIZONTAL
            setPadding(0, 0, 62, 0)
        }

        ((32).toFloat()).let {
            rvMoreCoupons.setPageTransformer { page, position ->
                page.translationX = -it * position
            }
        }
        rvMoreCoupons.getChildAt(0)?.overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        rvMoreCoupons.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrolled(position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                val newData = mutableListOf<Boolean>()
                for (i in 0 until (valueData?.size ?: 0)) {
                    if (i == position) {
                        newData.add(i, true)
                    } else {
                        newData.add(i, false)
                    }
                }
                (progressRecycler.adapter as? RecyclerProgressAdpater)?.setData(newData)
            }

        })
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.imageCrossClose -> {
                dismiss()
            }
            R.id.tvApply -> {
                binding.editCoupon.clearFocus()
                CommonUtils.hideKeyboard(requireActivity())
                if (binding.editCoupon.text.toString().trim().isNotEmpty()) {
                    showLoader()
                    couponCode = binding.editCoupon.text.toString()
                    viewModel.applyCoupon(binding.editCoupon.text.toString(), type)
                }
            }
            R.id.rootViewParent -> {
                CommonUtils.genericCastOrNull<Value>(v.getTag(R.id.rootViewParent))?.let {
                    showLoader()
                    it.coupon?.let { it1 ->
                        couponCode = it1
                        viewModel.applyCoupon(it1, type)
                    }
                }
            }

        }
    }

    private fun observeResponse() {
        viewModel.provideGetAllCouponsResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    getAllCouponsSuccess(it.body)
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
        viewModel.provideApplyCouponsResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    it.body.let { data ->
                        listener?.onActionItem(0, Value().also { value ->
                            value.coupon = couponCode
                            value.couponValue = data.data.value
                        })
                    }
                    Bundle().apply {
                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_COUPON)
                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_COUPON,
                            this)
                    }
                    dismiss()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }
    }

    private fun getAllCouponsSuccess(data: ServerResponse<GetAllCouponsResponse>) {
        data.data.value?.let { it1 ->
            valueData = it1
            if (!valueData.isNullOrEmpty()){
                val progressDotData: MutableList<Boolean> = MutableList(valueData?.size?:0) { false }
                progressDotData[0] = true
                binding.progressRecycler.adapter = RecyclerProgressAdpater(progressDotData)
                if (valueData?.size == 1) {
                    binding.llFrame.makeVisible(false)
                }
            }
        }
        binding.tvMore.makeVisible(false)
    }
}