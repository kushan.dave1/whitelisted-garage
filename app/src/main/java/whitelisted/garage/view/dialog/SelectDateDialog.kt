package whitelisted.garage.view.dialog

import android.os.Bundle
import android.view.View
import whitelisted.garage.R
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogSelectDateBinding
import whitelisted.garage.utils.AppENUM
import java.text.DecimalFormat
import java.util.*

class SelectDateDialog(private val listener: ActionListener?) : BaseDialogFragment() {

    private val binding by viewBinding(DialogSelectDateBinding::inflate)
    private var isStart: Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isStart = arguments?.getBoolean(AppENUM.IS_START) ?: true
        if (!isStart) {
            binding.tvLabel.text = getString(R.string.select_end_date)
            isCancelable = false
        } else isCancelable = true

        setupScreen()
    }

    private fun setupScreen() {
        val today = Calendar.getInstance()
        binding.datePicker.init(today.get(Calendar.YEAR),
            today.get(Calendar.MONTH),
            today.get(Calendar.DAY_OF_MONTH)

        ) { _, year, month, day ->
            val f = DecimalFormat("00")
            val monthVal = month + 1
            ("${f.format(day)}-${f.format(monthVal)}-$year").apply {
                listener?.onActionItem(this)
            }
            dismiss()
        }
        binding.datePicker.maxDate = today.timeInMillis
        if (!isStart) {
            val startDate = Calendar.getInstance()
            (arguments?.getString(AppENUM.START_TIME, ""))?.let {
                startDate.timeInMillis = it.toLong()
            }
            binding.datePicker.minDate = startDate.timeInMillis
        }
    }

}