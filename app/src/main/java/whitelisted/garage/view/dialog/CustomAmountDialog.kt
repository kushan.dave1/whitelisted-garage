package whitelisted.garage.view.dialog

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogCustomAmountBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.viewmodels.DashboardSharedViewModel

class CustomAmountDialog(private val listener: ActionListener?) : BaseDialogFragment(),
    TextWatcher {

    private val binding by viewBinding(DialogCustomAmountBinding::inflate)
    private var goCoinsValue = 1
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        goCoinsValue = arguments?.getInt(AppENUM.GO_COINS_VALUE) ?: 1
        binding.btnCancel.setOnClickListener(this)
        binding.btnBuy.setOnClickListener(this)
        binding.etAmount.addTextChangedListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnCancel -> {
                CommonUtils.hideKeyboard(requireActivity())
                dismiss()
            }
            R.id.btnBuy -> {
                CommonUtils.hideKeyboard(requireActivity())
                when {
                    binding.etAmount.text.toString().trim().isEmpty() -> {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.please_enter_amount),
                            true)
                    }
                    binding.etAmount.text.toString().toInt() in 100001 downTo 49 -> {
                        dismiss()
                        listener?.onActionItem(binding.etAmount.text.toString())
                    }
                    else -> {
                        dashboardViewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                            getString(R.string.currency_symbol_default)).let {
                            "${getString(R.string.value_entered_should_be_between)} $it${getString(R.string.fifty)} - $it${
                                getString(R.string.one_lakh)
                            }"
                        }
                    }
                }
            }
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0.toString().trim().isNotEmpty()) {
            val coinsValue = binding.etAmount.text.toString().toDouble().times(goCoinsValue)
            binding.etGoCoins.setText(coinsValue.toInt().toString())
        } else binding.etGoCoins.setText("")
    }

}