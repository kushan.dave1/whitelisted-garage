package whitelisted.garage.view.dialog

import android.os.Bundle
import android.view.View
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogMembershipPurchasedSuccessBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FragmentFactory

class MembershipPurchasedDialog : BaseDialogFragment() {

    private val binding by viewBinding(DialogMembershipPurchasedSuccessBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = false
        setupScreen()
    }

    private fun setupScreen() {
        binding.animationView.playAnimation()
        binding.imgCancel.setOnClickListener{
            dismiss()
        }

        binding.btnVisitSubs.setOnClickListener {
            dismiss()
            CommonUtils.addFragmentUtil(requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.SUBSCRIPTION_INFO_FRAGMENT))
        }
    }

}