package whitelisted.garage.view.dialog

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.PPIAndQuantity
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogAddItemBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.viewmodels.DashboardSharedViewModel

class AddItemDialog(private val listener: ActionListener?) : BaseDialogFragment(), TextWatcher {

    private val binding by viewBinding(DialogAddItemBinding::inflate)
    private var totalAmountCalculated: Double? = null
    private var isAccessories = false
    private var currencySymbol: String? = null
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()
    private var isInternational = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() = with(binding) {
        btnAddItem.setOnClickListener(this@AddItemDialog)
        tvPartName.text = arguments?.getString(AppENUM.SELECTED_PART, "")
        tvWorkDone.text = arguments?.getString(AppENUM.SELECTED_WORK_DONE, "")

        isInternational =
            (dashboardViewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                AppENUM.RefactoredStrings.defaultCurrency) != AppENUM.RefactoredStrings.defaultCurrency)

        if (isInternational) {
            tilTaxRate.makeVisible(false)
            tilTaxRate2.makeVisible(true)
            lblTaxRate.text = getString(R.string.tax_rate_percent_asterisk)
        } else {
            tilTaxRate2.makeVisible(false)
            tilTaxRate.makeVisible(true)
            lblTaxRate.text = getString(R.string.tax_rate_asterisk)
        }

        currencySymbol =
            dashboardViewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                getString(R.string.currency_symbol_default))

        isAccessories = arguments?.getBoolean(AppENUM.IS_ACCESSORIES) ?: false
        if (isAccessories) {
            llWorkDone.makeVisible(false)
            tvWorkDone.makeVisible(false)
        }
        if (arguments?.getBoolean(AppENUM.IntentKeysENUM.IS_EDIT, false) == true) {
            btnAddItem.text = getString(R.string.update_item)
            etPricePerItem.setText(arguments?.getString(AppENUM.PRICE_PER_ITEM, ""))
            etQuantity.setText(arguments?.getString(AppENUM.QUANTITY, ""))
            tvTax.setText(arguments?.getString(AppENUM.TAX_RATE, getString(R.string.zero_percent)))
            tvTax2.setText(arguments?.getString(AppENUM.TAX_RATE, getString(R.string.zero_percent))
                ?.replace("%", ""))
            currencySymbol + arguments?.getString(AppENUM.TOTAL_AMOUNT, "")
                .apply { tvTotalAmount.text = this }
        } else {
            tvTax.setText(getString(R.string.zero_percent))
            tvTax2.setText(getString(R.string.zero_percent).replace("%", ""))
        }
        ArrayAdapter(requireContext(),
            R.layout.support_simple_spinner_dropdown_item,
            resources.getStringArray(R.array.tax_list).toMutableList()).let {
            tvTax.setAdapter(it)
        }

        etPricePerItemTextWatcher()
        etQuantityItemTextWatcher()

        tvTax.addTextChangedListener(this@AddItemDialog)
        tvTax2.addTextChangedListener(this@AddItemDialog)
    }

    private fun etQuantityItemTextWatcher() = with(binding) {
        etQuantity.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?,
                p1: Int,
                p2: Int,
                p3: Int) { //do nothing
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().trim().isNotEmpty()) {
                    if (p0?.contains(".") == true) {
                        etQuantity.setText(etQuantity.text?.length?.let {
                            p0.toString().subSequence(0, it - 1)
                        })
                        etQuantity.setSelection(etQuantity.text.toString().length)
                    }
                    internationalCheck()
                }
            }
        })
    }

    private fun internationalCheck() = with(binding) {
        if (isInternational) {
            if (etPricePerItem.text.toString().trim().isNotEmpty() && etQuantity.text.toString()
                    .trim().isNotEmpty() && tvTax2.text.toString().trim()
                    .isNotEmpty()) updateTotalPrice()
        } else {
            if (etPricePerItem.text.toString().trim().isNotEmpty() && etQuantity.text.toString()
                    .trim().isNotEmpty() && tvTax.text.toString().contains("%")) updateTotalPrice()
        }
    }

    private fun etPricePerItemTextWatcher() = with(binding) {
        etPricePerItem.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?,
                p1: Int,
                p2: Int,
                p3: Int) { //do nothing
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.isNotEmpty() == true && p0.contains(".") && CommonUtils.countOccurrences(p0.toString(),
                        '.') == 2) {
                    etPricePerItem.setText(etPricePerItem.text?.length?.let {
                        p0.toString().subSequence(0, it)
                    })
                    etPricePerItem.setSelection(etPricePerItem.text.toString().length)
                }
                if (isInternational) {
                    if (etPricePerItem.text.toString().trim()
                            .isNotEmpty() && etQuantity.text.toString().trim()
                            .isNotEmpty() && tvTax2.text.toString().trim()
                            .isNotEmpty()) updateTotalPrice()
                } else {
                    if (etPricePerItem.text.toString().trim()
                            .isNotEmpty() && etQuantity.text.toString().trim()
                            .isNotEmpty() && tvTax.text.toString().contains("%")) updateTotalPrice()
                }
            }
        })
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnAddItem -> {
                when {
                    binding.etPricePerItem.text.toString().trim()
                        .isEmpty() || binding.etPricePerItem.text.toString().toDouble() < 1 -> {
                        CommonUtils.showToast(requireContext(), getString(R.string.error_ppi), true)
                    }
                    binding.etQuantity.text.toString().trim()
                        .isEmpty() || binding.etQuantity.text.toString().toDouble() < 1 -> {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.empty_quantity),
                            true)
                    }
                    binding.tvTax.text.toString().trim().isEmpty() -> {
                        if (!isInternational) CommonUtils.showToast(requireContext(),
                            getString(R.string.empty_tax_rate),
                            true)
                    }
                    binding.tvTax2.text.toString().trim().isEmpty() -> {
                        if (isInternational) CommonUtils.showToast(requireContext(),
                            getString(R.string.empty_tax_rate),
                            true)
                    }
                    else -> {
                        dismiss()
                        listener?.onActionItem(PPIAndQuantity().apply {
                            pricePerItem = binding.etPricePerItem.text.toString().toDouble()
                            quantity = binding.etQuantity.text.toString().toInt()
                            taxRate = if (isInternational) (binding.tvTax2.text.toString() + "%")
                            else binding.tvTax.text.toString()
                            totalAmount = totalAmountCalculated
                        })
                    }
                }
            }
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
    }

    override fun afterTextChanged(p0: Editable?) = with(binding) {
        if (isInternational) {
            if (etPricePerItem.text.toString().trim().isNotEmpty() && etQuantity.text.toString()
                    .isNotEmpty() && tvTax2.text.toString().trim().isNotEmpty()) updateTotalPrice()
        } else {
            if (etPricePerItem.text.toString().trim().isNotEmpty() && etQuantity.text.toString()
                    .trim().isNotEmpty() && tvTax.text.toString().contains("%")) updateTotalPrice()
        }
    }

    private fun updateTotalPrice() = with(binding) {
        if (etPricePerItem.text.toString().isNotEmpty()) {
            val totalPrice: Double = etPricePerItem.text.toString().toDouble()
                .times(etQuantity.text.toString().toDouble())
            val totalPriceWithTax: Double = if (isInternational) {
                totalPrice.plus(totalPrice.times(tvTax2.text.toString().replace("%", "").toDouble()
                    .div(100)))
            } else {
                totalPrice.plus(totalPrice.times(tvTax.text.toString().replace("%", "").toDouble()
                    .div(100)))
            }
            currencySymbol + String.format("%.2f", totalPriceWithTax).apply {
                tvTotalAmount.text = this
            }
            totalAmountCalculated = totalPriceWithTax
        }
    }

}