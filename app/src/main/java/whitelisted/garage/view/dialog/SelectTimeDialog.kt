package whitelisted.garage.view.dialog

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.dialog_select_date.tvLabel
import whitelisted.garage.R
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogSelectTimeBinding
import whitelisted.garage.utils.AppENUM
import kotlin.properties.Delegates

class SelectTimeDialog(private val listener: ActionListener?) : BaseDialogFragment() {
    private var time: String = ""

    private val binding by viewBinding(DialogSelectTimeBinding::inflate)
    private var isStart by Delegates.notNull<Boolean>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isStart = arguments?.getBoolean(AppENUM.IS_START) ?: true
        if (!isStart) {
            tvLabel?.text = getString(R.string.select_end_time)
            binding.btnSelectTime.text = getString(R.string.select_end_time)
            isCancelable = false
        } else isCancelable = true

        setupScreen()
    }

    private fun setupScreen() {
        binding.timePicker.setIs24HourView(true)
        binding.btnSelectTime.setOnClickListener {
            time = "${binding.timePicker.hour} : ${binding.timePicker.minute}"
            listener?.onActionItem(time)
            dismiss()
        }
    }
}