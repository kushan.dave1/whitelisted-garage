package whitelisted.garage.view.dialog

import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogVideoViewBinding
import whitelisted.garage.utils.AppENUM

class VideoViewDialog : BaseDialogFragment() {

    private val binding by viewBinding(DialogVideoViewBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupScreen()
    }

    private fun setupScreen() {
        val videoUrl = arguments?.getString(AppENUM.VIDEO_URL, "")
        val uri = Uri.parse(videoUrl)
        binding.vvDVV.setVideoURI(uri)
        val mediaController = MediaController(requireContext())
        mediaController.setAnchorView(binding.vvDVV)
        mediaController.setMediaPlayer(binding.vvDVV)
        binding.vvDVV.setMediaController(mediaController)
        binding.vvDVV.start()
    }

}