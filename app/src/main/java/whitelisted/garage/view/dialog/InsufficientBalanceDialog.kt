package whitelisted.garage.view.dialog

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.lifecycleScope
import com.razorpay.Checkout
import com.razorpay.PaymentData
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.SSCheckoutRequest
import whitelisted.garage.api.response.GetGOCoinOptionsResponse
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.databinding.InsufiicientGocoinsDialogBinding
import whitelisted.garage.eventBus.IsFromGMBEvent
import whitelisted.garage.eventBus.PaymentEventModel
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeBackgroundDrawable
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.viewmodels.GoCoinsFragmentViewModel
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel

class InsufficientBalanceDialog(private val listener: ActionListener?) : BaseDialogFragment() {

    private val binding by viewBinding2(InsufiicientGocoinsDialogBinding::inflate)
    private val viewModel: GoCoinsFragmentViewModel by viewModel()
    private var goCoinValue = 1
    private var keyID = ""
    private var receivedOrderId: String? = null
    private lateinit var razorpayCheckout: Checkout
    private var enteredAmount = "0"
    private var paymentInitiated = false
    private var goCoinToGet: String = "0"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
        getGoCoinAmountOptions()
        observerRazorPayKeyResponse()
        observeOrderIdResponse()
        observeVerifyPaymentResponse()

    }

    private fun getGoCoinAmountOptions() = lifecycleScope.launch {

        val options = viewModel.getGoCoinAmountOptions()
        options?.let { setDataToOptions(it) }

    }


    private fun setDataToOptions(options: GetGOCoinOptionsResponse) = binding?.apply {
        llOptions.makeVisible(true)
        if ((options.gocoinsOptions?.size ?: 0) >= 3) {
            tvOption1.text = (options.gocoinsOptions?.get(0) ?: 0).toString()
            tvOption2.text = (options.gocoinsOptions?.get(1) ?: 0).toString()
            tvOption3.text = (options.gocoinsOptions?.get(2) ?: 0).toString()
            when {
                options.defaultSelect.toString() == options.gocoinsOptions?.get(0) -> {
                    tvPop1.makeVisible(true)
                }
                options.defaultSelect.toString() == options.gocoinsOptions?.get(1) -> {
                    tvPop2.makeVisible(true)
                }
                options.defaultSelect.toString() == options.gocoinsOptions?.get(2) -> {
                    tvPop3.makeVisible(true)
                }
            }
        }
        llOption1.callOnClick()
        tvAmount.requestFocus()
        //requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    private fun setupView() = binding?.apply {
        goCoinValue = viewModel.getIntSharedPreference(AppENUM.GO_COINS_VALUE)
        tvAmount.doOnTextChanged { text, _, _, _ -> onAmountChanged(text.toString()) }
        llOption1.setOnClickListener(this@InsufficientBalanceDialog)
        llOption2.setOnClickListener(this@InsufficientBalanceDialog)
        llOption3.setOnClickListener(this@InsufficientBalanceDialog)
        btnPay.setOnClickListener(this@InsufficientBalanceDialog)
    }

    private fun onAmountChanged(amount: String) {
        disableAllOptions()
        if (amount.trim().isNotEmpty()) {
            enteredAmount = amount
            setDataToBuyBtn(amount)
            (enteredAmount.toInt().times(goCoinValue).toString()).apply {
                binding?.etGoCoinAmount?.setText(this)
                goCoinToGet = this
            }
        } else {
            binding?.etGoCoinAmount?.setText("0")
            setDataToBuyBtn("0")
            enteredAmount = "0"
            goCoinToGet = "0"
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setDataToBuyBtn(selectedAmount: String) {
        binding?.btnPay?.text = "${getString(R.string.pay)} ${
            viewModel.getStringSharedPreference(
                AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol
            )
        }${selectedAmount} ${
            getString(R.string.securely)
        }"
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.llOption1 -> {
                enableDisableViews(R.id.llOption1)
            }
            R.id.llOption2 -> {
                enableDisableViews(R.id.llOption2)
            }
            R.id.llOption3 -> {
                enableDisableViews(R.id.llOption3)
            }
            R.id.btnPay -> {
                onBtnPayClick()
            }
        }
    }

    private fun onBtnPayClick() {


        if (enteredAmount.isNotEmpty() && enteredAmount != "0") {

            CommonUtils.addFragmentUtil(
                requireActivity(),
                FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS,
                    Bundle().apply {
                        putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                        putString(AppENUM.IS_FROM,
                            AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_INSUFFICIENT_DIALOG)
                        putString(AppENUM.AMOUNT_TO_BE_PURCHASED, enteredAmount)
                        putString(AppENUM.GO_COIN_TO_GET, goCoinToGet)
                    }),
            )
            dismiss()

            /*if (viewModel.getStringSharedPreference(
                    AppENUM.UserKeySaveENUM.CURRENCY,
                    AppENUM.RefactoredStrings.defaultCurrency
                ) == AppENUM.RefactoredStrings.defaultCurrencySymbol
            ) {
                checkAmountAndProceed()
            } else {
                checkInternationalAmountAndProceed()
            }*/
        } else {
            CommonUtils.showToast(
                requireContext(),
                getString(R.string.plesae_enter_the_valid_go_coin)
            )
        }
    }


    private fun checkInternationalAmountAndProceed() {
        viewModel.getStringSharedPreference(
            AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            AppENUM.RefactoredStrings.defaultCurrency
        ).let {
            if (enteredAmount.contains(it)) enteredAmount.replace(it, "")
        }

        if (!paymentInitiated && (enteredAmount.toInt() >= 10)) {
            paymentInitiated = true
            showLoader()
            viewModel.getRazorPayKey()
        } else {
            "${getString(R.string.minimum_amonut_is)} ${
                viewModel.getStringSharedPreference(
                    AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    AppENUM.RefactoredStrings.defaultCurrencySymbol
                )
            }10".apply {
                CommonUtils.showToast(requireContext(), this)
            }
        }
    }

    private fun observerRazorPayKeyResponse() {
        viewModel.provideRazorPayKeyResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    keyID = it.body.data.key ?: ""
                    initiateRazorpay()
                    viewModel.getOrderId(
                        enteredAmount,
                        goCoinToGet,
                        getString(R.string.add_gocoins)
                    )

                }
                is Result.Failure -> {
                    paymentInitiated = false
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    dismiss()
                }
            }
        }
    }

    private fun observeOrderIdResponse() {
        viewModel.provideOrderIdForPaymentResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    receivedOrderId = it.body.data.id
                    try {
                        openRazorPayPayment(enteredAmount.toDouble().times(100).toString())
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                is Result.Failure -> {
                    paymentInitiated = false
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                    dismiss()
                }
            }
        }
    }

    private fun openRazorPayPayment(amountInPisa: String) {
        try {
            val options = JSONObject()
            options.put("name", getString(R.string.app_name))
            options.put("description",
                getString(R.string.garage_management_services)) //You can omit the image option to fetch the image from dashboard
            //            options.put("image", "")
            options.put("theme.color", "#00579c")
            options.put("currency",
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                    AppENUM.RefactoredStrings.defaultCurrency)) //            options.put("order_id", id)
            options.put("amount", amountInPisa) //pass amount in currency subunits

            val retryObj = JSONObject()
            retryObj.put("enabled", true)
            retryObj.put("max_count", 4)
            options.put("retry", retryObj)

            val prefill = JSONObject()
            prefill.put("name",
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.OWNER_NAME,
                    "")) //            prefill.put("email", "gaurav.kumar@example.com")
            prefill.put("contact",
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                    getString(R.string.ind_country_code))
                    .replace("+", "") + viewModel.getStringSharedPreference(AppENUM.MOBILE_NUMBER,
                    ""))
            var method = ""
            arguments?.getString(AppENUM.RazorPayENUM.RAZORPAY_METHOD, "")?.let {
                if (it.isNotEmpty()) {
                    prefill.put(AppENUM.RazorPayENUM.RAZORPAY_METHOD, it)
                    method = it
                }
            }
            if (method == AppENUM.RazorPayENUM.RAZORPAY_WALLET || method == AppENUM.RazorPayENUM.RAZORPAY_UPI) {
                arguments?.getString(AppENUM.RazorPayENUM.RAZORPAY_MODE, "")?.let {
                    if (it.isNotEmpty()) {
                        prefill.put(method, it)
                    }
                }
            }

            options.put("prefill", prefill)
            razorpayCheckout.open(requireActivity(), options)

        } catch (e: Exception) {
            CommonUtils.showToast(requireContext(), "Error in payment: " + e.message, true)
            e.printStackTrace()
        }
    }


    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        when (event) {
            is PaymentEventModel -> {
                ((event.extra as? PaymentData)?.paymentId)?.let { paymentId ->
                        viewModel.sendPaymentDetailsToServer(paymentId,
                            receivedOrderId ?: "",
                            enteredAmount,
                            viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
                                AppENUM.RefactoredStrings.defaultCurrency))

                }
            }
        }
    }

    private fun observeVerifyPaymentResponse() {
        viewModel.provideVerifyPaymentResponse().observe(viewLifecycleOwner) {
            hideLoader()
            dismiss()
            when (it) {
                is Result.Success -> {
                    CommonUtils.showToast(requireContext(), it.body.message, true)
                    showLoader()
                    viewModel.getGoCoinsScreenResponse()
                    paymentInitiated = false
                }
                is Result.Failure -> {
                    paymentInitiated = false
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }


    private fun initiateRazorpay() {
        Checkout.preload(context?.applicationContext)
        razorpayCheckout = Checkout()
        razorpayCheckout.setKeyID(keyID)
        razorpayCheckout.setImage(R.drawable.saas_logo)
    }


    private fun checkAmountAndProceed() {
        if (!paymentInitiated && (enteredAmount.toInt() >= 50)) {
            paymentInitiated = true
            showLoader()
            viewModel.getRazorPayKey()
        } else {
            "${getString(R.string.minimum_amonut_is)} ${
                viewModel.getStringSharedPreference(
                    AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    AppENUM.RefactoredStrings.defaultCurrencySymbol
                )
            }50".apply {
                CommonUtils.showToast(requireContext(), this)
            }
        }
    }


    private fun enableDisableViews(id: Int) = binding?.apply {
        when (id) {
            R.id.llOption1 -> {
                tvAmount.setText(tvOption1.text.toString())
                tvOption1.changeTextColor(requireContext(), R.color.counter_bg)
                tvCurrency1.changeTextColor(requireContext(), R.color.counter_bg)
                llOption1.changeBackgroundDrawable(
                    requireContext(),
                    R.drawable.bg_go_coins_options_popular
                )
                tvOption2.changeTextColor(requireContext(), R.color.tab_gray)
                tvOption3.changeTextColor(requireContext(), R.color.tab_gray)
                tvCurrency2.changeTextColor(requireContext(), R.color.tab_gray)
                tvCurrency3.changeTextColor(requireContext(), R.color.tab_gray)
                llOption2.changeBackgroundDrawable(
                    requireContext(),
                    R.drawable.bg_go_coins_options_nor
                )
                llOption3.changeBackgroundDrawable(
                    requireContext(),
                    R.drawable.bg_go_coins_options_nor
                )
            }
            R.id.llOption2 -> {
                tvAmount.setText(tvOption2.text.toString())
                tvOption2.changeTextColor(requireContext(), R.color.counter_bg)
                tvCurrency2.changeTextColor(requireContext(), R.color.counter_bg)
                llOption2.changeBackgroundDrawable(
                    requireContext(),
                    R.drawable.bg_go_coins_options_popular
                )
                tvCurrency1.changeTextColor(requireContext(), R.color.tab_gray)
                tvCurrency3.changeTextColor(requireContext(), R.color.tab_gray)
                tvOption1.changeTextColor(requireContext(), R.color.tab_gray)
                llOption1.changeBackgroundDrawable(
                    requireContext(),
                    R.drawable.bg_go_coins_options_nor
                )
                tvOption3.changeTextColor(requireContext(), R.color.tab_gray)
                llOption3.changeBackgroundDrawable(
                    requireContext(),
                    R.drawable.bg_go_coins_options_nor
                )
            }
            R.id.llOption3 -> {
                tvAmount.setText(tvOption3.text.toString())
                tvOption3.changeTextColor(requireContext(), R.color.counter_bg)
                tvCurrency3.changeTextColor(requireContext(), R.color.counter_bg)
                llOption3.changeBackgroundDrawable(
                    requireContext(),
                    R.drawable.bg_go_coins_options_popular
                )
                tvCurrency1.changeTextColor(requireContext(), R.color.tab_gray)
                tvCurrency2.changeTextColor(requireContext(), R.color.tab_gray)
                tvOption2.changeTextColor(requireContext(), R.color.tab_gray)
                llOption2.changeBackgroundDrawable(
                    requireContext(),
                    R.drawable.bg_go_coins_options_nor
                )
                tvOption1.changeTextColor(requireContext(), R.color.tab_gray)
                llOption1.changeBackgroundDrawable(
                    requireContext(),
                    R.drawable.bg_go_coins_options_nor
                )
            }
        }
        tvAmount.text?.length?.let { tvAmount.setSelection(it) }
    }

    private fun disableAllOptions() = binding?.apply {
        tvCurrency1.changeTextColor(requireContext(), R.color.tab_gray)
        tvCurrency2.changeTextColor(requireContext(), R.color.tab_gray)
        tvCurrency3.changeTextColor(requireContext(), R.color.tab_gray)
        tvOption2.changeTextColor(requireContext(), R.color.tab_gray)
        llOption2.changeBackgroundDrawable(
            requireContext(),
            R.drawable.bg_go_coins_options_nor
        )
        tvOption1.changeTextColor(requireContext(), R.color.tab_gray)
        llOption1.changeBackgroundDrawable(
            requireContext(),
            R.drawable.bg_go_coins_options_nor
        )
        tvOption3.changeTextColor(requireContext(), R.color.tab_gray)
        llOption3.changeBackgroundDrawable(
            requireContext(),
            R.drawable.bg_go_coins_options_nor
        )
    }


}