package whitelisted.garage.view.dialog

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import okhttp3.internal.filterList
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.BrandsResponseModel
import whitelisted.garage.api.response.CarDetail
import whitelisted.garage.api.response.CarModelResponseModel
import whitelisted.garage.api.response.TypeItem
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogSelectCarBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.lower
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.SelectBrandAdapter
import whitelisted.garage.view.adapter.SelectFuelTypeAdapter
import whitelisted.garage.view.adapter.SelectModelAdapter
import whitelisted.garage.viewmodels.SelectCarDialogViewModel

class SelectCarDialog(private val listener: ActionListener?) : BaseDialogFragment() {

    private val binding by viewBinding(DialogSelectCarBinding::inflate)
    private var selectionType = 0
    private var isReg = false
    private val viewModel: SelectCarDialogViewModel by sharedViewModel()
    private lateinit var brandsFilteredList: List<BrandsResponseModel>
    private lateinit var carModelsFilteredList: List<CarModelResponseModel>
    private lateinit var fuelTypeFilteredList: List<TypeItem>
    private var isBrandActive = false
    private var isModelActive = false
    private var isFuelActive = false
    private var isFromSSFilter = false
    private var isBikeSelection = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        arguments?.let {
            selectionType = it.getInt(AppENUM.CAR_SEARCH_TYPE, 0)
            isBikeSelection = it.getBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, false)
            isReg = it.getBoolean(AppENUM.IS_REG, false)
            isFromSSFilter = it.getBoolean(AppENUM.IS_FROM_SS_FILTER, false)
        }

        clickListener()
        setScreen()
        setObservers()
        addTextWatcher()
    }


    private fun clickListener() {
        if (isReg) {
            binding.btnSave.setOnClickListener(this)
        }
        binding.imageBackArrow.setOnClickListener(this)
        binding.imageClose.setOnClickListener(this)
    }

    private fun addTextWatcher() {
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                when (selectionType) {
                    0 -> {
                        setBrandsAdapter(p0.toString().trim())
                    }
                    1 -> {
                        setModelAdapter(p0.toString().trim())
                    }
                    2 -> {
                        setFuelTypeAdapter(p0.toString().trim())
                    }
                }
            }

            override fun afterTextChanged(p0: Editable?) { //
            }
        })
    }


    private fun setObservers() {
        viewModel.provideBrandListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    viewModel.brandsList = it.body.data
                    setBrandsAdapter("")
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

        viewModel.provideModelsListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    viewModel.carModelsList = it.body.data
                    setModelAdapter("")
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

        viewModel.provideFuelTypeListResponse().observe(viewLifecycleOwner) {
            hideLoader()
            when (it) {
                is Result.Success -> {
                    viewModel.fuelTypeList = it.body.data.type?.toList() ?: listOf()
                    setFuelTypeAdapter("")
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setScreen() = with(binding) {
        rvSelect.makeVisible(false)
        CommonUtils.hideKeyboard(activity)
        etSearch.clearFocus()
        etSearch.setText("")
        addTextWatcher()
        when (selectionType) {
            0 -> {
                llRegLayout.makeVisible(false)
                etSearch.makeVisible(true)
                imageBackArrow.makeVisible(false)
                showLoader()
                viewModel.getBrandsList(isBikeSelection)
                etSearch.hint = getString(R.string.search_brands)
                tvType.text = getString(R.string.select_brand_caps)
                isBrandActive = true
                isModelActive = false
                isFuelActive = false
            }
            1 -> {
                llRegLayout.makeVisible(false)
                tvType.text = getString(R.string.select_model_caps)
                etSearch.makeVisible(true)
                etSearch.hint = getString(R.string.search_models)
                showLoader()
                viewModel.getCarModelsList(viewModel.provideSelectedBrand().value?.id.toString(), isBikeSelection)
                isBrandActive = false
                isModelActive = true
                isFuelActive = false
                imageBackArrow.makeVisible(true)
            }
            2 -> {
                etSearch.hint = getString(R.string.search_fuel_types)
                tvType.text = getString(R.string.select_fuel_type_caps)
                etSearch.makeVisible(false)

                if (isReg) {
                    llRegLayout.makeVisible(true)
                    etRegNumber.setText(arguments?.getString(AppENUM.REG_NO, ""))
                    etODOMeter.setText(arguments?.getString(AppENUM.ODO_READING, ""))
                } else {
                    llRegLayout.makeVisible(false)
                }
                showLoader()
                viewModel.getFuelTypeList(viewModel.provideSelectedModel().value?.modelId.toString(), isBikeSelection)
                isBrandActive = false
                isModelActive = false
                isFuelActive = true
                imageBackArrow.makeVisible(true)
            }
        }
    }

    private fun setDataOnBackClick() = with(binding) {
        rvSelect.makeVisible(false)
        CommonUtils.hideKeyboard(requireActivity())
        etSearch.clearFocus()
        etSearch.setText("")
        addTextWatcher()
        when (selectionType) {
            1 -> {
                llRegLayout.makeVisible(false)
                tvType.text = getString(R.string.select_model_caps)
                etSearch.makeVisible(true)
                etSearch.hint = getString(R.string.search_models)
                isBrandActive = false
                isModelActive = true
                isFuelActive = false
                imageBackArrow.makeVisible(true)
                setModelAdapter("")
            }
            0 -> {
                llRegLayout.makeVisible(false)
                etSearch.makeVisible(true)
                imageBackArrow.makeVisible(false)
                etSearch.hint = getString(R.string.search_brands)
                tvType.text = getString(R.string.select_brand_caps)
                isBrandActive = true
                isModelActive = false
                isFuelActive = false
                setBrandsAdapter("")
            }
        }
    }

    private fun setBrandsAdapter(searchString: String) {
        GridLayoutManager(requireContext(), 3, GridLayoutManager.VERTICAL, false).apply {
            binding.rvSelect.layoutManager = this
        }
        if (searchString.isNotEmpty()) {
            brandsFilteredList = if (::brandsFilteredList.isInitialized) emptyList()
            else listOf()
            brandsFilteredList = viewModel.brandsList.filter {
                it.name?.lower().toString().contains(searchString.lower())
            }
            binding.rvSelect.adapter = SelectBrandAdapter(brandsFilteredList, this)
        } else binding.rvSelect.adapter = SelectBrandAdapter(viewModel.brandsList, this)
        binding.rvSelect.makeVisible(true)
    }

    private fun setModelAdapter(searchString: String) {
        GridLayoutManager(requireContext(), 3, GridLayoutManager.VERTICAL, false).apply {
            binding.rvSelect.layoutManager = this
        }
        if (searchString.isNotEmpty()) {
            carModelsFilteredList = if (::carModelsFilteredList.isInitialized) emptyList()
            else listOf()
            carModelsFilteredList = viewModel.carModelsList.filterList {
                this.carName?.lower().toString().contains(searchString.lower())
            }
            binding.rvSelect.adapter = SelectModelAdapter(carModelsFilteredList, this)
        } else binding.rvSelect.adapter = SelectModelAdapter(viewModel.carModelsList, this)
        binding.rvSelect.makeVisible(true)
    }

    private fun setFuelTypeAdapter(searchString: String) {
        GridLayoutManager(requireContext(), 3, GridLayoutManager.VERTICAL, false).apply {
            binding.rvSelect.layoutManager = this
        }
        if (searchString.isNotEmpty()) {
            fuelTypeFilteredList = if (::fuelTypeFilteredList.isInitialized) emptyList()
            else listOf()
            fuelTypeFilteredList = viewModel.fuelTypeList.filterList {
                this.type?.lower().toString().contains(searchString.lower())
            }
            binding.rvSelect.adapter = SelectFuelTypeAdapter(isReg, fuelTypeFilteredList, this)
        } else binding.rvSelect.adapter = SelectFuelTypeAdapter(isReg, viewModel.fuelTypeList, this)
        binding.rvSelect.makeVisible(true)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.clBaseBuyGC -> {
                handleOnClick(v)
            }
            R.id.imageClose -> {
                dismiss()
            }

            R.id.btnSave -> {
                (binding.rvSelect.adapter as? SelectFuelTypeAdapter)?.let {
                    if (it.selectedPos == -1) {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.please_select_fuel_type))
                    } else {
                        if (checkForm()) handleFuelTypeSelection(it.selectedPos)
                    }
                }

            }
            R.id.imageBackArrow -> {
                binding.etSearch.makeVisible(true)
                when {
                    isFuelActive -> {
                        selectionType = 1
                        setDataOnBackClick()
                    }
                    isModelActive -> {
                        selectionType = 0
                        setDataOnBackClick()
                    }
                    else -> {
                        dismiss()
                    }
                }
            }
        }
    }

    private fun handleOnClick(v: View) {
        CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
            CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.extra))?.let { extra ->
                when (extra) {
                    0 -> {
                        handleBradSelection(position)
                    }
                    1 -> {
                        handleModelSelection(position)
                    }
                    2 -> {
                        if (isReg) {
                            if (checkForm()) handleFuelTypeSelection(position)
                        } else handleFuelTypeSelection(position)
                    }
                }
            }
        }
    }

    private fun handleFuelTypeSelection(position: Int) {
        CommonUtils.hideKeyboardDialog(this)
        if (binding.etSearch.text.toString().trim()
                .isNotEmpty()) viewModel.provideSelectedFuelType().value =
            fuelTypeFilteredList[position]
        else viewModel.provideSelectedFuelType().value = viewModel.fuelTypeList[position]
        val car = CarDetail(
            car = viewModel.provideSelectedModel().value?.carName,
            brand_name = viewModel.provideSelectedBrand().value?.name,
            carIcon = viewModel.provideSelectedModel().value?.carIcon,
            fuelType = viewModel.provideSelectedFuelType().value?.type)
        listener?.onActionItem(2, car)
        binding.rvSelect.makeVisible(false)
        dismiss()
    }

    private fun checkForm(): Boolean {
        return when {
            binding.etRegNumber.text.toString().isEmpty() -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.registration_number_is_required))
                binding.etRegNumber.requestFocus()
                CommonUtils.showKeyboard(requireContext(), binding.etRegNumber)
                false
            }
            binding.etODOMeter.text.toString().isEmpty() -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.odometer_reading_is_requried))
                binding.etODOMeter.requestFocus()
                CommonUtils.showKeyboard(requireContext(), binding.etODOMeter)
                false
            }
            else -> {
                viewModel.provideRegistrationNumber().value = binding.etRegNumber.text.toString()
                viewModel.provideOdometerReading().value = binding.etODOMeter.text.toString()
                true
            }
        }
    }

    private fun handleModelSelection(position: Int) {
        if (binding.etSearch.text.toString().trim().isNotEmpty()) viewModel.provideSelectedModel().value =
            carModelsFilteredList[position]
        else viewModel.provideSelectedModel().value = viewModel.carModelsList[position]

        if (isFromSSFilter) {
            listener?.onActionItem(1)
            binding.rvSelect.makeVisible(false)
            dismiss()
        } else { //  listener?.onActionItem(1)
            binding.rvSelect.makeVisible(false) //   dismiss()
            selectionType = 2
            setScreen()
        }
    }

    private fun handleBradSelection(position: Int) {
        if (binding.etSearch.text.toString().trim().isNotEmpty()) {
            if (::brandsFilteredList.isInitialized) {
                if (brandsFilteredList.size > position) {
                    viewModel.provideSelectedBrand().value = brandsFilteredList[position]
                }
            }
        } else viewModel.provideSelectedBrand().value = viewModel.brandsList[position]
        binding.rvSelect.makeVisible(false) // dismiss()
        selectionType = 1
        setScreen()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.provideModelsListResponse().value = null
        viewModel.provideFuelTypeListResponse().value = null
    }

}