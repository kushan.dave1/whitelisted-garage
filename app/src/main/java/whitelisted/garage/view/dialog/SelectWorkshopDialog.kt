package whitelisted.garage.view.dialog

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogSelectWorkshopBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.SSItemsAdapter
import whitelisted.garage.view.adapter.SelectWorkshopAdapter
import whitelisted.garage.viewmodels.DashboardSharedViewModel
import whitelisted.garage.viewmodels.SelectWorkshopDialogViewModel

class SelectWorkshopDialog(private val workshopSelectListener: ActionListener?) :
    BaseDialogFragment() {
    private val binding by viewBinding(DialogSelectWorkshopBinding::inflate)
    private lateinit var workshopList: MutableList<WorkshopListResponseModel>
    private val viewModel: SelectWorkshopDialogViewModel by viewModel()
    private val dashboardSharedViewModel: DashboardSharedViewModel by sharedViewModel()
    private lateinit var adapter: SelectWorkshopAdapter
    private var fireScreen = ""
    private var isStart: Boolean? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() = with(binding) {
        arguments?.let {
            fireScreen =
                it.getString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN, "") ?: ""
            isStart = it.getBoolean(AppENUM.IS_START, false)
        }
        if (isStart == true) {
            imgRefresh.makeVisible(false)
            imgCancel.makeVisible(false)
            btnAddNewWorkshop.makeVisible(false)
            tvAddNewWorkshop.makeVisible(false)
            viewDivider2.makeVisible(false)
            isCancelable = false
        } else {
            imgRefresh.makeVisible(true)
            imgCancel.makeVisible(true)
            btnAddNewWorkshop.makeVisible(true)
            tvAddNewWorkshop.makeVisible(true)
            viewDivider2.makeVisible(true)
        }
        showLoader()
        viewModel.getWorkshopListFromDB()
        setObserver()
        setOnClickListeners()
    }

    private fun setOnClickListeners() {
        binding.imgRefresh.setOnClickListener(this)
        binding.imgCancel.setOnClickListener(this)
        binding.btnAddNewWorkshop.setOnClickListener(this)
    }

    private fun setObserver() {
        viewModel.provideWorkshopListResponseFromRoom().observe(this) {
            hideLoader()
            if (::workshopList.isInitialized) workshopList.clear()
            workshopList = it.toMutableList()

            if (workshopList.isNotEmpty()) {
                workshopList.forEach { ws -> ws.isLocked = false }
                val wsList = mutableListOf<WorkshopListResponseModel>()
                val isLocked = workshopList.any { item -> item.isLocked == true }
                wsList.addAll(workshopList.filter { item -> item.isLocked == null || item.isLocked == false })
                if (isLocked) {
                    wsList.add(WorkshopListResponseModel(dbId = -1))
                    wsList.addAll(workshopList.filter { item -> item.isLocked == true })
                }

                adapter = SelectWorkshopAdapter(wsList, this)
                val lm = GridLayoutManager(requireContext(), 1)
                lm.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int) = 1
                }
                binding.rvList.layoutManager = lm

                binding.rvList.adapter = adapter
                Handler(Looper.getMainLooper()).postDelayed({
                    if (isStart == true && workshopList.size == 1) {
                        selectWorkshop(workshopList[0])
                    }
                }, 200)
            } else {
                showLoader()
                viewModel.getWorkshopListAPI()
            }
        }
        viewModel.provideWorkshopListResponseFromAPI().observe(this) {
            hideLoader()
            it.let {
                when (it) {
                    is Result.Success -> {
                        val wsList = it.body.data
                        viewModel.addWorkshopsToRoom(wsList)
                    }
                    is Result.Failure -> CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }

        dashboardSharedViewModel.provideIsWorkshopRefresh().observe(this) {
            if (it) {
                dashboardSharedViewModel.provideIsWorkshopRefresh().postValue(false)
                showLoader()
                viewModel.getWorkshopListAPI()
            }
        }
    }


    private fun selectWorkshop(workshopListResponseModel: WorkshopListResponseModel) {
        workshopSelectListener?.onActionItem(workshopListResponseModel)
        dismiss()
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.rlShop -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    Bundle().apply {
                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                            fireScreen)
                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_SELECT_WORKSHOP,
                            this)

                    }
                    selectWorkshop(workshopList[position])
                }
            }
            R.id.imgRefresh -> {
                showLoader()
                viewModel.getWorkshopListAPI()
            }
            R.id.imgCancel -> dismiss()

            R.id.btnAddNewWorkshop -> {
                if (viewModel.getBooleanSharedPreference(AppENUM.UserKeySaveENUM.MEMBERSHIP_STATUS,
                        false)) {
                    Bundle().apply {
                        putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                            fireScreen)
                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_TAP_ADD_WORKSHOP,
                            this)

                    }
                    workshopSelectListener?.onActionItem("add_workshop")
                    dismiss()
                } else {
                    if (workshopList.any { ws -> ws.isLocked == true }) {
                        dismiss()
                        requireActivity().let {
                            fireFeatureLockedEvent()
                            FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT,
                                Bundle().apply {
                                    putString(AppENUM.PRO_LOCKED_ALERT_MSG,
                                        dashboardSharedViewModel.screensConfigResponse.value?.membershipConfig?.workshopLimitMsg)
                                })?.let { baseFragment ->
                                baseFragment.show(it.supportFragmentManager,
                                    baseFragment.javaClass.name)
                            }
                        }
                    } else {
                        Bundle().apply {
                            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                                fireScreen)
                            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_TAP_ADD_WORKSHOP,
                                this)

                        }
                        workshopSelectListener?.onActionItem("add_workshop")
                        dismiss()
                    }
                }
            }

            R.id.clBaseISLB -> {
                fireFeatureLockedEvent()
                if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID, "")
                        .isNotEmpty()) {
                    dismiss()
                    requireActivity().let {
                        FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT,
                            Bundle().apply {
                                putString(AppENUM.PRO_LOCKED_ALERT_MSG,
                                    dashboardSharedViewModel.screensConfigResponse.value?.membershipConfig?.workshopLimitMsg)
                            })?.let { baseFragment ->
                            baseFragment.show(it.supportFragmentManager,
                                baseFragment.javaClass.name)
                        }
                    }
                } else {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.please_select_a_workshop_first))
                }
            }
        }
    }


    private fun fireFeatureLockedEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SELECT_WORKSHOP_DIALOG)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.ERROR_HEADER_PRO,
            bundle)

    }



}