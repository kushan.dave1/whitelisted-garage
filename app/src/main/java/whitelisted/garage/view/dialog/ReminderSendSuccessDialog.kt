package whitelisted.garage.view.dialog

import android.os.Bundle
import android.view.View
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogReminderSendSuccessBinding

class ReminderSendSuccessDialog(private val listener: ActionListener?) : BaseDialogFragment() {

    private val binding by viewBinding(DialogReminderSendSuccessBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupScreen()
    }

    private fun setupScreen() {
        binding.btnOk.setOnClickListener {
            dismiss()
        }
    }

}