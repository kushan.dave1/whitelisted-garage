package whitelisted.garage.view.dialog

import android.os.Bundle
import android.view.View
import whitelisted.garage.R
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogGenericConfirmationBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils.changeTextColor

class GenericConfirmationDialog(private val listener: ActionListener?) : BaseDialogFragment() {

    private val binding by viewBinding(DialogGenericConfirmationBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isCancelable = false
        setupScreen()
    }

    private fun setupScreen() = with(binding) {
        arguments?.let {
            if (it.getBoolean(AppENUM.TITLE_COLOR_ACCENT, false)){
                tvTitle.changeTextColor(requireContext(),R.color.colorAccent )
            }
            tvTitle.text = it.getString(AppENUM.TITLE, "")
            tvDescription.text = it.getString(AppENUM.DESCRIPTION, "")
            btnPositive.text = it.getString(AppENUM.POSITIVE_BUTTON_TEXT, getString(R.string.okay))
            btnNegative.text =
                it.getString(AppENUM.NEGATIVE_BUTTON_TEXT, getString(R.string.cancel))
        }

        btnNegative.setOnClickListener(this@GenericConfirmationDialog)
        btnPositive.setOnClickListener(this@GenericConfirmationDialog)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnNegative -> {
                dismiss()
                listener?.onActionItem(false)
            }
            R.id.btnPositive -> {
                dismiss()
                listener?.onActionItem(true)
            }
        }
    }

}