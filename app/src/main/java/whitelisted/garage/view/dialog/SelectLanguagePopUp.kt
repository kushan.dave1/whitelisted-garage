package whitelisted.garage.view.dialog

import android.view.View
import whitelisted.garage.R
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogSelectLanguageBinding

class SelectLanguagePopUp(private val listener: ActionListener?) : BaseDialogFragment() {

    init { viewBinding(DialogSelectLanguageBinding::inflate) }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.parenLayout -> {
                whitelisted.garage.utils.CommonUtils.genericCastOrNull<String>(v.getTag(R.id.parenLayout))
                    ?.let {
                        dismiss()
                        listener?.onActionItem(it)
                    }
            }
        }
    }
}