package whitelisted.garage.view.dialog

import android.Manifest
import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.fragment_edit_bus_photo.rvPhoto
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.request.BiddinngPhotoModel
import whitelisted.garage.api.request.BrandWiseItem
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogAddRemarkPhotoBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.view.adapter.BiddingAdapterAddPhoto
import whitelisted.garage.viewmodels.WorkShopBiddingViewModel
import java.io.ByteArrayOutputStream
import java.util.*

class AddRemarkImagesDialog(private val listener: ActionListener?) : BaseDialogFragment() {

    private val binding by viewBinding(DialogAddRemarkPhotoBinding::inflate)
    private var uuid = UUID.randomUUID().toString()
    private lateinit var photoList: MutableList<BiddinngPhotoModel>
    private lateinit var photoAdapter: BiddingAdapterAddPhoto
    private var dataBrandWise: BrandWiseItem? = null
    private lateinit var storageRef: StorageReference
    private var uploadMediaRef: StorageReference? = null
    private var trackMediaUploadCount = 0
    private var internalImageUrl = mutableListOf<String>()
    private var internalImageUrI = mutableListOf<Uri>()
    private var imageUrl = mutableListOf<String>()
    private val viewModel: WorkShopBiddingViewModel by sharedViewModel()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListener()
        setData()
        observeIsMediaUploadedSuccess()
        observeIsMediaFetchedSuccess()
    }

    private fun clickListener() {
        binding.btnAddItem.setOnClickListener(this)
    }

    private fun setData() {
        arguments?.let {
            dataBrandWise = it.getParcelable(AppENUM.DATA)
        }
        photoList = mutableListOf(BiddinngPhotoModel(false, Uri.EMPTY))
        for (vale in dataBrandWise?.images ?: mutableListOf()) {
            photoList.add(BiddinngPhotoModel(false, Uri.parse(vale)))
        }
        BiddingAdapterAddPhoto(photoList, this).apply {
            photoAdapter = this
            rvPhoto?.adapter = this
        }
        binding.etRemark?.setText(dataBrandWise?.remark)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnAddItem -> {
                addItem()
            }

            R.id.llDelImage -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    photoAdapter.removeItemFromList(position)
                }
            }
            R.id.rlAddImage -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { _ ->
                    TedPermission.create().setPermissionListener(object : PermissionListener {
                        override fun onPermissionGranted() {
                            TedImagePicker.with(requireContext()).showCameraTile(true)
                                .startMultiImage { uris ->
                                    onImagesPicked(uris)
                                }
                        }

                        override fun onPermissionDenied(deniedPermissions: MutableList<String>?) { //
                        }
                    }).setPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE).check()
                }
            }
        }
    }

    private fun addItem() {
        if (!isLoaderShowing()) {
            internalImageUrI.clear()
            imageUrl.clear()
            for (value in photoList) {
                if (value.isInternal && value.uri != Uri.EMPTY) {
                    internalImageUrI.add(value.uri)
                } else if (value.uri != Uri.EMPTY) {
                    imageUrl.add(value.uri.toString())
                }
            }

            if (internalImageUrI.isEmpty()) {
                listener?.onActionItem(BrandWiseItem().apply {
                    remark = binding.etRemark.text.toString()
                    images = imageUrl
                })
                dismiss()
            } else {
                uploadImages()
            }
        }
    }

    private fun uploadImages() {
        showLoader()
        trackMediaUploadCount = 0
        for (data in internalImageUrI) {
            if (data != Uri.EMPTY) {
                uploadMedia(data)
            }
        }
    }

    private fun uploadMedia(uploadItem: Uri) {
        lifecycleScope.launch {
            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
            val orderRef: StorageReference = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
                .child(viewModel.getStringSharedPreference(AppENUM.USER_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                .child(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOP_BIDDING).child("$uuid/")

            val fileNameRand: String = (Calendar.getInstance().timeInMillis.toString())

            uploadMediaRef = orderRef.child(fileNameRand)


            val uploadTask: UploadTask?

            val emptyBitmap = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(emptyBitmap)
            canvas.drawColor(ContextCompat.getColor(requireContext(), R.color.white))
            var bmp: Bitmap = try {
                if (Build.VERSION.SDK_INT < 28) {
                    MediaStore.Images.Media.getBitmap(requireContext().contentResolver, uploadItem)
                        ?: emptyBitmap
                } else {
                    ImageDecoder.createSource(requireActivity().contentResolver, uploadItem).let {
                        ImageDecoder.decodeBitmap(it)
                    }
                }

            } catch (e: Exception) {
                emptyBitmap
            }
            val byteArrayOutputStream = ByteArrayOutputStream()
            bmp = CommonUtils.scaleBitmap(bmp)
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            val data = byteArrayOutputStream.toByteArray()
            uploadTask = uploadMediaRef?.putBytes(data)

            // Register observers to listen for when the download is done or if it fails
            uploadTask?.addOnFailureListener { // Log.d("",it.toString())
                // Handle unsuccessful uploads
            }
                ?.addOnSuccessListener { // taskSnapshot.metadata contains file metadata such as size, content-type, etc.
                    // ...
                    trackMediaUploadCount++
                    if (trackMediaUploadCount >= internalImageUrI.size) {
                        viewModel.setIsMediaUploadedSuccess(true)
                    }
                }?.addOnCompleteListener { //do nothing
                }
        }
    }

    private fun observeIsMediaUploadedSuccess() {
        viewModel.provideIsMediaUploadedSuccessFully().observe(viewLifecycleOwner) {
            if (it == true) {
                getAllImageUriFromFirebase()
            }
        }
    }

    private fun observeIsMediaFetchedSuccess() {
        viewModel.provideIsMediaDataFetched().observe(viewLifecycleOwner) {
            if (it == true) {
                val allIImageList = mutableListOf<String>()
                allIImageList.addAll(imageUrl)
                allIImageList.addAll(internalImageUrl)
                listener?.onActionItem(BrandWiseItem().apply {
                    remark = binding.etRemark.text.toString()
                    images = allIImageList
                })
                dismiss()
            }
        }
    }

    private fun getAllImageUriFromFirebase() {
        showLoader()
        lifecycleScope.launch {
            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
            val orderRef: StorageReference = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
                .child(viewModel.getStringSharedPreference(AppENUM.USER_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                .child(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOP_BIDDING).child("$uuid/")
            orderRef.listAll().addOnSuccessListener {
                it.items.forEach { item ->
                    item.downloadUrl.addOnSuccessListener { uri ->
                        internalImageUrl.add(uri.toString())
                        if (internalImageUrl.size == it.items.size) {
                            viewModel.setIsMediaDataFetched(true)
                        }
                    }
                }
            }.addOnFailureListener {
                hideLoader()
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun onImagesPicked(uris: List<Uri>) {
        repeat(uris.size) { position ->
            if (uris[position].toString().isNotEmpty()) {
                photoAdapter.getAdapterList().add(0, BiddinngPhotoModel(true, (uris[position])))
            }
            photoAdapter.notifyDataSetChanged()
        }
    }

}