package whitelisted.garage.view.dialog

import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.lifecycleScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.data.GetAccessoriesCatalogueResponse
import whitelisted.garage.api.response.GetInventoryPartItemResponse
import whitelisted.garage.api.response.ServiceResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogSelectRackItemBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.AddRackPartAdapter
import whitelisted.garage.view.adapter.RecentSearchesAdapter
import whitelisted.garage.viewmodels.SelectRackItemDialogViewModel


class SelectRackItemDialog(private val listener: ActionListener?) : BaseDialogFragment() {

    private val binding by viewBinding(DialogSelectRackItemBinding::inflate)
    private var listOfDataWorkshop = arrayListOf<ServiceResponseModel>()
    private var listOfDataAcc = arrayListOf<GetAccessoriesCatalogueResponse>()
    private val viewModel: SelectRackItemDialogViewModel by viewModel()
    private var isInventory = false
    private var searchText = ""
    private val getShopType: String
        get() = viewModel.getStringSharedPreference(
            AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT
        )
    private var isTwoWheeler = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        setScreen()
        setObservers()
        addTextWatcher()
        binding.etSearch.requestFocus()
        CommonUtils.showKeyboard(requireActivity(), binding.etSearch)
        isCancelable = true
        binding.rlBase.setOnClickListener(this)
        binding.iAddService.btnAddServiceByOwn.setOnClickListener(this)
        setFilter()
    }

    private fun setFilter() {
        val filter = InputFilter { source, _, _, _, _, _ ->
            if (source != null && AppENUM.blockCharacterSet.contains("" + source)) {
                ""
            } else null
        }
        binding.etSearch.filters = arrayOf(filter)
    }

    private fun setObservers() {
        observePartItemResponse()
        observeServicesResponse()
        observeAccCatalogueResponse()
    }

    private fun observeAccCatalogueResponse() = with(binding) {
        viewModel.provideAccPartItemResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    hideLoader()
                    val listOfData = mutableListOf<GetInventoryPartItemResponse>()
                    if (it.body.data.isNotEmpty()) {
                        (it.body.data as? ArrayList<GetAccessoriesCatalogueResponse>)?.let { arrayList ->
                            listOfDataAcc = arrayList
                            listOfDataAcc.forEach {
                                listOfData.add(GetInventoryPartItemResponse().apply {
                                    subCategoryName = it.variants?.name
                                    skuId = it.variants?.skuCode
                                    sellingPrice = it.price
                                    workshopId = it.workshopID

                                })
                            }
                            rvSelect.adapter = AddRackPartAdapter(this@SelectRackItemDialog)
                            (rvSelect.adapter as? AddRackPartAdapter)?.setData(listOfData)

                            rvSelect.makeVisible(true)
                            ivNoSearchResults.makeVisible(false)
                            iAddService.clAddByOwn.makeVisible(false)
                            lblNoResults.makeVisible(false)
                        }
                    } else {
                        rvSelect.adapter = null
                        rvSelect.makeVisible(false)
                        ivNoSearchResults.makeVisible(true)
                        iAddService.clAddByOwn.makeVisible(true)
                        lblNoResults.makeVisible(true)
                    }

                }
                is Result.Failure -> {
                    if (it.errorMessage.isNotEmpty()) {
                        CommonUtils.showToast(activity, it.errorMessage, true)
                        hideLoader()
                    }
                }
            }
        }
    }

    private fun observeServicesResponse() = with(binding) {
        viewModel.providePartsListResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    hideLoader()
                    val listOfData = mutableListOf<GetInventoryPartItemResponse>()
                    if (it.body.data.isNotEmpty()) {
                        (it.body.data as? ArrayList<ServiceResponseModel>)?.let { arrayList ->
                            listOfDataWorkshop = arrayList
                            listOfDataWorkshop.forEach {
                                listOfData.add(GetInventoryPartItemResponse().apply {
                                    subCategoryName = it.name
                                    skuId = it.id.toString()
                                    status = it.status
                                })
                            }
                            rvSelect.adapter = AddRackPartAdapter(this@SelectRackItemDialog)
                            (rvSelect.adapter as? AddRackPartAdapter)?.setData(listOfData)

                            rvSelect.makeVisible(true)
                            ivNoSearchResults.makeVisible(false)
                            iAddService.clAddByOwn.makeVisible(false)
                            lblNoResults.makeVisible(false)
                        }
                    } else {
                        rvSelect.makeVisible(false)
                        ivNoSearchResults.makeVisible(true)
                        iAddService.clAddByOwn.makeVisible(true)
                        lblNoResults.makeVisible(true)
                    }
                }
                is Result.Failure -> {
                    if (it.errorMessage.isNotEmpty()) {
                        CommonUtils.showToast(activity, it.errorMessage, true)
                        hideLoader()
                    }
                }
            }
        }
    }

    private fun observePartItemResponse() = with(binding) {
        viewModel.providePartItemResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    hideLoader()
                    if (it.body.data.isNotEmpty()) {
                        (it.body.data as? ArrayList<GetInventoryPartItemResponse>)?.let { arrayList ->
                            rvSelect.adapter = AddRackPartAdapter(this@SelectRackItemDialog)
                            (rvSelect.adapter as? AddRackPartAdapter)?.setData(arrayList)


                            rvSelect.makeVisible(true)
                            ivNoSearchResults.makeVisible(false)
                            iAddService.clAddByOwn.makeVisible(false)
                            lblNoResults.makeVisible(false)

                        }
                    } else {
                        rvSelect.makeVisible(false)
                        ivNoSearchResults.makeVisible(true)
                        iAddService.clAddByOwn.makeVisible(true)
                        lblNoResults.makeVisible(true)
                    }
                }
                is Result.Failure -> {
                    if (it.errorMessage.isNotEmpty()) {
                        CommonUtils.showToast(activity, it.errorMessage, true)
                        hideLoader()
                    }
                }
            }
        }
    }

    private fun setScreen() {
        arguments?.let {
            isInventory = it.getBoolean(AppENUM.IS_INVENTORY, false)
            isTwoWheeler = it.getBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, false)
        }

        if (!isInventory) {
            setRecentSearchAdapter()
            binding.iAddService.lblAddService.text =
                resources.getString(R.string.add_part_as_your_own)
        }
    }

    private fun setRecentSearchAdapter() {
        binding.lblRecentSearches.makeVisible(true)
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT) {
            val recentList =
                viewModel.getStringSharedPreference(AppENUM.RECENT_SEARCHES_ACCESSORIES, "")
            val recentSearches = recentList.split(",")
            if (recentSearches.isNotEmpty() && !recentSearches.contains("")) {
                (RecentSearchesAdapter(this)).apply {
                    this.setData(recentSearches.toMutableList())
                    binding.rvSelect.adapter = this
                    binding.rvSelect.makeVisible(true)
                }
            } else {
                (RecentSearchesAdapter(this)).apply {
                    this.setData(resources.getStringArray(R.array.recent_searches_accessories)
                        .toMutableList())
                    binding.rvSelect.adapter = this
                    binding.rvSelect.makeVisible(true)
                }
            }
        } else {
            val recentList =
                viewModel.getStringSharedPreference(AppENUM.RECENT_SEARCHES_RETAILER, "")
            val recentSearches = recentList.split(",")
            if (recentSearches.isNotEmpty() && !recentSearches.contains("")) {
                (RecentSearchesAdapter(this)).apply {
                    this.setData(recentSearches.toMutableList())
                    binding.rvSelect.adapter = this
                    binding.rvSelect.makeVisible(true)
                }
            } else {
                (RecentSearchesAdapter(this)).apply {
                    this.setData(resources.getStringArray(R.array.recent_searches_retailer)
                        .toMutableList())
                    binding.rvSelect.adapter = this
                    binding.rvSelect.makeVisible(true)
                }
            }
        }
    }

    private fun addTextWatcher() {
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun afterTextChanged(p0: Editable?) {
                searchText = p0.toString().trim()
                if (searchText.isNotEmpty()) {
                    binding.lblRecentSearches.makeVisible(false)
                    binding.rvSelect.adapter = null
                    showLoader()
                    binding.iAddService.tvSearchText.text = searchText

                    when (getShopType) {
                        AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> viewModel.getServicesAPI(
                            searchText,
                            isTwoWheeler)

                        AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                            viewModel.getAccessoriesPartsList(searchText, isTwoWheeler)
                        }

                        else -> {
                            viewModel.getRetailersPartsList(searchText, isTwoWheeler)
                        }
                    }
                }
            }
        })
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.parentLL -> {
                CommonUtils.genericCastOrNull<GetInventoryPartItemResponse>(v.getTag(R.id.parentLL))
                    ?.let {
                        binding.rvSelect.adapter = null
                        listener?.onActionItem(it)
                        dismiss()
                    }
            }

            R.id.rlBase -> {
                dismiss()
            }

            R.id.tvItemNameIRS -> {
                CommonUtils.genericCastOrNull<String>(v.getTag(R.id.model))?.let {
                    binding.etSearch.setText(it)
                    binding.etSearch.setSelection(binding.etSearch.text?.length ?: 0)
                    binding.rvSelect.adapter = null
                }
            }

            R.id.btnAddServiceByOwn -> addPartByOwn()

            R.id.btnDeleteCustomService -> deleteCustomPart(v.getTag(R.id.model) as GetInventoryPartItemResponse)
        }
    }

    private fun deleteCustomPart(model: GetInventoryPartItemResponse) =
        lifecycleScope.launchWhenStarted {
            when (getShopType) {

                AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {

                    showLoader()
                    val isDeleted = if(isTwoWheeler) viewModel.deleteCustomBikePart(model.skuId?.toLong())
                        else viewModel.deleteAccessoriesCustomPart(model.skuId)
                    hideLoader()
                    if (isDeleted) {
                        CommonUtils.showToast(requireContext(),
                            resources.getString(R.string.part_deleted_successfully))
                        viewModel.getAccessoriesPartsList(searchText, isTwoWheeler)
                    } else CommonUtils.showToast(requireContext(),
                        resources.getString(R.string.unable_to_delete_part))

                }

                AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {

                    showLoader()
                    val isDeleted = if(isTwoWheeler) viewModel.deleteCustomBikePart(model.skuId?.toLong())
                    else viewModel.deleteRetailersCustomPart(model.skuId)
                    hideLoader()
                    if (isDeleted) {
                        CommonUtils.showToast(requireContext(),
                            resources.getString(R.string.part_deleted_successfully))
                        viewModel.getRetailersPartsList(searchText, isTwoWheeler)
                    } else CommonUtils.showToast(requireContext(),
                        resources.getString(R.string.unable_to_delete_part))

                }

                else -> { // Car Workshop
                    showLoader()
                    val isDeleted = if(isTwoWheeler) viewModel.deleteCustomBikePart(model.skuId?.toLong())
                    else viewModel.deleteCustomPart(model.skuId)
                    hideLoader()
                    if (isDeleted) {
                        CommonUtils.showToast(requireContext(),
                            resources.getString(R.string.part_deleted_successfully))
                        viewModel.getServicesAPI(searchText, isTwoWheeler)
                    } else CommonUtils.showToast(requireContext(),
                        resources.getString(R.string.unable_to_delete_part))

                }

            }
        }

    private fun addPartByOwn() = lifecycleScope.launchWhenStarted {
        when (getShopType) {
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {

                val isAdded = if (isTwoWheeler) viewModel.addTwoWheelerServiceByOwn(searchText)
                else viewModel.addAccessoriesPartByOwn(searchText)
                if (isAdded) {
                    CommonUtils.showToast(requireContext(),
                        resources.getString(R.string.part_added_successfully))
                    viewModel.getAccessoriesPartsList(searchText, isTwoWheeler)
                } else CommonUtils.showToast(requireContext(),
                    resources.getString(R.string.failed_to_add_part))

            }

            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {

                val isAdded = if (isTwoWheeler) viewModel.addTwoWheelerServiceByOwn(searchText)
                else viewModel.addSparesRetailerPartByOwn(searchText)
                if (isAdded) {
                    CommonUtils.showToast(requireContext(),
                        resources.getString(R.string.part_added_successfully))
                    viewModel.getRetailersPartsList(searchText, isTwoWheeler)
                } else CommonUtils.showToast(requireContext(),
                    resources.getString(R.string.failed_to_add_part))

            }

            else -> {
                val isAdded =
                    if (isTwoWheeler) viewModel.addTwoWheelerServiceByOwn(searchText)
                    else viewModel.addWorkshopServiceByOwn(searchText)
                if (isAdded) {
                    CommonUtils.showToast(requireContext(),
                        resources.getString(R.string.part_added_successfully))
                    viewModel.getServicesAPI(searchText, isTwoWheeler)
                } else CommonUtils.showToast(requireContext(),
                    resources.getString(R.string.failed_to_add_part))
            }

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.providePartItemResponse().value = null
    }


}