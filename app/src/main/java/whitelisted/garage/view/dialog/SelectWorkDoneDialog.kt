package whitelisted.garage.view.dialog

import android.os.Bundle
import android.view.View
import whitelisted.garage.R
import whitelisted.garage.api.response.WorkDoneResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogSelectWorkDoneBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.adapter.WorkDoneAdapter

class SelectWorkDoneDialog(private val listener: ActionListener?) : BaseDialogFragment() {

    private val binding by viewBinding(DialogSelectWorkDoneBinding::inflate)
    private lateinit var workDoneList: MutableList<WorkDoneResponseModel>
    private lateinit var workDoneAdapter: WorkDoneAdapter
    private var selectedWorkDoneId: Int? = null
    private var selectedWorkDoneName: String? = ""
    private var selectedWorkDonePosition = -1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        arguments?.let {
            binding.tvPartName.text = it.getString(AppENUM.SELECTED_PART)
            (it.getParcelableArray(AppENUM.WORK_DONE_LIST) as? Array<WorkDoneResponseModel>)?.let { list ->
                workDoneList = list.toMutableList()
            }
        }

        if (::workDoneList.isInitialized) {
            workDoneList.forEach {
                it.isSelected = false
            }
            workDoneAdapter = WorkDoneAdapter(workDoneList, this)
            binding.rvWorkDone.adapter = workDoneAdapter

        }
        binding.btnNext.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.cbWorkDone -> {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let { position ->
                    var i = 0
                    workDoneList.forEach {
                        if (it.isSelected) {
                            it.isSelected = false
                        }
                        i++
                    }
                    workDoneList[position].isSelected = true
                    binding.rvWorkDone.post {
                        workDoneAdapter.notifyItemRangeChanged(0, workDoneList.size)
                    }
                    selectedWorkDonePosition = position
                    selectedWorkDoneId = workDoneList[position].id
                    selectedWorkDoneName = workDoneList[position].name
                }
            }
            R.id.btnNext -> {
                if (selectedWorkDonePosition != -1) {
                    dismiss()
                    FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.ADD_ITEM,
                        Bundle().apply {
                            putString(AppENUM.SELECTED_PART,
                                arguments?.getString(AppENUM.SELECTED_PART, ""))
                            putString(AppENUM.SELECTED_WORK_DONE, selectedWorkDoneName)
                        },
                        object : ActionListener {
                            override fun onActionItem(extra: Any?, extra2: Any?) {
                                listener?.onActionItem(selectedWorkDonePosition, extra)
                            }
                        })?.let { dialog ->
                        dialog.show(parentFragmentManager, dialog.javaClass.name)
                    }
                } else {
                    CommonUtils.showToast(requireContext(),
                        getString(R.string.error_select_work_done),
                        true)
                }
            }
        }
    }

}