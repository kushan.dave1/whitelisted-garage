package whitelisted.garage.view.dialog

import android.os.Bundle
import android.view.View
import whitelisted.garage.R
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.DialogWorkshopAddressNotAvlBinding
import whitelisted.garage.utils.AppENUM

class WorkshopAddressNotAvlDialog(private val listener: ActionListener?) : BaseDialogFragment() {

    private val binding by viewBinding(DialogWorkshopAddressNotAvlBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        clickListener()
        setData()
    }

    private fun clickListener() {
        binding.btnGoToWorkshop.setOnClickListener(this)
    }

    private fun setData() {
        arguments?.let {
            binding.tvHeader.text = it.getString(AppENUM.DATA, "")
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnGoToWorkshop -> {
                dismiss()
                listener?.onActionItem(true)
            }
        }
    }
}