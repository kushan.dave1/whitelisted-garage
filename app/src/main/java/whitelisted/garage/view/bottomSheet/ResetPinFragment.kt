package whitelisted.garage.view.bottomSheet

import android.os.Bundle
import android.view.View
import whitelisted.garage.R
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.databinding.FragmentResetPinSheetBinding

class ResetPinFragment(val listener: ActionListener?) : BaseBottomSheetFragment() {

    private val binding by viewBinding(FragmentResetPinSheetBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        binding.btnSubmit.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnSubmit -> {
                listener?.onActionItem(binding.etPin.text.toString())
                dismiss()
            }
        }
    }
}