package whitelisted.garage.view.bottomSheet

import android.os.Bundle
import android.view.View
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.response.LanguageData
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.databinding.FragmentBottomLanguageSelectionBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.view.adapter.LanguageAdapter
import whitelisted.garage.viewmodels.LanguageBottomSheetViewModel

class LanguageBottomFragment(val listener: ActionListener?) : BaseBottomSheetFragment() {

    private val binding by viewBinding(FragmentBottomLanguageSelectionBinding::inflate)
    private var languageValue = AppENUM.LanguageConstants.LAN_DEFAULT
    private val viewModel: LanguageBottomSheetViewModel by viewModel()


    private var isFrom = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupScreen()
        getData()
        setUpData()
    }

    override fun getTheme(): Int {
        return R.style.AppBottomSheetDialogTheme
    }

    private fun getData() {
        arguments?.let {
            isFrom = it.getBoolean(AppENUM.IS_FROM, false)
            languageValue = if (isFrom) {
                it.getString(AppENUM.LANGUAGE, AppENUM.LanguageConstants.LAN_DEFAULT)
            } else {
                viewModel.getStringSharedPreference(AppENUM.RefactoredStrings.LANGUAGE_CODE,
                    AppENUM.LanguageConstants.LAN_DEFAULT)
            }
        }
    }

    private fun setUpData() {
        val enableList = mutableListOf(
            false,
            false,
            false,
            false, // false,
            //  false,
            //  false,
            //  false,
            //  false
        )

        when (languageValue) { //            AppENUM.LanguageConstants.LAN_HINGLISH -> {
            //                enableList[0] = true
            //            }
            AppENUM.LanguageConstants.LAN_ENGLISH -> {
                enableList[1] = true
            }
            AppENUM.LanguageConstants.LAN_HINDI -> {
                enableList[0] = true
            }
            AppENUM.LanguageConstants.LAN_GUJARAT -> {
                enableList[2] = true
            }
            AppENUM.LanguageConstants.LAN_TELEAGU -> {
                enableList[3] = true
            } //            AppENUM.LanguageConstants.LAN_MARATHI -> {
            //                enableList[2] = true
            //            }
            //            AppENUM.LanguageConstants.LAN_KANNADA -> {
            //              enableList[4] = true
            //            }

            //            AppENUM.LanguageConstants.LAN_BANGLA -> {
            //                enableList[6] = true
            //            }
            //            AppENUM.LanguageConstants.LAN_TAMIL -> {
            //                enableList[8] = true
            //            }
        }
        binding.rvLanguage.adapter = LanguageAdapter(this, enableList)
    }

    private fun setupScreen() {
        binding.btnSave.setOnClickListener(this)
        binding.imageClose.setOnClickListener(this)
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_LANGUAGE_SELECTION)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_INIT_LANGUAGE_SELECTION,
                this)
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnSave -> {
                dismiss()
            }

            R.id.imageClose -> {
                dismiss()
            }

            R.id.parenLayout -> {
                whitelisted.garage.utils.CommonUtils.genericCastOrNull<LanguageData>(v.getTag(R.id.parenLayout))
                    ?.let {
                        if (isFrom) {
                            dismiss()
                            setLagValueToLister(it.string)
                        } else {
                            languageValue = it.string
                            setLanguage(languageValue)
                            restartTheApp()
                        }
                    }
            }
        }
    }

    private fun setLanguage(s: String) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_LANGUAGE_SELECTION)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.LANGUAGE, s)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_LANGUAGE_SELECTION,
                this)
        }

        when (s) {
            AppENUM.LanguageConstants.HINDI -> {
                setResource(AppENUM.LanguageConstants.LAN_HINDI)
            }
            AppENUM.LanguageConstants.HINGLISH -> {
                setResource(AppENUM.LanguageConstants.LAN_HINGLISH)
            }
            AppENUM.LanguageConstants.ENGLISH -> {
                setResource(AppENUM.LanguageConstants.LAN_ENGLISH)
            }
            AppENUM.LanguageConstants.BANGLA -> {
                setResource(AppENUM.LanguageConstants.LAN_BANGLA)
            }
            AppENUM.LanguageConstants.GUJARAT -> {
                setResource(AppENUM.LanguageConstants.LAN_GUJARAT)
            }
            AppENUM.LanguageConstants.KANNADA -> {
                setResource(AppENUM.LanguageConstants.LAN_KANNADA)
            }
            AppENUM.LanguageConstants.MARATHI -> {
                setResource(AppENUM.LanguageConstants.LAN_MARATHI)
            }
            AppENUM.LanguageConstants.TAMIL -> {
                setResource(AppENUM.LanguageConstants.LAN_TAMIL)
            }
            AppENUM.LanguageConstants.TELEAGU -> {
                setResource(AppENUM.LanguageConstants.LAN_TELEAGU)
            }
        }
    }

    private fun setLagValueToLister(s: String) {
        when (s) {
            AppENUM.LanguageConstants.HINDI -> {
                listener?.onActionItem((AppENUM.LanguageConstants.LAN_HINDI))
            }
            AppENUM.LanguageConstants.HINGLISH -> {
                listener?.onActionItem((AppENUM.LanguageConstants.LAN_HINGLISH))
            }
            AppENUM.LanguageConstants.ENGLISH -> {
                listener?.onActionItem((AppENUM.LanguageConstants.LAN_ENGLISH))
            }
            AppENUM.LanguageConstants.BANGLA -> {
                listener?.onActionItem((AppENUM.LanguageConstants.LAN_BANGLA))
            }
            AppENUM.LanguageConstants.GUJARAT -> {
                listener?.onActionItem((AppENUM.LanguageConstants.LAN_GUJARAT))
            }
            AppENUM.LanguageConstants.KANNADA -> {
                listener?.onActionItem((AppENUM.LanguageConstants.LAN_KANNADA))
            }
            AppENUM.LanguageConstants.MARATHI -> {
                listener?.onActionItem((AppENUM.LanguageConstants.LAN_MARATHI))
            }
            AppENUM.LanguageConstants.TAMIL -> {
                listener?.onActionItem((AppENUM.LanguageConstants.LAN_TAMIL))
            }
            AppENUM.LanguageConstants.TELEAGU -> {
                listener?.onActionItem((AppENUM.LanguageConstants.LAN_TELEAGU))
            }
        }
    }

    private fun setResource(language: String) {
        viewModel.putStringSharedPreference(AppENUM.RefactoredStrings.LANGUAGE_CODE, language)
    }

}