package whitelisted.garage.view.bottomSheet

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.IntentFilter
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import com.google.android.gms.auth.api.phone.SmsRetriever
import org.greenrobot.eventbus.EventBus
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.broadcast_receivers.MySMSBroadcastReceiver
import whitelisted.garage.databinding.FragmentVerifyOtpIdBinding
import whitelisted.garage.eventBus.OTPDetectedEvent
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.viewmodels.VerifyOtpFragmentViewModel

class VerifyOtpBottomSheetFragment(val listener: ActionListener?) : BaseBottomSheetFragment(),
    View.OnKeyListener, TextWatcher {
    var mobileNumber = ""
    var otp = ""
    private lateinit var countDownTimer: CountDownTimer
    private var secondsLeft: Int = 30
    private val binding by viewBinding(FragmentVerifyOtpIdBinding::inflate)


    override fun getTheme(): Int {
        return R.style.AppBottomSheetDialogTheme
    }

    private val viewModel: VerifyOtpFragmentViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()

    }

    @SuppressLint("SetTextI18n")
    private fun setupScreen() {
        //startSMSListener()
        arguments?.let {
            otp = it.getString(AppENUM.OTP,"") ?: ""
            mobileNumber = it.getString(AppENUM.MOBILE_NUMBER,"") ?: ""
        }
        binding.tvOtpSend.text = getString(R.string.otp_sned_to) + mobileNumber
        isCancelable = true
        addTextWatchers()
        binding.tvSendAgain.setOnClickListener(this)
        binding.btnVerifyOTP.setOnClickListener(this)
        observeResponses()
        startOtpTimer()
    }

    private fun observeResponses() {
        viewModel.provideGetOtpResponse().observe(viewLifecycleOwner) {
            binding.llLoader.llMaterialLoader.visibility = View.GONE
            when (it) {
                is Result.Success<*> -> {
                    it.body.let { data ->
                        val result = CommonUtils.genericCastOrNull<ServerResponse<String>>(data)
                        otp = result?.data ?: ""
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.send_successFully))
                        startOtpTimer()
                    }

                }
                is Result.Failure<*> -> {
                    CommonUtils.showToast(activity, it.errorMessage)
                }
            }
        }

        viewModel.provideVerifyOtpResponse().observe(viewLifecycleOwner) {
            binding.llLoader.llMaterialLoader.visibility = View.GONE
            when (it) {
                is Result.Success -> {
                    dismiss()
                    listener?.onActionItem(binding.etOtp1.text.toString() + binding.etOtp2.text.toString() + binding.etOtp3.text.toString() + binding.etOtp4.text.toString())

                }
                is Result.Failure -> {
                    CommonUtils.showToast(activity, it.errorMessage, true)
                }
            }
        }
    }

    private fun checkForm(): Boolean {
        if (binding.etOtp1.text.toString().isEmpty() || binding.etOtp2.text.toString()
                .isEmpty() || binding.etOtp3.text.toString().isEmpty() || binding.etOtp4.text.toString()
                .isEmpty()) {
            CommonUtils.showToast(activity, getString(R.string.alert_invalid_otp), true)
            when {
                binding.etOtp1.text.toString().isEmpty() -> binding.etOtp1.requestFocus()
                binding.etOtp2.text.toString().isEmpty() -> binding.etOtp2.requestFocus()
                binding.etOtp3.text.toString().isEmpty() -> binding.etOtp3.requestFocus()
                binding.etOtp4.text.toString().isEmpty() -> binding.etOtp4.requestFocus()
            }
            binding.llLoader.llMaterialLoader.makeVisible(false)
            return false
        }
        return true
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.tvSendAgain -> {
                binding.llLoader.llMaterialLoader.makeVisible(true) // EventBus.getDefault().post(ResendOtpEvent())
                viewModel.callGetOTPAPI(mobileNumber)
            }
            R.id.btnVerifyOTP -> {
                if (checkForm()) {

                    binding.llLoader.llMaterialLoader.visibility = View.VISIBLE
                    viewModel.callVerifyOtpAPI(mobileNumber,
                        binding.etOtp1.text.toString() + binding.etOtp2.text.toString() + binding.etOtp3.text.toString() + binding.etOtp4.text.toString())
                }

            }
        }
    }

    private fun startOtpTimer() {
        try {
            countDownTimer = object : CountDownTimer(30000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    binding.tvExpectOtp.makeVisible(true)
                    binding.clgSendAgain.makeVisible(false)
                    secondsLeft = (millisUntilFinished / 1000).toInt()
                    binding.tvExpectOtp.text = getString(R.string.expect_otp_in,
                        secondsLeft.toString() + " " + getString(R.string.seconds))
                }

                override fun onFinish() {
                    binding.tvExpectOtp.makeVisible(false)
                    binding.clgSendAgain.makeVisible(true)
                }
            }

            binding.tvExpectOtp.text =
                getString(R.string.expect_otp_in, "30 " + getString(R.string.seconds))
            countDownTimer.start()
        } catch (e: Exception) {//
        }
    }

    private fun addTextWatchers() {
        binding.etOtp1.addTextChangedListener(this)
        binding.etOtp2.addTextChangedListener(this)
        binding.etOtp3.addTextChangedListener(this)
        binding.etOtp4.addTextChangedListener(this)
        binding.etOtp1.setOnKeyListener(this)
        binding.etOtp2.setOnKeyListener(this)
        binding.etOtp3.setOnKeyListener(this)
        binding.etOtp4.setOnKeyListener(this)
    }

    override fun onKey(p0: View?, keyCode: Int, p2: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_DEL) { //this is for backspace
            if (binding.etOtp4.text?.isEmpty() == true) binding.etOtp3.requestFocus()
            if (binding.etOtp3.text?.isEmpty() == true) binding.etOtp2.requestFocus()
            if (binding.etOtp2.text?.isEmpty() == true) binding.etOtp1.requestFocus()

        }
        return false
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {//
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {//
    }

    override fun afterTextChanged(p0: Editable?) {
        when {
            binding.etOtp1.text?.isEmpty() == true -> binding.etOtp1.requestFocus()
            binding.etOtp2.text?.isEmpty() == true -> binding.etOtp2.requestFocus()
            binding.etOtp3.text?.isEmpty() == true -> binding.etOtp3.requestFocus()
            binding.etOtp4.text?.isEmpty() == true -> binding.etOtp4.requestFocus()
            else -> {
                CommonUtils.hideKeyboard(requireActivity()) //   btnRegister.callOnClick()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        countDownTimer.cancel()

    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        when (event) {
            is OTPDetectedEvent -> {
                if (event.otpString != null) {
                    binding.etOtp1.setText(event.otpString[0].toString())
                    binding.etOtp2.setText(event.otpString[1].toString())
                    binding.etOtp3.setText(event.otpString[2].toString())
                    binding.etOtp4.setText(event.otpString[3].toString())
                    binding.btnVerifyOTP.callOnClick()
                }
            }
        }
    }

    private fun startSMSListener() {
        val smsReceiver: MySMSBroadcastReceiver?
        try {
            smsReceiver = MySMSBroadcastReceiver()
            val client = SmsRetriever.getClient(requireContext())
            val retriever = client.startSmsRetriever()
            retriever.addOnSuccessListener {
                val listener = object : MySMSBroadcastReceiver.OTPReceiveListener {
                    override fun onOTPReceived(otp: String) {
                        Log.d("otp",
                            otp) //                        Toast.makeText(context, otp, Toast.LENGTH_SHORT).show()
                        EventBus.getDefault().post(OTPDetectedEvent(otp))
                    }

                    override fun onOTPTimeOut() {
                        Log.d("otp", "Timed Out.")
                    }
                }
                smsReceiver.initOTPListener(listener)
                requireContext().registerReceiver(smsReceiver,
                    IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION))
            }
            retriever.addOnFailureListener {
                Log.d("otp", "Problem to start listener") //Problem to start listener
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


}