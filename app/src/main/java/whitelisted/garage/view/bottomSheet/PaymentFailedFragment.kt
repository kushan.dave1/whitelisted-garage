package whitelisted.garage.view.bottomSheet

import android.os.Bundle
import android.view.View
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.databinding.FragmentPaymentFailedBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.viewmodels.SparesShopFragmentViewModel

class PaymentFailedFragment : BaseBottomSheetFragment() {

    private val binding by viewBinding(FragmentPaymentFailedBinding::inflate)
    private val sparesShopSharedViewModel: SparesShopFragmentViewModel by sharedViewModel()
    private var isAccCart = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        isAccCart = arguments?.getBoolean(AppENUM.IS_ACCESSORIES_CART, false) ?: false

        binding.ivCrossFPF.setOnClickListener(this)
        binding.btnRetryPayment.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.ivCrossFPF -> {
                dismiss()
            }
            R.id.btnRetryPayment -> {
                fireRetryEvent()
                sparesShopSharedViewModel.provideRetryPaymentEvent().postValue("retry")
                dismiss()
            }
        }
    }


    private fun fireRetryEvent() {
        val bundle = Bundle()
        if (isAccCart) bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.TOTAL_AMOUNT,
            sparesShopSharedViewModel.provideGetCartResponse().cartList[0].discountedAmount.toString())
        else bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.TOTAL_AMOUNT,
            sparesShopSharedViewModel.provideGetCartResponse().cartList[1].discountedAmount.toString())
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.PAYMENT_MODE,
            AppENUM.MODE_ONLINE)
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_SPARES_RETRY_PAYMENT)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.RETRY_PAYMENT,
            bundle)
    }

}