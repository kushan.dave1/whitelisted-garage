package whitelisted.garage.view.bottomSheet

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.normal.TedPermission
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.SendInstantReminderRequest
import whitelisted.garage.api.response.GetCustomerReminderResponse
import whitelisted.garage.api.response.PreviousReminder
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.base.BaseEventBusModel
import whitelisted.garage.databinding.FragmentInstantReminderBinding
import whitelisted.garage.eventBus.GoCoinsAddedEvent
import whitelisted.garage.network.Result
import whitelisted.garage.utils.*
import whitelisted.garage.utils.CommonUtils.makeInVisible
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.viewmodels.InstantReminderViewModel

class InstantReminderFragment(val listener: ActionListener?) : BaseBottomSheetFragment() {

    private val binding by viewBinding(FragmentInstantReminderBinding::inflate)
    private var languageValue = AppENUM.LanguageConstants.LAN_DEFAULT
    private var cusName = AppENUM.RefactoredStrings.cusName
    private var amount = AppENUM.RefactoredStrings.amount
    private var countryCode = AppENUM.RefactoredStrings.defaultCountryCode
    private var getCustomerReminderResponse: GetCustomerReminderResponse? = null
    private val viewModel: InstantReminderViewModel by viewModel()
    private var resultLauncher: ActivityResultLauncher<Intent>? = null
    private var isGoCoinsSufficient = false

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            initializeResultLauncher()
            setOnShowListener {
                (it as? BottomSheetDialog)?.let { sheet ->
                    sheet.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
                        ?.let { sheetView ->
                            BottomSheetBehavior.from(sheetView).apply {
                                state = BottomSheetBehavior.STATE_EXPANDED
                            }
                        }
                }
            }
        }
    }

    private fun initializeResultLauncher() {
        resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == Activity.RESULT_OK) { // There are no request codes
                    CommonUtils.fetchContactFromCursor(requireActivity(), result).apply {
                        binding.etCusName.setText(name)
                        binding.etPhoneNumber.setText(phoneNumber)
                    }
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeResponses()
        setupScreen()
    }

    private fun clickListener() {
        binding.imageCloseSendReminder.setOnClickListener(this)
        binding.llSendReminderBtn.setOnClickListener(this)
        binding.tvLanguage.setOnClickListener(this)
        binding.tvViewAll.setOnClickListener(this)
        binding.imgContact.setOnClickListener(this)
        addTextWatcher()
    }

    private fun addTextWatcher() {
        binding.etCusName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?,
                p1: Int,
                p2: Int,
                p3: Int) { // nothing

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //  nothing

            }

            override fun afterTextChanged(string: Editable?) {
                if (string.toString().isNotEmpty()) {
                    cusName = string.toString()
                    setMessage(cusName = cusName, amount = amount)
                } else {
                    setMessage(amount = amount)
                }
            }
        })
        binding.etAmount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?,
                p1: Int,
                p2: Int,
                p3: Int) { //  nothing
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //  nothing

            }

            override fun afterTextChanged(string: Editable?) {
                if (string.toString().isNotEmpty()) {
                    amount = string.toString()
                    setMessage(amount = amount, cusName = cusName)
                } else {
                    setMessage(cusName = cusName)
                }
            }
        })
    }


    private fun getContactsFromAddressBook() {
        TedPermission.create().setPermissionListener(object : PermissionListener {
            override fun onPermissionGranted() {
                val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
                resultLauncher?.launch(intent)
            }

            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) { // do nothing
            }
        }).setPermissions(Manifest.permission.READ_CONTACTS).check()

    }

    private fun setupScreen() = with(binding) {
        clickListener()
        isCancelable = true
        imgCountryFlag.let {
            ImageLoader.loadImage(it,
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_FLAG, ""))
        }
        countryCode = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE, "")
        tvCountryCode.text = countryCode
        languageValue = viewModel.getStringSharedPreference(AppENUM.RefactoredStrings.LANGUAGE_CODE,
            AppENUM.LanguageConstants.LAN_DEFAULT)
        setUpLanguage()
        llLoader.llMaterialLoader.makeVisible(true)
        viewModel.getCustomerReminder()

        checkAndHideLanguage()
    }

    private fun checkAndHideLanguage() {
        if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                AppENUM.RefactoredStrings.defaultCountryCode) != AppENUM.RefactoredStrings.defaultCountryCode) {
            binding.tvLanguage.makeInVisible(true)
        }
    }

    private fun observeResponses() {
        observeSendInstantReminder()
        observeCustomerReminder()
    }

    private fun setUpLanguage() = with(binding) {
        when (languageValue) {
            AppENUM.LanguageConstants.LAN_HINGLISH -> {
                tvLanguage.text = AppENUM.LanguageConstants.HINGLISH
            }
            AppENUM.LanguageConstants.LAN_HINDI -> {
                tvLanguage.text = AppENUM.LanguageConstants.HINDI
            }
            AppENUM.LanguageConstants.LAN_ENGLISH -> {
                tvLanguage.text = AppENUM.LanguageConstants.ENGLISH
            }
            AppENUM.LanguageConstants.LAN_BANGLA -> {
                tvLanguage.text = AppENUM.LanguageConstants.BANGLA
            }
            AppENUM.LanguageConstants.LAN_GUJARAT -> {
                tvLanguage.text = AppENUM.LanguageConstants.GUJARAT
            }
            AppENUM.LanguageConstants.LAN_KANNADA -> {
                tvLanguage.text = AppENUM.LanguageConstants.KANNADA
            }
            AppENUM.LanguageConstants.LAN_MARATHI -> {
                tvLanguage.text = AppENUM.LanguageConstants.MARATHI
            }
            AppENUM.LanguageConstants.LAN_TAMIL -> {
                tvLanguage.text = AppENUM.LanguageConstants.TAMIL
            }
            AppENUM.LanguageConstants.LAN_TELEAGU -> {
                tvLanguage.text = AppENUM.LanguageConstants.TELEAGU
            }
        }
    }

    private fun observeCustomerReminder() {
        viewModel.provideGetReminderResponse().observe(viewLifecycleOwner) {
            binding.llLoader.llMaterialLoader.makeVisible(false)
            when (it) {
                is Result.Success -> {
                    getCustomerReminderResponse = it.body.data //
                    setData(it.body.data)
                }
                is Result.Failure -> { //
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    private fun setData(data: GetCustomerReminderResponse) {

        data.let { getCustomerReminderResponse ->
            when (getCustomerReminderResponse.previousReminder?.size) {
                0 -> {
                    binding.llPreviewReminder.makeVisible(false)
                }
                1 -> {
                    binding.llPreviewReminder.makeVisible(true)
                    setDataToCustomerDetails1(getCustomerReminderResponse.previousReminder)
                }
                else -> {
                    binding.llPreviewReminder.makeVisible(true)
                    setDataToCustomerDetails(getCustomerReminderResponse.previousReminder)
                    binding.llPreviewReminder.makeVisible(true)
                }
            }
            val goCoinBalance = viewModel.getSharedPreference().getString(requireContext(),AppENUM.GO_COIN_BALANCE_AMOUNT).toLongOrNull() ?: 0
            isGoCoinsSufficient = goCoinBalance >= (getCustomerReminderResponse.reminderCost ?: 0)
            binding.tvReminderGoCoinCost.text = (getCustomerReminderResponse.reminderCost ?: 0).toString()
            setMessage(cusName = cusName, amount = amount) //setMessageText(data.msgText)
        }
    }

    private fun setDataToCustomerDetails(previousReminder: List<PreviousReminder>?) = with(binding) {
        previousReminder?.let {
            val data1 = previousReminder[0]
            val data2 = previousReminder[1]
            tvCusName1.text = data1.customerName
            tvCusName2.text = data2.customerName
            tvCusMobile1.text = data1.mobile
            tvCusMobile2.text = data2.mobile
            "${viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL)}${
                CommonUtils.convertDoubleTo2DecimalPlaces(data1.amount ?: 0.0)
            }".apply { tvOrderAmount1.text = this }
            "${viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL)}${
                CommonUtils.convertDoubleTo2DecimalPlaces(data2.amount ?: 0.0)
            }".apply { tvOrderAmount2.text = this }
            tvFirstName1.text = CommonUtils.getFirstLetter(data1.customerName ?: "")
            tvFirstName2.text = CommonUtils.getFirstLetter(data2.customerName ?: "")
        }
    }

    private fun setDataToCustomerDetails1(previousReminder: List<PreviousReminder>?) = with(binding) {
        previousReminder?.let {
            llRem2.makeVisible(false)
            tvCusMobile1.setPadding(0, 0, 0, 50)
            val data1 = previousReminder[0]
            tvCusName1.text = data1.customerName
            tvCusMobile1.text = data1.mobile
            "${viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL)}${
                CommonUtils.convertDoubleTo2DecimalPlaces(data1.amount ?: 0.0)
            }".apply { tvOrderAmount1.text = this }
            tvFirstName1.text = CommonUtils.getFirstLetter(data1.customerName ?: "")
        }
    }

    private fun observeSendInstantReminder() {
        viewModel.provideSendReminderResponse().observe(viewLifecycleOwner) {
            binding.llLoader.llMaterialLoader.makeVisible(false)
            when (it) {
                is Result.Success -> { //
                    dismiss()
                    listener?.onActionItem(AppENUM.OPEN_DIALOG)
                }
                is Result.Failure -> { //
                    if (it.code==423)
                        dismiss()
                    CommonUtils.showToast(requireContext(), it.errorMessage)
                }
            }
        }
    }

    private fun setMessage(cusName: String = AppENUM.RefactoredStrings.cusName,
        currency: String = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            getString(R.string.currency_symbol_default)),
        amount: String = AppENUM.RefactoredStrings.amount,
        shopName: String = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME,
            ""),
        ownerName: String = viewModel.getStringSharedPreference(AppENUM.MOBILE_NUMBER, "")) {
        when (languageValue) {
            AppENUM.LanguageConstants.LAN_ENGLISH -> {
                binding.tvMessageText.text = getString(R.string.instant_rem_eng,
                    cusName,
                    currency,
                    amount,
                    shopName,
                    shopName,
                    ownerName) //"Hi  $cusName\n\n\n A balance of $currency$amount is due for your purchase from $shopName\nPlease pay the due amount, to keep enjoying the credit services for future orders.\n\nThank you\n$shopName\n$ownerName\n\nTeam Easy Garage & Spares App"
            }
            AppENUM.LanguageConstants.LAN_HINDI -> {
                binding.tvMessageText.text = getString(R.string.instant_rem_hindi,
                    cusName,
                    currency,
                    amount,
                    shopName,
                    shopName,
                    ownerName) // message = "नमस्ते $cusName\n\n\nसे आपकी खरीदारी के लिए $currency$amount बकाया है। $shopName\nभविष्य की खरीदारियों के लिए क्रेडिट सेवाओं का आनंद लेते रहने के लिए कृपया देय राशि का भुगतान करें।\n\nशुक्रिया\n$shopName\n$ownerName\n\nTeam Easy Garage & Spares App"
            }
            AppENUM.LanguageConstants.LAN_GUJARAT -> {
                binding.tvMessageText.text = getString(R.string.instant_rem_gu,
                    cusName,
                    shopName,
                    cusName,
                    amount,
                    shopName,
                    ownerName) //  "હાય $cusName\n\n\n $shopName પરથી તમારી ખરીદી માટે $currency$amount.ની બાકી રકમ બાકી છે. ભાવિ ઓર્ડર માટે ક્રેડિટ સેવાઓનો આનંદ માણતા રહેવા કૃપા કરીને બાકી રકમ ચૂકવો.\n\n\n\nઆભાર\n$shopName\n$ownerName\n\nTeam Easy Garage & Spares App"
            }
            AppENUM.LanguageConstants.LAN_TELEAGU -> {
                binding.tvMessageText.text = getString(R.string.instant_rem_telegu,
                    cusName,
                    currency,
                    amount,
                    shopName,
                    shopName,
                    ownerName) //  "హాయ్ $cusName\n\n\n$currency$amount నుండి మీరు కొనుగోలు చేసినందుకు రూ. $shopName\nబకాయి ఉంది.\nభవిష్యత్ ఆర్డర్\u200Cల కోసం క్రెడిట్ సేవలను ఆస్వాదిస్తూ ఉండటానికి దయచేసి బకాయి మొత్తాన్ని చెల్లించండి.\n\nధన్యవాదాలు\n$shopName\n$ownerName\n\nTeam Easy Garage & Spares App"
            }
            else -> {
                binding.tvMessageText.text = getString(R.string.instant_rem_eng,
                    cusName,
                    currency,
                    amount,
                    shopName,
                    shopName,
                    ownerName)
            }
        }

    }

    override fun getTheme(): Int {
        return R.style.AppBottomSheetDialogTheme
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) { //
            R.id.imageCloseSendReminder -> {
                dismiss()
            }
            R.id.tvLanguage -> {
                FragmentFactory.fragmentBottomSheet(FragmentFactory.Fragments.LANGUAGE_BOTTOM,
                    Bundle().apply {
                        putBoolean(AppENUM.IS_FROM, true)
                        putString(AppENUM.LANGUAGE, languageValue)
                    },
                    object : ActionListener {
                        override fun onActionItem(extra: Any?, extra2: Any?) {
                            languageValue =
                                (extra as? String) ?: AppENUM.LanguageConstants.LAN_ENGLISH
                            setUpLanguage()
                            setMessage(cusName = cusName, amount = amount)
                        }
                    })?.let { baseFragment ->
                    baseFragment.show(parentFragmentManager, baseFragment.javaClass.name)
                }
            }
            R.id.tvViewAll -> {
                listener?.onActionItem(AppENUM.OPEN_FRAG)
                dismiss()
            }
            R.id.llSendReminderBtn ->  with(binding) {
                if (validateForm()) {
                    llLoader.llMaterialLoader.makeVisible(true)
                    viewModel.sendInstantReminder(SendInstantReminderRequest().apply {
                        this.amount = (etAmount.text.toString().toDoubleOrNull()) ?: 0.0
                        this.mobile = etPhoneNumber.text.toString().trim()
                        this.customerName = etCusName.text.toString().trim()
                        this.countryCode = tvCountryCode.text.toString().trim()
                        this.textMsg = tvMessageText.text.toString().trim()
                        this.language = languageValue
                        firebaseEventSendCusReminder(this.customerName.toString(),
                            this.amount.toString(),
                            this.mobile.toString(),
                            this.textMsg.toString())
                    })
                }
            }
            R.id.imgContact -> {
                getContactsFromAddressBook()
            }
        }
    }

    private fun firebaseEventSendCusReminder(name: String,
        amount: String,
        mobile: String,
        message: String) {
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_INSTANT_REMINDER)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.AMOUNT, amount)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.MESSAGE, message)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.CUSTOMER_NAME, name)
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.MODEL, mobile)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_PICK_COUNTRY,
                this)
        }
    }

    private fun validateForm(): Boolean {
        if (binding.etCusName.text.toString().trim().isEmpty()) {
            CommonUtils.showToast(requireContext(), getString(R.string.cus_name_not_empty))
            return false
        }
        if (binding.etAmount.text.toString().trim().isEmpty() || (binding.etAmount.text.toString().trim()
                .toIntOrNull() ?: 0) <= 0) {
            CommonUtils.showToast(requireContext(), getString(R.string.amount_can_not_be_empty))
            return false
        }
        if (binding.etPhoneNumber.text.toString().trim().isEmpty()) {
            CommonUtils.showToast(requireContext(), getString(R.string.phone_not_valid))
            return false
        }
        if (countryCode == AppENUM.RefactoredStrings.defaultCountryCode && binding.etPhoneNumber.text.toString()
                .trim().length != 10) {
            CommonUtils.showToast(requireContext(), getString(R.string.phone_not_empty))
            return false
        }
        if(!isGoCoinsSufficient) {
            FragmentFactory.fragmentDialog(
                FragmentFactory.Dialogs.INSUFFICIENT_BALANCE_DIALOG)?.let {
                it.show(parentFragmentManager, it.javaClass.name)
            }
            return false
        }
        return true
    }

    override fun onMessageEvent(event: BaseEventBusModel) {
        super.onMessageEvent(event)
        if(event is GoCoinsAddedEvent) {
            isGoCoinsSufficient = true
            binding.llSendReminderBtn.callOnClick()
        }
    }



}