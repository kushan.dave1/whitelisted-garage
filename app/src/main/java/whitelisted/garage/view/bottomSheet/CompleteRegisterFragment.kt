package whitelisted.garage.view.bottomSheet

import android.os.Bundle
import android.view.View
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.AddWorkshopRequest
import whitelisted.garage.api.request.CompleteRegData
import whitelisted.garage.api.request.UpdateProfileRequest
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.databinding.FragmentCompleteRegisterBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.viewmodels.CompleteRegistrationViewModel

class CompleteRegisterFragment(val listener: ActionListener?) : BaseBottomSheetFragment() {

    private val binding by viewBinding(FragmentCompleteRegisterBinding::inflate)
    private var data: CompleteRegData? = null
    private val viewModel: CompleteRegistrationViewModel by viewModel()
    private var isFromHome = false
    private var isOnlyName = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = true

        setupScreen()
    }

    private fun setupScreen() = with(binding) {
        btnSave.setOnClickListener(this@CompleteRegisterFragment)
        etAddress.setOnClickListener(this@CompleteRegisterFragment)
        setObservers()

        arguments?.let {
            isFromHome = it.getBoolean("home", false)
            isOnlyName = it.getBoolean(AppENUM.IS_ONLY_NAME, false)
            data = it.getParcelable(AppENUM.COMPLETE_REG_DATA)
            data?.let { data ->
                etAddress.setText(data.address ?: "")
                etName.setText(data.name ?: "")
                etState.setText(CommonUtils.getStateFromLocation(requireContext(),
                    data.addressData?.latitude ?: 0.0,
                    data.addressData?.longitude ?: 0.0))
                etCity.setText(CommonUtils.getCityFromLocation(requireContext(),
                    data.addressData?.latitude ?: 0.0,
                    data.addressData?.longitude ?: 0.0))
                etPincode.setText(CommonUtils.getPinCodeFromLocation(requireContext(),
                    data.addressData?.latitude ?: 0.0,
                    data.addressData?.longitude ?: 0.0))
            }
        }

        if (isOnlyName) {
            tvHeading.text = getString(R.string.to_continue_please_fill_your_account_details)
            lblShopAddress.makeVisible(false)
            etAddress.makeVisible(false)
            clStateCityPincode.makeVisible(false)
        } else {
            etName.setText(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.OWNER_NAME,
                ""))
            when (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)) {
                AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                    tvHeading.text =
                        getString(R.string.to_continue_please_fill_your_spares_shop_details)
                    lblShopAddress.text = getString(R.string.spares_shop_address)
                }
                AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                    tvHeading.text =
                        getString(R.string.to_continue_please_fill_your_accessories_shop_details)
                    lblShopAddress.text = getString(R.string.acc_shop_address)
                }
                else -> {
                    tvHeading.text =
                        getString(R.string.to_continue_please_fill_your_car_workshop_details)
                    lblShopAddress.text = getString(R.string.car_workshop_address)
                }
            }
        }
    }

    private fun setObservers() {
        viewModel.provideUpdateWorkshopResponse().observe(viewLifecycleOwner) {
            when (it) {
                is Result.Success -> {
                    viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_ADDRESS,
                        binding.etAddress.text.toString())
                    UpdateProfileRequest().apply {
                        name = binding.etName.text.toString()
                        mobile = viewModel.getStringSharedPreference(AppENUM.MOBILE_NUMBER, "")
                        viewModel.updateUserProfile(this)
                    }
                }
                is Result.Failure -> {
                    binding.loaderFCR.llMaterialLoader.makeVisible(false)
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

        viewModel.provideUpdateUserProfileResponse().observe(viewLifecycleOwner) { //hideLoader()
            when (it) {
                is Result.Success -> {
                    viewModel.putStringSharedPreference(AppENUM.UserKeySaveENUM.OWNER_NAME,
                        binding.etName.text.toString())
                    dismiss()
                    listener?.onActionItem(true, binding.etName.text.toString().trim())
                }
                is Result.Failure -> {
                    binding.loaderFCR.llMaterialLoader.makeVisible(false)
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnSave -> {
                if (isOnlyName) {
                    if (binding.etName.text.toString().trim().isEmpty()) {
                        CommonUtils.showToast(requireActivity(),
                            getString(R.string.alert_empty_name))
                        binding.etName.requestFocus()
                    } else {
                        UpdateProfileRequest().apply {
                            name = binding.etName.text.toString()
                            mobile = viewModel.getStringSharedPreference(AppENUM.MOBILE_NUMBER, "")
                            viewModel.updateUserProfile(this)
                        }
                    }
                } else {
                    if (formCheck()) {
                        saveDetails()
                    }
                }
            }
            R.id.etAddress -> {
                dismiss()
                Bundle().apply {
                    this.putBoolean(AppENUM.IS_FROM_COMPLETE_REG, true)
                    this.putString(AppENUM.NAME, binding.etName.text.toString())
                    this.putString(AppENUM.ADDRESS_POST, binding.etAddress.text.toString())
                    this.putBoolean("home", isFromHome)
                    CommonUtils.addFragmentUtil(activity,
                        FragmentFactory.fragment(FragmentFactory.Fragments.ADDRESS_FRAGMENT, this))
                }
            }
        }
    }

    private fun saveDetails() {
        AddWorkshopRequest().apply {
            workshopName =
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME, "")
            ownerId =
                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.OWNER_ID, "").toLong()
            ownerName = binding.etName.text.toString()
            address = binding.etAddress.text.toString()
            mapLink =
                "https://maps.google.com/?q=${data?.addressData?.latitude},${data?.addressData?.longitude}"
            types = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
            latitude = data?.addressData?.latitude
            longitude = data?.addressData?.longitude

            city = binding.etCity.text.toString()
            state = binding.etState.text.toString()
            country = CommonUtils.getCountryFromLocation(requireContext(),
                data?.addressData?.latitude ?: 0.0,
                data?.addressData?.longitude ?: 0.0)
            pin = binding.etPincode.text.toString()

            binding.loaderFCR.llMaterialLoader.makeVisible(true)
            viewModel.updateWorkshop(this)
        }
    }

    private fun formCheck(): Boolean {
        return when {
            binding.etAddress.text.toString().trim().isEmpty() -> {
                CommonUtils.showToast(activity, getString(R.string.alert_empty_address))
                binding.etAddress.requestFocus()
                false
            }
            binding.etCity.text.toString().trim().isEmpty() -> {
                CommonUtils.showToast(requireContext(), getString(R.string.please_enter_city), true)
                binding.etCity.requestFocus()
                false
            }
            binding.etState.text.toString().trim().isEmpty() -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.please_enter_state),
                    true)
                binding.etState.requestFocus()
                false
            }
            binding.etPincode.text.toString().trim().isEmpty() -> {
                CommonUtils.showToast(requireContext(),
                    getString(R.string.please_enter_pincode),
                    true)
                binding.etPincode.requestFocus()
                false
            }
            binding.etName.text.toString().trim().isEmpty() -> {
                CommonUtils.showToast(activity, getString(R.string.alert_empty_name))
                binding.etName.requestFocus()
                false
            }
            else -> {
                true
            }
        }
    }
}