package whitelisted.garage.view.bottomSheet

import android.os.Bundle
import android.view.View
import whitelisted.garage.R
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.databinding.FragmentBottomCancelBidBinding

class CancelWsBidBottomFragment(val listener: ActionListener?) : BaseBottomSheetFragment() {

    private val binding by viewBinding(FragmentBottomCancelBidBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    override fun getTheme(): Int {
        return R.style.AppBottomSheetDialogTheme
    }

    private fun setupScreen() {
        binding.btnSave.setOnClickListener(this)
        binding.imageClose.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnSave -> {
                listener?.onActionItem(true)
                dismiss()
            }

            R.id.imageClose -> {
                dismiss()
            }
        }
    }
}