package whitelisted.garage.view.bottomSheet

import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import whitelisted.garage.R
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.databinding.FragmentEditUpiIdBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils.changeBackgroundDrawable
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.view.fragments.payment.PaymentFragment

class EditUpiIdFragment(val listener: ActionListener?) : BaseBottomSheetFragment(), TextWatcher {

    private val binding by viewBinding(FragmentEditUpiIdBinding::inflate)
    private var fireScreen = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        isCancelable = true

        if (arguments?.getString(AppENUM.UPI_ID, "") != null) {
            binding.etUpiId.setText(arguments?.getString(AppENUM.UPI_ID,""))
            fireScreen =
                arguments?.getString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN, "")
                    ?: ""
        }

        binding.btnContinue.setOnClickListener(this)
        binding.etUpiId.addTextChangedListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnContinue -> {
                Bundle().apply {
                    putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN, fireScreen)
                    FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_QR_GENERATION,
                        this)
                }
                if (binding.etUpiId.text.toString().trim().isNotEmpty() && binding.etUpiId.text.toString().trim()
                        .contains("@") && binding.etUpiId.text.toString().split("@").size == 2) {
                    dismiss()
                    listener?.onActionItem(binding.etUpiId.text.toString().trim())
                } else {
                    binding.etUpiId.background = ContextCompat.getDrawable(requireContext(),
                        R.drawable.bg_input_field_warning)
                    binding.cgWarning.makeVisible(true) //                    CommonUtils.showToast(requireContext(), getString(R.string.error_enter_upi))
                }
            }
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
    }

    override fun afterTextChanged(p0: Editable?) {
        binding.etUpiId.changeBackgroundDrawable(requireContext(),R.drawable.bg_input_field)
        binding.cgWarning.makeVisible(false)
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        if ((activity?.supportFragmentManager?.findFragmentByTag("f4") != null)) (activity?.supportFragmentManager?.findFragmentByTag(
            "f4") as? PaymentFragment)?.onEditUpiDismiss()
    }

}