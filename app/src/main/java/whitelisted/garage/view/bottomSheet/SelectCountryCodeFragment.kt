package whitelisted.garage.view.bottomSheet

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import whitelisted.garage.R
import whitelisted.garage.api.response.CountryCodeResponseModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.databinding.FragmentSelectCountryCodeBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.view.adapter.CountryFlagAdapter
import java.util.*


class SelectCountryCodeFragment(val listener: ActionListener?) : BaseDialogFragment(), TextWatcher {

    private lateinit var selectedCountryCode: String
    private var filteredCCs = mutableListOf<CountryCodeResponseModel>()
    private var countryCode = mutableListOf<CountryCodeResponseModel>()

    private val binding by viewBinding(FragmentSelectCountryCodeBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = true
        selectedCountryCode = arguments?.getString(AppENUM.COUNTRY_CODE, "+91").toString()
        countryCode =
            arguments?.getParcelableArrayList<CountryCodeResponseModel>("") as MutableList<CountryCodeResponseModel>
        setupScreen()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupScreen() {
        binding.imgCancel.setOnClickListener(this)
        binding.etSearch.addTextChangedListener(this)

        if (::selectedCountryCode.isInitialized) {
            countryCode.forEach {
                it.isSelected = false
                if (it.dialCode == selectedCountryCode) {
                    it.isSelected = true
                }
            }
        }

        CountryFlagAdapter(this).apply {
            binding.rvCountryCodes.adapter = this
            setData(countryCode)
        }

        binding.rvCountryCodes.setOnTouchListener { _, _ ->
            CommonUtils.hideKeyboardDialog(this)
            false
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.clBase -> {
                CommonUtils.genericCastOrNull<CountryCodeResponseModel>(v.getTag(R.id.clBase)).let {
                    listener?.onActionItem(it)
                }
                dismiss()
            }

            R.id.imgCancel -> {
                dismiss()
            }
        }
    }

    override fun getTheme(): Int {
        return R.style.AppBottomSheetDialogTheme
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //do nothing
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        if (p0?.toString().toString().trim().isNotEmpty()) {
            filterCountryCodes(p0 ?: "")
        } else {
            (binding.rvCountryCodes.adapter as? CountryFlagAdapter)?.setData(countryCode)
            (binding.rvCountryCodes.adapter as? CountryFlagAdapter)?.notifyDataSetChanged()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun filterCountryCodes(search: CharSequence) {
        filteredCCs.clear()
        countryCode.forEach {
            if (it.name?.toLowerCase(Locale.getDefault())
                    ?.contains(search) == true) filteredCCs.add(it)
        }
        (binding.rvCountryCodes.adapter as? CountryFlagAdapter)?.setData(filteredCCs)
        (binding.rvCountryCodes.adapter as? CountryFlagAdapter)?.notifyDataSetChanged()
    }

    override fun afterTextChanged(p0: Editable?) { //do nothing
    }

}