package whitelisted.garage.view.bottomSheet

import android.net.Uri
import android.os.Bundle
import android.view.View
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.databinding.FragmentSelectImageBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.SearchImageAdapter
import whitelisted.garage.viewmodels.ChooseImageFragmentViewModel

class ChoseImageFragment(val listener: ActionListener?) : BaseBottomSheetFragment() {

    private val binding by viewBinding(FragmentSelectImageBinding::inflate)
    private val viewModel: ChooseImageFragmentViewModel by viewModel()
    private var lisPfUri = mutableListOf<Uri>()
    private var isSingleSelectOnly = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
        observeData()
    }

    private fun observeData() {
        viewModel.providePartItemResponse().observe(viewLifecycleOwner) {
            binding.loader.llMaterialLoader.makeVisible(false)
            when (it) {
                is Result.Success -> {
                    if (!it.body.data.isNullOrEmpty()) {
                        for (value in it.body.data) {
                            lisPfUri.add(Uri.parse(value.icon))
                        }
                        (binding.rvImage.adapter as? SearchImageAdapter)?.setData(lisPfUri)
                    }
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun setupScreen() {
        binding.btnDone.setOnClickListener(this)
        binding.loader.llMaterialLoader.makeVisible(true)
        isCancelable = true
        binding.rvImage.adapter = SearchImageAdapter(this)
        lisPfUri.add(Uri.EMPTY)

        (binding.rvImage.adapter as SearchImageAdapter).setData(lisPfUri)
        arguments?.let {
            isSingleSelectOnly = it.getBoolean(AppENUM.SINGLE_IMAGE)
            it.getString(AppENUM.SEARCH_STRING)?.let { it1 ->
                viewModel.searchImages("car $it1")
            }
        }

        if (isSingleSelectOnly) {
            binding.tvInfo.text = getString(R.string.add_rack_image)
            binding.btnDone.makeVisible(false)
        }
        (binding.rvImage.adapter as SearchImageAdapter).isViewOnly = isSingleSelectOnly
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnDone -> {
                val dataToBeSendToShow = mutableListOf<Uri>()
                val allData = (binding.rvImage.adapter as SearchImageAdapter).dataList
                val allSelectedData = (binding.rvImage.adapter as SearchImageAdapter).dataListSelected
                if (allData?.size == allSelectedData.size) {
                    for (i in 0 until (allSelectedData.size)) {
                        if (allSelectedData[i]) {
                            dataToBeSendToShow.add(allData[i])
                        }
                    }
                }

                listener?.onActionItem(dataToBeSendToShow)
                dismiss()
            }
            R.id.imgBusImage -> {
                CommonUtils.genericCastOrNull<Uri>(v.getTag(R.id.imgBusImage))?.let {
                    listener?.onActionItem(it)
                }
                dismiss()
            }
            R.id.rlAddImage -> {
                CommonUtils.genericCastOrNull<Uri>(v.getTag(R.id.rlAddImage))?.let {
                    listener?.onActionItem(it)
                }
                dismiss()
            }
        }
    }

//    override fun onCancel(dialog: DialogInterface) {
//        super.onCancel(dialog)
//        if ((activity?.supportFragmentManager?.findFragmentByTag("f4") != null)) (activity?.supportFragmentManager?.findFragmentByTag(
//            "f4") as? PaymentFragment)?.onEditUpiDismiss()
//    }

}