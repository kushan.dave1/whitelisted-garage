package whitelisted.garage.view.bottomSheet

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.databinding.FeatureLockedBottomDialogBinding
import whitelisted.garage.databinding.ItemProBenefitsBinding
import whitelisted.garage.utils.*
import whitelisted.garage.viewmodels.DashboardSharedViewModel

class ProLockedFragment : BaseBottomSheetFragment() {

    private val binding by viewBinding(FeatureLockedBottomDialogBinding::inflate)
    private val dashboardViewModel: DashboardSharedViewModel by sharedViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = true
        setupScreen()
    }

    @SuppressLint("SetTextI18n")
    private fun setupScreen() {
        val membership = dashboardViewModel.screensConfigResponse.value?.membership
        val proBenefitList = membership?.membershipList?.filter { it -> it.isActive == true && it.lockScreen == true }
        proBenefitList?.let {
            binding.rvProBenefits.layoutManager =
                GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)
            binding.rvProBenefits.addViews(it,
                ItemProBenefitsBinding::inflate) { binding, item, _ ->
                ImageLoader.loadImage(binding.imgProBenefit, item.icon)
                binding.tvProBenefit.text = item.name
            }
        }

        membership?.membershipPrice?.let {
            binding.btnBuyNow.text = resources.getString(R.string.buy_) + " @ " + dashboardViewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                AppENUM.RefactoredStrings.defaultCurrencySymbol)+ it
        }

        val alertMessage = arguments?.getString(AppENUM.PRO_LOCKED_ALERT_MSG, "")

        if (!alertMessage.isNullOrEmpty()){
            binding.lblUpgrade.text = alertMessage
        }

        binding.tvKnowMore.setOnClickListener(this)
        binding.btnBuyNow.setOnClickListener(this)
        binding.clMain.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.tvKnowMore, R.id.clMain -> {
                dismiss()
                fireInitMembershipEvent()
                CommonUtils.addFragmentUtil(requireActivity(),
                    FragmentFactory.fragment(FragmentFactory.Fragments.SUBSCRIPTION_INFO_FRAGMENT))
            }

            R.id.btnBuyNow-> {
                dismiss()
                firePurchaseMembershipEvent()
                dashboardViewModel.screensConfigResponse.value?.membership?.membershipPrice?.let { price->
                    Bundle().apply {
                        putBoolean(AppENUM.IS_FROM_OUTSIDE, true)
                        putString(AppENUM.IS_FROM,
                            AppENUM.RefactoredStrings.GC_OUTSIDE_VAL_PRO)
                        putString(AppENUM.AMOUNT_TO_BE_PURCHASED, price)
                        CommonUtils.addFragmentUtil(activity,
                            FragmentFactory.fragment(FragmentFactory.Fragments.GO_COINS, this))
                    }
                }

            }
        }
    }

    private fun firePurchaseMembershipEvent() {
        val bundle = Bundle()
        bundle.putString(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FEATURE_LOCKED)
        FirebaseAnalyticsLog.trackFireBaseEventLog(
            FirebaseAnalyticsLog.FirebaseEventNameENUM.PURCHASE_MEMBERSHIP,
            bundle)
    }


    private fun fireInitMembershipEvent() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
            FirebaseAnalyticsLog.FirebaseEventNameENUM.FEATURE_LOCKED)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.MEMBERSHIP_DETAIL,
            bundle)
    }

}