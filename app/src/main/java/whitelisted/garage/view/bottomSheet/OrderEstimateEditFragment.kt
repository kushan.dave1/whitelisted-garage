package whitelisted.garage.view.bottomSheet

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.CompoundButton
import androidx.core.content.ContextCompat
import org.koin.androidx.viewmodel.ext.android.viewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.CreateInventoryRackRequest
import whitelisted.garage.api.request.OEServicesItem
import whitelisted.garage.api.request.OrderInclusionsItem
import whitelisted.garage.api.request.RackItem
import whitelisted.garage.api.response.GetInventoryRackResponse
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.databinding.FragmentOrderEstimatesEditBinding
import whitelisted.garage.network.Result
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.view.adapter.RackListAdapter
import whitelisted.garage.viewmodels.OrderEstimateEditViewModel

class OrderEstimateEditFragment(val listener: ActionListener?) : BaseBottomSheetFragment(),
    TextWatcher, CompoundButton.OnCheckedChangeListener {

    private val binding by viewBinding(FragmentOrderEstimatesEditBinding::inflate)

    private val viewModel: OrderEstimateEditViewModel by viewModel()
    private lateinit var itemModel: OEServicesItem
    private lateinit var itemModelRetailer: OrderInclusionsItem
    private var selectedType = AppENUM.RefactoredStrings.WORKSHOP_CONSTANT
    private lateinit var rackList: MutableList<GetInventoryRackResponse>
    private lateinit var selectedRackId: String //    private late init var originalRackId : String
    private var isInternational = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        isCancelable = true
        isInternational = (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
            AppENUM.RefactoredStrings.defaultCurrency) != AppENUM.RefactoredStrings.defaultCurrency)

        if (isInternational) {
            binding.tilTaxRate.makeVisible(false)
            binding.tilTaxRate2.makeVisible(true)
        } else {
            binding.tilTaxRate.makeVisible(true)
            binding.tilTaxRate2.makeVisible(false)
        }

        checkWorkshopType()
        initializeItemModel()
        setTextWatchers()
        setTaxTypeAdapter()
        setOnClickListeners()
        setObservers()
        setStockUnitsTextWatcher()

        viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            getString(R.string.currency_symbol_default)).let {
            "$it ${getString(R.string.price_per_item_asterisk)}".apply {
                binding.tilPricePerItem.hint = this
            }
            "$it ${getString(R.string.discount_per_item_asterisk)}".apply {
                binding.tilDiscountPerItem.hint = this
            }
        }

        binding.llMaterialLoader.llMaterialLoader.makeVisible(true)
        viewModel.getInventoryRack()
    }

    private fun checkWorkshopType() {
        selectedType = viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
        when (selectedType) {
            AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                itemModel = arguments?.getParcelable(AppENUM.ORDER_MODEL) ?: OEServicesItem()
            }
            AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                itemModel = arguments?.getParcelable(AppENUM.ORDER_MODEL) ?: OEServicesItem()
            }
            AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                itemModelRetailer =
                    arguments?.getParcelable(AppENUM.ORDER_MODEL) ?: OrderInclusionsItem()
            }
        }
    }

    private fun initializeItemModel() {
        if (::itemModel.isInitialized) {
            setDataWithItem()
        } else if (::itemModelRetailer.isInitialized) {
            setDataWithRetailerItem()
        }
    }

    private fun setDataWithRetailerItem() = with(binding) {
        tvPartName.text = itemModelRetailer.serviceName
        tvWorkDone.makeVisible(false)
        if (itemModelRetailer.type.equals("inventory") || itemModelRetailer.partAlreadyAdded == true) {
            cvAddInInventory.visibility = View.GONE
        } else {
            lblInStock.makeVisible(false)
            tvStockLeft.makeVisible(false)
        }
        if (itemModelRetailer.pricePerQuantity != null || (itemModelRetailer.pricePerQuantity
                ?: 0.0) > 0) setData()
    }

    private fun setDataWithItem() = with(binding) {
        tvPartName.text = itemModel.serviceName
        if (itemModel.workDoneName.isNullOrEmpty()) {
            tvWorkDone.makeVisible(false)
        } else {
            tvWorkDone.text = itemModel.workDoneName
        } //            if (!itemModel.type.isNullOrEmpty()) {
        if (itemModel.type == "inventory" || itemModel.partAlreadyAdded == true) {
            cvAddInInventory.makeVisible(false)
        } else {
            lblInStock.makeVisible(false)
            tvStockLeft.makeVisible(false)
        } //            }
        if (itemModel.pricePerQuantity != null || (itemModel.pricePerQuantity ?: 0.0) > 0) setData()
    }

    private fun setOnClickListeners() = with(binding) {
        tilTaxRate.setOnClickListener(this@OrderEstimateEditFragment)
        btnSave.setOnClickListener(this@OrderEstimateEditFragment)
        imgSelectRackToggle.setOnClickListener(this@OrderEstimateEditFragment)
        btnNewRack.setOnClickListener(this@OrderEstimateEditFragment)
        switchAdd.setOnCheckedChangeListener(this@OrderEstimateEditFragment)
        etDiscountPerItem.setOnClickListener(this@OrderEstimateEditFragment)
        etPricePerItem.setOnClickListener(this@OrderEstimateEditFragment)
    }

    private fun setStockUnitsTextWatcher() {
        binding.etStockUnits.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                binding.tvStockLeft.text = binding.etStockUnits.text.toString()
            }

            override fun afterTextChanged(p0: Editable?) { //
            }
        })
    }

    private fun setObservers() {
        viewModel.provideGetInventoryRackResponse().observe(viewLifecycleOwner) {
            binding.llMaterialLoader.llMaterialLoader.makeVisible(false)
            when (it) {
                is Result.Success -> {
                    if (it.body.data.isNotEmpty()) {
                        rackList = it.body.data
                        binding.rvRackList.adapter = RackListAdapter(this)
                        (binding.rvRackList.adapter as RackListAdapter).setData(it.body.data)
                    }
                }
                is Result.Failure -> {}
            }
        }

        viewModel.provideCreateInventoryRackResponse().observe(viewLifecycleOwner) {
            binding.llMaterialLoader.llMaterialLoader.makeVisible(false)
            when (it) {
                is Result.Success -> {
                    selectedRackId = it.body.data.rackId.toString()
                    addItemToSelectedRack()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }

        viewModel.provideUpdateInventoryResponse().observe(viewLifecycleOwner) {
            binding.llMaterialLoader.llMaterialLoader.makeVisible(false)
            when (it) {
                is Result.Success -> {
                    updateModelAndDismiss()
                }
                is Result.Failure -> {
                    CommonUtils.showToast(requireContext(), it.errorMessage, true)
                }
            }
        }
    }

    private fun updateModelAndDismiss() = with(binding) {
        if (::itemModel.isInitialized) {
            itemModel.apply {
                pricePerQuantity = etPricePerItem.text.toString().toDouble()
                quantity = etQuantity.text.toString()
                    .toInt() //                    hsn = etHSN.text.toString()
                discountPerQuantity = etDiscountPerItem.text.toString().toDouble()
                taxRate = if (isInternational) etTaxRate2.text.toString() + "%"
                else etTaxRate.text.toString()
                taxable = tvTaxable.text.toString().replace(viewModel.getStringSharedPreference(
                    AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    getString(R.string.currency_symbol_default)), "").toDouble()
                total = tvTotal.text.toString()
                    .replace(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default)), "").toDouble()
                listener?.onActionItem(this)
            }
        } else if (::itemModelRetailer.isInitialized) {
            itemModelRetailer.apply {
                pricePerQuantity = etPricePerItem.text.toString().toDouble()
                quantity = etQuantity.text.toString()
                    .toInt() //                    itemModel.hsn = etHSN.text.toString()
                discountPerQuantity = etDiscountPerItem.text.toString().toDouble()
                taxRate = if (isInternational) etTaxRate2.text.toString() + "%"
                else etTaxRate.text.toString()
                taxable = tvTaxable.text.toString().replace(viewModel.getStringSharedPreference(
                    AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    getString(R.string.currency_symbol_default)), "").toDouble()
                total = tvTotal.text.toString()
                    .replace(viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default)), "").toDouble()
                listener?.onActionItem(this)
            }
        }
        dismiss()
    }

    private fun setTaxTypeAdapter() {
        if (!isInternational) {
            ArrayList<String>().apply {
                add(getString(R.string.zero_percent))
                add(getString(R.string.eighteen_percent))
                add(getString(R.string.twenty_eight_percent))
                add(getString(R.string.thirty_six_percent))
                ArrayAdapter(requireContext(),
                    R.layout.support_simple_spinner_dropdown_item,
                    this).let {
                    (binding.tilTaxRate.editText as? AutoCompleteTextView)?.setAdapter(it)
                }
            }
        }
    }

    override fun onClick(v: View) {
        super.onClick(v)
        when (v.id) {
            R.id.btnSave -> with(binding) { //                val item = OEServicesItem()
                if (formCheck()) {
                    if (etPricePerItem.text.toString().trim().toDouble() > 1000000) {
                        CommonUtils.showToast(requireContext(),
                            getString(R.string.price_value_error))
                        return
                    }
                    saveEstimateValue()
                } else CommonUtils.showToast(requireContext(),
                    getString(R.string.error_empty_fields),
                    true)
            }

            R.id.tilTaxRate -> CommonUtils.hideKeyboard(requireActivity())

            R.id.imgSelectRackToggle -> with(binding) {
                if (llSelectRack.visibility == View.GONE) {
                    imgSelectRackToggle.setImageResource(R.drawable.ic_arrow_up_gray)
                    llSelectRack.makeVisible(true)
                } else {
                    imgSelectRackToggle.setImageResource(R.drawable.ic_arrow_down_gray)
                    llSelectRack.makeVisible(false)
                }
            }

            R.id.btnNewRack -> with(binding) {
                llSelectRack.makeVisible(false)
                imgSelectRackToggle.makeVisible(false)
                etRackName.isEnabled = true
                etRackName.requestFocus()
                etRackName.hint = getString(R.string.enter_rack_name_)
            }

            R.id.tvRackName -> with(binding) {
                CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.tvRackName))?.let {
                    selectedRackId = rackList[it].id ?: ""
                    imgSelectRackToggle.setImageResource(R.drawable.ic_arrow_down_gray)
                    llSelectRack.makeVisible(false)
                    etRackName.setText(rackList[it].name)
                }
            }

            R.id.etDiscountPerItem -> with(binding) {
                if (etDiscountPerItem.text.toString().trim()
                        .isNotEmpty() && etDiscountPerItem.text.toString().trim()
                        .toDouble() == 0.0) {
                    etDiscountPerItem.setText("")
                }
            }

            R.id.etPricePerItem -> with(binding) {
                if (etPricePerItem.text.toString().trim()
                        .isNotEmpty() && etPricePerItem.text.toString().trim().toDouble() == 0.0) {
                    etPricePerItem.setText("")
                }
            }
        }
    }

    private fun saveEstimateValue() {
        if (binding.switchAdd.isChecked) {
            emptyChecks()
        } else {
            updateModelAndDismiss()
        }
    }

    private fun emptyChecks() = with(binding) {
        if (etRackName.text.toString().trim().isNotEmpty()) {
            if (etStockUnits.text.toString().trim().isNotEmpty()) {
                if (etCostPrice.text.toString().trim().isNotEmpty()) {
                    if (::selectedRackId.isInitialized) {
                        addItemToSelectedRack()
                    } else {
                        CreateInventoryRackRequest().apply {
                            rackName = etRackName.text.toString()
                            llMaterialLoader.llMaterialLoader.visibility = View.VISIBLE
                            viewModel.createInventoryRack(this)
                        }
                    }
                } else {
                    CommonUtils.showToast(requireContext(), getString(R.string.alert_cost_price))
                }
            } else {
                CommonUtils.showToast(requireContext(), getString(R.string.alert_stock_units))
            }
        } else {
            CommonUtils.showToast(requireContext(), getString(R.string.error_select_rack), true)
        }
    }

    private fun addItemToSelectedRack() = with(binding) {
        llMaterialLoader.llMaterialLoader.makeVisible(true)
        val req = CreateInventoryRackRequest()
        req.rackName = etRackName.text.toString()
        val items = mutableListOf<RackItem>()
        val rackItem = RackItem()
        rackItem.partName = tvPartName.text.toString()
        if (etCostPrice.text.toString().trim().isNotEmpty()) rackItem.buyingPrice =
            etCostPrice.text.toString().toDouble()

        if (etPricePerItem.text.toString().trim().isNotEmpty()) rackItem.selling_price =
            etPricePerItem.text.toString().toDouble()

        if (etStockUnits.text.toString().trim().isNotEmpty()) rackItem.quantity =
            etStockUnits.text.toString().toIntOrNull() ?: 0

        if (::itemModel.isInitialized) {
            if (viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                    AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) {
                rackItem.id = itemModel.id
            } else rackItem.skuId = itemModel.skuId.toString()
        } else if (::itemModelRetailer.isInitialized) rackItem.skuId =
            itemModelRetailer.skuId.toString()
        items.add(rackItem)
        req.rackItems = items
        viewModel.addItemToRack(selectedRackId, req)
    }

    private fun formCheck(): Boolean {
        var flag = true
        if (binding.etPricePerItem.text.toString().trim().isEmpty()) flag = false
        if (binding.etQuantity.text.toString().trim().isEmpty()) flag = false
        if (binding.etQuantity.text.toString().isEmpty() || binding.etQuantity.text.toString()
                .toInt() < 1) flag = false
        if (binding.etDiscountPerItem.text.toString().trim().isEmpty()) flag =
            false //        if (etHSN.text?.isEmpty() == true)
        //            flag = false
        if (isInternational) {
            if (binding.etTaxRate2.text.toString().trim().isEmpty()) flag = false
        } else {
            if (binding.etTaxRate.text.toString().trim().isEmpty()) flag = false
        }
        return flag
    }

    private fun setData() = with(binding) {
        if (::itemModel.isInitialized) {
            if ((itemModel.pricePerQuantity
                    ?: 0.0) > 0.0) etPricePerItem.setText(CommonUtils.convertDoubleTo2DecimalPlaces(
                itemModel.pricePerQuantity ?: 0.0))
            if ((itemModel.quantity ?: 0) < (itemModel.totalQuantity ?: 0)) tvStockLeft.text =
                itemModel.totalQuantity?.minus(itemModel.quantity ?: 0).toString()
            etQuantity.setText(itemModel.quantity.toString()) //        etHSN.setText(itemModel.hsn.toString())
            if ((itemModel.discountPerQuantity
                    ?: 0.0) > 0.0) etDiscountPerItem.setText(itemModel.discountPerQuantity.toString())
            if (isInternational) etTaxRate2.setText(itemModel.taxRate.toString().replace("%", ""))
            else etTaxRate.setText(itemModel.taxRate.toString())
            tvTaxable.text = CommonUtils.convertDoubleTo2DecimalPlaces(itemModel.taxable ?: 0.0)
            tvTotal.text = CommonUtils.convertDoubleTo2DecimalPlaces(itemModel.total ?: 0.0)
        } else if (::itemModelRetailer.isInitialized) {
            etPricePerItem.setText(CommonUtils.convertDoubleTo2DecimalPlaces(itemModelRetailer.pricePerQuantity
                ?: 0.0))
            if ((itemModelRetailer.quantity ?: 0) < (itemModelRetailer.totalQuantity
                    ?: 0)) tvStockLeft.text =
                itemModelRetailer.totalQuantity?.minus(itemModelRetailer.quantity ?: 0).toString()
            etQuantity.setText(itemModelRetailer.quantity.toString()) //        etHSN.setText(itemModel.hsn.toString())
            etDiscountPerItem.setText(itemModelRetailer.discountPerQuantity.toString())
            if (isInternational) etTaxRate2.setText(itemModelRetailer.taxRate.toString()
                .replace("%", ""))
            else etTaxRate.setText(itemModelRetailer.taxRate.toString())
            tvTaxable.text =
                CommonUtils.convertDoubleTo2DecimalPlaces(itemModelRetailer.taxable ?: 0.0)
            tvTotal.text = CommonUtils.convertDoubleTo2DecimalPlaces(itemModelRetailer.total ?: 0.0)

        }
    }

    private fun setTextWatchers() {
        etPricePerItemTextWatcher()
        etQuantityTextWatcher()
        etDiscountPerItemTextWatcher()
        binding.etTaxRate.addTextChangedListener(this)
        binding.etTaxRate2.addTextChangedListener(this)
    }

    private fun etDiscountPerItemTextWatcher() = with(binding) {
        etDiscountPerItem.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.isNotEmpty() == true) {
                    if (p0.contains(".") && CommonUtils.countOccurrences(p0.toString(), '.') == 2) {
                        etDiscountPerItem.setText(p0.toString()
                            .subSequence(0, etDiscountPerItem.text.toString().length - 1))
                        etDiscountPerItem.setSelection(etDiscountPerItem.text.toString().length)
                    }
                    updateTaxableAndPrice()
                }
            }
        })
    }

    private fun etQuantityTextWatcher() = with(binding) {
        etQuantity.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.isNotEmpty() == true) {
                    if (p0.contains(".")) {
                        etQuantity.setText(p0.toString()
                            .subSequence(0, etQuantity.text.toString().length - 1))
                        etQuantity.setSelection(etQuantity.text.toString().length)
                    }
                    var totalStockLeft = 0
                    if (::itemModel.isInitialized) {
                        totalStockLeft =
                            itemModel.totalQuantity?.minus(etQuantity.text.toString().toInt()) ?: 0
                    } else if (::itemModelRetailer.isInitialized) {
                        totalStockLeft =
                            itemModelRetailer.totalQuantity?.minus(etQuantity.text.toString()
                                .toInt()) ?: 0
                    }
                    if (totalStockLeft < 0) totalStockLeft = 0
                    tvStockLeft.text = totalStockLeft.toString()
                    updateTaxableAndPrice()
                }
            }
        })
    }

    private fun etPricePerItemTextWatcher() = with(binding) {
        etPricePerItem.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.isNotEmpty() == true) {
                    if (p0.contains(".") && CommonUtils.countOccurrences(p0.toString(), '.') == 2) {
                        etPricePerItem.setText(p0.toString()
                            .subSequence(0, etPricePerItem.text.toString().length - 1))
                        etPricePerItem.setSelection(etPricePerItem.text.toString().length)
                    }
                    updateTaxableAndPrice()
                }
            }
        })
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { //
    }

    override fun afterTextChanged(p0: Editable?) {
        if (p0?.isNotEmpty() == true) {
            updateTaxableAndPrice()
        }
    }

    private fun updateTaxableAndPrice() = with(binding) {
        if (isInternational) {
            if (etPricePerItem.text.toString().trim().isNotEmpty() && etQuantity.text.toString()
                    .trim().isNotEmpty() && etTaxRate2.text.toString().trim()
                    .isNotEmpty() && etDiscountPerItem.text.toString().trim().isNotEmpty()) {
                val totalBeforeTax: Double = etPricePerItem.text.toString().toDouble()
                    .times(etQuantity.text.toString().toInt())
                    .minus(etDiscountPerItem.text.toString().toDouble()
                        .times(etQuantity.text.toString().toInt()))
                val total: Double =
                    totalBeforeTax.plus(totalBeforeTax.times(etTaxRate2.text.toString().trim()
                        .toInt()).div(100))

                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    getString(R.string.currency_symbol_default)) + CommonUtils.convertDoubleTo2DecimalPlaces(
                    totalBeforeTax).apply {
                    tvTaxable.text = this
                }

                viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                    getString(R.string.currency_symbol_default)) + CommonUtils.convertDoubleTo2DecimalPlaces(
                    total).apply {
                    tvTotal.text = this
                }
            }
        } else {
            if (etPricePerItem.text?.isNotEmpty() == true && etQuantity.text?.isNotEmpty() == true && etTaxRate.text.isNotEmpty() && etDiscountPerItem.text?.isNotEmpty() == true) {
                try {
                    val totalBeforeTax: Double = etPricePerItem.text.toString().toDouble()
                        .times(etQuantity.text.toString().toInt())
                        .minus(etDiscountPerItem.text.toString().toDouble()
                            .times(etQuantity.text.toString().toInt()))
                    val total: Double =
                        totalBeforeTax.plus(totalBeforeTax.times(etTaxRate.text.toString()
                            .replace("%", "").toInt()).div(100))
                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default)) + String.format("%.2f",
                        totalBeforeTax).apply {
                        tvTaxable.text = this
                    }

                    viewModel.getStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
                        getString(R.string.currency_symbol_default)) + String.format("%.2f", total)
                        .apply {
                            tvTotal.text = this
                        }
                } catch (e: Exception) {
                    e.printStackTrace()

                }
            }
        }
    }

    override fun onCheckedChanged(p0: CompoundButton?, isChecked: Boolean) {
        if (isChecked) {
            binding.rlAddInInventory.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.bg_add_inventory_enabled)
            binding.llAddInInventory.makeVisible(true)
        } else {
            binding.rlAddInInventory.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.bg_add_inventory_disabled)
            binding.llAddInInventory.makeVisible(false)
        }
    }
}
