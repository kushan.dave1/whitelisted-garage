package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_payment_item.view.*
import whitelisted.garage.R
import whitelisted.garage.api.request.OEServicesItem

class PaymentEstimateItemsAdapter(
    val currencySymbol: String,
    var dataList: MutableList<OEServicesItem>,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_payment_item, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MyViewHolder).bind()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind() {

            val model = dataList[layoutPosition]
            itemView.tvPartName.text = model.serviceName
            itemView.tvBrand.text = model.brandName
            itemView.tvPrice.text = "$currencySymbol${model.total}"
            itemView.tvQuantity.text = model.quantity.toString()

        }
    }

    fun getAdapterList(): List<OEServicesItem> {
        return dataList
    }

    fun addItemToAdapterList(item: OEServicesItem) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}