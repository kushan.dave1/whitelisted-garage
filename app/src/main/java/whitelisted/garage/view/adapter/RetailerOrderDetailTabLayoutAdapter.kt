package whitelisted.garage.view.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment
import whitelisted.garage.view.fragments.orderInclusions.OrderInclusionsFragment
import whitelisted.garage.view.fragments.payment.PaymentFragment
import whitelisted.garage.view.fragments.retailNewOrderDetail.RetailNewOrderDetailFragment

class RetailerOrderDetailTabLayoutAdapter(activity: FragmentActivity) :
    FragmentStateAdapter(activity) {

    private var fragments: MutableList<Fragment> = mutableListOf()

    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                Bundle().apply {
                    putBoolean(AppENUM.IntentKeysENUM.IS_UNDER_TAB, true)
                    putString(AppENUM.IntentKeysENUM.ORDER_ID, OrderDetailWrapperFragment.orderId)
                    (FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_NEW_ORDER,
                        this) as Fragment).let {
                        fragments.add(it)
                        return it
                    }
                }
            }
            1 -> {
                Bundle().apply {
                    putString(AppENUM.IntentKeysENUM.ORDER_ID, OrderDetailWrapperFragment.orderId)
                    (FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_ORDER_INCLUSIONS,
                        this) as Fragment).let {
                        fragments.add(it)
                        return it
                    }
                }
            }

            2 -> {
                Bundle().apply {
                    putString(AppENUM.IntentKeysENUM.ORDER_ID, OrderDetailWrapperFragment.orderId)
                    (FragmentFactory.fragment(FragmentFactory.Fragments.PAYMENT_FRAGMENT,
                        this) as Fragment).let {
                        fragments.add(it)
                        return it
                    }
                }
            }
        }
        return FragmentFactory.fragment(FragmentFactory.Fragments.RETAIL_NEW_ORDER,
            null) as Fragment
    }

    fun updateFragment(position: Int) {
        try { // Check fragment type to make sure it is one we know has an updateView Method

            when (val fragment: Fragment = fragments[position]) {
                is RetailNewOrderDetailFragment -> {
                    fragment.updateScreen()
                }
                is OrderInclusionsFragment -> {
                    fragment.updateScreen()
                }
                is PaymentFragment -> {
                    fragment.updateScreen()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}