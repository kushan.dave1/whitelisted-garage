package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_lie_orders_car.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.OrderListResponseModel
import whitelisted.garage.utils.ImageLoader

class LiveOrderCarAdapter(var dataList: MutableList<OrderListResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return LiveOrderCarItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_lie_orders_car, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as LiveOrderCarItemViewHolder).bind()
    }

    inner class LiveOrderCarItemViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataList[layoutPosition]
            ImageLoader.loadImage(itemView.imageCar, model.car_icon)
            itemView.tvCarName.text = model.carName ?: ""
            view.llLiveOrderCarParent.setOnClickListener {
                view.llLiveOrderCarParent.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }
        }
    }
}