package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_oe_added_package.view.*
import whitelisted.garage.R
import whitelisted.garage.api.request.Packages
import whitelisted.garage.utils.AppENUM

class AddedOEPackagesAdapter(val currencySymbol: String,
    var dataList: MutableList<Packages>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PackageItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_oe_added_package, parent, false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PackageItemViewHolder).bind()
    }

    inner class PackageItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind() {
            val model = dataList[layoutPosition]
            itemView.tvSelPackage.text = model.packageName
            itemView.tvPackagePrice.text = currencySymbol + model.packageTotal
            itemView.imgDelSelPackage.setOnClickListener {
                itemView.imgDelSelPackage.setTag(R.id.model, AppENUM.AdaptersConstant.DEL_PACKAGE)
                itemView.imgDelSelPackage.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<Packages> {
        return dataList
    }


    fun addItemToAdapterList(item: Packages) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}