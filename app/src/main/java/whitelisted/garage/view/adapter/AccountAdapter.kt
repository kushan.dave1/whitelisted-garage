package whitelisted.garage.view.adapter

import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_account.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.AccountItem
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class AccountAdapter(var dataListItemModel: MutableList<AccountItem>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AccountItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_account, parent, false))
    }

    override fun getItemCount(): Int = dataListItemModel.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AccountItemViewHolder).bind()
    }

    inner class AccountItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataListItemModel[layoutPosition]
            itemView.clBaseIA?.let {
                it.id = layoutPosition
            }
            (CommonUtils.getDrawable(itemView.viewClickIA.context,
                R.drawable.bg_account_item) as? GradientDrawable).apply {
                this?.setColor(CommonUtils.getColorParsed(itemView.viewClickIA.context, model.background ?: ""))
                this?.setStroke(2, CommonUtils.getColor(itemView.viewClickIA.context, R.color.screen_bg))
                itemView.viewClickIA?.background = this
            }
            if (model.tags?.isNotEmpty() == true) {
                itemView.tvTags.makeVisible(true)
                itemView.tvTags.text = model.tags[0]
            } else {
                itemView.tvTags.makeVisible(false)
            }
            ImageLoader.loadImage(itemView.imgAccountItem, model.icon)
            itemView.tvItemName.text = model.text
            itemView.tvDescription.text = model.description
            itemView.viewClickIA?.setOnClickListener {
                it.setTag(R.id.position, layoutPosition)
                it.setTag(R.id.model, model.redirection)
                recyclerItemClickListener.onClick(it)
            }
//            itemView.clBaseIA?.setOnClickListener { itemView.viewClickIA?.callOnClick() }
        }
    }

    fun getAdapterList(): List<AccountItem> {
        return dataListItemModel
    }

    fun addItemToAdapterList(itemModel: AccountItem) {
        dataListItemModel.add(itemModel)
        notifyItemInserted(dataListItemModel.size)
    }

}