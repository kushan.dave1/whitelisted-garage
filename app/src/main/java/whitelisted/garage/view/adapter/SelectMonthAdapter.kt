package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_month.view.*
import whitelisted.garage.R

class SelectMonthAdapter(var onClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var dataListItemModel: List<String>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_month, parent, false))
    }

    override fun getItemCount(): Int = dataListItemModel?.size ?: 0
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MyViewHolder).bind()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataListItemModel?.get(layoutPosition)
            itemView.tvMonthItem.text = model
            itemView.tvMonthItem.setOnClickListener {
                it.setTag(R.id.tvMonthItem, layoutPosition)
                onClickListener.onClick(it)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setUpData(data: List<String>) {
        this.dataListItemModel = data
        notifyDataSetChanged()
    }
}