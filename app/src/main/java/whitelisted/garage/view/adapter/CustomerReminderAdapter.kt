package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.PreviousReminder
import whitelisted.garage.databinding.ItemCustomerReminderBinding
import whitelisted.garage.utils.CommonUtils
import kotlin.random.Random


class CustomerReminderAdapter(val currencySymbol: String,
    var dataListItemModel: List<PreviousReminder>?,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private val images = listOf(R.drawable.ic_circle_blue,
        R.drawable.ic_circle_blue_green,
        R.drawable.ic_circle_mgenta_voilet,
        R.drawable.ic_circle_muglai_green,
        R.drawable.ic_circle_saffron,
        R.drawable.ic_circle_shadow)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CusReminderViewHolder(ItemCustomerReminderBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataListItemModel?.size ?: 0
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CusReminderViewHolder).bind()
    }

    inner class CusReminderViewHolder(var view: ItemCustomerReminderBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind() {
            val model = dataListItemModel?.get(layoutPosition)
            model?.let { it ->
                view.tvCusName.text = it.customerName
                view.tvCusMobile.text = it.mobile
                "$currencySymbol${CommonUtils.convertDoubleTo2DecimalPlaces(it.amount ?: 0.0)}".apply {
                    view.tvOrderAmount.text = this
                }
                view.tvFirstName1.text = CommonUtils.getFirstLetter(it.customerName ?: "")
                val random = Random.nextInt(images.size)
                view.frame1.background = ContextCompat.getDrawable(view.frame1.context,
                    images[random]) //   CommonUtils.getDrawable()
                if (model.due_collected == false) {
                    view.llCollectDue.alpha = 1f
                    view.llCollectDue.setOnClickListener { llCollectDue ->
                        view.llCollectDue.setTag(R.id.model, model)
                        recyclerItemClickListener.onClick(llCollectDue)
                    }
                    view.llSendReminder.alpha = 1f
                    view.llSendReminder.setOnClickListener { llSendReminder ->
                        view.llSendReminder.setTag(R.id.model, model)
                        recyclerItemClickListener.onClick(llSendReminder)
                    }
                } else {
                    view.llCollectDue.alpha = .5f
                    view.llSendReminder.alpha = .5f
                }
            }

        }
    }
}