package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_new_enquiry_order.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.AccRetailBiding
import whitelisted.garage.databinding.ItemWsReceivedBidBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible

class WorkShopBidReceivedAdapter(var accRetailBiding: List<AccRetailBiding>,
    val recyclerItemClickListener: View.OnClickListener,
    var isCompletedSection: Boolean,
    var shopId: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var bestPrice = accRetailBiding.minWithOrNull(Comparator.comparingDouble {
        it.totalAmount?.toDouble() ?: 0.0
    })

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return WSBidReceivedViewHolder(ItemWsReceivedBidBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = accRetailBiding.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as WSBidReceivedViewHolder).bind()
    }

    inner class WSBidReceivedViewHolder(var view: ItemWsReceivedBidBinding) :
        RecyclerView.ViewHolder(view.root) {

        @SuppressLint("SetTextI18n")
        fun bind() {
            val modelData = accRetailBiding[layoutPosition]
            if (modelData.isContactNumber?.contains(shopId) == true) {
                view.clgMobileMasked.makeVisible(false)
                view.clgDirection.makeVisible(true)
            } else {
                view.clgMobileMasked.makeVisible(true)
                view.clgDirection.makeVisible(false)
                var maskedPhone: String?
                val initialFour = modelData.contactNumber?.substring(0, 4)
                val noOfStars = (modelData.contactNumber?.length ?: 10).minus(4)
                maskedPhone = initialFour
                if (noOfStars > 0) {
                    for (i in 1..(noOfStars)) {
                        maskedPhone += "*"
                    }
                }
                itemView.tvPhoneNumberMasked?.text = maskedPhone
            }
            view.tvDistanceInKM.text = "${modelData.distance ?: 0} ${
                CommonUtils.getString(view.tvDistanceInKM.context, R.string.km_away)
            }"
            view.tvWorkshopName.text = modelData.workshopName
            view.tvTotalItem.text = (modelData.totalItem ?: 0).toString()
            view.tvItemAvl.text = (modelData.totalAvailableAmount ?: 0).toString()
            view.tvTotalCost.text = (modelData.totalAmount ?: 0).toString()
            view.tvItemNotAvl.text =
                ((modelData.totalItem ?: 0) - (modelData.totalAvailableAmount ?: 0)).toString()

            val itemsText = StringBuilder()
            var i = 0
            var countMoreItemsText = ""
            var lineCount = 0
            kotlin.run breaking@{
                modelData.items?.forEach {
                    if (itemView.tvItems.text.length > 80) lineCount = 1
                    if (modelData.isItemTextExpanded == false && lineCount == 1) {
                        countMoreItemsText = "+ ${modelData.items?.size?.minus(i)} ${
                            itemView.tvItems.context.getString(R.string.more_items_sc)
                        }"
                        "${itemView.tvItems.text} $countMoreItemsText".apply {
                            itemView.tvItems?.text = this
                        }
                        return@breaking
                    }
                    itemsText.append(it).append(" | ")
                    itemView.tvItems?.text = itemsText.substring(0, itemsText.length - 3)

                    i++
                }
            }
            if (lineCount == 1) {
                val spannable = SpannableString(itemView.tvItems.text)
                itemView.tvItems?.text = getClickableTextWithColor(spannable,
                    countMoreItemsText,
                    ContextCompat.getColor(itemView.tvItems.context, R.color.colorAccent),
                    object : (View) -> Unit {
                        override fun invoke(p1: View) {
                            itemView.tvItems.setTag(R.id.tvItems, layoutPosition)
                            recyclerItemClickListener.onClick(itemView.tvItems)
                        }
                    })
                itemView.tvItems?.movementMethod = LinkMovementMethod()
            }

            if (isCompletedSection) {
                if ((modelData.isAccepted == true)) {
                    view.llPrent.background =
                        CommonUtils.getDrawable(view.llPrent.context, R.drawable.bg_white_4)
                    view.imgCall.setImageResource(R.drawable.ic_ws_call)
                    view.imgDirection.setImageResource(R.drawable.ic_ws_location)
                    view.llViewDetails.background =
                        CommonUtils.getDrawable(view.llPrent.context, R.drawable.bg_view_details)
                    view.imgCall.setOnClickListener {
                        view.imgCall.setTag(R.id.model, modelData.contactNumber)
                        recyclerItemClickListener.onClick(it)
                    }
                    view.imgDirection.setOnClickListener {
                        view.imgDirection.setTag(R.id.model, modelData)
                        recyclerItemClickListener.onClick(it)
                    }
                    view.llViewDetails.setOnClickListener {
                        view.llViewDetails.setTag(R.id.model, modelData)
                        recyclerItemClickListener.onClick(it)
                    }
                } else {
                    view.imgCall.setImageResource(R.drawable.ic_ws_call_disable)
                    view.imgDirection.setImageResource(R.drawable.ic_ws_location_disabled)
                    view.llPrent.background = CommonUtils.getDrawable(view.llPrent.context,
                        R.drawable.bg_white_disabled_4)
                    view.llViewDetails.background = CommonUtils.getDrawable(view.llPrent.context,
                        R.drawable.bg_view_details_disale)
                    view.clgMobileMasked.makeVisible(false)
                    view.clgDirection.makeVisible(true)
                }

            } else {
                if (modelData.statusId != AppENUM.WSBiddingStatusConstant.REJECTED) {
                    view.imgCall.setOnClickListener {
                        view.imgCall.setTag(R.id.model, modelData.contactNumber)
                        recyclerItemClickListener.onClick(it)
                    }
                    view.imgDirection.setOnClickListener {
                        view.imgDirection.setTag(R.id.model, modelData)
                        recyclerItemClickListener.onClick(it)
                    }
                    view.llViewDetails.setOnClickListener {
                        view.llViewDetails.setTag(R.id.model, modelData)
                        recyclerItemClickListener.onClick(it)
                    }
                    itemView.llUnlockMobile.setOnClickListener {
                        it.setTag(R.id.model, modelData)
                        recyclerItemClickListener.onClick(it)
                    }
                }
            }
            if (modelData.statusId == AppENUM.WSBiddingStatusConstant.REJECTED) {
                view.tvBestPrice.makeVisible(true)
                view.tvBestPrice.background = ContextCompat.getDrawable(view.tvBestPrice.context,
                    R.drawable.bg_ws_best_rejected)
                view.tvBestPrice.text =
                    CommonUtils.getString(view.tvBestPrice.context, R.string.rejected)
                view.imgCall.setImageResource(R.drawable.ic_ws_call_disable)
                view.imgDirection.setImageResource(R.drawable.ic_ws_location_disabled)
                view.llPrent.background =
                    CommonUtils.getDrawable(view.llPrent.context, R.drawable.bg_white_disabled_4)
                view.llViewDetails.background =
                    CommonUtils.getDrawable(view.llPrent.context, R.drawable.bg_view_details_disale)
                view.clgMobileMasked.makeVisible(false)
                view.clgDirection.makeVisible(true)

            } else {
                view.tvBestPrice.background =
                    ContextCompat.getDrawable(view.tvBestPrice.context, R.drawable.bg_ws_best_price)
                view.tvBestPrice.text =
                    CommonUtils.getString(view.tvBestPrice.context, R.string.best_price)
                if (bestPrice?.totalAmount == modelData.totalAmount) {
                    view.tvBestPrice.makeVisible(true)
                } else {
                    view.tvBestPrice.makeVisible(false)
                }
            }
        }


        private fun getClickableTextWithColor(spannable: Spannable,
            clickableText: String,
            @ColorInt color: Int,
            onClickListener: (View) -> Unit): Spannable {
            val text = spannable.toString()

            val start = text.indexOf(clickableText)
            spannable.setSpan(object : ClickableSpan() {
                override fun onClick(widget: View) {
                    onClickListener.invoke(widget)
                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.color = color
                    ds.isUnderlineText = false
                }
            }, start, start + clickableText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            return spannable
        }
    }

}