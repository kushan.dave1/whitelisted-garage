package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_payment_item.view.*
import whitelisted.garage.R
import whitelisted.garage.api.request.OrderInclusionsItem

class PaymentInclusionsItemsAdapter(
    val currencySymbol: String,
    var dataList: MutableList<OrderInclusionsItem>,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_payment_item, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MyViewHolder).bind()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind() {

            val model = dataList[layoutPosition]
            itemView.tvPartName.text = model.serviceName
            if (model.brandName.isNullOrEmpty()) {
                itemView.tvBrand.text = itemView.tvBrand.context.getString(R.string.generic)
            } else {
                if (model.brandName == "null") {
                    itemView.tvBrand.text = itemView.tvBrand.context.getString(R.string.generic)
                } else itemView.tvBrand.text = model.brandName
            }
            itemView.tvPrice.text = "$currencySymbol${model.total}"
            itemView.tvQuantity.text = model.quantity.toString()

        }
    }

    fun getAdapterList(): List<OrderInclusionsItem> {
        return dataList
    }

    fun addItemToAdapterList(item: OrderInclusionsItem) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}