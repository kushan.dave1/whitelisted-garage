package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.SSHomeBannersResponseModel
import whitelisted.garage.databinding.ItemSsBannerBinding
import whitelisted.garage.utils.ImageLoader

class SSHomeBannersAdapter(var dataList: MutableList<SSHomeBannersResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SSBrandItemViewHolder(ItemSsBannerBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SSBrandItemViewHolder).bind()
    }

    inner class SSBrandItemViewHolder(var binding: ItemSsBannerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]
            ImageLoader.loadImage(binding.imgBanner,
                model.image,
                placeholderImage = R.drawable.ic_placeholder_home_gmb)

            binding.cvSSHomeBanner.setOnClickListener {
                it.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<SSHomeBannersResponseModel> {
        return dataList
    }

}
