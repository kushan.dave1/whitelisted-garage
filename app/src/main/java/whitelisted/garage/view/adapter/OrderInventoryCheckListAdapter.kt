package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_inventory_check.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.OrderInventoryCheckListItemModel

class OrderInventoryCheckListAdapter(var dataListItemModel: MutableList<OrderInventoryCheckListItemModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return WorkshopItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_inventory_check, parent, false))
    }

    override fun getItemCount(): Int = dataListItemModel.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as WorkshopItemViewHolder).bind()
    }

    inner class WorkshopItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataListItemModel[layoutPosition]
            itemView.tvInventoryCheck.text = model.name
            itemView.tvInventoryCheck.isEnabled = model.isSelected ?: false
            if (model.isSelected == true) {
                itemView.tvInventoryCheck.setCompoundDrawablesRelativeWithIntrinsicBounds(0,
                    0,
                    R.drawable.ic_item_check,
                    0)
            } else {
                itemView.tvInventoryCheck.setCompoundDrawablesRelativeWithIntrinsicBounds(0,
                    0,
                    0,
                    0)
            }
            itemView.clItemBase.setOnClickListener {
                if (model.isSelected == true) {
                    model.isSelected = false
                    itemView.tvInventoryCheck.isEnabled = false
                    itemView.tvInventoryCheck.setCompoundDrawablesRelativeWithIntrinsicBounds(0,
                        0,
                        0,
                        0)
                } else {
                    model.isSelected = true
                    itemView.tvInventoryCheck.isEnabled = true
                    itemView.tvInventoryCheck.setCompoundDrawablesRelativeWithIntrinsicBounds(0,
                        0,
                        R.drawable.ic_item_check,
                        0)
                }
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<OrderInventoryCheckListItemModel> {
        return dataListItemModel
    }

    fun addItemToAdapterList(itemModel: OrderInventoryCheckListItemModel) {
        dataListItemModel.add(itemModel)
        notifyItemInserted(dataListItemModel.size)
    }

}