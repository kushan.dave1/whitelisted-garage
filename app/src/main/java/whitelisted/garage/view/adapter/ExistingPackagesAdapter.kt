package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_custom_package.view.*
import kotlinx.android.synthetic.main.item_existing_package.view.*
import kotlinx.android.synthetic.main.item_existing_package.view.tvPackageName
import kotlinx.android.synthetic.main.item_existing_package.view.tvVehicleType
import whitelisted.garage.R
import whitelisted.garage.api.response.AllPackagesResponseModel
import whitelisted.garage.utils.CommonUtils.makeVisible

class ExistingPackagesAdapter(var dataList: MutableList<AllPackagesResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ExistingPackageItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_existing_package, parent, false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ExistingPackageItemViewHolder).bind()
    }

    inner class ExistingPackageItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataList[layoutPosition]
            if (model.isCustom == true) {
                itemView.rlEdit.makeVisible(true)
            } else itemView.rlEdit.makeVisible(false)
            itemView.tvPackageName.text = model.name
            itemView.btnEditPackage.setOnClickListener {
                itemView.btnEditPackage.setTag(R.id.position, layoutPosition)
                itemView.btnEditPackage.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }
            itemView.btnDuplicatePackage.setOnClickListener {
                itemView.btnDuplicatePackage.setTag(R.id.position, layoutPosition)
                itemView.btnDuplicatePackage.setTag(R.id.model,
                    model)
                recyclerItemClickListener.onClick(it)
            }

            if (model.isTwoWheeler == true){
                itemView.tvVehicleType.setViewBackgroundColor(R.color.alt_gray)
                itemView.tvVehicleType.text = itemView.tvVehicleType.context.getString(R.string._2_wheeler)
            }else{
                itemView.tvVehicleType.setViewBackgroundColor(R.color.colorAccent)
                itemView.tvVehicleType.text = itemView.tvVehicleType.context.getString(R.string._4_wheeler)
            }
        }
    }

    fun getAdapterList(): List<AllPackagesResponseModel> {
        return dataList
    }

    fun addItemToAdapterList(item: AllPackagesResponseModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}