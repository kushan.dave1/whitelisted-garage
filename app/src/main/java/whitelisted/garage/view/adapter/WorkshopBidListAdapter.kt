package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.GetWsOngoingBidResponse
import whitelisted.garage.databinding.ItemWsBidBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible

class WorkshopBidListAdapter(var list: List<GetWsOngoingBidResponse>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return WSBBidListViewHolder(ItemWsBidBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = list.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as WSBBidListViewHolder).bind()
    }

    inner class WSBBidListViewHolder(var view: ItemWsBidBinding) :
        RecyclerView.ViewHolder(view.root) {
        @SuppressLint("SetTextI18n")
        fun bind() {
            val modelData = list[layoutPosition]
            view.tvTotalItem.text = (modelData.totalItem ?: 0).toString()
            view.tvOrderId.text = modelData.id
            view.tvOrderIdSecond.text = modelData.id
            modelData.retailerDetails?.let {
                view.tvRetailerName.text = it.workshopName
                view.tvDistance.text = "${it.distance ?: 0} ${
                    CommonUtils.getString(view.tvDistance.context, R.string.km_away)
                }"
            }
            modelData.createdAt?.let {
                if (it.isNotEmpty()) {
                    view.tvJobCardDate.text = CommonUtils.convertToDate(it)
                    view.tvJobCardTime.text = CommonUtils.convertToTime(it)
                }
            }
            if (modelData.isItemTextExpanded == true) {
                view.tvSeeMoreLess.text =
                    CommonUtils.getString(view.tvSeeMoreLess.context, R.string.see_less)
                view.tvSeeMoreLess.setCompoundDrawablesRelativeWithIntrinsicBounds(null,
                    null,
                    CommonUtils.getDrawable(view.tvSeeMoreLess.context,
                        R.drawable.ic_accent_see_less),
                    null)

            } else {
                view.tvSeeMoreLess.text =
                    CommonUtils.getString(view.tvSeeMoreLess.context, R.string.see_more)
                view.tvSeeMoreLess.setCompoundDrawablesRelativeWithIntrinsicBounds(null,
                    null,
                    CommonUtils.getDrawable(view.tvSeeMoreLess.context,
                        R.drawable.ic_accent_see_more),
                    null)
            }
            dataBindingAccToStatus(modelData, view)

            view.llParent.setOnClickListener {
                view.llParent.setTag(R.id.model, modelData)
                recyclerItemClickListener.onClick(it)
            }
            view.tvCall.setOnClickListener {
                view.tvCall.setTag(R.id.model, modelData.retailerDetails?.contactNumber)
                recyclerItemClickListener.onClick(it)
            }
            view.tvDirections.setOnClickListener {
                view.tvDirections.setTag(R.id.model, modelData)
                recyclerItemClickListener.onClick(it)
            }
            view.btnMarkComplete.setOnClickListener {
                view.btnMarkComplete.setTag(R.id.model, modelData)
                recyclerItemClickListener.onClick(it)
            }
            view.tvSeeMoreLess.setOnClickListener {
                modelData.isItemTextExpanded = modelData.isItemTextExpanded != true
                notifyItemChanged(layoutPosition)
            }
        }

        private fun dataBindingAccToStatus(modelData: GetWsOngoingBidResponse,
            view: ItemWsBidBinding) {
            when (modelData.statusId) {
                AppENUM.WSBiddingStatusConstant.NEW -> {
                    view.lblNewOrders.makeVisible(true)
                    view.lblOthersOrders.makeVisible(false)
                    view.llNewBid.makeVisible(true)
                    view.llOGoingBid.makeVisible(false)
                    view.llCompletedBid.makeVisible(false)
                    view.llCost.makeVisible(false)
                    view.llTVBidStatus.text =
                        CommonUtils.getString(view.llTVBidStatus.context, R.string.new_)
                    view.llTVBidStatus.background =
                        CommonUtils.getDrawable(view.llTVBidStatus.context,
                            R.drawable.bg_ws_new_bid)
                    view.llTVBidStatus.setTextColor(CommonUtils.getColor(view.llTVBidStatus.context,
                        R.color.status_completed))
                }
                AppENUM.WSBiddingStatusConstant.ON_GOING -> {
                    view.lblNewOrders.makeVisible(true)
                    view.lblOthersOrders.makeVisible(false)
                    view.llNewBid.makeVisible(false)
                    view.llOGoingBid.makeVisible(true)
                    view.llCompletedBid.makeVisible(false)
                    view.llCost.makeVisible(false)
                    view.llTVBidStatus.text =
                        CommonUtils.getString(view.llTVBidStatus.context, R.string.on_going_small)
                    view.llTVBidStatus.background =
                        CommonUtils.getDrawable(view.llTVBidStatus.context,
                            R.drawable.bg_ws_onngoig_bid)
                    view.llTVBidStatus.setTextColor(CommonUtils.getColor(view.llTVBidStatus.context,
                        R.color.color_sky))
                    view.tvBidReceivedCount.text = (modelData.bidsReceived ?: 0).toString()

                }
                AppENUM.WSBiddingStatusConstant.COMPLETED -> {
                    if (modelData.isItemTextExpanded == true) {
                        view.lblSeeMoreVisibility.makeVisible(true)
                    } else {
                        view.lblSeeMoreVisibility.makeVisible(false)
                    }
                    view.lblNewOrders.makeVisible(false)
                    view.lblOthersOrders.makeVisible(true)
                    view.llNewBid.makeVisible(false)
                    view.llOGoingBid.makeVisible(false)
                    view.llCompletedBid.makeVisible(true)
                    view.llCost.makeVisible(true)
                    view.llTVBidStatus.text = CommonUtils.getString(view.llTVBidStatus.context,
                        R.string.reday_for_delivery)
                    view.llTVBidStatus.background =
                        CommonUtils.getDrawable(view.llTVBidStatus.context,
                            R.drawable.bg_ws_completed_bid)
                    view.llTVBidStatus.setTextColor(CommonUtils.getColor(view.llTVBidStatus.context,
                        R.color.chestnut))
                    view.tvItemCost.text = (modelData.totalAmount ?: 0).toString()
                    modelData.retailerDetails?.let {
                        view.tvAddress.text = it.address ?: ""
                        view.tvAsgndRetailer.text = it.workshopName ?: ""
                        view.tvPhNno.text = (it.contactNumber ?: "").toString()
                    }
                    if (modelData.isActive == true) {
                        view.tvBidStatus.makeVisible(true)
                    } else {
                        view.tvBidStatus.makeVisible(false)
                    }
                }
            }
        }
    }
}