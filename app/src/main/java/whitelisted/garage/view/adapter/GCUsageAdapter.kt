package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.text.SpannableStringBuilder
import android.text.style.ImageSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_gc_usage.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.GoCoinUsageItem
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class GCUsageAdapter(private var goCoiValue: String,
    var currencySymbol: String,
    var dataList: MutableList<GoCoinUsageItem>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return GCUsageItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_gc_usage, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as GCUsageItemViewHolder).bind()
    }

    inner class GCUsageItemViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList[layoutPosition]
            ImageLoader.loadImageWithCallback(itemView.imgGCUsage,
                model.usageUrl,
                object : ImageLoader.ImageLoadCallback {
                    override fun onSuccess() {
                        setButtonText(model.redirection ?: "", view)
                    }

                    override fun onError(throwable: Throwable) { // nothing
                    }
                },
                placeholderImage = R.drawable.ic_placeholder_go_coins_banner)

            view.imgGCUsage.setOnClickListener {
                view.imgGCUsage.setTag(R.id.model, model.redirection)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setButtonText(redirection: String, view: View) {
        when (redirection) {
            AppENUM.ConfigConstants.GMB_COMBO -> {
                view.tvExtraText.makeVisible(true)
                view.tvExtraText.text =
                    "${view.tvExtraText.context.getString(R.string.only)} ${currencySymbol + goCoiValue}"
            }
            AppENUM.ConfigConstants.INSTNAT_REMINDER -> {
                view.tvExtraText.makeVisible(true)
                headerText(view.imgGCUsage.context, view.tvExtraText)
            }
            else -> {
                view.tvExtraText.makeVisible(false)
            }
        }
    }

    private fun headerText(context: Context, textView: TextView) {
        val ssb = SpannableStringBuilder()
        ssb.append(context.getString(R.string.one_sms_))
        ssb.append(" ")
        ssb.append(" ")
        val length = ssb.length
        val d1 = ContextCompat.getDrawable(context, R.drawable.ic_ref_coin)
        d1?.setBounds(0, 0, d1.intrinsicWidth, d1.intrinsicHeight)
        val span1 = ImageSpan(d1!!, ImageSpan.ALIGN_BASELINE)
        ssb.setSpan(span1, length - 1, length, 0)
        ssb.append(" ")
        ssb.append("1")
        textView.text = ssb
    }

    fun getAdapterList(): List<GoCoinUsageItem> {
        return dataList
    }

    fun addItemToAdapterList(item: GoCoinUsageItem) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}