package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.request.GenericItem
import whitelisted.garage.databinding.ItemWsMostUsedPartBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.ImageLoader

class WorkMostUsedPartAdapter(var genericItems: List<GenericItem>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return WSMostUsedPartViewHolder(ItemWsMostUsedPartBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = genericItems.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as WSMostUsedPartViewHolder).bind()
    }

    inner class WSMostUsedPartViewHolder(var view: ItemWsMostUsedPartBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind() {
            val modelData = genericItems[layoutPosition]
            view.tvPartName.text = modelData.partName ?: ""
            if (modelData.images.isNullOrEmpty()) {
                ImageLoader.loadDrawable(
                    CommonUtils.getDrawable(view.imgPart.context, R.drawable.ic_rack_dummy_image),
                    view.imgPart,
                )
            } else {
                ImageLoader.loadImage(view.imgPart, modelData.images?.get(0) ?: "")
            }
            if (modelData.isAdded == true) {
                view.btnAdd.text = CommonUtils.getString(view.btnAdd.context, R.string.added)
                view.btnAdd.setCompoundDrawablesRelativeWithIntrinsicBounds(ContextCompat.getDrawable(
                    view.btnAdd.context,
                    R.drawable.ic_check_accent), null, null, null)
            } else {
                view.btnAdd.text = CommonUtils.getString(view.btnAdd.context, R.string.add)
                view.btnAdd.setCompoundDrawablesRelativeWithIntrinsicBounds(ContextCompat.getDrawable(
                    view.btnAdd.context,
                    R.drawable.ic_add_accent), null, null, null)
            }
            view.btnAdd.setOnClickListener {
                view.btnAdd.setTag(R.id.model, modelData)
                view.btnAdd.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            view.imgPart.setOnClickListener {
                view.imgPart.setTag(R.id.model, modelData.images)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun updateAddBtnState(position: Int) {
        genericItems[position].isAdded = genericItems[position].isAdded != true
        notifyItemChanged(position)
    }

}