package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_bus_show_categories.view.*
import whitelisted.garage.R

class BusShowCategoryAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var data: List<String>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return EditCategoryViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_bus_show_categories, parent, false))
    }

    override fun getItemCount(): Int = data?.size ?: 0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as EditCategoryViewHolder).bind()
    }

    inner class EditCategoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind() {
            itemView.tvCategories.text = data?.get(layoutPosition) ?: ""
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setDataToAdapter(list: List<String>) {
        data = emptyList()
        data = list
        notifyDataSetChanged()
    }

    fun getAdapterData(): List<String>? {
        return data
    }

}