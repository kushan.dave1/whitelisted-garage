package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import whitelisted.garage.api.response.HighlightsItem
import whitelisted.garage.databinding.ItemHighlightsBinding

class ProductHighlightsAdapter(var dataList: List<HighlightsItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layout = ItemHighlightsBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return ProductHighlightsViewHolder(layout)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ProductHighlightsViewHolder).bind()
    }

    inner class ProductHighlightsViewHolder(val binding: ItemHighlightsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            dataList[layoutPosition].let { highlight ->
                binding.ivIH.load(highlight.iconUrl)
                binding.tvHighlight.text = highlight.name
            }
        }
    }
}