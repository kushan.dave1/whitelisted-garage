package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.GetInventoryPartItemResponse
import whitelisted.garage.databinding.ItemAddRackPartBinding
import whitelisted.garage.databinding.ItemPartBinding
import whitelisted.garage.utils.CommonUtils.makeVisible

class AddRackPartAdapter(var listener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var isWorkshopOnly = false
    private var dataList = mutableListOf<GetInventoryPartItemResponse>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ItemAddRackPartBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AddPartItemViewHolder(binding)
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AddPartItemViewHolder).bind()
    }

    inner class AddPartItemViewHolder(private val binding: ItemAddRackPartBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind() = with(binding) {

            val model = dataList[layoutPosition]
            if (isWorkshopOnly) {
                llGenericPartLayout.makeVisible(false)
                llWorkshopPartLayout.makeVisible(true)
                tvWorkShopPartName.text = model.subCategoryName
            } else {
                llGenericPartLayout.makeVisible(true)
                llWorkshopPartLayout.makeVisible(false)
                tvPartName.text = model.subCategoryName
            }
            parentLL.setOnClickListener {
                parentLL.setTag(R.id.parentLL, model)
                listener.onClick(it)
            }
            BtnAdd.setOnClickListener {
                BtnAdd.setTag(R.id.model, model)
                listener.onClick(it)
            }

            // When part is custom
            customGrp.makeVisible(model.workshopId != null || model.status == 99)
            btnDeleteCustomService.setOnClickListener {
                it.setTag(R.id.model,model)
                listener.onClick(it)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(data: MutableList<GetInventoryPartItemResponse>) {
        try {
            dataList.clear()
            dataList = data
            notifyDataSetChanged()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}