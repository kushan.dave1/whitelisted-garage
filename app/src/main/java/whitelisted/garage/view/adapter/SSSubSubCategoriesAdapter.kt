package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.SubArray
import whitelisted.garage.databinding.ItemSubSubCategoryBinding
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class SSSubSubCategoriesAdapter(var dataList: List<SubArray>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemFilterBrandViewHolder(ItemSubSubCategoryBinding.inflate(LayoutInflater.from(
            parent.context), parent, false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemFilterBrandViewHolder).bind()
    }

    inner class ItemFilterBrandViewHolder(var binding: ItemSubSubCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]

            binding.tvSubSubCatImage.text = model.name
            ImageLoader.loadImage(binding.imgSubSubCatImage, model.image)

            binding.clBaseSSISSC.setOnClickListener {
                it.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }

            if (model.isEnable == true) {
                binding.clSSC.alpha = 1f
                binding.tvSoldOutISSC.makeVisible(false)
            } else {
                binding.clSSC.alpha = 0.5f
                binding.tvSoldOutISSC.makeVisible(true)
            }
        }
    }

    fun getAdapterList(): List<SubArray> {
        return dataList
    }

}