package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.request.CreateInventoryRackRequest
import whitelisted.garage.api.response.GetInventoryRackResponse
import whitelisted.garage.databinding.ItemAddRackBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class InventoryAdapter(val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<InventoryAdapter.MyViewHolder>() {
    private var dataList = mutableListOf<GetInventoryRackResponse>()
    override fun getItemCount(): Int = dataList.size

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: MutableList<GetInventoryRackResponse>) {
        dataList.clear()
        dataList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = ItemAddRackBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = dataList[position]
        holder.bind(data)

    }

    inner class MyViewHolder(var binding: ItemAddRackBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: GetInventoryRackResponse) {

            if (!data.images.isNullOrEmpty()) {
                data.images?.get(0).let {
                    ImageLoader.loadImage(binding.imageDummy, it)
                }
            } else {
                ImageLoader.loadDrawable(
                    CommonUtils.getDrawable(binding.imageDummy.context,
                        R.drawable.ic_rack_dummy_image),
                    binding.imageDummy,
                )
            }
            binding.tvRackCount.text = (data.items_count).toString()
            binding.editAddRackName.setText(data.name)
            binding.tvRckName.text = data.name
            if (data.isNewRack == true) {
                binding.imgSaveRack.makeVisible(true)
                binding.imgAddRackItems.makeVisible(false)
                binding.editAddRackName.makeVisible(true)
                binding.tvRckName.makeVisible(false)
                binding.editAddRackName.requestFocus()
            } else {
                binding.imgSaveRack.makeVisible(false)
                binding.imgAddRackItems.makeVisible(true)
                binding.tvRckName.makeVisible(true)
                binding.editAddRackName.makeVisible(false)
            }
            binding.imgSaveRack.setOnClickListener {
                val isUpdate = (data.id == null)
                binding.imgSaveRack.setTag(R.id.imgSaveRack,
                    CreateInventoryRackRequest(emptyList(),
                        binding.editAddRackName.text.toString().trim(),
                        data.images,
                        false,
                        isUpdate = isUpdate,
                        id = data.id))
                recyclerItemClickListener.onClick(it)
            }
            binding.imgAddRackItems.setOnClickListener {
                binding.imgAddRackItems.setTag(R.id.imgAddRackItems, data)
                recyclerItemClickListener.onClick(it)

            }
            binding.editCard.setOnClickListener {
                binding.editCard.setTag(R.id.editCard, layoutPosition)
                recyclerItemClickListener.onClick(it)

            }
            binding.llCard.setOnClickListener {
                if ((data.id != null && data.isNewRack == false)) {
                    binding.llCard.setTag(R.id.llCard, data)
                    recyclerItemClickListener.onClick(it)
                }
            }
        }
    }

    fun getAdapterData(): MutableList<GetInventoryRackResponse> {
        return dataList
    }
}

