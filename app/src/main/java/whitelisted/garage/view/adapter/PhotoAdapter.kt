package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_bus_image.view.*
import kotlinx.android.synthetic.main.item_car_image.view.llDelImage
import kotlinx.android.synthetic.main.item_car_image.view.rlAddImage
import whitelisted.garage.R
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader


class PhotoAdapter(
    val recyclerItemClickListener: View.OnClickListener,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var dataList: MutableList<Uri>? = null
    var isViewOnly = false
    var viewType = 0

    companion object {
        const val VIEW_TYPE_ONE = 0
        const val VIEW_TYPE_TWO = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_ONE) {
            PhotoViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_bus_image, parent, false))
        } else {
            PhotoViewHolderForBidding(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_add_image_bid, parent, false))
        }

    }

    override fun getItemViewType(position: Int): Int {
        return when (viewType) {
            0 -> VIEW_TYPE_ONE
            1 -> VIEW_TYPE_TWO
            else -> {
                0
            }
        }
    }

    override fun getItemCount(): Int = dataList?.size ?: 0
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (viewType == OrderHistoryAdapter.VIEW_TYPE_ONE) {
            (holder as PhotoViewHolder).bind()
        } else {
            (holder as PhotoViewHolderForBidding).bind()
        }
    }

    inner class PhotoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList?.get(layoutPosition)
            if (model == Uri.EMPTY) {
                itemView.rlBusImage.makeVisible(false)
                itemView.rlAddImage.makeVisible(true)
            } else {
                itemView.rlBusImage.makeVisible(true)
                itemView.rlAddImage.makeVisible(false)
                ImageLoader.loadImage(itemView.imgBusImage, model)
            }
            if (isViewOnly) {
                itemView.llDelImage.makeVisible(false)
            } else {
                itemView.llDelImage.makeVisible(true)
            }
            itemView.llDelImage.setOnClickListener {
                itemView.llDelImage.setTag(R.id.position, layoutPosition)
                itemView.llDelImage.setTag(R.id.model, dataList?.get(layoutPosition))
                recyclerItemClickListener.onClick(it)
            }
            itemView.rlAddImage.setOnClickListener {
                itemView.rlAddImage.setTag(R.id.position, layoutPosition)
                itemView.rlAddImage.setTag(R.id.model, dataList?.get(layoutPosition))
                recyclerItemClickListener.onClick(it)
            }
            itemView.imgBusImage.setOnClickListener {
                itemView.imgBusImage.setTag(R.id.position, layoutPosition)
                itemView.imgBusImage.setTag(R.id.model, dataList?.get(layoutPosition))
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    inner class PhotoViewHolderForBidding(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList?.get(layoutPosition)

            if (model == Uri.EMPTY) {
                itemView.rlBusImage.makeVisible(false)
                itemView.rlAddImage.makeVisible(true)
            } else {
                itemView.rlBusImage.makeVisible(true)
                itemView.rlAddImage.makeVisible(false)
                ImageLoader.loadImage(itemView.imgBusImage, model)
            }
            if (isViewOnly) {
                itemView.llDelImage.makeVisible(false)
            } else {
                itemView.llDelImage.makeVisible(true)
            }
            itemView.llDelImage.setOnClickListener {
                itemView.llDelImage.setTag(R.id.position, layoutPosition)
                itemView.llDelImage.setTag(R.id.model, dataList?.get(layoutPosition))
                recyclerItemClickListener.onClick(it)
            }
            itemView.rlAddImage.setOnClickListener {
                itemView.rlAddImage.setTag(R.id.position, layoutPosition)
                itemView.rlAddImage.setTag(R.id.model, dataList?.get(layoutPosition))
                recyclerItemClickListener.onClick(it)
            }
            itemView.imgBusImage.setOnClickListener {
                itemView.imgBusImage.setTag(R.id.position, layoutPosition)
                itemView.imgBusImage.setTag(R.id.model, dataList?.get(layoutPosition))
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): MutableList<Uri> {
        return dataList ?: mutableListOf()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: MutableList<Uri>) {
        dataList = list
        notifyDataSetChanged()

    }

    fun addItemToAdapterList(item: Uri) {
        dataList?.add(item)
        dataList?.size?.let { notifyItemInserted(it) }
    }

    fun removeItemFromList(position: Int) {
        dataList?.removeAt(position)
        notifyItemRemoved(position)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setIsViewOnly(boolean: Boolean) {
        isViewOnly = boolean
        notifyDataSetChanged()
    }
}