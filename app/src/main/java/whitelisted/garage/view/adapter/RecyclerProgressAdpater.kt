package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_recycler_view.view.*
import whitelisted.garage.R
import whitelisted.garage.utils.CommonUtils


class RecyclerProgressAdpater(

    var dataListItemModel: MutableList<Boolean>,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ProgressViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_recycler_view, parent, false))
    }

    override fun getItemCount(): Int = dataListItemModel.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ProgressViewHolder).bind()
    }

    inner class ProgressViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val data = dataListItemModel[layoutPosition]
            if (data) {
                itemView.parentView.background =
                    CommonUtils.getDrawable(itemView.context, R.drawable.bg_active_scroll)
            } else {
                itemView.parentView.background =
                    CommonUtils.getDrawable(itemView.context, R.drawable.bg_inactive_scroll)
            }
        }
    }

    fun getData(): MutableList<Boolean> {
        return dataListItemModel
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(data: MutableList<Boolean>) {
        dataListItemModel.clear()
        dataListItemModel = data
        notifyDataSetChanged()
    }

}