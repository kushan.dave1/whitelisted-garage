package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_go_coin_transaction.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.GoCoinTransactionsItem
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible

class GCTransactionsAdapter(

    var dataList: MutableList<GoCoinTransactionsItem>,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return GCTransactionItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_go_coin_transaction, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as GCTransactionItemViewHolder).bind()
    }

    inner class GCTransactionItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind() {
            val model = dataList[layoutPosition]
            itemView.tvTitle.text = model.heading
            itemView.tvRemarks.text = model.remark
            itemView.tvDate.text = CommonUtils.convertNumMonthDateToNormal(model.date)
            if (model.type == AppENUM.CREDIT) {
                itemView.tvAmount.setTextColor(CommonUtils.getColor(itemView.tvAmount.context,
                    R.color.green_text_color))

                itemView.tvAmount.text = "+${model.amount}"
            } else {
                itemView.tvAmount.setTextColor(CommonUtils.getColor(itemView.tvAmount.context,
                    R.color.red_text_color))
                itemView.tvAmount.text = "-${model.amount}"
            }
            if (layoutPosition == dataList.size - 1) itemView.viewDivider.makeVisible(false)
            else itemView.viewDivider.makeVisible(true)
        }
    }

    fun getAdapterList(): List<GoCoinTransactionsItem> {
        return dataList
    }

    fun addItemToAdapterList(item: GoCoinTransactionsItem) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}