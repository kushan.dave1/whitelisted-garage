package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_payments.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.GetPaymentResponseModel
import whitelisted.garage.utils.CommonUtils

class PaymentsAdapter(
    val currencySymbol: String,
    var dataListResponseModel: MutableList<GetPaymentResponseModel>,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PaymentsItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_payments, parent, false))
    }

    override fun getItemCount(): Int = dataListResponseModel.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PaymentsItemViewHolder).bind()
    }

    inner class PaymentsItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind() {
            val model = dataListResponseModel[layoutPosition]
            itemView.tvTransactionMode.text = model.transactionMode
            itemView.tvPaidAmount.text = "$currencySymbol${model.amount}"
            itemView.tvDiscountValue.text = "$currencySymbol${model.discountValue}"
            itemView.tvTransactionDate.text = CommonUtils.convertDateWithTime(model.createdAt)
        }
    }
}