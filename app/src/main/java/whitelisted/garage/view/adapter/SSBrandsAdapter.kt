package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.SSBrandsResponseModel
import whitelisted.garage.databinding.ItemSsBrandBinding
import whitelisted.garage.utils.ImageLoader

class SSBrandsAdapter(var dataList: MutableList<SSBrandsResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SSBrandItemViewHolder(ItemSsBrandBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SSBrandItemViewHolder).bind()
    }

    inner class SSBrandItemViewHolder(var binding: ItemSsBrandBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]
            ImageLoader.loadImage(binding.ivBrandImageSS, model.image, placeholderImage = R.drawable.ic_placeholder_gbc_banner)
//            model.background?.let {
//                if (it.isNotEmpty()) {
//                    ImageLoader.loadImage(binding.ivBrandImageSS, model.background, placeholderImage = R.drawable.ic_placeholder_gbc_banner)
//                } else { //do nothing
//                }
//            }

            binding.cvBaseISSB.setOnClickListener {
                it.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<SSBrandsResponseModel> {
        return dataList
    }

    fun addItemToAdapterList(item: SSBrandsResponseModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }
}
