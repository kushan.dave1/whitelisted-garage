package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.request.BrandWiseItem
import whitelisted.garage.databinding.ItemBidWsRadSubItemBinding
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class WorkshopBidBrandSubAdapter(var isViewOnly: Boolean,
    var list: MutableList<BrandWiseItem>,
    val recyclerItemClickListener: View.OnClickListener) :

    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SettingViewHolder(ItemBidWsRadSubItemBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = list.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SettingViewHolder).bind()
    }

    inner class SettingViewHolder(var view: ItemBidWsRadSubItemBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind() {
            val modelData = list[layoutPosition]
            view.tvPartName.text = modelData.partName
            view.tvQuantity.text = (modelData.quantity ?: 0).toString()
            view.tvViewQuantity.text = (modelData.quantity ?: 0).toString()

            if (layoutPosition == (list.size - 1)) {
                view.lineView.makeVisible(false)
            } else {
                view.lineView.makeVisible(true)
            }

            if (modelData.remark.isNullOrEmpty()) {
                view.tvAddRemark.makeVisible(true)
                view.llRemarks.makeVisible(false)
            } else {
                view.tvAddRemark.makeVisible(false)
                view.llRemarks.makeVisible(true)
                view.tvRemarkValue.text = modelData.remark
            }

            if (modelData.images.isNullOrEmpty()) {
                view.llTvPartPhoto.makeVisible(false)
                view.llPhotoContainer.makeVisible(false)
            } else {
                view.llTvPartPhoto.makeVisible(true)
                view.llPhotoContainer.makeVisible(true)
                setImageToView(view, modelData.images)
            }

            view.img1.setOnClickListener {
                view.img1.setTag(R.id.position, 0)
                view.img1.setTag(R.id.model, modelData.images)
                recyclerItemClickListener.onClick(it)
            }
            view.img2.setOnClickListener {
                view.img2.setTag(R.id.position, 1)
                view.img2.setTag(R.id.model, modelData.images)
                recyclerItemClickListener.onClick(it)
            }
            view.img3.setOnClickListener {
                view.img3.setTag(R.id.position, 2)
                view.img3.setTag(R.id.model, modelData.images)
                recyclerItemClickListener.onClick(it)
            }
            view.img4.setOnClickListener {
                view.img4.setTag(R.id.position, 3)
                view.img4.setTag(R.id.model, modelData.images)
                recyclerItemClickListener.onClick(it)
            }
            view.tvMorePhotos.setOnClickListener {
                view.tvMorePhotos.setTag(R.id.position, layoutPosition)
                view.tvMorePhotos.setTag(R.id.model, modelData.images)
                recyclerItemClickListener.onClick(it)
            }
            view.imgEditRemark.setOnClickListener {
                view.tvAddRemark.callOnClick()
            }
            view.tvAddRemark.setOnClickListener {
                view.tvAddRemark.setTag(R.id.IsGeneric, false)
                view.tvAddRemark.setTag(R.id.position, layoutPosition)
                view.tvAddRemark.setTag(R.id.model, modelData)
                recyclerItemClickListener.onClick(it)
            }
            view.imgWpPluse.setOnClickListener {
                view.imgWpPluse.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
                list[layoutPosition].quantity = list[layoutPosition].quantity?.plus(1)
                view.tvQuantity.text = (modelData.quantity ?: 0).toString()
            }
            view.imgWpMinuse.setOnClickListener {

                if ((list[layoutPosition].quantity?.toInt() ?: 0) > 1) {

                    list[layoutPosition].quantity = list[layoutPosition].quantity?.minus(1)
                    view.tvQuantity.text = (modelData.quantity ?: 0).toString()
                } else {
                    list.removeAt(layoutPosition)
                    notifyItemRemoved(layoutPosition)
                }
                view.imgWpMinuse.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            if (isViewOnly) {
                view.llQuantityManageLayout.makeVisible(false)
                view.tvViewQuantity.makeVisible(true)
                view.tvAddRemark.makeVisible(false)
                view.imgEditRemark.makeVisible(false)
            }

        }

        @SuppressLint("SetTextI18n")
        private fun setImageToView(view: ItemBidWsRadSubItemBinding, images: List<String>?) {
            view.tvMorePhotos.makeVisible(false)
            when (images?.size) {
                1 -> {
                    ImageLoader.loadImage(view.img1, images[0])
                    view.img1.makeVisible(true)
                    view.img2.makeVisible(false)
                    view.img3.makeVisible(false)
                    view.img4.makeVisible(false)
                }
                2 -> {
                    ImageLoader.loadImage(view.img1, images[0])
                    ImageLoader.loadImage(view.img2, images[1])
                    view.img1.makeVisible(true)
                    view.img2.makeVisible(true)
                    view.img3.makeVisible(false)
                    view.img4.makeVisible(false)
                }
                3 -> {
                    ImageLoader.loadImage(view.img1, images[0])
                    ImageLoader.loadImage(view.img2, images[1])
                    ImageLoader.loadImage(view.img3, images[2])
                    view.img1.makeVisible(true)
                    view.img2.makeVisible(true)
                    view.img3.makeVisible(true)
                    view.img4.makeVisible(false)
                }
                4 -> {
                    ImageLoader.loadImage(view.img1, images[0])
                    ImageLoader.loadImage(view.img2, images[1])
                    ImageLoader.loadImage(view.img3, images[2])
                    ImageLoader.loadImage(view.img4, images[3])
                    view.img1.makeVisible(true)
                    view.img2.makeVisible(true)
                    view.img3.makeVisible(true)
                    view.img4.makeVisible(true)
                }
                else -> {
                    ImageLoader.loadImage(view.img1, images?.get(0) ?: "")
                    ImageLoader.loadImage(view.img2, images?.get(1))
                    ImageLoader.loadImage(view.img3, images?.get(2))
                    ImageLoader.loadImage(view.img4, images?.get(3))
                    view.img1.makeVisible(true)
                    view.img2.makeVisible(true)
                    view.img3.makeVisible(true)
                    view.img4.makeVisible(true)
                    view.tvMorePhotos.makeVisible(true)
                    view.tvMorePhotos.text =
                        "+${((images?.size ?: 0) - 4)} ${view.tvMorePhotos.context.getString(R.string.more_photos)}"
                }
            }
        }
    }
}