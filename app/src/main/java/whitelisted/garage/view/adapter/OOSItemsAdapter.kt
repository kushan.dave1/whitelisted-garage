package whitelisted.garage.view.adapter

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.SSItemModel
import whitelisted.garage.databinding.ItemOosItemBinding
import whitelisted.garage.utils.CommonUtils.roundOff
import whitelisted.garage.utils.ImageLoader

class OOSItemsAdapter(val currencySymbol: String,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val dataList: MutableList<SSItemModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return OOSItemViewHolder(ItemOosItemBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as OOSItemViewHolder).bind()
    }

    inner class OOSItemViewHolder(var binding: ItemOosItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]
            ImageLoader.loadImage(binding.ivPartImage,
                model.imageUrl,
                placeholderImage = R.drawable.ic_placeholder_square)
            binding.tvPartName.text = model.title
            "${binding.tvPartId.context.getString(R.string.part_no_colon)} ${model.skuCode}".apply {
                binding.tvPartId.text = this
            }
            "$currencySymbol${model.gomTotalAmount.roundOff()}".apply {
                binding.tvPrice.text = this
            }
            "$currencySymbol${((model.mrp ?: 0.0).times(model.quantity?:0)).roundOff()}".apply {
                binding.tvMRP.text = this
            }
            binding.tvMRP.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

            binding.clItemBaseISSC.setOnClickListener {
                it.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }

//            binding.clItemBaseISSC.alpha = 0.5f

        }

    }

    fun getAdapterList(): List<SSItemModel> {
        return dataList
    }

    fun addItemToAdapterList(item: SSItemModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

    inner class ProductDiffUtil(private val oldList:List<SSItemModel>,
        private val newList: List<SSItemModel>) : DiffUtil.Callback() {
        override fun getOldListSize() = oldList.size
        override fun getNewListSize() = newList.size
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].productId == newList[newItemPosition].productId
        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition] == newList[newItemPosition]
    }

    fun setNewList(newList: List<SSItemModel>) {
        DiffUtil.calculateDiff(ProductDiffUtil(this.dataList, newList)).dispatchUpdatesTo(this)
        this.dataList.clear()
        this.dataList.addAll(newList)
    }
}
