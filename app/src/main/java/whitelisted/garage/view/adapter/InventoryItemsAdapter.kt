package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_select_inventory.view.*
import whitelisted.garage.R
import whitelisted.garage.api.request.RackItem

class InventoryItemsAdapter(var dataList: MutableList<RackItem>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SelectInventoryViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_select_inventory, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SelectInventoryViewHolder).bind()
    }

    inner class SelectInventoryViewHolder(view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        fun bind() {

            val model = dataList[layoutPosition]
            itemView.tvItemName.text = model.partName
            itemView.tvQuantity.text = model.quantity.toString()
            if (!model.brandName.isNullOrEmpty() && model.brandName != "null") itemView.tvBrand.text =
                model.brandName ?: itemView.tvBrand.context.getString(R.string.generic)
            itemView.tvRackName.text = model.rackName?.trim()
            itemView.imgPlus.setOnClickListener {
                if (itemView.tvItemCount.text.toString().toInt() < (model.quantity ?: 0)) {
                    val selectedCount = itemView.tvItemCount.text.toString().toInt() + 1
                    itemView.tvItemCount.text = selectedCount.toString()
                    model.selectedQty = selectedCount
                }
            }
            itemView.imgMinus.setOnClickListener {
                if (itemView.tvItemCount.text.toString().toInt() > 0) {
                    val selectedCount = itemView.tvItemCount.text.toString().toInt() - 1
                    itemView.tvItemCount.text = selectedCount.toString()
                    model.selectedQty = selectedCount
                } else { // nothig
                }
            }
        }

        override fun onClick(v: View?) {
            if (dataList.size <= layoutPosition) {
                return
            }
            recyclerItemClickListener.onClick(v)
        }
    }

    fun getAdapterList(): List<RackItem> {
        return dataList
    }

    fun addItemToAdapterList(item: RackItem) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}