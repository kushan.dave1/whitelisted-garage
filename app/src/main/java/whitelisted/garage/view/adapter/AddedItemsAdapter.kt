package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_added_item.view.*
import whitelisted.garage.R
import whitelisted.garage.api.request.ServicesItem
import whitelisted.garage.utils.CommonUtils

class AddedItemsAdapter(val currencySymbol: String,
    var dataList: MutableList<ServicesItem>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AddedItemsItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_added_item, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AddedItemsItemViewHolder).bind()
    }

    inner class AddedItemsItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind() {

            val model = dataList[layoutPosition]
            itemView.tvPartName.text = model.serviceName
            itemView.tvWorkDone.text = model.workDone
            "$currencySymbol${CommonUtils.convertDoubleTo2DecimalPlaces(model.pricePerItem ?: 0.0)}".apply {
                itemView.tvPricePerItem.text = this
            }
            itemView.tvQuantity.text = model.quantity.toString()
            itemView.tvTaxRate.text = model.taxRate
            itemView.tvTotal.text =
                "$currencySymbol${CommonUtils.convertDoubleTo2DecimalPlaces(model.total ?: 0.0)}"
            itemView.imgEdit.setOnClickListener {
                itemView.imgEdit.setTag(R.id.model, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            itemView.imgDel.setOnClickListener {
                itemView.imgDel.setTag(R.id.model, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<ServicesItem> {
        return dataList
    }

    fun addItemToAdapterList(item: ServicesItem) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}