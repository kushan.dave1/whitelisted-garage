package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.libraries.places.api.model.AutocompletePrediction
import kotlinx.android.synthetic.main.item_address_result.view.*
import whitelisted.garage.R

@SuppressLint("NotifyDataSetChanged")
class GooglePlacesAdapter(val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var dataList: List<AutocompletePrediction>? = emptyList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyPlacesViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_address_result, parent, false))
    }


    override fun getItemCount(): Int = dataList?.size ?: 0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MyPlacesViewHolder).bind()
    }

    inner class MyPlacesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataList?.get(layoutPosition)
            itemView.cityTv.text = model?.getPrimaryText(null)
            itemView.addressTv.text = model?.getFullText(null)
            itemView.rvParent.setOnClickListener {
                itemView.rvParent.setTag(R.id.model, dataList?.get(layoutPosition))
                itemView.rvParent.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun setData(list: List<AutocompletePrediction>) {
        dataList = list
        notifyDataSetChanged()

    }

    fun clearData() {
        this.dataList = emptyList()
        notifyDataSetChanged()
    }

}