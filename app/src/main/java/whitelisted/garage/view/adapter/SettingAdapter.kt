package whitelisted.garage.view.adapter

import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_setting.view.*
import whitelisted.garage.R
import whitelisted.garage.databinding.ItemSettingBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.ImageLoader

class SettingAdapter(currencySymbol: String, val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val images = mutableListOf(
        R.drawable.ic_my_profile,
        R.drawable.ic_language,
        R.drawable.ic_qr,
        R.drawable.ic_help_and_support,
        R.drawable.ic_download_latest,
        R.drawable.ic_feedback,
        R.drawable.ic_share_your_,
        R.drawable.ic_start_demo,
//        R.drawable.ic_manage_employee
    )

    private val imagesBC = mutableListOf(
        R.color.ic_my_profile,
        R.color.ic_language,
        R.color.ic_qr,
        R.color.ic_help_and_support,
        R.color.ic_download_latest,
        R.color.ic_feedback,
        R.color.ic_share_your_,
        R.color.ic_start_demo,
//        R.color.ic_language
    )

    private val header = mutableListOf(
        R.string.my_profile,
        R.string.language,
        R.string.qr_code,
        R.string.help_and_nsupport,
        R.string.download_latest,
        R.string.feedback,
        R.string.share_easy_garage_app,
        R.string.start_demo,
//        R.string.manage_employees
        )
    private val desc = mutableListOf(
        R.string.manage_profile_details,
        R.string.use_your_personalised_language_suitable_to_you,
        R.string.receive_paymets_by_qr_code,
        R.string.contact_us_for_any_queries,
        R.string.download_the_latest_version_of_the_app,
        R.string.submit_your_feedback,
        R.string.share_link_with_other_owners_to_invite_them,
        R.string.start_the_demo_to_get_a_better_understanding_of_the_app,
//        R.string.manage_all_employees
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SettingViewHolder(ItemSettingBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    init {
        if (currencySymbol != AppENUM.RefactoredStrings.defaultCurrency) {
            images.removeAt(1)
            imagesBC.removeAt(1)
            header.removeAt(1)
            desc.removeAt(1)
            images.removeAt(1)
            imagesBC.removeAt(1)
            header.removeAt(1)
            desc.removeAt(1)
        }
    }

    override fun getItemCount(): Int = header.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SettingViewHolder).bind()
    }

    inner class SettingViewHolder(var view: ItemSettingBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind() {
            val drawable = CommonUtils.getDrawable(itemView.clBaseIS.context,
                R.drawable.bg_setting_item) as GradientDrawable
            drawable.setColor(CommonUtils.getColor(view.clBaseIS.context, imagesBC[layoutPosition]))
            drawable.setStroke(2,
                CommonUtils.getColor(itemView.clBaseIS.context, R.color.screen_bg))
            view.clBaseIS.background = drawable
            ImageLoader.loadDrawable(CommonUtils.getDrawable(view.imgSettingItem.context,
                images[layoutPosition]), view.imgSettingItem)
            (view.clBaseIS).id = layoutPosition + 20
            view.tvItemName.text =
                CommonUtils.getString(view.tvItemName.context, header[layoutPosition])
            view.tvDescription.text =
                CommonUtils.getString(view.tvDescription.context, desc[layoutPosition])
            view.settingItemParent.setOnClickListener {
                view.settingItemParent.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }

        }

    }
}