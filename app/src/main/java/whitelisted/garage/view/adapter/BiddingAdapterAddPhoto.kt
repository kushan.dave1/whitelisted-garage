package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_bus_image.view.*
import kotlinx.android.synthetic.main.item_car_image.view.llDelImage
import kotlinx.android.synthetic.main.item_car_image.view.rlAddImage
import whitelisted.garage.R
import whitelisted.garage.api.request.BiddinngPhotoModel
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class BiddingAdapterAddPhoto(
    var dataList: MutableList<BiddinngPhotoModel>?,
    val recyclerItemClickListener: View.OnClickListener,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PhotoViewHolderForBidding(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_add_image_bid, parent, false))
    }

    override fun getItemCount(): Int = dataList?.size ?: 0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PhotoViewHolderForBidding).bind()
    }

    inner class PhotoViewHolderForBidding(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList?.get(layoutPosition)
            if (model?.uri == Uri.EMPTY) {
                itemView.rlBusImage.makeVisible(false)
                itemView.rlAddImage.makeVisible(true)
            } else {
                itemView.rlBusImage.makeVisible(true)
                itemView.rlAddImage.makeVisible(false)
                ImageLoader.loadImage(itemView.imgBusImage, model?.uri)
            }
            itemView.llDelImage.setOnClickListener {
                itemView.llDelImage.setTag(R.id.position, layoutPosition)
                itemView.llDelImage.setTag(R.id.model, dataList?.get(layoutPosition))
                recyclerItemClickListener.onClick(it)
            }
            itemView.rlAddImage.setOnClickListener {
                itemView.rlAddImage.setTag(R.id.position, layoutPosition)
                itemView.rlAddImage.setTag(R.id.model, dataList?.get(layoutPosition))
                recyclerItemClickListener.onClick(it)
            }
            itemView.imgBusImage.setOnClickListener {
                itemView.imgBusImage.setTag(R.id.position, layoutPosition)
                itemView.imgBusImage.setTag(R.id.model, dataList?.get(layoutPosition))
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): MutableList<BiddinngPhotoModel> {
        return dataList ?: mutableListOf()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: MutableList<BiddinngPhotoModel>) {
        dataList = list
        notifyDataSetChanged()
    }

    fun addItemToAdapterList(item: BiddinngPhotoModel) {
        dataList?.add(item)
        dataList?.size?.let { notifyItemInserted(it) }
    }

    fun removeItemFromList(position: Int) {
        dataList?.removeAt(position)
        notifyItemRemoved(position)
    }


}