package whitelisted.garage.view.adapter

import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_service_type.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.WorkDoneResponseModel

class WorkDoneAdapter(
    var dataList: MutableList<WorkDoneResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ServiceTypeItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_service_type, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ServiceTypeItemViewHolder).bind()
    }

    inner class ServiceTypeItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList[layoutPosition]
            itemView.cbWorkDone.text = model.name
            itemView.cbWorkDone.isChecked = model.isSelected
            itemView.cbWorkDone.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    var i = 0
                    dataList.forEach {
                        if (it.isSelected) {
                            it.isSelected = false
                        }
                        i++
                    }
                    model.isSelected = true
                    Handler(Looper.getMainLooper()).postDelayed({
                        notifyItemRangeChanged(0, dataList.size)
                    }, 200)
                    itemView.cbWorkDone.setOnClickListener {
                        itemView.cbWorkDone.setTag(R.id.position, layoutPosition)
                        recyclerItemClickListener.onClick(it)
                    }
                } else {
                    itemView.cbWorkDone.setOnClickListener {
                        itemView.cbWorkDone.setTag(R.id.position, layoutPosition)
                        recyclerItemClickListener.onClick(it)
                    }
                }
            }
        }
    }

}