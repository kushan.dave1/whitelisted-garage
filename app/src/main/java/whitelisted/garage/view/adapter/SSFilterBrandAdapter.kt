package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.SSBrandsResponseModel
import whitelisted.garage.databinding.ItemFilterBrandBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeBackgroundColor
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class SSFilterBrandAdapter(var dataList: MutableList<SSBrandsResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemFilterBrandViewHolder(ItemFilterBrandBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemFilterBrandViewHolder).bind()
    }

    inner class ItemFilterBrandViewHolder(var binding: ItemFilterBrandBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]
            binding.tvBrandNameIFB.text = model.brandName
            ImageLoader.loadImage(binding.imgBrandImageIFB, model.icon)

            if (model.isSelected) {
                binding.clBrandFilter.changeBackgroundColor(binding.clBrandFilter.context,
                    R.color.light_blue_bg)
//                CommonUtils.getCustomFontTypeface(binding.tvBrandNameIFB.context,
//                    R.font.gilroy_semibold)?.let {
//                    binding.tvBrandNameIFB.typeface = it
//                }
                binding.tvBrandNameIFB.changeTextColor(binding.tvBrandNameIFB.context,
                    R.color.colorAccent)
                binding.viewSelector.makeVisible(true)

            } else {
                binding.clBrandFilter.changeBackgroundColor(binding.clBrandFilter.context,
                    R.color.white)
//                CommonUtils.getCustomFontTypeface(binding.tvBrandNameIFB.context,
//                    R.font.gilroy_medium)?.let {
//                    binding.tvBrandNameIFB.typeface = it
//                }
                binding.tvBrandNameIFB.changeTextColor(binding.tvBrandNameIFB.context,
                    R.color.black_trans_new)
                binding.viewSelector.makeVisible(false)
            }

            binding.clBrandFilter.setOnClickListener {
                it.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<SSBrandsResponseModel> {
        return dataList
    }

    fun addItemToAdapterList(item: SSBrandsResponseModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }
}