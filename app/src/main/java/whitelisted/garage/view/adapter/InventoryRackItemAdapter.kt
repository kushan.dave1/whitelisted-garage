package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.request.RackItem
import whitelisted.garage.databinding.ItemRackBinding
import whitelisted.garage.databinding.ItemRackViewBinding
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class InventoryRackItemAdapter(val currencySymbol: String,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var dataList = mutableListOf<RackItem>()
    override fun getItemCount(): Int = dataList.size

    companion object {
        const val VIEW_TYPE_ONE = 0
        const val VIEW_TYPE_TWO = 1
    }

    var viewType = 0

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: MutableList<RackItem>) {
        dataList.clear()
        dataList.addAll(list)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return when (viewType) {
            0 -> VIEW_TYPE_ONE
            1 -> VIEW_TYPE_TWO
            else -> {
                0
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_TWO) {
            return MyViewHolder1(ItemRackViewBinding.inflate(LayoutInflater.from(parent.context),
                parent,
                false))
        }

        return MyViewHolder(ItemRackBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = dataList[position]
        if (viewType == VIEW_TYPE_TWO) {
            (holder as MyViewHolder1).bind(data)
        } else {
            (holder as MyViewHolder).bind(data)
        }
    }

    inner class MyViewHolder(var binding: ItemRackBinding) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {
        private var qty = 0

        @SuppressLint("SetTextI18n")
        fun bind(data: RackItem) {
            binding.root.clipToOutline = true
            if (!data.images.isNullOrEmpty()) {
                binding.llPhotoLayout.makeVisible(true)
                setDataToImageView(data, binding)
            } else {
                binding.llPhotoLayout.makeVisible(false)
            }

            if((data.quantity?:0) == 0) {
                qty = 0
                binding.tvQuantity1.text = "0"
                binding.tvOutOfStockIR.visibility = View.VISIBLE
                binding.llQuantity.makeVisible(true)
                binding.tvQuantity.makeVisible(false)
            }
            else {

                binding.tvOutOfStockIR.visibility = View.GONE
                binding.llQuantity.visibility = View.INVISIBLE
                binding.tvQuantity.makeVisible(true)
                binding.btnSaveIR.makeVisible(false)
            }

            binding.tvPartName.text = data.partName
            if (!data.brandName.isNullOrEmpty()) {
                binding.llTvBrand.makeVisible(true)
                if (data.brandName != "null") binding.tvBrand.text = data.brandName ?: ""
                else binding.tvBrand.text = binding.tvBrand.context.getString(R.string.generic)
            } else {
                binding.llTvBrand.makeVisible(false)
            }
            binding.tvQuantity.text = (data.quantity ?: 0).toString()
            binding.tvBuyingPrice.text = "$currencySymbol${data.buyingPrice ?: 0}"
            binding.tvSellingPrice.text = "$currencySymbol${data.selling_price ?: 0}"
            binding.imageDeleteRackItem.setOnClickListener {
                binding.imageDeleteRackItem.setTag(R.id.imageDeleteRackItem, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            binding.imageEditRackItem.setOnClickListener {
                binding.imageEditRackItem.setTag(R.id.imageEditRackItem, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            binding.llPhotoLayout.setOnClickListener {
                binding.llPhotoLayout.setTag(R.id.llPhotoLayout, data.images)
                recyclerItemClickListener.onClick(it)
            }

            binding.ivMinusQty.setOnClickListener {
                if(qty<1) return@setOnClickListener
                else binding.tvQuantity1.text = (--qty).toString()
                if(qty == 0){
                    it.alpha = 0.5f
                    it.isClickable = false
                    binding.tvOutOfStockIR.makeVisible(true)
                    binding.btnSaveIR.makeVisible(false)
                }
            }

            binding.ivPlusQty.setOnClickListener {
                binding.ivMinusQty.alpha = 1f
                binding.ivMinusQty.isClickable = true
                binding.tvQuantity1.text = (++qty).toString()
                binding.tvOutOfStockIR.makeVisible(false)
                binding.btnSaveIR.makeVisible(true)
            }

            binding.btnSaveIR.setOnClickListener {
                data.quantity = qty
                it.tag = data
                recyclerItemClickListener.onClick(it)
            }

        }

        override fun onClick(v: View?) {
            v?.let {
                recyclerItemClickListener.onClick(v)
            }
        }

        @SuppressLint("SetTextI18n")
        private fun setDataToImageView(data: RackItem, binding: ItemRackBinding) {
            setImageTo(binding.img1, data.images?.get(0))
            if ((data.images?.size ?: 0) > 1) {
                binding.tvMorePhotoCount.text =
                    "+${(data.images?.size ?: 0) - 1} ${binding.img1.context.getString(R.string.more_photos)}"
            }
        }

        private fun setImageTo(view: AppCompatImageView, string: String?) {
            ImageLoader.loadImage(view, string)
        }

    }

    inner class MyViewHolder1(var binding: ItemRackViewBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {


        @SuppressLint("SetTextI18n")
        fun bind(data: RackItem) {
            if (!data.images.isNullOrEmpty()) {
                binding.llPhotoLayout.makeVisible(true)
                setDataToImageView(data, binding)
            } else {
                binding.llPhotoLayout.makeVisible(false)
            }
            binding.tvPartName.text = data.partName
            if (!data.brandName.isNullOrEmpty()) {
                binding.llTvBrand.makeVisible(true)
                if (data.brandName != "null") binding.tvBrand.text = data.brandName ?: ""
            } else {
                binding.llTvBrand.makeVisible(false)
            }
            binding.tvQuantity.text = (data.quantity ?: 0).toString()
            binding.tvBuyingPrice.text = "$currencySymbol${data.buyingPrice ?: 0}"
            binding.tvSellingPrice.text = "$currencySymbol${data.selling_price ?: 0}"

            binding.llPhotoLayout.setOnClickListener {
                binding.llPhotoLayout.setTag(R.id.llPhotoLayout, data.images)
                recyclerItemClickListener.onClick(it)
            }


        }

        override fun onClick(v: View?) {
            v?.let {
                recyclerItemClickListener.onClick(v)
            }
        }


        private fun setDataToImageView(data: RackItem, binding: ItemRackViewBinding) {
            setImageTo(binding.img1, data.images?.get(0))
            if ((data.images?.size ?: 0) > 1) {
                ("+${(data.images?.size ?: 0) - 1} ${binding.img1.context.getString(R.string.more_photos)}").apply {
                    binding.tvMorePhotoCount.text = this
                }
            }
        }


        private fun setImageTo(view: AppCompatImageView, string: String?) {
            ImageLoader.loadImage(view, string)
        }
    }

    fun getAdapterData(): MutableList<RackItem> {
        return dataList
    }


}

