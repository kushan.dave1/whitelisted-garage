package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_select_car.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.TypeItem
import whitelisted.garage.utils.CommonUtils.changeBackgroundDrawable
import whitelisted.garage.utils.ImageLoader

class SelectFuelTypeAdapter(val isReg: Boolean,
    var dataList: List<TypeItem>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var selectedPos = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SelectCarItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_select_car, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SelectCarItemViewHolder).bind()
    }

    inner class SelectCarItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("NotifyDataSetChanged")
        fun bind() {
            val model = dataList[layoutPosition]
            ImageLoader.loadImage(itemView.imgBrandModelFuel, model.image)
            itemView.tvBrandModelFuel.text = model.type
            if (model.isSelected) {
                itemView.clBaseBuyGC.changeBackgroundDrawable(itemView.clBaseBuyGC.context,
                    R.drawable.bg_white_4_stroke_accent)
            } else itemView.clBaseBuyGC.changeBackgroundDrawable(itemView.clBaseBuyGC.context,
                R.drawable.bg_transparent)
            itemView.clBaseBuyGC.setOnClickListener {
                if (isReg) {
                    dataList.forEach { dataItem ->
                        dataItem.isSelected = false
                    }
                    model.isSelected = true
                    selectedPos = layoutPosition
                    notifyDataSetChanged()
                } else {
                    itemView.clBaseBuyGC.setTag(R.id.position, layoutPosition)
                    itemView.clBaseBuyGC.setTag(R.id.extra, 2)
                    recyclerItemClickListener.onClick(it)
                }
            }
        }
    }

    fun getAdapterList(): List<TypeItem> {
        return dataList
    }


}