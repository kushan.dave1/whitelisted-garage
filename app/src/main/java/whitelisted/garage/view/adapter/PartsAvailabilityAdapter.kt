package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_text_view_2.view.*
import whitelisted.garage.R

class PartsAvailabilityAdapter(var dataList: MutableList<String>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_text_view_2, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MyViewHolder).bind()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataList[layoutPosition]
            itemView.tvItem.text = model
            itemView.tvItem.setOnClickListener {
                it.setTag(R.id.tvItem, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<String> {
        return dataList
    }

}