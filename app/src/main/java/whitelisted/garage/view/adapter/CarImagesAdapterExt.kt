package whitelisted.garage.view.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_car_image.view.*
import whitelisted.garage.R
import whitelisted.garage.utils.CommonUtils.makeInVisible
import whitelisted.garage.utils.ImageLoader

class CarImagesAdapterExt(var dataList: MutableList<Uri>,
    val isExterior: Boolean,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CarImageItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_car_image, parent, false))
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CarImageItemViewHolder).bind()
    }

    inner class CarImageItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataList[layoutPosition]
            if (layoutPosition == dataList.size - 1 && dataList.size != 20) {
                itemView.rlCarImage.makeInVisible(true)
                itemView.rlAddImage.makeInVisible(false)
            } else {
                itemView.rlCarImage.makeInVisible(false)
                itemView.rlAddImage.makeInVisible(true)
                ImageLoader.loadImage(itemView.imgCarImage, model)
            }
            itemView.llDelImage.setOnClickListener {
                itemView.llDelImage.setTag(R.id.position, layoutPosition)
                itemView.llDelImage.setTag(R.id.model, isExterior)
                recyclerItemClickListener.onClick(it)
            }
            itemView.rlAddImage.setOnClickListener {
                itemView.rlAddImage.setTag(R.id.position, layoutPosition)
                itemView.rlAddImage.setTag(R.id.model, isExterior)
                recyclerItemClickListener.onClick(it)
            }
            itemView.imgCarImage.setOnClickListener {
                itemView.imgCarImage.setTag(R.id.position, layoutPosition)
                itemView.imgCarImage.setTag(R.id.model, dataList[layoutPosition])
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): MutableList<Uri> {
        return dataList
    }

    fun addItemToAdapterList(item: Uri) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}