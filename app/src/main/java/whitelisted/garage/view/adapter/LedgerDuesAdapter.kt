package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.Ledger
import whitelisted.garage.databinding.ItemLedgerDueBinding
import whitelisted.garage.utils.CommonUtils

class LedgerDuesAdapter(val currencySymbol: String,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<LedgerDuesAdapter.MyViewHolder>() {
    private var dataList = mutableListOf<Ledger>()

    override fun getItemCount(): Int = dataList.size

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: MutableList<Ledger>) {
        dataList.clear()
        dataList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = ItemLedgerDueBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = dataList[position]
        holder.bind(data)

    }

    inner class MyViewHolder(var binding: ItemLedgerDueBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        fun bind(data: Ledger) {
            binding.tvWorkshopName.text = data.customerDetails?.name
            binding.tvDateTime.text = CommonUtils.convertDateWithDaySlashes(data.createdAt)
            binding.tvOrderId.text = data.orderId
            val text = "$currencySymbol${data.paymentInfo?.billAmount.toString()}"
            binding.tvTotalPayment.text = text
            val text2 = "$currencySymbol${data.amount.toString()}"
            binding.tvTotalDue.text = text2
            binding.llSendReminder.setOnClickListener {
                binding.llSendReminder.setTag(R.id.llSendReminder, data)
                recyclerItemClickListener.onClick(binding.llSendReminder)
            }
            binding.llCollectDue.setOnClickListener {
                binding.llCollectDue.setTag(R.id.llCollectDue, data)
                recyclerItemClickListener.onClick(binding.llCollectDue)
            }
            binding.imgCall.setOnClickListener {
                binding.imgCall.setTag(R.id.imgCall, data)
                recyclerItemClickListener.onClick(binding.imgCall)
            }
            binding.imgWhatsapp.setOnClickListener {
                binding.imgWhatsapp.setTag(R.id.imgWhatsapp, data)
                recyclerItemClickListener.onClick(binding.imgWhatsapp)
            }
            binding.clBase.setOnClickListener {
                binding.clBase.setTag(R.id.clBase, data)
                recyclerItemClickListener.onClick(binding.clBase)
            }
        }

        override fun onClick(v: View?) {
            v?.let {
                recyclerItemClickListener.onClick(v)
            }
        }
    }


    fun getAdapterData(): MutableList<Ledger> {
        return dataList
    }
}

