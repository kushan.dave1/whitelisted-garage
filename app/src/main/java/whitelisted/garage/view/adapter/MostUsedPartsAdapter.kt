package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.MostUsedPartsResponseModel
import whitelisted.garage.databinding.ItemMostUsedPartBinding
import whitelisted.garage.utils.CommonUtils.makeVisible

class MostUsedPartsAdapter(val isAcc: Boolean,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<MostUsedPartsAdapter.MyViewHolder>() {
    private var dataList = mutableListOf<MostUsedPartsResponseModel>()
    override fun getItemCount(): Int = dataList.size


    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: MutableList<MostUsedPartsResponseModel>) {
        dataList.clear()
        dataList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemMostUsedPartBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = dataList[position]
        holder.bind(data, position)

    }

    inner class MyViewHolder(var binding: ItemMostUsedPartBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: MostUsedPartsResponseModel, position: Int) {

            if (isAcc) {
                binding.tvPartName.text = data.categoryName
            } else {
                binding.tvPartName.text = data.name
            }
            binding.tvBrand.text = binding.tvBrand.context.getString(R.string.generic)
            if (!data.rackName.isNullOrEmpty()) {
                binding.llRack.makeVisible(true)
                binding.tvRack.text = data.rackName
                binding.lblQuantity.makeVisible(true)
                binding.tvQuantity.makeVisible(true)
                binding.tvQuantity.text = data.quantity
            } else {
                binding.llRack.makeVisible(false)
                binding.lblQuantity.makeVisible(false)
                binding.tvQuantity.makeVisible(false)
            }
            if (data.isSelected) {
                binding.btnAdd.text = binding.btnAdd.context.getString(R.string.added)
                binding.btnAdd.setCompoundDrawablesRelativeWithIntrinsicBounds(ContextCompat.getDrawable(
                    binding.btnAdd.context,
                    R.drawable.ic_check_accent), null, null, null)
            } else {
                binding.btnAdd.text = binding.btnAdd.context.getString(R.string.add)
                binding.btnAdd.setCompoundDrawablesRelativeWithIntrinsicBounds(ContextCompat.getDrawable(
                    binding.btnAdd.context,
                    R.drawable.ic_add_accent), null, null, null)
            }
            binding.btnAdd.setOnClickListener {
                data.isSelected = !data.isSelected
                notifyItemChanged(position)
            }
        }
    }

    fun getAdapterData(): MutableList<MostUsedPartsResponseModel> {
        return dataList
    }
}
