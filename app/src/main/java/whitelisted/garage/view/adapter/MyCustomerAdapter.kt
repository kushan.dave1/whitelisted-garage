package whitelisted.garage.view.adapter

import DateUtil
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.request.MyCustomerModel
import whitelisted.garage.databinding.ItemMycustomerBinding
import whitelisted.garage.databinding.ItemMycustomerRetailBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader
import kotlin.random.Random

class MyCustomerAdapter(

    var dataList: MutableList<MyCustomerModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var viewType = 0

    var expandedPos = -1
    set(value) {
        val oldExpanded = field
        field = value
        notifyItemChanged(oldExpanded)
        notifyItemChanged(value)
    }

    private val images =
        listOf(R.drawable.ic_dummy_cus1, R.drawable.ic_dummy_cus2, R.drawable.ic_dummy_cus3)

    companion object {
        const val VIEW_TYPE_ONE = 0
        const val VIEW_TYPE_TWO = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == VIEW_TYPE_ONE) return CustomerViewHolder(ItemMycustomerBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        return CustomerRetailViewModel(ItemMycustomerRetailBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }


    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (viewType == VIEW_TYPE_ONE) {
            (holder as CustomerViewHolder).bind(dataList[position])
        } else {
            (holder as CustomerRetailViewModel).bind(dataList[position])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (viewType) {
            0 -> VIEW_TYPE_ONE
            1 -> VIEW_TYPE_TWO
            else -> {
                0
            }
        }
    }

    inner class CustomerViewHolder(var binding: ItemMycustomerBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: MyCustomerModel) = with(binding) {

            btnMore.setTag(R.id.position, layoutPosition)
            btnMore.setOnClickListener(recyclerItemClickListener)

            val numberOfVisits = data.workshopVisits ?: 0
            tvWorkshopVisits.text = numberOfVisits.toString()
            /*if (numberOfVisits == 0) {
                workshopVisitsTv.makeVisible(false)
            }*/
            tvName.text = data.name
            tvPhoneNumber.text = data.mobile
            tvRegNumber.text = data.registration_number
            tvCarMake.text = data.carName
            tvCustomerAddress.text = data.address

            ImageLoader.loadImage(imageView, data.car_icon)
            val date: String? = data.lastVisit?.let {
                DateUtil.getFormattedDateUtcToGmt(AppENUM.DateUtilKey.TIME_FORMAT_NZ,
                    AppENUM.DDMMYYYY,
                    it)

            }

            tvLastVisit.text = date
            tvSMS.setOnClickListener {
                tvSMS.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            tvWhatsApp.setOnClickListener {
                tvWhatsApp.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            tvCall.setOnClickListener {
                tvCall.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            llSeeMore.setOnClickListener {
                expandedPos = if(expandedPos == layoutPosition) -1 else layoutPosition
                it.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }

            if(layoutPosition == expandedPos) {
                if(data.orderDetails != null) clExpandedLayout.makeVisible(true)
                else btnNoDataAvailable.makeVisible(true)
                tvSeeMore.text = root.context.getString(R.string.view_less)
                ivSeeMore.rotation = 180f
            } else {
                clExpandedLayout.makeVisible(false)
                btnNoDataAvailable.makeVisible(false)
                tvSeeMore.text = root.context.getString(R.string.view_more)
                ivSeeMore.rotation = 0f
            }


            //Last Order Details
            val packages = data.orderDetails?.packages
            if(!packages.isNullOrEmpty()) {
                lblPackages.makeVisible(true)
                tvPackage1.makeVisible(true)
                tvPackage1.text = packages[0].packageName


                if(packages.size > 1) {
                    tvPackage2.makeVisible(true)
                    tvPackage2.text = packages[1].packageName
                }
                else tvPackage2.makeVisible(false)

                if(packages.size > 2) {
                    lblPlusMorePackages.makeVisible(true)
                    lblPlusMorePackages.text = "+${packages.size-2} " + root.context.getString(R.string.more_items_sc)
                }
                else lblPlusMorePackages.makeVisible(false)

            }
            else {
                lblPackages.makeVisible(false)
                tvPackage1.makeVisible(false)
                tvPackage2.makeVisible(false)
                lblPlusMorePackages.makeVisible(false)
            }

            val services = data.orderDetails?.services
            if(!services.isNullOrEmpty()) {
                lblServices.makeVisible(true)
                lblWorkDone.makeVisible(true)
                tvService1.makeVisible(true)
                tvServiceWorkDone1.makeVisible(true)
                tvService1.text = services[0].serviceName
                tvServiceWorkDone1.text = services[0].workDoneName

                if(services.size > 1) {
                    tvService2.makeVisible(true)
                    tvServiceWorkDone2.makeVisible(true)
                    tvService2.text = services[1].serviceName
                    tvServiceWorkDone2.text = services[1].workDoneName
                }
                else {
                    tvService2.makeVisible(false)
                    tvServiceWorkDone2.makeVisible(false)
                }

                if(services.size > 2) {
                    lblPlusMoreServices.makeVisible(true)
                    lblPlusMoreServices.text = "+${services.size-2} " + root.context.getString(R.string.more_items_sc)
                } else lblPlusMoreServices.makeVisible(false)

            }
            else {
                lblServices.makeVisible(false)
                tvService1.makeVisible(false)
                tvService2.makeVisible(false)
                lblWorkDone.makeVisible(false)
                tvServiceWorkDone1.makeVisible(false)
                tvServiceWorkDone2.makeVisible(false)
                lblPlusMoreServices.makeVisible(false)

            }

            divider2.makeVisible(!(packages.isNullOrEmpty() || services.isNullOrEmpty()))

            btnDownloadLOD.setOnClickListener {
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse("${BuildConfig.BASE_URL}orders/bill-pdf/?order_id=${data.orderId}&is_pdf=1&bill_flag=2&workshop_type=workshop&bill_type=Tax%20Invoice")
                root.context.startActivity(i)
            }
        }
    }

    inner class CustomerRetailViewModel(var binding: ItemMycustomerRetailBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: MyCustomerModel)  = with(binding) {
            btnMore.setTag(R.id.position, layoutPosition)
            btnMore.setOnClickListener(recyclerItemClickListener)

            tvRetailerName.text = data.name
            tvName.text = data.bill_name
            val random = Random.nextInt(images.size)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(imgCus.context,
                images[random]), imgCus)
            if (data.dueAmount.toString().isNotEmpty() && data.dueAmount != null) {
                llTvDueAmount.makeVisible(true)
                tvDueAmount.makeVisible(true)
                tvDueAmount.text = data.dueAmount.toString()
            } else {
                llTvDueAmount.makeVisible(false)
                tvDueAmount.makeVisible(false)
            }
            if (data.lifetime_amount.toString().isNotEmpty() && data.lifetime_amount != null) {
                llTvLifetimeAmount.makeVisible(true)
                tvLifetimeAmount.makeVisible(true)
                tvLifetimeAmount.text = data.lifetime_amount.toString()
            } else {
                llTvLifetimeAmount.makeVisible(false)
                tvLifetimeAmount.makeVisible(false)
            }
            tvPhNumber.text = data.mobile
            tvCustomerAddress.text = data.address
            val date: String? = data.lastVisit?.let {
                DateUtil.getFormattedDateUtcToGmt(AppENUM.DateUtilKey.TIME_FORMAT_NZ,
                    AppENUM.DDMMYYYY,
                    it)
            }
            if (data.isSave == true) {
                tvStatus.makeVisible(true)
            } else {
                tvStatus.makeVisible(false)
            }
            tvLastVisit.text = date
            tvSMS.setOnClickListener {
                tvSMS.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            tvWhatsApp.setOnClickListener {
                tvWhatsApp.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            tvCall.setOnClickListener {
                tvCall.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }

            btnSeeMore.setOnClickListener {
                expandedPos = if(expandedPos == layoutPosition) -1 else layoutPosition
                it.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }

            if(layoutPosition == expandedPos) {
                if(data.orderDetails != null)
                clExpandedLayout.makeVisible(true)
                else btnNoDataAvailable.makeVisible(true)
                btnSeeMore.setText(root.context.getString(R.string.view_less))
                btnSeeMore.setDrawableRotation(180f)

            } else {
                clExpandedLayout.makeVisible(false)
                btnNoDataAvailable.makeVisible(false)
                btnSeeMore.setText(root.context.getString(R.string.view_more))
                btnSeeMore.setDrawableRotation(0f)
            }
            
            data.orderDetails?.let {
                val brand = it.services[0].brandName
                tvSpare1.text = it.services[0].serviceName ?: ""
                tvSpareBrand1.text = if(!brand.isNullOrEmpty()) brand else binding.root.resources.getString(R.string.generic)

                if(it.services.size > 1) {
                    val brand2 = it.services[1].brandName
                    tvSpare2.makeVisible(true)
                    tvSpareBrand2.makeVisible(true)
                    tvSpare2.text = it.services[1].serviceName ?: ""
                    tvSpareBrand2.text = if(!brand2.isNullOrEmpty()) brand2 else binding.root.resources.getString(R.string.generic)
                } else {
                    tvSpare2.makeVisible(false)
                    tvSpareBrand2.makeVisible(false)
                }

                if(it.services.size > 2) {
                    lblPlusMoreServices.makeVisible(true)
                    lblPlusMoreServices.text = "+${it.services.size-2} " + root.context.getString(R.string.more_items_sc)

                } else lblPlusMoreServices.makeVisible(false)

                btnDownloadLOD.setOnClickListener {
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse("${BuildConfig.BASE_URL}orders/bill-pdf/?order_id=${data.orderId}&is_pdf=1&bill_flag=2&workshop_type=workshop&bill_type=Tax%20Invoice")
                    root.context.startActivity(i)
                }
                
            }

        }
    }
}