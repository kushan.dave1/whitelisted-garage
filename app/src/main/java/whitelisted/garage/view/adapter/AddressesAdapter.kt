package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.AddressResponseModel
import whitelisted.garage.databinding.ItemAddressBinding
import whitelisted.garage.utils.CommonUtils.makeVisible

class AddressesAdapter(var selAddressId: String,
    var dataList: MutableList<AddressResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var currentSelectedPos = -1
    private var lastSelectedPos = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SSBrandItemViewHolder(ItemAddressBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SSBrandItemViewHolder).bind()
    }

    inner class SSBrandItemViewHolder(var binding: ItemAddressBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]
            if (model.address_type?.isNotEmpty() == true) binding.tvAddressType.text =
                model.address_type
            else binding.tvAddressType.text = binding.tvAddressType.context.getString(R.string.home)
            binding.tvAddress.text = model.address

            model.isEditDel?.let {
                if (it) {
                    binding.imgMore.makeVisible(true)
                } else binding.imgMore.makeVisible(false)
            }

            binding.imgMore.setOnClickListener {
                it.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }
            binding.rbAddress.isChecked =
                currentSelectedPos != -1 && currentSelectedPos == layoutPosition

            binding.clBaseIA.setOnClickListener {
                selAddressId = model.addressId ?: ""
                lastSelectedPos = currentSelectedPos
                currentSelectedPos = layoutPosition
                if (lastSelectedPos != -1) notifyItemChanged(lastSelectedPos)
                else { //
                }
                notifyItemChanged(currentSelectedPos)
                it.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            binding.rbAddress.setOnClickListener { binding.clBaseIA.callOnClick() }

            if (selAddressId == model.addressId) {
                binding.rbAddress.isChecked = true
                currentSelectedPos = layoutPosition
            } else binding.rbAddress.isChecked = false
        }
    }

    fun getAdapterList(): List<AddressResponseModel> {
        return dataList
    }

    fun addItemToAdapterList(item: AddressResponseModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }
}
