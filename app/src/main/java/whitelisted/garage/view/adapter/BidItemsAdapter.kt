package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ncorti.slidetoact.SlideToActView
import whitelisted.garage.R
import whitelisted.garage.api.response.BidItem
import whitelisted.garage.databinding.ItemBidItemBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeBackgroundDrawable
import whitelisted.garage.utils.CommonUtils.makeVisible

class BidItemsAdapter(val partsAvailabilityList: MutableList<String>,
    val currencySymbol: String,
    var dataList: MutableList<BidItem>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), View.OnClickListener {

    private var partsAvailabilityPopup: PopupWindow? = null
    private var selectedPopupPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return WSBidHistoryViewHolder(ItemBidItemBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as WSBidHistoryViewHolder).bind()
    }

    inner class WSBidHistoryViewHolder(var binding: ItemBidItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        @SuppressLint("SetTextI18n")
        fun bind() {
            val model = dataList[layoutPosition]
            binding.lblPricePerItem.text =
                "${binding.lblPricePerItem.context.getString(R.string.price_per_item_asterisk)} ($currencySymbol)"
            binding.tvPartName.text = model.partName
            binding.tvItemCount.text = model.quantityAvailable.toString()
            model.mfYear?.let {
                if (it.isNotEmpty()) {
                    binding.tvVariant.text = model.mfYear
                    binding.cgVariant.makeVisible(true)
                } else binding.cgVariant.makeVisible(false)
            }
            binding.tvCarName.text = model.carModel
            model.remarks?.let {
                if (it.trim().isNotEmpty()) {
                    binding.cgRemarks.makeVisible(true)
                    binding.tvRemarks.text = it
                } else {
                    binding.cgRemarks.makeVisible(false)
                }
            }
            if (model.isGeneric == true) {
                binding.cgVariant.makeVisible(false)
            } else {
                binding.cgVariant.makeVisible(true)
            }
            binding.etPricePerItem.setText(model.pricePerItem.toString())
            binding.tvTotalPrice.text =
                currencySymbol + ((model.pricePerItem?.times(model.quantityAvailable
                    ?: 1)).toString())
            binding.viewPlus.setOnClickListener {
                if (binding.tvItemCount.text.toString().toInt() >= (model.quantity ?: 1)) {
                    CommonUtils.showToast(binding.tvItemCount.context,
                        binding.tvItemCount.context.getString(R.string.max_quantity_error))
                } else {
                    if (binding.etPricePerItem.text.toString().isNotEmpty()) {
                        model.quantityAvailable = model.quantityAvailable?.plus(1)
                        binding.tvItemCount.text = model.quantityAvailable.toString()
                        val totalPrice = (model.pricePerItem?.times(model.quantityAvailable ?: 1))
                        binding.tvTotalPrice.text = currencySymbol + totalPrice

                    } else { // do nothing
                    }
                }
            }
            binding.viewMinus.setOnClickListener {
                if (binding.tvItemCount.text.toString().toInt() <= 1) {
                    CommonUtils.showToast(binding.tvItemCount.context,
                        binding.tvItemCount.context.getString(R.string.quantity_can_not_be_zero))
                } else {
                    if (binding.etPricePerItem.text.toString().isNotEmpty()) {
                        model.quantityAvailable = model.quantityAvailable?.minus(1)
                        binding.tvItemCount.text = model.quantityAvailable.toString()
                        val totalPrice = (model.pricePerItem?.times(model.quantityAvailable ?: 1))
                        binding.tvTotalPrice.text = currencySymbol + totalPrice
                    } else { // do nothing
                    }
                }
            }

            CommonUtils.genericCastOrNull<TextWatcher>(binding.etPricePerItem.getTag(R.id.etPricePerItem))
                ?.let {
                    binding.etPricePerItem.removeTextChangedListener(it)
                }
            binding.viewPrice.setOnClickListener {
                it.setTag(R.id.viewPrice, layoutPosition)
                recyclerItemClickListener.onClick(it)

                if (binding.etPricePerItem.text.toString()
                        .isNotEmpty() && binding.etPricePerItem.text.toString().toDouble() == 0.0) {
                    binding.etPricePerItem.setText("")
                }
                CommonUtils.showKeyboard(binding.etPricePerItem.context, binding.etPricePerItem)
                binding.etPricePerItem.requestFocus()
                binding.etPricePerItem.isCursorVisible = true
                binding.etPricePerItem.setSelection(binding.etPricePerItem.text.toString().length)
                val watcher = object : TextWatcher {
                    override fun beforeTextChanged(p0: CharSequence?,
                        p1: Int,
                        p2: Int,
                        p3: Int) { // do nothing
                    }

                    override fun onTextChanged(p0: CharSequence?,
                        p1: Int,
                        p2: Int,
                        p3: Int) { // do nothing
                    }

                    override fun afterTextChanged(p0: Editable?) {
                        if (p0.toString().isNotEmpty() && p0.toString().trim().toDouble() > 0) {
                            model.pricePerItem = p0.toString().trim().toDouble()
                            val totalPrice =
                                (model.pricePerItem?.times(model.quantityAvailable ?: 1))
                            binding.tvTotalPrice.text = currencySymbol + totalPrice
                        }
                    }
                }
                binding.etPricePerItem.addTextChangedListener(watcher)
                binding.etPricePerItem.setTag(R.id.etPricePerItem, watcher)
            }
            binding.btnAccept.onSlideCompleteListener =
                object : SlideToActView.OnSlideCompleteListener {
                    override fun onSlideComplete(view: SlideToActView) {
                        model.isAvailable = true
                        model.isRejected = false
                        binding.btnReject.resetSlider()
                        binding.btnAccept.makeVisible(false)
                        binding.rlAcceptedButton.makeVisible(true)
                        binding.btnReject.makeVisible(true)
                        binding.rlRejectedButton.makeVisible(false)
                        recyclerItemClickListener.onClick(binding.btnAccept)
                        binding.clIBI.changeBackgroundDrawable(binding.clIBI.context,
                            R.drawable.bg_accepted_card)
                    }
                }

            binding.btnReject.onSlideCompleteListener =
                object : SlideToActView.OnSlideCompleteListener {
                    override fun onSlideComplete(view: SlideToActView) {
                        model.isAvailable = false
                        model.isRejected = true
                        binding.btnAccept.resetSlider()
                        binding.btnAccept.makeVisible(true)
                        binding.rlAcceptedButton.makeVisible(false)
                        binding.btnReject.makeVisible(false)
                        binding.rlRejectedButton.makeVisible(true)
                        recyclerItemClickListener.onClick(binding.btnReject)
                        binding.clIBI.changeBackgroundDrawable(binding.clIBI.context,
                            R.drawable.bg_rejected_card)
                    }
                }

            when {
                model.isAvailable == true -> { //                binding.btnAccept.completeSlider()
                    binding.btnReject.resetSlider()
                    binding.btnAccept.makeVisible(false)
                    binding.rlAcceptedButton.makeVisible(true)
                    binding.btnReject.makeVisible(true)
                    binding.rlRejectedButton.makeVisible(false)
                    binding.clIBI.changeBackgroundDrawable(binding.clIBI.context,
                        R.drawable.bg_accepted_card)
                }
                model.isRejected == true -> {
                    binding.btnAccept.resetSlider() //                binding.btnReject.completeSlider()
                    binding.btnAccept.makeVisible(true)
                    binding.rlAcceptedButton.makeVisible(false)
                    binding.btnReject.makeVisible(false)
                    binding.rlRejectedButton.makeVisible(true)
                    binding.clIBI.changeBackgroundDrawable(binding.clIBI.context,
                        R.drawable.bg_rejected_card)
                }
                else -> { // do nothing
                    binding.clIBI.changeBackgroundDrawable(binding.clIBI.context,
                        R.drawable.bg_white_6_border_gray)
                }
            }

            binding.tvPartsAvailability.setOnClickListener {
                selectedPopupPosition = layoutPosition
                partsAvailabilityPopup = showPartsAvailabilityListPopup(it.context)
                partsAvailabilityPopup?.isOutsideTouchable = true
                partsAvailabilityPopup?.isFocusable = true
                partsAvailabilityPopup?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                partsAvailabilityPopup?.elevation = 10f
                partsAvailabilityPopup?.showAsDropDown(it)
                partsAvailabilityPopup?.update()
            }
            binding.tvPartsAvailability.text = partsAvailabilityList[model.partAvailability ?: 0]
            
            if ((model.images?.size ?: 0) > 0) {
                model.images?.let { images ->
                    binding.cgWorkshopPartPhotos.makeVisible(true)
                    if (model.isImagesExpanded == false) {
                        if (images.size <= 3) {
                            binding.rvWorkshopPartPhotos.adapter =
                                BidItemPartPhotosAdapter(images, false, this)
                            binding.tvMorePhotos.makeVisible(false)
                        } else {
                            binding.rvWorkshopPartPhotos.adapter =
                                BidItemPartPhotosAdapter(images.subList(0, 3), false, this)
                            binding.tvMorePhotos.makeVisible(true)
                            binding.tvMorePhotos.text =
                                "+${images.size.minus(3)} ${binding.tvMorePhotos.context.getString(R.string.more_photos)}"
                        }
                    } else {
                        binding.rvWorkshopPartPhotos.adapter =
                            BidItemPartPhotosAdapter(images, false, this)
                        binding.tvMorePhotos.makeVisible(false)
                    }
                }

            } else {
                binding.cgWorkshopPartPhotos.makeVisible(false)
            }

            binding.tvMorePhotos.setOnClickListener {
                model.isImagesExpanded = true
                notifyItemChanged(layoutPosition)
            }

            if (model.addedImages.isNullOrEmpty()) {
                model.addedImages = mutableListOf("")
                binding.rvPartPhotos.adapter =
                    BidItemPartPhotosAdapter(model.addedImages ?: mutableListOf(), true, this)
            } else {
                if ((model.addedImages?.size ?: 0) > 5) {
                    model.addedImages?.removeAt(0)
                } else { // do nothing
                }
                binding.rvPartPhotos.adapter =
                    BidItemPartPhotosAdapter(model.addedImages ?: mutableListOf(), true, this)
            }


        }

        override fun onClick(v: View?) {
            CommonUtils.genericCastOrNull<Int>(v?.getTag(R.id.rlAddImage))?.let {
                v?.setTag(R.id.rlAddImage, layoutPosition)
                recyclerItemClickListener.onClick(v)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged", "InflateParams")
    private fun showPartsAvailabilityListPopup(context: Context): PopupWindow {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_parts_availability, null)
        val recyclerView = view.findViewById<RecyclerView>(R.id.rvWorkDone)
        val adapter = PartsAvailabilityAdapter(partsAvailabilityList, this)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged() //        view.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        return PopupWindow(view,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            true)
    }

    override fun onClick(v: View?) {
        CommonUtils.genericCastOrNull<Int>(v?.getTag(R.id.tvItem))?.let {
            if (selectedPopupPosition != -1) {
                dataList[selectedPopupPosition].partAvailability = it
                notifyItemChanged(selectedPopupPosition)
            }
        }

        partsAvailabilityPopup.let {
            if (it?.isShowing == true) {
                it.dismiss()
            }
            partsAvailabilityPopup = null
        }
    }


}