package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.request.BrandWiseItem
import whitelisted.garage.databinding.ItemWsEquiryBrandBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader


class WorkBidEnquireBrandListAdapter(var genericItems: List<BrandWiseItem>,
    val recyclerItemClickListener: View.OnClickListener,
    val partsAvailabilityList: MutableList<String>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return WSBidEnquiryBrandViewHolder(ItemWsEquiryBrandBinding.inflate(LayoutInflater.from(
            parent.context), parent, false))
    }

    override fun getItemCount(): Int = genericItems.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as WSBidEnquiryBrandViewHolder).bind()
    }

    inner class WSBidEnquiryBrandViewHolder(var view: ItemWsEquiryBrandBinding) :
        RecyclerView.ViewHolder(view.root) {

        fun bind() {
            val modelData = genericItems[layoutPosition]
            view.tvPartName.text = modelData.partName ?: ""
            view.tvCarName.text = modelData.carModel ?: ""
            view.tvQuantity.text = (modelData.quantity ?: 0).toString()
            view.tvPrice.text = (modelData.price_per_item ?: 0).toString()
            if (partsAvailabilityList.size == 4) {
                when (modelData.part_availability ?: 0) {
                    0 -> {
                        view.tvPartAvl.text = partsAvailabilityList[0]
                    }
                    1 -> {
                        view.tvPartAvl.text = partsAvailabilityList[1]
                    }
                    2 -> {
                        view.tvPartAvl.text = partsAvailabilityList[2]
                    }
                    3 -> {
                        view.tvPartAvl.text = partsAvailabilityList[3]
                    }
                }
            }
            if (modelData.mfYear.isNullOrEmpty()) {
                view.tvVariant.makeVisible(false)
                view.llTvVariant.makeVisible(false)
            } else {
                view.tvVariant.makeVisible(true)
                view.llTvVariant.makeVisible(true)
                view.tvVariant.text = modelData.mfYear
            }
            if (modelData.brandName.isNullOrEmpty()) {
                view.tvBrand.makeVisible(false)
                view.llTvBrand.makeVisible(false)
            } else {
                view.tvBrand.makeVisible(true)
                view.llTvBrand.makeVisible(true)
                view.tvBrand.text = modelData.brandName
            }

            if (modelData.remark.isNullOrEmpty()) {
                view.tvRemark.makeVisible(false)
                view.llTvRemark.makeVisible(false)
            } else {
                view.tvRemark.makeVisible(true)
                view.llTvRemark.makeVisible(true)
                view.tvRemark.text = modelData.remark
            }
            if (modelData.is_available == true) {
                view.llTVBidStatus.text =
                    CommonUtils.getString(view.llTVBidStatus.context, R.string.available)
                view.llTVBidStatus.background =
                    CommonUtils.getDrawable(view.llTVBidStatus.context, R.drawable.bg_ws_new_bid)
                view.llTVBidStatus.setTextColor(CommonUtils.getColor(view.llTVBidStatus.context,
                    R.color.status_completed))
            } else {
                view.llTVBidStatus.text =
                    CommonUtils.getString(view.llTVBidStatus.context, R.string.not_available)
                view.llTVBidStatus.background = CommonUtils.getDrawable(view.llTVBidStatus.context,
                    R.drawable.bg_ws_rejected_bid)
                view.llTVBidStatus.setTextColor(CommonUtils.getColor(view.llTVBidStatus.context,
                    R.color.gom_red))
            }

            if (modelData.images.isNullOrEmpty()) {
                view.llTvPartPhoto.makeVisible(false)
                view.llPhotoContainer.makeVisible(false)
            } else {
                view.llTvPartPhoto.makeVisible(true)
                view.llPhotoContainer.makeVisible(true)
                setImageToView(view, modelData.images)
            }
            view.img1.setOnClickListener {
                view.img1.setTag(R.id.position, 0)
                view.img1.setTag(R.id.model, modelData.images)
                recyclerItemClickListener.onClick(it)
            }
            view.img2.setOnClickListener {
                view.img2.setTag(R.id.position, 1)
                view.img2.setTag(R.id.model, modelData.images)
                recyclerItemClickListener.onClick(it)
            }
            view.img3.setOnClickListener {
                view.img3.setTag(R.id.position, 2)
                view.img3.setTag(R.id.model, modelData.images)
                recyclerItemClickListener.onClick(it)
            }
            view.img4.setOnClickListener {
                view.img4.setTag(R.id.position, 3)
                view.img4.setTag(R.id.model, modelData.images)
                recyclerItemClickListener.onClick(it)
            }
            view.tvMorePhotos.setOnClickListener {
                view.tvMorePhotos.setTag(R.id.position, layoutPosition)
                view.tvMorePhotos.setTag(R.id.model, modelData.images)
                recyclerItemClickListener.onClick(it)
            }
        }

        @SuppressLint("SetTextI18n")
        private fun setImageToView(view: ItemWsEquiryBrandBinding, images: List<String>?) {
            view.tvMorePhotos.makeVisible(false)
            when (images?.size) {

                1 -> {
                    ImageLoader.loadImage(view.img1, images[0])
                    view.img1.makeVisible(true)
                    view.img2.makeVisible(false)
                    view.img3.makeVisible(false)
                    view.img4.makeVisible(false)
                }
                2 -> {
                    ImageLoader.loadImage(view.img1, images[0])
                    ImageLoader.loadImage(view.img2, images[1])
                    view.img1.makeVisible(true)
                    view.img2.makeVisible(true)
                    view.img3.makeVisible(false)
                    view.img4.makeVisible(false)
                }
                3 -> {
                    ImageLoader.loadImage(view.img1, images[0])
                    ImageLoader.loadImage(view.img2, images[1])
                    ImageLoader.loadImage(view.img3, images[2])
                    view.img1.makeVisible(true)
                    view.img2.makeVisible(true)
                    view.img3.makeVisible(true)
                    view.img4.makeVisible(false)
                }
                4 -> {
                    ImageLoader.loadImage(view.img1, images[0])
                    ImageLoader.loadImage(view.img2, images[1])
                    ImageLoader.loadImage(view.img3, images[2])
                    ImageLoader.loadImage(view.img4, images[3])
                    view.img1.makeVisible(true)
                    view.img2.makeVisible(true)
                    view.img3.makeVisible(true)
                    view.img4.makeVisible(true)
                }
                else -> {
                    ImageLoader.loadImage(view.img1, images?.get(0) ?: "")
                    ImageLoader.loadImage(view.img2, images?.get(1))
                    ImageLoader.loadImage(view.img3, images?.get(2))
                    ImageLoader.loadImage(view.img4, images?.get(3))
                    view.img1.makeVisible(true)
                    view.img2.makeVisible(true)
                    view.img3.makeVisible(true)
                    view.img4.makeVisible(true)
                    view.tvMorePhotos.makeVisible(true)
                    view.tvMorePhotos.text =
                        "+${((images?.size ?: 0) - 4)} ${view.tvMorePhotos.context.getString(R.string.more_photos)}"
                }
            }
        }
    }

}