package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_rack_item_image.view.*
import whitelisted.garage.R
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class RackItemPartImageAdapter(
    val recyclerItemClickListener: View.OnClickListener,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var dataList: MutableList<Uri>? = null
    var isViewOnly = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PhotoViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_rack_item_image, parent, false))
    }

    override fun getItemCount(): Int = dataList?.size ?: 0
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PhotoViewHolder).bind()
    }

    inner class PhotoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList?.get(layoutPosition)
            if (model == Uri.EMPTY) {
                itemView.rlBusImage.makeVisible(false)
                itemView.rlAddImage.makeVisible(true)
            } else {
                itemView.rlBusImage.makeVisible(true)
                itemView.rlAddImage.makeVisible(false)
                ImageLoader.loadImage(itemView.imgBusImage, model)
            }
            if (isViewOnly) {
                itemView.llDelImage.makeVisible(false)
            } else {
                itemView.llDelImage.makeVisible(true)
            }

            itemView.llDelImage.setOnClickListener {
                itemView.llDelImage.setTag(R.id.llDelImage, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            if (isViewOnly) {
                itemView.imgBusImage.setOnClickListener {
                    itemView.imgBusImage.setTag(R.id.imgBusImage,
                        dataList?.get(layoutPosition) ?: "")
                    recyclerItemClickListener.onClick(it)
                }
            }
            itemView.rlAddImage.setOnClickListener {
                recyclerItemClickListener.onClick(it)
            }
        }


    }

    fun getAdapterList(): MutableList<String> {
        val dataToReturn = mutableListOf<String>()
        for (value in dataList ?: mutableListOf()) {
            if (value != Uri.EMPTY) {
                dataToReturn.add(value.toString())
            }
        }
        return dataToReturn
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: MutableList<Uri>) {
        dataList = list
        notifyDataSetChanged()

    }

    fun getDataSetSize(): Int {
        return dataList?.size ?: 0
    }

    fun addItemToAdapterList(item: Uri) {
        dataList?.add(item)
        dataList?.size?.let { notifyItemInserted(it) }
    }


}