package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_added_inclusions.view.*
import whitelisted.garage.R
import whitelisted.garage.api.request.OrderInclusionsItem
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible

@SuppressLint("SetTextI18n")
class AddedInclusionsAdapter(val currencySymbol: String,
    var dataList: MutableList<OrderInclusionsItem>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_added_inclusions, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MyViewHolder).bind()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind() {
            val model = dataList[layoutPosition]
            if (model.type == "part") {
                itemView.tvInventoryTag.makeVisible(false)
            } else itemView.tvInventoryTag.makeVisible(true)
            itemView.tvPartName.text = model.serviceName
            itemView.tvPricePerItem.text = "$currencySymbol${
                CommonUtils.convertDoubleTo2DecimalPlaces(model.pricePerQuantity ?: 0.0)
            }"
            itemView.tvQuantity.text = model.quantity.toString()
            itemView.tvTotalPrice.text = "$currencySymbol${
                CommonUtils.convertDoubleTo2DecimalPlaces(model.total ?: 0.0)
            }"
            itemView.imgPlus.setOnClickListener {
                if ((model.pricePerQuantity ?: 0.0) > 0) {
                    val qty = itemView.tvQuantity.text.toString().toInt() + 1
                    val perWithTaxAmount = model.total?.div(model.quantity ?: 0)
                    itemView.tvQuantity.text = qty.toString()
                    model.quantity = qty
                    model.total = model.total?.plus(perWithTaxAmount ?: 0.0)
                    itemView.tvTotalPrice.text = "$currencySymbol${
                        CommonUtils.convertDoubleTo2DecimalPlaces(model.total ?: 0.0)
                    }"
                    itemView.rlPlusMinus.callOnClick()
                } else {
                    val qty = itemView.tvQuantity.text.toString().toInt() + 1
                    itemView.tvQuantity.text = qty.toString()
                    itemView.rlPlusMinus.callOnClick()
                }
            }
            itemView.imgMinus.setOnClickListener {
                val perWithTaxAmount = model.total?.div(model.quantity ?: 0)
                if (itemView.tvQuantity.text.toString().toInt() > 1) {
                    val qty = itemView.tvQuantity.text.toString().toInt() - 1
                    itemView.tvQuantity.text = qty.toString()
                    model.quantity = qty
                    model.total = model.total?.minus(perWithTaxAmount ?: 0.0)
                    itemView.tvTotalPrice.text = "$currencySymbol${
                        CommonUtils.convertDoubleTo2DecimalPlaces(model.total ?: 0.0)
                    }"
                    itemView.rlPlusMinus.callOnClick()
                }
            }
            itemView.imgEdit.setOnClickListener {
                itemView.imgEdit.setTag(R.id.model, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            itemView.imgDel.setOnClickListener {
                itemView.imgDel.setTag(R.id.model, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }

            itemView.rlPlusMinus.setOnClickListener {
                itemView.rlPlusMinus.setTag(R.id.model, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }

            if (layoutPosition == dataList.size - 1) {
                itemView.viewDivider.makeVisible(false)
            } else itemView.viewDivider.makeVisible(true)
        }
    }

    fun getAdapterList(): List<OrderInclusionsItem> {
        return dataList
    }

    fun addItemToAdapterList(item: OrderInclusionsItem) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}