package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_language.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.LanguageData
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class LanguageAdapter(val recyclerItemClickListener: View.OnClickListener,
    var enableList: MutableList<Boolean>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var string = mutableListOf<String>()
    private var languageStrings = mutableListOf<String>()
    private val images = listOf(
        R.drawable.ic_lang_hindi,
        R.drawable.ic_lang_english,
        R.drawable.ic_lang_gujrati,
        R.drawable.ic_lang_telugu, //  R.drawable.ic_lang_kannada,
        // R.drawable.ic_lang_tamil, //  R.drawable.ic_lang_hinglish,
        //        R.drawable.ic_lang_marathi,
        //   R.drawable.ic_lang_bengali,
    )

    init {


        string.add(AppENUM.LanguageConstants.HINDI) //hindi
        string.add(AppENUM.LanguageConstants.ENGLISH) // english
        // string.add(AppENUM.LanguageConstants.HINGLISH)//hinglish
        //        string.add(AppENUM.LanguageConstants.MARATHI)// marathi

        string.add(AppENUM.LanguageConstants.GUJARAT) // gujarati
        //  string.add(AppENUM.LanguageConstants.BANGLA)// bengali
        string.add(AppENUM.LanguageConstants.TELEAGU) // telugu
        //   string.add(AppENUM.LanguageConstants.KANNADA) // kannada
        //  string.add(AppENUM.LanguageConstants.TAMIL) // tamil

        languageStrings.add(AppENUM.LanguageConstants.WORD_HINDI) //hindi
        languageStrings.add(AppENUM.LanguageConstants.WORD_ENGLISH) // english
        //  languageStrings.add(AppENUM.LanguageConstants.WORD_HINGLISH)//hinglish
        //        languageStrings.add(AppENUM.LanguageConstants.WORD_MARATHI)// marathi
        languageStrings.add(AppENUM.LanguageConstants.WORD_GUJARAT) // gujarati
        // languageStrings.add(AppENUM.LanguageConstants.WORD_BANGLA)// bengali
        languageStrings.add(AppENUM.LanguageConstants.WORD_TELEAGU) // telugu
        // languageStrings.add(AppENUM.LanguageConstants.WORD_KANNADA) // kannada
        // languageStrings.add(AppENUM.LanguageConstants.WORD_TAMIL)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AccountItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_language, parent, false))
    }

    override fun getItemCount(): Int = string.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AccountItemViewHolder).bind()
    }

    inner class AccountItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            itemView.tvLanguage.text = string[layoutPosition]
            if (languageStrings[layoutPosition].isEmpty()) {
                itemView.tvLanguageInEng.makeVisible(false)
            } else {
                itemView.tvLanguageInEng.makeVisible(true)
                itemView.tvLanguageInEng.text = languageStrings[layoutPosition]
            }
            ImageLoader.loadDrawable(ContextCompat.getDrawable(itemView.imageLanguage.context,
                images[layoutPosition]), itemView.imageLanguage)
            if (enableList[layoutPosition]) {
                itemView.parenLayout.background =
                    ContextCompat.getDrawable(itemView.context, R.drawable.bg_active_language)
            } else {
                itemView.parenLayout.background =
                    ContextCompat.getDrawable(itemView.context, R.drawable.bg_in_active_language)
            }
            if (!enableList[layoutPosition]) {
                itemView.parenLayout.setOnClickListener {
                    itemView.parenLayout.setTag(R.id.parenLayout,
                        LanguageData(layoutPosition,
                            string[layoutPosition],
                            enableList[layoutPosition]))
                    recyclerItemClickListener.onClick(it)
                }
            }
        }
    }


}