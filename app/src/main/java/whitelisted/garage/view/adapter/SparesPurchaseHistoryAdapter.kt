package whitelisted.garage.view.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.response.SparesOrderHistory
import whitelisted.garage.databinding.CardSparesPurchaseHistoryBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.view.fragments.sparesShop.SparesOrderHistoryFragment.Companion.STATUS_CANCELED
import whitelisted.garage.view.fragments.sparesShop.SparesOrderHistoryFragment.Companion.STATUS_DELIVERED
import whitelisted.garage.view.fragments.sparesShop.SparesOrderHistoryFragment.Companion.STATUS_DISPATCHED
import whitelisted.garage.view.fragments.sparesShop.SparesOrderHistoryFragment.Companion.STATUS_IN_TRANSIT
import whitelisted.garage.view.fragments.sparesShop.SparesOrderHistoryFragment.Companion.STATUS_OUT_FOR_DELIVERY
import whitelisted.garage.view.fragments.sparesShop.SparesOrderHistoryFragment.Companion.STATUS_PENDING
import kotlin.math.roundToInt

class SparesPurchaseHistoryAdapter(private val list: List<SparesOrderHistory>,
    private val symbol: String) : RecyclerView.Adapter<SparesPurchaseHistoryAdapter.Holder>() {

    private lateinit var onItemClickListener: (v: View) -> Unit

    fun setOnItemClickListener(listener: (v: View) -> Unit) {
        this.onItemClickListener = listener
    }

    inner class Holder(private val binding: CardSparesPurchaseHistoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: SparesOrderHistory) {
            binding.tvItemCountCSPH.text = (item.items?.size ?: 0).toString().plus("  ")
                .plus(binding.root.resources.getString(R.string.items))
            binding.tvDateCSPH.text = CommonUtils.formatDateNormal(item.createdAt)
            binding.tvOrderIdCSPH.text = item.orderId
            binding.tvAmountCSPH.text = symbol + (item.total?.minus(item.discount?.roundToInt()?:0))

            when (item.status) {
                STATUS_DELIVERED -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.delivered)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.status_complete)
                }
                STATUS_DISPATCHED -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.dispatched)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.orange)
                }
                STATUS_PENDING -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.pending)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.pending_red)
                }
                STATUS_IN_TRANSIT -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.in_transit)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.in_transit)
                }
                STATUS_OUT_FOR_DELIVERY -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.out_for_delivery)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.in_transit)
                }
                /*STATUS_CANCELED -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.canceled)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.pending_red)
                }*/
                else -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.canceled)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.pending_red)
                }
            }

            binding.root.setOnClickListener {
                it.tag = item
                onItemClickListener(it)
            }

            binding.btnOrderDetailsCSPH.setOnClickListener {
                it.tag = item
                onItemClickListener(it)
            }

            binding.btnDownloadInvoiceCSPH.setOnClickListener {
                if (item.invoices != null && item.invoices.isNotEmpty()) {
                    it.tag = item.invoices
                    onItemClickListener(it)
                } else {
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data =
                        Uri.parse("${BuildConfig.BASE_URL}shop/marketplace-invoice?order_id=${item.clientOrderOid}")
                    binding.root.context.startActivity(i)
                }
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding = CardSparesPurchaseHistoryBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false)
        return Holder(binding)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount() = list.size
}