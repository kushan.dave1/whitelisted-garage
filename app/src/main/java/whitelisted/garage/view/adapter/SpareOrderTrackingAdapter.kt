package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.content.ClipData
import android.content.ClipboardManager
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.res.ResourcesCompat
import whitelisted.garage.R
import whitelisted.garage.api.response.DspTrackingDetails
import whitelisted.garage.api.response.HistoryItem
import whitelisted.garage.api.response.HistoryObject
import whitelisted.garage.databinding.ItemTrackOrderNewBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible

class SpareOrderTrackingAdapter(private val dataList: List<HistoryObject>?)
    : RecyclerView.Adapter<SpareOrderTrackingAdapter.TrackInfoViewHolder>() {

    private val statuses = listOf("Placed", "Dispatched", "Out for Delivery", "Delivered","Cancelled")
    private val keys = listOf("pending", "dispatched", "out_for_delivery", "delivered", "cancelled")
    private val isDelivered = dataList?.find { it.status == keys[3] } != null
    private val isCancelled = dataList?.find { it.status == keys[4] } != null
    var onTrackingIdClicked: ((String) -> Unit)? = null

    var dspTrackingDetails: DspTrackingDetails? = null
        set(value) {
            field = value
            if(value != null) notifyItemChanged(1)
        }

    var trackingId: String? = null
        set(value) {
            field = value
            if(value != null) notifyItemChanged(1)
        }

    var isExpanded = false
        set(value) {
        field = value
        notifyItemChanged(1)
        if(value) notifyItemRangeInserted(2, if(isCancelled) 4 else 3)
        else notifyItemRangeRemoved(2, if(isCancelled) 4 else 3)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackInfoViewHolder {
        val binding = ItemTrackOrderNewBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return TrackInfoViewHolder(binding)

    }

    override fun getItemCount(): Int = if(!isExpanded) 2 else if(isCancelled) 5 else 4

    override fun onBindViewHolder(holder: TrackInfoViewHolder, position: Int) {
        holder.bind(position, position == itemCount-1)
    }

    inner class TrackInfoViewHolder(val binding: ItemTrackOrderNewBinding) : RecyclerView.ViewHolder(binding.root) {
        private val resources = binding.root.resources
        fun bind(pos: Int, isLast: Boolean) {
            binding.tvTrackInfoTitleOTA.text = if(!isExpanded && pos == 1) resources.getString(R.string.delivered) else statuses[pos]

            val status =  dataList?.findLast { it.status == keys[ if(!isExpanded && pos == 1) 3 else pos ] }
            when {
                status != null -> markReached(status.updatedAt)
                isDelivered -> markReached()
                else -> markUnreached()
            }
            when {
                isLast -> {
                    binding.lineBelowTick.visibility = View.GONE
                    binding.bottomSpace.visibility = View.GONE
                }
                else -> {
                    binding.lineBelowTick.visibility = View.VISIBLE
                    binding.bottomSpace.visibility = View.VISIBLE
                }
            }

            if(status?.status == keys[1] && pos == 1 && trackingId != null) {

                binding.groupDispatched.makeVisible(true)
                binding.tvTrackingId.text = trackingId
                binding.copyTrackingId.setOnClickListener {
                    val clip = ClipData.newPlainText("label", trackingId)
                    getSystemService(binding.root.context, ClipboardManager::class.java)?.setPrimaryClip(clip)
                    CommonUtils.showToast(binding.root.context, resources.getString(R.string.copied_to_clipboard), false)
                }

                binding.tvTrackingId.setOnClickListener {
                    onTrackingIdClicked?.invoke(trackingId?:"")
                }

                binding.tvCourierItem.text = status.description
                binding.tvCourierName.text = status.place

                /*dspTrackingDetails?.trackingData?.dspHistory?.findLast { it.status == "in_transit" }?.let {
                    binding.tvCourierItem.text = it.description
                    binding.tvCourierName.text = it.place
                }*/


            } else binding.groupDispatched.makeVisible(false)

        }

        private fun markReached(timeStamp: String? = null) {
            binding.datePlaced.text = if(timeStamp != null) resources.getString(R.string.on)+ " " + CommonUtils.formatDateNormal2(timeStamp) else ""
            binding.ivTrackInfoIconOTA.setImageDrawable(ResourcesCompat.getDrawable(resources,R.drawable.ic_tick_green_rounded,resources.newTheme()))
            binding.lineBelowTick.setBackgroundColor(ResourcesCompat.getColor(resources,R.color.green_color2,resources.newTheme()))
        }

        private fun markUnreached() {
            binding.datePlaced.text = ""
            binding.ivTrackInfoIconOTA.setImageDrawable(ResourcesCompat.getDrawable(resources,R.drawable.ic_circle_white,resources.newTheme()))
            binding.lineBelowTick.setBackgroundColor(ResourcesCompat.getColor(resources,R.color.track_unreach,resources.newTheme()))
        }

    }

}