package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_package.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.AllPackagesResponseModel
import whitelisted.garage.utils.AppENUM

class PackagesAdapter(var dataList: List<AllPackagesResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PackageItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_package, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PackageItemViewHolder).bind()
    }

    inner class PackageItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataList[layoutPosition]
            itemView.tvPackageName.text = model.name
            itemView.tvPackageName.setOnClickListener {
                itemView.tvPackageName.setTag(R.id.model, AppENUM.AdaptersConstant.PACKAGE_SELECTED)
                itemView.tvPackageName.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<AllPackagesResponseModel> {
        return dataList
    }

}