package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_added_part.view.*
import whitelisted.garage.R
import whitelisted.garage.api.request.JobCardItem
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils.makeVisible

class AddedJobCardItemsAdapter(var dataList: MutableList<JobCardItem>,
    val type: String,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AddedPartItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_added_part, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AddedPartItemViewHolder).bind()
    }

    inner class AddedPartItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList[layoutPosition]
            when (type) {
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                    itemView.tvPartName.text = model.serviceName
                    itemView.tvServiceType.text = model.workDoneName
                    itemView.imgDelSelPart.setOnClickListener {
                        itemView.imgDelSelPart.setTag(R.id.model,
                            AppENUM.AdaptersConstant.ADDED_PART)
                        itemView.imgDelSelPart.setTag(R.id.position, layoutPosition)
                        recyclerItemClickListener.onClick(it)
                    }
                    itemView.tvServiceType.makeVisible(true)
                }
                AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                    itemView.tvPartName.text = model.serviceName
                    itemView.imgDelSelPart.setOnClickListener {
                        itemView.imgDelSelPart.setTag(R.id.model,
                            AppENUM.AdaptersConstant.ADDED_PART)
                        itemView.imgDelSelPart.setTag(R.id.position, layoutPosition)
                        recyclerItemClickListener.onClick(it)
                    }
                    itemView.tvServiceType.makeVisible(false)
                }
                AppENUM.RefactoredStrings.RETAILER_CONSTANT -> { // nothing
                }
                else -> {
                    itemView.tvServiceType.makeVisible(true)
                }
            }
        }

    }

    fun getAdapterList(): List<JobCardItem> {
        return dataList
    }

    fun addItemToAdapterList(item: JobCardItem) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}