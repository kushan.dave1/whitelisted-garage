package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_select_car.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.BrandsResponseModel
import whitelisted.garage.utils.ImageLoader

class SelectBrandAdapter(

    var dataList: List<BrandsResponseModel>, val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SelectCarItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_select_car, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SelectCarItemViewHolder).bind()
    }

    inner class SelectCarItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList[layoutPosition]
            ImageLoader.loadImage(itemView.imgBrandModelFuel, model.thumbnail)
            itemView.tvBrandModelFuel.text = model.name
            itemView.clBaseBuyGC.setOnClickListener {
                itemView.clBaseBuyGC.setTag(R.id.position, layoutPosition)
                itemView.clBaseBuyGC.setTag(R.id.extra, 0)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<BrandsResponseModel> {
        return dataList
    }

}