package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.databinding.ItemGmbDetailsLBinding
import whitelisted.garage.utils.ImageLoader


class GMBInfoDetailsAdapter(multiImage: List<String>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var dataList = listOf(multiImage.last()) + multiImage + listOf(multiImage.first())

    inner class MyViewHolderL(var binding: ItemGmbDetailsLBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: String) {
            ImageLoader.loadImage(binding.imgPhone, data,placeholderImage = R.drawable.ic_placeholder_gbc_banner)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolderL(ItemGmbDetailsLBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = dataList[position]
        (holder as MyViewHolderL).bind(data)
    }

    override fun getItemCount(): Int = dataList.size
}

