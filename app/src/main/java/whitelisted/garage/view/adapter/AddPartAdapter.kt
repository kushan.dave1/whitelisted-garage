package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_add_part.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.ServiceResponseModel

class AddPartAdapter(

    var dataList: MutableList<ServiceResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AddPartItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_add_part, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AddPartItemViewHolder).bind()
    }

    inner class AddPartItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList[layoutPosition]
            itemView.tvPartName.text = model.name
            itemView.tvPartDesc.text = model.description ?: ""
            itemView.tvAdd.setOnClickListener {
                itemView.tvAdd.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<ServiceResponseModel> {
        return dataList
    }

    fun addItemToAdapterList(item: ServiceResponseModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}