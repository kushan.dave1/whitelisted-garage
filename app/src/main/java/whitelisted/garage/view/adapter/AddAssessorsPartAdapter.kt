package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_add_part.view.*
import whitelisted.garage.R
import whitelisted.garage.api.data.GetAccessoriesCatalogueResponse
import whitelisted.garage.api.data.RecyclerData

class AddAssessorsPartAdapter(var dataList: MutableList<GetAccessoriesCatalogueResponse>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AddPartItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_add_part_acc, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AddPartItemViewHolder).bind()
    }

    inner class AddPartItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList[layoutPosition]
            itemView.tvPartName.text = model.variants?.name
            itemView.tvAdd.setOnClickListener {
                itemView.tvAdd.setTag(R.id.model,
                    RecyclerData(layoutPosition, dataList[layoutPosition]))
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<GetAccessoriesCatalogueResponse> {
        return dataList
    }

    fun addItemToAdapterList(item: GetAccessoriesCatalogueResponse) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }


}