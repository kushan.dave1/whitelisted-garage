package whitelisted.garage.view.adapter

import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.SSItemModel
import whitelisted.garage.databinding.CartQtyDropdownBinding
import whitelisted.garage.databinding.ItemBundleItemBinding
import whitelisted.garage.databinding.ItemSparesShopCartBinding
import whitelisted.garage.databinding.ItemTextViewBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.checkNull
import whitelisted.garage.utils.CommonUtils.makeInVisible
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.CommonUtils.roundOff
import whitelisted.garage.utils.ImageLoader

class CartItemsAdapter(var etd:String?,
    var cartItems: List<SSItemModel>,
    val currencySymbol: String,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val dataList: MutableList<SSItemModel> = mutableListOf()
    private var popupWindow: PopupWindow? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SSBrandItemViewHolder(ItemSparesShopCartBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SSBrandItemViewHolder).bind()
    }

    inner class SSBrandItemViewHolder(var binding: ItemSparesShopCartBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]
            ImageLoader.loadImage(binding.ivPartImage,
                model.imageUrl,
                placeholderImage = R.drawable.ic_placeholder_square)
            binding.tvPartName.text = model.title
            "${binding.tvPartId.context.getString(R.string.part_no_colon)} ${model.skuCode}".apply {
                binding.tvPartId.text = this
            }
            "$currencySymbol${model.gomTotalAmount.roundOff()}".apply {
                binding.tvPrice.text = this
            }
            "$currencySymbol${((model.mrp ?: 0.0).times(model.quantity ?: 1)).roundOff()}".apply {
                binding.tvMRP.text = this
            }
            binding.tvMRP.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

            binding.imgPlusCart.setOnClickListener {
                if (model.inventory.checkNull() > model.quantity.checkNull()) {
//                    binding.tvQuantity.text =
//                        "${binding.tvQuantity.text.toString().toInt().plus(1)}"
                    model.quantity = binding.tvQuantity.text.toString().toInt().plus(1)
                    it.setTag(R.id.model, model)
                    recyclerItemClickListener.onClick(it)
                } else CommonUtils.showToast(binding.root.context,
                    binding.root.context.resources.getString(R.string.only_items_in_stock,
                        model.inventory))
            }

            binding.imgMinusCart.setOnClickListener {
//                binding.tvQuantity.text = "${binding.tvQuantity.text.toString().toInt().minus(1)}"
                model.quantity = binding.tvQuantity.text.toString().toInt().minus(1)
//                if ((binding.tvQuantity.text.toString().toInt()) == 0) {
//                    dataList.removeAt(layoutPosition)
//                    notifyItemRemoved(layoutPosition)
//                    notifyItemChanged(layoutPosition - 1)
//                } else { //do nothing
//                }
                it.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }
            binding.ivDelete.setOnClickListener {
//                dataList.removeAt(layoutPosition)
//                notifyItemRemoved(layoutPosition)
//                notifyItemChanged(layoutPosition - 1)
                model.quantity = 0
                it.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }
            binding.tvQuantity.text = "${model.quantity}"

            if (layoutPosition == dataList.size - 1) {
                binding.viewDividerISSC.makeInVisible(true)
            } else binding.viewDividerISSC.makeVisible(true)

            binding.viewPDPClick.setOnClickListener {

            }
            binding.clItemBaseISSC.setOnClickListener {
                it.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }
            val bulkQty = model.bulkOrderQuantity ?: 0
            if (bulkQty > 0 && (model.inventory ?: 0) >= bulkQty) {
                val discountText =
                    binding.root.resources.getString(R.string.extra) + " " + model.bulkDiscountRate?.toInt()
                        .toString() + "% " + binding.root.resources.getString(R.string.discount_bulk)

                binding.clBulk.makeVisible(true)
                if ((model.quantity ?: 0) >= (model.bulkOrderQuantity ?: 0)) {
                    binding.bulkGroup.makeVisible(false)
                    binding.tvApplied.makeVisible(true)
                    binding.tvBulkHeading.text = discountText
                } else {
                    binding.tvApplied.makeVisible(false)
                    binding.bulkGroup.makeVisible(true)
                    binding.tvBulkHeading.text =
                        binding.root.resources.getString(R.string.bulk_add_more_items,
                            (model.bulkOrderQuantity ?: 0).minus(model.quantity ?: 0))
                    binding.lblBulDesc.text = discountText
                    binding.bulkIndicator.progress = ((model.quantity?.toDouble()
                        ?.div((model.bulkOrderQuantity ?: 0).toDouble()))?.times(100.0))?.toInt()
                        ?: 0
                }
            } else binding.clBulk.makeVisible(false)

            binding.tvQuantity.setOnClickListener {
                it.setTag(R.id.model, it)
                showQtyDropdown(model, binding.rlPlusMinus)
            }

            if ((model.bundleItems?.size ?: 0) > 0) {
                val bundleList = mutableListOf<SSItemModel>()
                model.bundleItems?.forEach {
                    val ifPresent = cartItems.any { item -> item.productId == it.productId }
                    if (!ifPresent) {
                        if ((it.inventory ?: 0) > 0) bundleList.add(it)
                    }
                }
                if (bundleList.size > 0) {
                    binding.cgBundleItems.makeVisible(true)
                    binding.rvBundleItems.clear()
                    binding.rvBundleItems.addViews(bundleList,
                        ItemBundleItemBinding::inflate) { binding, item, _ ->
                        ImageLoader.loadImage(binding.ivPartImage,
                            item.imageUrl,
                            placeholderImage = R.drawable.ic_placeholder_square)
                        binding.tvPartName.text = item.title
                        "${binding.tvPartId.context.getString(R.string.part_no_colon)} ${item.skuCode}".apply {
                            binding.tvPartId.text = this
                        }
                        "$currencySymbol${item.gomPrice.roundOff()}".apply {
                            binding.tvPrice.text = this
                        }
                        "$currencySymbol${((item.mrp ?: 0.0).times(item.quantity ?: 1)).roundOff()}".apply {
                            binding.tvMRP.text = this
                        }
                        binding.tvMRP.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

                        binding.btnAddIBI.setOnClickListener {
                            item.quantity = 1
                            it.setTag(R.id.model, item)
                            recyclerItemClickListener.onClick(it)
                        }
                    }
                } else {
                    binding.cgBundleItems.makeVisible(false)
                }
            } else binding.cgBundleItems.makeVisible(false)

            binding.ivRemoveBundleItems.setOnClickListener {
                binding.cgBundleItems.makeVisible(false)
            }

            if (etd.isNullOrEmpty()){
                binding.tvETD.makeVisible(false)
            }else{
                binding.tvETD.makeVisible(true)
                binding.tvETD.text = "${binding.tvETD.context.getString(R.string.estimated_delivery)} $etd"
            }
        }

        private fun showQtyDropdown(model: SSItemModel, view: View) {
            val ctx = binding.root.context
            val inventory = model.inventory ?: 1
            val inflater = LayoutInflater.from(ctx)
            val dropdown = CartQtyDropdownBinding.inflate(inflater, null, false)
            dropdown.rvQty.setLayoutManagerAsLinear()
            val quantities = if (inventory <= 5) (1..inventory).toList()
            else (1..if (inventory >= 30) 30 else inventory).toList().filter { it % 5 == 0 }
            dropdown.rvQty.addViews(quantities,
                ItemTextViewBinding::inflate) { itemBinding, qty, pos ->
                itemBinding.tvTextView.text = qty.toString()
                itemBinding.divider.makeVisible(pos != quantities.size - 1)
                itemBinding.tvTextView.setTextColor(ctx.resources.getColor(R.color.black,
                    ctx.resources.newTheme()))
                itemBinding.tvTextView.gravity = Gravity.CENTER
                itemBinding.tvTextView.setOnClickListener {
                    model.quantity = qty
                    it.setTag(R.id.model, model)
                    recyclerItemClickListener.onClick(it)
                    popupWindow?.dismiss()
                }
            }

            if (popupWindow != null) popupWindow?.dismiss()
            popupWindow =
                PopupWindow(dropdown.root, view.width, ViewGroup.LayoutParams.WRAP_CONTENT)
            popupWindow?.isOutsideTouchable = true
            popupWindow?.isFocusable = true
            popupWindow?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            popupWindow?.showAsDropDown(view, 0, 0, Gravity.CENTER)
        }

    }

    fun getAdapterList(): List<SSItemModel> {
        return dataList
    }

    fun addItemToAdapterList(item: SSItemModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

    inner class ProductDiffUtil(private val oldList: List<SSItemModel>,
        private val newList: List<SSItemModel>) : DiffUtil.Callback() {
        override fun getOldListSize() = oldList.size
        override fun getNewListSize() = newList.size
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].productId == newList[newItemPosition].productId

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition] == newList[newItemPosition]
    }

    fun setNewList(estDelivery:String?, newList: List<SSItemModel>) {
        etd= estDelivery
        DiffUtil.calculateDiff(ProductDiffUtil(this.dataList, newList)).dispatchUpdatesTo(this)
        this.dataList.clear()
        cartItems = newList
        this.dataList.addAll(newList)
    }
}
