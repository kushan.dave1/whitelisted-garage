package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_carousel.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.CarouselDataItem
import whitelisted.garage.utils.ImageLoader

class CarouselAdapter(var dataListItemModel: MutableList<CarouselDataItem>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CarouselItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_carousel, parent, false))
    }

    override fun getItemCount(): Int = dataListItemModel.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CarouselItemViewHolder).bind()
    }

    inner class CarouselItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataListItemModel[layoutPosition]
            ImageLoader.loadImage(itemView.imgCarouselImage, model.carouselImage)
            itemView.imgCarouselImage.setOnClickListener {
                itemView.imgCarouselImage.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<CarouselDataItem> {
        return dataListItemModel
    }

    fun addItemToAdapterList(itemModel: CarouselDataItem) {
        dataListItemModel.add(itemModel)
        notifyItemInserted(dataListItemModel.size)
    }

}