package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_specification.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.SpecificationsItem

class ProductSpecificationAdapter(var dataList: List<SpecificationsItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_specification, parent, false)
        return FeatureItemViewHolder(layoutView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as FeatureItemViewHolder).bind()
    }

    inner class FeatureItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            dataList[layoutPosition].let { compatability ->
                itemView.specName.text = compatability.name
                itemView.guideline_one.text = compatability.value

            }
        }
    }
}