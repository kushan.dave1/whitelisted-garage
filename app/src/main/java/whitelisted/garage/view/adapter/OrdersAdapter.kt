package whitelisted.garage.view.adapter

import DateUtil
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_orders.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.OrderListResponseModel
import whitelisted.garage.databinding.ItemOrdersHistoryRetailBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader
import kotlin.random.Random

class OrdersAdapter(val currencySymbol: String,
    var dataList: MutableList<OrderListResponseModel>,
    val showStatus: Boolean,
    val type: String,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    //  var images = context.resources.obtainTypedArray(R.array.cus_dummy)
    var viewType = 0
    private val images =
        listOf(R.drawable.ic_dummy_cus1, R.drawable.ic_dummy_cus2, R.drawable.ic_dummy_cus3)

    // Holds the views for adding it to image and text
    companion object {
        const val VIEW_TYPE_ONE = 0
        const val VIEW_TYPE_TWO = 1
    }

    override fun getItemViewType(position: Int): Int {
        return when (viewType) {
            0 -> VIEW_TYPE_ONE
            1 -> VIEW_TYPE_TWO
            else -> {
                0
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ONE) {
            return OrdersItemViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_orders, parent, false))
        }

        return MyViewHolder1(ItemOrdersHistoryRetailBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


        if (viewType == VIEW_TYPE_ONE) {
            (holder as OrdersItemViewHolder).bind()
        } else {
            (holder as MyViewHolder1).bind(dataList[position])
        }
    }

    inner class OrdersItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind() {

            val model = dataList[layoutPosition]

            ImageLoader.loadImage(itemView.imgCarImage, model.car_icon)

            if (showStatus) {
                itemView.tvStatus.makeVisible(true)
                when (model.statusId) {
                    AppENUM.RefactoredStrings.OPEN_ORDER_STATUS, AppENUM.RefactoredStrings.OPEN_ORDER_STATUS_2 -> {
                        itemView.tvStatus.text =
                            itemView.tvStatus.context.getString(R.string.open_caps)
                        itemView.tvStatus.background =
                            CommonUtils.getDrawable(itemView.tvStatus.context,
                                R.drawable.bg_status_open)
                    }
                    AppENUM.RefactoredStrings.WIP_ORDER_STATUS -> {
                        itemView.tvStatus.text =
                            itemView.tvStatus.context.getString(R.string.wip_caps)
                        itemView.tvStatus.background =
                            CommonUtils.getDrawable(itemView.tvStatus.context,
                                R.drawable.bg_status_wip)
                    }

                    AppENUM.RefactoredStrings.READY_ORDER_STATUS -> {
                        itemView.tvStatus.text =
                            itemView.tvStatus.context.getString(R.string.ready_caps)
                        itemView.tvStatus.background =
                            CommonUtils.getDrawable(itemView.tvStatus.context,
                                R.drawable.bg_status_ready)
                    }
                    AppENUM.RefactoredStrings.COMPLETED_ORDER_STATUS -> {
                        itemView.tvStatus.text =
                            itemView.tvStatus.context.getString(R.string.completed_caps)
                        itemView.tvStatus.background =
                            CommonUtils.getDrawable(itemView.tvStatus.context,
                                R.drawable.bg_status_completed)
                    }
                }
            } else itemView.tvStatus.makeVisible(false)
            itemView.tvCarMake.text = model.carName
            itemView.tvRegNumber.text = model.registrationNumber

            if (model.fuel_type?.isNotEmpty() == true) {
                itemView.tvFuelType.text = model.fuel_type
            } else {
                itemView.tvFuelType.text = "NA"
            }

            itemView.tvName.text = model.name
            itemView.tvPhoneNumber.text = model.mobile
            itemView.tvInvoiceId.text = model.orderId
            if (model.invoiceAmount?.isNotEmpty() == true) itemView.tvTotalAmount.text =
                currencySymbol + CommonUtils.convertDoubleTo2DecimalPlaces(model.invoiceAmount.toDouble())

            when {
                model.payment_status.equals(AppENUM.UNPAID, ignoreCase = true) -> {
                    itemView.tvPaymentStatus.text = model.payment_status
                    itemView.tvPaymentStatus.setTextColor(itemView.tvPaymentStatus.context.getColor(
                        R.color.red_text_color))
                }
                model.payment_status.equals(AppENUM.PAID, ignoreCase = true) -> {
                    itemView.tvPaymentStatus.text = model.payment_status
                    itemView.tvPaymentStatus.setTextColor(itemView.tvPaymentStatus.context.getColor(
                        R.color.green_new))
                }
                else -> {
                    itemView.tvPaymentStatus.text = ""

                }
            }
            itemView.cvBase.setOnClickListener {
                itemView.cvBase.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)

            }
        }
    }

    inner class MyViewHolder1(var binding: ItemOrdersHistoryRetailBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(data: OrderListResponseModel) {
            val random = Random.nextInt(images.size)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(binding.imgCus.context,
                images[random]), binding.imgCus)
            binding.tvOrderId.text = data.orderId.toString()
            binding.tvItemCount.text = data.name.toString()
            binding.tvOrderStatus.text = data.statusName.toString()
            binding.tvDate.text =
                DateUtil.getFormattedDateUtcToGmt(AppENUM.DateUtilKey.TIME_FORMAT_NZ,
                    AppENUM.DDMMYYYY,
                    data.createdAt ?: "")
            if (data.invoiceAmount?.isNotEmpty() == true) binding.tvAmount.text =
                currencySymbol + "${data.invoiceAmount}"
            else {
                binding.tvAmount.text = "NA"
            }
            binding.cvBase.setOnClickListener {
                binding.cvBase.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<OrderListResponseModel> {
        return dataList
    }

    fun addItemToAdapterList(item: OrderListResponseModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}