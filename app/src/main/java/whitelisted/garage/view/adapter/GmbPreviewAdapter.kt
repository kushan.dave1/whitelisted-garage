package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_gmb_preview.view.*
import whitelisted.garage.R
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class GmbPreviewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var isGMB = false
    private var dataList: MutableList<String>? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return GmbPreviewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_gmb_preview, parent, false))
    }

    override fun getItemCount(): Int = dataList?.size ?: 0
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as GmbPreviewHolder).bind()
    }

    inner class GmbPreviewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataList?.get(layoutPosition)
            if (isGMB) {
                itemView.imagePreviewWebsite.makeVisible(false)
                itemView.imagePreviewGMB.makeVisible(true)

                ImageLoader.loadImage(itemView.imagePreviewGMB, model)
            } else {
                itemView.imagePreviewGMB.makeVisible(false)
                itemView.imagePreviewWebsite.makeVisible(true)
                ImageLoader.loadImage(itemView.imagePreviewWebsite, model)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(data: MutableList<String>) {
        dataList = data
        notifyDataSetChanged()
    }

}