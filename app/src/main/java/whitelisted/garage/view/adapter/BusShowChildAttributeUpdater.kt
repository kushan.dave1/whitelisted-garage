package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_bus_show_child_attributes.view.*
import whitelisted.garage.R


class BusShowChildAttributeUpdater : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var dataList = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AttributeViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_bus_show_child_attributes, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AttributeViewHolder).bind()
    }

    private inner class AttributeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            itemView.tvChildAttribute.text = dataList[layoutPosition]
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: MutableList<String>) {
        dataList = list
        notifyDataSetChanged()
    }


}