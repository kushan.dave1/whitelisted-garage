package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_recycler_view_dotted.view.*
import whitelisted.garage.R
import whitelisted.garage.utils.CommonUtils.makeVisible


class RecyclerDottedProgressAdapter(

    var dataListItemModel: MutableList<Boolean>,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ProgressViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_recycler_view_dotted, parent, false))
    }

    override fun getItemCount(): Int = dataListItemModel.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ProgressViewHolder).bind()
    }

    inner class ProgressViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val data = dataListItemModel[layoutPosition]
            if (data) {
                itemView.llLongProgressView.makeVisible(true)
                itemView.llProgressDot.makeVisible(false)
            } else {
                itemView.llProgressDot.makeVisible(true)
                itemView.llLongProgressView.makeVisible(false)
            }
        }
    }

    fun getData(): MutableList<Boolean> {
        return dataListItemModel
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(data: MutableList<Boolean>) {
        dataListItemModel.clear()
        dataListItemModel = data
        notifyDataSetChanged()
    }

}