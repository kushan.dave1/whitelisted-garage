package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_coupon.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.Value
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader


class CouponAdapter(var onClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var dataListItemModel: List<Value>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CouponItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_coupon, parent, false))
    }

    override fun getItemCount(): Int = dataListItemModel?.size ?: 0
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CouponItemViewHolder).bind()
    }

    inner class CouponItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataListItemModel?.get(layoutPosition)
            model?.let {

                itemView.rootViewParent.setOnClickListener {
                    it.setTag(R.id.rootViewParent, model)
                    onClickListener.onClick(it)
                }
                if (model.is_timer == true) {
                    itemView.llValid.makeVisible(false)
                    ImageLoader.loadImageWithCallback(itemView.imgCoupon,
                        model.image,
                        object : ImageLoader.ImageLoadCallback {
                            override fun onSuccess() {
                                itemView.llValid.makeVisible(true)
                            }

                            override fun onError(throwable: Throwable) {
                                itemView.llValid.makeVisible(false)
                            }
                        },
                        placeholderImage = R.drawable.ic_placeholder_doc)
                    handleCounter(model, itemView)
                } else {
                    ImageLoader.loadImage(itemView.imgCoupon, model.image)
                    itemView.llValid.makeVisible(false)
                }
            }

        }

        private fun handleCounter(model: Value, itemView: View) {
            val differentTimeInHour =
                CommonUtils.differenceFromTodayInHours(model.createdAt ?: "", (model.valid_upto))
            val countDownTimer = object : CountDownTimer(differentTimeInHour, 1000) {
                override fun onTick(millisUntilFinished: Long) {

                    val totalSecondsLeft = millisUntilFinished.toInt() / 1000
                    val hoursLeft = totalSecondsLeft / 3600
                    val minutesLeft = totalSecondsLeft % 3600 / 60
                    val secondsLeft = totalSecondsLeft % 60
                    setDataToTextView(CommonUtils.convertIntTo2ZerosPlaces(hoursLeft),
                        itemView.tvHours1,
                        itemView.tvHours2)
                    setDataToTextView(CommonUtils.convertIntTo2ZerosPlaces(minutesLeft),
                        itemView.tvMinutes1,
                        itemView.tvMinutes2)
                    setDataToTextView(CommonUtils.convertIntTo2ZerosPlaces(secondsLeft),
                        itemView.tvSec1,
                        itemView.tvSec2)
                }

                override fun onFinish() {
                    setDataToTextView(CommonUtils.convertIntTo2ZerosPlaces(0),
                        itemView.tvHours1,
                        itemView.tvHours2)
                    setDataToTextView(CommonUtils.convertIntTo2ZerosPlaces(0),
                        itemView.tvMinutes1,
                        itemView.tvMinutes2)
                    setDataToTextView(CommonUtils.convertIntTo2ZerosPlaces(0),
                        itemView.tvSec1,
                        itemView.tvSec2)
                }

            }
            countDownTimer.start()
        }
    }

    private fun setDataToTextView(time: String, tv1: TextView, tv2: TextView) {
        val char = time.toCharArray()
        when (char.size) {
            0, 1 -> {
                tv1.text = "0"
                tv2.text = "0"
            }
            2 -> {
                tv1.text = char[0].toString()
                tv2.text = char[1].toString()
            }
            else -> {
                tv1.text = time.subSequence(0, (time.length - 1)).toString()
                tv2.text = char[char.size - 1].toString()
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setUpData(data: List<Value>) {
        this.dataListItemModel = data
        notifyDataSetChanged()
    }
}