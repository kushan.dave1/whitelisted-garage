package whitelisted.garage.view.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import coil.transform.CircleCropTransformation
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.item_workshop.view.*
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.databinding.ItemWorkshopBinding
import whitelisted.garage.databinding.ItemWorkshopLockedBannerBinding
import whitelisted.garage.databinding.ItemWorkshopLockedBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.utils.ImageLoader
import java.util.*

class WorkshopsAdapter(
    val workshopRestrictionMsg : String,
    val userId: String,
    var dataList: MutableList<WorkshopListResponseModel>,
    val recyclerItemClickListener: View.OnClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    init {
        val isLocked = dataList.any { it.isLocked ?: false }
        if (isLocked) {
            val list = mutableListOf<WorkshopListResponseModel>()
            list.addAll(dataList.filter { item -> item.isLocked == false })
            list.add(WorkshopListResponseModel(dbId = -1))
            list.addAll(dataList.filter { item -> item.isLocked == true })
            dataList = list
        }
    }


    companion object {
        const val ENABLED_ITEM = 1
        const val BANNER = 2
        const val DISABLED_ITEM = 3
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            dataList[position].workshopId.isNullOrEmpty() -> SelectWorkshopAdapter.BANNER
            dataList[position].isLocked == true -> SelectWorkshopAdapter.DISABLED_ITEM
            else -> SelectWorkshopAdapter.ENABLED_ITEM
        }
    }


    private lateinit var storageReference: StorageReference
    private lateinit var workshopImageReference: StorageReference

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        storageReference = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
        return when (viewType) {
            ENABLED_ITEM -> WorkshopsItemViewHolder(
                ItemWorkshopBinding.inflate(LayoutInflater.from(parent.context),parent,false)
            )

            DISABLED_ITEM -> WorkshopLockedItemViewHolder(
                ItemWorkshopLockedBinding.inflate(LayoutInflater.from(parent.context),parent,false)
            )

            else -> BannerViewHolder(ItemWorkshopLockedBannerBinding.inflate(LayoutInflater.from(parent.context),parent,false))

        }
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is WorkshopsItemViewHolder -> holder.bind()
            is WorkshopLockedItemViewHolder -> holder.bind()
            is BannerViewHolder -> holder.bind()
        }
    }


    inner class BannerViewHolder(var binding: ItemWorkshopLockedBannerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            binding.root.setOnClickListener {
                CommonUtils.addFragmentUtil( binding.root.context as FragmentActivity,
                    FragmentFactory.fragment(FragmentFactory.Fragments.PRO_LOCKED_FRAGMENT, params = Bundle().apply {
                        putString(AppENUM.PRO_LOCKED_ALERT_MSG, workshopRestrictionMsg)
                    }))
            }

        }
    }

    inner class WorkshopsItemViewHolder(private val binding: ItemWorkshopBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]
            binding.tvWorkshopName.text = model.name
            binding.tvWorkShopID.text = model.workshopId
            binding.tvOwnerName.text = model.ownerName
            binding.tvAddress.text = model.address
            loadImageFromFirebase()
            when (model.types) {

                AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                    binding.tvShopStatus.text =
                        binding.tvShopStatus.context.getString(R.string.accessories_shop)
                            .toUpperCase(Locale.getDefault())
                    binding.tvShopStatus.setTextColor(
                        CommonUtils.getColor(
                            binding.tvShopStatus.context,
                            R.color.text_acc_type
                        )
                    )
                    binding.tvShopStatus.background =
                        CommonUtils.getDrawable(
                            binding.tvShopStatus.context,
                            R.drawable.bg_acc_type
                        )
                }
                AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                    binding.tvShopStatus.text =
                        binding.tvShopStatus.context.getString(R.string.spares_shop)
                            .toUpperCase(Locale.getDefault())
                    binding.tvShopStatus.setTextColor(
                        CommonUtils.getColor(
                            binding.tvShopStatus.context,
                            R.color.text_retailer_type
                        )
                    )
                    binding.tvShopStatus.background =
                        CommonUtils.getDrawable(
                            binding.tvShopStatus.context,
                            R.drawable.bg_retail_type
                        )
                }
                else -> {
                    binding.tvShopStatus.text =
                        binding.tvShopStatus.context.getString(R.string.workshop)
                            .toUpperCase(Locale.getDefault())
                    binding.tvShopStatus.setTextColor(
                        CommonUtils.getColor(
                            binding.tvShopStatus.context,
                            R.color.text_workshop_type
                        )
                    )
                    binding.tvShopStatus.background =
                        CommonUtils.getDrawable(
                            binding.tvShopStatus.context,
                            R.drawable.bg_workshop_type
                        )
                }
            }

            binding.imageContextCopy.setOnClickListener {
                binding.imageContextCopy.setTag(R.id.model, model.workshopId)
                recyclerItemClickListener.onClick(it)
            }
            binding.imgOptions.setOnClickListener {
                binding.imgOptions.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }

            binding.cvBase.setOnClickListener {
                binding.cvBase.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }

        }

        private fun loadImageFromFirebase() {
            workshopImageReference =
                storageReference.child(AppENUM.RefactoredStrings.PATH_USERS).child("$userId/")
                    .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                    .child("${dataList[layoutPosition].workshopId}/")
                    .child(AppENUM.RefactoredStrings.PATH_WORKSHOP_IMAGE)
                    .child("${dataList[layoutPosition].workshopId}")
            workshopImageReference.downloadUrl.addOnSuccessListener {
                ImageLoader.loadImageWithCallback(
                    binding.imgWorkshopImage,
                    it.toString(),
                    object : ImageLoader.ImageLoadCallback {
                        override fun onSuccess() {
                            binding.imgWorkshopImage.scaleType = ImageView.ScaleType.CENTER_CROP
                        }

                        override fun onError(throwable: Throwable) {
                            binding.imgWorkshopImage.scaleType = ImageView.ScaleType.CENTER_INSIDE
                        }
                    },
                    transform = CircleCropTransformation(),
                    placeholderImage = R.drawable.ic_apartment_gray
                )
            }.addOnFailureListener {

            }.addOnCompleteListener {

            }
        }
    }

    inner class WorkshopLockedItemViewHolder(
        private val
        binding: ItemWorkshopLockedBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]
            binding.tvWorkshopName.text = model.name
            binding.tvWorkShopID.text = model.workshopId
            binding.tvOwnerName.text = model.ownerName
            binding.tvAddress.text = model.address
            loadImageFromFirebase()
            when (model.types) {

                AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                    binding.tvShopStatus.text =
                        binding.tvShopStatus.context.getString(R.string.accessories_shop)
                            .toUpperCase(Locale.getDefault())
                }
                AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                    binding.tvShopStatus.text =
                        binding.tvShopStatus.context.getString(R.string.spares_shop)
                            .toUpperCase(Locale.getDefault())
                }
                else -> {
                    binding.tvShopStatus.text =
                        binding.tvShopStatus.context.getString(R.string.workshop)
                            .toUpperCase(Locale.getDefault())

                }
            }
        }

        private fun loadImageFromFirebase() {
            workshopImageReference =
                storageReference.child(AppENUM.RefactoredStrings.PATH_USERS).child("$userId/")
                    .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                    .child("${dataList[layoutPosition].workshopId}/")
                    .child(AppENUM.RefactoredStrings.PATH_WORKSHOP_IMAGE)
                    .child("${dataList[layoutPosition].workshopId}")
            workshopImageReference.downloadUrl.addOnSuccessListener {
                ImageLoader.loadImageWithCallback(
                    binding.imgWorkshopImage,
                    it.toString(),
                    object : ImageLoader.ImageLoadCallback {
                        override fun onSuccess() {
                            binding.imgWorkshopImage.scaleType = ImageView.ScaleType.CENTER_CROP
                        }

                        override fun onError(throwable: Throwable) {
                            binding.imgWorkshopImage.scaleType = ImageView.ScaleType.CENTER_INSIDE
                        }
                    },
                    transform = CircleCropTransformation(),
                    placeholderImage = R.drawable.ic_apartment_gray
                )
            }.addOnFailureListener {

            }.addOnCompleteListener {

            }
        }
    }
}