package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_add_image_bid.view.*
import whitelisted.garage.R
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class BidItemPartPhotosAdapter(var dataList: MutableList<String>,
    val isUpload: Boolean,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_add_image_bid, parent, false))
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MyViewHolder).bind()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList[layoutPosition]
            if (model == "") {
                itemView.rlBusImage.makeVisible(false)
                itemView.rlAddImage.makeVisible(true)
                itemView.llDelImage.makeVisible(false)
            } else {
                if (isUpload) {
                    itemView.llDelImage.makeVisible(true)
                } else itemView.llDelImage.makeVisible(false)
                itemView.rlBusImage.makeVisible(true)
                itemView.rlAddImage.makeVisible(false)
                ImageLoader.loadImage(itemView.imgBusImage, model)
            }

            itemView.llDelImage.setOnClickListener {
                dataList.removeAt(layoutPosition)
                notifyItemRemoved(layoutPosition)
                if (dataList[0].isNotEmpty()) {
                    dataList.add(0, "")
                    notifyItemInserted(0)
                } else { // do nothing
                }
            }
            itemView.rlAddImage.setOnClickListener {
                it.setTag(R.id.rlAddImage, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): MutableList<String> {
        return dataList
    }

    fun addItemToAdapterList(item: String) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}