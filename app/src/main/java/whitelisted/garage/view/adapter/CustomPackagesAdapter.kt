package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_custom_package.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.AllPackagesResponseModel
import whitelisted.garage.utils.CommonUtils

class CustomPackagesAdapter(val currencySymbol: String,
    var dataList: MutableList<AllPackagesResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var isAccessories = false
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CustomPackageItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_custom_package, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CustomPackageItemViewHolder).bind()
    }

    inner class CustomPackageItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind() {

            val model = dataList[layoutPosition]
            itemView.tvPackageName.text = model.name
            itemView.tvPrice.text =
                "$currencySymbol${CommonUtils.convertDoubleTo2DecimalPlaces(model.total ?: 0.0)}"
            if (isAccessories) {
                itemView.tvItemsCount.text = model.items_count.toString()
            } else {
                itemView.tvItemsCount.text = model.totalServices?.toString()
            }
            itemView.imgEdit.setOnClickListener {
                itemView.imgEdit.setTag(R.id.position, layoutPosition)
                itemView.imgEdit.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }
            itemView.imgDel.setOnClickListener {
                itemView.imgDel.setTag(R.id.position, layoutPosition)
                itemView.imgDel.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }
            if (model.isTwoWheeler == true){
                itemView.tvVehicleType.setViewBackgroundColor(R.color.alt_gray)
                itemView.tvVehicleType.text = itemView.tvVehicleType.context.getString(R.string._2_wheeler)
            }else{
                itemView.tvVehicleType.setViewBackgroundColor(R.color.colorAccent)
                itemView.tvVehicleType.text = itemView.tvVehicleType.context.getString(R.string._4_wheeler)
            }
        }
    }

    fun getAdapterList(): List<AllPackagesResponseModel> {
        return dataList
    }

    fun addItemToAdapterList(item: AllPackagesResponseModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}