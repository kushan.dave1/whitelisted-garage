package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.FaqsItem
import whitelisted.garage.databinding.ItemGoCoinsFaqBinding

class GoCoinsFAQAdapter(
    var dataList: MutableList<FaqsItem>,
    val recyclerItemClickListener: View.OnClickListener? = null
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return GCFAQItemViewHolder(
            ItemGoCoinsFaqBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as GCFAQItemViewHolder).bind()
    }

    inner class GCFAQItemViewHolder(private val binding: ItemGoCoinsFaqBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]
            binding.tvQuestion.text = model.que ?: model.ques

            if (model.isSelected == true) {
                binding.llAnswer.visibility = View.VISIBLE
                binding.imgToggle.setImageResource(R.drawable.ic_arrow_up_black)
            } else {
                binding.llAnswer.visibility = View.GONE
                binding.imgToggle.setImageResource(R.drawable.ic_arrow_down_black)
            }
            binding.clBase.setOnClickListener {
                binding.clBase.setTag(R.id.model, layoutPosition)
                recyclerItemClickListener?.onClick(it) ?: setSelected(layoutPosition)
            }

            if (!model.mcq.isNullOrEmpty()) {
                var text = model.ans + "\n"
                model.mcq.forEach {
                    text = text.plus(" • ").plus(it).plus("\n")
                }
                binding.tvAnswer.text = text
            } else if (!model.type.isNullOrEmpty()) {
                var text = model.ans + "\n"
                model.type.forEach {
                    text = text.plus(" • ").plus(it).plus("\n")
                }
                binding.tvAnswer.text = text
            } else
                binding.tvAnswer.text = model.ans
        }

        private fun setSelected(position: Int) {
            dataList[position].isSelected = dataList[position].isSelected != true
            notifyItemChanged(position)
        }
    }

    fun getAdapterList(): List<FaqsItem> {
        return dataList
    }

    fun closeAll() {
        dataList.forEachIndexed { index, faqsItem ->
            if (faqsItem.isSelected == true) {
                faqsItem.isSelected = false
                notifyItemChanged(index)
            }
        }
    }


}