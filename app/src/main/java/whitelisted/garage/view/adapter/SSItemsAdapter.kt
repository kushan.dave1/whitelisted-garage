package whitelisted.garage.view.adapter

import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.ViewCompat
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.ImageAssetDelegate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import whitelisted.garage.R
import whitelisted.garage.api.request.Enquiry
import whitelisted.garage.api.response.SSItemModel
import whitelisted.garage.databinding.CardEnquireLookingForBinding
import whitelisted.garage.databinding.CardPlpBannerBinding
import whitelisted.garage.databinding.ItemSsItemBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeViewCompoundDrawablesWithInterinsicBounds
import whitelisted.garage.utils.CommonUtils.checkNull
import whitelisted.garage.utils.CommonUtils.getBitmapFromUrl
import whitelisted.garage.utils.CommonUtils.makeInVisible
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader
import kotlin.math.ceil
import kotlin.math.roundToLong

class SSItemsAdapter(val currencySymbol: String,
    var dataList: MutableList<SSItemModel>,
    val recyclerItemClickListener: View.OnClickListener,
    private val showBorder: Boolean = false,
    private val isRecentSearchItem: Boolean = false,
    private val addEnquiryBanner: Boolean = false) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val ITEM = 1
        const val BANNER = 2
        const val ENQUIRY = 3
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            addEnquiryBanner && position == 0 -> ENQUIRY
            dataList[position - if(addEnquiryBanner) 1 else 0].isBanner -> BANNER
            else -> ITEM
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when(viewType) {
            ENQUIRY -> EnquiryHolder(CardEnquireLookingForBinding.inflate(LayoutInflater.from(
                parent.context), parent, false))

            BANNER -> BannerViewHolder(CardPlpBannerBinding.inflate(LayoutInflater.from(
                parent.context), parent, false))

            else -> SSItemsViewHolder(ItemSsItemBinding.inflate(LayoutInflater.from(parent.context),
                parent,
                false))
        }
    }

    override fun getItemCount(): Int = dataList.size + if(addEnquiryBanner) 1 else 0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val pos = position - if(addEnquiryBanner) 1 else 0
        when (holder) {
            is BannerViewHolder -> holder.bind(pos)
            is SSItemsViewHolder -> holder.bind(pos)
        }
    }

    inner class BannerViewHolder(var binding: CardPlpBannerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            when (dataList[position].bannerObject?.type ?: 1) {
                1 -> {
                    ImageLoader.loadImage(binding.imgTopBannerSWFF,
                        dataList[position].bannerObject?.url,
                        placeholderImage = R.drawable.ic_placeholder_gbc_banner)
                    binding.clPlayView.makeVisible(false)
                    binding.imgTopBannerSWFF.makeVisible(true)
                    binding.lavCPB.makeVisible(false)
                }
                2 -> {
                    binding.imgTopBannerSWFF.makeVisible(false)
                    binding.lavCPB.makeVisible(true)
                    dataList[position].bannerObject?.lottieImages?.let { lottieImages ->
                        for (lImage in lottieImages) {
                            CoroutineScope(Dispatchers.Main).launch {
                                val bitmap =
                                    getBitmapFromUrl(binding.lavCPB.context, lImage).await()
                                binding.lavCPB.setImageAssetDelegate(ImageAssetDelegate {
                                    return@ImageAssetDelegate bitmap
                                })
                                if (lImage == lottieImages[lottieImages.size - 1]) {
                                    binding.lavCPB.setAnimationFromUrl(dataList[position].bannerObject?.url)
                                    binding.lavCPB.playAnimation()
                                }
                            }
                        }
                    }
                }
                else -> {
                    binding.imgTopBannerSWFF.makeVisible(true)
                    binding.lavCPB.makeVisible(false)
                    ImageLoader.loadImage(binding.imgTopBannerSWFF,
                        dataList[position].bannerObject?.thumbnail,
                        placeholderImage = R.drawable.ic_placeholder_gbc_banner)
                    binding.clPlayView.makeVisible(true)
                }
            }

            binding.imgTopBannerSWFF.setOnClickListener {
                it.setTag(R.id.model, dataList[position])
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    inner class SSItemsViewHolder(var binding: ItemSsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            if (showBorder) {
                binding.customViewISSI.setBorder(2f, R.color.new_gray, 30f)
            }

            if(isRecentSearchItem) {
                binding.tvCompatibility.makeVisible(false)
                binding.tvItemId.makeVisible(false)
                binding.clTagISSI.makeInVisible(false)
                binding.tvBrandName.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                    setMargins(16,0,0,0)
                }
                binding.ivItemImage.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                    setMargins(16,1,16,0)
                }
            }
        }

        fun bind(position: Int) {

            val model = dataList[position]

            if (model.isShimmer) {
                binding.sflISSI.makeVisible(true)
                binding.clItemISSI.makeVisible(false)
            } else {
                binding.sflISSI.makeVisible(false)
                binding.clItemISSI.makeVisible(true)
                ImageLoader.loadImageWithCallback(binding.ivItemImage,
                    model.imageUrl,
                    placeholderImage = R.drawable.ic_placeholder_square,
                    imageLoadCallback = object : ImageLoader.ImageLoadCallback {
                        override fun onSuccess() {

                        }

                        override fun onError(throwable: Throwable) {
                            ImageLoader.loadDrawable(
                                ContextCompat.getDrawable(binding.ivItemImage.context,
                                    R.drawable.ic_no_image),
                                binding.ivItemImage,
                            )
                        }
                    })

                if (!model.brand.isNullOrEmpty()) binding.tvBrandName.text = model.brand
                else binding.tvBrandName.text =
                    binding.tvBrandName.context.getString(R.string.generic)

                binding.tvItemName.text = model.title
                if (model.subHeading.isNullOrEmpty()) {
                    binding.tvCompatibility.text =
                        binding.tvCompatibility.context.getString(R.string.generic)
                } else {
                    binding.tvCompatibility.text = model.subHeading
                }
                "${binding.tvItemId.context.getString(R.string.part_no_colon)} ${model.skuCode}".apply {
                    binding.tvItemId.text = this
                }
                "$currencySymbol${model.mrp?.roundToLong()?.toDouble()}".apply {
                    ("${binding.tvMRP.context.getString(R.string.mrp)} $this").apply {
                        binding.tvMRP.text = this
                    }
                    binding.tvMRP.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                }
                "$currencySymbol${model.gomPrice?.roundToLong()?.toDouble()}".apply {
                    binding.tvPrice.text = this
                }
                if ((model.gomPrice ?: 0.0) == (model.mrp ?: 0.0)) {
                    binding.tvPercentOff.makeVisible(false)
                } else {
                    val percent =
                        ((model.mrp?.minus(model.gomPrice ?: 0.0))?.times(100))?.div(model.mrp ?: 0.0)
                    if ((percent
                            ?: 0.0) > 0) { //                    binding.tvPercentOff.makeVisible(true)
                        binding.tvPercentOff.makeVisible(false)
                        "${ceil(percent ?: 0.0).toInt()}% ${binding.tvPercentOff.context.getString(R.string.off)}".apply {
                            binding.tvPercentOff.text = this
                        }
                    } else {
                        binding.tvPercentOff.makeVisible(false)
                    }
                }

                binding.btnAdd.setOnClickListener {
                    binding.btnAdd.makeVisible(false)
                    binding.rlPlusMinus.makeVisible(true)
                    binding.tvQuantity.text = "1"
                    model.quantity = binding.tvQuantity.text.toString().toInt()
                    it.setTag(R.id.model, model)
                    recyclerItemClickListener.onClick(it)
                }
                binding.imgPlus.setOnClickListener {
                    if (model.inventory.checkNull() > model.quantity.checkNull()) {
                        binding.tvQuantity.text =
                            "${binding.tvQuantity.text.toString().toInt().plus(1)}"
                        model.quantity = binding.tvQuantity.text.toString().toInt()
                        it.setTag(R.id.model, model)
                        recyclerItemClickListener.onClick(it)
                    } else CommonUtils.showToast(
                        binding.root.context,
                        binding.root.context.resources.getString(R.string.only_items_in_stock, model.quantity)
                    )
                }
                binding.imgMinus.setOnClickListener {
                    binding.tvQuantity.text =
                        "${binding.tvQuantity.text.toString().toInt().minus(1)}"
                    model.quantity = binding.tvQuantity.text.toString().toInt()
                    if ((binding.tvQuantity.text.toString().toInt()) == 0) {
                        binding.btnAdd.makeVisible(true)
                        binding.rlPlusMinus.makeVisible(false)
                    } else { //do nothing
                    }
                    it.setTag(R.id.model, model)
                    recyclerItemClickListener.onClick(it)
                }
                binding.clISI.setOnClickListener {
                    it.setTag(R.id.model, model)
                    recyclerItemClickListener.onClick(it)
                }

                binding.btnEnquireNow.setOnClickListener {
                    it.setTag(R.id.model, model)
                    recyclerItemClickListener.onClick(it)
                }

                if (model.isEnquiry) {
                    binding.btnEnquireNow.makeVisible(true)
                    binding.viewOOSLayer.makeVisible(false)
                    binding.tvOutOfStockISSI.makeVisible(false)
                    binding.tvNotifyMeISSI.makeVisible(false)
                    binding.rlPlusMinus.makeVisible(false)
                    binding.btnAdd.makeVisible(false)
                    if (model.tag?.tagName.isNullOrEmpty()) {
                        binding.clTagISSI.makeInVisible(true)
                        binding.tvTagISSI.text =
                            binding.tvTagISSI.context.getString(R.string.best_price)
                    } else {
                        binding.clTagISSI.makeInVisible(false)
                        model.tag?.let { tag ->
                            binding.tvTagISSI.text = tag.tagName
                            val wrappedDrawable =
                                DrawableCompat.wrap(AppCompatResources.getDrawable(binding.tvTagISSI.context,
                                    R.drawable.ic_ss_tag_bg)!!)
                            wrappedDrawable.setTint(Color.parseColor(tag.bgColor))
                            binding.imgTagBg.setImageDrawable(wrappedDrawable)
                            binding.tvTagISSI.setTextColor(Color.parseColor(tag.textColor))
                        }
                    }
                } else if ((model.inventory ?: 0) > 0) {
                    binding.btnEnquireNow.makeVisible(false)
                    binding.viewOOSLayer.makeVisible(false)
                    binding.tvOutOfStockISSI.makeVisible(false)
                    binding.tvNotifyMeISSI.makeVisible(false)
                    if ((model.quantity ?: 0) > 0) {
                        binding.rlPlusMinus.makeVisible(true)
                        binding.btnAdd.makeVisible(false)
                        binding.tvQuantity.text = model.quantity.toString()
                    } else {
                        binding.rlPlusMinus.makeVisible(false)
                        binding.btnAdd.makeVisible(true)
                    }
                    binding.btnAdd.isEnabled = true
                    if (model.tag?.tagName.isNullOrEmpty()) {
                        binding.clTagISSI.makeInVisible(true)
                        binding.tvTagISSI.text =
                            binding.tvTagISSI.context.getString(R.string.best_price)
                    } else {
                        binding.clTagISSI.makeInVisible(false)
                        model.tag?.let { tag ->
                            binding.tvTagISSI.text = tag.tagName
                            val wrappedDrawable =
                                DrawableCompat.wrap(AppCompatResources.getDrawable(binding.tvTagISSI.context,
                                    R.drawable.ic_ss_tag_bg)!!)
                            wrappedDrawable.setTint(Color.parseColor(tag.bgColor))
                            binding.imgTagBg.setImageDrawable(wrappedDrawable)
                            binding.tvTagISSI.setTextColor(Color.parseColor(tag.textColor))
                        }
                    }
                } else {
                    binding.btnEnquireNow.makeVisible(false)
                    binding.viewOOSLayer.makeVisible(true)
                    binding.tvOutOfStockISSI.makeVisible(true)
                    binding.tvNotifyMeISSI.makeVisible(true)
                    binding.rlPlusMinus.makeVisible(false)
                    binding.btnAdd.makeVisible(false)
                    binding.btnAdd.isEnabled = false
                    binding.clTagISSI.makeInVisible(true)

                    if (model.isNotified == true) {
                        binding.tvNotifyMeISSI.text =
                            binding.tvNotifyMeISSI.context.getString(R.string.reminder_on)
                        binding.tvNotifyMeISSI.changeViewCompoundDrawablesWithInterinsicBounds(
                            drawableEnd = ContextCompat.getDrawable(binding.tvNotifyMeISSI.context,
                                R.drawable.ic_ss_notified))
                        binding.tvNotifyMeISSI.isEnabled = false
                    } else {
                        binding.tvNotifyMeISSI.text =
                            binding.tvNotifyMeISSI.context.getString(R.string.remind_me)
                        binding.tvNotifyMeISSI.changeViewCompoundDrawablesWithInterinsicBounds(
                            drawableEnd = ContextCompat.getDrawable(binding.tvNotifyMeISSI.context,
                                R.drawable.ic_ss_notify_alert))
                        binding.tvNotifyMeISSI.isEnabled = true
                    }
                }

                binding.tvNotifyMeISSI.setOnClickListener {
                    it.setTag(R.id.model, model)
                    it.setTag(R.id.position, layoutPosition)
                    recyclerItemClickListener.onClick(it)
                }
            }
            ViewCompat.setTransitionName(binding.ivItemImage, model.skuCode)
        }

    }

    inner class EnquiryHolder(private val binding:CardEnquireLookingForBinding): RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener(recyclerItemClickListener)
        }
    }

    fun getAdapterList(): List<SSItemModel> {
        return dataList
    }

    fun addItemListToAdapter(items: List<SSItemModel>) {
        try {
            dataList.removeAt(dataList.size - 1)
            notifyItemRemoved(dataList.size + if(addEnquiryBanner) 1 else 0)
            dataList.removeAt(dataList.size - 1)
            notifyItemRemoved(dataList.size + if(addEnquiryBanner) 1 else 0)
            dataList.addAll(items)
            val startPos = dataList.size + if(addEnquiryBanner) 1 else 0
            notifyItemRangeInserted(startPos, items.size)
        } catch (e: ArrayIndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }
}
