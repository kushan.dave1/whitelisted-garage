package whitelisted.garage.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.item_saved_workshop.view.*
import whitelisted.garage.R


class SavedWorkshopsAdapter(context: Context, val arrayList: ArrayList<String>) :
    ArrayAdapter<String>(context, 0, arrayList) {

    fun provideDataList(): ArrayList<String> {
        return arrayList
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        // convertView which is recyclable view
        var currentItemView: View? = convertView

        // of the recyclable view is null then inflate the custom layout for the same
        if (currentItemView == null) {
            currentItemView =
                LayoutInflater.from(context).inflate(R.layout.item_saved_workshop, parent, false)
        }

        // get the position of the view from the ArrayAdapter
        val currentNumberPosition: String? = getItem(position)

        // then according to the position of the view assign the desired image for the same
        currentItemView?.tvSavedWorkshop?.text = currentNumberPosition
        if (currentNumberPosition == context.getString(R.string.create_new)) {
            currentItemView?.tvSavedWorkshop?.setTextColor(ContextCompat.getColorStateList(
                currentItemView.tvSavedWorkshop.context,
                R.color.colorAccent))
        } else {
            currentItemView?.tvSavedWorkshop?.setTextColor(ContextCompat.getColorStateList(
                currentItemView.tvSavedWorkshop.context,
                R.color.black_trans_new))
        }

        // then return the recyclable view
        return currentItemView ?: View(context)
    }
}