package whitelisted.garage.view.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import whitelisted.garage.api.response.MediaUrl
import whitelisted.garage.databinding.ItemProductImageZoomableBinding
import whitelisted.garage.databinding.ProductDetailImageItemBinding
import whitelisted.garage.databinding.ProductDetailVideoItemBinding

class ProductDetailImagesAdapter(
    private val mediaList: List<MediaUrl>,
    val listener: View.OnClickListener? = null,
    val isZoomableImages: Boolean = false
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var selectedPosition = 0

    override fun getItemViewType(position: Int) = mediaList[position].type

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return if (viewType == 1) {
            if(isZoomableImages) ProductZoomImageViewHolder(ItemProductImageZoomableBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ), parent, false
            ))
            else ProductDetailImageViewHolder(
                ProductDetailImageItemBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )
        }

        else ProductDetailVideoViewHolder(
            ProductDetailVideoItemBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ProductDetailImageViewHolder -> holder.bind(position)
            is ProductZoomImageViewHolder -> holder.bind(position)
            is ProductDetailVideoViewHolder -> {
                holder.bind(position)
                holder.setIsRecyclable(false)
            }
        }
    }

    override fun getItemCount() = mediaList.size

    inner class ProductDetailImageViewHolder(private val productDetailImageItemBinding: ProductDetailImageItemBinding) :
        RecyclerView.ViewHolder(productDetailImageItemBinding.root) {
        fun bind(position: Int) {
            mediaList[position].let { media ->
                productDetailImageItemBinding.ivPartImage.load(media.url)
                productDetailImageItemBinding.ivPartImage.setOnClickListener(listener)
            }
        }
    }

    inner class ProductZoomImageViewHolder(private val productDetailImageItemBinding: ItemProductImageZoomableBinding) :
        RecyclerView.ViewHolder(productDetailImageItemBinding.root) {
        fun bind(position: Int) {
            mediaList[position].let { media ->
                whitelisted.garage.utils.ImageLoader.loadImage(productDetailImageItemBinding.imgProductImageSDF,media.url)
                //productDetailImageItemBinding.imgProductImageSDF.load(media.url)
            }
        }
    }

    inner class ProductDetailVideoViewHolder(private val productDetailVideoBinding: ProductDetailVideoItemBinding) :
        RecyclerView.ViewHolder(productDetailVideoBinding.root) {
        fun bind(position: Int) {
            mediaList[position].let { media ->
                val videoUri = Uri.parse(media.url)
                productDetailVideoBinding.ivPartVideo.setVideoURI(videoUri)
                productDetailVideoBinding.ivPartVideo.start()
            }

            if (position == selectedPosition) productDetailVideoBinding.ivPartVideo.resume()
            else productDetailVideoBinding.ivPartVideo.pause()
        }
    }

    fun onPageScrolled(position: Int) {
        if(position != selectedPosition) {
            val oldPos = selectedPosition
            selectedPosition = position
            if(mediaList[oldPos].type == 2) notifyItemChanged(oldPos)
            if(mediaList[selectedPosition].type == 2) notifyItemChanged(selectedPosition)
        }
    }


}

