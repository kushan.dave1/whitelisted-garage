package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.GetInventoryRackResponse
import whitelisted.garage.databinding.ItemRackNameBinding

class RackListAdapter(val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RackListAdapter.MyViewHolder>() {
    private var dataList = mutableListOf<GetInventoryRackResponse>()
    override fun getItemCount(): Int = dataList.size


    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: MutableList<GetInventoryRackResponse>) {
        dataList.clear()
        dataList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = ItemRackNameBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = dataList[position]
        holder.bind(data, position)
    }

    inner class MyViewHolder(var binding: ItemRackNameBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: GetInventoryRackResponse, position: Int) {
            binding.tvRackName.text = data.name
            binding.tvRackName.setOnClickListener {
                binding.tvRackName.setTag(R.id.tvRackName, position)
                recyclerItemClickListener.onClick(binding.tvRackName)
            }
        }
    }

    fun getAdapterData(): MutableList<GetInventoryRackResponse> {
        return dataList
    }
}
