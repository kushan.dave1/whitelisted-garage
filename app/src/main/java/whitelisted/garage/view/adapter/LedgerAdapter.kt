package whitelisted.garage.view.adapter

import DateUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.Ledger
import whitelisted.garage.databinding.ItemLedgerBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils

class LedgerAdapter(
    val currencySymbol: String,
    var dataList: MutableList<Ledger>,
) : RecyclerView.Adapter<LedgerAdapter.MyViewHolder>() {

    override fun getItemCount(): Int = dataList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = ItemLedgerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = dataList[position]
        holder.bind(data)
    }

    inner class MyViewHolder(var binding: ItemLedgerBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: Ledger) {
            binding.tvCustomerName.text = data.name
            binding.tvPartDesc.text = data.remark
            binding.tvDate.text =
                DateUtil.getFormattedDateUtcToGmt(AppENUM.DateUtilKey.TIME_FORMAT_NZ,
                    AppENUM.DDMMYYYY,
                    data.date ?: "")
            "$currencySymbol${CommonUtils.convertDoubleTo2DecimalPlaces(data.amount ?: 0.0)}".apply {
                binding.tvAmount.text = this
            }
            if (data.type.equals(AppENUM.CREDIT)) {
                binding.tvAmount.setTextColor(CommonUtils.getColor(binding.tvAmount.context,
                    R.color.green_text_color))
            } else {
                binding.tvAmount.setTextColor(CommonUtils.getColor(binding.tvAmount.context,
                    R.color.red_text_color))

            }
        }
    }
}

