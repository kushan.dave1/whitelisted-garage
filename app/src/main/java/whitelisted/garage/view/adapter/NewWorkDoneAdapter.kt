package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_text_view.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.WorkDoneResponseModel
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils

class NewWorkDoneAdapter(var dataList: MutableList<WorkDoneResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ServiceTypeItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_text_view, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ServiceTypeItemViewHolder).bind()
    }

    inner class ServiceTypeItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataList[layoutPosition]
            itemView.tvTextView.text = model.name
            if (model.isSelected) {
                itemView.tvTextView.setTextColor(CommonUtils.getColor(itemView.tvTextView.context,
                    R.color.colorAccent))
            } else itemView.tvTextView.setTextColor(CommonUtils.getColor(itemView.tvTextView.context,
                R.color.blacktextA2))
            itemView.tvTextView.setOnClickListener {
                itemView.tvTextView.setTag(R.id.model, AppENUM.AdaptersConstant.SERVICE_TYPE)
                itemView.tvTextView.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<WorkDoneResponseModel> {
        return dataList
    }

    fun addItemToAdapterList(item: WorkDoneResponseModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}