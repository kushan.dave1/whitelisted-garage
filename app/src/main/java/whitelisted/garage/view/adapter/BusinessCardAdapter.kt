package whitelisted.garage.view.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import coil.ImageLoader
import coil.load
import whitelisted.garage.api.response.BusinessCardDetails
import whitelisted.garage.api.response.BusinessCardTemplate
import whitelisted.garage.databinding.CardBusinessCardBinding

class BusinessCardAdapter(val cards: List<BusinessCardTemplate>): RecyclerView.Adapter<BusinessCardAdapter.ViewHolder>() {

    var data: BusinessCardDetails? = null
        set(value) { field = value
            cards.indices.forEach {
                notifyItemChanged(it)
            }
        }

    inner class ViewHolder(private val binding: CardBusinessCardBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(card: BusinessCardTemplate) = binding.apply {

            root.clipToOutline = true
            root.outlineProvider = ViewOutlineProvider.BACKGROUND
            val imageLoader = ImageLoader.Builder(root.context).allowHardware(false).build()
            imgTemplate.load(card.data?.image?:card.image, imageLoader)

            val details = card.data?: data
            if(details!=null) with(details) {
                companyName.text = businessName
                personName.text = name
                binding.designation.text = "($designation)"
                mobileNumber.text = mobile
                emailAddress.text = email
                shopAddress.text = address

                emailAddress.visibility =  if(email.isEmpty()) View.GONE else View.VISIBLE
                iconEmail.visibility = if(email.isEmpty()) View.GONE else View.VISIBLE
            }

            val tc = card.data?.textColor?:card.textColor
            if(!tc.isNullOrEmpty()) with(Color.parseColor(tc)) {
                companyName.setTextColor(this)
                personName.setTextColor(this)
                designation.setTextColor(this)
                mobileNumber.setTextColor(this)
                emailAddress.setTextColor(this)
                shopAddress.setTextColor(this)
            }

            (card.data?.businessCardIcons?:card.businessCardIcons)?.let { icons ->
                iconName.load(icons.nameIcon, imageLoader)
                iconAddress.load(icons.addressIcon, imageLoader)
                iconMobile.load(icons.phoneIcon, imageLoader)
                iconEmail.load(icons.emailIcon, imageLoader)
            }

            /*if(!isEditCard) card.data?.let {
                btnDelete.makeVisible(true)
                btnShare.makeVisible(true)
                btnEdit.makeVisible(true)

                btnDelete.setOnClickListener { v->
                    v.tag = it.id
                    onCardClickListener?.invoke(v)
                }

                btnShare.setOnClickListener {
                    btnDelete.makeVisible(false)
                    btnShare.makeVisible(false)
                    btnEdit.makeVisible(false)
                    onCardClickListener?.invoke(root)
                }

                btnEdit.setOnClickListener { v ->
                    v.tag = it
                    onCardClickListener?.invoke(v)
                }

            }

            if(isShowShareAndDeleteButton) {
                btnDelete.makeVisible(true)
                btnShare.makeVisible(true)
                btnShare.makeVisible(true)
                isShowShareAndDeleteButton = false
            }*/

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = CardBusinessCardBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(cards[position])
        /*when (position) {
            0 -> {
                val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT)
                params.setMargins(40,0,10,0)
                holder.itemView.layoutParams = params
            }
            cards.size-1 -> {
                val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT)
                params.setMargins(10,0,40,0)
                holder.itemView.layoutParams = params
            }
            else -> {
                val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT)
                params.setMargins(5,0,5,0)
                holder.itemView.layoutParams = params
            }
        }*/

    }

    override fun getItemCount() = cards.size

}