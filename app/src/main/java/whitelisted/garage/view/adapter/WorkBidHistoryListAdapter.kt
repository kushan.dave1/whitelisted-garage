package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.GetWsOngoingBidResponse
import whitelisted.garage.databinding.ItemWsBidHistoryBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible


class WorkBidHistoryListAdapter(var genericItems: List<GetWsOngoingBidResponse>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return WSBidHistoryViewHolder(ItemWsBidHistoryBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = genericItems.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as WSBidHistoryViewHolder).bind()
    }

    inner class WSBidHistoryViewHolder(var view: ItemWsBidHistoryBinding) :
        RecyclerView.ViewHolder(view.root) {
        @SuppressLint("SetTextI18n")
        fun bind() {
            val modelData = genericItems[layoutPosition]
            view.tvOrderId.text = modelData.id
            when (modelData.statusId) {
                AppENUM.WSBiddingStatusConstant.COMPLETED -> {
                    view.llInvoiceGroup.makeVisible(true)
                    view.llTVBidStatus.text =
                        CommonUtils.getString(view.llTVBidStatus.context, R.string.completed)
                    view.llTVBidStatus.background =
                        CommonUtils.getDrawable(view.llTVBidStatus.context,
                            R.drawable.bg_ws_completed_bid)
                    view.llTVBidStatus.setTextColor(CommonUtils.getColor(view.llTVBidStatus.context,
                        R.color.chestnut))
                }
                AppENUM.WSBiddingStatusConstant.REJECTED -> {
                    view.llInvoiceGroup.makeVisible(false)
                    view.llTVBidStatus.text =
                        CommonUtils.getString(view.llTVBidStatus.context, R.string.rejected)
                    view.llTVBidStatus.background =
                        CommonUtils.getDrawable(view.llTVBidStatus.context,
                            R.drawable.bg_ws_rejected_bid)
                    view.llTVBidStatus.setTextColor(CommonUtils.getColor(view.llTVBidStatus.context,
                        R.color.gom_red))
                }
                AppENUM.WSBiddingStatusConstant.CANCLED -> {
                    view.llInvoiceGroup.makeVisible(false)
                    view.llTVBidStatus.text =
                        CommonUtils.getString(view.llTVBidStatus.context, R.string.canceled)
                    view.llTVBidStatus.background =
                        CommonUtils.getDrawable(view.llTVBidStatus.context,
                            R.drawable.bg_ws_rejected_bid)
                    view.llTVBidStatus.setTextColor(CommonUtils.getColor(view.llTVBidStatus.context,
                        R.color.gom_red))
                }
                AppENUM.WSBiddingStatusConstant.ACCEPTED -> {
                    view.llInvoiceGroup.makeVisible(false)
                    view.llTVBidStatus.text =
                        CommonUtils.getString(view.llTVBidStatus.context, R.string.accepted)
                    view.llTVBidStatus.background =
                        CommonUtils.getDrawable(view.llTVBidStatus.context,
                            R.drawable.bg_ws_new_bid)
                    view.llTVBidStatus.setTextColor(CommonUtils.getColor(view.llTVBidStatus.context,
                        R.color.status_completed))
                }
                AppENUM.WSBiddingStatusConstant.NEW -> {
                    view.llInvoiceGroup.makeVisible(false)
                    view.llTVBidStatus.text =
                        CommonUtils.getString(view.llTVBidStatus.context, R.string.new_)
                    view.llTVBidStatus.background =
                        CommonUtils.getDrawable(view.llTVBidStatus.context,
                            R.drawable.bg_ws_new_bid)
                    view.llTVBidStatus.setTextColor(CommonUtils.getColor(view.llTVBidStatus.context,
                        R.color.status_completed))
                }
                AppENUM.WSBiddingStatusConstant.ON_GOING -> {
                    view.llInvoiceGroup.makeVisible(false)
                    view.llTVBidStatus.text =
                        CommonUtils.getString(view.llTVBidStatus.context, R.string.on_going)
                    view.llTVBidStatus.background =
                        CommonUtils.getDrawable(view.llTVBidStatus.context,
                            R.drawable.bg_ws_onngoig_bid)
                    view.llTVBidStatus.setTextColor(CommonUtils.getColor(view.llTVBidStatus.context,
                        R.color.color_sky))

                }
            }
            modelData.retailerDetails?.let {
                view.tvRetailerName.text = it.workshopName ?: ""
            }
            modelData.createdAt?.let {
                if (it.isNotEmpty()) {
                    view.tvDate.text = CommonUtils.convertToDate(it)
                    view.tvTime.text = CommonUtils.convertToTime(it)
                }
            }
            view.tvTotalAmout.text = modelData.totalAmount ?: "0"
            view.tvTotalItems.text = (modelData.totalItem ?: 0).toString()
            view.btnInvoice.setOnClickListener {
                view.btnInvoice.setTag(R.id.model, modelData)
                recyclerItemClickListener.onClick(it)
            }
        }
    }
}