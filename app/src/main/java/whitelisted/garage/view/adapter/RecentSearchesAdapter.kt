package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.databinding.ItemRecentSearchBinding
import whitelisted.garage.utils.CommonUtils.makeVisible

class RecentSearchesAdapter(val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecentSearchesAdapter.MyViewHolder>() {
    private var dataList = mutableListOf<String>()
    override fun getItemCount(): Int = dataList.size

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: MutableList<String>) {
        dataList.clear()
        dataList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemRecentSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = dataList[position]
        holder.bind(data, position)
    }

    inner class MyViewHolder(var binding: ItemRecentSearchBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: String, position: Int) {

            binding.tvItemNameIRS.text = data
            binding.tvItemNameIRS.setOnClickListener {
                it.setTag(R.id.model, data)
                recyclerItemClickListener.onClick(it)
            }

            if (position == dataList.size - 1) {
                binding.viewDividerIRS.makeVisible(false)
            } else binding.viewDividerIRS.makeVisible(true)
        }
    }

    fun getAdapterData(): MutableList<String> {
        return dataList
    }
}
