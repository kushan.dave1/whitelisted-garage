package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_home_payment.view.*
import whitelisted.garage.R
import whitelisted.garage.api.request.HomeSummeryData
import whitelisted.garage.utils.CommonUtils.makeVisible

class HomeSummeryAdapter(var context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var dataList: List<HomeSummeryData>? = emptyList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return HomeSummeryViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_home_payment, parent, false))
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: List<HomeSummeryData>) {
        dataList = emptyList()
        dataList = list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = dataList?.size ?: 0
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as HomeSummeryViewHolder).bind()
    }

    inner class HomeSummeryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val data = dataList?.get(layoutPosition)
            data?.let {
                if (data.isOrderSummery) {
                    itemView.ll0.makeVisible(false)
                    itemView.tv1.text = data.openTotal
                    itemView.tv2.text = data.inProgressPending
                    itemView.tv3.text = data.completePaid
                    itemView.tvLL1.text = context.getString(R.string.open)
                    itemView.tvLL2.text = context.getString(R.string.in_progress)
                    itemView.tvLL3.text = context.getString(R.string.completed)
                    itemView.tvSummeryHeader.text =
                        context.getString(R.string.order_summary) //                    itemView.imgOrdersIndicator.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_home_indicator_active))
                    //                    itemView.imgPaymentsIndicator.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_home_indicator_inactive))
                } else {
                    itemView.ll0.makeVisible(false)
                    itemView.tv1.text = data.openTotal
                    itemView.tv2.text = data.inProgressPending
                    itemView.tv3.text = data.completePaid
                    itemView.tvLL1.text = context.getString(R.string.total)
                    itemView.tvLL2.text = context.getString(R.string.pending)
                    itemView.tvLL3.text = context.getString(R.string.paid)
                    itemView.tvSummeryHeader.text =
                        context.getString(R.string.payment_summary) //                    itemView.imgOrdersIndicator.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_home_indicator_inactive))
                    //                    itemView.imgPaymentsIndicator.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_home_indicator_active))
                }
            }
        }
    }

}