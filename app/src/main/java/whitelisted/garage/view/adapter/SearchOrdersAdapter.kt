package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.SearchOrderResponseModel
import whitelisted.garage.databinding.ItemSearchOrderBinding
import whitelisted.garage.databinding.ItemSearchOrderRetailBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.ImageLoader
import kotlin.random.Random

class SearchOrdersAdapter(val currencySymbol: String,
    var dataList: MutableList<SearchOrderResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val images =
        listOf(R.drawable.ic_dummy_cus1, R.drawable.ic_dummy_cus2, R.drawable.ic_dummy_cus3)
    var viewType = 0

    companion object {
        const val VIEW_TYPE_ONE = 0
        const val VIEW_TYPE_TWO = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ONE) {
            return UnderstandingHolder(ItemSearchOrderBinding.inflate(LayoutInflater.from(parent.context),
                parent,
                false))
        }
        return MyViewHolder1(ItemSearchOrderRetailBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (viewType == VIEW_TYPE_ONE) {
            (holder as UnderstandingHolder).bind(dataList[position])
        } else {
            (holder as MyViewHolder1).bind(dataList[position])
        }
    }

    inner class UnderstandingHolder(var binding: ItemSearchOrderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: SearchOrderResponseModel) {
            ImageLoader.loadImage(binding.imageView, model.carDetails?.car_icon)
            binding.tvCarName.text = model.carDetails?.car
            binding.tvRegNumber.text = model.carDetails?.registrationNumber
            binding.tvStatus.text = model.statusName
            binding.tvCustomerName.text = model.customerDetails?.name
            binding.tvInvoiceId.text = model.orderId
            binding.tvUpdatedAtTime.text = CommonUtils.convertDateWithDay(model.updatedAt)
            binding.tvOrderDate.text = CommonUtils.convertToDate(model.createdAt)
            binding.btnCall.setOnClickListener {
                binding.btnCall.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            binding.clBaseBuyGC.setOnClickListener {
                binding.clBaseBuyGC.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }

            binding.btnCreateNewOrder.setOnClickListener{
                it.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    inner class MyViewHolder1(var binding: ItemSearchOrderRetailBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(model: SearchOrderResponseModel) {
            val random = Random.nextInt(images.size)
            ImageLoader.loadDrawable(ContextCompat.getDrawable(binding.imgCus.context,
                images[random]), binding.imgCus)
            binding.tvPhone.text = model.customerDetails?.mobile.toString()
            binding.tvOrderId.text = model.orderId
            "$currencySymbol${CommonUtils.convertDoubleTo2DecimalPlaces(model.paymentInfo?.bill_amount ?: 0.0)}".apply {
                binding.tvAmount.text = this
            }
            binding.tvName.text = model.customerDetails?.name
            binding.tvDate.text = CommonUtils.convertToDate(model.createdAt)
            binding.btnCall.setOnClickListener {
                binding.btnCall.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            binding.clBaseBuyGC.setOnClickListener {
                binding.clBaseBuyGC.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            binding.btnCreateNewOrder.setOnClickListener{
                it.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<SearchOrderResponseModel> {
        return dataList
    }

    fun addItemToAdapterList(item: SearchOrderResponseModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addAllToAdapterList(items: List<SearchOrderResponseModel>) {
        dataList.clear()
        dataList.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return when (viewType) {
            0 -> VIEW_TYPE_ONE
            1 -> VIEW_TYPE_TWO
            else -> {
                0
            }
        }
    }
}