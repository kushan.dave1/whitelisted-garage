package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.RemindersItem
import whitelisted.garage.databinding.ItemReminderBinding
import whitelisted.garage.databinding.ItemReminderRetailBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class RemindersAdapter(

    var dataList: MutableList<RemindersItem>, val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var viewType = 0
    var restrictionDate = ""


    companion object {
        const val VIEW_TYPE_ONE = 0
        const val VIEW_TYPE_TWO = 1
    }

    override fun getItemViewType(position: Int): Int {
        return when (viewType) {
            0 -> VIEW_TYPE_ONE
            1 -> VIEW_TYPE_TWO
            else -> {
                0
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ONE) {
            return RemainderingHolder(ItemReminderBinding.inflate(LayoutInflater.from(parent.context),
                parent,
                false))
        }
        return RemainderingHolderRetail(ItemReminderRetailBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (viewType == VIEW_TYPE_ONE) {
            (holder as RemainderingHolder).bind()
        } else {
            (holder as RemainderingHolderRetail).bind()
        }

    }

    inner class RemainderingHolder(var binding: ItemReminderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]
            if (model.latestReminder?.isNotEmpty() == true) {
                binding.tvScheduledDate.visibility = View.VISIBLE
                binding.tvSendReminder.text =
                    binding.tvSendReminder.context.getString(R.string.send_again)
                if (model.scheduled_at.isNullOrEmpty()) {
                    val lastReminderText =
                        binding.tvScheduledDate.context.getString(R.string.last_reminder) + " - " + CommonUtils.formatDateNormal(
                            model.latestReminder)
                    binding.tvScheduledDate.text = lastReminderText

                    binding.tvScheduled.text =
                        binding.tvScheduled.context.getString(R.string.schedule)
                    binding.tvScheduled.setTextColor(binding.tvScheduled.context.getColor(R.color.counter_bg))
                    binding.btnScheduled.background =
                        CommonUtils.getDrawable(binding.btnScheduled.context,
                            R.drawable.bg_sky_stroke)
                    binding.tvScheduled.setCompoundDrawablesWithIntrinsicBounds(CommonUtils.getDrawable(
                        binding.tvScheduled.context,
                        R.drawable.ic_event__sky_blue), null, null, null)

                } else {
                    binding.tvSendReminder.text =
                        binding.tvSendReminder.context.getString(R.string.send_reminder)
                    val scheduledText =
                        binding.tvSendReminder.context.getString(R.string.scheduled) + " - " + CommonUtils.formatDateNormal(
                            model.scheduled_at)
                    binding.tvScheduledDate.text = scheduledText

                    binding.btnScheduled.background =
                        CommonUtils.getDrawable(binding.btnScheduled.context,
                            R.drawable.bg_green_stroke)
                    binding.tvScheduled.text =
                        binding.tvScheduled.context.getString(R.string.scheduled)
                    binding.tvScheduled.setTextColor(binding.tvScheduled.context.getColor(R.color.status_completed))
                    binding.tvScheduled.setCompoundDrawablesWithIntrinsicBounds(CommonUtils.getDrawable(
                        binding.tvScheduled.context,
                        R.drawable.ic_check_circle_white), null, null, null)
                }
            } else {
                binding.tvScheduledDate.makeVisible(false)
            }

            ImageLoader.loadImage(binding.rlReminderImage, model.car_icon)
            binding.tvCarMake.text = model.carName
            binding.tvVisitDate.text = CommonUtils.formatDateNormal(model.createdAt)
            binding.tvCustomerName.text = model.customerName
            binding.tvPhone.text = model.mobile
            if (CommonUtils.isBeforeDate(restrictionDate, (model.latestReminder ?: ""))) {
                binding.btnSendReminder.alpha = .5f
                binding.btnSendReminder.isEnabled = false
            } else {
                binding.btnSendReminder.alpha = 1.0f
                binding.btnSendReminder.isEnabled = true
            }
            binding.btnSendReminder.setOnClickListener {
                binding.btnSendReminder.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            binding.btnScheduled.setOnClickListener {
                binding.btnScheduled.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }


    }

    inner class RemainderingHolderRetail(var binding: ItemReminderRetailBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind() {

            val model = dataList[layoutPosition]

            if (!model.latestReminder.isNullOrEmpty()) {
                binding.tvScheduledDate.visibility = View.VISIBLE
                binding.tvSendReminder.text =
                    binding.tvSendReminder.context.getString(R.string.send_again)
                if (model.scheduled_at.isNullOrEmpty()) {
                    binding.tvScheduledDate.text =
                        binding.tvScheduledDate.context.getString(R.string.last_reminder) +
                    " - " + CommonUtils.formatDateNormal(model.latestReminder)
                    binding.tvScheduled.text =
                        binding.tvScheduled.context.getString(R.string.schedule)
                    binding.tvScheduled.setTextColor(binding.tvScheduled.context.getColor(R.color.counter_bg))
                    binding.btnScheduled.background =
                        CommonUtils.getDrawable(binding.btnScheduled.context,
                            R.drawable.bg_sky_stroke)
                    binding.tvScheduled.setCompoundDrawablesWithIntrinsicBounds(CommonUtils.getDrawable(
                        binding.tvScheduled.context,
                        R.drawable.ic_event__sky_blue), null, null, null)

                } else {
                    binding.tvSendReminder.text =
                        binding.tvSendReminder.context.getString(R.string.send_reminder)
                    binding.tvScheduledDate.text =
                        binding.tvScheduledDate.context.getString(R.string.scheduled) +
                    " - " + CommonUtils.formatDateNormal(model.scheduled_at)
                    binding.btnScheduled.background =
                        CommonUtils.getDrawable(binding.btnScheduled.context,
                            R.drawable.bg_green_stroke)
                    binding.tvScheduled.text =
                        binding.tvScheduled.context.getString(R.string.scheduled)
                    binding.tvScheduled.setTextColor(binding.tvScheduled.context.getColor(R.color.status_completed))
                    binding.tvScheduled.setCompoundDrawablesWithIntrinsicBounds(CommonUtils.getDrawable(
                        binding.tvScheduled.context,
                        R.drawable.ic_check_circle_white), null, null, null)
                }
            } else {
                binding.tvScheduledDate.makeVisible(false)
            }
            binding.tvVisitDate.text = CommonUtils.formatDateNormal(model.createdAt)
            binding.tvName.text = model.customerName
            binding.tvPhone.text = model.mobile
            if (CommonUtils.isBeforeDate(restrictionDate, (model.latestReminder ?: ""))) {
                binding.btnSendReminder.alpha = .5f
                binding.btnSendReminder.isEnabled = false
            } else {
                binding.btnSendReminder.alpha = 1.0f
                binding.btnSendReminder.isEnabled = true
            }
            binding.btnSendReminder.setOnClickListener {
                binding.btnSendReminder.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            binding.btnScheduled.setOnClickListener {
                binding.btnScheduled.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }


    }

    fun getAdapterList(): List<RemindersItem> {
        return dataList
    }

    fun addItemToAdapterList(item: RemindersItem) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}