package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.CountryCodeResponseModel
import whitelisted.garage.databinding.ItemCountryCodeBinding
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class CountryFlagAdapter(val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<CountryFlagAdapter.MyViewHolder>() {

    private var dataList = mutableListOf<CountryCodeResponseModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            ItemCountryCodeBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = dataList[position]
        holder.bind(data)
    }

    override fun getItemCount(): Int = dataList.size

    inner class MyViewHolder(var binding: ItemCountryCodeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(data: CountryCodeResponseModel) {

            ImageLoader.loadImage(binding.imgCountryFlag,
                data.image,
                placeholderImage = R.drawable.ic_placeholder_doc)
            binding.tvCountryName.text = "${data.name} (${data.dialCode})"
            if (data.isSelected == true) {
                binding.imgCheck.makeVisible(true)
                CommonUtils.getCustomFontTypeface(binding.tvCountryName.context,
                    R.font.gilroy_semibold)?.let {
                    binding.tvCountryName.typeface = it
                }
            } else {
                binding.imgCheck.makeVisible(false)
                CommonUtils.getCustomFontTypeface(binding.tvCountryName.context,
                    R.font.gilroy_medium)?.let {
                    binding.tvCountryName.typeface = it
                }
            }
            binding.clBase.setOnClickListener {
                binding.clBase.setTag(R.id.clBase, data)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: MutableList<CountryCodeResponseModel>) {
        dataList.clear()
        dataList.addAll(list)
        notifyDataSetChanged()
    }

    fun getAdapterData(): MutableList<CountryCodeResponseModel> {
        return dataList
    }
}
