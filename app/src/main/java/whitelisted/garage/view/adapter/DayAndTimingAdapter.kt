package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_days_timing.view.*
import whitelisted.garage.R
import whitelisted.garage.api.request.DayTimeData
import whitelisted.garage.api.request.StartAndEndTimeData


class DayAndTimingAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var daysNameList = mutableListOf<String>()
    private var timeData = mutableListOf<StartAndEndTimeData>()
    private var data: DayTimeData? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TimingViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_days_timing, parent, false))
    }

    override fun getItemCount(): Int = daysNameList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as TimingViewHolder).bind()
    }

    inner class TimingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            try {
                val time = timeData[layoutPosition]
                itemView.tvDays.text = daysNameList[layoutPosition]
                itemView.tvStartTime.text = time.startTime
                itemView.tvEndTime.text = time.endTime
            } catch (e: Exception) { //
            }
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    fun setDataToAdapter(dayTimeData: DayTimeData) {
        data = null
        daysNameList.clear()
        timeData.clear()
        data = dayTimeData
        for (i in 0 until 7) {
            if (dayTimeData.timeList[i].isEnable) {
                data?.timeList?.get(i)?.let { timeData.add(it) }
                data?.timeList?.get(i)?.let { daysNameList.add(it.days) }
            }
        }
        notifyDataSetChanged()
    }

    fun getAdapterData(): DayTimeData {
        return DayTimeData(timeData)
    }

}