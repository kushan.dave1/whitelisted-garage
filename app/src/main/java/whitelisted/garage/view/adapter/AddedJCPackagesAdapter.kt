package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_added_packages.view.*
import whitelisted.garage.R
import whitelisted.garage.api.request.JobCardPackageModel
import whitelisted.garage.utils.AppENUM

class AddedJCPackagesAdapter(var dataList: MutableList<JobCardPackageModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PackageItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_added_packages, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PackageItemViewHolder).bind()
    }

    inner class PackageItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList[layoutPosition]
            itemView.tvSelPackage.text = model.packageModel?.name
            if (itemView.rvInclusives.layoutManager == null) {
                itemView.rvInclusives.layoutManager =
                    LinearLayoutManager(itemView.rvInclusives.context)
            }
            itemView.rvInclusives.adapter =
                PackageInclusiveAdapter(model.inclusivesList ?: mutableListOf())
            itemView.imgDelSelPackage.setOnClickListener {
                itemView.imgDelSelPackage.setTag(R.id.model, AppENUM.AdaptersConstant.DEL_PACKAGE)
                itemView.imgDelSelPackage.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }

    }

    fun getAdapterList(): List<JobCardPackageModel> {
        return dataList
    }

    fun addItemToAdapterList(item: JobCardPackageModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}