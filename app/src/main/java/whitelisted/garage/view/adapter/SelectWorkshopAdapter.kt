package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_workshop_list.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.databinding.CardPlpBannerBinding
import whitelisted.garage.databinding.ItemLockedWorkshopItemBinding
import whitelisted.garage.databinding.ItemSsItemBinding
import whitelisted.garage.databinding.ItemWorkshopListBinding
import whitelisted.garage.databinding.ItemWorkshopLockedBannerBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible

class SelectWorkshopAdapter(val dataList: MutableList<WorkshopListResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val ENABLED_ITEM = 1
        const val BANNER = 2
        const val DISABLED_ITEM = 3
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            dataList[position].workshopId.isNullOrEmpty()-> BANNER
            dataList[position].isLocked == true -> DISABLED_ITEM
            else -> ENABLED_ITEM
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ENABLED_ITEM) WorkshopItemViewHolder(ItemWorkshopListBinding.inflate(LayoutInflater.from(
            parent.context), parent, false))
        else if (viewType== DISABLED_ITEM) LockedWorkshopItemViewHolder(ItemLockedWorkshopItemBinding.inflate(LayoutInflater.from(
            parent.context), parent, false))
        else BannerViewHolder(ItemWorkshopLockedBannerBinding.inflate(LayoutInflater.from(
            parent.context), parent, false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is BannerViewHolder -> holder.bind(position)
            is WorkshopItemViewHolder -> {
                holder.bind(position)
            }
            is LockedWorkshopItemViewHolder -> {
                holder.bind(position)
            }
        }
    }

    inner class WorkshopItemViewHolder(var binding: ItemWorkshopListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {

            val model = dataList[position]
            binding.tvShopName.text = model.name ?: ""
            when (model.types) {
                AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                    binding.tvShopStatus.text =
                        binding.tvShopStatus.context.getString(R.string.accessories_shop)

                    binding.tvShopStatus.setTextColor(CommonUtils.getColor(binding.tvShopStatus.context,
                        R.color.text_acc_type))
                    binding.tvShopStatus.background =
                        CommonUtils.getDrawable(binding.tvShopStatus.context,
                            R.drawable.bg_acc_type)
                }
                AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                    binding.tvShopStatus.text =
                        binding.tvShopStatus.context.getString(R.string.spares_shop)
                    binding.tvShopStatus.setTextColor(CommonUtils.getColor(binding.tvShopStatus.context,
                        R.color.text_retailer_type))
                    binding.tvShopStatus.background =
                        CommonUtils.getDrawable(binding.tvShopStatus.context,
                            R.drawable.bg_retail_type)
                }
                else -> {
                    binding.tvShopStatus.text =
                        binding.tvShopStatus.context.getString(R.string.workshop)
                    binding.tvShopStatus.setTextColor(CommonUtils.getColor(binding.tvShopStatus.context,
                        R.color.text_workshop_type))
                    binding.tvShopStatus.background =
                        CommonUtils.getDrawable(binding.tvShopStatus.context,
                            R.drawable.bg_workshop_type)
                }
            }

            if (model.membershipStatus == true){
                binding.clPro.makeVisible(true)
            }else binding.clPro.makeVisible(false)

            if (layoutPosition == dataList.size - 1) {
                binding.viewDivider.makeVisible(false)
            } else binding.viewDivider.makeVisible(true)

            if (model.workshopRole.isNullOrEmpty()) {
                binding.tvRole.makeVisible(false)
            } else {
                binding.tvRole.makeVisible(true)
                binding.tvRole.text = model.workshopRole
                when (model.workshopRole) {
                    AppENUM.EMPROLE.DRIVER -> {
                        binding.tvRole.setViewBackgroundColor(R.color.bg_role_driver)
                        binding.tvRole.setTextColor(binding.tvRole.context.getColor(R.color.driver_color))
                    }
                    AppENUM.EMPROLE.MECHANIC -> {
                        binding.tvRole.setViewBackgroundColor(R.color.bg_role_mechanic)
                        binding.tvRole.setTextColor(binding.tvRole.context.getColor(R.color.mechanic_color))
                    }
                    else -> {
                        binding.tvRole.setViewBackgroundColor(R.color.bg_role_owner_sup)
                        binding.tvRole.setTextColor(binding.tvRole.context.getColor(R.color.owner_sup_color))
                    }
                }
            }

            binding.rlShop.setOnClickListener {
                binding.rlShop.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }


    }

    inner class BannerViewHolder(var binding: ItemWorkshopLockedBannerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {

            val model = dataList[position]
            binding.clBaseISLB.setOnClickListener{
                recyclerItemClickListener.onClick(it)
            }

        }
    }

    inner class LockedWorkshopItemViewHolder(var binding: ItemLockedWorkshopItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {

            val model = dataList[position]
            binding.tvShopName.text = model.name ?: ""
            when (model.types) {
                AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT -> {
                    binding.tvShopStatus.text =
                        binding.tvShopStatus.context.getString(R.string.accessories_shop)

//                    binding.tvShopStatus.setTextColor(CommonUtils.getColor(binding.tvShopStatus.context,
//                        R.color.text_acc_type))
//                    binding.tvShopStatus.background =
//                        CommonUtils.getDrawable(binding.tvShopStatus.context,
//                            R.drawable.bg_acc_type)
                }
                AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                    binding.tvShopStatus.text =
                        binding.tvShopStatus.context.getString(R.string.spares_shop)
//                    binding.tvShopStatus.setTextColor(CommonUtils.getColor(binding.tvShopStatus.context,
//                        R.color.text_retailer_type))
//                    binding.tvShopStatus.background =
//                        CommonUtils.getDrawable(binding.tvShopStatus.context,
//                            R.drawable.bg_retail_type)
                }
                else -> {
                    binding.tvShopStatus.text =
                        binding.tvShopStatus.context.getString(R.string.workshop)
//                    binding.tvShopStatus.setTextColor(CommonUtils.getColor(binding.tvShopStatus.context,
//                        R.color.text_workshop_type))
//                    binding.tvShopStatus.background =
//                        CommonUtils.getDrawable(binding.tvShopStatus.context,
//                            R.drawable.bg_workshop_type)
                }
            }
            if (layoutPosition == dataList.size - 1) {
                binding.viewDivider.makeVisible(false)
            } else binding.viewDivider.makeVisible(true)

            if (model.workshopRole.isNullOrEmpty()) {
                binding.tvRole.makeVisible(false)
            } else {
                binding.tvRole.makeVisible(true)
                binding.tvRole.text = model.workshopRole
//                when (model.workshopRole) {
//                    AppENUM.EMPROLE.DRIVER -> {
//                        binding.tvRole.setViewBackgroundColor(R.color.bg_role_driver)
//                        binding.tvRole.setTextColor(binding.tvRole.context.getColor(R.color.driver_color))
//                    }
//                    AppENUM.EMPROLE.MECHANIC -> {
//                        binding.tvRole.setViewBackgroundColor(R.color.bg_role_mechanic)
//                        binding.tvRole.setTextColor(binding.tvRole.context.getColor(R.color.mechanic_color))
//                    }
//                    else -> {
//                        binding.tvRole.setViewBackgroundColor(R.color.bg_role_owner_sup)
//                        binding.tvRole.setTextColor(binding.tvRole.context.getColor(R.color.owner_sup_color))
//                    }
//                }
            }
        }


    }

}