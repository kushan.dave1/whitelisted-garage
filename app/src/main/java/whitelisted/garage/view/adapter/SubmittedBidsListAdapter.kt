package whitelisted.garage.view.adapter

import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_submitted_bid.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.AccRetailBidListResponseModelSubmitAccept
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.changeBackgroundDrawable
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.changeViewCompoundDrawablesWithInterinsicBounds
import whitelisted.garage.utils.CommonUtils.makeVisible

class SubmittedBidsListAdapter(val shopId: String,
    val currencySymbol: String,
    var dataList: MutableList<AccRetailBidListResponseModelSubmitAccept>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AddPartItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_submitted_bid, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AddPartItemViewHolder).bind()
    }

    inner class AddPartItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataList[layoutPosition]
            itemView.tvShopName?.text = model.bidingCart?.workshopName
            itemView.tvOrderId?.text = model.bidId

            model.distance?.let {
                "$it ${itemView.tvDistance.context.getString(R.string.km_away)}".apply {
                    itemView.tvDistance?.text = this
                }
            }
            if (model.bidingCart?.isContactNumber?.contains(shopId) == true) {
                itemView.clgMobileMasked.makeVisible(false)
                itemView.tvPhoneNumberISB.makeVisible(true)
                itemView.tvPhoneNumberISB?.text = model.bidingCart.contactNumber
            } else {
                itemView.clgMobileMasked.makeVisible(true)
                itemView.tvPhoneNumberISB.makeVisible(false)
                var maskedPhone: String?
                val initialFour = model.bidingCart?.contactNumber?.substring(0, 4)
                val noOfStars = (model.bidingCart?.contactNumber?.length ?: 10).minus(4)
                maskedPhone = initialFour
                if (noOfStars > 0) {
                    for (i in 1..(noOfStars)) {
                        maskedPhone += "*"
                    }
                }
                itemView.tvPhoneNumberMasked?.text = maskedPhone
            }
            itemView.tvDate?.text = CommonUtils.convertToDate(model.createdAt)
            itemView.tvTime?.text = CommonUtils.convertToTime(model.createdAt)
            itemView.tvTotalParts?.text = model.totalItem.toString()
            "$currencySymbol${model.totalAmount}".apply {
                itemView.tvTotalPrice?.text = this
            }

            val itemsText = StringBuilder()
            var i = 0
            var countMoreItemsText = ""
            kotlin.run breaking@{
                model.items?.forEach {
                    if (model.isItemTextExpanded == false) {
                        if (model.items.size > 3) {
                            if (i < 3) {
                                itemsText.append(it).append(" | ")
                                itemView.tvItems?.text =
                                    itemsText.substring(0, itemsText.length - 3)

                                i++
                            } else {
                                countMoreItemsText = "+ ${model.items.size.minus(i)} ${
                                    itemView.tvItems.context.getString(R.string.more_items_sc)
                                }"
                                "${itemView.tvItems.text} $countMoreItemsText".apply {
                                    itemView.tvItems?.text = this
                                }
                                return@breaking
                            }
                        } else {
                            itemsText.append(it).append(" | ")
                            itemView.tvItems?.text = itemsText.substring(0, itemsText.length - 3)

                            i++
                        }
                    } else {
                        itemsText.append(it).append(" | ")
                        itemView.tvItems?.text = itemsText.substring(0, itemsText.length - 3)

                        i++
                    }
                }
            }
            if ((model.items?.size ?: 0) > 3 && model.isItemTextExpanded == false) {
                val spannable = SpannableString(itemView.tvItems.text)
                itemView.tvItems?.text = getClickableTextWithColor(spannable,
                    countMoreItemsText,
                    ContextCompat.getColor(itemView.tvItems.context, R.color.colorAccent),
                    object : (View) -> Unit {
                        override fun invoke(p1: View) {
                            model.isItemTextExpanded = true
                            itemView.tvItems.setTag(R.id.tvItems, layoutPosition)
                            recyclerItemClickListener.onClick(itemView.tvItems)
                        }
                    })
                itemView.tvItems?.movementMethod = LinkMovementMethod()
            } else { // do nothing
            }

            if (model.statusId != 7) {
                itemView.tvInfoISB.changeTextColor(itemView.tvInfoISB.context,
                    R.color.ic_text_dark_chestnut)
                itemView.tvInfoISB.changeBackgroundDrawable(itemView.tvInfoISB.context,
                    R.drawable.bg_submitted_waiting)
                itemView.tvInfoISB.changeViewCompoundDrawablesWithInterinsicBounds(drawableStart = ContextCompat.getDrawable(
                    itemView.tvInfoISB.context,
                    R.drawable.ic_waiting_hourglass))
                itemView.tvInfoISB.text =
                    itemView.tvInfoISB.context.getString(R.string.bid_has_been_submitted_waiting_for_store_s_acceptance)
            } else {
                itemView.tvInfoISB.changeTextColor(itemView.tvInfoISB.context, R.color.alert_red_2)
                itemView.tvInfoISB.changeBackgroundDrawable(itemView.tvInfoISB.context,
                    R.drawable.bg_submitted_cancelled)
                itemView.tvInfoISB.changeViewCompoundDrawablesWithInterinsicBounds(drawableStart = ContextCompat.getDrawable(
                    itemView.tvInfoISB.context,
                    R.drawable.ic_cancel_red))
                itemView.tvInfoISB.text =
                    itemView.tvInfoISB.context.getString(R.string.bid_has_been_rejected)
            }

            itemView.tvPhoneNumberISB.setOnClickListener {
                it.setTag(R.id.tvPhoneNumberISB, model)
                recyclerItemClickListener.onClick(it)
            }

            itemView.llUnlockMobileISB.setOnClickListener {
                it.setTag(R.id.llUnlockMobileISB, model)
                recyclerItemClickListener.onClick(it)
            }

        }
    }

    fun getAdapterList(): List<AccRetailBidListResponseModelSubmitAccept> {
        return dataList
    }

    fun addItemToAdapterList(item: AccRetailBidListResponseModelSubmitAccept) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

    fun getClickableTextWithColor(spannable: Spannable,
        clickableText: String,
        @ColorInt color: Int,
        onClickListener: (View) -> Unit): Spannable {
        val text = spannable.toString()

        val start = text.indexOf(clickableText)
        spannable.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                onClickListener.invoke(widget)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.color = color
                ds.isUnderlineText = false
            }
        }, start, start + clickableText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return spannable
    }

}