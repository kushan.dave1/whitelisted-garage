package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_billing.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.BillingResponseModel
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeInVisible

class BillingListAdapter(val currencySymbol: String,
    var dataList: MutableList<BillingResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BillingItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_billing, parent, false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as BillingItemViewHolder).bind()
    }

    inner class BillingItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind() {

            val model = dataList[layoutPosition]
            if (model.paymentStatus != null && model.paymentStatus.equals(AppENUM.UNPAID,
                    ignoreCase = true)) {
                itemView.tvPaymentStatus.text = model.paymentStatus
                itemView.tvPaymentStatus.setTextColor(itemView.tvPaymentStatus.context.getColor(R.color.red_text_color))
            } else if (model.paymentStatus != null && model.paymentStatus.equals(AppENUM.PAID,
                    ignoreCase = true)) {
                itemView.tvPaymentStatus.text = model.paymentStatus
                itemView.tvPaymentStatus.setTextColor(itemView.tvPaymentStatus.context.getColor(R.color.green_new))
            } else {
                itemView.tvPaymentStatus.text = ""
            }
            itemView.tvName.text = model.name
            itemView.tvCarReg.text = model.carName + " - " + model.registrationNumber
            if (model.carName.isNullOrEmpty()) {
                itemView.tvCarReg.makeInVisible(true)
            } else {
                itemView.tvCarReg.makeInVisible(false)

            }
            itemView.tvCreatedDate.text = CommonUtils.convertDateWithDayNOTime(model.createdAt)
            itemView.tvDeliveredDate.text = CommonUtils.convertDateWithDayNOTime(model.deliveredAt)
            itemView.tvAmount.text =
                currencySymbol + CommonUtils.convertDoubleTo2DecimalPlaces(model.invoiceAmount
                    ?: 0.0)
            itemView.btnDownloadPdf.setOnClickListener {
                itemView.btnDownloadPdf.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            itemView.clBaseBuyGC.setOnClickListener {
                itemView.clBaseBuyGC.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }

        }
    }

    fun getAdapterList(): List<BillingResponseModel> {
        return dataList
    }

    fun addItemToAdapterList(item: BillingResponseModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }
}
