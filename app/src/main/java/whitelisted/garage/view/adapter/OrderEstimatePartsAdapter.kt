package whitelisted.garage.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_order_estimate_part.view.*
import whitelisted.garage.R
import whitelisted.garage.api.request.OEServicesItem
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible

class OrderEstimatePartsAdapter(val currencySymbol: String,
    val isAcc: Boolean,
    val context: Context,
    var dataList: MutableList<OEServicesItem>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return OrderEstimatePartsItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_order_estimate_part, parent, false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as OrderEstimatePartsItemViewHolder).bind()
    }

    inner class OrderEstimatePartsItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataList[layoutPosition]

            if (layoutPosition == dataList.size - 1) {
                itemView.viewDivider.makeVisible(false)
            } else itemView.viewDivider.makeVisible(true)

            if (model.type.equals("inventory")) {
                itemView.tvInventoryTag.makeVisible(true)
            } else itemView.tvInventoryTag.makeVisible(false)
            itemView.tvPartName.text = model.serviceName
            if (isAcc) {
                itemView.tvWorkDone.makeVisible(false)
            } else {
                itemView.tvWorkDone.makeVisible(true)
                itemView.tvWorkDone.text = model.workDoneName
            }
            val ppiText = "$currencySymbol${
                CommonUtils.convertDoubleTo2DecimalPlaces(model.pricePerQuantity ?: 0.0)
            }"
            itemView.tvPricePerItem.text = ppiText

            itemView.tvQuantity.text = "${model.quantity ?: 1}"
            itemView.tvTaxRate.text = model.taxRate ?: "0%"
            val totalText = "$currencySymbol${
                CommonUtils.convertDoubleTo2DecimalPlaces(model.total ?: 0.0)
            }"
            itemView.tvTotal.text = totalText
            itemView.imgDelPart.setOnClickListener {
                itemView.imgDelPart.setTag(R.id.model, AppENUM.AdaptersConstant.ADDED_PART)
                itemView.imgDelPart.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            itemView.imgEditPart.setOnClickListener {
                itemView.imgEditPart.setTag(R.id.model, AppENUM.AdaptersConstant.ADDED_PART)
                itemView.imgEditPart.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            itemView.tvWorkDone.setOnClickListener {
                itemView.tvWorkDone.setTag(R.id.model, AppENUM.AdaptersConstant.ADDED_PART)
                itemView.tvWorkDone.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<OEServicesItem> {
        return dataList
    }

    fun addItemToAdapterList(item: OEServicesItem) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}