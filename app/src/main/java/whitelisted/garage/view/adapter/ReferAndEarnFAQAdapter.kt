package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_go_coins_faq.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.Faq
import whitelisted.garage.utils.CommonUtils.makeVisible

class ReferAndEarnFAQAdapter(var dataList: List<Faq>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return GCFAQItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_go_coins_faq, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as GCFAQItemViewHolder).bind()
    }

    inner class GCFAQItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataList[layoutPosition]
            itemView.tvQuestion.text = model.question
            itemView.tvAnswer.text = model.answer
            if (model.isSelected == true) {
                itemView.llAnswer.makeVisible(true)
                itemView.imgToggle.setImageResource(R.drawable.ic_arrow_up_black)
            } else {
                itemView.llAnswer.makeVisible(false)
                itemView.imgToggle.setImageResource(R.drawable.ic_arrow_down_black)
            }
            itemView.clBase.setOnClickListener {
                itemView.clBase.setTag(R.id.clBase, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }

    }

    fun getAdapterList(): List<Faq> {
        return dataList
    }


}