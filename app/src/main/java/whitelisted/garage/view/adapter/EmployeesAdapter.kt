package whitelisted.garage.view.adapter

import DateUtil
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import coil.transform.CircleCropTransformation
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.response.EmployeeListResponseModel
import whitelisted.garage.databinding.ItemEmployeeBinding
import whitelisted.garage.databinding.ManageEmployeesTopCardBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeInVisible
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader
import kotlin.random.Random

class EmployeesAdapter(var dataListResponseModel: MutableList<EmployeeListResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), AdapterView.OnItemSelectedListener {

    private var spinnerSelectedpos = 0
    private var spinnerView: AppCompatTextView? = null
    private var workShopName = ""
    private val profilePicPlaceholders = listOf(R.drawable.ic_avatar_1,
        R.drawable.ic_avatar_2,
        R.drawable.ic_avatar_3,
        R.drawable.ic_avatar_4,
        R.drawable.ic_avatar_6,
        R.drawable.ic_avatar_7)

    private var isSpinnerActive: Boolean = false
    private var storageRef: StorageReference? = null
    private var storage: FirebaseStorage? =
        FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == 0)
            EmployeeHeaderViewHolder(ManageEmployeesTopCardBinding
                .inflate(LayoutInflater.from(parent.context), parent, false))

        else EmployeeItemViewHolder( ItemEmployeeBinding
            .inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemViewType(position: Int) = position

    override fun getItemCount(): Int = dataListResponseModel.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(position != 0) (holder as EmployeeItemViewHolder).bind(position-1)
        else (holder as EmployeeHeaderViewHolder).bind()
    }

    inner class EmployeeItemViewHolder(private val binding: ItemEmployeeBinding)
        : RecyclerView.ViewHolder(binding.root) {

        fun bind(layoutPosition:Int) = binding.apply {

            val employee = dataListResponseModel[layoutPosition]

            itemView.clipToOutline = true
            btnMore.setOnClickListener {
                btnMore.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            tvEmployeeName.text = employee.name

            tvNumber.text = employee.mobile
            tvEID.text = employee.userId.toString()

            employee.userRole.let { role ->
                tvRole.text = role
                when(role){
                    "Owner" -> {
                        tvRole.background =
                            CommonUtils.getDrawable(root.context,R.drawable.item_owner_bg)
                        tvRole
                            .setTextColor(CommonUtils.getColor(root.context,R.color.owner_color))
                    }

                    "Supervisor" -> {
                        tvRole.background = CommonUtils.getDrawable(root.context,R.drawable.item_supervisor_bg)
                        tvRole.setTextColor(CommonUtils.getColor(root.context,R.color.supervisor_color))
                    }

                    "Driver" -> {
                        tvRole.background = CommonUtils.getDrawable(root.context,R.drawable.item_driver_bg)
                        tvRole.setTextColor(CommonUtils.getColor(root.context,R.color.driver_color))
                    }

                    "Mechanic" -> {
                        tvRole.background = CommonUtils.getDrawable(root.context,R.drawable.item_mechanic_bg)
                        tvRole.setTextColor(CommonUtils.getColor(root.context,R.color.mechanic_color))
                    }

                }
            }
            if (employee.email?.isNotEmpty() == true) {
                llEmail.makeVisible(true)
                tvEmail.text = employee.email
            } else {
                llEmail.makeInVisible(true)
            }
            if (employee.joiningDate?.isNotEmpty() == true) {
                lblDOJ.makeVisible(true)
                tvDOJ.makeVisible(true)
                val date: String = employee.joiningDate.let {
                    DateUtil.getFormattedDateUtcToGmt(AppENUM.DateUtilKey.TIME_FORMAT_NZ,
                        AppENUM.DDMMYYYY,
                        it)
                }
                tvDOJ.text = date
            } else {
                lblDOJ.makeInVisible(true)
                tvDOJ.makeInVisible(true)
            }
            storageRef = storage?.reference
            employee.image?.let {
                if (it.isNotEmpty()) {
                    storageRef?.child(it)?.let { sigRef ->
                        try {
                            ImageLoader.loadImageWithCallback(ivManageEmployee,
                                sigRef.path,
                                object : ImageLoader.ImageLoadCallback {
                                    override fun onSuccess() {
                                        ivManageEmployee.scaleType =
                                            ImageView.ScaleType.CENTER_CROP
                                    }

                                    override fun onError(throwable: Throwable) {
                                        ivManageEmployee.scaleType =
                                            ImageView.ScaleType.CENTER_INSIDE
                                        val random = Random.nextInt(profilePicPlaceholders.size)
                                        ImageLoader.loadDrawable(CommonUtils.getDrawable(ivManageEmployee.context,
                                            profilePicPlaceholders[random]),
                                            ivManageEmployee)
                                    }
                                },
                                transform = CircleCropTransformation(),
                                placeholderImage = R.drawable.ic_add_profile_pic)
                        } catch (e: Exception) {
                            e.printStackTrace()

                            val random = Random.nextInt(profilePicPlaceholders.size)
                            ImageLoader.loadDrawable(CommonUtils.getDrawable(ivManageEmployee.context,
                                profilePicPlaceholders[random]), ivManageEmployee)
                        }
                    }
                } else {
                    val random = Random.nextInt(profilePicPlaceholders.size)
                    ImageLoader.loadDrawable(CommonUtils.getDrawable(ivManageEmployee.context,
                        profilePicPlaceholders[random]), ivManageEmployee)
                }
            }
            btnViewPerformance.setOnClickListener{
                it.tag = employee.userId.toString()
                recyclerItemClickListener.onClick(it)
            }

            btnCall.setOnClickListener {
                Intent(Intent.ACTION_DIAL).apply {
                    this.data = Uri.parse("tel:${employee.mobile}")
                    root.context.startActivity(this)
                }
            }
        }
    }

    inner class EmployeeHeaderViewHolder(private val binding: ManageEmployeesTopCardBinding)
                                        :RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("ClickableViewAccessibility")
        fun bind() {
            spinnerView = binding.workshopAutotv
            binding.llAddEmployee.setOnClickListener(recyclerItemClickListener)
            binding.tvTotalEmpCount.text = (dataListResponseModel.size).toString()
            binding.workshopAutotv.text = workShopName

            binding.workshopAutotv.setOnTouchListener { _, _ ->
                isSpinnerActive = true
                false
            }
            binding.workshopAutotv.setOnClickListener(recyclerItemClickListener)
        }
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        if (isSpinnerActive) spinnerView?.let {
            spinnerSelectedpos = position
            isSpinnerActive = false
            it.tag = position
            recyclerItemClickListener.onClick(it)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) { //
    }

    fun setWorkshopName(workshopName:String?) {
        workShopName = workshopName?:""

    }
}