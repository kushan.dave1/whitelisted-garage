package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.ServiceResponseModel
import whitelisted.garage.databinding.CardAddServiceAsOwnBinding
import whitelisted.garage.databinding.ItemPartBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils.makeVisible

class PartsAdapter(var dataList: MutableList<ServiceResponseModel>, val searchText: String,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        return if(viewType == 1)
        PartItemViewHolder(ItemPartBinding.inflate(inflater, parent, false))
        else AddServiceByOwnHolder(CardAddServiceAsOwnBinding.inflate(inflater,parent,false))

    }

    override fun getItemViewType(position: Int): Int  = if(dataList.isEmpty()) 0 else 1

    override fun getItemCount(): Int = if(dataList.size > 0) dataList.size else 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(dataList.isNotEmpty()) (holder as PartItemViewHolder).bind()
    }

    inner class PartItemViewHolder(private val binding: ItemPartBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]
            binding.tvPartName.text = model.name
            binding.tvPartDesc.text = model.description
            binding.clPart.setOnClickListener {
                binding.clPart.setTag(R.id.model, AppENUM.AdaptersConstant.PART)
                binding.clPart.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
            binding.btnDeleteCustomPart.makeVisible((model.status ?: 1 ) == 99)
            binding.btnDeleteCustomPart.setOnClickListener {
                it.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }

        }
    }

    inner class AddServiceByOwnHolder(binding: CardAddServiceAsOwnBinding): RecyclerView.ViewHolder(binding.root) {
        init {
            binding.tvSearchText.text = searchText
            binding.btnAddServiceByOwn.setOnClickListener(recyclerItemClickListener)
        }
    }

    fun getAdapterList(): List<ServiceResponseModel> {
        return dataList
    }

    fun addItemToAdapterList(item: ServiceResponseModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}