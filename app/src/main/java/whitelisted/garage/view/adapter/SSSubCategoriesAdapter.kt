package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.SubArray
import whitelisted.garage.api.response.SubCategory
import whitelisted.garage.databinding.ItemSubCategoryListBinding
import whitelisted.garage.utils.CommonUtils

class SSSubCategoriesAdapter(var dataList: List<SubCategory>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemFilterBrandViewHolder(ItemSubCategoryListBinding.inflate(LayoutInflater.from(
            parent.context), parent, false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemFilterBrandViewHolder).bind()
    }

    inner class ItemFilterBrandViewHolder(var binding: ItemSubCategoryListBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        fun bind() {

            val model = dataList[layoutPosition]

            binding.tvNameISCL.text = model.subCategory
            binding.rvSubSubCategorySCL.adapter = SSSubSubCategoriesAdapter(model.subArray, this)
        }


        override fun onClick(view: View?) {
            when (view?.id) {
                R.id.clBaseSSISSC -> {
                    CommonUtils.genericCastOrNull<SubArray>(view.getTag(R.id.model))
                        ?.let { subSubCat ->
                            view.setTag(R.id.model, subSubCat)
                            recyclerItemClickListener.onClick(view)
                        }
                }
            }
        }
    }

    fun getAdapterList(): List<SubCategory> {
        return dataList
    }

}