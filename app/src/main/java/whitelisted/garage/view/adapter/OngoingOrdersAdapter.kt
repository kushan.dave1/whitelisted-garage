package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_ongoing_order.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.DataItem
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.ImageLoader
import kotlin.random.Random

class OngoingOrdersAdapter(val currencySymbol: String,
    var dataList: MutableList<DataItem>,
    val recyclerItemClickListener: View.OnClickListener,
    val type: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val retailAccPlaceholders = listOf(R.drawable.ic_retail_pc_1,
        R.drawable.ic_retail_pc_2,
        R.drawable.ic_retail_pc_3,
        R.drawable.ic_retail_pc_4,
        R.drawable.ic_retail_pc_5,
        R.drawable.ic_retail_pc_6,
        R.drawable.ic_retail_pc_7,
        R.drawable.ic_retail_pc_8,
        R.drawable.ic_retail_pc_9)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return OngoingOrderItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_ongoing_order, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as OngoingOrderItemViewHolder).bind()
    }

    inner class OngoingOrderItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind() {
            val model = dataList[layoutPosition]
            when (type) {
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT -> {
                    itemView.tvRegNumber.text = model.registrationNumber
                    itemView.tvPersonName.text = model.name
                    ImageLoader.loadImage(itemView.imgCarImage, model.carIcon)
                }
                AppENUM.RefactoredStrings.ACCESSORIES_CONSTANT, AppENUM.RefactoredStrings.RETAILER_CONSTANT -> {
                    itemView.tvRegNumber.text = model.name
                    itemView.tvPersonName.text = model.orderId
                    val random = Random.nextInt(retailAccPlaceholders.size)
                    ImageLoader.loadDrawable(ContextCompat.getDrawable(itemView.imgCarImage.context,
                        retailAccPlaceholders[random]), itemView.imgCarImage)
                }
            }
            itemView.tvCost.text = CommonUtils.getString(itemView.tvCost.context,
                R.string.total) + " " + currencySymbol + CommonUtils.convertDoubleTo2DecimalPlaces(
                model.invoiceAmount ?: 0.0)
            itemView.tvDate.text = CommonUtils.formatDateNormal(model.createdAt)
            when (model.statusId) {
                AppENUM.RefactoredStrings.OPEN_ORDER_STATUS, AppENUM.RefactoredStrings.OPEN_ORDER_STATUS_2 -> {
                    itemView.tvOrderStatus.text =
                        CommonUtils.getString(itemView.tvOrderStatus.context, R.string.open_order)

                }
                AppENUM.RefactoredStrings.WIP_ORDER_STATUS -> {
                    itemView.tvOrderStatus.text =
                        CommonUtils.getString(itemView.tvOrderStatus.context,
                            R.string.work_in_progress)
                }
                AppENUM.RefactoredStrings.READY_ORDER_STATUS -> {
                    itemView.tvOrderStatus.text =
                        CommonUtils.getString(itemView.tvOrderStatus.context,
                            R.string.ready_for_delivery)
                }
                AppENUM.RefactoredStrings.COMPLETED_ORDER_STATUS -> {
                    itemView.tvOrderStatus.text =
                        CommonUtils.getString(itemView.tvOrderStatus.context, R.string.completed)
                }
            }
            itemView.clBaseBuyGC.setOnClickListener {
                itemView.clBaseBuyGC.setTag(R.id.position, layoutPosition)
                itemView.clBaseBuyGC.setTag(R.id.model, "")
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<DataItem> {
        return dataList
    }

    fun addItemToAdapterList(item: DataItem) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

}