package whitelisted.garage.view.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.FragmentFactory
import whitelisted.garage.view.fragments.newOrderDetail.NewOrderDetailFragment
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment
import whitelisted.garage.view.fragments.orderEstimate.OrderEstimatesFragment
import whitelisted.garage.view.fragments.payment.PaymentFragment

class OrderDetailTabLayoutAdapter(activity: FragmentActivity) : FragmentStateAdapter(activity) {

    private var fragments: MutableList<Fragment> = mutableListOf()

    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                Bundle().apply {
                    putBoolean(AppENUM.IntentKeysENUM.IS_UNDER_TAB, true)
                    putString(AppENUM.IntentKeysENUM.ORDER_ID, OrderDetailWrapperFragment.orderId)
                    (FragmentFactory.fragment(FragmentFactory.Fragments.NEW_ORDER_DETAIL,
                        this) as Fragment).let {
                        fragments.add(it)
                        return it
                    }
                }
            }
            1 -> {
                Bundle().apply {
                    putString(AppENUM.IntentKeysENUM.ORDER_ID, OrderDetailWrapperFragment.orderId)
                    (FragmentFactory.fragment(FragmentFactory.Fragments.ORDER_ESTIMATES,
                        this) as Fragment).let {
                        fragments.add(it)
                        return it
                    }
                }
            }
            2 -> {
                Bundle().apply {
                    putString(AppENUM.IntentKeysENUM.ORDER_ID, OrderDetailWrapperFragment.orderId)
                    (FragmentFactory.fragment(FragmentFactory.Fragments.PAYMENT_FRAGMENT,
                        this) as Fragment).let {
                        fragments.add(it)
                        return it
                    }
                }
            }
        }
        return FragmentFactory.fragment(FragmentFactory.Fragments.NEW_ORDER_DETAIL,
            null) as Fragment
    }

    fun updateFragment(position: Int) {
        try {
            val fragment: Fragment =
                fragments[position] // Check fragment type to make sure it is one we know has an updateView Method // Check fragment type to make sure it is one we know has an updateView Method
            when (fragment) {
                is NewOrderDetailFragment -> {
                    fragment.updateScreen()
                }
                is OrderEstimatesFragment -> {
                    fragment.updateScreen()
                }
                is PaymentFragment -> {
                    fragment.updateScreen()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}