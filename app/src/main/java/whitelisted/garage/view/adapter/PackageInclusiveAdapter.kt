package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_package_inclusive.view.*
import whitelisted.garage.R

class PackageInclusiveAdapter(var dataList: List<String>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PackageInclusiveItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_package_inclusive, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PackageInclusiveItemViewHolder).bind()
    }

    inner class PackageInclusiveItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val model = dataList[layoutPosition]
            itemView.tvInclusiveName.text = model
        }
    }

    fun getAdapterList(): List<String> {
        return dataList
    }

}