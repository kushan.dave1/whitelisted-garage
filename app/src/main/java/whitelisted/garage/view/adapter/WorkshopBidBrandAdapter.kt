package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.greenrobot.eventbus.EventBus
import whitelisted.garage.R
import whitelisted.garage.api.request.BrandWiseItem
import whitelisted.garage.databinding.ItemBidWsBrandBinding
import whitelisted.garage.eventBus.UpdateCartValueInBidEvent
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible

class WorkshopBidBrandAdapter(var list: MutableMap<String, MutableList<BrandWiseItem>>,
    var listOfKey: MutableList<String>,
    var isViewOnly: Boolean,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SettingViewHolder(ItemBidWsBrandBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = listOfKey.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SettingViewHolder).bind()
    }

    inner class SettingViewHolder(var view: ItemBidWsBrandBinding) :
        RecyclerView.ViewHolder(view.root), View.OnClickListener {
        fun bind() {
            view.rvBrandItems.adapter = list[listOfKey[layoutPosition]]?.let {
                WorkshopBidBrandSubAdapter(isViewOnly, it, this)
            }
            view.tvBrandName.text = listOfKey[layoutPosition]
            if (isViewOnly) {
                view.tvAddPart.makeVisible(false)
            }
            view.tvAddPart.setOnClickListener {
                view.tvAddPart.setTag(R.id.model, list[listOfKey[layoutPosition]])
                recyclerItemClickListener.onClick(it)
            }
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.imgWpMinuse -> {
                    CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let {
                        if (list[listOfKey[layoutPosition]].isNullOrEmpty()) {
                            list.remove(listOfKey[layoutPosition])
                            listOfKey.removeAt(layoutPosition)
                            notifyItemRemoved(layoutPosition)
                        }
                        EventBus.getDefault().post(UpdateCartValueInBidEvent())
                    }
                }
                R.id.imgWpPluse -> {
                    CommonUtils.genericCastOrNull<Int>(v.getTag(R.id.position))?.let {}
                }
                R.id.tvAddRemark -> {
                    recyclerItemClickListener.onClick(v)
                }
                R.id.img1, R.id.img2, R.id.img3, R.id.img4 -> {
                    recyclerItemClickListener.onClick(v)
                }
                R.id.tvMorePhotos -> {
                    recyclerItemClickListener.onClick(v)
                }
            }
        }
    }
}