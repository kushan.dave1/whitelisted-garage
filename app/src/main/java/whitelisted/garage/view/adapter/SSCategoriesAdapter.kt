package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.SSBrandsResponseModel
import whitelisted.garage.api.response.SSCategoryResponseModel
import whitelisted.garage.databinding.ItemSsCategoryBinding
import whitelisted.garage.utils.ImageLoader

class SSCategoriesAdapter(var dataList: MutableList<SSCategoryResponseModel>,
    val isAcc: Boolean,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SSBrandItemViewHolder(ItemSsCategoryBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SSBrandItemViewHolder).bind()
    }

    inner class SSBrandItemViewHolder(var binding: ItemSsCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]
            ImageLoader.loadImage(binding.imgCategoryImage, model.image)
            binding.tvCategoryName.text = model.name
            binding.clBaseISSC.setOnClickListener {
                it.setTag(R.id.model, model)
                it.setTag(R.id.type, isAcc)
                recyclerItemClickListener.onClick(it)
            }
        }
    }
}
