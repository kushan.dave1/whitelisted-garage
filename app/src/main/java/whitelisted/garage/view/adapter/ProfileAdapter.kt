package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_profile.view.*
import whitelisted.garage.R
import whitelisted.garage.api.request.ProfileItemData
import whitelisted.garage.api.request.ProfileNavigationData
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class ProfileAdapter(val recyclerItemClickListener: View.OnClickListener, var coutryCode: String) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var dataListItemModel: List<Int>? = null
    private var dataListConstant: List<String>? = null
    private var dataListDes: List<String>? = null
    private var image: List<Int>? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AccountItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_profile, parent, false))
    }

    override fun getItemCount(): Int = dataListItemModel?.size ?: 0
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AccountItemViewHolder).bind()
    }

    inner class AccountItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataListItemModel?.get(layoutPosition)
            model?.let {
                var desc = dataListDes?.get(layoutPosition)
                itemView.tvItemName.text = itemView.tvItemName.context.getString(it)
                if (desc.isNullOrEmpty()) {
                    desc = "-"
                    itemView.tvDescription.text = "-"
                    itemView.imageWarning.makeVisible(true)
                } else {
                    itemView.tvDescription.text = desc
                    itemView.imageWarning.makeVisible(false)
                }
                if (dataListConstant?.get(layoutPosition) == AppENUM.Profile.PHONE_NUMBER) {
                    "$coutryCode $desc".apply {
                        itemView.tvDescription.text = this
                    }
                }
                ImageLoader.loadDrawable(ContextCompat.getDrawable(itemView.imgAccountItem.context,
                    image?.get(layoutPosition) ?: 0), itemView.imgAccountItem)
                if (layoutPosition == (dataListItemModel?.size ?: 0) - 1) {
                    itemView.line.makeVisible(false)
                } else {
                    itemView.line.makeVisible(true)
                }
                if (model.let { it1 -> itemView.tvItemName.context.getString(it1) } == itemView.tvItemName.context.getString(
                        R.string.role)) {
                    itemView.r1.makeVisible(false)
                } else {
                    itemView.r1.makeVisible(true)
                    itemView.llParentView.setOnClickListener { view ->
                        val profile = ProfileNavigationData().apply {
                            this.constant = dataListConstant?.get(layoutPosition)
                            this.value = desc
                            this.header =
                                model.let { it1 -> itemView.llParentView.context.getString(it1) }
                        }
                        itemView.llParentView.setTag(R.id.llParentView, profile)
                        recyclerItemClickListener.onClick(view)
                    }
                }
            }
            if (coutryCode != AppENUM.RefactoredStrings.defaultCountryCode && dataListConstant?.get(
                    layoutPosition) == AppENUM.Profile.GSTN) {
                itemView.makeVisible(false)
            } else {
                itemView.makeVisible(true)
            }

        }


    }


    @SuppressLint("NotifyDataSetChanged")
    fun setData(data: ProfileItemData) {
        data.let {
            dataListConstant = data.dataListConstant
            dataListDes = data.dataListDes
            dataListItemModel = data.dataListHeader
            image = data.image
            notifyDataSetChanged()
        }
    }


}