package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import whitelisted.garage.R
import whitelisted.garage.api.data.IllustrationImage


class IllustrationsAdapter(private val sliderItems: MutableList<IllustrationImage>,
    private val viewPager2: ViewPager2) :
    RecyclerView.Adapter<IllustrationsAdapter.MyViewHolder>() {

    @SuppressLint("NotifyDataSetChanged")
    private val runnable = Runnable {
        sliderItems.addAll(sliderItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_illustration, parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.setImage(sliderItems[position])
        if (position == sliderItems.size - 2) {
            viewPager2.post(runnable)
        }
    }

    override fun getItemCount(): Int {
        return sliderItems.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val imageView: ImageView = itemView.findViewById(R.id.imgIllustration)
        fun setImage(sliderItems: IllustrationImage) {
            imageView.setImageResource(sliderItems.image)
        }
    }

}