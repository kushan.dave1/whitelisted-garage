package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.setPadding
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.OptionsItem
import whitelisted.garage.databinding.ItemPaymentOptionBinding
import whitelisted.garage.utils.CommonUtils.makeInVisible
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader

class PaymentOptionsAdapter(var dataList: List<OptionsItem>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var currentSelectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemFilterBrandViewHolder(ItemPaymentOptionBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemFilterBrandViewHolder).bind()
    }

    inner class ItemFilterBrandViewHolder(var binding: ItemPaymentOptionBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            val model = dataList[layoutPosition]

            ImageLoader.loadImage(binding.imgPaymentOption, model.imageName)
            binding.tvPaymentOption.text = model.name

            if (model.discountText.isNullOrEmpty()) {
                binding.tvDiscount.makeInVisible(true)
            } else {
                binding.tvDiscount.makeInVisible(false)
                binding.tvDiscount.text = model.discountText
            }

            binding.clBaseIPO.setOnClickListener {
                it.setTag(R.id.position, layoutPosition)
                it.setTag(R.id.model, model)
                recyclerItemClickListener.onClick(it)
            }

            if (model.isSelected){
                binding.imgPaymentOption.setPadding(20)
                binding.imgSelectedImage.makeVisible(true)
                binding.tvPaymentOption.isSelected = true
            }else{
                binding.imgPaymentOption.setPadding(0)
                binding.imgSelectedImage.makeVisible(false)
                binding.tvPaymentOption.isSelected = false
            }
        }
    }

    fun getAdapterList(): List<OptionsItem> {
        return dataList
    }

}