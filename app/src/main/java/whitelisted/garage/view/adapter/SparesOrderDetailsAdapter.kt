package whitelisted.garage.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import whitelisted.garage.R
import whitelisted.garage.api.response.OrderItem
import whitelisted.garage.databinding.CardPurchaseSummaryItemBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.roundOff
import whitelisted.garage.view.fragments.sparesShop.SparesOrderHistoryFragment
import kotlin.math.roundToInt

class SparesOrderDetailsAdapter(private val list: List<OrderItem>, private val symbol: String) :
    RecyclerView.Adapter<SparesOrderDetailsAdapter.Holder>() {

    var isExpanded = false
        set(value) {
            field = value
            notifyItemRangeChanged(2, list.size - 2)
        }
    var onTrackingIdClicked: ((String) -> Unit)? = null
    var trackingId: String? = null

    inner class Holder(private val binding: CardPurchaseSummaryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: OrderItem, isLast: Boolean) {
            binding.ivItemCPSI.load(item.imageUrl)
            binding.tvItemNameCPSI.text = item.title
            binding.tvItemIdCPSI.text =
                binding.root.resources.getString(R.string.part_no_colon).plus(AppENUM.SPACE)
                    .plus(item.productId)

            binding.tvItemAmountCPSI.text = symbol + item.mrp.roundOff()
            binding.tvItemPrice.text = symbol + (item.mrp?.div(item.quantity?:1))?.roundToInt()
            binding.tvItemQtyCPSI.text = item.quantity.toString()
            binding.vDivider.visibility = if (isLast) View.GONE else View.VISIBLE

            val trackingAdapter = SpareOrderTrackingAdapter(item.history)
            trackingAdapter.onTrackingIdClicked = onTrackingIdClicked
            binding.rvTrack.layoutManager = LinearLayoutManager(binding.root.context)
            binding.rvTrack.adapter = trackingAdapter
            trackingAdapter.trackingId = trackingId

            binding.tvSettledCSPH.text = ""

            binding.ivViewMoreTrack.setOnClickListener {

                val isExpanded = (binding.rvTrack.adapter as SpareOrderTrackingAdapter).isExpanded
                (binding.rvTrack.adapter as SpareOrderTrackingAdapter).isExpanded = !isExpanded
                binding.ivViewMoreTrack.animate().rotation(if (isExpanded) 0f else 180f)
                    .setDuration(500).start()

            }

            when (item.status) {
                 SparesOrderHistoryFragment.STATUS_DELIVERED -> {
                    binding.tvSettledCSPH.text = CommonUtils.getString(binding.root.context, R.string.delivered)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.status_complete)
                }
                SparesOrderHistoryFragment.STATUS_OUT_FOR_DELIVERY -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.out_for_delivery)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.in_transit)
                }
                SparesOrderHistoryFragment.STATUS_DISPATCHED -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.dispatched)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.orange)
                }
                SparesOrderHistoryFragment.STATUS_PENDING -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.pending)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.pending_red)
                }
                SparesOrderHistoryFragment.STATUS_IN_TRANSIT -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.in_transit)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.in_transit)
                }

                /*SparesOrderHistoryFragment.STATUS_CANCELED -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.canceled)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.pending_red)
                }*/

                else -> {
                    binding.tvSettledCSPH.text =
                        CommonUtils.getString(binding.root.context, R.string.canceled)
                    binding.tvSettledCSPH.setViewBackgroundColor(R.color.pending_red)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding = CardPurchaseSummaryItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return Holder(binding)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position], position == list.size - 1)
    }

    override fun getItemCount() = if (isExpanded || list.size < 2) list.size else 2
}