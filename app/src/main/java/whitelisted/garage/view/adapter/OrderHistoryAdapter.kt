package whitelisted.garage.view.adapter

import DateUtil
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.R
import whitelisted.garage.api.response.OrderListResponseModel
import whitelisted.garage.databinding.ItemOrdersHistoryBinding
import whitelisted.garage.databinding.ItemOrdersHistoryRetailBinding
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.utils.ImageLoader
import kotlin.random.Random

class OrderHistoryAdapter(val currencySymbol: String,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var dataList = ArrayList<OrderListResponseModel>()
    private val images =
        listOf(R.drawable.ic_dummy_cus1, R.drawable.ic_dummy_cus2, R.drawable.ic_dummy_cus3)
    var viewType = 0

    // Holds the views for adding it to image and text
    companion object {
        const val VIEW_TYPE_ONE = 0
        const val VIEW_TYPE_TWO = 1
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(list: MutableList<OrderListResponseModel>) {
        dataList.clear()
        dataList.addAll(list)
        notifyDataSetChanged()

    }

    @SuppressLint("NotifyDataSetChanged")
    fun addData(list: MutableList<OrderListResponseModel>) {
        dataList.addAll(list)
        val startPos = dataList.size
        notifyItemRangeInserted(startPos, list.size)
    }

    override fun getItemViewType(position: Int): Int {
        return when (viewType) {
            0 -> VIEW_TYPE_ONE
            1 -> VIEW_TYPE_TWO
            else -> {
                0
            }
        }
    }

    override fun getItemCount(): Int = dataList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == VIEW_TYPE_ONE) {
            val view =
                ItemOrdersHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return MyViewHolder(view)
        }

        return MyViewHolder1(ItemOrdersHistoryRetailBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (viewType == VIEW_TYPE_ONE) {
            (holder as OrderHistoryAdapter.MyViewHolder).bind(dataList[position])
        } else {
            (holder as OrderHistoryAdapter.MyViewHolder1).bind(dataList[position])
        }
    }


    inner class MyViewHolder(var binding: ItemOrdersHistoryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(data: OrderListResponseModel) {

            ImageLoader.loadImage(binding.imgCarImage, data.car_icon)
            binding.tvOrderDate.text =
                DateUtil.getFormattedDateUtcToGmt(AppENUM.DateUtilKey.TIME_FORMAT_NZ,
                    AppENUM.DDMMYYYY,
                    data.createdAt ?: "")
            binding.tvCarMake.text = data.carName
            binding.tvRegNumber.text = data.registrationNumber
            if (data.fuel_type?.isNotEmpty() == true) {
                binding.tvFuelType.text = data.fuel_type
            } else {
                binding.tvFuelType.text = "NA"
            }
            if (data.statusId == 130) binding.tvStatus.makeVisible(true)
            else binding.tvStatus.makeVisible(false)
            binding.tvName.text = data.name
            binding.tvPhoneNumber.text = data.mobile
            binding.tvInvoiceId.text = data.orderId
            when {
                data.payment_status.equals(AppENUM.UNPAID, ignoreCase = true) -> {
                    binding.tvPaymentStatus.text = data.payment_status
                    binding.tvPaymentStatus.setTextColor(binding.tvPaymentStatus.context.getColor(R.color.red_text_color))
                }
                data.payment_status.equals(AppENUM.PAID, ignoreCase = true) -> {
                    binding.tvPaymentStatus.text = data.payment_status
                    binding.tvPaymentStatus.setTextColor(binding.tvPaymentStatus.context.getColor(R.color.green_new))
                }
                else -> {
                    binding.tvPaymentStatus.text = ""
                }
            }
            if (data.invoiceAmount?.isNotEmpty() == true) binding.tvTotalAmount.text =
                currencySymbol + "${data.invoiceAmount}"
            else {
                binding.tvTotalAmount.text = "NA"
            }
            if (data.mechanic_name?.isNotEmpty() == true) binding.llMechNameTv.text =
                data.mechanic_name
            else {
                binding.llMechNameTv.text = "NA"
            }
            binding.clBase.setOnClickListener {
                binding.clBase.setTag(R.id.model, data)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    inner class MyViewHolder1(var binding: ItemOrdersHistoryRetailBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(data: OrderListResponseModel) {
            val random = Random.nextInt(images.size)

            ImageLoader.loadDrawable(ContextCompat.getDrawable(binding.imgCus.context,
                images[random]), binding.imgCus)
            binding.tvOrderId.text = data.orderId.toString()
            binding.tvItemCount.text = data.name.toString()
            binding.tvOrderStatus.text = data.statusName.toString()
            binding.tvDate.text =
                DateUtil.getFormattedDateUtcToGmt(AppENUM.DateUtilKey.TIME_FORMAT_NZ,
                    AppENUM.DDMMYYYY,
                    data.createdAt ?: "")
            if (data.invoiceAmount?.isNotEmpty() == true) binding.tvAmount.text =
                currencySymbol + "${data.invoiceAmount}"
            else {
                binding.tvAmount.text = "NA"
            }
            binding.clBase.setOnClickListener {
                binding.clBase.setTag(R.id.position, layoutPosition)
                recyclerItemClickListener.onClick(it)
            }
        }
    }
}

