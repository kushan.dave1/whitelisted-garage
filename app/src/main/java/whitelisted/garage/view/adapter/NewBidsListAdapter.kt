package whitelisted.garage.view.adapter

import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_new_enquiry_order.view.*
import whitelisted.garage.R
import whitelisted.garage.api.response.AccRetailBidListResponseModel
import whitelisted.garage.api.response.DistanceObject
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.CommonUtils.makeVisible

class NewBidsListAdapter(val shopId: String,
    var dataList: MutableList<AccRetailBidListResponseModel>,
    val recyclerItemClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AddPartItemViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_new_enquiry_order, parent, false))
    }

    override fun getItemCount(): Int = dataList.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AddPartItemViewHolder).bind()
    }

    inner class AddPartItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {

            val model = dataList[layoutPosition]
            itemView.tvShopName?.text = model.workshopName
            itemView.tvOrderId?.text = model.id
            val distanceObject: DistanceObject? =
                Gson().fromJson(model.distance?.getAsJsonObject(shopId).toString(),
                    DistanceObject::class.java)
            distanceObject?.let {
                "${it.distance} ${itemView.tvDistance.context.getString(R.string.km_away)}".apply {
                    itemView.tvDistance?.text = this
                }
            }
            if (model.isContactNumber?.contains(shopId) == true) {
                itemView.clgMobileMasked.makeVisible(false)
                itemView.tvPhoneNumberINEO.makeVisible(true)
                itemView.tvPhoneNumberINEO?.text = model.contactNumber
            } else {
                itemView.clgMobileMasked.makeVisible(true)
                itemView.tvPhoneNumberINEO.makeVisible(false)
                var maskedPhone: String?
                val initialFour = model.contactNumber?.substring(0, 4)
                val noOfStars = (model.contactNumber?.length ?: 10).minus(4)
                maskedPhone = initialFour
                if (noOfStars > 0) {
                    for (i in 1..(noOfStars)) {
                        maskedPhone += "*"
                    }
                }
                itemView.tvPhoneNumberMasked?.text = maskedPhone
            }
            itemView.tvDate?.text = CommonUtils.convertToDate(model.createdAt)
            itemView.tvTime?.text = CommonUtils.convertToTime(model.createdAt)

            val itemsText = StringBuilder()
            var i = 0
            var countMoreItemsText = ""
            kotlin.run breaking@{
                model.items?.forEach {
                    if (model.isItemTextExpanded == false) {
                        if (model.items.size > 3) {
                            if (i < 3) {
                                itemsText.append(it).append(" | ")
                                itemView.tvItems?.text =
                                    itemsText.substring(0, itemsText.length - 3)

                                i++
                            } else {
                                countMoreItemsText = "+ ${model.items.size.minus(i)} ${
                                    itemView.tvItems.context.getString(R.string.more_items_sc)
                                }"
                                "${itemView.tvItems.text} $countMoreItemsText".apply {
                                    itemView.tvItems?.text = this
                                }
                                return@breaking
                            }
                        } else {
                            itemsText.append(it).append(" | ")
                            itemView.tvItems?.text = itemsText.substring(0, itemsText.length - 3)

                            i++
                        }
                    } else {
                        itemsText.append(it).append(" | ")
                        itemView.tvItems?.text = itemsText.substring(0, itemsText.length - 3)

                        i++
                    }
                }
            }
            if ((model.items?.size ?: 0) > 3 && model.isItemTextExpanded == false) {
                val spannable = SpannableString(itemView.tvItems.text)
                itemView.tvItems?.text = getClickableTextWithColor(spannable,
                    countMoreItemsText,
                    ContextCompat.getColor(itemView.tvItems.context, R.color.colorAccent),
                    object : (View) -> Unit {
                        override fun invoke(p1: View) {
                            model.isItemTextExpanded = true
                            itemView.tvItems.setTag(R.id.tvItems, layoutPosition)
                            recyclerItemClickListener.onClick(itemView.tvItems)
                        }
                    })
                itemView.tvItems?.movementMethod = LinkMovementMethod()
            } else { // do nothing
            }

            itemView.btnSubmitBid.setOnClickListener {
                it.setTag(R.id.btnSubmitBid, model)
                recyclerItemClickListener.onClick(it)
            }

            itemView.llUnlockMobile.setOnClickListener {
                it.setTag(R.id.llUnlockMobile, model)
                recyclerItemClickListener.onClick(it)
            }

            itemView.tvPhoneNumberINEO.setOnClickListener {
                it.setTag(R.id.tvPhoneNumberINEO, model)
                recyclerItemClickListener.onClick(it)
            }
        }
    }

    fun getAdapterList(): List<AccRetailBidListResponseModel> {
        return dataList
    }

    fun addItemToAdapterList(item: AccRetailBidListResponseModel) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

    private fun getClickableTextWithColor(spannable: Spannable,
        clickableText: String,
        @ColorInt color: Int,
        onClickListener: (View) -> Unit): Spannable {
        val text = spannable.toString()

        val start = text.indexOf(clickableText)
        spannable.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                onClickListener.invoke(widget)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.color = color
                ds.isUnderlineText = false
            }
        }, start, start + clickableText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return spannable
    }

}