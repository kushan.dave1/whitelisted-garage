package whitelisted.garage.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_bus_show_attributes.view.*
import whitelisted.garage.R

class BusShowAttributeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var attributeData: MutableSet<String>? = null
    private var list = mutableMapOf<String, MutableList<String>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AttributeViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_bus_show_attributes, parent, false))
    }

    override fun getItemCount(): Int = attributeData?.size ?: 0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AttributeViewHolder).bind()
    }

    private inner class AttributeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind() {
            val data = attributeData?.elementAt(layoutPosition)
            data?.let {
                itemView.tvAttributeHeading.text = it
                itemView.rvAttributes.adapter = BusShowChildAttributeUpdater()
                (itemView.rvAttributes.adapter as BusShowChildAttributeUpdater).setData(list[it]
                    ?: mutableListOf())
            }
        }
    }


    @SuppressLint("NotifyDataSetChanged")
    fun setData(listData: MutableSet<String>,
        gmbLessAttributeData: MutableMap<String, MutableList<String>>?) {
        attributeData = listData
        list = gmbLessAttributeData ?: mutableMapOf()
        notifyDataSetChanged()
    }
}