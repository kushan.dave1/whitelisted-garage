package whitelisted.garage.utils

import android.annotation.SuppressLint
import android.app.*
import android.content.*
import android.content.res.Resources
import android.database.Cursor
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.location.Address
import android.location.Geocoder
import android.media.ExifInterface
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.ContactsContract
import android.provider.MediaStore
import android.telephony.TelephonyManager
import android.util.Patterns
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.ViewCompat
import androidx.core.view.drawToBitmap
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import coil.ImageLoader
import coil.request.ImageRequest
import coil.request.SuccessResult
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.play.core.review.ReviewManagerFactory
import com.google.android.play.core.review.model.ReviewErrorCode
import com.google.android.play.core.review.testing.FakeReviewManager
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.common.BitMatrix
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import org.apache.commons.text.WordUtils
import whitelisted.garage.R
import whitelisted.garage.api.data.Contact
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.network.error.errorCode
import java.io.*
import java.net.InetAddress
import java.net.NetworkInterface
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.time.temporal.WeekFields
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.math.roundToInt


object CommonUtils {
    private var notificationId = 0

    private var notificationBuilder: NotificationCompat.Builder? = null
    private var notificationManager: NotificationManager? = null

    fun showToast(context: Context?, message: String, showShort: Boolean = true) {
        if (message.isNotEmpty()) {
            try {
                context?.let {
                    if (message.toLowerCase(Locale.ENGLISH).contains("no data")) {
                        Toast.makeText(it, message, Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(it,
                            message,
                            if (showShort) Toast.LENGTH_SHORT else Toast.LENGTH_LONG).show()
                    }
                }
            } catch (e: Exception) { //Do nothing
            }
        }
    }

    fun ViewPager.setOnPageChangedListener(listener: (Int) -> Unit) {
        this.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                listener(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }



    fun openWebPage(context: Context, url: String?) {
        val webpage = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, webpage)
        intent.resolveActivity(context.packageManager)?.let {
            context.startActivity(intent)
        }

    }

    fun getCameraPhotoOrientation(context: Context, imageUri: Uri?, imagePath: String?): Int {
        var rotate = 0
        try {
            context.contentResolver.notifyChange(imageUri!!, null)
            val imageFile = File(imagePath)
            val exif = ExifInterface(imageFile.absolutePath)
            val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL)
            when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
                ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180
                ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90
            } //            Log.i("RotateImage", "Exif orientation: $orientation")
            //            Log.i("RotateImage", "Rotate value: $rotate")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return rotate
    }

    @SuppressLint("Range", "Recycle")
    fun fetchContactFromCursor(activity: Activity, result: ActivityResult): Contact {
        var c: Cursor?
        result.data?.data.apply {
            c = activity.contentResolver.query(this ?: Uri.EMPTY, null, null, null, null)
        }
        c?.let {
            while (it.moveToNext()) {
                if (it.getString(it.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))
                        .toInt() > 0) {
                    val pCur =
                        activity.contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            arrayOf(it.getString(it.getColumnIndex(ContactsContract.Contacts._ID))),
                            null)
                    while (pCur?.moveToNext() == true) {
                        var phone =
                            pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                        if (phone.contains("-")) phone = phone.substring(phone.indexOf("-"))
                        return Contact(name = it.getString(it.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)),
                            phoneNumber = phone.replace(" ", ""))
                    }
                }
            }
            return Contact()
        } ?: return Contact()
    }

    fun getRealPathFromURI(activity: Activity, contentUri: Uri): String? {
        var res: String? = null
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor? = activity.contentResolver.query(contentUri, proj, null, null, null)
        if (cursor?.moveToFirst() == true) {
            val columnIndex: Int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            res = cursor.getString(columnIndex)
        }
        cursor?.close()
        return res
    }

    fun getCustomFontTypeface(context: Context, fontId: Int): Typeface? {
        return ResourcesCompat.getFont(context, fontId)
    }

    @SuppressLint("SimpleDateFormat")
    fun getCurrentMonth(): Int {
        val dateFormat: DateFormat = SimpleDateFormat("MM")
        val date = Date()
        return dateFormat.format(date).toInt()
    }

    fun bitmapToFile(context: Context, bitmap: Bitmap): File { // Get the context wrapper
        val wrapper = ContextWrapper(context)

        // Initialize a new file instance to save bitmap object
        var file = wrapper.getDir("Images", Context.MODE_PRIVATE)
        file = File(file, "${UUID.randomUUID()}.jpg")

        try { // Compress the bitmap and save in jpg format
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        // Return the saved bitmap uri
        return file
    }

    fun reduceBitmapSize(bitmap: Bitmap, MAX_SIZE: Int): Bitmap? {

        val bitmapHeight: Int = bitmap.height
        val bitmapWidth: Int = bitmap.width
        val ratioSquare = (bitmapHeight * bitmapWidth / MAX_SIZE).toDouble()
        if (ratioSquare <= 1) return bitmap
        val ratio = Math.sqrt(ratioSquare)
        val requiredHeight = Math.round(bitmapHeight / ratio).toInt()
        val requiredWidth = Math.round(bitmapWidth / ratio).toInt()
        return Bitmap.createScaledBitmap(bitmap, requiredWidth, requiredHeight, true)
    }

    fun sendSMSTEXT(context: Context, mobile: String, message: String) {
        val sendIntent = Intent(Intent.ACTION_VIEW)
        sendIntent.data = Uri.parse("sms:")
        sendIntent.data = Uri.parse("smsto:".plus(mobile))
        sendIntent.putExtra("sms_body", message.replace("*", ""))
        context.startActivity(sendIntent)
    }

    fun showRewardToast(context: Activity?, message: String, isCredit: Boolean) {
        try {
            context?.let {
                val inflater = context.layoutInflater
                val view = when (isCredit) {
                    true -> {
                        inflater.inflate(R.layout.layout_credit_reward,
                            context.findViewById(R.id.root))

                    }
                    false -> {
                        inflater.inflate(R.layout.layout_debit_reward,
                            context.findViewById(R.id.root))

                    }
                }
                val text: TextView = view.findViewById(R.id.tvText)
                text.text = message
                val toast = Toast(context)
                toast.setGravity(Gravity.TOP, 0, 140);
                toast.duration = Toast.LENGTH_LONG
                toast.view = view
                toast.show()
            }
        } catch (e: Exception) {

        }
    }

    fun countOccurrences(s: String, ch: Char): Int {
        return s.filter { it == ch }.count()
    }

    fun showReviewDialog(activity: Activity) {
        val manager = ReviewManagerFactory.create(activity)
        val request = manager.requestReviewFlow()
        request.addOnCompleteListener { task ->
            if (task.isSuccessful) { // We got the ReviewInfo object
                val reviewInfo = task.result
                val flow = manager.launchReviewFlow(activity, reviewInfo)
                flow.addOnCompleteListener { _ -> // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown. Thus, no
                    // matter the result, we continue our app flow.
                }
            } else { // There was some problem, log or handle the error code.
                @ReviewErrorCode val reviewErrorCode =
                    (task.exception as java.lang.Exception).errorCode
            }
        }
    }

    fun showFakeReviewDialog(activity: Activity) {
        val manager = FakeReviewManager(activity)
        val request = manager.requestReviewFlow()
        request.addOnCompleteListener { task ->
            if (task.isSuccessful) { // We got the ReviewInfo object
                val reviewInfo = task.result
                val flow = manager.launchReviewFlow(activity, reviewInfo)
                flow.addOnCompleteListener { _ -> // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown. Thus, no
                    // matter the result, we continue our app flow.
                }
            } else { // There was some problem, log or handle the error code.
                @ReviewErrorCode val reviewErrorCode =
                    (task.exception as java.lang.Exception).errorCode
            }
        }
    }

    fun scaleBitmap(bm: Bitmap): Bitmap {
        var width = bm.width
        var height = bm.height
        val maxHeight = 500
        val maxWidth = 500
        when {
            width > height -> {
                try { // landscape
                    val ratio = width / maxWidth
                    width = maxWidth
                    height = (height / ratio)
                } catch (ex: java.lang.Exception) {
                    height = maxHeight
                    width = maxWidth
                }

            }
            height > width -> {
                try { // portrait
                    val ratio = height / maxHeight
                    height = maxHeight
                    width = (width / ratio)
                } catch (ex: java.lang.Exception) {
                    height = maxHeight
                    width = maxWidth
                }

            }
            else -> { // square
                height = maxHeight
                width = maxWidth
            }
        }
        return Bitmap.createScaledBitmap(bm, width, height, true)

    }

    fun isPasswordValid(password: String): Boolean { //        String pattern = "^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$";
        return if (password.length in 6..200) {
            val pattern = "^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-ZÀ-ÿ0-9!@#$%^&*]{6,200}$"
            Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(password).matches()
        } else {
            false
        }
    }

    fun isValidMobile(s: String?): Boolean {
        val p: Pattern = Pattern.compile("^\\d{10}$")
        val m: Matcher = p.matcher(s)
        return m.matches()
    }

    fun getFormattedDate(inFormat: String, outFormat: String, dateToBeParsed: String): String {
        val inFormatter = SimpleDateFormat(inFormat, Locale.ENGLISH)
        val outFormatter = SimpleDateFormat(outFormat, Locale.ENGLISH)

        return try {
            outFormatter.format(inFormatter.parse(dateToBeParsed) ?: Date())
        } catch (e: Exception) {
            ""
        }
    }

    fun getFormattedDate(outFormat: String, dateToBeParsed: Date): String {
        val outFormatter = SimpleDateFormat(outFormat, Locale.ENGLISH)

        return outFormatter.format(dateToBeParsed)
    }

    fun hideKeyboard(act: Activity?) {
        act?.let {
            it.currentFocus?.run {
                val inputMethodManager =
                    it.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(this.windowToken, 0)
                it.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            }
        }
    }

    fun openPhoneCallApp(mobile: String, context: Context) {
        val u = Uri.parse("tel:$mobile")

        val i = Intent(Intent.ACTION_DIAL, u)
        try {
            context.startActivity(i)
        } catch (s: SecurityException) {
            showToast(context, context.getString(R.string.error_occured))

        }

    }

    fun showKeyboard(act: Context?, editText: EditText) {
        act?.let {
            it.run {
                val inputMethodManager =
                    act.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                inputMethodManager?.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
            }
        }
    }

    fun hideKeyboardFragment(fragment: BaseFragment) {
        val imm = fragment.requireActivity()
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(fragment.requireView().windowToken, 0)
    }

    fun hideKeyboardDialog(fragment: BaseDialogFragment) {
        val view = fragment.view
        val imm = fragment.requireActivity()
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(fragment.requireView().windowToken, 0)
    }

    inline fun <reified T> genericCastOrNull(anything: Any?): T? {
        return anything as? T
    }

    fun createPartFromString(param: String): RequestBody {
        return param.toRequestBody("multipart/form-data".toMediaTypeOrNull())
    }

    fun saveSignature(activity: FragmentActivity?, bitmap: Bitmap?): File? {
        activity?.let { act ->
            bitmap?.let {
                val root = act.filesDir.toString()
                val myDir = File("$root/Garage/Inventory/Signature")
                myDir.mkdirs()
                val fname = "Signature" + ".jpg"
                val file = File(myDir, fname)
                if (file.exists()) {
                    file.delete()
                }
                val out = FileOutputStream(file)

                try {
                    if (bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)) {
                        out.flush()
                        out.close()
                        return file
                    }
                    out.flush()
                    out.close()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                } finally {
                    try {
                        out.close()
                    } catch (e: Exception) { //Do nothing
                    }
                }
            }
        }

        return null

    }

    @Throws(IOException::class)
    fun compressImage(path: String): String {
        val file = File(path)
        val bitmap = BitmapFactory.decodeFile(path)
        val os: OutputStream = BufferedOutputStream(FileOutputStream(file))
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, os)
        os.close()
        return path
    }

    fun getFormattedDateWithDay(strDate: String?): String? {
        val yyyyMmDd = "yyyy-MM-dd"
        var fmtOut: SimpleDateFormat? = null
        var date: Date? = null
        try {
            val formatter: DateFormat = SimpleDateFormat(yyyyMmDd, Locale.ENGLISH)
            date = formatter.parse(strDate ?: "") as Date
            fmtOut = SimpleDateFormat("EEE, d MMM yy", Locale.ENGLISH)
        } catch (e: java.lang.Exception) {
        }
        return fmtOut?.format(date ?: Date())
    }

    fun String?.lower(): String = this?.toLowerCase(Locale.getDefault()) ?: ""

    fun String?.upper(): String = this?.toUpperCase(Locale.getDefault()) ?: ""

    fun <T> LifecycleOwner.callApi(apiCall: suspend () -> T, callback: (T) -> Unit) {
        lifecycleScope.launch {
            val data = apiCall()
            if (data != null) callback(data)
        }
    }

    fun formatDateNormal(strDate: String?): String? {
        val yyyyMmDd = "yyyy-MM-dd"
        var fmtOut: SimpleDateFormat? = null
        var date: Date? = null
        try {
            val formatter: DateFormat = SimpleDateFormat(yyyyMmDd, Locale.ENGLISH)
            date = formatter.parse(strDate ?: "") as Date
            fmtOut = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
        } catch (e: java.lang.Exception) {
        }
        return fmtOut?.format(date ?: Date())
    }

    fun formatDateNormal2(strDate: String?): String {
        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var newDateStr = ""
        try {
            val myDate = simpleDateFormat.parse(strDate ?: "")
            val postFormater = SimpleDateFormat("EEE, d MMM, yyyy", Locale.getDefault())
            newDateStr = postFormater.format(myDate ?: "")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return newDateStr
    }

    fun formatDateNormal3(strDate: String?): String {
        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var newDateStr = ""
        try {
            val myDate = simpleDateFormat.parse(strDate ?: "")
            val postFormater = SimpleDateFormat("d MMM, yyyy", Locale.getDefault())
            newDateStr = postFormater.format(myDate ?: "")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return newDateStr
    }

    fun String?.toDate(): Date? {
        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")

        return try {
             simpleDateFormat.parse(this ?: "")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            null
        }
    }

    fun convertDateWithDay(time: String?): String? {
        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var newDateStr = ""
        try {
            val myDate = simpleDateFormat.parse(time ?: "")
            val postFormater = SimpleDateFormat("dd-MM-yyyy, hh:mm aaa", Locale.getDefault())
            newDateStr = postFormater.format(myDate ?: "")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return newDateStr
    }

    fun convertDateWithDaySlashes(time: String?): String? {
        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var newDateStr = ""
        try {
            val myDate = simpleDateFormat.parse(time ?: "")
            val postFormater = SimpleDateFormat("dd/MM/yyyy, hh:mm aaa", Locale.getDefault())
            newDateStr = postFormater.format(myDate ?: "")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return newDateStr
    }

    fun saveToDisk(activity: Activity,
        body: ResponseBody,
        filename: String,
        notificationTitle: String,
        notificationSubTitle: String,
        mimeType: String = "text/csv") {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val contentValues = ContentValues()
            contentValues.put(MediaStore.Downloads.TITLE, filename)
            contentValues.put(MediaStore.Downloads.DISPLAY_NAME, filename)
            contentValues.put(MediaStore.Downloads.MIME_TYPE, mimeType)
            contentValues.put(MediaStore.Downloads.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS)

            val resolver: ContentResolver = activity.application.contentResolver
            val downloadedFileUri: Uri? =
                resolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues)

            try {
                var inputStream: InputStream? = null
                var outputStream: OutputStream? = null
                try {
                    notificationId += 1
                    Handler(Looper.getMainLooper()).postDelayed({
                        sendNotification(activity, notificationTitle, fileUri = downloadedFileUri)
                        inputStream = body.byteStream()
                        outputStream = downloadedFileUri?.let { resolver.openOutputStream(it) }

                        inputStream?.use { input ->
                            outputStream?.use { fileOut ->
                                input.copyTo(fileOut)
                            }
                        }

                        outputStream?.flush() //                        orderFileDownloadLiveData.postValue(Event(true))
                        downloadNotification(notificationSubTitle)
                    }, 100)
                    return
                } catch (e: IOException) {
                    e.printStackTrace() //                    orderFileDownloadLiveData.postValue(Event(false))
                    downloadNotification(activity.getString(R.string.download_failed))
                    return
                } finally {
                    inputStream?.also { it.close() }
                    outputStream?.also { it.close() }
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return
            }
        } else {
            try {
                val destinationFile =
                    File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                        filename)
                var inputStream: InputStream? = null
                var outputStream: OutputStream? = null
                try {
                    notificationId += 1
                    sendNotification(activity, notificationTitle, fileName = filename)
                    inputStream = body.byteStream()
                    outputStream = FileOutputStream(destinationFile)

                    inputStream.use { input ->
                        outputStream?.use { fileOut ->
                            input?.copyTo(fileOut)
                        }
                    }
                    outputStream?.flush() //                    orderFileDownloadLiveData.postValue(Event(true))
                    downloadNotification(notificationSubTitle)
                    return
                } catch (e: IOException) {
                    e.printStackTrace() //                    orderFileDownloadLiveData.postValue(Event(false))
                    downloadNotification(activity.getString(R.string.download_failed))
                    return
                } finally {
                    inputStream?.also { it.close() }
                    outputStream?.also { it.close() }
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return
            }
        }
    }

    fun savePDFToDisk(activity: Activity,
        byteArray: ByteArray,
        filename: String,
        notificationTitle: String,
        notificationSubTitle: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val contentValues = ContentValues()
            contentValues.put(MediaStore.Downloads.TITLE, filename)
            contentValues.put(MediaStore.Downloads.DISPLAY_NAME, filename)
            contentValues.put(MediaStore.Downloads.MIME_TYPE, "application/pdf")
            contentValues.put(MediaStore.Downloads.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS)

            val resolver: ContentResolver = activity.application.contentResolver
            val downloadedFileUri: Uri? =
                resolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues)

            try {
                var inputStream: InputStream? = null
                var outputStream: OutputStream? = null
                try {
                    notificationId += 1
                    Handler(Looper.getMainLooper()).postDelayed({

                        inputStream = byteArray.inputStream()
                        outputStream = downloadedFileUri?.let { resolver.openOutputStream(it) }

                        inputStream.use { input ->
                            outputStream.use { fileOut ->
                                if (fileOut != null) {
                                    input?.copyTo(fileOut)
                                }
                            }
                        }
                        outputStream?.flush() //                        orderFileDownloadLiveData.postValue(Event(true))
                        downloadNotification(notificationSubTitle)
                    }, 100)
                    return
                } catch (e: IOException) {
                    e.printStackTrace() //                    orderFileDownloadLiveData.postValue(Event(false))
                    downloadNotification(activity.getString(R.string.download_failed))
                    return
                } finally {
                    inputStream?.also { it.close() }
                    outputStream?.also { it.close() }
                    sendNotification(activity,
                        notificationTitle,
                        fileUri = downloadedFileUri,
                        type = AppENUM.RefactoredStrings.APPLICATION_PDF)
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return
            }
        } else {
            try {
                val destinationFile =
                    File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                        filename)
                var inputStream: InputStream? = null
                var outputStream: OutputStream? = null
                try {
                    notificationId += 1

                    inputStream = byteArray.inputStream()
                    outputStream = FileOutputStream(destinationFile)

                    inputStream.use { input ->
                        outputStream?.use { fileOut ->
                            input?.copyTo(fileOut)
                        }
                    }
                    outputStream?.flush() //                    orderFileDownloadLiveData.postValue(Event(true))
                    downloadNotification(notificationSubTitle)
                    return
                } catch (e: IOException) {
                    e.printStackTrace() //                    orderFileDownloadLiveData.postValue(Event(false))
                    downloadNotification(activity.getString(R.string.download_failed))
                    return
                } finally {
                    inputStream?.also { it.close() }
                    outputStream?.also { it.close() }
                    sendNotification(activity,
                        notificationTitle,
                        fileName = filename,
                        type = AppENUM.RefactoredStrings.APPLICATION_PDF)
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return
            }
        }
    }

    private fun downloadNotification(message: String) {
        notificationBuilder?.setContentText(message)?.setProgress(0, 0, false)
        notificationManager?.notify(notificationId, notificationBuilder?.build())
    }

    private fun sendNotification(activity: Activity,
        notificationTitle: String,
        fileUri: Uri? = null,
        type: String = "text/csv",
        fileName: String? = null) {
        val intent = Intent(Intent.ACTION_VIEW)
        val uriForFile = when {
            fileName.isNullOrEmpty() && fileUri != null -> fileUri
            else -> FileProvider.getUriForFile(activity.applicationContext,
                activity.applicationContext.packageName + ".provider",
                File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                    fileName ?: ""))
        }
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        intent.setDataAndType(uriForFile, type)
        val pendingIntent = PendingIntent.getActivity(activity.applicationContext,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE)
        val channelId = activity.getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        notificationBuilder = NotificationCompat.Builder(activity.applicationContext, channelId)
            .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
            .setContentTitle(notificationTitle)
            .setContentText(activity.applicationContext.getString(R.string.download_in_progress))
            .setAutoCancel(true).setSound(defaultSoundUri).setContentIntent(pendingIntent)

        notificationBuilder?.setSmallIcon(R.drawable.ic_placeholder_doc)
        notificationBuilder?.color =
            ContextCompat.getColor(activity.applicationContext, R.color.colorRed)

        notificationManager =
            activity.applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                "Channel title",
                NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager?.createNotificationChannel(channel)
        }

        notificationManager?.notify(notificationId, notificationBuilder?.build())
    }

    fun downloadPDFFile(activity: Activity,
        downloadFileName: String,
        pdfFile: String,
        downloadMessage: String = "") {
        try {
            val downloadManager =
                activity.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            val downloadUri =
                Uri.parse(pdfFile) //            val downloadUri = Uri.parse("https://www.clickdimensions.com/links/TestPDFfile.pdf") //            val fileName = URLUtil.guessFileName(pdfFile,"","")
            val fileName = downloadFileName
            val downFile =
                File(activity.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString(),
                    fileName)
            if (!downFile.exists()) downFile.mkdirs() //            val result: File =
            //                File(downFile.absolutePath.toString() + File.separator + fileName + ".pdf")
            val request = DownloadManager.Request(downloadUri).apply {
                setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE).setTitle(
                    fileName)
                    .setDescription(downloadMessage) //                    .setDestinationInExternalFilesDir(
                    //                        activity.baseContext,
                    //                        Environment.DIRECTORY_DOWNLOADS,
                    //                        "$fileName.pdf"
                    //                    )
                    .setDestinationUri(Uri.fromFile(downFile)).setAllowedOverMetered(true)
                    .setAllowedOverRoaming(true)
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
            }
            request.addRequestHeader("Accept", "application/pdf")
            downloadManager.enqueue(request)
        } catch (e: Exception) {
            showToast(activity, activity.getString(R.string.sorry_error_occurred))
        }
    }

    /*  fun convertToDateWithoutUTC(time: String?): String? {
          val simpleDateFormat = SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ,
              Locale.getDefault()) //        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
          var newDateStr = ""
          try {
              val myDate = simpleDateFormat.parse(time ?: "")
              val postFormater = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
              newDateStr = postFormater.format(myDate ?: "")
          } catch (e: java.lang.Exception) {
              e.printStackTrace()
          }
          return newDateStr
      }*/

    fun isBeforeDate(time1: String?, time2: String): Boolean {
        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        try {
            val date1 = simpleDateFormat.parse(time1 ?: "")
            val date2 = simpleDateFormat.parse(time2 ?: "")
            return date1?.before(date2) ?: false
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return false
    }


    fun convertToDate(time: String?): String? {
        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var newDateStr = ""
        try {
            val myDate = simpleDateFormat.parse(time ?: "")
            val postFormater = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            newDateStr = postFormater.format(myDate ?: "")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return newDateStr
    }

    fun convertToTime(time: String?): String? {
        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var newDateStr = ""
        try {
            val myDate = simpleDateFormat.parse(time ?: "")
            val postFormater = SimpleDateFormat("hh:mm aaa", Locale.getDefault())
            newDateStr = postFormater.format(myDate ?: "")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return newDateStr
    }

    /* fun convertToTimeWithoutUTC(time: String?): String? {
         val simpleDateFormat = SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ,
             Locale.getDefault()) //        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
         var newDateStr = ""
         try {
             val myDate = simpleDateFormat.parse(time ?: "")
             val postFormater = SimpleDateFormat("hh:mm aaa", Locale.getDefault())
             newDateStr = postFormater.format(myDate ?: "")
         } catch (e: java.lang.Exception) {
             e.printStackTrace()
         }
         return newDateStr
     }
 */
    private val QRcodeWidth = 600
    fun textToImageEncode(Value: String): Bitmap? {
        val bitMatrix: BitMatrix
        try {
            bitMatrix = MultiFormatWriter().encode(Value,
                BarcodeFormat.QR_CODE,
                QRcodeWidth,
                QRcodeWidth,
                null)
        } catch (Illegalargumentexception: IllegalArgumentException) {
            return null
        }
        val bitMatrixWidth = bitMatrix.getWidth()
        val bitMatrixHeight = bitMatrix.getHeight()
        val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)
        for (y in 0 until bitMatrixHeight) {
            val offset = y * bitMatrixWidth
            for (x in 0 until bitMatrixWidth) {
                pixels[offset + x] = if (bitMatrix.get(x, y)) Color.parseColor("#000000")
                else Color.parseColor("#ffffff")
            }
        }
        val bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)
        bitmap.setPixels(pixels, 0, QRcodeWidth, 0, 0, bitMatrixWidth, bitMatrixHeight)
        return bitmap
    }

    fun convertDateWithTime(time: String?): String? {
        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var newDateStr = ""
        try {
            val myDate = simpleDateFormat.parse(time ?: "")
            val postFormater = SimpleDateFormat("dd MMM, hh:mm aaa", Locale.getDefault())
            newDateStr = postFormater.format(myDate ?: "")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return newDateStr
    }

    fun convertDateWithDayNOTime(time: String?): String? {
        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var newDateStr = ""
        try {
            val myDate = simpleDateFormat.parse(time ?: "")
            val postFormater = SimpleDateFormat("EEE, dd MMM yyyy", Locale.getDefault())
            newDateStr = postFormater.format(myDate ?: "")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return newDateStr
    }

    fun convertNumMonthDateToNormalWithoutUTC(time: String?): String? {
        val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy",
            Locale.getDefault()) //        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var newDateStr = ""
        try {
            val myDate = simpleDateFormat.parse(time ?: "")
            val postFormater = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
            newDateStr = postFormater.format(myDate ?: "")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return newDateStr
    }

    fun convertNumMonthDateToNormal(time: String?): String? {
        val simpleDateFormat = SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ,
            Locale.getDefault()) //        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var newDateStr = ""
        try {
            val myDate = simpleDateFormat.parse(time ?: "")
            val postFormater = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
            newDateStr = postFormater.format(myDate ?: "")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return newDateStr
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun returnMillisForDate(time: String?): Long {
        val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        var milis: Long = 0 //        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var newDateStr = ""
        try {
            val myDate = simpleDateFormat.parse(time ?: "")
            if (myDate != null) {
                milis = myDate.toInstant().toEpochMilli()
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return milis
    }

    //    private var httpClient: OkHttpClient? = null
    //
    //    // this method is used to fetch svg and load it into target imageview.
    //    fun fetchSvg(context: Context, url: String?, target: ImageView) {
    //        if (httpClient == null) {
    //            httpClient = Uri.Builder()
    //                .cache(Cache(context.cacheDir, 5 * 1024 * 1014))
    //                .build()
    //        }
    //
    //        // here we are making HTTP call to fetch data from URL.
    //        val request: Request = Builder().url(url).build()
    //        httpClient!!.newCall(request).enqueue(object : Callback() {
    //            fun onFailure(call: Call?, e: IOException?) {
    //                // we are adding a default image if we gets any error.
    //                target.setImageResource(R.drawable.gfgimage)
    //            }
    //
    //            @Throws(IOException::class)
    //            fun onResponse(call: Call?, response: Response) {
    //                // sharp is a library which will load stream which we generated
    //                // from url in our target imageview.
    //                val stream: InputStream = response.body().byteStream()
    //                Sharp.loadInputStream(stream).into(target)
    //                stream.close()
    //            }
    //        })
    //    }


    @JvmOverloads
    fun addFragmentUtil(activity: FragmentActivity?,
        fragment: BaseFragment?,
        addToBackStack: Boolean = true,
        target: Int = R.id.fragment_container_2,
        allowStateLoss: Boolean = false,
        isChild: Boolean = false) {
        hideKeyboard(activity)
        try {
            activity?.let { activityIt ->
                fragment?.let { _ ->

                    val ft: FragmentTransaction =
                        activityIt.supportFragmentManager.beginTransaction()
                    ft.add(target, fragment, fragment.javaClass.name)
                    if (addToBackStack) {
                        ft.addToBackStack(fragment.javaClass.name)
                    }

                    if (allowStateLoss) {
                        ft.commitAllowingStateLoss()
                    } else {
                        ft.commit()
                    }
                }
            }
        } catch (e: Exception) { //Do nothing
        }

    }

    @JvmOverloads
    fun replaceFragmentUtil(activity: FragmentActivity?,
        fragment: BaseFragment?,
        addToBackStack: Boolean = false,
        target: Int = R.id.fragment_container_2,
        allowStateLoss: Boolean = false) {
        hideKeyboard(activity)

        activity?.let { activityIt ->
            fragment?.let { _ ->
                try {
                    val ft: FragmentTransaction =
                        activityIt.supportFragmentManager.beginTransaction()
                    ft.replace(target, fragment)
                    if (addToBackStack) {
                        ft.addToBackStack(fragment.javaClass.name)
                    }
                    if (allowStateLoss) {
                        ft.commitAllowingStateLoss()
                    } else {
                        ft.commit()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

    }

    fun capitalizeFirstLetter(stringPassed: String): String {

        return if (stringPassed.isEmpty()) {
            stringPassed
        } else {
            WordUtils.capitalizeFully(stringPassed)
        }

    }

    private const val WITH_DECIMAL_FORMAT = "##,##,##0.00"
    fun numberToCurrencyString(amount: Double): String {
        val amounts: String
        return try {
            val formatter = DecimalFormat(WITH_DECIMAL_FORMAT)
            amounts = formatter.format(amount.toInt())
            if (amounts.contains(".00")) {
                amounts.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
            } else {
                amounts
            }
        } catch (e: Exception) {
            "" + amount
        }
    }

    fun Double?.isNotZero() = (this ?: 0.0) != 0.0

    fun CharSequence?.isValidEmail() =
        !isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

    fun View?.makeVisible(type: Boolean) {
        this?.let {
            if (type) it.visibility = View.VISIBLE
            else it.visibility = View.GONE
        }
    }

    fun View?.makeInVisible(type: Boolean) {
        this?.let {
            if (type) it.visibility = View.INVISIBLE
            else it.visibility = View.VISIBLE
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun daysOfWeekFromLocale(): Array<DayOfWeek> {
        val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
        var daysOfWeek =
            DayOfWeek.values() // Order `daysOfWeek` array so that firstDayOfWeek is at index 0. // Only necessary if firstDayOfWeek != DayOfWeek.MONDAY which has ordinal 0.
        if (firstDayOfWeek != DayOfWeek.MONDAY) {
            val rhs = daysOfWeek.sliceArray(firstDayOfWeek.ordinal..daysOfWeek.indices.last)
            val lhs = daysOfWeek.sliceArray(0 until firstDayOfWeek.ordinal)
            daysOfWeek = rhs + lhs
        }
        return daysOfWeek
    }


    fun convertSecondsToRecFormat(seconds: String): String {
        val min = seconds.toInt() / 60
        val secLeft = String.format("%02d", seconds.toInt() % 60)
        return "$min:$secLeft"
    }

    fun calculateSeekBarProgress(timerCount: Float, totalLength: Float): Int {
        return ((timerCount / totalLength) * 100).toInt()
    }

    fun convertDoubleTo2DecimalPlaces(double: Double): String {
        return String.format("%.2f", double)
    }

    fun convertIntTo2ZerosPlaces(int: Int): String {
        return String.format("%02d", int)
    }

    //    fun Double?.roundOffTo2Decimals() =
    //        (((convertDoubleTo2DecimalPlaces(this ?: 0.0)).toDouble().roundToLong()) / 100.0)

    fun Double?.roundOff() = this?.roundToInt()

    fun isWithinRange(date: String): Boolean {
        val testDate = getDateObjectFromString(date)
        val currentDate = Calendar.getInstance().time
        val cal = Calendar.getInstance()
        cal.time = testDate
        cal.add(Calendar.DAY_OF_MONTH, 30)
        val endDate = cal.time
        return currentDate.after(endDate) || currentDate.equals(endDate)
    }

    fun getCountryCodeFromTelephonyM(context: Context): String? {
        val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as? TelephonyManager
        return tm?.networkCountryIso
    }

    fun sendEMail(context: Context, to: String, message: String) {
        val email = Intent(Intent.ACTION_SEND)
        email.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        email.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
        email.putExtra(Intent.EXTRA_TEXT, message)
        email.type = "message/rfc822"
        email.setPackage("com.google.android.gm")

        try {
            context.startActivity(Intent.createChooser(email, "Choose an Email client :"))
        } catch (e: ActivityNotFoundException) {
            showToast(context, e.message.toString())
        }
    }

    fun differenceFromTodayInHours(date: String, hours: Long?): Long {

        var dateObject1: Date? = null
        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        try {
            dateObject1 = simpleDateFormat.parse(date)

        } catch (e: java.lang.Exception) {

        }

        val currentDateObj = Calendar.getInstance().time
        val cal1 = Calendar.getInstance()
        cal1.time = dateObject1 ?: Date()
        cal1.add(Calendar.HOUR, hours?.toInt() ?: 0)
        val timeDifference = (cal1.time.time - currentDateObj.time)
        return timeDifference

    }

    private fun getDateObjectFromString(dateString: String): Date? {
        var dateObject: Date? = null
        val simpleDateFormat =
            SimpleDateFormat(AppENUM.DateUtilKey.TIME_FORMAT_NZ, Locale.getDefault())
        try {
            dateObject = simpleDateFormat.parse(dateString)

        } catch (e: java.lang.Exception) {

        }
        return dateObject
    }


    fun isValidGSTNo(str: String?): Boolean {

        val regex =
            "(0[0-9]|1[1-9]|2[0-9]|3[0-7])[A-Z]{3}[CPHFATBLJG]" + "{1}[A-Z]{1}\\d{4}[A-Z]{1}\\d{1}[A-Z0-9]{2}"
        val p = Pattern.compile(regex)

        if (str == null) {
            return false
        }

        val m: Matcher = p.matcher(str)

        return m.matches()
    }

    fun getIPAddress(useIPv4: Boolean): String? {
        try {
            val interfaces: List<NetworkInterface> =
                Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intf in interfaces) {
                val addrs: List<InetAddress> = Collections.list(intf.inetAddresses)
                for (addr in addrs) {
                    if (!addr.isLoopbackAddress) {
                        val sAddr =
                            addr.hostAddress //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        val isIPv4 = sAddr.indexOf(':') < 0
                        if (useIPv4) {
                            if (isIPv4) return sAddr
                        } else {
                            if (!isIPv4) {
                                val delim = sAddr.indexOf('%') // drop ip6 zone suffix
                                return if (delim < 0) sAddr.toUpperCase() else sAddr.substring(0,
                                    delim).toUpperCase()
                            }
                        }
                    }
                }
            }
        } catch (ignored: java.lang.Exception) {
        } // for now eat exceptions
        return ""
    }

    fun TextView.changeTextColor(context: Context, intColor: Int) {
        this.setTextColor(ContextCompat.getColor(context, intColor))
    }

    fun EditText.changeTextColor(context: Context, intColor: Int) {
        this.setTextColor(ContextCompat.getColor(context, intColor))
    }

    fun View.changeBackgroundDrawable(context: Context, drawable: Int) {
        this.background = ContextCompat.getDrawable(context, drawable)
    }

    fun View.changeBackgroundColor(context: Context, color: Int) {
        this.setBackgroundColor(ContextCompat.getColor(context, color))
    }

    fun getColor(context: Context, intColor: Int): Int {
        return ContextCompat.getColor(context, intColor)
    }

    fun getColorParsed(context: Context, intColor: String): Int {
        try {
            return Color.parseColor(intColor)
        } catch (e: Exception) { //do nothing
        }
        return context.getColor(R.color.white)
    }

    fun TextView.changeViewCompoundDrawablesWithInterinsicBounds(drawableStart: Drawable? = null,
        drawableEnd: Drawable? = null,
        drawableTop: Drawable? = null,
        drawableBottom: Drawable? = null) {
        this.setCompoundDrawablesWithIntrinsicBounds(drawableStart,
            drawableTop,
            drawableEnd,
            drawableBottom)
    }

    suspend fun getBitmapFromUrl(context: Context, icon: String?): Deferred<Bitmap?> =
        CoroutineScope(Dispatchers.IO).async { //            val executors = Executors.newSingleThreadExecutor()
            var image: Bitmap? = null
            try { //                executors.execute {
                //                    val input = URL(icon).openStream()
                image = getBitmap(context, icon)
                return@async image
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                return@async null
            }
        }

    suspend fun getBitmap(context: Context, icon: String?): Bitmap? = withContext(Dispatchers.IO) {
        val loader = ImageLoader(context)
        val request = ImageRequest.Builder(context).data(icon)
            .allowHardware(false) // Disable hardware bitmaps.
            .build()
        val result = (loader.execute(request) as? SuccessResult)?.drawable
        return@withContext (result as? BitmapDrawable)?.bitmap
    }

    fun getDrawable(context: Context, drawableInt: Int): Drawable? {
        return ContextCompat.getDrawable(context, drawableInt)
    }

    fun getString(context: Context, stringInt: Int): String {
        return context.getString(stringInt)
    }

    fun getFirstLetter(char: String): String {
        return if (char.isEmpty()) {
            ""
        } else {
            char.toCharArray()[0].toString().toUpperCase(Locale.ROOT)
        }

    }

    fun View.isVisible(): Boolean {
        return this.visibility == View.VISIBLE
    }

    fun View.getLocationPointOnScreen(): Point {
        val location = IntArray(2)
        this.getLocationOnScreen(location)
        return Point(location[0], location[1])
    }

    fun getCityFromLocation(context: Context, latitude: Double, longitude: Double): String {
        val gcd = Geocoder(context, Locale.getDefault())
        val addresses: List<Address>? = gcd.getFromLocation(latitude, longitude, 1)
        return if (addresses?.isNotEmpty() == true) {
            addresses[0].locality ?: ""
        } else ""
    }

    fun getStateFromLocation(context: Context, latitude: Double, longitude: Double): String {
        val gcd = Geocoder(context, Locale.getDefault())
        val addresses: List<Address>? = gcd.getFromLocation(latitude, longitude, 1)
        return if (addresses?.isNotEmpty() == true) {
            addresses[0].adminArea ?: ""
        } else ""
    }

    fun getCountryFromLocation(context: Context, latitude: Double, longitude: Double): String {
        val gcd = Geocoder(context, Locale.getDefault())
        val addresses: List<Address>? = gcd.getFromLocation(latitude, longitude, 1)
        return if (addresses?.isNotEmpty() == true) {
            addresses[0].countryName ?: ""
        } else ""
    }

    fun getPinCodeFromLocation(context: Context, latitude: Double, longitude: Double): String {
        val gcd = Geocoder(context, Locale.getDefault())
        val addresses: List<Address>? = gcd.getFromLocation(latitude, longitude, 1)
        return if (addresses?.isNotEmpty() == true) {
            addresses[0].postalCode ?: ""
        } else ""
    }

    interface OnTextChange {
        fun onTextChange(v: View, text: String)
    }

    fun TextView.setOnTextChangeListener(listener: OnTextChange) {
        doOnTextChanged { text, _, _, _ ->
            listener.onTextChange(this, text.toString())
        }
    }

    fun Spinner.setOnTextChangeListener(list: List<String>, listener: OnTextChange) {
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long) {
                listener.onTextChange(this@setOnTextChangeListener, list[position])
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    fun Spinner.setOnPositionChangedListener(listener: (Int) -> Unit) {
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long) {
                listener(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }


    fun ViewPager2.setupViewPager() {
        clipToPadding = false; clipChildren = false
        offscreenPageLimit = 3
        val pageMarginPx = resources.getDimensionPixelOffset(R.dimen.dp_20)
        val offsetPx = resources.getDimensionPixelOffset(R.dimen.dp_10)
        setPageTransformer { page, position ->
            val viewPager = page.parent.parent as ViewPager2
            val offset = position * -(2 * offsetPx + pageMarginPx)
            if (viewPager.orientation == ViewPager2.ORIENTATION_HORIZONTAL) {
                if (ViewCompat.getLayoutDirection(viewPager) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                    page.translationX = -offset
                } else {
                    page.translationX = offset
                }
            } else {
                page.translationY = offset
            }
        }
    }

    fun captureView(view: View, bitmapCallback: (Bitmap) -> Unit) {
        val tBitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(tBitmap)
        view.draw(canvas)
        canvas.setBitmap(null)
        bitmapCallback.invoke(tBitmap)
    }

    fun captureView2(view: View, bitmapCallback: (Bitmap) -> Unit) {
        val window = (view.context as Activity).window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { // Above Android O, use PixelCopy
            val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
            val location = IntArray(2)
            view.getLocationInWindow(location)
            PixelCopy.request(window,
                Rect(location[0], location[1], location[0] + view.width, location[1] + view.height),
                bitmap,
                {
                    if (it == PixelCopy.SUCCESS) {
                        bitmapCallback.invoke(bitmap)
                    }
                },
                Handler(Looper.getMainLooper()))
        } else {
            val tBitmap = Bitmap.createBitmap(view.layoutParams.width,
                view.layoutParams.height,
                Bitmap.Config.ARGB_8888)
            val canvas = Canvas(tBitmap)
            view.draw(canvas) //canvas.setBitmap(null)
            bitmapCallback.invoke(tBitmap)
        }
    }

    fun captureView3(view: View, bitmapCallback: (Bitmap) -> Unit) {
        val bitmap = view.drawToBitmap(Bitmap.Config.ARGB_8888)
        bitmapCallback(bitmap)
    }

    fun linearLayoutManager(ctx: Context, divider: Float) =
        object : LinearLayoutManager(ctx, HORIZONTAL, false) {
            override fun checkLayoutParams(lp: RecyclerView.LayoutParams): Boolean {
                lp.width = (width / divider).toInt()
                return true
            }
        }

    fun getColor(resources: Resources, res: Int) =
        ResourcesCompat.getColor(resources, res, resources.newTheme())

    fun spinnerRecyclerAdapter(list: List<String?>?,
        lRes: Int = R.layout.item_text_view,
        onCardClicked: (Int) -> Unit): RecyclerView.Adapter<RecyclerView.ViewHolder> {
        val v = object : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup,
                viewType: Int): RecyclerView.ViewHolder {
                val view = LayoutInflater.from(parent.context).inflate(lRes, parent, false)
                return object : RecyclerView.ViewHolder(view) {}
            }

            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
                holder.itemView.findViewById<TextView>(R.id.tvTextView).text =
                    list?.get(position).splitCamelCase()
                holder.itemView.setOnClickListener { onCardClicked(position) }
            }

            override fun getItemCount() = list?.size ?: 0

        }
        return v
    }

    fun String?.splitCamelCase(): String {
        if (isNullOrEmpty()) return ""
        val spaceSeparated = trim().replace("_", " ")
        val stringBuilder = StringBuilder(spaceSeparated)
        stringBuilder.forEachIndexed { index, char ->
            if (char.toString() == " ") stringBuilder.setCharAt(index + 1,
                stringBuilder[index + 1].toUpperCase())
        }
        stringBuilder.setCharAt(0, stringBuilder[0].toUpperCase())
        return stringBuilder.toString()
    }

    fun String?.capitalize(): String {
        if (isNullOrEmpty()) return ""
        if (this.split(" ").size > 1) {
            val string = this.split(" ")
            var capitalizedString = ""
            string.forEach {
                it.capitalize()
                capitalizedString = "$capitalizedString $it"
            }
            return capitalizedString.trim()
        } else return this.capitalize(Locale.getDefault())
    }

    fun <T : ViewDataBinding> recyclerAdapter(list: List<Any>? = listOf(),
        layoutRes: Int,
        cb: (T, Int) -> Unit): RecyclerView.Adapter<RecyclerView.ViewHolder> {
        class MyHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)

        val v = object : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup,
                viewType: Int): RecyclerView.ViewHolder {
                val binding: ViewDataBinding =
                    DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                        layoutRes,
                        parent,
                        false)
                return MyHolder(binding)
            }

            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
                try {
                    cb((holder as? MyHolder)?.binding as T, position)
                } catch (e: ClassCastException) {
                    e.printStackTrace()
                }

            }

            override fun getItemCount() = list?.size ?: 0
        }
        return v
    }

    fun RecyclerView.onScrolledToBottom(listener: () -> Unit) {
        addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val offset = computeVerticalScrollOffset()
                val extent = computeVerticalScrollExtent()
                val range = computeVerticalScrollRange()
                if (offset + extent > range - 1000) listener()
            }
        })
    }

    fun Dialog?.pinViewToBottom(viewToPin: View, parent: ViewGroup) {
        (this as BottomSheetDialog?)?.behavior?.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {}
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                viewToPin.y =
                    ((bottomSheet.parent as View).height - bottomSheet.top - viewToPin.height).toFloat()
            }
        }.apply {
            parent.post { onSlide(parent as View, 0f) }
        })

    }

    fun Context.dipToPixels(dipValue: Float): Int =
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, resources.displayMetrics)
            .toInt()

    val Int.px: Int get() = (this * Resources.getSystem().displayMetrics.density).toInt()

    fun Int?.ifNotNullOrZero() = this != null && this != 0

    fun Int?.checkNull() = this ?: 0

    val Float.dp: Float get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, Resources.getSystem().displayMetrics)
}
