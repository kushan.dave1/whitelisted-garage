package whitelisted.garage.utils

import android.content.Context
import android.util.AttributeSet
import android.view.View

class CustomView: View, CustomViewInterface {

    override var borderWidth: Float = 1f
    override val view: View get() = this
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setBgDrawable(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        setBgDrawable(attrs)
    }

}