package whitelisted.garage.utils

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import whitelisted.garage.databinding.RecyclerViewScrollIndiactorBinding

class RecyclerViewScrollIndicator: FrameLayout {

    private lateinit var binding: RecyclerViewScrollIndiactorBinding
    constructor(context: Context) : super(context) {inflateView(context)}
    constructor(context: Context,attrs:AttributeSet) : super(context,attrs) {inflateView(context)}
    constructor(context: Context,attrs:AttributeSet,p0:Int) : super(context,attrs,p0) {inflateView(context)}

    private fun inflateView(context: Context) {
        binding = RecyclerViewScrollIndiactorBinding.inflate(LayoutInflater.from(context),this,true)
    }

    fun attachToRecyclerView(recyclerView: RecyclerView) {
        val indicatorWidthPercentage = (binding.indicator.width.toFloat() / width.toFloat())*100.0f
        recyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val offset = recyclerView.computeHorizontalScrollOffset()
                val extent = recyclerView.computeHorizontalScrollExtent()
                val range = recyclerView.computeHorizontalScrollRange()

                // get what percent the recyclerView got scrolled
                var recyclerViewScrolledPercentage = 100.0f * offset / (range - extent).toFloat()

                // get what percent inner indicator width is of full indicator width
                val innerIndicatorWidthPercentage = ((recyclerViewScrolledPercentage * indicatorWidthPercentage) / 100)

                // get scroll position by deducting inner indicator position from actual scrolled position, so that...
                recyclerViewScrolledPercentage -= innerIndicatorWidthPercentage

                // finally update the position of indicator in x direction
                val x = getScrollIndicatorPercentage(recyclerViewScrolledPercentage)
                if(x != 0f) binding.indicator.x = x
            }
            private fun getScrollIndicatorPercentage(percentage: Float): Float {
                return (width * percentage) / 100
            }
        })
    }

}