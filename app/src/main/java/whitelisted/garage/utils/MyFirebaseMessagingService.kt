package whitelisted.garage.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.moengage.firebase.MoEFireBaseHelper
import com.moengage.pushbase.MoEPushHelper
import whitelisted.garage.R
import whitelisted.garage.view.activities.DashboardActivity
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        remoteMessage.let {
            if (MoEPushHelper.getInstance()
                    .isFromMoEngagePlatform(remoteMessage.data) && !MoEPushHelper.getInstance()
                    .isSilentPush(remoteMessage.data)) { //  FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseEventNameENUM.FBE_MOENGAGE_NOTIFICATION, Bundle())
                MoEFireBaseHelper.Companion.getInstance()
                    .passPushPayload(applicationContext, remoteMessage.data)
            } else {
                var notificationData: Map<String, String>? // Check if message contains a data payload.
                remoteMessage.data.isNotEmpty().let {
                    notificationData = remoteMessage.data
                }

                // Check if message contains a notification payload.
                remoteMessage.notification?.let {
                    if (notificationData?.containsKey("action") == true) {
                        val action = notificationData?.get("action")
                        if (action.equals("whitelisted")) {
                            sendNotification(it, notificationData)
                        }
                    }
                } ?: run {
                    sendNotification(notificationData)
                }
            }
        }
    }

    override fun onNewToken(token: String) {
        MoEFireBaseHelper.getInstance().passPushToken(applicationContext, token)
    }

    private fun sendNotification(message: RemoteMessage.Notification,
        notificationData: Map<String, String>?) {
        val intent = Intent(this, DashboardActivity::class.java)
        notificationData?.isNotEmpty()?.run {
            val deeplinkURL = notificationData["url"]
            Log.d("deeplinkNoti", deeplinkURL ?: "")
            intent.data = Uri.parse(deeplinkURL)
        } //            .apply { //            putExtra(COME_FROM, COME_FROM_NOTIFICATION) //            message.clickAction?.let {
        //                putExtra(NOTIFICATION_TYPE, it)
        //            }
        //        }
        //        notificationData?.isNotEmpty()?.run {
        //            if (message.clickAction == NOTIFICATION_TYPE_ORDER_STATUS) {
        //                //  val orderId = notificationData["order_id"]
        //                //  intent.putExtra(ORDER_ID, orderId)
        //                val deepLinkURL = notificationData["deeplinkurl"]
        //                intent.putExtra("branch", deepLinkURL)
        //                intent.data = Uri.parse(deepLinkURL)
        //                intent.putExtra("branch_force_new_session", true)
        //                val linkURL = Uri.parse(deepLinkURL)
        //                if (linkURL?.authority.equals("gomechanic.in")) {
        //                    if (linkURL.pathSegments[0] == "order") {
        //                        if (linkURL.pathSegments[1].isNotEmpty()) {
        //                            val orderId = linkURL.pathSegments[1]
        //                            intent.putExtra(ORDER_ID, orderId)
        //                        }
        //                    } else if (linkURL.pathSegments[0] == "show-all-cart") {
        //                        val url = linkURL.pathSegments[0]
        //                        intent.putExtra(CUSTOM_NOTIFICATION_URL, url)
        //                    }
        //                }
        //            }
        //        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE)
        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL).setContentTitle(message.title)
            .setContentText(message.body).setAutoCancel(true).setSound(defaultSoundUri)
            .setContentIntent(pendingIntent).setSmallIcon(R.drawable.ic_notif_easy_garage_logo)
            .setColor(ContextCompat.getColor(this, R.color.colorAccent))
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationData?.get("image")?.let { imageUrl ->
            getBitmapFromUrl(imageUrl)?.let { bitmap ->
                notificationBuilder.setStyle(NotificationCompat.BigPictureStyle().bigPicture(bitmap)
                    .bigLargeIcon(null)).setLargeIcon(bitmap)
            }
        }
        notificationManager.notify(0 /* ID of notification */,
            notificationBuilder.build()) //        val bundle = Bundle()
        //        bundle.putString(FirebaseEventNameENUM.FBE_NOTIFICATION_TITLE, message.title)
        //        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseEventNameENUM.FBE_NOTIFICATION_CLICK, bundle)
    }

    private fun sendNotification(notificationData: Map<String, String>?) {
        notificationData?.let { mapData ->
            val title = mapData["title"] ?: getString(R.string.app_name)
            val url = mapData["url"]
            val body = mapData["body"] ?: ""
            val intent = Intent(this, DashboardActivity::class.java)
            val deeplinkURL = notificationData["url"]
            if (!deeplinkURL.isNullOrEmpty()) {
                Log.d("deeplinkNoti", deeplinkURL)
                intent.data = Uri.parse(deeplinkURL)
            }

            //            intent.putExtra(CUSTOM_NOTIFICATION_URL, url) //            intent.putExtra("branch", url) //            intent.putExtra("deeplink", url)
            //            intent.putExtra("branch_force_new_session", true)
            //            url?.let {
            //                intent.data = Uri.parse(url)
            //            }
            //            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val pendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE)
            val channelId = getString(R.string.default_notification_channel_id)
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL).setContentTitle(title)
                .setContentText(body).setAutoCancel(true).setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setStyle(NotificationCompat.BigTextStyle().bigText(body))
                .setSmallIcon(R.drawable.ic_notif_easy_garage_logo)
                .setColor(ContextCompat.getColor(this, R.color.colorAccent))
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT)
                notificationManager.createNotificationChannel(channel)
            }
            mapData["image"]?.let { imageUrl ->
                getBitmapFromUrl(imageUrl)?.let { bitmap ->
                    notificationBuilder.setStyle(NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap).bigLargeIcon(null)).setLargeIcon(bitmap)
                }
            }

            notificationManager.notify(0,
                notificationBuilder.build()) //            val bundle = Bundle()
            //            bundle.putString(FirebaseEventNameENUM.FBE_NOTIFICATION_TITLE, title)
            //            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseEventNameENUM.FBE_NOTIFICATION_CLICK, bundle)
        }
    }

    private fun getBitmapFromUrl(imageUrl: String): Bitmap? {
        return try {
            val url = URL(imageUrl)
            val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input: InputStream = connection.inputStream
            BitmapFactory.decodeStream(input)
        } catch (e: Exception) {
            null
        }
    }


}