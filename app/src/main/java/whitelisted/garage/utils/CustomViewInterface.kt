package whitelisted.garage.utils

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.view.ViewOutlineProvider
import androidx.core.content.res.ResourcesCompat
import androidx.core.content.withStyledAttributes
import whitelisted.garage.R

interface CustomViewInterface {

    var borderWidth: Float
    val view: View


    fun setBgDrawable(attrs: AttributeSet?) {

        val backgroundDrawable = GradientDrawable()
        view.context.withStyledAttributes(attrs, R.styleable.CustomView) {

            val backgroundColor = if (hasValue(R.styleable.CustomView_viewBackgroundColor)) {
                getColorStateList(R.styleable.CustomView_viewBackgroundColor)
            } else ColorStateList.valueOf(
                ResourcesCompat.getColor(
                    resources,
                    android.R.color.transparent,
                    resources.newTheme()
                )
            )


            val radius = getDimension(R.styleable.CustomView_viewCornerRadius, 0f)
            val topLeftRadius = getDimension(R.styleable.CustomView_viewRadiusTopLeft, 0f)
            val topRightRadius = getDimension(R.styleable.CustomView_viewRadiusTopRight, 0f)
            val bottomLeftRadius = getDimension(R.styleable.CustomView_viewRadiusBottomLeft, 0f)
            val bottomRightRadius = getDimension(R.styleable.CustomView_viewRadiusBottomRight, 0f)
            val borderColor = getColor(R.styleable.CustomView_viewBorderColor, 0)
            val dashWidth = getDimension(R.styleable.CustomView_borderDashWidth, 0f)
            val dashGap = getDimension(R.styleable.CustomView_borderDashGap, 0f)

            borderWidth = getDimension(R.styleable.CustomView_viewBorderWidth, 1f)

            val startColor = getColor(R.styleable.CustomView_viewStartColor, -1)
            val midColor = getColor(R.styleable.CustomView_viewMidColor, -1)
            val endColor = getColor(R.styleable.CustomView_viewEndColor, -1)

            val orientation = getInt(R.styleable.CustomView_viewGradientOrientation, 0)



            backgroundDrawable.color = backgroundColor
            if(borderWidth != 0f) backgroundDrawable.setStroke(
                borderWidth.toInt(),
                ColorStateList.valueOf(borderColor),
                dashWidth,
                dashGap
            )

            backgroundDrawable.cornerRadii = floatArrayOf(
                topLeftRadius,
                topLeftRadius,
                topRightRadius,
                topRightRadius,
                bottomRightRadius,
                bottomRightRadius,
                bottomLeftRadius,
                bottomLeftRadius
            )

            if (radius != 0f) backgroundDrawable.cornerRadius = radius


            if (startColor != -1 && endColor != -1) {
                backgroundDrawable.colors =
                    if (midColor != -1) arrayOf(startColor, midColor, endColor).toIntArray()
                    else arrayOf(startColor, endColor).toIntArray()
                backgroundDrawable.orientation = when(orientation) {
                    0 -> GradientDrawable.Orientation.LEFT_RIGHT
                    1 -> GradientDrawable.Orientation.TOP_BOTTOM
                    else -> GradientDrawable.Orientation.TL_BR
                }

            }

            view.background = backgroundDrawable
            view.outlineProvider = ViewOutlineProvider.BACKGROUND

            view.clipToOutline = true

        }

    }

    fun setOutlineProvider(w: Int, h: Int) {
        view.outlineProvider = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            object : ViewOutlineProvider() {
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setRect(0, 0, w, h)
                }
            }
        } else ViewOutlineProvider.BACKGROUND
        view.clipToOutline = true
    }

    fun setCornerRadius(radius: Float) {
        (view.background as? GradientDrawable)?.apply {
            cornerRadius = radius
            view.background = this
        }
        view.invalidate()
    }

    fun setCornerRadii(leftTop: Float, rightTop: Float, rightBottom: Float, leftBottom: Float) {
        (view.background as? GradientDrawable)?.apply {
            cornerRadii = floatArrayOf(
            leftTop, leftTop,
            rightTop, rightTop,
            rightBottom, rightBottom,
            leftBottom, leftBottom
        )
            view.background = this
        }
        view.invalidate()
    }

    fun setViewBorderColor(color: Int) {

        (view.background as? GradientDrawable)?.apply {

            setStroke(
                borderWidth.toInt(),
                ColorStateList.valueOf(
                    ResourcesCompat.getColor(
                        view.resources,
                        color,
                        view.resources.newTheme()
                    )
                )
            )
            view.background = this
        }
        view.invalidate()
    }

    fun setViewBackgroundColor(string: String?) {
        (view.background as? GradientDrawable)?.apply {
            this.color = ColorStateList.valueOf(Color.parseColor(string))
        }
        view.invalidate()
    }

    fun setViewBackgroundColor(color: Int) {
        (view.background as? GradientDrawable)?.apply {
            this.color = ColorStateList.valueOf(
                ResourcesCompat.getColor(
                    view.resources,
                    color,
                    view.resources.newTheme()
                )
            )
        }
        view.invalidate()
    }

    fun setBorder(width: Float, color: Int, radius: Float) {
        borderWidth = width
        (view.background as? GradientDrawable)?.apply {
            setStroke(
                borderWidth.toInt(),
                ColorStateList.valueOf(
                    ResourcesCompat.getColor(
                        view.resources,
                        color,
                        view.resources.newTheme()
                    )
                )
            )
            cornerRadius = radius
            view.background = this
        }
        view.invalidate()
    }

    fun makeCircular() {
        (view.background as? GradientDrawable)?.apply {
            val radius = if(view.width < view.height) view.width else view.height
            this.cornerRadius = radius.toFloat()/2
        }
        view.invalidate()
    }

}