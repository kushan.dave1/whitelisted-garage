package whitelisted.garage.utils

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat

/**
 * Change the color of a part of the text contained in this textView
 *
 * @param subStringToColorize has to already be set in the textView's text
 * @param colorResId
 */
fun TextView.colorize(subStringToColorize: String,
    @ColorRes colorResId: Int,
    ignoreCase: Boolean = false) {

    val spannable: Spannable = SpannableString(text)

    val startIndex = text.indexOf(subStringToColorize, startIndex = 0, ignoreCase)
    val endIndex = startIndex + subStringToColorize.length

    val color: Int = ContextCompat.getColor(context, colorResId)

    if (startIndex != -1) {
        spannable.setSpan(ForegroundColorSpan(color),
            startIndex,
            endIndex,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        setText(spannable, TextView.BufferType.SPANNABLE)
    }
}