package whitelisted.garage.utils

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class PaginationScrollListenerGridLayout(private val recyclerView: RecyclerView) :
    RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val offset = recyclerView.computeVerticalScrollOffset()
        val extent = recyclerView.computeVerticalScrollExtent()
        val range  = recyclerView.computeVerticalScrollRange()
        if(offset + extent == range) loadMoreItems()

        /*val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
        if (!isLoading && !isLastPage) {
            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                loadMoreItems()
            }
        }*/
        if (dy>0){
            downScroll()
        }else{
            upScroll()
        }
//        val currentFirstVisible: Int = layoutManager.findFirstVisibleItemPosition()
//        if (currentFirstVisible > firstVisibleInListview) downScroll() else upScroll()
//        firstVisibleInListview = currentFirstVisible
    }

    protected open fun upScroll() {}
    protected open fun downScroll() {}
    protected abstract fun loadMoreItems()
    abstract val isLastPage: Boolean
    abstract val isLoading: Boolean

}