package whitelisted.garage.utils

import android.net.Uri
import android.os.Bundle
import whitelisted.garage.base.ActionListener
import whitelisted.garage.base.BaseBottomSheetFragment
import whitelisted.garage.base.BaseDialogFragment
import whitelisted.garage.base.BaseFragment
import whitelisted.garage.view.bottomSheet.*
import whitelisted.garage.view.dialog.*
import whitelisted.garage.view.fragments.AddEditAddressFragment
import whitelisted.garage.view.fragments.SubscriptionInfoFragment
import whitelisted.garage.view.fragments.account.AccountFragment
import whitelisted.garage.view.fragments.account.BusinessCardFragment
import whitelisted.garage.view.fragments.addEditEmployee.AddEditEmployeeFragment
import whitelisted.garage.view.fragments.addressAndMap.AddressPickerFragment
import whitelisted.garage.view.fragments.addressAndMap.MapFragment
import whitelisted.garage.view.fragments.billing.BillingFragment
import whitelisted.garage.view.fragments.bottomNavFragment.BottomNavFragment
import whitelisted.garage.view.fragments.createEditPackage.CreateEditAccessoriesPackageFragment
import whitelisted.garage.view.fragments.createEditPackage.CreateEditPackageFragment
import whitelisted.garage.view.fragments.createPackages.CreatePackagesFragment
import whitelisted.garage.view.fragments.enquiryOrderRequests.EnquiryOrderRequestsFragment
import whitelisted.garage.view.fragments.feedback.FeedbackFragment
import whitelisted.garage.view.fragments.home.HomeFragment
import whitelisted.garage.view.fragments.home.goCoins.GoCoinsFragment
import whitelisted.garage.view.fragments.home.goCoins.goCoinsTransactions.GoCoinsTransactionsFragment
import whitelisted.garage.view.fragments.inventory.*
import whitelisted.garage.view.fragments.language.LanguageSelectionFragment
import whitelisted.garage.view.fragments.ledger.LedgerFragment
import whitelisted.garage.view.fragments.ledger.giveortakeledger.GiveOrTakeLedgerFragment
import whitelisted.garage.view.fragments.login.LoginFragment
import whitelisted.garage.view.fragments.manageEmployee.EmployeePerformanceFragment
import whitelisted.garage.view.fragments.manageEmployee.ManageEmployeeFragment
import whitelisted.garage.view.fragments.managePackages.ManagePackagesFragment
import whitelisted.garage.view.fragments.myBusiness.*
import whitelisted.garage.view.fragments.mycustomer.MyCustomerFragment
import whitelisted.garage.view.fragments.newOrderDetail.NewOrderDetailFragment
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment
import whitelisted.garage.view.fragments.orderEstimate.OrderEstimatesFragment
import whitelisted.garage.view.fragments.orderHistory.OrderHistoryFragment
import whitelisted.garage.view.fragments.orderInclusions.OrderInclusionsFragment
import whitelisted.garage.view.fragments.orderInventory.TakeSignatureFragment
import whitelisted.garage.view.fragments.orderSummery.OrderSummaryFragment
import whitelisted.garage.view.fragments.ordersList.OrdersListFragment
import whitelisted.garage.view.fragments.otp.OTPFragment
import whitelisted.garage.view.fragments.payment.AddPaymentFragment
import whitelisted.garage.view.fragments.payment.PaymentFragment
import whitelisted.garage.view.fragments.placeBid.PlaceBidFragment
import whitelisted.garage.view.fragments.qrCode.QRCodeFragment
import whitelisted.garage.view.fragments.referAndEarn.FAQsFragment
import whitelisted.garage.view.fragments.referAndEarn.ReferAndEarnFragment
import whitelisted.garage.view.fragments.reminder.CustomerRemindersFragment
import whitelisted.garage.view.fragments.reminder.RemindersFragment
import whitelisted.garage.view.fragments.resetPassword.ResetPasswordFragment
import whitelisted.garage.view.fragments.retailAddPart.RetailOrderAddPartFragment
import whitelisted.garage.view.fragments.retailNewOrderDetail.RetailNewOrderDetailFragment
import whitelisted.garage.view.fragments.searchOrders.SearchOrdersFragment
import whitelisted.garage.view.fragments.selectAddress.SelectAddressFragment
import whitelisted.garage.view.fragments.setting.EditProfileFragment
import whitelisted.garage.view.fragments.setting.ProfileFragment
import whitelisted.garage.view.fragments.setting.SettingFragment
import whitelisted.garage.view.fragments.signup.SignUpFragment
import whitelisted.garage.view.fragments.sparesShop.*
import whitelisted.garage.view.fragments.splash.SplashFragment
import whitelisted.garage.view.fragments.webViewFragment.WebViewFragment
import whitelisted.garage.view.fragments.workshopBidding.*
import whitelisted.garage.view.fragments.workshops.addWorkshop.AddWorkshopFragment
import whitelisted.garage.view.fragments.workshops.viewEditWorkshop.ViewEditWorkshopFragment
import whitelisted.garage.view.fragments.workshops.workshopList.WorkshopsListFragment

class FragmentFactory private constructor() {
    init {
        throw IllegalStateException("FragmentFactory class")
    }

    abstract class Fragments private constructor() {
        init {
            throw IllegalStateException("Screens class")
        }

        companion object {
            const val HOME_TABS = 0
            const val SPARES_TABS = 1
            const val INVENTORY_TABS = 2
            const val INVENTORY_TABS_INTERNATIONAL = 3
            const val BILLING_TABS = 4
            const val ACCOUNT_TABS = 5

            const val LOGIN = "LOGIN"
            const val ORDER_SUMMARY = "ORDER_SUMMARY"
            const val ORDER_HISTORY = "ORDER_HISTORY"
            const val SETTINGS = "SETTINGS"
            const val EDIT_PROFILE = "EDIT_PROFILE"
            const val PROFILE = "PROFILE"
            const val OTP_FRAGMENT = "OTP"
            const val SIGN_UP = "SIGN_UP"
            const val FORGOT_PASSWORD = "FORGOT_PASSWORD"
            const val RESET_PASSWORD = "RESET_PASSWORD"
            const val HOME = "HOME"
            const val ORDER_SPARES = "ORDER_SPARES"
            const val BILLING = "BILLING"
            const val ACCOUNT = "ACCOUNT"
            const val ORDER_DETAIL_WRAPPER = "ORDER_DETAIL_WRAPPER"
            const val NEW_ORDER_DETAIL = "NEW_ORDER_DETAIL"
            const val ORDER_INVENTORY_FRAGMENT = "ORDER_INVENTORY_FRAGMENT"
            const val JOB_CARD_FRAGMENT = "JOB_CARD_FRAGMENT"
            const val TAKE_SIGNATURE = "TAKE_SIGNATURE"
            const val ORDER_ESTIMATES = "ORDER_ESTIMATES"
            const val ORDER_ESTIMATES_EDIT = "ORDER_ESTIMATES_EDIT"
            const val PAYMENT_FRAGMENT = "PAYMENT_FRAGMENT"
            const val ADD_PAYMENT_FRAGMENT = "ADD_PAYMENT_FRAGMENT"
            const val ADD_EDIT_EMPLOYEE = "ADD_EDIT_EMPLOYEE"
            const val MANAGE_EMPLOYEE = "MANAGE_EMPLOYEE"
            const val LEDGER = "LEDGER"
            const val GIVE_OR_TAKE = "GIVE_OR_TAKE"
            const val MANAGE_PACKAGES = "MANAGE_PACKAGES"
            const val MY_CUSTOMER = "MY_CUSTOMER"
            const val CREATE_PACKAGES = "CREATE_PACKAGES"
            const val CREATE_EDIT_PACKAGE = "CREATE_PACKAGE"
            const val CREATE_EDIT_ACCESS_PACKAGE = "CREATE_EDIT_RETAIL_PACKAGE"

            const val WORKSHOP_LIST = "WORKSHOP_LIST"
            const val ADD_WORKSHOP = "ADD_WORKSHOP"
            const val VIEW_EDIT_WORKSHOP = "VIEW_EDIT_WORKSHOP"

            const val REMINDERS = "REMINDERS"

            const val QR_CODE = "QR_CODE"
            const val EDIT_UPI_ID = "EDIT_UPI_ID"
            const val VERIFY_OTP_SHEET = "VERIFY_OTP_SHEET"

            const val ORDERS_LIST = "ORDERS_LIST"
            const val SEARCH_ORDERS = "SEARCH_ORDERS"

            const val WEB_VIEW = "WEB_VIEW"
            const val ADDRESS_FRAGMENT = "ADDRESS_FRAGMNENT"
            const val MAP_FRAGMENT = "MAP_FRAGMENT"
            const val FEEDBACK_FRAGMENT = "FEEDBACK_FRAGMENT"
            const val LANGUAGE_SELECTION_FRAGMENT = "LanguageSelectionFragment"
            const val MY_BUSINESS_FRAGMENT = "MY_BUSINESS_FRAGMENT"
            const val MY_WEBSITE_FRAGMENT = "MY_WEBSITE_FRAGMENT"
            const val VIEW_GMB_FRAGMENT = "VIEW_GMB_FRAGMENT"
            const val EDIT_GMB_FRAGMENT = "EDIT_GMB_FRAGMENT"
            const val EDIT_WEBSITE_FRAGMENT = "EDIT_WEBSITE_FRAGMENT"
            const val VIEW_WEBSITE_FRAGMENT = "VIEW_WEBSITE_FRAGMENT"
            const val Edit_Business_Garage_Name_Fragment = "Edit_Business_Garage_Name_Fragment"
            const val Edit_Business_Category_Fragment = "EditBusinessCategoryFragment"
            const val Edit_Business_Photo_Fragment = "Edit_Business_Photo_Fragment"
            const val Edit_Business_Timings_Fragment = "Edit_Business_Timings_Fragment"
            const val Edit_Business_Attributes_Fragment = "Edit_Business_Attributes_Fragment"
            const val INVENTORY_IMAGE_PREVIEW = "INVENTORY_IMAGE_PREVIEW"
            const val GMB_UnLock_Fragment = "GMBUnLockFragment"
            const val WEBSITE_UnLock_Fragment = "WEBSITE_UnLock_Fragment"
            const val CHOOSE_IMAGE = "CHOOSE_IMAGE"
            const val GO_COINS = "GO_COINS"
            const val GO_COIN_TRANSACTIONS = "GO_COIN_TRANSACTIONS"
            const val RESET_PIN = "RESET_PIN"
            const val LANGUAGE_BOTTOM = "LANGUAGE_BOTTOM"
            const val INVENTORY_FRAGMENT = "RETAIL_INVENTORY_FRAGMENT"
            const val ADD_RACK_FRAGMENT = "ADD_RACK_FRAGMENT"
            const val ADD_RACK_ITEM_FRAGMENT = "ADD_RACK_ITEM_FRAGMENT"
            const val Edit_RACK_ITEM_FRAGMENT = "Edit_RACK_ITEM_FRAGMENT"

            const val RETAIL_NEW_ORDER = "RETAIL_NEW_ORDER"
            const val RETAIL_ORDER_INCLUSIONS = "RETAIL_ORDER_INCLUSIONS"
            const val RETAIL_ADD_PART =
                "RETAIL_ADD_PART" //            const val ORDER_INCLUSIONS_EDIT = "ORDER_INCLUSIONS_EDIT"

            const val COMPLETE_REGISTRATION = "COMPLETE_REGISTRATION"
            const val INSTANT_REMINDER = "INSTANT_REMINDER"
            const val CANCEL_WS_BID = "CANCEL_WS_BID"
            const val WEBSITE_INFO = "WEBSITE_INFO"
            const val GMB_INFO = "GMB_INFO"
            const val REFER_AND_EARN = "REFER_AND_EARN"
            const val FQA_S = "FQA_S"
            const val GMB_PREVIEW = "GMB_PREVIEW"
            const val SELECT_COUNTRY_CODE = "SELECT_COUNTRY_CODE"
            const val CUSTOMER_REMINDER_FRAGMENT = "CUSTOMER_REMINDER_FRAGMENT"
            const val WORKSHOP_BIDDING_ADD_PART_FRAGMENT = "WORKSHOP_BIDDING_ADD_PART_FRAGMENT"
            const val WORKSHOP_BIDDING_CART_DETAILS_FRAGMENT =
                "WORKSHOP_BIDDING_CART_DETAILS_FRAGMENT"
            const val WORKSHOP_BIDDING_STATUS_FRAGMENT = "WORKSHOP_BIDDING_STATUS_FRAGMENT"
            const val WORKSHOP_BIDDING_LIST_FRAGMENT = "WORKSHOP_BIDDING_LIST_FRAGMENT"
            const val ENQUIRY_ORDER_REQUESTS = "ENQUIRY_ORDER_REQUESTS"
            const val WORKSHOP_BID_RECEIVED_FRAGMENT = "WORKSHOP_BID_RECEIVED_FRAGMENT"
            const val WORKSHOP_BID_ENQUIRY_FRAG = "WORKSHOP_BID_ENQUIRY_FRAG"
            const val PLACE_BID = "PLACE_BID"
            const val WORKSHOP_ADDRESS_NOT_AVL = "PLACE_BID"
            const val INVENTORY_BULK_UPLOAD = "INVENTORY_BULK_UPLOAD"
            const val FRAGMENT_BUSINESS_CARD = "FRAGMENT_BUSINESS_CARD"
            const val SPARES_SHOP = "SPARES_SHOP"
            const val EMPLOYEE_PERFORMANCE = "EMPLOYEE_PERFORMANCE"
            const val SHOP_WITH_FILTER = "SHOP_WITH_FILTER"
            const val SS_CART = "SS_CART"
            const val PAYMENT_FAILED = "PAYMENT_FAILED"
            const val SELECT_ADDRESS = "SELECT_ADDRESS"
            const val ADD_EDIT_ADDRESS = "ADD_EDIT_ADDRESS"

            const val SPARES_ORDER_HISTORY = "SPARES_PURCHASE_HISTORY"
            const val SPARES_ENQUIRIES = "SPARES_ENQUIRIES"
            const val SPARES_ORDER_DETAIL = "SPARES_ORDER_DETAIL"

            const val SPARES_PURCHASE_SUMMARY = "SPARES_PURCHASE_SUMMARY"
            const val SPARES_PRODUCT_DETAILS = "SPARES_PRODUCT_DETAILS"
            const val SPARES_PRODUCT_ZOOM = "SPARES_PRODUCT_ZOOM"
            const val SS_PAYMENT_TYPE = "SS_PAYMENT_TYPE"
            const val SEARCH_SS_ITEMS = "SEARCH_SS_ITEMS"
            const val SELECT_CAR_AND_SEARCH = "SELECT_CAR_AND_SEARCH"
            const val FILTER_AND_SORT_FRAGMENT = "FILTERANDSORTFRAGMENT"
            const val BOTTOM_NAV = "BOTTOM_NAV"
            const val SPLASH = "SPLASH"
            const val SELECT_CATEGORY = "SELECT_CATEGORY"
            const val SELECT_CATEGORY_OR_BRAND = "SELECT_CATEGORY_OR_BRAND"
            const val SEND_ENQUIRY = "SEND_ENQUIRY"
            const val PRO_LOCKED_FRAGMENT = "PRO_LOCKED_FRAGMENT"
            const val SUBSCRIPTION_INFO_FRAGMENT = "SUBSCRIPTION_INFO_FRAGMENT"
        }
    }

    abstract class Dialogs private constructor() {
        init {
            throw IllegalStateException("Dialogs class")
        }

        companion object {
            const val SELECT_WORKSHOP_LIST = "SELECT_WORKSHOP_LIST"
            const val SELECT_WORK_DONE = "SELECT_WORK_DONE"
            const val ADD_ITEM = "ADD_ITEM"
            const val ADD_REMARK_PHOTO = "ADD_REMARK_PHOTO"
            const val GENERIC_CONFIRMATION = "GENERIC_CONFIRMATION"
            const val SELECT_DATE = "SELECT_DATE"
            const val SELECT_TIME = "SELECT_TIME"
            const val SELECT_DATE_REMINDER = "SELECT_DATE_REMINDER"
            const val SELECT_CAR = "SELECT_CAR"
            const val SELECT_RACK_ITEM = "SELECT_RACK_ITEM"
            const val SELECT_WORKSHOP_PART_NAME = "SELECT_WORKSHOP_PART_NAME"
            const val CUSTOM_AMOUNT = "CUSTOM_AMOUNT"
            const val APPLY_COUPON_DIALOG = "APPLY_COUPON_DIALOG"
            const val APPLY_MARKETPLACE_COUPON_DIALOG = "APPLY_MARKETPLACE_COUPON_DIALOG"
            const val INSUFFICIENT_BALANCE_DIALOG = "INSUFFICIENT_BALANCE_DIALOG"
            const val SELECT_LANGUAGE_POP_UP = "SELECT_LANGUAGE_POP_UP"
            const val REMINDER_SENT_SUCCESS = "REMINDER_SENT_SUCCESS"
            const val ADD_EDIT_CUSTOMER_DETAILS = "ADD_EDIT_CUSTOMER_DETAILS"
            const val VIDEO_VIEW ="VIDEO_VIEW"
            const val MEMBERSHIP_PURCHASED_DIALOG ="MEMBERSHIP_SUCCESS_DIALOG"
        }
    }

    companion object {

        /**
         * Returns fragment either from not or base app
         */
        fun fragment(fragmentName: String, params: Bundle? = null): BaseFragment? {

            if (fragmentName.isNotEmpty()) {
                var baseFragment: BaseFragment? = null
                if (fragmentName.contains("http://") || fragmentName.contains("https://")) {

                    val uri = Uri.parse(fragmentName)
                    val pathValue = uri.pathSegments
                    val screenName = ""

                    if (pathValue != null && pathValue.isNotEmpty()) {
                        baseFragment = getFragmentFromAppPackage(screenName)?.apply {
                            arguments = params ?: Bundle()
                        }
                    }
                } else {
                    baseFragment = getFragmentFromAppPackage(fragmentName)?.apply {
                        arguments = params ?: Bundle()
                    }
                }
                return baseFragment
            } else {
                return null
            }

        }

        /**
         * Returns fragment either from not or base app
         */
        fun fragmentDialog(fragmentName: String,
            params: Bundle? = null,
            listener: ActionListener?=null): BaseDialogFragment? {

            if (fragmentName.isNotEmpty()) {
                var baseFragment: BaseDialogFragment? = null
                if (fragmentName.contains("http://") || fragmentName.contains("https://")) {

                    val uri = Uri.parse(fragmentName)
                    val pathValue = uri.pathSegments
                    val screenName = ""

                    if (pathValue != null && pathValue.isNotEmpty()) {
                        baseFragment =
                            getDialogFragmentFromAppPackage(screenName, listener)?.apply {
                                arguments = params ?: Bundle()
                            }
                    }
                } else {
                    baseFragment = getDialogFragmentFromAppPackage(fragmentName, listener)?.apply {
                        arguments = params ?: Bundle()
                    }
                }
                return baseFragment
            } else {
                return null
            }

        }

        /**
         * Returns fragment from base application
         */
        private fun getFragmentFromAppPackage(fragment: String,
            actionListener: ActionListener? = null): BaseFragment? {

            return when (fragment) {
                Fragments.LOGIN -> LoginFragment()
                Fragments.SIGN_UP -> SignUpFragment()
                Fragments.OTP_FRAGMENT -> OTPFragment() //                Fragments.FORGOT_PASSWORD -> ForgotPasswordFragment()
                Fragments.RESET_PASSWORD -> ResetPasswordFragment()

                Fragments.HOME -> HomeFragment() //                Fragments.ORDER_SPARES -> OrderSparesFragment()
                Fragments.BILLING -> BillingFragment()
                Fragments.ACCOUNT -> AccountFragment()
                Fragments.ORDER_DETAIL_WRAPPER -> OrderDetailWrapperFragment()
                Fragments.NEW_ORDER_DETAIL -> NewOrderDetailFragment() //                Fragments.ORDER_INVENTORY_FRAGMENT -> OrderInventoryFragment()
                //                Fragments.JOB_CARD_FRAGMENT -> JobCardFragment()
                Fragments.TAKE_SIGNATURE -> TakeSignatureFragment()
                Fragments.ORDER_ESTIMATES -> OrderEstimatesFragment()
                Fragments.PAYMENT_FRAGMENT -> PaymentFragment()
                Fragments.ADD_EDIT_EMPLOYEE -> AddEditEmployeeFragment()
                Fragments.MANAGE_EMPLOYEE -> ManageEmployeeFragment()
                Fragments.LEDGER -> LedgerFragment()
                Fragments.SETTINGS -> SettingFragment()
                Fragments.EDIT_PROFILE -> EditProfileFragment()
                Fragments.PROFILE -> ProfileFragment()
                Fragments.GIVE_OR_TAKE -> GiveOrTakeLedgerFragment()
                Fragments.MANAGE_PACKAGES -> ManagePackagesFragment()
                Fragments.MY_CUSTOMER -> MyCustomerFragment()
                Fragments.CREATE_PACKAGES -> CreatePackagesFragment()
                Fragments.CREATE_EDIT_PACKAGE -> CreateEditPackageFragment()
                Fragments.CREATE_EDIT_ACCESS_PACKAGE -> CreateEditAccessoriesPackageFragment()
                Fragments.WORKSHOP_LIST -> WorkshopsListFragment()
                Fragments.ADD_WORKSHOP -> AddWorkshopFragment()
                Fragments.VIEW_EDIT_WORKSHOP -> ViewEditWorkshopFragment()
                Fragments.REMINDERS -> RemindersFragment()
                Fragments.QR_CODE -> QRCodeFragment()

                Fragments.ORDERS_LIST -> OrdersListFragment()
                Fragments.SEARCH_ORDERS -> SearchOrdersFragment()
                Fragments.ORDER_HISTORY -> OrderHistoryFragment()
                Fragments.WEB_VIEW -> WebViewFragment()
                Fragments.ORDER_SUMMARY -> OrderSummaryFragment()
                Fragments.ADDRESS_FRAGMENT -> AddressPickerFragment()
                Fragments.MAP_FRAGMENT -> MapFragment()
                Fragments.FEEDBACK_FRAGMENT -> FeedbackFragment()
                Fragments.LANGUAGE_SELECTION_FRAGMENT -> LanguageSelectionFragment() //                Fragments.MY_BUSINESS_FRAGMENT -> MyBusinessFragment()
                //                Fragments.MY_WEBSITE_FRAGMENT -> MyWebsiteFragment()
                Fragments.VIEW_GMB_FRAGMENT -> ViewGmbWebsiteComboFragment()
                Fragments.EDIT_GMB_FRAGMENT -> EditGmbWebsiteComboFragment()
                Fragments.Edit_Business_Garage_Name_Fragment -> EditBusinessGarageNameFragment()
                Fragments.Edit_Business_Photo_Fragment -> EditBusinessPhotoFragment()

                Fragments.INVENTORY_IMAGE_PREVIEW -> InventoryImagePreviewFragment()
                Fragments.GO_COINS -> GoCoinsFragment()
                Fragments.GMB_UnLock_Fragment -> GmbWebsiteComboUnLockFragment()
                Fragments.GO_COIN_TRANSACTIONS -> GoCoinsTransactionsFragment()
                Fragments.INVENTORY_FRAGMENT -> InventoryFragment()
                Fragments.ADD_RACK_FRAGMENT -> AddRackFragment()
                Fragments.ADD_RACK_ITEM_FRAGMENT -> AddRackItemFragment()
                Fragments.Edit_RACK_ITEM_FRAGMENT -> EditRackItemFragment()
                Fragments.RETAIL_NEW_ORDER -> RetailNewOrderDetailFragment()
                Fragments.RETAIL_ORDER_INCLUSIONS -> OrderInclusionsFragment()
                Fragments.RETAIL_ADD_PART -> RetailOrderAddPartFragment()
                Fragments.GMB_INFO -> GmbWebsiteComboInfoFragment()
                Fragments.REFER_AND_EARN -> ReferAndEarnFragment()
                Fragments.FQA_S -> FAQsFragment()
                Fragments.GMB_PREVIEW -> GMBPreviewFragment()
                Fragments.CUSTOMER_REMINDER_FRAGMENT -> CustomerRemindersFragment()
                Fragments.WORKSHOP_BIDDING_ADD_PART_FRAGMENT -> WorkshopBiddingAddPartFragment()
                Fragments.WORKSHOP_BIDDING_CART_DETAILS_FRAGMENT -> WorkshopBiddingCartDetailsFragment()
                Fragments.WORKSHOP_BIDDING_STATUS_FRAGMENT -> WorkshopBiddingStatusFragment()
                Fragments.WORKSHOP_BIDDING_LIST_FRAGMENT -> WorkshopBiddingListFragment()
                Fragments.ENQUIRY_ORDER_REQUESTS -> EnquiryOrderRequestsFragment()
                Fragments.WORKSHOP_BID_RECEIVED_FRAGMENT -> WorkshopBiddingReceivedFragment()
                Fragments.WORKSHOP_BID_ENQUIRY_FRAG -> WorkshopBiddingEnquiryFragment()
                Fragments.INVENTORY_BULK_UPLOAD -> InventoryBulkUploadFragment()
                Fragments.PLACE_BID -> PlaceBidFragment()
                Fragments.FRAGMENT_BUSINESS_CARD -> BusinessCardFragment()
                Fragments.SPARES_SHOP -> SparesShopFragment()
                Fragments.EMPLOYEE_PERFORMANCE -> EmployeePerformanceFragment()
                Fragments.SHOP_WITH_FILTER -> ShopWithFilterFragment()
                Fragments.SS_CART -> SparesShopCartFragment()
                Fragments.SPARES_ORDER_HISTORY -> SparesOrderHistoryFragment()
                Fragments.SPARES_ENQUIRIES -> SparesEnquiryListFragment()
                Fragments.SPARES_ORDER_DETAIL -> SparesOrderDetailFragment()
                Fragments.SPARES_PURCHASE_SUMMARY -> PurchaseSummaryFragment()
                Fragments.FILTER_AND_SORT_FRAGMENT -> FilterAndSortFragment()
                Fragments.SPARES_PRODUCT_DETAILS -> SparesProductDetails()
                Fragments.SPARES_PRODUCT_ZOOM -> ZoomImageFragment()
                Fragments.SS_PAYMENT_TYPE -> SSPaymentTypeFragment()
//                Fragments.SEARCH_SS_ITEMS -> SearchSSItemsFragment()
                Fragments.SELECT_CAR_AND_SEARCH -> SelectCarAndSearchFragment()
                Fragments.BOTTOM_NAV -> BottomNavFragment()
                Fragments.SPLASH -> SplashFragment()
                Fragments.SELECT_CATEGORY -> SelectCategoryFragment()
                Fragments.SELECT_CATEGORY_OR_BRAND -> SelectCategoryOrBrandFragment()
                Fragments.SEND_ENQUIRY -> SendEnquiryFragment()
                Fragments.SUBSCRIPTION_INFO_FRAGMENT -> SubscriptionInfoFragment()
                else -> {
                    null
                }
            }
        }

        /**
         * Returns fragment from base application
         */
        private fun getDialogFragmentFromAppPackage(fragment: String,
            listener: ActionListener?): BaseDialogFragment? {

            return when (fragment) { //Screens.REGISTRATION_NUMBER_FILTER->RegistrationNumberFilter()
                Dialogs.SELECT_WORKSHOP_LIST -> SelectWorkshopDialog(listener)
                Dialogs.SELECT_WORK_DONE -> SelectWorkDoneDialog(listener)
                Dialogs.ADD_ITEM -> AddItemDialog(listener)
                Dialogs.ADD_REMARK_PHOTO -> AddRemarkImagesDialog(listener)
                Dialogs.GENERIC_CONFIRMATION -> GenericConfirmationDialog(listener)
                Dialogs.SELECT_DATE -> SelectDateDialog(listener)
                Dialogs.SELECT_TIME -> SelectTimeDialog(listener)
                Dialogs.SELECT_DATE_REMINDER -> SelectDateForReminderDialog(listener)
                Dialogs.SELECT_CAR -> SelectCarDialog(listener)
                Dialogs.SELECT_RACK_ITEM -> SelectRackItemDialog(listener)
                Dialogs.SELECT_WORKSHOP_PART_NAME -> SelectWorkShopPartName(listener)
                Dialogs.CUSTOM_AMOUNT -> CustomAmountDialog(listener)
                Dialogs.APPLY_COUPON_DIALOG -> ApplyCouponDialog(listener)
                Dialogs.APPLY_MARKETPLACE_COUPON_DIALOG -> ApplyMarketplaceCouponDialog(listener)
                Dialogs.INSUFFICIENT_BALANCE_DIALOG -> InsufficientBalanceDialog(listener)
                Dialogs.SELECT_LANGUAGE_POP_UP -> SelectLanguagePopUp(listener)
                Dialogs.REMINDER_SENT_SUCCESS -> ReminderSendSuccessDialog(listener)
                Fragments.SELECT_COUNTRY_CODE -> SelectCountryCodeFragment(listener)
                Fragments.WORKSHOP_ADDRESS_NOT_AVL -> WorkshopAddressNotAvlDialog(listener)
                Dialogs.ADD_EDIT_CUSTOMER_DETAILS -> AddEditCustomerDetailsDialog(listener)
                Dialogs.VIDEO_VIEW -> VideoViewDialog()
                Dialogs.MEMBERSHIP_PURCHASED_DIALOG -> MembershipPurchasedDialog()

                else -> {
                    null
                }
            }
        }

        fun fragmentBottomSheet(fragmentName: String,
            params: Bundle? = null,
            listener: ActionListener? = null): BaseBottomSheetFragment? {
            return if (fragmentName.isNotEmpty()) {
                getFragmentFromAppPackagefragmentBottomSheet(fragmentName, listener)?.apply {
                    arguments = params ?: Bundle()
                }
            } else {
                null
            }
        }

        /**
         * Returns fragment from base application
         */
        private fun getFragmentFromAppPackagefragmentBottomSheet(fragment: String,
            listener: ActionListener?): BaseBottomSheetFragment? {

            return when (fragment) {

                Fragments.ORDER_ESTIMATES_EDIT -> OrderEstimateEditFragment(listener)
                Fragments.ADD_PAYMENT_FRAGMENT -> AddPaymentFragment(listener)
                Fragments.EDIT_UPI_ID -> EditUpiIdFragment(listener)
                Fragments.VERIFY_OTP_SHEET -> VerifyOtpBottomSheetFragment(listener)
                Fragments.RESET_PIN -> ResetPinFragment(listener)
                Fragments.LANGUAGE_BOTTOM -> LanguageBottomFragment(listener)
                Fragments.CHOOSE_IMAGE -> ChoseImageFragment(listener)
                Fragments.COMPLETE_REGISTRATION -> CompleteRegisterFragment(listener)
                Fragments.INSTANT_REMINDER -> InstantReminderFragment(listener)
                Fragments.CANCEL_WS_BID -> CancelWsBidBottomFragment(listener)
                Fragments.PAYMENT_FAILED -> PaymentFailedFragment()
                Fragments.SELECT_ADDRESS -> SelectAddressFragment()
                Fragments.ADD_EDIT_ADDRESS -> AddEditAddressFragment()
                Fragments.PRO_LOCKED_FRAGMENT -> ProLockedFragment()
                else -> {
                    null
                }

            }
        }
    }

}