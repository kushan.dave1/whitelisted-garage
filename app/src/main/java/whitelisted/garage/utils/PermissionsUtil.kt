package whitelisted.garage.utils

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class PermissionsUtil(private var context: Activity) {
    private var permissionListener: PermissionListener? = null

    // --Commented out by Inspection (9/17/2018 5:36 PM):String tag;
    private val requestCode = 1001
    private var permission: String? = null
    private var selpermission1: String? = null
    private var selpermission2: String? = null
    fun askPermission(context: Activity,
        selPermission: String?,
        permissionListener: PermissionListener) {
        this.context = context
        this.permissionListener = permissionListener
        permission = selPermission
        if (ContextCompat.checkSelfPermission(context,
                permission!!) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(context, arrayOf(permission), requestCode)
        } else {
            permissionListener.onPermissionResult(true)
        }
    }

    fun askPermissions(context: Activity,
        permission1: String?,
        permission2: String?,
        permissionListener: PermissionListener) {
        this.context = context
        this.permissionListener = permissionListener
        selpermission1 = permission1
        selpermission2 = permission2
        if (ContextCompat.checkSelfPermission(context,
                permission1!!) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                context,
                permission2!!) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(context,
                arrayOf(permission1, permission2),
                requestCode)
        } else {
            permissionListener.onPermissionResult(true)
        }
    }

    fun onRequestPermissionsResult(requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray) {
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permissionListener!!.onPermissionResult(true)
        } else if (grantResults.size > 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                permissionListener!!.onPermissionResult(true)
            } else {
                showAgainPermissionDialog(selpermission1)
                showAgainPermissionDialog(selpermission2)
            }
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(context, permission!!)) {
                val alertBuilder = AlertDialog.Builder(context)
                alertBuilder.setCancelable(true)
                alertBuilder.setTitle("Permission Required")
                if (permission == WRITE_EXTERNAL_STORAGE) alertBuilder.setMessage(storagePermInfoMsg) else if (permission == READ_SMS) alertBuilder.setMessage(
                    smsPermInfoMsg) else if (permission == RECEIVE_SMS) alertBuilder.setMessage(
                    smsPermInfoMsg) else if (permission == ACCOUNTS) alertBuilder.setMessage(
                    accountsPermInfoMsg) else if (permission == READ_CALENDAR) alertBuilder.setMessage(
                    calendarPermInfoMsg) else if (permission == WRITE_CALENDAR) alertBuilder.setMessage(
                    calendarPermInfoMsg) else if (permission == CAMERA) alertBuilder.setMessage(
                    cameraPermInfoMsg) else if (permission == LOCATION) alertBuilder.setMessage(
                    locationPermInfoMsg) else if (permission == READ_CONTACTS) alertBuilder.setMessage(
                    contactsPermInfoMsg) else if (permission == WRITE_CONTACTS) alertBuilder.setMessage(
                    contactsPermInfoMsg)
                alertBuilder.setPositiveButton("Grant") { dialog, which ->
                    ActivityCompat.requestPermissions(context, arrayOf(permission), requestCode)
                }
                alertBuilder.setNegativeButton("Deny") { dialog, which ->
                    dialog.dismiss()
                    permissionListener!!.onPermissionResult(false)
                }
                val alert = alertBuilder.create()
                alert.show()
            } else {
                if (permission == WRITE_EXTERNAL_STORAGE) Toast.makeText(context,
                    storagePermErrorMsg,
                    Toast.LENGTH_LONG).show() else if (permission == READ_SMS) Toast.makeText(
                    context,
                    smsPermErrorMsg,
                    Toast.LENGTH_LONG).show() else if (permission == RECEIVE_SMS) Toast.makeText(
                    context,
                    smsPermErrorMsg,
                    Toast.LENGTH_LONG).show() else if (permission == ACCOUNTS) Toast.makeText(
                    context,
                    accountsPermErrorMsg,
                    Toast.LENGTH_LONG).show() else if (permission == READ_CALENDAR) Toast.makeText(
                    context,
                    calendarPermErrorMsg,
                    Toast.LENGTH_LONG).show() else if (permission == WRITE_CALENDAR) Toast.makeText(
                    context,
                    calendarPermErrorMsg,
                    Toast.LENGTH_LONG).show() else if (permission == CAMERA) Toast.makeText(context,
                    cameraPermErrorMsg,
                    Toast.LENGTH_LONG).show() else if (permission == LOCATION) Toast.makeText(
                    context,
                    locationPermErrorMsg,
                    Toast.LENGTH_LONG).show() else if (permission == READ_CONTACTS) Toast.makeText(
                    context,
                    contactsPermErrorMsg,
                    Toast.LENGTH_LONG).show() else if (permission == WRITE_CONTACTS) Toast.makeText(
                    context,
                    contactsPermErrorMsg,
                    Toast.LENGTH_LONG).show()
                permissionListener!!.onPermissionResult(false)
            }
        }
    }

    private fun showAgainPermissionDialog(permission: String?) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(context, permission!!)) {
            val alertBuilder = AlertDialog.Builder(context)
            alertBuilder.setCancelable(true)
            alertBuilder.setTitle("Permission Required")
            if (permission == WRITE_EXTERNAL_STORAGE) alertBuilder.setMessage(storagePermInfoMsg) else if (permission == READ_SMS) alertBuilder.setMessage(
                smsPermInfoMsg) else if (permission == RECEIVE_SMS) alertBuilder.setMessage(
                smsPermInfoMsg) else if (permission == ACCOUNTS) alertBuilder.setMessage(
                accountsPermInfoMsg) else if (permission == READ_CALENDAR) alertBuilder.setMessage(
                calendarPermInfoMsg) else if (permission == WRITE_CALENDAR) alertBuilder.setMessage(
                calendarPermInfoMsg) else if (permission == CAMERA) alertBuilder.setMessage(
                cameraPermInfoMsg) else if (permission == LOCATION) alertBuilder.setMessage(
                locationPermInfoMsg) else if (permission == READ_CONTACTS) alertBuilder.setMessage(
                contactsPermInfoMsg) else if (permission == WRITE_CONTACTS) alertBuilder.setMessage(
                contactsPermInfoMsg)
            alertBuilder.setPositiveButton("Grant") { dialog, which ->
                ActivityCompat.requestPermissions(context,
                    arrayOf<String?>(permission),
                    requestCode)
            }
            alertBuilder.setNegativeButton("Deny") { dialog, which ->
                dialog.dismiss()
                permissionListener!!.onPermissionResult(false)
            }
            val alert = alertBuilder.create()
            alert.show()
        } else {
            if (permission == WRITE_EXTERNAL_STORAGE) Toast.makeText(context,
                storagePermErrorMsg,
                Toast.LENGTH_LONG).show() else if (permission == READ_SMS) Toast.makeText(context,
                smsPermErrorMsg,
                Toast.LENGTH_LONG)
                .show() else if (permission == RECEIVE_SMS) Toast.makeText(context,
                smsPermErrorMsg,
                Toast.LENGTH_LONG).show() else if (permission == ACCOUNTS) Toast.makeText(context,
                accountsPermErrorMsg,
                Toast.LENGTH_LONG).show() else if (permission == READ_CALENDAR) Toast.makeText(
                context,
                calendarPermErrorMsg,
                Toast.LENGTH_LONG).show() else if (permission == WRITE_CALENDAR) Toast.makeText(
                context,
                calendarPermErrorMsg,
                Toast.LENGTH_LONG).show() else if (permission == CAMERA) Toast.makeText(context,
                cameraPermErrorMsg,
                Toast.LENGTH_LONG).show() else if (permission == LOCATION) Toast.makeText(context,
                locationPermErrorMsg,
                Toast.LENGTH_LONG).show() else if (permission == READ_CONTACTS) Toast.makeText(
                context,
                contactsPermErrorMsg,
                Toast.LENGTH_LONG).show() else if (permission == WRITE_CONTACTS) Toast.makeText(
                context,
                contactsPermErrorMsg,
                Toast.LENGTH_LONG).show()
            permissionListener!!.onPermissionResult(false)
        }
    }

    interface PermissionListener {
        fun onPermissionResult(isGranted: Boolean)
    }

    companion object {
        var READ_CONTACTS = Manifest.permission.READ_CONTACTS
        var WRITE_CONTACTS = Manifest.permission.WRITE_CONTACTS
        var WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE
        var CAMERA = Manifest.permission.CAMERA
        var READ_SMS = Manifest.permission.READ_SMS
        var RECEIVE_SMS = Manifest.permission.RECEIVE_SMS
        var LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
        var READ_CALENDAR = Manifest.permission.READ_CALENDAR
        var WRITE_CALENDAR = Manifest.permission.WRITE_CALENDAR
        var ACCOUNTS = Manifest.permission.GET_ACCOUNTS
        var BACKGROUND_LOCATION = Manifest.permission.ACCESS_BACKGROUND_LOCATION
        var COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION
        var BLUETOOTH = Manifest.permission.BLUETOOTH
        private const val storagePermInfoMsg =
            "App needs this permission to store files on phone's storage. Are you sure you want to deny this permission ?"
        private const val storagePermErrorMsg =
            "Storage permission denied. You can enable permission from settings"
        private const val contactsPermInfoMsg =
            "App needs this permission to access phone contacts. Are you sure you want to deny this permission ?"
        private const val contactsPermErrorMsg =
            "Contacts permission denied. You can enable permission from settings"
        private const val smsPermInfoMsg =
            "App needs this permission to receive/read SMS for auto detection of OTP. Are you sure you want to deny this permission ?"
        private const val smsPermErrorMsg =
            "SMS permission denied. You can enable permission from settings"
        private const val accountsPermInfoMsg =
            "App needs this permission to access Google Account on phone. Are you sure you want to deny this permission ?"
        private const val accountsPermErrorMsg =
            "Contacts permission denied. You can enable permission from settings"
        private const val cameraPermInfoMsg =
            "App needs this permission to capture photos using phone's camera. Are you sure you want to deny this permission ?"
        private const val cameraPermErrorMsg =
            "Camera permission denied. You can enable permission from settings"
        private const val calendarPermInfoMsg =
            "App needs this permission to access phone's calendar. Are you sure you want to deny this permission ?"
        private const val calendarPermErrorMsg =
            "Calendar permission denied. You can enable permission from settings"
        private const val locationPermInfoMsg =
            "App needs this permission to access your location. Are you sure you want to deny this permission ?"
        private const val locationPermErrorMsg =
            "Location permission denied. You can enable permission from settings"
    }

}