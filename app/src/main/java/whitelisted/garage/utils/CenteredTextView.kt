package whitelisted.garage.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.graphics.drawable.RotateDrawable
import android.os.Build
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.withStyledAttributes
import whitelisted.garage.R

class CenteredTextView : View, CustomViewInterface {

    override var borderWidth = 1f
    override val view: View get() = this
    private var text: String = ""
    private var paintTextSize = 0
    private val textPaint = TextPaint()
    private var textColor = 0xffff0000.toInt()
    private var drawable: Drawable? = null
    private var textX = 0f
    private var textY = 0f
    private var drawablePadding = 0f
    private var drawableTint: Int = 0
    private lateinit var staticLayout: StaticLayout
    private var textWidth = 0f
    private var paddingStartEnd = 0f
    private var paddingTopBottom = 0f
    private var fontFamily: Typeface? = null
    private var drawableWidth = 0f
    private var drawableHeight = 0f
    private var drawableRotation = 0f
    private var drawablePosition = 0

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setupAttrs(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context,
        attrs,
        defStyleAttr) {
        setupAttrs(attrs)
    }

    private fun setupAttrs(attrs: AttributeSet) {
        context.withStyledAttributes(attrs, R.styleable.CustomTextView) {
            try {
                val isAllCaps = getBoolean(R.styleable.CustomTextView_textAllCaps, false)
                text = getString(R.styleable.CustomTextView_viewText) ?: ""
                paintTextSize = getDimensionPixelSize(R.styleable.CustomTextView_viewTextSize, 32)
                textColor = getColor(R.styleable.CustomTextView_viewTextColor, 0xffff0000.toInt())
                drawable = getDrawable(R.styleable.CustomTextView_drawableSrc)
                drawablePadding =
                    getDimensionPixelSize(R.styleable.CustomTextView_drawablePadding, 0).toFloat()
                drawableTint = getColor(R.styleable.CustomTextView_drawableTint, 0)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    fontFamily = getFont(R.styleable.CustomTextView_textFontFamily)
                }
                paddingStartEnd = getDimension(R.styleable.CustomTextView_paddingStartEnd, 0f)
                paddingTopBottom = getDimension(R.styleable.CustomTextView_paddingTopBottom, 0f)
                drawableWidth = getDimension(R.styleable.CustomTextView_drawableWidth, 0f)
                drawableHeight = getDimension(R.styleable.CustomTextView_drawableHeight, 0f)
                drawableRotation = getFloat(R.styleable.CustomTextView_drawableRotation, 0f)
                drawablePosition = getInt(R.styleable.CustomTextView_drawablePosition, 0)

                drawableWidth =
                    if (drawableWidth == 0f && drawable != null) drawable?.intrinsicWidth?.toFloat()
                        ?: 0f else drawableWidth
                drawableHeight =
                    if (drawableHeight == 0f && drawable != null) drawable?.intrinsicHeight?.toFloat()
                        ?: 0f else drawableHeight

                if (isAllCaps) text = text.toUpperCase()
                setBgDrawable(attrs)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            initializeView()

        }
    }

    private fun initializeView() {

        textPaint.reset()
        textPaint.apply {
            isAntiAlias = true
            color = textColor
            textSize = paintTextSize.toFloat()
        }

        if (fontFamily != null) {
            textPaint.typeface = fontFamily
        }
        textWidth = textPaint.measureText(text)

        staticLayout =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) StaticLayout.Builder.obtain(text,
                0,
                text.length,
                textPaint,
                textWidth.toInt()).setAlignment(Layout.Alignment.ALIGN_NORMAL)
                .setLineSpacing(0f, 0f).build()
            else StaticLayout(text,
                textPaint,
                textWidth.toInt(),
                Layout.Alignment.ALIGN_NORMAL,
                0f,
                0f,
                false)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val widthSpecSize = MeasureSpec.getSize(widthMeasureSpec)
        val widthSpecMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightSpecSize = MeasureSpec.getSize(heightMeasureSpec)
        val heightSpecMode = MeasureSpec.getMode(heightMeasureSpec)

        val viewWidth =
            staticLayout.width + paddingStartEnd + if (drawable != null) drawableWidth + drawablePadding else 0f
        val viewHeight =
            if (drawableHeight > staticLayout.height) drawableHeight else staticLayout.height + paddingTopBottom

        val w = if (widthSpecMode == MeasureSpec.AT_MOST) viewWidth.toInt() else widthSpecSize
        val h = if (heightSpecMode == MeasureSpec.AT_MOST) viewHeight.toInt() else heightSpecSize

        setMeasuredDimension(w, h)

    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawText(canvas)
        drawable?.let {
            drawDrawable(it, canvas)
        }
    }

    private fun drawText(canvas: Canvas) {

        val textHeight = staticLayout.height
        textX = (width - textWidth) * 0.5f
        textY = (height - textHeight) * 0.5f
        if (drawable != null) {
            if (drawablePosition == 0) textX += (drawableWidth + drawablePadding) / 2
            else textX -= (drawableWidth + drawablePadding) / 2
        }
        canvas.save()
        canvas.translate(textX, textY)
        staticLayout.draw(canvas)
        canvas.restore()
    }

    private fun drawDrawable(drawable: Drawable, canvas: Canvas) {
        canvas.save()
        val drawableX = if (drawablePosition == 0) textX - drawableWidth - drawablePadding
        else textX + textWidth + drawablePadding
        val drawableY = (height - drawableHeight) * 0.5f

        if (drawableRotation != 0f) canvas.rotate(drawableRotation,
            drawableX + drawableWidth * 0.5f,
            drawableY + drawableHeight * 0.5f)
        canvas.translate(drawableX, drawableY)

        if (drawableTint != 0) drawable.setTint(drawableTint)

        drawable.setBounds(0, 0, drawableWidth.toInt(), drawableHeight.toInt())
        drawable.draw(canvas)

        canvas.restore()
    }

    fun setText(text: String) {
        this.text = text
        initializeView()
        invalidate()
    }

    fun setDrawableRes(res: Int) {
        drawable = resources.getDrawable(res, resources.newTheme())
        invalidate()
    }

    fun setDrawableRotation(rotation: Float) {
        drawableRotation = rotation
        invalidate()
    }


}