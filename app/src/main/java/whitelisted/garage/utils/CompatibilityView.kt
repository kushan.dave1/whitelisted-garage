package whitelisted.garage.utils

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import whitelisted.garage.R
import whitelisted.garage.api.response.Compatibility
import whitelisted.garage.databinding.CompatibilityCard2Binding
import whitelisted.garage.databinding.CompatibilityCardBinding
import whitelisted.garage.databinding.CompatibilityViewBinding
import whitelisted.garage.utils.CommonUtils.makeVisible

class CompatibilityView : LinearLayout {

    companion object {
        const val NAME = "name"
        const val YEAR = "year"
        const val VARIANT = "variant"
        const val FUEL = "fuel"
        const val IMAGE = "image"
    }

    private lateinit var compatibilities: List<Compatibility>
    private lateinit var binding: CompatibilityViewBinding

    constructor(context: Context) : super(context) { initView() }
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) { initView() }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs,
        defStyleAttr) { initView() }

    private fun initView() {
        binding = CompatibilityViewBinding.inflate(LayoutInflater.from(context), this, true)
        binding.compatibilityRvBrands.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.compatibilityRvModels.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.compatibilityRvYears.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.compatibilityRvVariant.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

    }

    fun setData(compatibilities: List<Compatibility>) {
        this.compatibilities = compatibilities.filter { !it.brand.isNullOrEmpty() }
        val brands = mutableListOf<Map<String, String?>>()

        this.compatibilities.map { cmp ->
            val added = brands.find { it[NAME] == cmp.brand } != null
            if (!added) {
                brands.add(mapOf(NAME to (cmp.brand ?: ""), IMAGE to (cmp.brandImage ?: "")))
            }
        }

        binding.compatibilityRvBrands.adapter = CompatibilityAdapter1(brands) { brand ->
            setBrand(brand)
        }
        if (brands.isNotEmpty()) setBrand(brands[0][NAME] ?: "")
    }

    private fun setBrand(brand: String) {
        val models = mutableListOf<Map<String, String?>>()
        compatibilities.filter { it.brand == brand }.map { cmp ->
            val added = models.find { it[NAME] == cmp.model } != null
            if (!added) {
                cmp.model?.let {
                    models.add(mapOf(NAME to it))
                }
            }
        }

        binding.compatibilityRvModels.adapter = CompatibilityAdapter1(models) { model ->
            setModel(model)
        }
        if (models.isNotEmpty()) setModel(models[0][NAME] ?: "")
    }

    private fun setModel(model: String) {
        val variants = compatibilities.filter { it.model == model }.distinctBy { it.variant }
            .map { mapOf(VARIANT to it.variant, FUEL to it.fuelType) }
        binding.compatibilityRvYears.adapter = CompatibilityAdapter2(variants)
    }


    inner class CompatibilityAdapter1(private val compatibilities: List<Map<String, String?>>,
        private val onClickListener: (String) -> Unit) :
        RecyclerView.Adapter<CompatibilityAdapter1.ViewHolder1>() {

        private var selectedPos = 0
            set(value) {
                val oldSelected = field
                field = value
                notifyItemChanged(oldSelected)
                notifyItemChanged(selectedPos)
            }

        inner class ViewHolder1(private val binding: CompatibilityCardBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(compatibilities: Map<String, String?>, position: Int) {
                binding.tvName.text = compatibilities[NAME]
                val img = compatibilities[IMAGE]
                binding.compatibilityImg.makeVisible(!img.isNullOrEmpty())
                binding.compatibilityImg.load(img)
                binding.root.setOnClickListener {
                    onClickListener(compatibilities[NAME] ?: "")
                    selectedPos = position
                }
                if (position == selectedPos) setSelected()
                else setUnselected()

            }

            private fun setSelected() {
                binding.cclMain.setViewBorderColor(R.color.text_color_red)
                binding.tvName.setTextColor(ContextCompat.getColor(binding.root.context,
                    R.color.text_color_red))
                binding.tvName.typeface =
                    ResourcesCompat.getFont(binding.root.context, R.font.gilroy_semibold)
            }

            private fun setUnselected() {
                binding.cclMain.setViewBorderColor(R.color.field_outline_33)
                binding.tvName.setTextColor(ContextCompat.getColor(binding.root.context,
                    R.color.black_trans_new))
                binding.tvName.typeface =
                    ResourcesCompat.getFont(binding.root.context, R.font.gilroy_medium)
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
            val binding =
                CompatibilityCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ViewHolder1(binding)
        }

        override fun onBindViewHolder(holder: ViewHolder1, position: Int) {
            holder.bind(compatibilities[position], position)
        }

        override fun getItemCount() = compatibilities.size

    }


    inner class CompatibilityAdapter2(private val compatibilities: List<Map<String, String?>>) :
        RecyclerView.Adapter<CompatibilityAdapter2.ViewHolder2>() {
        private var selectedVariant = -1
        inner class ViewHolder2(private val binding: CompatibilityCard2Binding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(compatibilities: Map<String, String?>) {

                if (compatibilities.containsKey(YEAR)) {
                    binding.tvName.text = compatibilities[YEAR]
                    binding.tvName.makeVisible(true)
                }

                if (compatibilities.containsKey(VARIANT)) {
                    binding.tvName.text = compatibilities[VARIANT]
                    binding.tvName.makeVisible(true)
                }

                if (compatibilities.containsKey(FUEL)) {
                    binding.tvFuel.text = compatibilities[FUEL]
                    binding.tvFuel.makeVisible(true)
                } else binding.tvFuel.makeVisible(false)

                binding.root.setOnClickListener {
                    val prev = selectedVariant
                    selectedVariant = layoutPosition
                    notifyItemChanged(selectedVariant)
                    notifyItemChanged(prev)
                }
                if (layoutPosition == selectedVariant) setSelected()
                else setUnselected()

            }

            private fun setSelected() {
                binding.cclMain.setViewBorderColor(R.color.text_color_red)
                binding.tvName.setTextColor(ContextCompat.getColor(binding.root.context,
                    R.color.text_color_red))
                binding.tvFuel.setTextColor(ContextCompat.getColor(binding.root.context,
                    R.color.text_color_red))
                binding.tvName.typeface =
                    ResourcesCompat.getFont(binding.root.context, R.font.gilroy_semibold)
            }

            private fun setUnselected() {
                binding.cclMain.setViewBorderColor(R.color.field_outline_33)
                binding.tvName.setTextColor(ContextCompat.getColor(binding.root.context,
                    R.color.black_trans_new))
                binding.tvFuel.setTextColor(ContextCompat.getColor(binding.root.context,
                    R.color.black_trans_new))
                binding.tvName.typeface =
                    ResourcesCompat.getFont(binding.root.context, R.font.gilroy_medium)
            }


        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder2 {
            val binding = CompatibilityCard2Binding.inflate(LayoutInflater.from(parent.context),
                parent,
                false)
            return ViewHolder2(binding)
        }

        override fun onBindViewHolder(holder: ViewHolder2, position: Int) {
            holder.bind(compatibilities[position])
        }

        override fun getItemCount() = compatibilities.size

    }

}