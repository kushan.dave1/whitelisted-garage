package whitelisted.garage.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ImageDecoder
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.gun0912.tedpermission.normal.TedPermission
import com.gun0912.tedpermission.PermissionListener
import gun0912.tedimagepicker.builder.TedImagePicker
import io.branch.referral.util.Product
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import whitelisted.garage.R
import whitelisted.garage.api.request.Enquiry
import whitelisted.garage.api.response.CarDetail
import whitelisted.garage.api.response.SSItemModel
import whitelisted.garage.base.ActionListener
import whitelisted.garage.databinding.EnquirySuccessDialogBinding
import whitelisted.garage.databinding.ItemRackItemImageBinding
import whitelisted.garage.databinding.ViewEnquiryBinding
import whitelisted.garage.utils.CommonUtils.changeTextColor
import whitelisted.garage.utils.CommonUtils.getString
import whitelisted.garage.utils.CommonUtils.isVisible
import whitelisted.garage.utils.CommonUtils.makeVisible
import whitelisted.garage.viewmodels.SelectCarDialogViewModel
import java.io.ByteArrayOutputStream
import java.util.*

class EnquiryView : ConstraintLayout {

    private val layoutRes = R.layout.view_enquiry
    private var quantity = 1
    private var isSendBtnVisible = true
    private var listOfUri = mutableListOf<Uri>()
    private var listOfUrls = mutableListOf<String>()
    private var imagesAdapter: RecyclerView.Adapter<*>? = null
    private val binding by lazy { ViewEnquiryBinding.bind(this) }
    private var productEnqImagesRef: StorageReference? = null
    private var uploadCount = 0
    private var onUploadingFinishedListener: ((Enquiry) -> Unit)? = null
    private var onUploadingStart: (() -> Unit)? = null
    private var onSentDialogDismiss: (() -> Unit)? = null
    private var userId = ""
    private var workshopId = ""
    private var product: SSItemModel? = null
    private var selectedCar: CarDetail? = null
    private var twoWheelerType = false
    private var isGoCoinsSufficient = false
    private val thisFragment: Fragment
    get() = (context as FragmentActivity).supportFragmentManager.fragments.last()
    private val enquiry: Enquiry
        get() =
            Enquiry(
                binding.etRemarks.text.toString(),
                listOfUrls,
                quantity,
                partName,
                vehicle = carName,
                fuelType = selectedCar?.fuelType,
                vehicleImage = selectedCar?.carIcon
            )


    constructor(context: Context) : super(context) {
        initView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initView()
    }


    private fun initView() {
        inflate(context, layoutRes, this)
        setupClickListners()

        imagesAdapter =
            binding.rvImages.addViews(listOfUri, ItemRackItemImageBinding::inflate, ::imagesBinder)

        updateQuantity(0)
        binding.checkBox.isChecked = true

    }

    private fun setupClickListners() = binding.apply {
        imgPlus.setOnClickListener { updateQuantity(+1) }
        imgMinus.setOnClickListener { updateQuantity(-1) }
        ivAddImage.setOnClickListener { openAddImageView() }
        ivAddPhotos.setOnClickListener { openAddImageView() }
        llAddCarDetails.setOnClickListener { showSelectCarDialog() }
        imgSwitchVehicleType.setOnClickListener { changeVehicleType() }
        btnSendEnquiry.setOnClickListener { startUploadingImages() }
        btnReshare.setOnClickListener { showShared(false) }
        btnContactus.setOnClickListener { contactUs() }
        binding.checkBox.setOnCheckedChangeListener { _, isChecked -> showAddCardDetails(isChecked) }
    }

    fun onSendEnquiryClickedFromPDP(){
        if(binding.cl2.isVisible()) showShared(false)
        else startUploadingImages()
    }

    private fun showAddCardDetails(checked: Boolean) {
        binding.llAddCarDetails.makeVisible(checked)
        binding.grpVehicle.makeVisible(checked)
        if (!checked) {
            binding.clCarDetails.makeVisible(false)
        }
        selectedCar = null
    }

    private fun startUploadingImages() {
        if (binding.etPartName.isVisible() && binding.etPartName.text?.isEmpty() == true) {
            CommonUtils.showToast(
                context,
                context.resources.getString(R.string.please_enter_part_name)
            )
            return
        }

        if(binding.checkBox.isChecked && selectedCar == null) {
            CommonUtils.showToast(
                context,
                context.resources.getString(R.string.vehicle_not_selected)
            )
            return
        }

        if(!isGoCoinsSufficient) {
            FragmentFactory.fragmentDialog(
                FragmentFactory.Dialogs.INSUFFICIENT_BALANCE_DIALOG)?.let {
                it.show(thisFragment.parentFragmentManager, it.javaClass.name)
            }
            return
        }

        onUploadingStart?.invoke()
        if (listOfUri.isNotEmpty()) listOfUri.forEach { uri -> uploadMedia(uri) }
        else onUploadingFinishedListener?.invoke(enquiry)
    }

    private fun updateQuantity(by: Int) {
        this.quantity += by
        if (quantity == 0) quantity = 1
        binding.tvEnquiryQuantity.text = quantity.toString()

    }

    private val partName: String get() = binding.etPartName.text.toString()

    private fun openAddImageView() {
        TedPermission.create().setPermissionListener(object : PermissionListener {
            override fun onPermissionGranted() {
                TedImagePicker.with(context).showCameraTile(true).startMultiImage {
                    listOfUri.addAll(it)
                    imagesAdapter?.notifyItemRangeInserted(listOfUri.size - it.size, it.size)
                    binding.rvImages.makeVisible(true)
                    binding.ivAddPhotos.makeVisible(false)
                }
            }

            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
                CommonUtils.showToast(context, resources.getString(R.string.permission_denied))
            }

        }).setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .check()


    }

    private fun imagesBinder(binding: ItemRackItemImageBinding, model: Uri, adapterPosition: Int) {
        binding.llDelImage.makeVisible(false)
        binding.imgBusImage.load(model)
        binding.root.setBorder(2f, R.color.bg_selected, 20f)
        val params = RelativeLayout.LayoutParams(
            this.binding.ivAddImage.width,
            this.binding.ivAddImage.height
        )
        params.marginStart = 10
        binding.root.layoutParams = params
        binding.root.setPadding(2, 2, 2, 2)
        binding.cvImage.useCompatPadding = false
    }

    fun setPartName(name: String?): EnquiryView {
        binding.etPartName.setText(name)
        binding.etPartName.makeVisible(true)
        binding.lblPartName.makeVisible(true)
        return this
    }

    fun setUserId(id: String): EnquiryView {
        userId = id
        return this
    }

    fun setWorkshopId(id: String): EnquiryView {
        workshopId = id
        return this
    }

    fun setProductEnquired(product: SSItemModel?): EnquiryView {
        this.product = product
        return this
    }

    fun setOnSentDialogDismissListener(listener: () -> Unit): EnquiryView {
        onSentDialogDismiss = listener
        return this
    }

    fun setUploadingPath(reference: StorageReference): EnquiryView {
        this.productEnqImagesRef = reference
        return this
    }

    fun setOnUploadingStartListener(l: () -> Unit): EnquiryView {
        onUploadingStart = l
        return this
    }

    fun setOnUploadingFinishedListener(listener: (Enquiry) -> Unit) {
        onUploadingFinishedListener = listener
    }

    @Suppress("DEPRECATION")
    private fun uploadMedia(uploadItem: Uri) {
        val fileNameRand: String = (Calendar.getInstance().timeInMillis.toString())

        val uploadMediaRef = productEnqImagesRef?.child(fileNameRand)

        val uploadTask: UploadTask?

        val emptyBitmap = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(emptyBitmap)
        canvas.drawColor(ContextCompat.getColor(context, R.color.white))
        var bmp: Bitmap = try {
            if (Build.VERSION.SDK_INT < 28) {
                MediaStore.Images.Media.getBitmap(context.contentResolver, uploadItem)
                    ?: emptyBitmap
            } else {
                ImageDecoder.createSource(context.contentResolver, uploadItem).let {
                    ImageDecoder.decodeBitmap(it)
                }
            }

        } catch (e: Exception) {
            emptyBitmap
        }
        val byteArrayOutputStream = ByteArrayOutputStream()
        bmp = CommonUtils.scaleBitmap(bmp)
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val data = byteArrayOutputStream.toByteArray()
        uploadTask = uploadMediaRef?.putBytes(data)

        uploadTask?.continueWithTask { task ->
            if (!task.isSuccessful) task.exception?.let { throw it }
            uploadMediaRef?.downloadUrl
        }?.addOnCompleteListener {
            uploadCount++

            if (it.isSuccessful) listOfUrls.add(it.result.toString())
            if (uploadCount == listOfUri.size) onUploadingFinishedListener?.invoke(enquiry)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun clear() {
        quantity = 1; binding.etRemarks.setText("")
        listOfUri.clear(); listOfUrls.clear()
        imagesAdapter?.notifyDataSetChanged()
        binding.rvImages.makeVisible(false)
        binding.ivAddPhotos.makeVisible(true)
        binding.tvEnquiryQuantity.text = quantity.toString()
    }

    fun showEnquirySuccessDialog() {
        showShared()
        val binding = EnquirySuccessDialogBinding.inflate(LayoutInflater.from(context))
        val builder = AlertDialog.Builder(context)
        val dialog = builder.setView(binding.root).create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding.ivClose.setOnClickListener { dialog.dismiss() }
        binding.btnOk.setOnClickListener { dialog.dismiss() }
        dialog.setOnDismissListener { onSentDialogDismiss?.invoke() }
        dialog.show()
    }

    fun showShared(isShared: Boolean = true) {
        if (isShared) clear()
        binding.cl2.makeVisible(isShared)
        binding.cl1.makeVisible(!isShared)
        binding.ivEnquiry.makeVisible(!isShared)
        if(isSendBtnVisible) binding.btnSendEnquiry.makeVisible(!isShared)
    }

    private fun contactUs() {
        fireContactUsEvent()
        sendEmail()
    }

    private fun sendEmail() {
        val email = Intent(Intent.ACTION_SEND)
        email.putExtra(Intent.EXTRA_EMAIL, arrayOf(AppENUM.HELP_SUPPORT_MAIL))
        email.putExtra(
            Intent.EXTRA_SUBJECT,
            getString(
                context,
                R.string.enquiry_pending
            ) + if (userId.isNotEmpty()) " - $userId" else ""
        )

        email.type = "message/rfc822"
        email.setPackage("com.google.android.gm")

        val textSB = StringBuilder().append(getString(context, R.string.enquiry_email_text))
        textSB.append("\n")
        product?.let {
            textSB.append(getString(context, R.string.product_name) + " : " + it.title + "\n")
            textSB.append(getString(context, R.string.sku_code) + " : " + it.skuCode + "\n")
            textSB.append(getString(context, R.string.brand) + " : " + it.brand + "\n")
            textSB.append(getString(context, R.string.category) + " : " + it.skuCategory + "\n")
            textSB.append(getString(context, R.string.quantity) + " : " + quantity + "\n")
        }
        textSB.append(getString(context, R.string.workshop_id) + " : " + workshopId)
        textSB.append("\n")
        email.putExtra(Intent.EXTRA_TEXT, textSB.toString())
        try {
            context.startActivity(
                Intent.createChooser(
                    email,
                    getString(context, R.string.choose_an_email_client)
                )
            )
        } catch (e: ActivityNotFoundException) {
            CommonUtils.showToast(context, e.message.toString())
        }
    }


    private fun showSelectCarDialog() {
        FragmentFactory.fragmentDialog(FragmentFactory.Dialogs.SELECT_CAR, Bundle().apply {
            putInt(AppENUM.CAR_SEARCH_TYPE, 0)
            putBoolean(AppENUM.IntentKeysENUM.IS_TWO_WHEELER, twoWheelerType)
        }, object : ActionListener {
            override fun onActionItem(extra: Any?, extra2: Any?) {
                when (extra as Int) {
                    2 -> { //search items via selected car
                        selectedCar = extra2 as CarDetail
                        setSelectedCar()
                    }
                }
            }
        })?.let { dialog ->
            val childManager = thisFragment.childFragmentManager
            dialog.show(childManager, dialog.javaClass.name)
        }
    }

    private fun setSelectedCar() {

        binding.llAddCarDetails.makeVisible(false)
        binding.clCarDetails.makeVisible(true)

        ImageLoader.loadImage(binding.imgCarImage, selectedCar?.carIcon)
        binding.tvCarName.text = resources.getString(
            R.string.concatenate_string,
            selectedCar?.brand_name,
            selectedCar?.car
        )
        binding.tvFuelType.text = selectedCar?.fuelType

    }

    private val carName: String get() = binding.tvCarName.text.toString()

    private fun changeVehicleType() {
        if (twoWheelerType) {
            twoWheelerType = false
            binding.lblFourWheeler.changeTextColor(context, R.color.colorAccent)
            binding.lblTwoWheeler.changeTextColor(context, R.color.black_text_new)
            ImageLoader.loadDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_double_switch_off
                ), binding.imgSwitchVehicleType
            )
            binding.tvAddVehicleDetails.text = resources.getString(R.string.add_car_details)
            binding.tvAddVehicleDetails.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(context, R.drawable.ic_add_car_details),
                null,
                null,
                null
            )

        } else {
            twoWheelerType = true
            binding.lblFourWheeler.changeTextColor(context, R.color.black_text_new)
            binding.lblTwoWheeler.changeTextColor(context, R.color.colorAccent)
            ImageLoader.loadDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_double_switch_on
                ), binding.imgSwitchVehicleType
            )
            binding.tvAddVehicleDetails.text = resources.getString(R.string.add_bike_details)
            binding.tvAddVehicleDetails.setCompoundDrawablesRelativeWithIntrinsicBounds(
                ContextCompat.getDrawable(context, R.drawable.ic_two_wheeler),
                null,
                null,
                null
            )

        }
        selectedCar = null
        binding.llAddCarDetails.makeVisible(true)
        binding.clCarDetails.makeVisible(false)

    }

    private fun fireContactUsEvent() {
        Bundle().apply {
            putString(
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ENQUIRY
            )
            FirebaseAnalyticsLog.trackFireBaseEventLog(
                FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_TAP_SUPPORT,
                this
            )

        }
    }

    fun setEnquiryCost(cost: Int, goCoins: String) {

        binding.llCostOfEnquiry.makeVisible( cost > 0)
        binding.tvEnquiryGoCoinCost.text = cost.toString()
        val goCoinBalance = try { goCoins.toInt() } catch (e: Exception) { 0 }

        isGoCoinsSufficient = goCoinBalance >= cost

    }

    fun hideSendEnquiryBtn() {
        isSendBtnVisible = false
        binding.btnSendEnquiry.makeVisible(false)
    }


}
