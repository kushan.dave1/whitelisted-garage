package whitelisted.garage.utils

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils

class AppStorePreferences(private var preference: String, private var mode: Int) {
    @Volatile
    private var sharedPreferences: SharedPreferences? = null

    private fun getSharedPreferences(context: Context): SharedPreferences? {
        if (sharedPreferences == null) {
            synchronized(AppStorePreferences::class.java) {
                if (sharedPreferences == null) {
                    sharedPreferences = context.getSharedPreferences(preference, mode)
                }
            }
        }
        return sharedPreferences
    }

    fun putInt(context: Context?, key: String, value: Int) {
        if (context != null) {
            getSharedPreferences(context)!!.edit().putInt(key, value).apply()
        }
    }

    fun putFloat(context: Context?, key: String, value: Float) {
        if (context != null) {
            getSharedPreferences(context)!!.edit().putFloat(key, value).apply()
        }
    }

    fun putString(context: Context?, key: String, value: String) {
        if (context != null) {
            val edit = getSharedPreferences(context)!!.edit()
            if (TextUtils.isEmpty(value)) {
                edit.remove(key)
            } else {
                edit.putString(key, value)
            }
            edit.apply()
        }
    }

    fun putBoolean(context: Context?, key: String, value: Boolean) {
        if (context != null) {
            getSharedPreferences(context)!!.edit().putBoolean(key, value).apply()
        }
    }

    fun getString(context: Context?, key: String, defaultValue: String? = null): String {
        context?.let {
            return getSharedPreferences(it)!!.getString(key, defaultValue ?: "").toString()
        } ?: kotlin.run {
            return defaultValue ?: ""
        }

    }

    fun getFloatFromSf(context: Context?, key: String, defaultValue: Float? = null): Float {
        context?.let {
            return getSharedPreferences(it)!!.getFloat(key, defaultValue ?: 1.0f)
        } ?: kotlin.run {
            return defaultValue ?: 1.0f
        }

    }

    fun getInt(context: Context, key: String): Int {
        return getInt(context, key, 0)
    }

    fun getFloat(context: Context, key: String, defaultValue: Float?): Float {
        return getFloatFromSf(context, key, defaultValue)
    }


    fun getBoolean(context: Context, key: String): Boolean {
        return getBoolean(context, key, false)
    }

    fun getBoolean(context: Context?, key: String, def: Boolean): Boolean {
        return context?.let {
            try {
                getSharedPreferences(context)?.getBoolean(key, def) ?: def
            } catch (ex: Exception) {
                def
            }
        } ?: run {
            def
        }

    }


    private fun getInt(context: Context?, key: String, fallBack: Int): Int {
        return if (context != null) {
            try {
                getSharedPreferences(context)!!.getInt(key, fallBack)
            } catch (ex: ClassCastException) {
                val str = getSharedPreferences(context)!!.getString(key, null)
                if (!TextUtils.isEmpty(str)) {
                    return try {
                        val intValue = Integer.parseInt(str!!)
                        putInt(context, key, intValue)
                        intValue
                    } catch (numberExp: NumberFormatException) {
                        fallBack
                    }

                }
                fallBack
            }

        } else {
            fallBack
        }
    }

    fun clearAllSharedPrefrences(context: Context) {
        try {
            getSharedPreferences(context)?.edit()?.let { shared ->
                shared.clear()
                shared.apply()
            }
        } catch (ex: ClassCastException) {

        }
    }
}
