package whitelisted.garage.utils

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatEditText

class CustomEditText: AppCompatEditText, CustomViewInterface {

    override var borderWidth = 1f
    override val view: View get() = this

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context,attrs){ setBgDrawable(attrs) }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr:Int) : super(context,attrs,defStyleAttr) { setBgDrawable(attrs) }

}