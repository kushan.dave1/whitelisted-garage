package whitelisted.garage.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import com.facebook.appevents.AppEventsLogger
import com.google.firebase.analytics.FirebaseAnalytics
import com.moengage.core.Properties
import com.moengage.core.analytics.MoEAnalyticsHelper
import io.branch.referral.util.BranchEvent
import whitelisted.garage.App
import whitelisted.garage.BuildConfig
import whitelisted.garage.utils.FirebaseAnalyticsLog.FirebaseEventNameENUM.Companion.EVENT_LOGIN
import whitelisted.garage.utils.FirebaseAnalyticsLog.FirebaseEventNameENUM.Companion.EVENT_PURCHASE_GO_COINS
import whitelisted.garage.utils.FirebaseAnalyticsLog.FirebaseEventNameENUM.Companion.EVENT_PURCHASE_G_COMBO
import whitelisted.garage.utils.FirebaseAnalyticsLog.FirebaseEventNameENUM.Companion.EVENT_REGISTER
import whitelisted.garage.utils.FirebaseAnalyticsLog.FirebaseEventNameENUM.Companion.LOG_OUT_USER
import whitelisted.garage.utils.FirebaseAnalyticsLog.FirebaseEventNameENUM.Companion.MOBILE_NUMBER
import whitelisted.garage.utils.FirebaseAnalyticsLog.FirebaseEventNameENUM.Companion.UNIQUE_ID

@SuppressLint("StaticFieldLeak")
class FirebaseAnalyticsLog {

    companion object {
        private var firebaseAnalytics: FirebaseAnalytics? = null
        private var sharedPref: SharedPreferences? = null
        private var contextApp: Context? = null

        var logger: AppEventsLogger? = null
        fun init(context: Context) {
            contextApp = context
            firebaseAnalytics = FirebaseAnalytics.getInstance(context)
            logger = AppEventsLogger.newLogger(context)
            sharedPref = context.getSharedPreferences(AppENUM.RefactoredStrings.APP_NAME,
                Context.MODE_PRIVATE)

        }

        fun trackFireBaseEventLog(eventName: String, keyBundle: Bundle) {
            keyBundle.apply {
                putString(FirebaseEventNameENUM.USER_ROLE,
                    sharedPref?.getString(AppENUM.USER_ROLE, ""))
                putString(FirebaseEventNameENUM.OWNER_NAME,
                    sharedPref?.getString(AppENUM.UserKeySaveENUM.OWNER_NAME, ""))
                putString(FirebaseEventNameENUM.GARAGE_NAME,
                    sharedPref?.getString(AppENUM.UserKeySaveENUM.WORKSHOP_NAME, ""))
                putString(FirebaseEventNameENUM.GARAGE_CITY,
                    sharedPref?.getString(AppENUM.UserKeySaveENUM.WORKSHOP_CITY, ""))
                putString(FirebaseEventNameENUM.USER_ID, sharedPref?.getString(AppENUM.USER_ID, ""))
                putString(FirebaseEventNameENUM.APP_VER, BuildConfig.VERSION_NAME)
                putString(FirebaseEventNameENUM.SHOP_TYPE,
                    sharedPref?.getString(AppENUM.UserKeySaveENUM.SHOP_TYPE, ""))
                putString(FirebaseEventNameENUM.LANGUAGE,
                    sharedPref?.getString(AppENUM.RefactoredStrings.LANGUAGE_CODE,
                        AppENUM.RefactoredStrings.defaultLanguage))
                putString(FirebaseEventNameENUM.COUNTRY_ID,
                    sharedPref?.getString(AppENUM.UserKeySaveENUM.COUNTRY_ID,
                        AppENUM.RefactoredStrings.defaultCountryId))
                putString(FirebaseEventNameENUM.PHONE_NUMBER,
                    sharedPref?.getString(AppENUM.MOBILE_NUMBER,""))

            }
            firebaseAnalytics?.logEvent(eventName, keyBundle) // moenage events
            moengageEvent(eventName, keyBundle)
//            facebookEvent(eventName, keyBundle)
            trackBranchEvent(eventName, keyBundle)
        }

        fun setUniqueUserOrAttribute(eventName: String, value: String) {
            when (eventName) {
                UNIQUE_ID -> {
                    contextApp?.let { MoEAnalyticsHelper.setUniqueId(it, value) }
                }
                MOBILE_NUMBER -> {
                    contextApp?.let { MoEAnalyticsHelper.setMobileNumber(it, value) }
                }
                LOG_OUT_USER -> { //  conntextApp?.let { MoEAnalyticsHelper.lo }
                }
            }
        }

        private fun facebookEvent(eventName: String, keyBundle: Bundle) {
            when (eventName) {
                EVENT_PURCHASE_G_COMBO -> {
                    logger?.logEvent(eventName, keyBundle)
                }
                EVENT_PURCHASE_GO_COINS -> {
                    logger?.logEvent(eventName, keyBundle)
                }
                EVENT_LOGIN -> {
                    logger?.logEvent(eventName, keyBundle)
                }
                EVENT_REGISTER -> {
                    logger?.logEvent(eventName, keyBundle)
                }
            }

        }


        private fun moengageEvent(eventName: String, keyBundle: Bundle) {
            val moEngageProps = Properties()

            val bundleArgs = (keyBundle).keySet()

            for (key in bundleArgs) {
                if (key == null) {
                    continue
                }
                moEngageProps.addAttribute(key, keyBundle.getString(key) ?: "")
            }
            contextApp?.let { MoEAnalyticsHelper.trackEvent(it, eventName, moEngageProps) }
        }

        private fun trackBranchEvent(eventName: String, keyBundle: Bundle?) {
            keyBundle?.let {
                when {
                    eventName.equals(EVENT_LOGIN, ignoreCase = true) -> {
                        branchEventLog(eventName, "", keyBundle)
                    }
                    eventName.equals(EVENT_REGISTER, ignoreCase = true) -> {
                        branchEventLog(eventName, "", keyBundle)
                    }
                    eventName.equals(EVENT_PURCHASE_GO_COINS, ignoreCase = true) -> {
                        branchEventLog(eventName, "", keyBundle)
                    }
                    eventName.equals(EVENT_PURCHASE_G_COMBO, ignoreCase = true) -> {
                        branchEventLog(eventName, "", keyBundle)
                    }
                    else -> {
                        branchEventLog(eventName, "", keyBundle)
                    }

                }
            }
        }


        private fun branchEventLog(eventName: String, affiliation: String, keyBundle: Bundle) {
            val branchEvent = BranchEvent(eventName)
            branchEvent.setCustomerEventAlias(eventName)
            branchEvent.setAffiliation(affiliation)
            val bundleArgs = (keyBundle).keySet()
            for (key in bundleArgs) {
                if (key == null) {
                    continue
                }
                branchEvent.addCustomDataProperty(key, keyBundle.getString(key) ?: "")
            }
            branchEvent.logEvent(App.applicationContext())
        }


    }


    abstract class FirebaseEventNameENUM private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {



            //Attributes
            const val SUBSCRIPTION_INFO = "subscription_info"
            const val SEARCH_TEXT = "search_text"
            const val FEATURE_LOCKED = "feature_locked"
            const val QTY_SELECTED = "qty_selected"
            const val PART_NAME = "part_name"
            const val CART_VALUE = "cart_value"
            const val CART_ITEMS = "cart_items"
            const val IS_ENQUIRY = "is_enquiry"
            const val IS_FASTMOVING = "is_fastmoving"
            const val POPULAR_TAG = "popular_tag"
            const val REMARK = "remark"
            const val PART_NUMBER = "part_number"
            const val PART_SKU_CODE = "part_sku_code"
            const val PART_PRICE = "price"
            const val PART_ID = "part_id"
            const val PART_BRAND = "part_brand"
            const val PART_TYPE = "part_type"
            const val CITY = "city"
            const val STATE = "state"
            const val PIN = "pin"
            const val ITM_QTY = "item_qty"
            const val ITEM_DETAILS = "item_details"
            const val REDIRECTION_URL = "redirection_url"
            const val TOTAL_AMOUNT = "total_amount"
            const val SELECTED_CATEGORY = "selected_category"
            const val SELECTED_BRAND = "selected_brand"
            const val USER_ID = "user_id"
            const val AMOUNT = "amount"
            const val MESSAGE = "MESSAGE"
            const val UPI_ID = "upi_id"
            const val LOG_OUT_USER = "LOG_OUT_USER"
            const val UNIQUE_ID = "unique_id"
            const val MOBILE_NUMBER = "mobile_number"
            const val MODE_OF_PAYMENT = "mode_of_payment"
            const val WORKSHOP_COUNT = "workshop_count"
            const val PHONE_NUMBER = "phone_number"
            const val GARAGE_NAME = "garage_name"
            const val GARAGE_CITY = "garage_city"
            const val OWNER_NAME = "owner_name"
            const val FIRE_SCREEN = "fire_screen"
            const val UPLOAD_STATUS = "upload_status"
            const val COUPON_VALUE = "coupon_value"
            const val COUPON = "coupon"
            const val LANGUAGE = "language"
            const val POSITION = "position"
            const val BANNER_POSITION = "banner_pos"
            const val BANNER_RED = "banner_redirection"
            const val USER_ROLE = "user_role"
            const val APP_VER = "app_ver"
            const val SHOP_TYPE = "shop_type"
            const val COUNTRY_ID = "country_id"
            const val CUSTOMER_NAME = "cus_name"
            const val CUSTOMER_EMAIL = "cus_email"
            const val CUSTOMER_PHONE = "cus_phone"
            const val EMP_NAME = "emp_name"
            const val NAME = "name"
            const val EMP_EMAIL = "emp_email"
            const val EMP_PHONE = "emp_num"
            const val EMP_ROLE = "emp_role"
            const val CUSTOMER_CAR_BRAND = "cus_car_brand"
            const val CUSTOMER_CAR_REG = "cus_car_reg"
            const val CUSTOMER_CAR_ODO = "cus_car_odo"
            const val CUSTOMER_ADDRESS = "cus_add"
            const val ADDRESS = "address"
            const val BILL_TYPE = "bill_type"
            const val TAX_TYPE = "tax_type"
            const val GSTIN_NUM = "gstin_num"
            const val PACK_COUNT = "pack_count"
            const val PACK_NAME = "pack_name"
            const val PACK_PRICE = "pack_price"
            const val PACK_ITEMS = "pack_items"
            const val PREVIOUSLY_ORDERED_ITEMS = "previously_ordered_items"
            const val ACTION_TYPE = "action_type"
            const val FILTER_TYPE = "filter_type"
            const val SELECTION_TYPE = "selection_type"
            const val ACTION_UPDATE = "action_update"
            const val ACTION_CREATE = "action_create"
            const val BILL_DATE = "bill_date"
            const val TAB_HOME = "tab_home"
            const val TAB_BILLING = "tab_billing"
            const val TAB_ACCOUNT = "tab_account"
            const val TAB_INVENTORY = "tab_inventory"
            const val TAB_SPARES = "tab_spares"
            const val G_COMBO_AMOUNT = "g_combo_amount"
            const val GC_ADDED = "GC_added"
            const val TOTAL_ITEMS = "total_items"
            const val BRAND = "brand"
            const val MODEL = "model"
            const val PART = "part"
            const val QUANTITY = "qty"
            const val BUYING_PRICE = "buying_price"
            const val SELLING_PRICE = "selling_price"
            const val PAYMENT_AMOUNT = "payment_amount"
            const val PAYMENT_MODE = "payment_mode"
            const val PAYMENT_STATUS = "payment_status"
            const val PACKAGE_NAME = "package_name"
            const val PART_SERVICES = "parts_services"
            const val DISCOUNT = "discount"
            const val TOTAL = "total"
            const val DIAL_CODE = "dial_code"
            const val FBE_SEARCH_TERM = "search_term"
            const val CURRENCY = "currency"
            const val CURRENCY_SYMBOL = "currency_symbol"
            const val PRODUCT_NAME = "product_name"
            const val PRODUCT_COUNT = "product_count"
            const val PRODUCT_BRAND = "product_brand"
            const val PRODUCT_QTY = "product_qty"
            const val ITEM_NAME = "items_name"
            const val ITEM_COUNT = "items_count"
            const val TOTAL_PRICE = "total_price"
            const val AVL_ITEMS = "avail_items"
            const val UN_AVL_ITEMS = "unavail_items"
            const val TOTAL_COST = "total_cost"
            const val RETAILER_NAME = "retailer_name"
            const val RETAILER_LOC = "retailer_location"
            const val RETAILER_NUMBER = "retailer_number"

            const val PART_CATEGORY = "part_category"
            const val PART_SUBCAT = "part_subcat"

            const val ORDER_DETAILS =  "order_details"
            const val AMOUNT_PAID =  "amount_paid"
            const val PARTS_USED =  "parts_used"

            const val NEW_SERVICE =  "new_service"


            const val FILTERS_APPLIED = "filters_applied"
            const val SORT_TYPE = "sort_type"
            const val CATEGORY_SELECTED = "category_selected"
            const val BRANDS_SELECTED = "brands_selected"


            // fire_screen names
            const val FS_LOGIN = "login"
            const val FS_ORDER_SUMMERY = "order_summery"
            const val FS_PAYMENT = "payment"
            const val FS_QRCODE = "qrcode"
            const val FS_REGISTER = "signup"
            const val FS_REMINDER = "reminder"
            const val FS_OTP = "otp"
            const val FS_SETTING = "setting"
            const val FS_ENQUIRY = "Enquiry"
            const val FS_ACCOUNT = "account"
            const val FS_DASHBOARD = "dashboard"
            const val FS_BILLING = "billing"
            const val FS_HOMEPAGE = "homepage"
            const val FS_SPARES_SHOP_PLP = "spares_shop_plp"
            const val FS_MY_CUSTOMERS = "my_customers"
            const val FS_ORDER_ESTIMATES = "order_estimates"
            const val FS_SPARES_SHOP_PDP = "spares_shop_pdp"
            const val FS_SPARES_SHOP_ENQUIRY = "fs_spares_shop_enquiry"
            const val FS_SPARES_SELECT_ADDRESS = "spares_select_address"
            const val FS_SPARES_SHOP_CART = "spares_shop_cart"
            const val FS_SPARES_SORT_AND_FILTER = "spares_sort_and_filter"
            const val FS_SPARES_PAYMENT_TYPE = "spares_payment_type"
            const val FS_INVENTORY_BULK_UPLOAD = "spares_payment_type"
            const val FS_SPARES_RETRY_PAYMENT = "spares_payment_retry"
            const val FS_SPARES_SHOP_SEARCH = "spares_shop_search"
            const val FS_SEARCH_HOMEPAGE = "fs_search_homepage"
            const val FS_SPARES_SHOP = "spares_shop"
            const val FS_LANGUAGE_SELECTION = "language"
            const val FS_COUPON = "apply_coupon"
            const val FS_LEDGER_GVE_OR_TAKE = "ledger_give_or_take"
            const val FS_GO_COIN = "gocoin"
            const val FS_ADD_EMP = "add_employee"
            const val FS_ADD_SHOP = "add_shop"
            const val FS_WORKSHOP_LIST = "workshop_list"
            const val FS_SEARCH_ORDERS = "fs_search_orders"
            const val FS_SELECT_WORKSHOP_DIALOG = "fs_select_workshop_dialog"
            const val FS_NEW_ORDER_DETAILS = "new_order_detail"
            const val FS_NEW_ORDER_DETAILS_SPARE = "new_order_detail_spare"
            const val FS_MANAGE_PACKAGE = "manage_package"
            const val FS_MANAGE_EMP = "manage_employee"
            const val FS_CREATE_PACKAGE = "create_package"
            const val FS_CREATE_EDIT_PACKAGE = "create_edit_package"
            const val FS_G_COMBO_INFO = "g_combo_info"
            const val FS_GMB_EDIT = "gmb_edit"
            const val FS_GMB = "my_gmb"
            const val FS_GMB_COMBO = "g_combo"
            const val FS_SPARES_MARKETPLACE = "spares_marketplace"
            const val FS_WALLET = "wallet"
            const val FS_INVENTORY = "inventory"
            const val FS_BUSINESS_CARD = "business_card"
            const val FS_ADD_INVENTORY_RACK = "add_inventory_rack"
            const val FS_ADD_INVENTORY_RACK_ITEM = "add_inventory_rack_item"
            const val FS_ORDER_INVENTORY = "order_inventory"
            const val FS_JC = "job_card"
            const val FS_ESTIMATE = "estimate"
            const val FS_INSTANT_REMINDER = "instant_reminder"
            const val FS_CUS_REMINDER = "cus_reminder"
            const val FS_ADD_BIDDING = "bidding_search_add"
            const val FS_BIDDING_CART = "bidding_cart_details"
            const val FS_ACC_BID_ENQUIRY = "acc_bid_enquiry"
            const val FS_ACC_BID_DETAILS = "acc_place_bid_detaild"
            const val FS_WORKSHOP_BI_RECEIVE = "workshop_bid_receive"
            const val FS_WORKSHOP_BID_ENQUIRY = "workshop_bid_enquiry"
            const val FS_WORKSHOP_BID_LIST = "workshop_bid_list"
            const val FS_MARKETPLACE_SEARCH = "MARKETPLACE_SEARCH"


            // event name
            const val EVENT_INIT_LOGIN = "init_login"
            const val EVENT_BOTTOM_TAB = "bottom_tab"
            const val EVENT_SEARCH = "search"
            const val EVENT_TAP_WORKSHOP = "tap_workshop"
            const val EVENT_TAP_REMINDER = "tap_reminder"
            const val EVENT_LOGIN = "login"
            const val EVENT_LOGIN_REG = "login_register"
            const val EVENT_REGISTER = "register"
            const val EVENT_INIT_REGISTER = "init_register"
            const val EVENT_TAP_SUPPORT = "tap_support"
            const val EVENT_DOWNLOAD_LATEST = "download_latest_app"
            const val EVENT_LOG_OUT = "logout"
            const val EVENT_INIT_LOG_OUT = "init_logout"
            const val EVENT_TAP_ADD_WORKSHOP = "tap_add_workshop"
            const val EVENT_SELECT_WORKSHOP = "select_workshop"
            const val EVENT_TAP_SEARCH = "tap_search"
            const val EVENT_CREATE_NEW_ORDER = "create_new_order"
            const val EVENT_SAVE_ORDER = "save_order_to"
            const val EVENT_CREATE_PACKAGE = "tap_create_pack"
            const val EVENT_INIT_CREATE_PACKAGE = "init_create_pack"
            const val EVENT_INIT_Add_PACKAGE = "init_add_pack"
            const val EVENT_INIT_BILL_DOWNLOAD = "init_bill_download"
            const val EVENT_QR_GENERATION = "qr_generation"
            const val EVENT_ADD_EMPLOYEE = "add_employee"
            const val EVENT_DELETE_EMPLOYEE = "delete_employee"
            const val EVENT_INIT_EMP_MAN = "init_emp_man"
            const val EVENT_INIT_MANAGE_EMP = "init_manage_employee"
            const val EVENT_INIT_G_COMBO = "init_GCombo"
            const val EVENT_INIT_LEDGER = "init_ledger"
            const val EVENT_INIT_SETTING = "init_settings"
            const val EVENT_INIT_REMINDER = "init_reminder"
            const val EVENT_INIT_QR = "init_QR"
            const val EVENT_INIT_WEBSITE = "init_my_website"
            const val EVENT_INIT_MANAGE_PACK = "init_manage_pack"
            const val EVENT_INIT_WORKSHOP = "init_workshop"
            const val EVENT_INIT_INSTANT_REMINDER = "init_instant_reminder"
            const val EVENT_INIT_MY_CUSTOMER = "init_my_customer"
            const val EVENT_INIT_ORDER_HISTORY = "init_order_history"
            const val EVENT_INIT_FEEDBACK = "init_feedback"
            const val EVENT_INIT_WALLET = "init_wallet"
            const val EVENT_PURCHASE_G_COMBO = "purchase_GCombo"
            const val EVENT_PURCHASE_GO_COINS = "purchase_GoCoins"
            const val EVENT_BUY_GO_COINS = "Gocoins_buy"
            const val EVENT_CLICK_BANNER = "click_banner"
            const val EVENT_INIT_RACK = "init_rack"
            const val EVENT_INIT_GO_COIN = "init_gocoin"
            const val EVENT_INIT_WEBSITE_REQUEST = "init_web_request"
            const val EVENT_INIT_ITEM_INV = "init_item_inv"
            const val EVENT_ADD_ITEMS = "add_items"
            const val EVENT_LEDGER_GIVE = "ledger_give"
            const val EVENT_LEDGER_TAKE = "ledger_take"
            const val EVENT_INIT_ADD_INVENTORY = "init_add_inventory"
            const val EVENT_INIT_PAYMENT = "init_payment_page"
            const val EVENT_INIT_JC = "init_jc"
            const val EVENT_SAVE_INVENTORY = "save_inventory"
            const val EVENT_SAVE_JC = "save_jc"
            const val EVENT_SAVE_ESTIMATE = "save_estimate"
            const val EVENT_INIT_ESTIMATE = "init_estimate"
            const val EVENT_ADD_PAYMENT = "add_payment"
            const val EVENT_SAVE_QR = "save_qr"
            const val EVENT_MARK_COMPLETE = "mark_complete"
            const val EVENT_SHOP_CREATION = "init_shop_creation"
            const val EVENT_SHOP_CREATION_SAVE = "save_shop_creation"
            const val EVENT_LANGUAGE_SELECTION = "language_selection"
            const val EVENT_INIT_LANGUAGE_SELECTION = "init_language"
            const val EVENT_INIT_COUPON = "init_coupon"
            const val EVENT_APPLY_COUPON = "apply_coupon"
            const val EVENT_REQUEST_GMB_CREATION = "request_GCombo_creation"
            const val EVENT_PICK_COUNTRY = "pick_country"
            const val EVENT_INIT_G_COMBO_PREVIEW = "init_GCombo_preview"
            const val EVENT_UNLOCK_G_COMBO = "unlock_GCombo"
            const val EVENT_INIT_G_COMBO_PAYMENT = "init_GCombo_payment"
            const val EVENT_TAP_G_COMBO_REMINDER = "tap_GCombo_reminder"
            const val EVENT_INIT_CUS_REMINDER = "init_cust_reminder"
            const val EVENT_SEND_CUS_REMINDER = "send_cust_reminder"
            const val EVENT_INIT_BIDDING = "init_bidding"
            const val EVENT_BID_ADD_PRODUCT = "bid_add_product"
            const val EVENT_BID_RAISE_ENQUIRY = "bid_raise_enquiry"
            const val EVENT_BID_ENQ_SUCC = "bid_enq_succ"
            const val EVENT_PRODUCT_ACCEPT = "prod_accept"
            const val EVENT_PRODUCT_REJECT = "prod_reject"
            const val EVENT_ACCEPT_ALL = "accept_all"
            const val EVENT_REJECT_ALL = "reject_all"
            const val EVENT_SUBMIT_BID = "submit_bid"
            const val EVENT_UNLOCK_CONTACT = "unlock_contact"
            const val EVENT_ACCEPT_BID = "accept_bid"
            const val EVENT_REJECT_BID = "reject_bid"
            const val EVENT_WORKSHOP_MARK_COMPLETE = "workshop_mark_comp"
            const val EVENT_RETAILER_MARK_COMPLETE = "ret_mark_comp"
            const val INIT_SPARES_MARKET = "init_spares_market"
            const val SELECT_CAT = "select_cat"
            const val SELECT_BRAND = "select_brand"
            const val ADD_TO_CART = "add_to_cart"
            const val VIEW_CART = "view_cart"
            const val SPARES_CHECKOUT = "spares_checkout"
            const val SELECT_MODE = "select_mode"
            const val PLACE_ORDER = "place_order"
            const val RETRY_PAYMENT = "retry_payment"
            const val INIT_ORDER_HISTORY = "init_order_history"
            const val INIT_CART = "init_cart"
            const val INIT_INVENTORY = "init_inventory"
            const val INIT_BULK_INVENTORY = "init_bulk_inventory"
            const val INIT_SAMPLE_DOWNLOAD = "init_sample_download"
            const val UPLOAD_INVENTORY_FILE = "upload_inventory_file"
            const val MARKETPLACE_HOME_CAT = "marketplace_home_cat"
            const val INIT_ACC_CART = "init_acc_cart"
            const val PURCHASE_ACC = "purchase_acc"
            const val POPULAR_PARTS = "popular_parts"
            const val FILTER_SORT = "filter_sort"
            const val REMIND_ME = "remind_me"

            const val LAST_ORDER_DETAILS = "init_last_order_details"
            const val SERVICE_ADDITION = "service_addition"
            const val SPARES_SUBCAT = "spares_subcat"

            const val MARKETPLACE_SEARCH = "marketplace_search"
            const val INIT_ENQUIRE_STOCK = "init_enquire_stock"
            const val INIT_ENQUIRE_OOS = "init_enquire_oos"
            const val ENQUIRE_NOW_STOCK = "enquire_now_stock"
            const val ENQUIRE_NOW_OOS = "enquire_now_oos"
            const val BULK_PURC_APPLIED = "bulk_purc_applied"
            const val PDP_BANNER_CLICK = "pdp_banner_click"

            const val CLICK_SIMILAR = "click_similar"
            const val QTY_DROP_DOWN = "qty_drop_down"
            const val BULK_SAVING = "bulk_saving"
            const val RECENT_SEARCHES = "recent_searches"

            const val PREVIOUSLY_ORDERED = "previously_ordered"
            const val BUNDLE_PRODUCT = "bundle_product"

            const val INIT_MEMBERSHIP = "init_membership"
            const val PURCHASE_MEMBERSHIP = "init_purchase_membership"

            const val MEMBERSHIP_DETAIL = "membership_detail"
            const val ERROR_HEADER_PRO = "error_header_Pro"

            const val INIT_BCARD_PRO = "init_business_card_pro"

            const val CLICK_CHECK_NOW = "click_check_now"

            const val GMB_PRO_APPLIED = "gmb_pro_applied"

            const val PRO_PAYMENT_STATUS = "pro_payment_status"

            const val ADD_TO_CART_PRO = "add_to_cart_pro"

            const val SERIAL_NO_SCANNER = "serial_no_scanner"
            const val CART_DELETION = "cart_deletion"

        }
    }
}