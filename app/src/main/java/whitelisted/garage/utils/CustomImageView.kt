package whitelisted.garage.utils

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.res.ResourcesCompat
import androidx.core.content.withStyledAttributes
import whitelisted.garage.R

class CustomImageView: AppCompatImageView, CustomViewInterface {

    override var borderWidth: Float = 0f
    override val view: View get() = this
    private var isCircular = false
    private val paint by lazy { Paint() }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context,attrs){ setUpAttributes(attrs) }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr:Int) : super(context,attrs,defStyleAttr) { setUpAttributes(attrs) }

    private fun setUpAttributes(attrs: AttributeSet) {
        setBgDrawable(attrs)
        context.withStyledAttributes(attrs, R.styleable.CustomView) {
            isCircular = getBoolean(R.styleable.CustomView_makeCircle,false)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(isCircular) {
            paint.style = Paint.Style.STROKE
            paint.strokeWidth = 5f
            paint.shader = SweepGradient(w.toFloat()/2, h.toFloat()/2, intArrayOf(
                ResourcesCompat.getColor(
                    view.resources,
                    R.color.sweep_color_2,
                    view.resources.newTheme()
                ),
                ResourcesCompat.getColor(
                    view.resources,
                    R.color.sweep_color_1,
                    view.resources.newTheme()
                ),
                ResourcesCompat.getColor(
                    view.resources,
                    R.color.sweep_color_2,
                    view.resources.newTheme()
                )
            ),null
            )
        }
    }

    override fun onDraw(canvas: Canvas?)  {
        super.onDraw(canvas)
        if(isCircular) {
            makeCircular()
            //canvas?.drawCircle(width.toFloat()/2,height.toFloat()/2, width.toFloat()/2,paint)
        }

    }

}