package whitelisted.garage.utils

class AppENUM {
    companion object {

        const val PAYMENT_FAILED = "payment_failed"
        const val IMAGE_SEARCH = "image_search"
        const val CANCELLED = "cancelled"
        const val PENDING = "pending"
        const val ACTIVE = "active"
        const val AI_TOKEN = "TOI00EasyGarage"
        const val SHOW_PREVIOUSLY_ORDERED = "show_previously_ordered"
        const val POPULAR_SEARCHES = "Popular Searches"
        const val RECENT_SEARCHES = "recent_searches"
        const val IS_BULK_APPLIED = "is_bulk_applied"
        const val BIKE_ACCESSORIES_CATALOGUE = "gms/bike-accessories-catalogue/"
        const val GET_ALL_BIKE_SERVICES = "get_all_bike_services"
        const val RETAILERS_BIKE_CATALOGUE = "gms/retailers-bike-catalogue/"
        const val IS_NESTED = "is_nested"
        const val SEARCH_MAP = "search_map"
        const val INVOICE = "invoice"
        const val IS_ORDER = "IS_ORDER"
        const val IS_ONLY_NAME = "IS_ONLY_NAME"
        const val IS_EMPTY = "IS_EMPTY"
        const val IS_EMI = "IS_EMI"
        const val MY_CUSOMTER_MODEL = "MY_CUSOMTER_MODEL"
        const val SEARCH_ORDER_MODEL = "SEARCH_ORDER_MODEL"
        const val MODE_ONLINE = "online"
        const val MODE_OFFLINE = "cash"
        const val MODE_EMI = "emi"
        const val SS_PRODUCT_ID = "ss_product_id"
        const val SYMBOL = ""
        const val GOCOINS = "gocoins"
        const val COUPON = "coupon"
        const val type = "gocoins"
        const val ADDRESS_ID = "address_id"
        const val IS_SEE_ALL = "IS_SEE_ALL"
        const val SPACE = " "
        const val SSITEM = "ss_item"
        const val EXCEL_MIME = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        const val SPARES_PRODUCT_ITEM = "purchase_item"
        const val SPARES_ORDER_ITEM = "order_item"
        const val ADDRESS_TYPE = "ADDRESS_TYPE"
        const val ADD_ADDRESS = "ADD_ADDRESS"
        const val CUSTOMER_DETAILS = "CUSTOMER_DETAILS"
        const val REG_NO = "REG_NO"
        const val ODO_READING = "ODO_READING"
        const val COUNTRY_CODE = "COUNTRY_CODE"
        const val INNER_FRAGMENT = "INNER_FRAGMENT"
        const val COUNTRY_CODE_LIST = "COUNTRY_CODE_LIST"
        const val TITLE = "TITLE"
        const val TITLE_COLOR_ACCENT = "TITLE_COLOR_ACCENT"
        const val DESCRIPTION = "DESCRIPTION"
        const val POSITIVE_BUTTON_TEXT = "POSITIVE_BUTTON_TEXT"
        const val NEGATIVE_BUTTON_TEXT = "NEGATIVE_BUTTON_TEXT"
        const val IS_CREDIT = "IS_CREDIT"
        const val APP_PLAY_STORE_URL =
            "https://play.google.com/store/apps/details?id=whitelisted.garage"
        const val IND_FLAG =
            "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IN.svg"
        const val GET_IP_URL = "https://api64.ipify.org/?format=json"
        const val MAP_URL = "https://maps.google.com/?q="
        const val CALLBACK_URL = "CALLBACK_URL"
        const val BID_PDF_END_POINT = "orders/biding-invoice/?id="
        const val KeyID = "rzp_test_hUP6znNfoU5Aog"
        const val ALL = "ALL"
        const val ID = "ID"
        const val OPEN_DIALOG = "OPEN_DIALOG"
        const val OPEN_FRAG = "OPEN_FRAG"
        const val SaaSGarage = "SaaSGarage"
        const val SENT = "SENT"
        const val UNPAID = "Unpaid"
        const val PAID = "Paid"
        const val GENERIC = "Generic"

        const val SCHEDULE = "SCHEDULE"
        const val SCHEDULE_REMINDER = "schedule_reminder"
        const val SEND_REMINDER = "send_reminder"
        const val View_Reminder_Data = "view_reminder_data"
        const val IS_INVENTORY = "IS_INVENTORY"
        const val LANGUAGE = "LANGUAGE"
        const val TYPE = "TYPE"
        const val GMB = "GMB"
        const val GMB_SMALL = "gmb"
        const val GMB_WEBSITE = "GMBWEBSITE"
        const val IS_FROM_GMB = "IS_FROM_GMB"
        const val IS_FROM_OUTSIDE = "IS_FROM_OUTSIDE"
        const val IS_FROM = "IS_FROM"
        const val IS_FROM_PDP = "IS_FROM_PDP"
        const val FROM_SS_PAYMENT = "FROM_SS_PAYMENT"
        const val IS_FROM_SS_FILTER = "IS_FROM_SS_FILTER"
        const val IS_VIEW_ONLY = "IS_VIEW_ONLY"
        const val IS_FROM_HOME_RETRY = "IS_FROM_HOME_RETRY"
        const val IS_GMB = "IS_GMB"
        const val SINGLE_IMAGE = "SINGLE_IMAGE"
        const val AMOUNT_TO_BE_PURCHASED = "AMOUNT_TO_BE_PURCHASED"
        const val GO_COIN_TO_GET = "GO_COIN_TO_GET"
        const val BRAND_ID = "BRAND_ID"
        const val GO_COIN_BALANCE_AMOUNT = "GO_COIN_BALANCE_AMOUNT"
        const val BIOMETRIC_ENABLED = "BIOMETRIC_ENABLED"
        const val MODEL_ID = "MODEL_ID"
        const val CAR_SEARCH_TYPE = "CAR_SEARCH_TYPE"
        const val IS_SOFT_UPDATE_AVAILABLE = "IS_SOFT_UPDATE_AVAILABLE"
        const val HELP_SUPPORT_MAIL = "developer@freegarage.in"
        const val PLAY_STORE_LINK = "market://details?id="
        const val PLAY_STORE_URL = "https://play.google.com/store/apps/details?id="
        const val INVENTORY_DATA = "INVENTORY_DATA"
        const val INVENTORY_CSV = "-InventoryCSV.csv"
        const val INVENTORY_SAMPLE_CSV = "-InventorySample.xlsx"
        const val INVOICE_PDF = "-Invoive.pdf"
        const val BILLING_CSV = "-BillingCSV.csv"
        const val CAMERA_REQUEST = 123
        const val GALLERY_REQUEST = 124
        const val GO_COINS_VALUE = "GO_COINS_VALUE"
        const val WHATS_APP_PACKAGE = "com.whatsapp"
        const val WHATS_APP_BUSNINESS_PACKAGE = "com.whatsapp.w4b"
        val carDocuments =
            arrayOf("RC", "PUC", "Insurance", "Road Tax", "Passenger Tax", "No Document")
        const val NO_DOCUMENT = "No Document"
        const val TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
        const val DETECTING_CALLS = "detecting_calls"
        const val TAG_PERMISSION = "TAG_PERMISSION"
        const val RECENT_SEARCHES_WORKSHOP = "r_s_workshop"
        const val RECENT_SEARCHES_ACCESSORIES = "r_s_acc"
        const val RECENT_SEARCHES_RETAILER = "r_s_retailer"
        const val blockCharacterSet = "~#^|$%&*!@-_+=&(){}[]:;'<>?/.,"


        var battryBrands =
            arrayOf("Exide", "Amaron", "SF-Sonic", "Tata Green", "Ac-Delco", "Varta", "Other")
        var battryBrandsMalaysia = arrayOf("Amaron", "Century", "Hella", "Yuasa", "Varta", "Other")
        var inventoryItemsInsurance = arrayOf("Perfume",
            "Idol",
            "Fog Lamp",
            "USB",
            "Lighter",
            "Speaker",
            "Jack Set",
            "Chargers",
            "Tool Kit",
            "Spare Wheel",
            "Woofers")
        var inventoryItemsBikes =
            arrayOf("Perfume", "Idol", "USB", "Lighter", "Jack Set", "Chargers", "Tool Kit")

        //val timeSlotList: Array<String> = arrayOf("10 AM to 11 AM", "11 AM to 12 PM", "12 PM to 01 PM", "01 PM to 02 PM", "02 PM to 03 PM", "03 PM to 04 PM", "04 PM to 05 PM", "05 PM to 06 PM", "06 PM to 07 PM", "07 PM to 08 PM", "08 PM to 09 PM", "09 PM to 10 PM", "10 PM to 11 PM", "10 AM to 12 PM", "12 PM to 02 PM", "02 PM to 04 PM", "04 PM to 06 PM")
        //val timeSlotPost = arrayOf("10:30:00", "11:30:00", "12:30:00", "13:30:00", "14:30:00", "15:30:00", "16:30:00", "17:30:00", "18:30:00", "19:30:00", "20:30:00", "21:30:00", "22:30:00", "10:00:00", "12:00:00", "14:00:00", "16:00:00")
        val timeSlotList: Array<String> = arrayOf("10 AM to 11 AM",
            "11 AM to 12 PM",
            "12 PM to 01 PM",
            "01 PM to 02 PM",
            "02 PM to 03 PM",
            "03 PM to 04 PM",
            "04 PM to 05 PM",
            "05 PM to 06 PM",
            "06 PM to 07 PM",
            "07 PM to 08 PM",
            "08 PM to 09 PM",
            "09 PM to 10 PM",
            "10 PM to 11 PM")
        val timeSlotPost = arrayOf("10:00:00",
            "11:00:00",
            "12:00:00",
            "13:00:00",
            "14:00:00",
            "15:00:00",
            "16:00:00",
            "17:00:00",
            "18:00:00",
            "19:00:00",
            "20:00:00",
            "21:00:00",
            "22:00:00")
        val timeSlotPostHalf = arrayOf("10:30:00",
            "11:30:00",
            "12:30:00",
            "13:30:00",
            "14:30:00",
            "15:30:00",
            "16:30:00",
            "17:30:00",
            "18:30:00",
            "19:30:00",
            "20:30:00",
            "21:30:00",
            "22:30:00")

        val newTimeSlotList: Array<String> = arrayOf("10 AM to 11 AM",
            "11 AM to 12 PM",
            "12 PM to 01 PM",
            "01 PM to 02 PM",
            "02 PM to 03 PM",
            "03 PM to 04 PM",
            "04 PM to 05 PM",
            "05 PM to 06 PM",
            "06 PM to 07 PM",
            "07 PM to 08 PM",
            "08 PM to 09 PM",
            "09 PM to 10 PM",
            "10 PM to 11 PM")
        val newTimeSlotPost = arrayOf("10:00:00",
            "11:00:00",
            "12:00:00",
            "13:00:00",
            "14:00:00",
            "15:00:00",
            "16:00:00",
            "17:00:00",
            "18:00:00",
            "19:00:00",
            "20:00:00",
            "21:00:00",
            "22:00:00")

        val FILTER_LIST_REMINDER = mutableListOf<String>().apply {
            add("1week")
            add("1month")
            add("6month")
            add("1year")
        }
        const val TODAY = "today"
        const val MONTH = "month"
        const val YESTERDAY = "yesterday"
        const val CUSTOM = "custom"

        val FILTER_LIST_ORDER_HISTORY = mutableListOf<String>().apply {
            add("Today")
            add("Yesterday")
            add("This Month")
            add("Custom")
        }
        val FILTER_LIST_WS_BID = mutableListOf<String>().apply {
            add("New")
            add("OnGoing")
            add("Complete")
        }

        //        val FILTER_LIST_REMINDER_DATA = mutableListOf<String>().apply {
        //            add(R.string._1_week)
        //            add("1 month")
        //            add("6 month")
        //            add("1 year")
        //        }
        val DAYS_DATA = listOf<String>(
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday",
        )
        val DAYS_BOOLEAN = listOf<Boolean>(
            true,
            true,
            true,
            true,
            true,
            true,
            true,
        )
        val DAYS_TIME = listOf<String>("09:00", "21:00")
        const val IS_MY_CURRENT_LOCATION = "IS_MY_CURRENT_LOCATION"
        const val PLACES_ID = "PLACES_ID"
        const val LATITUDE = "LATITUDE"
        const val LONGITUDE = "LONGITUDE"
        const val GIVE = "Give"
        const val TAKE = "Take"
        const val NULL = "null"

        const val WALKTHROUGH_SHOWN = "WALKTHROUGH_SHOWN"
        const val USER_DATA = "USER_DATA"
        const val DATA = "DATA"
        const val USER_ROLE = "USER_ROLE"
        const val CREDIT = "credit"
        const val FAQ = "FAQ"
        const val DEBIT = "debit"
        const val HEADING = "HEADING"
        const val USER_ID = "USER_ID"
        const val AUTHORIZATION = "Authorization"
        const val CRAPP = "CRAPP"
        const val FLEET_ID = "fleet_id"
        const val ORDER_ID = "order_id"
        const val ORDERS_COUNT = "ORDERS_COUNT"
        const val PACKAGE_COUNT = "PACKAGE_COUNT"
        const val PRO_LOCKED_ALERT_MSG = "PRO_LOCKED_ALERT_MSG"
        const val GARAGE_ESCALATE_REMARK = "garage_escalate_remark"
        const val DELIVERY_DATE = "delivery_date"
        const val ESTIMATE_FLAG = "estimate_flag"
        const val IS_PDF = "is_pdf"
        const val ORDER_MODEL = "order_model"
        const val POSITION = "position"
        const val DEEPLINK_INSTANT_REMINDER = "DEEPLINK_INSTANT_REMINDER"
        const val PACKAGE = "package"
        const val SERVICE = "service"
        const val CAR_ID = "car_id"
        const val CART_ID = "cart_id"
        const val ORDER_TYPE = "order_type"
        const val LEAD_TYPE = "lead_type"
        const val PACKAGE_CAR_ID = "package_car_id"
        const val JOB_CARD = "job_card"
        const val EXTERIOR_IMAGE_PICKER_CODE = 201
        const val INTERIOR_IMAGE_PICKER_CODE = 202
        const val IMAGE_PICKER_CODE = 203
        const val IMAGE_PICKER_FOR_EDIT_WORKSHOP = 204
        const val ESTIMATE = "estimate"
        const val SERVICE_NAME = "service_name"
        const val GARAGE_ID = "garage_id"
        const val FLEET_DEAL_ID = "fleet_deal_id"
        const val GARAGETYPE = "Operation/Garage"
        const val ADMIN = "admin"
        const val OWNER = "Owner"
        const val ORDER_OF_GARAGE = "Garage"
        const val ORDER_OF_GOMECHANIC = "Gomechanic"
        const val TYPE_1 = "1"
        const val TYPE_0 = "0"
        const val PICKUP = 1
        const val WALKIN = 2
        const val NAME = "name"
        const val COMPLETE_REG_DATA = "COMPLETE_REG_DATA"
        const val PHONE_POST = "phone"
        const val LEAD_ID_POST = "lead_id"
        const val FLEET_TYPE_POST = "fleet_type"
        const val COUPON_TYPE_POST = "coupon_code_id"
        const val ODOMETER_POST = "odometer_reading"
        const val CR_POST = "cr_id"
        const val GARAGE_POST = "garage_id"
        const val ARRIVAL_MODE_POST = "arrival_mode"
        const val MOBILE = "mobile"
        const val OTP = "otp"
        const val USER_TYPE = "user_type"
        const val EMAIL = "email"
        const val CHANNEL = "channel"
        const val LOCATION_ID = "location_id"
        const val APP_NAME = "app_name"
        const val ACCESS_TOKEN_ = "ACCESS_TOKEN_"
        const val INSURANCE_ORDER_TYPE = 103
        const val RETAIL_ORDER_TYPE = 101
        const val PREVENTIVE_ORDER_TYPE = 100
        const val SOS_ORDER_TYPE = 107


        // Inventory parameter
        const val FLEET_ID_POST = "fleet_id"
        const val CAR_ID_POST = "car_id"
        const val ORDER_ID_POST = "order_id"
        const val INVENTORY_ITEMS_POST = "inventory_items"
        const val BATTERY_INFO_POST = "battery_info"
        const val OTHERS_POST = "others"
        const val ID_POST = "id"
        const val INITIAL_PROBLEM_POST = "initial_problem"
        const val FUEL_METER_READING_POST = "fuel_meter_reading"
        const val INVENTORY_DOCUMENTS_POST = "inventory_documents"
        const val INVENTORY_COUNT_POST = "inventory_count"
        const val FILE_IMAGES = "insuranceFile"
        const val MASK_IMAGE = "mask_image"
        const val ADDRESS_POST = "address"
        const val RECON = "recon"
        const val PAGE = "page"
        const val PAGE_NAME = "pagename"

        //        const val WORKSHOP_ID = "workshop_id"
        const val GMB_GO_COIN_BALANCE = "GMB_GO_COIN_BALANCE"
        const val GMB_CONFIG = "GMB_CONFIG"
        const val GMB_GO_COIN_PRICE = "GMB_GO_COIN_PRICE"
        const val EPOCH = "epoch"
        const val START_DATE = "start_date"
        const val END_DATE = "end_date"
        const val LEAD_MODEL = "lead_model"
        const val DRIVER_MODEL = "driver_model"
        const val GARAGE_GST_POST = "garage_gst_status"
        const val PICK_DATE_POST = "pick_date"
        const val TIME_SLOT_POST = "time_slot"
        const val IS_IGST = "is_igst"
        const val EXPRESS_PRIORITTY = "1"
        const val OLD_PHOTO_ADAPTER = 1
        const val NEW_PHOTO_ADAPTER = 2
        const val CLUSTER_PHOTO_ADAPTER = 3
        const val PAYMENT_PHOTO_ADAPTER = 4

        //VOICE RECORDING
        //TIME LIST FRAGMENT
        const val DELIVERY_TIME = "delivery_time"
        const val ORDER_PRIORITY = "order_priority"
        const val PICKUP_TIME = "pickup_time"
        const val INSURANCE_TYPE_ORDER = "insurance_order_type"
        const val COMPANY_ID = "company_id"

        ///Order Status
        const val GENERAL = "general"

        //        billing
        const val IS_START = "IS_START"
        const val START_TIME = "START_TIME"
        const val DUE_AMOUNT = "DUE_AMOUNT"

        // Inventory parameter
        const val DEFAULT_LEADS = "14"
        const val IS_GOMECHANIC = "is_gomechanic"
        const val HST_ORDER_ACT_ID = "hstOrderActId"
        const val CONTRACT_TYPE = "contract_type"
        const val REPLACED = "replaced"
        const val TOPUP = "Top-up"
        const val INSURANCE = "insurance"
        const val IMAGE_URL = "https://dev.eapp.gomechanic.app/"
        const val VIDEO_URL = "VIDEO_URL"
        const val FLEET_TYPE = "fleet_type"
        const val FILE_ID = "file_id"
        const val IS_MULTIPLE = "is_multiple"

        const val START_DATE_PICKER = "START_DATE_PICKER"
        const val END_DATE_PICKER = "END_DATE_PICKER"
        const val FCM_TOKEN = "FCM_TOKEN"
        const val SELECTED_PART = "SELECTED_PART"
        const val SELECT_ADDRESS = "SELECT_ADDRESS"
        const val SELECT_ADDRESS_CART = "SELECT_ADDRESS_CART"
        const val SELECT_ADDRESS_PDP = "SELECT_ADDRESS_PDP"
        const val SELECT_ADDRESS_ENQUIRY = "SELECT_ADDRESS_ENQUIRY"
        const val SELECT_ADDRESS_PAYMENT = "SELECT_ADDRESS_PAYMENT"
        const val CURRENCY_SYMBOL = "CURRENCY_SYMBOL"
        const val SELECTED_WORK_DONE = "SELECTED_WORK_DONE"
        const val IS_ACCESSORIES = "IS_ACCESSORIES"
        const val IS_ACCESSORIES_CART = "IS_ACCESSORIES_CART"
        const val IS_FROM_COMPLETE_REG = "IS_FROM_COMPLETE_REG"
        const val IS_REG = "IS_REG"
        const val IS_BIKE_SELECTION = "IS_BIKE_SELECTION"
        const val IS_JOB_CARD = "IS_JOB_CARD"
        const val SHOW_INVENTORY_LIST = "SHOW_INVENTORY_LIST"
        const val PRICE_PER_ITEM = "PRICE_PER_ITEM"
        const val QUANTITY = "QUANTITY"
        const val TAX_RATE = "TAX_RATE"
        const val TOTAL_AMOUNT = "TOTAL_AMOUNT"
        const val DAY_TIME_DATA = "DAY_TIME_DATA"
        const val CATEGORIES_DATA = "CATEGORIES_DATA"
        const val CATEGORY_DATA = "CATEGORY_DATA"
        const val IS_COMBO_PURCHASED = "IS_COMBO_PURCHASED"

        const val PAID_TAB = 1
        const val PAYMENT_DUE_TAB = 2
        const val APPROVED_TAB = 3
        const val ESCALATED_TAB = 4
        const val REQUESTED_GOM_TAB = 5
        const val SETTLEMENTS_PAYMENT_SUMMARY_TAB = 6
        const val SETTLED_BILLS_TAB = 7

        const val CAR_BASED_ORDERS_TAB = 1
        const val BULK_ORDERS_TAB = 2

        const val CAR_TYPE_DATA = "car_type"
        const val DUPLICATE_ID = "21"

        const val WORK_DONE_LIST = "WORK_DONE_LIST"

        const val DDMMYYYY = "dd-MM-yyyy"
        const val YYYYMMDD = "yyyy-MM-dd"
        const val LIMIT = "limit"
        const val OFFSET = "offset"
        const val STATUS_IDS = "status_ids"
        const val SEARCH_STRING = "search_string"
        const val SEARCH_REG = "search_reg"
        const val FOREGROUND_SERVICE_NOTI_ID = 1111

        //SharedPrefrence
        const val B2B_ORDER = "0"
        const val PREPAID = 1
        const val GENDER = "gender"
        const val UPI_ID = "upi_id"
        const val MOBILE_NUMBER = "MOBILE_NUMBER"
        const val V = "v"

        //Health Card
        const val HEALTH_CARD_MODEL = "Health_card_model"
        const val MALFUNCTION_SUB_SERVICE_POSITION = 1
        const val DENT_PENT_SUB_SERVICE_POSITION = 7
        const val leftSwipe = 2
        const val isColor = "1"
        const val isNotColor = "0"
        const val ELECTRICAL_EQUIPMENT_MALFUNCTION_CHECK = "7_2"
        const val OTHERS_TO_BE_ENTERED_MANUALLY_CHECK = "OTHERS_TO_BE_ENTERED_MANUALLY_CHECK"
        const val ELECTRICAL_EQUIPMENT_MALFUNCTION =
            "Electrical Equipment Malfunction (To be entered Manually)"

    }

    abstract class IntentKeysENUM private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {

            const val SHOP_TYPE = "SHOP_TYPE"
            const val SELECT_TAB = "SELECT_TAB"
            const val COUNTRY_CODE = "COUNTRY_CODE"
            const val COUNTRY_FLAG = "COUNTRY_FLAG"
            const val MOBILE_NUMBER = "MOBILE_NUMBER"
            const val IS_FORGOT = "IS_FORGOT"
            const val IS_LOGIN = "IS_LOGIN"
            const val IS_UNDER_TAB = "IS_UNDER_TAB"
            const val IS_NEW_ORDER = "IS_NEW_ORDER"
            const val ORDER_ID = "ORDER_ID"
            const val PACKAGE_NAME = "PACKAGE_NAME"
            const val IS_EDIT = "IS_EDIT"
            const val SINGLE_PARCELABLE_DATA = "SINGLE_PARCELABLE_DATA"
            const val IS_NEW_PACKAGE = "IS_NEW_PACKAGE"
            const val IS_TWO_WHEELER = "IS_TWO_WHEELER"
            const val WORKSHOP_MODEL = "WORKSHOP_MODEL"
            const val PACKAGE_ID = "PACKAGE_ID"
            const val IS_DUPLICATE = "IS_DUPLICATE"
            const val WEB_VIEW_URL = "WEB_VIEW_URL"
            const val PAYMENT_MODEL = "PAYMENT_MODEL"
            const val WORKSHOP_ID = "WORKSHOP_ID"
            const val WORKSHOP_BID_DATA = "WORKSHOP_BID_DATA"
            const val TYPE = "TYPE"
            const val POSITION = "POSITION"
            const val BRAND_NAME = "BRAND_NAME"
            const val CATEGORY_NAME = "CATEGORY_NAME"
            const val PLP_TOP_BANNER = "PLP_TOP_BANNER"
            const val PLP_BOTTOM_BANNER = "PLP_BOTTOM_BANNER"
            const val PLP_BANNERS = "PLP_BANNERS"
            const val PLP_BANNERS_POSITION = "PLP_BANNERS_POSITION"
        }
    }

    abstract class ConfigConstants private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {

            const val PDP_BANNER = "ec_banner"
            const val ACCOUNT_MANAGE_EMPLOYEES = "manage_employees"
            const val ACCOUNT_MANAGE_WORKSHOP = "manage_workshops"
            const val ACCOUNT_REMINDER = "reminder"
            const val ACCOUNT_QR_CODE = "qr_code"
            const val GMB_WEBSITE = "GMBWEBSITE"
            const val ACCOUNT_MY_CUSTOMERS = "my_customers"
            const val ACCOUNT_MANAGE_PACKAGES = "manage_packages"
            const val ACCOUNT_LEDGER = "ledger"
            const val ACCOUNT_ORDER_HISTORY = "order_history"
            const val ACCOUNT_SETTINGS = "settings"
            const val ACCOUNT_FEEDBACK = "feedback"
            const val ACCOUNT_SHARE_APP = "share"
            const val ACCOUNT_GMB = "GMB"
            const val BUSINESS_CARD = "business_card"
            const val BILLING = "billing"

            const val GMB_COMBO = "GMB_COMBO"
            const val INSTNAT_REMINDER = "INSTNAT_REMINDER"


            const val DEEPLINK_INVENTORY = "inventory"
            const val DEEPLINK_QR = "qr"
            const val DEEPLINK_GMB = "gmb"
            const val DEEPLINK_GMWEB = "gmweb"
            const val DEEPLINK_REMINDERS = "reminders"
            const val DEEPLINK_LEDGER = "ledger"
            const val DEEPLINK_ACCOUNT = "account"
            const val DEEPLINK_MY_CUSTOMERS = "mycustomers"
            const val DEEPLINK_MANAGE_EMPLOYEE = "emp"
            const val DEEPLINK_GO_COINS_WALLET = "wallet"
            const val DEEPLINK_BILLING = "billing"
            const val DEEPLINK_MANAGE_PACKAGE = "pack"
            const val DEEPLINK_ORDER_HISTORY = "history"
            const val DEEPLINK_ONGOING_ORDERS = "ongoing"
            const val DEEPLINK_HOME = "home"
            const val DEEPLINK_PROFILE = "myprofile"
            const val DEEPLINK_CREATE_ORDER = "neworder"
            const val DEEPLINK_GMB_COMBO = "gbc"
            const val DEEPLINK_REFERRAL = "referral"
            const val DEEPLINK_REFER = "refer"
            const val DEEPLINK_INSTANT_REMINDER = "instantremin"
            const val DEEPLINK_BIDING_HOME = "bidhome"
            const val DEEPLINK_BIDING_LIST = "bidlist"
            const val DEEPLINK_SS_MARKETPLACE = "marketplace"
            const val DEEPLINK_SPARES_CART = "spares_cart"
            const val DEEPLINK_SS_BRAND_LIST = "brand_list"
            const val DEEPLINK_SS_CATEGORY_LIST = "category_list"
            const val DEEPLINK_SS_ORDER_HISTORY = "order_history"
            const val DEEPLINK_BULK_INVENTORY = "bulk_inventory"
            const val DEEPLINK_BUSINESS_CARDS = "cards"
//            const val DEEPLINK_SS_BRAND_AUTOGOLD = "autogold"
//            const val DEEPLINK_SS_BRAND_LUMAX = "lumax"
//            const val DEEPLINK_SS_BRAND_MOTHERSON = "motherson"
//            const val DEEPLINK_SS_BRAND_SUBROS = "subros"
//            const val DEEPLINK_SS_BRAND_UNOMINDIA = "Uno Minda"
//            const val DEEPLINK_SS_BRAND_GABRIEL = "gabriel"
//            const val DEEPLINK_SS_BRAND_VALEO = "valeo"
//            const val DEEPLINK_SS_BRAND_GMSPARES = "gmspares"
//            const val DEEPLINK_SS_BRAND_GMACC = "gmacc"
            const val DEEPLINK_SS_PRODUCT_DETAIL = "skuid"
            const val DEEPLINK_SS_CATEGORY = "ss_category"
            const val DEEPLINK_AS_CATEGORY = "as_category"
            const val DEEPLINK_SS_BRAND = "ss_brand"
            const val DEEPLINK_ALL_ACC = "allacc"
            const val DEEPLINK_ALL_SPARES = "allspares"
            const val DEEPLINK_BUSINESS_CARD = "business_card"
            const val DEEPLINK_MEMBERSHIP = "membership"

        }
    }


    abstract class UserKeySaveENUM private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {

            const val WORKSHOP_NAME = "WORKSHOP_NAME"
            const val WORKSHOP_CITY = "WORKSHOP_CITY"
            const val OWNER_NAME = "OWNER_NAME"
            const val OWNER_ID = "OWNER_ID"
            const val WORKSHOP_GSTIN = "WORKSHOP_GSTIN"
            const val OWNER_MOBILE = "OWNER_MOBILE"
            const val WORKSHOP_MOBILE = "workshop_mobile"
            const val WORKSHOP_ID = "workshop_id"
            const val SHOP_TYPE = "shop_type"
            const val USER_TYPE = "user_type"
            const val USER_ROLE = "user_role"
            const val USER_ID = "user_id"
            const val PHONE_NUMBER = "PHONE_NUMBER"
            const val SHOP_ADDRESS = "SHOP_ADDRESS"
            const val COUNTRY_CODE = "COUNTRY_CODE"
            const val COUNTRY_FLAG = "COUNTRY_FLAG"
            const val COUNTRY_ID = "COUNTRY_ID"
            const val CURRENCY = "CURRENCY"
            const val CURRENCY_SYMBOL = "CURRENCY_SYMBOL"
            const val EMAIL = "EMAIL"
            const val HOME_SCREEN_RESPONSE = "HOME_SCREEN_RESPONSE"
            const val CONFIG_API_RESPONSE = "CONFIG_API_RESPONSE"
            const val SPARES_HOME_RESPONSE = "SPARES_HOME_RESPONSE"
            const val INVENTORY_RESPONSE = "INVENTORY_RESPONSE"
            const val MEMBERSHIP_STATUS = "MEMBERSHIP_STATUS"

        }
    }

    abstract class PaymentTypeENUM private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {

            const val ONLINE = "online"
            const val CASH = "cash"
            const val UPI = "upi"
            const val WALLET = "wallet"
            const val EMI = "EMI"

        }
    }


    abstract class DateUtilKey private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {
            const val TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            const val TIME_FORMAT_NZ = "yyyy-MM-dd'T'HH:mm:ss"
            const val LIST_DISPLAY_FORMAT = "dd MMM, hh:mm a"
            const val FORMAT_WITH_TIME = "yyyy-MM-dd HH:mm:ss"
            const val FORMAT_WITHOUT_SECONDS = "yyyy-MM-dd HH:mm"
            const val FORMAT_IN_SIMPLE = "yyyy-MM-dd"
            const val FORMAT_OUT_SIMPLE = "yyyy-MM-dd"
            const val FORMAT_CREATED_DATE = "dd MMM yyyy hh:mm a"
            const val FORMAT_UPDATED_DATE = "EEE, dd MMM yy"
            const val FORMAT_SIMPLE_YEAR_FIRST = "yyyy-MM-dd"
            const val DD_MM_YYYY = "dd-MM-yyyy"
            const val DD_MM = "dd MMM"

        }
    }

    abstract class RazorPayENUM private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {
            const val RAZORPAY_METHOD = "method"
            const val RAZORPAY_WALLET = "wallet"
            const val RAZORPAY_UPI = "upi"
            const val RAZORPAY_MODE = "mode"

        }
    }

    abstract class RefactoredStrings private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {

            const val APPLICATION_PDF = "application/pdf"
            const val PROVIDER = ".provider"
            const val COUNTRY = "country"
            const val CURRENCY = "currency"
            const val TYPE = "type"
            const val VERSION_CODE = "versioncode"
            const val SEE_MORE = "See More"
            const val SEE_LESS = "See Less"
            const val DUE = "due"
            const val WORKSHOP_CONSTANT = "workshop"
            const val ACCESSORIES_CONSTANT = "acc"
            const val RETAILER_CONSTANT = "retailer"
            const val IMAGE_PICKER_REQUEST_CODE_INVENTORY_EXTERIOR = 123
            const val IMAGE_PICKER_REQUEST_CODE_INVENTORY_INTERIOR = 124
            const val APP_VERSION = "app_version"
            const val APP_NAME = "Whitelisted_GarageAndroid"
            const val IS_DEBUG = "is_debug"
            const val WORKSHOP_ID_HEADER = "workshop"
            const val CLOSE_POPUP = "Close PopUp"
            const val YES = "Yes"
            const val LANGUAGE = "language"
            const val LANGUAGE_CODE = "language_code"
            const val COUNTRY_CODE = "country_code"
            const val BRAND = "BRAND"
            const val IS_BRAND = "is_brand"
            const val CATEGORY = "CATEGORY"
            const val IS_CATEGORY = "is_category"
            const val GC_OUTSIDE_VAL_HOME = "GC_OUTSIDE_VAL_HOME"
            const val GC_OUTSIDE_VAL_GMB = "GC_OUTSIDE_VAL_GMB"
            const val GC_OUTSIDE_VAL_INSUFFICIENT_DIALOG = "GC_OUTSIDE_VAL_INSUFFICIENT_DIALOG"
            const val GC_OUTSIDE_VAL_SPARES_SHOP = "GC_OUTSIDE_VAL_SPARES_SHOP"
            const val GC_OUTSIDE_VAL_PRO = "GC_OUTSIDE_VAL_PRO"
            const val GC_OUTSIDE_VAL_BUY_SUBS = "GC_OUTSIDE_VAL_BUY_SUBS"
            const val ADDRESS_ID = "address_id"

            val languageNames = arrayOf("English", "हिंदी", "தமிழ்", "తెలుగు", "Bahasa Melayu")
            val languageCodes = arrayOf("en", "hi", "ta", "te", "ms")
            val countryIsoCodes = arrayOf("IN", "MYS")
            val countryDefaultLanguageCodes = arrayOf("en", "ms")
            val countryDialerCodes = arrayOf("+91", "+60")
            val countryNames = arrayOf("India", "Malaysia")

            const val defaultCurrency = "INR"
            const val defaultCurrencySymbol = "₹"
            const val defaultCountryCode = "+91"
            const val defaultLanguage = "en"
            const val cusName = "<Customer Name>"
            const val amount = "<Amount Due>"
            const val defaultFlag =
                "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IN.svg"
            const val defaultCountryId = "0"
            const val OPEN_ORDER_STATUS = 10
            const val OPEN_ORDER_STATUS_2 = 20
            const val WIP_ORDER_STATUS = 30
            const val READY_ORDER_STATUS = 80
            const val COMPLETED_ORDER_STATUS = 150
            const val PATH_WHITELISTED_APP_DATA = "whitelisted_app_data/"
            const val PATH_WORKSHOPS = "workshops/"
            const val PATH_WORKSHOP_IMAGE = "workshop_image/"
            const val PATH_BUSINESS_CARDS = "business_cards/"
            const val PATH_USERS = "${PATH_WHITELISTED_APP_DATA}users/"
            const val PATH_PROFILE = "profile/"
            const val PATH_EMPLOYEES = "employees/"
            const val PATH_ORDERS = "orders/"
            const val PATH_GMB = "gmb/"
            const val PATH_WORKSHOP_BIDDING = "workshop_biding/"
            const val PATH_BIDING_ORDER = "workshop_biding/"
            const val PATH_WEBSITE = "website/"
            const val PATH_INVENTORY = "inventory/"
            const val PATH_INVENTORY_RACKS = "${PATH_INVENTORY}racks/"
            const val PATH_ORDER_INVENTORY = "order_inventory/"
            const val PATH_EXTERIOR_IMAGES = "exterior_images/"
            const val PATH_INTERIOR_IMAGES = "interior_images/"
            const val PATH_SIGNATURE_IMAGE = "signature_image/"
            const val PATH_SS_PRODUCT_ENQUIRY = "ss_product_enquiry/"
            const val PATH_AUDIO_FILE =
                "${PATH_ORDER_INVENTORY}audio_file/" // //            const val WORKSHOP_IMAGE_PATH = "$PATH_WHITELISTED_APP_DATA$PATH_WORKSHOPS$PATH_WORKSHOP_IMAGES" //            const val IMAGE_PROFILE_PATH = "$PATH_WHITELISTED_APP_DATA$PATH_USERS$PATH_PROFILE_IMAGES" //            const val EMPLOYEE_IMAGE_PATH = "$PATH_WHITELISTED_APP_DATA$PATH_EMPLOYEES$PATH_EMPLOYEE_IMAGES" //            const val ORDER_DETAILS_PATH = "$PATH_WHITELISTED_APP_DATA$PATH_ORDERS" //            const val GMB_PATH = "$PATH_WHITELISTED_APP_DATA$PATH_WORKSHOPS$PATH_GMB" //            const val GMB_WEBSITE_PATH = "$PATH_WHITELISTED_APP_DATA$PATH_WORKSHOPS$PATH_WEBSITE"
            //            const val WORKSHOP_INVENTORY_PATH = "$PATH_WHITELISTED_APP_DATA$PATH_WORKSHOPS$PATH_INVENTORY"
            //            const val INVENTORY_CAR_EXTERIOR_IMAGES =
            //                "$PATH_ORDER_INVENTORY$PATH_EXTERIOR_IMAGES"
            //            const val INVENTORY_CAR_INTERIOR_IMAGES =
            //                "$PATH_ORDER_INVENTORY$PATH_INTERIOR_IMAGES"
            //            const val INVENTORY_SIGNATURE_IMAGE =
            //                "$PATH_ORDER_INVENTORY$PATH_SIGNATURE_IMAGE"
            //            const val INVENTORY_AUDIO_FILE =
            //                "$PATH_WHITELISTED_APP_DATA$PATH_EMPLOYEE$PATH_WORKSHOP_IMAGES"
        }
    }

    abstract class EnglishStrings private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {
            const val Credit = "Credit"
        }
    }

    abstract class LanguageConstants private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {
            const val LAN_DEFAULT_CURRENCY = "INR"
            const val LAN_DEFAULT = "en"
            const val LAN_ENGLISH = "en"
            const val LAN_HINGLISH = "gl"
            const val LAN_HINDI = "hi"
            const val LAN_BANGLA = "bn"
            const val LAN_GUJARAT = "gu"
            const val LAN_KANNADA = "kn"
            const val LAN_MARATHI = "mr"
            const val LAN_TAMIL = "ta"
            const val LAN_TELEAGU = "te"

            const val ENGLISH = "English"
            const val HINGLISH = "Hinglish"
            const val HINDI = "हिंदी"
            const val BANGLA = "বাংলা"
            const val GUJARAT = "ગુજરાતી"
            const val KANNADA = "ಕನ್ನಡ"
            const val MARATHI = "मराठी"
            const val TAMIL = "தமிழ்"
            const val TELEAGU = "తెలుగు"

            const val WORD_ENGLISH = ""
            const val WORD_HINGLISH = ""
            const val WORD_HINDI = "Hindi"
            const val WORD_BANGLA = "Bangla"
            const val WORD_GUJARAT = "Gujarati"
            const val WORD_KANNADA = "Kannada"
            const val WORD_MARATHI = "Marathi"
            const val WORD_TAMIL = "Tamil"
            const val WORD_TELEAGU = "Telugu"
        }
    }

    abstract class FilterString private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {
            const val ALL = ""
            const val SENT = "sent"
            const val SCHEDULE = "scheduled"
            const val CREDIT = "credit"
            const val DEBIT = "debit"
            const val GMB = "gmb"
            const val WEBSITE = "website"

        }
    }

    abstract class WSBiddingStatusConstant private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {
            const val NEW = 1
            const val ON_GOING = 2
            const val COMPLETED = 3
            const val CLOSED = 4
            const val SUBMITTED = 5
            const val ACCEPTED = 6
            const val REJECTED = 7
            const val CANCLED = 8

        }
    }

    abstract class Profile private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {
            const val OWNER_NAME = "OWNER_NAME"
            const val PHONE_NUMBER = "PHONE_NUMBER"
            const val EMAIL = "EMAIL"
            const val JOINING_DATE = "JOINING_DATE"
            const val ROLE = "ROLE"
            const val WORKSHOP_NAME = "WORKSHOP_NAME"
            const val WORKSHOP_ADD = "WORKSHOP_ADD"
            const val GSTN = "GSTN"


        }
    }

    abstract class OwnerTypeENUM private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {
            const val DISTRIBUTOR = "distributor"
            const val INDIVIDUAL = "individual"


        }
    }

    abstract class EMPROLE private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {
            const val OWNER = "Owner"
            const val SUPERVISOR = "Supervisor"
            const val MECHANIC = "Mechanic"
            const val DRIVER = "Driver"


        }
    }

    abstract class AdaptersConstant private constructor() {
        init {
            throw IllegalStateException("")
        }

        companion object {
            const val DEL_PACKAGE = "del_package"
            const val ADDED_PART = "added_part"
            const val PACKAGE_SELECTED = "package_selected"
            const val PART = "part"
            const val SERVICE_TYPE = "service_type"
            const val INVENTORY = "inventory"


        }
    }

}


