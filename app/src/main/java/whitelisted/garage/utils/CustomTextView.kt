package whitelisted.garage.utils

import android.content.Context
import android.graphics.LinearGradient
import android.graphics.Paint
import android.graphics.Shader
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.withStyledAttributes
import whitelisted.garage.R


class CustomTextView : AppCompatTextView, CustomViewInterface {

    override var borderWidth = 1f
    override val view: View get() = this

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setUpAttributes(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) { setUpAttributes(attrs) }

    private fun setUpAttributes(attrs: AttributeSet) {
        setBgDrawable(attrs)
        context.withStyledAttributes(attrs, R.styleable.CustomTextView) {
            val startColor = getColor(R.styleable.CustomTextView_textStartColor, 0)
            val endColor = getColor(R.styleable.CustomTextView_textEndColor, 0)
            val strikeThrough = getBoolean(R.styleable.CustomTextView_strikethrough, false)

            if(startColor != 0 && endColor != 0) {
                setTextColor(startColor)
                paint.shader = LinearGradient(0f, 0f,
                    paint.measureText(text.toString()),
                    textSize,
                    intArrayOf(startColor, endColor),
                    floatArrayOf(0f,1f),
                    Shader.TileMode.CLAMP
                )
            }

            if(strikeThrough) paint.flags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            invalidate()
        }
    }

}