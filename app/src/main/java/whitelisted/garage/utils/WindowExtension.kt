package whitelisted.garage.utils

import android.view.Window

typealias SelectedFilters = MutableMap<String,List<String>>

fun Window.getSoftInputMode(): Int {
    return attributes.softInputMode
}