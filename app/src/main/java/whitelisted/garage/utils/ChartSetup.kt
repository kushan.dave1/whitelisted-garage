package whitelisted.garage.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Path
import android.graphics.RectF
import android.icu.text.DecimalFormat
import android.util.Log
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.github.mikephil.charting.animation.ChartAnimator
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.renderer.BarChartRenderer
import com.github.mikephil.charting.utils.*
import whitelisted.garage.R
import kotlin.collections.ArrayList

class ChartSetup(val currencySymbol: String) {

    fun setUpPieChart(chart: PieChart, list: List<Int>) = with(chart) {

        String
        setUsePercentValues(false) ;description.isEnabled = false
        setExtraOffsets(5f, 10f, 5f, 5f)
        dragDecelerationFrictionCoef = 0.95f; holeRadius = 58f
        transparentCircleRadius = 0f; setDrawCenterText(false)
        rotationAngle = 0f; isRotationEnabled = false; setTouchEnabled(false)
        isHighlightPerTapEnabled = false; setDrawRoundedSlices(true)
        animateY(1000, Easing.EaseInOutQuad)
        legend.isEnabled = false; setDrawEntryLabels(false)

        val entries: ArrayList<PieEntry> = ArrayList()

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        list.forEach {
            entries.add(PieEntry(it.toFloat(), "", null))
        }
        val dataSet = PieDataSet(entries, "")
        dataSet.setDrawIcons(false)

        val color1 = Color.rgb(78, 203, 113) // green
        val color2 = Color.rgb(133, 182, 255) // light blue
        val color3 = Color.rgb(46, 57, 157) // dark blue
        val color4 = Color.rgb(255, 154, 98) // orange

        dataSet.colors = mutableListOf(color1, color2, color3, color4)

        //dataSet.setSelectionShift(0f);
        val data = PieData(dataSet)
        data.isHighlightEnabled = false
        data.setDrawValues(false)
        chart.data = data

        chart.highlightValues(null)
        chart.invalidate()

    }

    fun setUpBarChart(chart: BarChart, counts: List<Float>, months: List<String>) = with(chart) {

        setDrawBarShadow(false); isDoubleTapToZoomEnabled = false
        description.isEnabled = false; axisRight.isEnabled = false
        legend.isEnabled = false; setPinchZoom(false)
        setScaleEnabled(false); setDrawGridBackground(false)

        xAxis.apply {
            position = XAxis.XAxisPosition.BOTTOM
            typeface = ResourcesCompat.getFont(chart.context, R.font.gilroy_medium)
            setDrawGridLines(false)
            granularity = 1f // only intervals of 1 Month
            labelCount = counts.size
            valueFormatter = MonthAxisValueFormatter(months)
        }


        axisLeft.apply {
            val range = counts.maxOrNull() ?: 0f
            typeface = ResourcesCompat.getFont(chart.context, R.font.gilroy_medium)
            setDrawGridLines(false)
            setLabelCount(range.toInt(), false)
            valueFormatter = MyAxisValueFormatter(range)
            setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
            spaceTop = 15f
            axisMinimum = 0f // this replaces setStartAtZero(true)
        }

        val marker = XYMarkerView(context)
        marker.chartView = this
        setMarker(marker)

        val renderer = CustomBarChartRender(this, animator, viewPortHandler)
        renderer.setRadius(15)
        setRenderer(renderer)

        val values: ArrayList<BarEntry> = ArrayList()
        var i = 0

        counts.forEach {
            values.add(BarEntry(i.toFloat(), it))
            i++
        }

        val set1 = BarDataSet(values, "")
        set1.setDrawValues(false)
        set1.highLightColor =  ContextCompat.getColor(chart.context,R.color.colorAccent) //Color.rgb(0, 87, 156)
        set1.color = Color.rgb(240, 245, 250)
        val dataSets: ArrayList<IBarDataSet> = ArrayList()
        dataSets.add(set1)

        val data1 = BarData(dataSets)
        data1.setValueTextSize(10f)
        data1.setValueTypeface(ResourcesCompat.getFont(chart.context, R.font.gilroy_medium))
        data1.barWidth = 0.5f
        this.data = data1
        setVisibleXRangeMaximum(5f)
        invalidate()

        /*if (data != null && data.dataSetCount > 0) {
            set1 = xdata.getDataSetByIndex(0) as BarDataSet
            set1.values = values
            set1.setDrawValues(false)

            data.notifyDataChanged()
            notifyDataSetChanged()
        } else {
            set1 = BarDataSet(values, "")
            set1.setDrawValues(false)
            set1.highLightColor = Color.rgb(0, 87, 156)
            set1.color = Color.rgb(240, 245, 250)
            val dataSets: ArrayList<IBarDataSet> = ArrayList()
            dataSets.add(set1)

            val data1 = BarData(dataSets)
            data1.setValueTextSize(10f)
            //data.setValueTypeface(tfLight)
            data1.barWidth = 0.9f
            this.data = data1
            invalidate()
        }*/
    }

    fun setupLineChart(chart: LineChart, amounts: List<Float>, months: List<String>) = with(chart) {

        setTouchEnabled(true)
        setScaleEnabled(false); setPinchZoom(false)

        val values: ArrayList<Entry> = ArrayList()
        var i = 0
        amounts.forEach {
            values.add(Entry(i.toFloat(), it))
            i++
        }

        val set1 = LineDataSet(values, "DataSet 1")

        set1.apply {
            lineWidth = 1.75f
            circleRadius = 5f
            circleHoleRadius = 2.5f
            color = CommonUtils.getColor(context.resources, R.color.colorAccent)
            setCircleColor(set1.color)
            setGradientColor(CommonUtils.getColor(context.resources, R.color.colorAccent),
                CommonUtils.getColor(context.resources, R.color.white))
            valueTypeface = ResourcesCompat.getFont(chart.context, R.font.gilroy_medium)
            setDrawFilled(true)
            setDrawValues(false)
            isHighlightEnabled = true
        }

        // no description text
        description.isEnabled = false

        setDrawGridBackground(false)

        legend.isEnabled = false

        axisRight.isEnabled = false

        xAxis.apply {
            isEnabled = true
            typeface = ResourcesCompat.getFont(chart.context, R.font.gilroy_medium)
            position = XAxis.XAxisPosition.BOTTOM
            setDrawGridLines(false)
            granularity = 1f
            valueFormatter = MonthAxisValueFormatter(months)
            labelCount = months.size

        }

        axisLeft.apply {
            val range = amounts.maxOrNull() ?: 0f
            isEnabled = true
            typeface = ResourcesCompat.getFont(chart.context, R.font.gilroy_medium)
            valueFormatter = AmountAxisValueFormatter(range)
            labelCount = range.toInt()
            axisMinimum = 0f
            setDrawGridLines(false)

            isGranularityEnabled = true
            granularity = if (range > 99999) 100000f else if (range > 999) 1000f else 100f

        }

        val marker = XYLineMarkerView(context)
        marker.chartView = this
        setMarker(marker)


        // animate calls invalidate()...
        animateX(2500)

        data = LineData(set1)
        invalidate()
        setVisibleXRangeMaximum(5f)
        invalidate()
    }


    inner class CustomBarChartRender(chart: BarDataProvider?,
        animator: ChartAnimator?,
        viewPortHandler: ViewPortHandler?) : BarChartRenderer(chart, animator, viewPortHandler) {

        private var mRadius = 0
        fun setRadius(mRadius: Int) {
            this.mRadius = mRadius
        }

        private var barPath: Path? = null

        override fun drawDataSet(c: Canvas, dataSet: IBarDataSet, index: Int) {
            val trans: Transformer = mChart.getTransformer(dataSet.axisDependency)
            mBarBorderPaint.color = dataSet.barBorderColor
            mBarBorderPaint.strokeWidth = Utils.convertDpToPixel(dataSet.barBorderWidth)
            mShadowPaint.color = dataSet.barShadowColor
            val drawBorder = dataSet.barBorderWidth > 0f
            val phaseX = mAnimator.phaseX
            val phaseY = mAnimator.phaseY

            // initialize the buffer
            val buffer = mBarBuffers[index]
            buffer.setPhases(phaseX, phaseY)
            buffer.setDataSet(index)
            buffer.setInverted(mChart.isInverted(dataSet.axisDependency))
            buffer.setBarWidth(mChart.barData.barWidth)
            buffer.feed(dataSet)
            trans.pointValuesToPixel(buffer.buffer)
            val isSingleColor = dataSet.colors.size == 1
            if (isSingleColor) {
                mRenderPaint.color = dataSet.color
            }
            var j = 0
            while (j < buffer.size()) {
                if (!mViewPortHandler.isInBoundsLeft(buffer.buffer[j + 2])) {
                    j += 4
                    continue
                }
                if (!mViewPortHandler.isInBoundsRight(buffer.buffer[j])) break
                if (!isSingleColor) {
                    mRenderPaint.color = dataSet.getColor(j / 4)
                }
                barPath = roundRect(RectF(buffer.buffer[j],
                    buffer.buffer[j + 1] - 2,
                    buffer.buffer[j + 2],
                    buffer.buffer[j + 3] - 2), mRadius.toFloat(), mRadius.toFloat())
                c.drawPath(barPath!!, mRenderPaint)
                j += 4
            }
        }

        override fun drawHighlighted(c: Canvas, indices: Array<out Highlight>) {
            val barData = mChart.barData
            for (high in indices) {
                val set = barData.getDataSetByIndex(high.dataSetIndex)

                if (set == null || !set.isHighlightEnabled) continue

                val e = set.getEntryForXValue(high.x, high.y)

                if (!isInBoundsX(e, set)) continue

                val trans = mChart.getTransformer(set.axisDependency)

                mHighlightPaint.color = set.highLightColor
                mHighlightPaint.alpha = set.highLightAlpha

                val isStack = high.stackIndex >= 0 && e.isStacked

                val y1: Float
                val y2: Float

                if (isStack) {
                    if (mChart.isHighlightFullBarEnabled) {
                        y1 = e.positiveSum
                        y2 = -e.negativeSum
                    } else {
                        val range = e.ranges[high.stackIndex]
                        y1 = range.from
                        y2 = range.to
                    }
                } else {
                    y1 = e.y
                    y2 = 0f
                }

                prepareBarHighlight(e.x, y1, y2, barData.barWidth / 2f, trans)

                val path = roundRect(mBarRect, mRadius.toFloat(), mRadius.toFloat())
                mBarRect.top = mBarRect.top - 5

                setHighlightDrawPos(high, mBarRect)

                c.drawPath(path, mHighlightPaint) //c.drawRect(mBarRect, mHighlightPaint)

            }
        }

        private fun roundRect(rect: RectF, rx: Float, ry: Float): Path {
            var rx = rx
            var ry = ry
            val top = rect.top
            val left = rect.left
            val right = rect.right
            val bottom = rect.bottom
            val path = Path()
            if (rx < 0) rx = 0f
            if (ry < 0) ry = 0f
            val width = right - left
            val height = bottom - top
            if (rx > width / 2) rx = width / 2
            if (ry > height / 2) ry = height / 2
            val widthMinusCorners = width - 2 * rx
            val heightMinusCorners = height - 2 * ry
            path.moveTo(right, top + ry)
            path.rQuadTo(0f, -ry, -rx, -ry) //top-right corner

            path.rLineTo(-widthMinusCorners, 0f)

            path.rQuadTo(-rx, 0f, -rx, ry) //top-left corner

            path.rLineTo(0f, heightMinusCorners)

            path.rQuadTo(0f, ry, rx, ry) //bottom-left corner

            path.rLineTo(widthMinusCorners, 0f)
            path.rQuadTo(rx, 0f, rx, -ry) //bottom-right corner

            path.rLineTo(0f, -heightMinusCorners)
            path.close() //Given close, last lineto can be removed.
            return path
        }
    }


    inner class MonthAxisValueFormatter(private val mMonths: List<String>) : ValueFormatter() {

        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            return try {
                mMonths[value.toInt()]
            } catch (e: ArrayIndexOutOfBoundsException) {
                ""
            }
        }
    }

    inner class MyAxisValueFormatter(private val range: Float) : ValueFormatter() {
        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            return when {
                value == range || range < 20f -> value.toString()
                value % 20 == 0f -> value.toInt().toString()
                else -> ""
            }
        }
    }

    inner class XYMarkerView(context: Context) : MarkerView(context, R.layout.custom_marker_view) {
        private val tvContent: TextView = findViewById(R.id.tvContent)

        override fun refreshContent(e: Entry, highlight: Highlight) {
            tvContent.text = e.y.toInt().toString()
            super.refreshContent(e, highlight)
        }

        override fun getOffset(): MPPointF {
            return MPPointF((-(width / 2)).toFloat(), (-height - 5).toFloat())
        }

    }

    inner class XYLineMarkerView(context: Context) :
        MarkerView(context, R.layout.custom_marker_view) {
        private val tvContent: TextView = findViewById(R.id.tvContent)

        private val format = DecimalFormat("##,##,##0")

        override fun refreshContent(e: Entry, highlight: Highlight) {
            "$currencySymbol ${format.format(e.y.toInt())}".apply {
                tvContent.text = this
            }
            super.refreshContent(e, highlight)
        }

        override fun getOffset(): MPPointF {
            return MPPointF((-(width / 2)).toFloat(), (-height - 5).toFloat())
        }

    }

    inner class AmountAxisValueFormatter(private val range: Float) : ValueFormatter() {

        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            return when {
                range > 99999 -> {
                    val inLacs = (value / 100000).toInt()
                    if (inLacs == 0) "0" else if (inLacs % 10 == 0) "$inLacs lacs" else ""
                }
                range > 999 -> {
                    val inThousands = (value / 1000).toInt()
                    if (inThousands == 0) "0" else if (inThousands % 2 == 0) "$inThousands K" else ""
                }
                else -> if (value.toInt() % 20 == 0) value.toString() else ""
            }

        }
    }

}