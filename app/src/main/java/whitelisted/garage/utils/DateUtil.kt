import whitelisted.garage.utils.AppENUM
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

object DateUtil {

    val endDate: String
        get() {
            val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
            val sdf = SimpleDateFormat(AppENUM.DateUtilKey.FORMAT_IN_SIMPLE, Locale.getDefault())
            return sdf.format(calendar.time) + " 23:59:59"
        }

    val today: String
        get() {
            val calendar = Calendar.getInstance()
            val sdf = SimpleDateFormat(AppENUM.DateUtilKey.FORMAT_IN_SIMPLE, Locale.getDefault())
            return sdf.format(calendar.time) + " 00:00:01"
        }


    val yesterday: String
        get() {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_YEAR, -1)
            val sdf = SimpleDateFormat(AppENUM.DateUtilKey.FORMAT_IN_SIMPLE, Locale.getDefault())
            return sdf.format(calendar.time) + " 00:00:01"
        }


    val lastThreeDays: String
        get() {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_YEAR, -3)
            val sdf = SimpleDateFormat(AppENUM.DateUtilKey.FORMAT_IN_SIMPLE, Locale.getDefault())
            return sdf.format(calendar.time) + " 00:00:01"
        }


    val lastWeek: String
        get() {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_YEAR, -7)
            val sdf = SimpleDateFormat(AppENUM.DateUtilKey.FORMAT_IN_SIMPLE, Locale.getDefault())
            return sdf.format(calendar.time) + " 00:00:01"
        }


    val lastFifteenDays: String
        get() {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_YEAR, -15)
            val sdf = SimpleDateFormat(AppENUM.DateUtilKey.FORMAT_IN_SIMPLE, Locale.getDefault())
            return sdf.format(calendar.time) + " 00:00:01"
        }


    val lastMonth: String
        get() {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_YEAR, -30)
            val sdf = SimpleDateFormat(AppENUM.DateUtilKey.FORMAT_IN_SIMPLE, Locale.getDefault())
            return sdf.format(calendar.time) + " 00:00:01"
        }


    fun getFormattedDateUtcToGmt(inFormat: String,
        outFormat: String,
        dateToBeParsed: String): String {
        val inFormatter = SimpleDateFormat(inFormat, Locale.getDefault()).apply {
            timeZone = TimeZone.getTimeZone("UTC")
        }
        val outFormatter = SimpleDateFormat(outFormat, Locale.getDefault()).apply {
            timeZone = TimeZone.getDefault()
        }

        return try {
            outFormatter.format(inFormatter.parse(dateToBeParsed) ?: Date())
        } catch (e: Exception) {
            ""
        }

    }

    fun getUtcDateFromLocalDate(outFormat: String,
        dateToBeParsed: Date): String { //Assumption is that the dateToBeParsed in in the local timezone i.e. system default timezone
        val outFormatter = SimpleDateFormat(outFormat, Locale.getDefault()).apply {
            timeZone = TimeZone.getTimeZone("UTC")
        }

        return try {
            outFormatter.format(dateToBeParsed)
        } catch (e: Exception) {
            outFormatter.format(Calendar.getInstance(TimeZone.getDefault()).time)
        }

    }

    fun getUtcDateFromLocalDate(dateToBeParsed: Date): String { //Assumption is that the dateToBeParsed in in the local timezone i.e. system default timezone
        val outFormatter =
            SimpleDateFormat(AppENUM.DateUtilKey.FORMAT_WITH_TIME, Locale.getDefault()).apply {
                timeZone = TimeZone.getTimeZone("UTC")
            }
        return try {
            outFormatter.format(dateToBeParsed)
        } catch (e: Exception) {
            outFormatter.format(Calendar.getInstance(TimeZone.getDefault()).time)
        }
    }

    fun getDateFirstFormattedDate(): String? {
        return SimpleDateFormat(AppENUM.DateUtilKey.FORMAT_IN_SIMPLE, Locale.getDefault()).format(
            Date())
    }

    fun getDateFirstFormattedDate(calendar: Calendar): String? {
        return SimpleDateFormat(AppENUM.DateUtilKey.FORMAT_SIMPLE_YEAR_FIRST,
            Locale.getDefault()).format(calendar.timeInMillis)
    }

    fun getCurrentTime(): String? { //date output format
        val dateFormat: DateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        val cal = Calendar.getInstance()
        return dateFormat.format(cal.time)
    }

    fun getCurrentHour(): String? { //date output format
        val dateFormat: DateFormat = SimpleDateFormat("HH", Locale.getDefault())
        val cal = Calendar.getInstance()
        return dateFormat.format(cal.time)
    }

    private fun calculateRange(): Int {
        try {
            val currentDate = LocalDate.now()
            val firstDate = LocalDate.of(2005, 1, 1)
            return if (firstDate != null && currentDate != null) {
                currentDate.year - firstDate.year
            } else {
                0
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return 0
        }
    }

    fun isDateExpired(s: String?): Boolean {
        val split = s?.split("-")
        if (!split.isNullOrEmpty() && split?.size == 3) {
            val currentDate = LocalDate.now()
            val firstDate = LocalDate.of(split[2].toInt(), split[1].toInt(), split[0].toInt())
            return currentDate.isAfter(firstDate)
        }

        return false

    }

    fun giveDateRange(): ArrayList<String> {
        val list = arrayListOf<Int>()
        for (i in 0..calculateRange()) {
            val limitedDate = LocalDate.of(2005, 1, 1)
            val newDate = limitedDate.plusYears(i.toLong())
            list.add(newDate.year)
            list.sortDescending()
        }
        return conventTOStringList(list)
    }

    private fun conventTOStringList(list: ArrayList<Int>): ArrayList<String> {
        val listString = arrayListOf<String>()
        for (value in list) {
            listString.add(value.toString())
        }
        return listString
    }

}
