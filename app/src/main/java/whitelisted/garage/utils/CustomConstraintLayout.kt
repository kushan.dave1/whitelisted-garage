package whitelisted.garage.utils

import android.content.Context
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.withStyledAttributes
import whitelisted.garage.R

class CustomConstraintLayout : ConstraintLayout, CustomViewInterface {

    override var borderWidth: Float = 0f
    override val view: View get() = this
    private var colourPartial : Int = 0
    private var partialWeight : Float? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setupView(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        setupView(attrs)

    }

    private fun setupView(attrs: AttributeSet) {
        context.withStyledAttributes(attrs, R.styleable.CustomView) {
            colourPartial = getColor(R.styleable.CustomView_colorPartial,0)
            partialWeight = getFloat(R.styleable.CustomView_partialWeight,0.5f)
        }
        setBgDrawable(attrs)
    }

   /* override fun onDraw(canvas: Canvas?) {
        if(colourPartial != 0) {
            paint.color = colourPartial
            paint.style = Paint.Style.FILL
            val rect = RectF(0f + borderWidth, 0f + borderWidth, width.toFloat() - borderWidth, height.toFloat() - borderWidth)
            val bottom = height.toFloat() * (partialWeight ?: 0f)

            canvas?.drawRect(0f,0f,width.toFloat(),bottom,paint)
        }
        super.onDraw(canvas)
    }
*/
}