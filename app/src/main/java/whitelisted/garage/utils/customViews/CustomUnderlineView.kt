package whitelisted.garage.utils.customViews

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View

class CustomUnderlineView: View {


    private val path = Path()
    private val paint = Paint()
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context,
        attrs,
        defStyleAttr)


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        path.moveTo(0f,0f)
        path.lineTo(width.toFloat()-10,0f)
        path.arcTo(width.toFloat()-10,0f,width.toFloat(),height.toFloat(),270f,180f,false)
        path.lineTo(0f,3f)
        paint.color = Color.WHITE
        paint.style = Paint.Style.FILL
        canvas?.drawPath(path,paint)

    }

}