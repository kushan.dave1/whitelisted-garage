package whitelisted.garage.utils.customViews

import android.util.AttributeSet
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.*
import androidx.viewbinding.ViewBinding
import whitelisted.garage.utils.CustomViewInterface

@Suppress("UNCHECKED_CAST")
class CustomRecyclerView : RecyclerView, CustomViewInterface {

    override var borderWidth = 0f
    override val view: View get() = this
    private val concatAdapter = ConcatAdapter()
    private var isVertical = true

    init {
        adapter = concatAdapter
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setBgDrawable(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        setBgDrawable(attrs)
    }

    fun <T : ViewBinding, V> addViews(
        list: List<V>, inflater: (LayoutInflater, ViewGroup?, Boolean) -> T,
        binder: (T, model: V, adapterPosition: Int, recyclerPosition: Int) -> Unit
    ) {
        val recyclerCount = concatAdapter.itemCount
        val adapter = object : RecyclerView.Adapter<CustomHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolder {
                val binding = inflater(LayoutInflater.from(context), parent, false)
                return CustomHolder(binding)
            }

            override fun onBindViewHolder(holder: CustomHolder, position: Int) {
                binder(
                    holder.binding as T,
                    list[position], position,
                    recyclerCount + position
                )
            }

            override fun getItemCount() = list.size
        }
        concatAdapter.addAdapter(adapter)
    }

    fun <T : ViewBinding, V : Any> addViews(
        list: List<V>, inflater: (LayoutInflater, ViewGroup?, Boolean) -> T,
        binder: (binding: T, model: V, adapterPosition: Int) -> Unit
    ): Adapter<*> {
        val adapter = object : Adapter<CustomHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolder {
                val binding = inflater(LayoutInflater.from(context), parent, false)
                return CustomHolder(binding)
            }

            override fun onBindViewHolder(holder: CustomHolder, position: Int) {
                binder(holder.binding as T, list[position], position)
            }

            override fun getItemCount() = list.size
        }
        concatAdapter.addAdapter(adapter)

        return adapter
    }

    fun <T : ViewBinding, V : Any> addViewsInfiniteScroll(
        list: List<V>, inflater: (LayoutInflater, ViewGroup?, Boolean) -> T,
        binder: (binding: T, model: V, adapterPosition: Int) -> Unit
    ): Adapter<*> {
        var isAutoScrollEnabled = true
        val adapter = object : Adapter<CustomHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolder {
                val binding = inflater(LayoutInflater.from(context), parent, false)
                return CustomHolder(binding)
            }

            override fun onBindViewHolder(holder: CustomHolder, position: Int) {
                val realPos = position % list.size
                binder(holder.binding as T, list[realPos] as V, realPos)
            }

            override fun getItemCount() = Int.MAX_VALUE
        }
        concatAdapter.addAdapter(adapter)

        scrollToPosition(Int.MAX_VALUE/2)
        addOnScrollListener(object : OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if(newState == 0){
                    smoothScrollBy(5,0)
                }
            }


            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val layoutManager = layoutManager as LinearLayoutManager
                val firstItemVisible: Int = layoutManager.findFirstVisibleItemPosition()
                if (firstItemVisible != 1 && (firstItemVisible % list.size == 1)) {
                    layoutManager.scrollToPosition(1)
                } else if (firstItemVisible != 1 && firstItemVisible > list.size && (firstItemVisible % list.size > 1)) {
                    layoutManager.scrollToPosition(firstItemVisible % list.size)
                } else if (firstItemVisible == 0) {
                    layoutManager.scrollToPositionWithOffset(list.size, -recyclerView.computeHorizontalScrollOffset())
                }
                if(isAutoScrollEnabled) smoothScrollBy(dx + 5, 0)
            }
        })

        setOnTouchListener { v, event ->
            v.performClick()
            when(event.action){
                MotionEvent.ACTION_DOWN -> isAutoScrollEnabled = false
                MotionEvent.ACTION_UP,MotionEvent.ACTION_CANCEL  -> {
                    isAutoScrollEnabled = true
                }
            }
            false
        }


        return adapter
    }

    fun <H : ViewBinding, T : ViewBinding, HM, IM> addViewsWithHeader(
        map: Map<HM, List<IM>>,
        headerInflater: (LayoutInflater, ViewGroup?, Boolean) -> H,
        itemInflater: (LayoutInflater, ViewGroup?, Boolean) -> T,
        headerBinder: ((H, headerModel: HM) -> Unit)? = null,
        itemBinder: ((T, itemModel: IM, headerModel: HM) -> Unit)? = null,
        headerBinder2: ((H, HM, Int) -> Unit)? = null,
        itemBinder2: ((
            binding: T,
            itemModel: IM,
            headerModel: HM,
            adapter: Adapter<*>?,
            position: Int
        ) -> Unit)? = null
    ) {
        val flatList = mutableListOf<FlatItem>()
        map.forEach { (t, u) ->
            flatList.add(FlatItem(t, isHeader = true))
            u.forEach { flatList.add(FlatItem(t, it)) }
        }

        val adapter = object : Adapter<CustomHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolder {
                return if (viewType == 1) {
                    val binding = headerInflater(LayoutInflater.from(context), parent, false)
                    CustomHolder(binding)
                } else {
                    val binding = itemInflater(LayoutInflater.from(context), parent, false)
                    CustomHolder(binding)
                }
            }

            override fun onBindViewHolder(holder: CustomHolder, position: Int) {
                val item = flatList[position]

                holder.binding.root.tag =

                    if (item.isHeader) headerBinder?.invoke(holder.binding as H, item.header as HM)
                    else itemBinder?.invoke(
                        holder.binding as T,
                        item.model as IM,
                        item.header as HM
                    )

                if (item.isHeader) headerBinder2?.invoke(
                    holder.binding as H,
                    item.header as HM,
                    position
                )
                else itemBinder2?.invoke(
                    holder.binding as T,
                    item.model as IM, item.header as HM, adapter, position
                )
            }

            override fun getItemCount() = flatList.size

            override fun getItemViewType(position: Int) = if (flatList[position].isHeader) 1 else 0
        }
        concatAdapter.addAdapter(adapter)
    }


    fun <T : ViewBinding> addSingleView(
        position: Int, inflater: (LayoutInflater, ViewGroup?, Boolean) -> T,
        binder: (T) -> Unit
    ) {

        val adapter = object : Adapter<CustomHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolder {
                val binding = inflater(LayoutInflater.from(context), parent, false)
                return CustomHolder(binding)
            }

            override fun onBindViewHolder(holder: CustomHolder, position: Int) {
                binder(holder.binding as T)
            }

            override fun getItemCount() = 1
        }
        concatAdapter.addAdapter(position, adapter)
    }

    fun <T : ViewBinding> addSingleView(
        inflater: (LayoutInflater, ViewGroup?, Boolean) -> T,
        binder: (T) -> Unit
    ) {

        val adapter = object : Adapter<CustomHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolder {
                val binding = inflater(LayoutInflater.from(context), parent, false)
                return CustomHolder(binding)
            }

            override fun onBindViewHolder(holder: CustomHolder, position: Int) {
                binder(holder.binding as T)
            }

            override fun getItemCount() = 1
        }
        concatAdapter.addAdapter(adapter)
    }

    fun addSingleView(binder: (ViewGroup) -> View) {
        val adapter = object : Adapter<ViewHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
                object : ViewHolder(binder(parent)) {}

            override fun onBindViewHolder(holder: ViewHolder, position: Int) {}
            override fun getItemCount() = 1
        }
        concatAdapter.addAdapter(adapter)
    }

    fun <T : ViewDataBinding> addViews(
        itemCount: Int = 0,
        layoutRes: Int,
        binder: (T, Int, Int) -> Unit
    ) {
        val adapter = object : Adapter<CustomHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolder {
                val binding = DataBindingUtil.inflate<T>(
                    LayoutInflater.from(context),
                    layoutRes,
                    parent,
                    false
                )
                return CustomHolder(binding)
            }

            override fun onBindViewHolder(holder: CustomHolder, position: Int) {
                binder(holder.binding as T, position, concatAdapter.itemCount + position)
            }

            override fun getItemCount() = itemCount
        }
        concatAdapter.addAdapter(adapter)
    }

    inner class CustomHolder(val binding: ViewBinding) : ViewHolder(binding.root)

    inner class FlatItem(val header: Any?, val model: Any? = null, val isHeader: Boolean = false)

    fun setLayoutManagerAsLinear() {
        layoutManager = LinearLayoutManager(context)
        isVertical = true
    }

    fun setLayoutManagerAsLinearHorizontal() {
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        isVertical = false
    }

    fun setLayoutManagerAsGrid(spanCount: Int) {
        layoutManager = GridLayoutManager(context, spanCount)
        isVertical = true
    }


    fun setLayoutManagerAsGridHorizontal(spanCount: Int) {
        layoutManager = GridLayoutManager(context, spanCount, GridLayoutManager.HORIZONTAL, false)
        isVertical = false
    }

    fun setLayoutManagerAsStaggered(spanCount: Int) {
        layoutManager = StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL)
        isVertical = true
    }

    fun addAdapter(adapter: Adapter<*>) {
        concatAdapter.addAdapter(adapter)
    }

    fun clear(): CustomRecyclerView {
        concatAdapter.adapters.forEach { concatAdapter.removeAdapter(it) }
        return this
    }

    fun onScrolledToEnd(listener: () -> Unit) {
        addOnScrollListener(object : OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val offset =
                    if (isVertical) computeVerticalScrollOffset() else computeHorizontalScrollOffset()
                val extent =
                    if (isVertical) computeVerticalScrollExtent() else computeHorizontalScrollExtent()
                val range =
                    if (isVertical) computeVerticalScrollRange() else computeHorizontalScrollRange()
                if (offset + extent == range) listener()
            }
        })
    }


    fun notifyAdapterChangedAtPosition(adapterPosition: Int, adapter: Adapter<*>) {
        concatAdapter.removeAdapter(concatAdapter.adapters[adapterPosition])
        concatAdapter.addAdapter(adapterPosition, adapter)
    }

    fun notifyItemRemovedAtPosition(concatAdapterPosition: Int) {
        concatAdapter.notifyItemRemoved(concatAdapterPosition)
    }

    fun notifyAt(position: Int) {
        concatAdapter.notifyItemChanged(position)
    }

}