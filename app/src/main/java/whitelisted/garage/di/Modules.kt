package whitelisted.garage.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import whitelisted.garage.BuildConfig
import whitelisted.garage.api.API
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.APIRepositoryImpl
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.database.GoMechDB
import whitelisted.garage.database.RoomModule
import whitelisted.garage.database.repository.RoomRepository
import whitelisted.garage.network.NetworkBuilder
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.AppStorePreferences
import whitelisted.garage.viewmodels.MyCustomerViewModel
import whitelisted.garage.viewmodels.*

object Modules {

    private val applicationModules = module {
        single {
            get<Application>().run {
                AppStorePreferences(AppENUM.RefactoredStrings.APP_NAME, Context.MODE_PRIVATE)
            }
        }

        single {
            Room.databaseBuilder(get(), GoMechDB::class.java, BuildConfig.DEFAULT_ROOM)
                .allowMainThreadQueries().fallbackToDestructiveMigration().build()
        }
    }

    private val viewModelModules = module {
        viewModel { BaseViewModel(get(), get()) }
        viewModel { LoginViewModel(get(), get(), get()) }
        viewModel { CommonSharedViewModel(get(), get(), get()) }
        viewModel { SelectWorkshopDialogViewModel(get(), get(), get()) }
        viewModel { NewOrderFragmentViewModel(get(), get()) }
        viewModel { InventoryFragmentViewModel(get(), get()) }
        viewModel { JobCardFragmentViewModel(get(), get()) }
        viewModel { OrderDetailWrapperViewModel(get(), get()) }
        viewModel { HomeFragmentViewModel(get(), get()) }
        viewModel { OrderEstimatesViewModel(get(), get()) }
        viewModel { PaymentViewModel(get(), get()) }
        viewModel { ManageEmployeeFragmentViewModel(get(), get(), get()) }
        viewModel { ManagePackagesFragmentViewModel(get(), get()) }
        viewModel { AddEditEmployeeViewModel(get(), get()) }
        viewModel { AddWorkshopViewModel(get(), get()) }
        viewModel { RemindersViewModel(get(), get()) }
        viewModel { WorkshopListViewModel(get(), get(), get()) }
        viewModel { ViewEditWorkshopViewModel(get(), get()) }
        viewModel { CreateEditPackageViewModel(get(), get()) }
        viewModel { CreateEditAccessoriesPackageViewModel(get(), get()) }
        viewModel { ManagePackagesViewModel(get(), get()) }
        viewModel { LedgerFragmentViewModel(get(), get()) }
        viewModel { GiveOrTakeFragmentViewModel(get(), get()) }
        viewModel { MyCustomerViewModel(get(), get()) }
        viewModel { CreatePackagesFragmentViewModel(get(), get()) }
        viewModel { AccountFragmentViewModel(get(), get(), get()) }
        viewModel { BusinessCardViewModel(get(), get()) }
        viewModel { QRCodeFragmentViewModel(get(), get()) }
        viewModel { OrderListViewModel(get(), get()) }
        viewModel { BillingFragmentViewModel(get(), get()) }
        viewModel { SearchOrdersFragmentViewModel(get(), get()) }
        viewModel { SparesFilterViewModel(get(), get()) }
        viewModel { SettingsFragmentViewModel(get(), get()) }
        viewModel { OrderHistoryFragmentViewModel(get(), get()) }
        viewModel { OrderSummaryFragmentViewModel(get(), get()) }
        viewModel { AddressViewModel(get(), get()) }
        viewModel { VerifyOtpFragmentViewModel(get(), get()) }
        viewModel { FeedbackFragmentViewModel(get(), get()) }
        viewModel { GoCoinsFragmentViewModel(get(), get()) }
        viewModel { MyBusinessViewModel(get(), get(), get()) }
        viewModel { UnlockGMBFragmentViewModel(get(), get(), get()) }
        viewModel { SelectCarDialogViewModel(get(), get()) }
        viewModel { GoCoinTransactionsViewModel(get(), get()) }
        viewModel { InventoryTabFragmentViewModel(get(), get()) }
        viewModel { AddRackItemFragmentViewModel(get(), get()) }
        viewModel { AddRackFragmentViewModel(get(), get()) }
        viewModel { RetailNewOrderDetailFragmentViewModel(get(), get()) }
        viewModel { SelectRackItemDialogViewModel(get(), get()) }
        viewModel { ChooseImageFragmentViewModel(get(), get()) }
        viewModel { RetailOrderAddPartViewModel(get(), get()) }
        viewModel { OrderInclusionsViewModel(get(), get()) }
        viewModel { CompleteRegistrationViewModel(get(), get()) }
        viewModel { OrderEstimateEditViewModel(get(), get()) }
        viewModel { ReferAndEarnFragmentViewModel(get(), get()) }
        viewModel { ProfileFragmentViewModel(get(), get()) }
        viewModel { WebsiteInfoViewModel(get(), get(), get()) }
        viewModel { LanguageBottomSheetViewModel(get(), get(), get()) }
        viewModel { ApplyCouponViewModel(get(), get()) }
        viewModel { PaymentsSharedViewModel(get(), get()) }
        viewModel { InstantReminderViewModel(get(), get(), get()) }
        viewModel { WorkShopBiddingViewModel(get(), get()) }
        viewModel { EnquiryOrderRequestsFragmentVIewModel(get(), get()) }
        viewModel { DashboardSharedViewModel(get(), get(), get()) }
        viewModel { PlaceBidFragmentViewModel(get(), get()) }
        viewModel { SparesShopFragmentViewModel(get(), get(), get()) }
        viewModel { SelectAddressViewModel(get(), get()) }
        viewModel { AddressSharedViewModel(get(), get()) }
        viewModel { BottomNavViewModel(get(), get()) }
        viewModel { WalkthroughSharedViewModel(get(), get()) }
    }

    private val daoModules = module {
        single { RoomModule(get()).getRoomDao() }
    }

    private val networkModules = module {
        single { NetworkBuilder.create(API::class.java, get()) }
    }

    private val repoModules = module {
        single<APIRepository> { APIRepositoryImpl(get(), get()) }
        single { RoomRepository(get(), get()) }
    }

    fun getAll() =
        listOf(applicationModules, viewModelModules, networkModules, repoModules, daoModules)

}