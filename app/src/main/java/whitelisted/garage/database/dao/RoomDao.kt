package whitelisted.garage.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import whitelisted.garage.api.response.SSItemModel
import whitelisted.garage.api.response.WorkshopListResponseModel

@Dao
interface RoomDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWorkshopList(carTypeResponseList: List<WorkshopListResponseModel>?)

    @Query("SELECT * FROM workshops")
    fun getWorkshopList(): List<WorkshopListResponseModel>

    @Query("DELETE FROM workshops")
    fun deleteWorkshopList()


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRecentItems(item: List<SSItemModel>)

    @Query("SELECT * FROM ss_item")
    suspend fun getRecentItems(): List<SSItemModel>

    @Query("DELETE FROM ss_item")
    fun deleteRecentItems()

    //    @Insert(onConflict = OnConflictStrategy.REPLACE)
    //    fun insertInventoryCheckList(list: InventoryCheckListResponse)
    //
    //    @Query("SELECT * FROM inventoryCheckList")
    //    fun getInventoryCheckList(): InventoryCheckListResponse
    //
    //    @Query("DELETE FROM inventoryCheckList")
    //    fun deleteInventoryCheckList()

}