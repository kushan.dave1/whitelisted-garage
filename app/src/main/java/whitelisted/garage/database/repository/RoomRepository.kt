package whitelisted.garage.database.repository

import whitelisted.garage.api.response.SSItemModel
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.database.GoMechDB
import whitelisted.garage.database.dao.RoomDao

class RoomRepository(private val roomDao: RoomDao, var db: GoMechDB) {

    fun insertWorkshopList(data: List<WorkshopListResponseModel>?) {
        roomDao.insertWorkshopList(data)
    }

    fun getWorkshopList(): List<WorkshopListResponseModel> {
        return roomDao.getWorkshopList()
    }

    fun deleteWorkshopList() {
        roomDao.deleteWorkshopList()
    }

    suspend fun insertRecentItems(data: List<SSItemModel>) {
        roomDao.insertRecentItems(data)
    }

    suspend fun getRecentItems(): List<SSItemModel> {
        return roomDao.getRecentItems()
    }

     fun deleteRecentItems() {
        roomDao.deleteRecentItems()
    }

    //    fun deleteInventoryList() {
    //        roomDao.deleteInventoryCheckList()
    //    }
    //
    //    fun insertInventoryCheckList(listModel: InventoryCheckListResponse) {
    //        roomDao.insertInventoryCheckList(listModel)
    //    }


}