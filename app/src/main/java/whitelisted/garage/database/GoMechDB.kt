package whitelisted.garage.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import whitelisted.garage.BuildConfig
import whitelisted.garage.api.response.CarVariant
import whitelisted.garage.api.response.SSItemModel
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.database.dao.RoomDao
import whitelisted.garage.database.typeConverters.BundleItemsConverter
import whitelisted.garage.database.typeConverters.WorkshopListEmployeeIdsTypeConverter

@Database(entities = [CarVariant::class, WorkshopListResponseModel::class, SSItemModel::class],
    version = 10,
    exportSchema = false)
@TypeConverters(WorkshopListEmployeeIdsTypeConverter::class, BundleItemsConverter::class)
abstract class GoMechDB : RoomDatabase() {
    abstract val roomDao: RoomDao

    companion object {
        private var INSTANCE: GoMechDB? = null
        fun getInstance(context: Context): GoMechDB {
            if (INSTANCE == null) {
                INSTANCE =
                    Room.databaseBuilder(context, GoMechDB::class.java, BuildConfig.DEFAULT_ROOM)
                        .allowMainThreadQueries().fallbackToDestructiveMigration().build()
            }
            return INSTANCE as GoMechDB
        }

    }

}