package whitelisted.garage.database.typeConverters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class WorkshopTypesTypeConverter {
    @TypeConverter
    fun stringToWorkshopList(value: String?): List<String>? {
        val listType: Type = object : TypeToken<List<String>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun workshopListToString(list: List<String>?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }
}