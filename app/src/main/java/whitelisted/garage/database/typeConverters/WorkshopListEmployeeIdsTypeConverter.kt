package whitelisted.garage.database.typeConverters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class WorkshopListEmployeeIdsTypeConverter {
    @TypeConverter
    fun stringToWorkshopList(value: String?): List<Long>? {
        val listType: Type = object : TypeToken<List<Long>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun workshopListToString(list: List<Long>?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }
}