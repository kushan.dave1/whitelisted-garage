package whitelisted.garage.database.typeConverters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import whitelisted.garage.api.response.SSItemModel
import java.lang.reflect.Type

class BundleItemsConverter{
    @TypeConverter
    fun stringToBundleList(value: String?): List<SSItemModel>? {
        val listType: Type = object : TypeToken<List<SSItemModel>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun bundleListToString(list: List<SSItemModel>?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }
}