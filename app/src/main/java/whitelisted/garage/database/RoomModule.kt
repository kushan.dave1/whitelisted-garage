package whitelisted.garage.database

import whitelisted.garage.database.dao.RoomDao

class RoomModule(private val appDatabase: GoMechDB) {

    fun getRoomDao(): RoomDao {
        return appDatabase.roomDao
    }
}