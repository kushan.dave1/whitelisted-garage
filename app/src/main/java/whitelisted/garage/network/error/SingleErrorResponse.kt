package whitelisted.garage.network.error

import com.google.gson.annotations.SerializedName

data class SingleErrorResponse(@SerializedName("error") val error: String)