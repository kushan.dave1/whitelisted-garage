package whitelisted.garage.network.error

import com.google.gson.annotations.SerializedName

data class DefaultErrorResponse(
    @SerializedName("type") val type: String?,
    @SerializedName("status") val status: Boolean?,
    @SerializedName("message") val message: String,
    @SerializedName("detail") val detail: String,
    @SerializedName("data") val data: Map<String,String>
    )