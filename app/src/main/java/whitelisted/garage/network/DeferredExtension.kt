@file:Suppress("UNCHECKED_CAST", "BlockingMethodInNonBlockingContext")

package whitelisted.garage.network

import android.util.Log
import com.google.gson.Gson
import com.squareup.moshi.Moshi
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Deferred
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject
import retrofit2.Response
import whitelisted.garage.eventBus.ProLockedEvent
import whitelisted.garage.eventBus.SessionExpiredEvent
import whitelisted.garage.network.error.DefaultErrorResponse
import whitelisted.garage.network.error.DynamicErrorResponse
import whitelisted.garage.network.error.JsonApiErrorResponse
import whitelisted.garage.network.error.SingleErrorResponse
import java.net.HttpURLConnection
import java.net.ProtocolException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

suspend inline fun <T> Deferred<Response<T>>.awaitAndGet(): Result<T> {
    return try {
        val response = await()
        if (response.isSuccessful) {
            if (response.body() is ServerResponse<*>) {
                val result = response.body() as ServerResponse<*>
                if (result.status) {
                    Result.Success(response.body(), response.code()) as Result<T>
                } else {
                    Result.Failure(result.message, 2)
                }
            } else {
                Result.Success(response.body(), response.code()) as Result<T>
            }
        } else if (response.code() == 401) {
            val errorBody = response.errorBody()?.source()?.readUtf8()
            val gson = Gson()
            val errorResponse = try {
                gson.fromJson(errorBody, DefaultErrorResponse::class.java)
            } catch (e: Exception) {
                null
            }

            EventBus.getDefault().post(SessionExpiredEvent())
            Result.Failure(errorResponse?.detail ?: "", response.code())

        } else if (response.code() == 403) {

            //            EventBus.getDefault().post(SessionExpiredEvent())

            val errorBody = response.errorBody()?.source()?.readUtf8()
            val gson = Gson()
            val errorResponse = try {
                gson.fromJson(errorBody, DefaultErrorResponse::class.java)
            } catch (e: Exception) {
                null
            }
            Result.Failure(errorResponse?.detail ?: "", response.code())
        } else if (response.code() == 423) {
            val errorBody = response.errorBody()?.source()?.readUtf8()
            val gson = Gson()
            val errorResponse = try {
                gson.fromJson(errorBody, DefaultErrorResponse::class.java)
            } catch (e: Exception) {
                null
            }
            EventBus.getDefault().post(ProLockedEvent(errorResponse?.message ?: ""))
            Result.Failure(errorResponse?.detail ?: "", response.code())
        } else {
            val errorBody = response.errorBody()?.source()?.readUtf8()
            val moshi = Moshi.Builder().build()
            val jsonAdapter = moshi.adapter(DefaultErrorResponse::class.java)
            val errorResponse = try {
                jsonAdapter.fromJson(errorBody ?: "")?.message
            } catch (e: Exception) {
                null
            }
            val jsonDynamicAdapter = moshi.adapter(DynamicErrorResponse::class.java)
            val jsonDynamicErrorResponse = try {
                jsonDynamicAdapter.fromJson(errorBody ?: "")!!.errors.values.flatten()
                    .joinToString(", ")
            } catch (e: Exception) {
                null
            }
            val jsonSingleAdapter = moshi.adapter(SingleErrorResponse::class.java)
            val jsonSingleErrorResponse = try {
                jsonSingleAdapter.fromJson(errorBody ?: "")!!.error
            } catch (e: Exception) {
                null
            }
            val jsonApiAdapter = moshi.adapter(JsonApiErrorResponse::class.java)
            val jsonApiErrorResponse = try {
                jsonApiAdapter.fromJson(errorBody ?: "")?.errorList?.get(0)?.message
            } catch (e: Exception) {
                null
            }

            val jsonObjectResponse = try {
                JSONObject(errorBody ?: "").getString("message")
            } catch (e: Exception) {
                null
            }

            Result.Failure(try {
                errorResponse ?: jsonDynamicErrorResponse ?: jsonSingleErrorResponse
                ?: jsonApiErrorResponse ?: jsonObjectResponse ?: "Something went wrong"
            } catch (e: SocketTimeoutException) {
                "Timeout"
            }, response.code())
        }
    } catch (e: UnknownHostException) {
        Result.Failure("Failed to connect to server", 101)
    } catch (e: SocketTimeoutException) {
        Result.Failure("Timeout", HttpURLConnection.HTTP_INTERNAL_ERROR)
    } catch (e: CancellationException) { //This case is hit in the case of jobs getting cancelled
        //In this case we shall not be sending any message and therefore the search will not display this message
        Result.Failure("", HttpURLConnection.HTTP_NO_CONTENT)
    } catch (e: Exception) {

        if (e is ProtocolException) {
            Result.Failure("No data found", HttpURLConnection.HTTP_NO_CONTENT)
        } else {
            Result.Failure(e.toString(), HttpURLConnection.HTTP_INTERNAL_ERROR)
        }
    }
}
