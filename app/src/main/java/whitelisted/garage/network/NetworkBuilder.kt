package whitelisted.garage.network

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.localebro.okhttpprofiler.OkHttpProfilerInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import whitelisted.garage.App
import whitelisted.garage.BuildConfig
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.AppStorePreferences
import java.util.concurrent.TimeUnit

object NetworkBuilder {
    fun <T> create(apiType: Class<T>, sharedPreferences: AppStorePreferences) =
        Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).client(gmHttpClient(sharedPreferences))
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
            .addConverterFactory(GsonConverterFactory.create()).build().create(apiType)

    private fun gmHttpClient(sharedPreferences: AppStorePreferences): OkHttpClient {
        val builder = OkHttpClient.Builder().connectTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES).readTimeout(1, TimeUnit.MINUTES)
            .retryOnConnectionFailure(true).addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }).addNetworkInterceptor(StethoInterceptor())
        val auth = sharedPreferences.getString(App.applicationContext(), AppENUM.ACCESS_TOKEN_,"") //
        builder.addInterceptor { chain ->
            var request = chain.request()
            val tempNewUrl =
                request.url.newBuilder() //                .addQueryParameter(AppENUM.RefactoredStrings.APP_VERSION, BuildConfig.VERSION_NAME)

            if (BuildConfig.DEBUG) {
                tempNewUrl.addQueryParameter(AppENUM.RefactoredStrings.IS_DEBUG, "true")
            }
            val newUrl = tempNewUrl.build()
            val requestBuilder = request.newBuilder().url(newUrl)

            request = requestBuilder.build()
            chain.proceed(request)
        }


        if (BuildConfig.DEBUG) {
            builder.addInterceptor(OkHttpProfilerInterceptor())
        }



        if (auth.isNotEmpty()) {
            builder.addInterceptor { chain ->
                var request = chain.request()
                val newUrl =
                    request.url.newBuilder() //.addQueryParameter(AppENUM.RefactoredStrings.APP_VERSION,BuildConfig.VERSION_NAME)
                        .build()
                val wid = sharedPreferences.getString(App.applicationContext(),
                    AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                    "")
                val type = sharedPreferences.getString(App.applicationContext(),
                    AppENUM.UserKeySaveENUM.SHOP_TYPE,
                    AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
                val lang = sharedPreferences.getString(App.applicationContext(),
                    AppENUM.RefactoredStrings.LANGUAGE_CODE,
                    AppENUM.RefactoredStrings.defaultLanguage)
                val currency = sharedPreferences.getString(App.applicationContext(),
                    AppENUM.UserKeySaveENUM.CURRENCY,
                    AppENUM.RefactoredStrings.defaultCurrency)
                val countryId = sharedPreferences.getString(App.applicationContext(),
                    AppENUM.UserKeySaveENUM.COUNTRY_ID,
                    AppENUM.RefactoredStrings.defaultCountryId)
                val versionCode = BuildConfig.VERSION_CODE
                val requestBuilder = request.newBuilder().url(newUrl)
                    .header(AppENUM.AUTHORIZATION, String.format("%s %s", "Bearer", auth))
                    .header(AppENUM.RefactoredStrings.WORKSHOP_ID_HEADER, wid)
                    .header(AppENUM.RefactoredStrings.LANGUAGE, lang)
                    .header(AppENUM.RefactoredStrings.CURRENCY, currency)
                    .header(AppENUM.RefactoredStrings.COUNTRY, countryId)
                    .header(AppENUM.RefactoredStrings.TYPE, type)
                    .header(AppENUM.RefactoredStrings.VERSION_CODE, versionCode.toString())
                request = requestBuilder.build()
                chain.proceed(request)
            }
        }
        return builder.build()
    }
}
