package whitelisted.garage.network

import com.google.gson.annotations.SerializedName

data class ServerResponse<T>(@SerializedName("status") val status: Boolean,
    @SerializedName("message") val message: String,
    @SerializedName("count") val count: Int,
    @SerializedName("data") val data: T,
    @SerializedName("error") val error: String,
    @SerializedName("errorCode") val errorCode: Int)

