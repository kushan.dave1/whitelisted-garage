package whitelisted.garage.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

abstract class CommonOkhttpInstance : OkHttpClient() {
    companion object {
        private var INSTANCE: OkHttpClient? = null
        fun getInstance(): OkHttpClient {
            if (INSTANCE == null) {
                INSTANCE = OkHttpClient.Builder().connectTimeout(10, TimeUnit.MINUTES)
                    .writeTimeout(30, TimeUnit.MINUTES).readTimeout(30, TimeUnit.MINUTES)
                    .retryOnConnectionFailure(true).addInterceptor(HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY

                    }).build()
            }
            return INSTANCE as OkHttpClient
        }

    }

}