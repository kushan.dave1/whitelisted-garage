package whitelisted.garage.eventBus

import whitelisted.garage.base.BaseEventBusModel

class WorkshopDeletedEvent(var id: String) : BaseEventBusModel(id) {}