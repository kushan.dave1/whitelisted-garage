package whitelisted.garage.eventBus

import whitelisted.garage.base.BaseEventBusModel

class RefreshOrderSummaryEvent(orderId: String) : BaseEventBusModel(orderId)