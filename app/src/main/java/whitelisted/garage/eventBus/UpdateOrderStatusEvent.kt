package whitelisted.garage.eventBus

import whitelisted.garage.base.BaseEventBusModel

class UpdateOrderStatusEvent(val status: String) : BaseEventBusModel(status)