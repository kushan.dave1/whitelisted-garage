package whitelisted.garage.eventBus

import whitelisted.garage.api.request.JobCardItem
import whitelisted.garage.base.BaseEventBusModel

class AddJobCardPartEventModel(type: String, jcItem: JobCardItem) : BaseEventBusModel(type, jcItem)