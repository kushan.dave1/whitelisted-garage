package whitelisted.garage.eventBus

import whitelisted.garage.base.BaseEventBusModel

class ProLockedEvent(msg:String) : BaseEventBusModel(str = msg)