package whitelisted.garage.eventBus

import com.razorpay.PaymentData
import whitelisted.garage.base.BaseEventBusModel

class PaymentEventModel(paymentData: PaymentData) : BaseEventBusModel("", extra = paymentData) {}