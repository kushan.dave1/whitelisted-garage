package whitelisted.garage.eventBus

import whitelisted.garage.api.response.GetWsBidEnquiryResponse
import whitelisted.garage.base.BaseEventBusModel

class RefreshWsBidList(var wsBidEnquiryResponse: GetWsBidEnquiryResponse?) :
    BaseEventBusModel("", wsBidEnquiryResponse)