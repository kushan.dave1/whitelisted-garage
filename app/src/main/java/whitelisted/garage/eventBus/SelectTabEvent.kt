package whitelisted.garage.eventBus

import whitelisted.garage.base.BaseEventBusModel

class SelectTabEvent(tabPosition: String) : BaseEventBusModel(tabPosition) {}