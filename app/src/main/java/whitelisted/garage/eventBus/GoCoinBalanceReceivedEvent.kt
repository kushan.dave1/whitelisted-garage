package whitelisted.garage.eventBus

import whitelisted.garage.base.BaseEventBusModel

class GoCoinBalanceReceivedEvent(var gocoinBalance: Long) : BaseEventBusModel("", gocoinBalance)