package whitelisted.garage.eventBus

import whitelisted.garage.base.BaseEventBusModel

class GmbRequestedEvent(var isGMBRequested: Boolean) : BaseEventBusModel("", isGMBRequested)