package whitelisted.garage.eventBus

import whitelisted.garage.api.request.OrderInclusionsItem
import whitelisted.garage.base.BaseEventBusModel

class AddRetailInclusionPartEventModel(type: String, orderInclusionsItem: OrderInclusionsItem) :
    BaseEventBusModel(type, orderInclusionsItem)