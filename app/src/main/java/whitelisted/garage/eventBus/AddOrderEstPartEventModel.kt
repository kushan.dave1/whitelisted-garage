package whitelisted.garage.eventBus

import whitelisted.garage.api.request.OEServicesItem
import whitelisted.garage.base.BaseEventBusModel

class AddOrderEstPartEventModel(type: String, oeItem: OEServicesItem) :
    BaseEventBusModel(type, oeItem)