package whitelisted.garage.eventBus

import whitelisted.garage.api.data.CarNameOrderIdStatusModel
import whitelisted.garage.base.BaseEventBusModel

class SetOrderDetailsEvent(val carNameOrderIdStatusModel: CarNameOrderIdStatusModel) :
    BaseEventBusModel(extra = carNameOrderIdStatusModel)