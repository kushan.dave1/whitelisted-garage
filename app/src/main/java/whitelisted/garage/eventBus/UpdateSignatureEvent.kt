package whitelisted.garage.eventBus

import android.graphics.Bitmap
import whitelisted.garage.base.BaseEventBusModel

class UpdateSignatureEvent(bitmap: Bitmap) : BaseEventBusModel(extra = bitmap)