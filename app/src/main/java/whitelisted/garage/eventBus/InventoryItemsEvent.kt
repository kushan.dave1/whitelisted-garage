package whitelisted.garage.eventBus

import whitelisted.garage.api.request.RackItem
import whitelisted.garage.base.BaseEventBusModel

class InventoryItemsEvent(val inventoryItemsList: MutableList<RackItem>) :
    BaseEventBusModel("", inventoryItemsList)