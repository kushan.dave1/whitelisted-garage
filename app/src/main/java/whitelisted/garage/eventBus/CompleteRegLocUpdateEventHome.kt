package whitelisted.garage.eventBus

import whitelisted.garage.api.request.CompleteRegData
import whitelisted.garage.base.BaseEventBusModel

class CompleteRegLocUpdateEventHome(var addressData: CompleteRegData) :
    BaseEventBusModel("", addressData)