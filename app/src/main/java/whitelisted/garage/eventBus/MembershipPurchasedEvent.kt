package whitelisted.garage.eventBus

import whitelisted.garage.base.BaseEventBusModel

class MembershipPurchasedEvent(isSuccess:Boolean): BaseEventBusModel(extra = isSuccess) {}