package whitelisted.garage.eventBus

import whitelisted.garage.api.data.SetOptionsImgStatusObject
import whitelisted.garage.base.BaseEventBusModel

class SetOptionsImageStatusEvent(forRetailer: String,
    setOptionsImgStatusObject: SetOptionsImgStatusObject) :
    BaseEventBusModel(forRetailer, setOptionsImgStatusObject)