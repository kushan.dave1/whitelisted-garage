package whitelisted.garage.eventBus

import whitelisted.garage.base.BaseEventBusModel

class OTPDetectedEvent(val otpString: String?) : BaseEventBusModel("")