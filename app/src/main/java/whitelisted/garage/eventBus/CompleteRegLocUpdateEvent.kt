package whitelisted.garage.eventBus

import whitelisted.garage.api.request.CompleteRegData
import whitelisted.garage.base.BaseEventBusModel

class CompleteRegLocUpdateEvent(var addressData: CompleteRegData) :
    BaseEventBusModel("", addressData)