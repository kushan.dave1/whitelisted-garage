package whitelisted.garage.eventBus

import whitelisted.garage.api.request.AddressData
import whitelisted.garage.base.BaseEventBusModel

class LocationSelectedEvent(var addressData: AddressData) : BaseEventBusModel("", addressData)