@file:Suppress("DeferredIsResult")

package whitelisted.garage.api

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.*
import whitelisted.garage.api.data.GetAccessoriesCatalogueResponse
import whitelisted.garage.api.request.*
import whitelisted.garage.api.response.*
import whitelisted.garage.api.response.login.GetProfileCompletion
import whitelisted.garage.api.response.login.LoginUserResponse
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.view.fragments.orderInventory.OrderInventoryRequest

interface API {

    @GET("users/otp/")
    fun getOTP(@Query("hash_code") hashCode: String?,
        @Query("country_code") countryCode: String?,
        @Query("mobile") mobile: String?): Deferred<Response<ServerResponse<String>>>

    @GET("users/otp/")
    fun getOTP(@Query("hash_code") hashCode: String?,
        @Query("country_code") countryCode: String?,
        @Query("mobile") mobile: String,
        @Query("otp") otp: String): Deferred<Response<ServerResponse<String>>>

    @GET("users/profile/{id}/")
    fun getUserProfile(@Path("id") id: String): Deferred<Response<ServerResponse<GetUserProfileResponse>>>

    @PUT("users/profile/{id}/")
    fun updateUserProfile(@Path("id") id: String,
        @Body request: UpdateProfileRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("users/otp/")
    fun getOTPForLogin(@Query("hash_code") hashCode: String?,
        @Query("country_code") countryCode: String?,
        @Query("mobile") mobile: String?,
        @Query("action") action: String = "login"): Deferred<Response<ServerResponse<String>>>

    @POST("users/sign-up/")
    fun signUpUser(@Body signUpRequest: SignUpRequest): Deferred<Response<ServerResponse<LoginUserResponse>>>

    @POST("users/login/")
    fun loginUser(@Body loginReq: LoginRequest): Deferred<Response<ServerResponse<LoginUserResponse>>>

    @POST("business/employees/")
    fun addEmployee(@Query("workshop_id") workshopId: String,
        @Body addEditEmployee: AddEditEmployee): Deferred<Response<ServerResponse<LoginUserResponse>>>

    @POST("business/employees/")
    fun editEmployee(@Body addEditEmployee: AddEditEmployee,
        @Query("id") employeeId: String?): Deferred<Response<ServerResponse<LoginUserResponse>>>

    @POST("users/otp/")
    fun verifyOTP(@Body verifyOtpRequest: VerifyOtpRequest): Deferred<Response<ServerResponse<Any>>>

    @POST("users/otp/")
    fun verifyOTPForLogin(@Body verifyOtpRequest: VerifyOtpRequest): Deferred<Response<ServerResponse<LoginUserResponse>>>

    @POST("users/otp/")
    fun trueCallerLogin(@Body trueCallerLoginRequest: TrueCallerLoginRequest): Deferred<Response<ServerResponse<LoginUserResponse>>>

    @GET("business/employees/")
    fun getManageEmployeeData(@Query("workshop_id") workshopId: String): Deferred<Response<ServerResponse<List<EmployeeListResponseModel>>>>

    @GET("business/employees/")
    fun getRoleBasedEmployeeList(@Query("roles") role: String): Deferred<Response<ServerResponse<List<EmployeeListResponseModel>>>>

    @POST("users/forgot-password/")
    fun resetPassword(@Body resetPasswordRequest: ResetPasswordRequest): Deferred<Response<ServerResponse<Any>>>

    @GET("business/workshops/")
    fun getWorkshopList(): Deferred<Response<ServerResponse<List<WorkshopListResponseModel>>>>

    @GET("whitelisted-workshop/check-subscription/")
    fun getSubscriptionStatus(@Query("workshop_id") workshopId: String?): Deferred<Response<ServerResponse<SubscriptionStatusResponse>>>

    @GET("business/home-dashboard/")
    fun getHomeScreen(@Query("workshop_id") workshopId: String?): Deferred<Response<ServerResponse<HomeScreenResponse>>>

    @POST("orders/order/")
    fun placeOrder(@Body request: NewOrderRequest?): Deferred<Response<ServerResponse<PlaceOrderResponse>>>

    @POST("orders/order/")
    fun updateOrderDetail(@Query("order_id") orderId: String?,
        @Body request: NewOrderRequest?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>

    @GET("orders/order/")
    fun getOrderDetail(@Query("order_id") mobile: String?): Deferred<Response<ServerResponse<OrderDetailResponse>>>

    @GET("orders/inventory-check-list/")
    fun getOrderInventoryCheckList(@Query("two_wheeler") isTwoWheeler: Boolean = false): Deferred<Response<ServerResponse<List<String>>>>

    @GET("orders/car-documents/")
    fun getCarDocsList(): Deferred<Response<ServerResponse<List<String>>>>

    @POST("orders/inventory/")
    fun addOrderInventory(@Body orderInventoryReq: OrderInventoryRequest?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>

    @GET("orders/inventory/")
    fun getOrderInventory(@Query("order_id") orderId: String): Deferred<Response<ServerResponse<OrderInventoryRequest>>>

    @GET("car/cars/")
    fun getCarsList(): Deferred<Response<ServerResponse<List<CarListModel>>>>

    @GET("get_all_packages")
    fun getPackagesList(@Query("package") packageId: Int = 1): Deferred<Response<ServerResponse<List<PackagesResponse>>>>

    @GET("get_all_services")
    fun getServicesList(@Query("value") searchedString: String?): Deferred<Response<ServerResponse<List<ServiceResponseModel>>>>

    @GET("get_all_bike_services")
    fun getBikeServicesList(@Query("value") searchedString: String?): Deferred<Response<ServerResponse<List<ServiceResponseModel>>>>

    @POST("business/workshops/")
    fun addWorkshop(@Body request: AddWorkshopRequest?): Deferred<Response<ServerResponse<WorkshopIDResponse>>>

    @PUT("business/workshops/")
    fun updateWorkshop(@Query("id") workshopId: String,
        @Body request: AddWorkshopRequest?): Deferred<Response<ServerResponse<WorkshopIDResponse>>>

    @GET("business/order-reminders/")
    fun getRemindersList(@Query("type") reminderType: String?,
        @Query("period") periodType: String): Deferred<Response<ServerResponse<RemindersListResponse>>>

    @GET("business/order-reminders/")
    fun searchRemindersList(@Query("search") reminderType: String?): Deferred<Response<ServerResponse<RemindersListResponse>>>

    @POST("business/order-reminders/")
    fun sendReminder(@Query("order_id") orderId: String?): Deferred<Response<ServerResponse<OrderIdResponse>>>

    @POST("business/order-reminders/")
    fun scheduleReminder(@Query("order_id") orderId: String?,
        @Query("schedule_at") schedule_at: String?): Deferred<Response<ServerResponse<OrderIdResponse>>>

    @DELETE("business/workshops/")
    fun deleteWorkshop(@Query("id") workshopId: String): Deferred<Response<ServerResponse<WorkshopIDResponse>>>


    @DELETE("business/employees/")
    fun deleteEmployee(@Query("id") workshopId: String): Deferred<Response<ServerResponse<WorkshopIDResponse>>>

    @GET("business/custom-package/")
    fun getCustomPackageList(): Deferred<Response<ServerResponse<List<AllPackagesResponseModel>>>>

    @GET("business/custom-bike-package/")
    fun getCustomPackageListBike(): Deferred<Response<ServerResponse<List<AllPackagesResponseModel>>>>

    @GET("business/customers/")
    fun getMyCustomerList(): Deferred<Response<ServerResponse<List<MyCustomerModel>>>>

    @GET("business/customers/")
    fun getMyCustomerListWithSearch(@Query("search") search: String?): Deferred<Response<ServerResponse<List<MyCustomerModel>>>>

    @POST("business/custom-package/")
    fun createCustomPackage(@Body request: CreateCustomPackageRequest?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @POST("business/custom-bike-package/")
    fun createCustomPackageBike(@Body request: CreateCustomPackageRequest?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @PUT("business/custom-package/")
    fun updateCustomPackageName(@Query("id") packageId: String?,
        @Body request: UpdatePackageNameRequest?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @PUT("business/custom-bike-package/")
    fun updateCustomPackageNameBike(@Query("id") packageId: String?,
        @Body request: UpdatePackageNameRequest?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @DELETE("business/custom-package/")
    fun deleteCustomPackage(@Query("package_id") packageId: String?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @DELETE("business/custom-bike-package/")
    fun deleteCustomPackageBike(@Query("package_id") packageId: String?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @GET("get_work_done")
    fun getWorkDoneList(): Deferred<Response<ServerResponse<List<WorkDoneResponseModel>>>>

    @POST("business/custom-package-services/")
    fun addServiceToPackage(@Query("package_id") packageId: String?,
        @Body request: ServicesItem?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @POST("business/custom-bike-package-services/")
    fun addServiceToPackageBike(@Query("package_id") packageId: String?,
        @Body request: ServicesItem?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @PUT("business/custom-package-services/")
    fun updateServiceOfPackage(@Query("package_id") packageId: String?,
        @Query("service_id") serviceId: Int?,
        @Body request: ServicesItem?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @PUT("business/custom-bike-package-services/")
    fun updateServiceOfPackageBike(@Query("package_id") packageId: String?,
        @Query("service_id") serviceId: Int?,
        @Body request: ServicesItem?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @DELETE("business/custom-package-services/")
    fun deleteServiceOfPackage(@Query("package_id") packageId: String?,
        @Query("service_id") serviceId: Int?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @DELETE("business/custom-bike-package-services/")
    fun deleteServiceOfPackageBike(@Query("package_id") packageId: String?,
        @Query("service_id") serviceId: Int?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @GET("business/custom-package-services/")
    fun getServicesOfPackage(@Query("package_id") packageId: String?): Deferred<Response<ServerResponse<List<ServicesItem>>>>

    @GET("business/custom-bike-package-services/")
    fun getServicesOfPackageBike(@Query("package_id") packageId: String?): Deferred<Response<ServerResponse<List<ServicesItem>>>>

    @GET("/business/custom-package/")
    fun getAccCustomPackageDetails(@Query("package_id") packageId: String?): Deferred<Response<ServerResponse<GetAccCustomPackageDetails>>>

    @GET("/business/custom-bike-package/")
    fun getAccCustomPackageDetailsBike(@Query("package_id") packageId: String?): Deferred<Response<ServerResponse<GetAccCustomPackageDetails>>>

    @GET("business/package/")
    fun getExistingPackages(): Deferred<Response<ServerResponse<List<AllPackagesResponseModel>>>>

    @PUT("business/gomechanic-packages/")
    fun duplicatePackage(@Query("package_id") packageId: String?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    @GET("orders/bills/")
    fun getBillingList(@Query("start_date") startDate: String?,
        @Query("end_date") endDate: String?): Deferred<Response<ServerResponse<List<BillingResponseModel>>>>

    @GET("orders/filter/")
    fun getOrdersList(@Query("type") type: String?): Deferred<Response<ServerResponse<OrderListResponse>>>

    @GET("orders/history/")
    fun getFilteredOrdersList(@Query("date_filter") date_filter: String?,
        @Query("start_date") start_date: String?,
        @Query("end_date") end_date: String?,
        @Query("limit") limit: Int = 1,
        @Query("offset") offSet: Int = 0): Deferred<Response<ServerResponse<OrderListResponse>>>

    @GET("business/ledger/")
    fun getLedger(@Query("type") type: String?,
        @Query("transaction_type") transactionType: String?): Deferred<Response<ServerResponse<LedgerResponseModel>>>

    @POST("business/ledger/")
    fun addLedger(@Body addToLedgerRequest: AddToLedgerRequest): Deferred<Response<ServerResponse<LedgerResponseModel>>>

    @GET("orders/order/")
    fun getSearchOrdersList(@Query("search") searchString: String?): Deferred<Response<ServerResponse<List<SearchOrderResponseModel>>>>

    @POST("orders/job-card/")
    fun addJobCard(@Body jobCardRequest: JobCardRequest?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>

    @GET("orders/job-card/")
    fun getJobCard(@Query("order_id") orderId: String?): Deferred<Response<ServerResponse<List<JobCardRequest>>>>

    @PUT("orders/estimate/")
    fun updateOrderEstimates(@Body orderEstimatesRequest: OrderEstimateRequestResponse?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>

    @GET("orders/estimate/")
    fun getOrderEstimates(@Query("order_id") orderId: String?): Deferred<Response<ServerResponse<List<OrderEstimateRequestResponse>>>>

    @GET("/orders/payments/")
    fun getPaymentDetails(@Query("order_id") orderId: String?): Deferred<Response<ServerResponse<List<GetPaymentResponseModel>>>>

    @PUT("orders/payments/")
    fun completePayment(@Body orderIdRequest: OrderIdResponse?): Deferred<Response<ServerResponse<JSONObject>>>

    @POST("orders/payments/")
    fun addPayment(@Body paymentRequest: AddPaymentRequest?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>

    @GET("business/text/")
    fun getMessageFromAPI(@Query("user_id") orderId: String?): Deferred<Response<ServerResponse<GetMessageFromApiResponse>>>

    @GET("business/text/")
    fun getMessageFromAPIRetailer(@Query("mobile") mobile: String?): Deferred<Response<ServerResponse<GetMessageFromApiResponse>>>

    @GET("gms/banner-redirection/")
    fun getBannerImages(): Deferred<Response<ServerResponse<BannerImagesResponse>>>

    @GET("gms/carousel-redirection/")
    fun getCarousel(): Deferred<Response<ServerResponse<HomeCarouselResponse>>>

    @GET("business/ledger/")
    @Streaming
    fun downloadLedgerCSV(@Query("type") type: String = "today",
        @Query("download") download: Int = 1): Deferred<Response<ResponseBody>>

    @GET("orders/bills/")
    @Streaming
    fun downloadBillingCSV(@Query("start_date") startDate: String?,
        @Query("end_date") endDate: String?,
        @Query("download") download: Int = 1): Deferred<Response<ResponseBody>>

    @GET("orders/history/")
    @Streaming
    fun downloadOrderHistoryCSV(@Query("date_filter") date_filter: String?,
        @Query("start_date") start_date: String?,
        @Query("end_date") end_date: String?,
        @Query("download") download: Int = 1): Deferred<Response<ResponseBody>>

    @PUT("orders/order-status/")
    fun cancelOrder(@Body cancelOrder: CancelOrderRequest?): Deferred<Response<ServerResponse<JSONObject>>>

    @GET("business/upi/")
    fun getUPIId(): Deferred<Response<ServerResponse<GetUPIResponse>>>

    @POST("business/upi/")
    fun saveUPIId(@Body saveUPIIdRequest: SaveUPIIdRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("orders/summary/")
    fun getOrderSummary(@Query("order_id") orderId: String?): Deferred<Response<ServerResponse<List<OrderSummaryResponse>>>>

    @GET("users/otp/")
    fun getOTPForProfile(@Query("mobile") mobile: String): Deferred<Response<ServerResponse<String>>>

    @POST("users/otp/")
    fun verifyOTPForProfile(@Body verifyOtpRequest: VerifyOtpRequest): Deferred<Response<ServerResponse<Any>>>

    @GET("gms/screens-configuration/")
    fun getConfig(): Deferred<Response<ServerResponse<ScreensConfigResponse>>>

    @GET("gms/versions/")
    fun getUpdateInfo(): Deferred<Response<ServerResponse<GetUpdateInfoResponse>>>

    @GET("business/gmb-categories/")
    fun getGMBCategories(): Deferred<Response<ServerResponse<GetGMDCategoriesResponse>>>

    @GET("business/go-coins/")
    fun getGoCoinsScreenResponse(): Deferred<Response<ServerResponse<GetGoCoinsScreenResponse>>>

    @POST("users/pin/{userId}/")
    fun setPin(@Path("userId") userId: String,
        @Body request: SetPinRequest): Deferred<Response<ServerResponse<SetPinRequest>>>

    @GET("car/brands/")
    fun getBrandsList(): Deferred<Response<ServerResponse<List<BrandsResponseModel>>>>

    @GET("bike/brands/")
    fun getBikeBrandsList(): Deferred<Response<ServerResponse<List<BrandsResponseModel>>>>

    @GET("car/brand-cars/{brandId}/")
    fun getCarModelsList(@Path("brandId") brandId: String?): Deferred<Response<ServerResponse<List<CarModelResponseModel>>>>

    @GET("bike/brand-bike/{brandId}/")
    fun getBikeModelsList(@Path("brandId") brandId: String?): Deferred<Response<ServerResponse<List<CarModelResponseModel>>>>

    @GET("car/fuel-types/{modelId}/")
    fun getFuelTypeList(@Path("modelId") modelId: String?): Deferred<Response<ServerResponse<FuelTypeListResponse>>>

    @GET("bike/fuel-types/{modelId}/")
    fun getBikeFuelTypeList(@Path("modelId") modelId: String?): Deferred<Response<ServerResponse<FuelTypeListResponse>>>

    @GET("business/gmb/")
    fun getGMB(): Deferred<Response<ServerResponse<GetGMDResponse>>>

    @GET("/business/gmb-website/")
    fun getWebsite(): Deferred<Response<ServerResponse<GetWebsiteResponse>>>

    @GET("business/gmb-attributes/")
    fun getGMBAttributes(): Deferred<Response<ServerResponse<MutableMap<String, MutableList<String>>>>>

    @POST("business/gmb/")
    fun saveGMB(@Body saveGMBRequest: SaveGMBRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @POST("/business/gmb-website/")
    fun saveGMBWebsite(@Body saveGMBRequest: SaveWebsiteRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("business/gocoins-options/")
    fun getGoCoinsOptions(): Deferred<Response<ServerResponse<GetGOCoinOptionsResponse>>>

    @POST("business/go-coins/")
    fun unLockGMB(@Body unlockGMBRequest: UnlockGMBRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("users/pin/{userId}/")
    fun getPin(@Path("userId") userId: String): Deferred<Response<ServerResponse<SetPinRequest>>>

    @POST("business/workshop-order/")
    @FormUrlEncoded
    fun getOrderIdForPayment(@FieldMap map: Map<String, String>): Deferred<Response<ServerResponse<OrderIdForPaymentResponse>>>

    @GET("gms/rzp-key/")
    fun getRazorPayKey(): Deferred<Response<ServerResponse<GetRazorPayKey>>>

    @POST("business/workshop-payment/")
    @FormUrlEncoded
    fun verifyRazorPayPayment(@FieldMap map: Map<String, String>): Deferred<Response<ServerResponse<JsonElement>>>

    @POST("users/buy-membership/")
    @FormUrlEncoded
    fun buyMembership(@FieldMap map: Map<String, String>): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("business/gocoins-transactions/")
    fun getAllGoCoinTransactions(@Query("type") type: String,
        @Query("period") period: String): Deferred<Response<ServerResponse<GCTransactionsResponse>>>

    @GET("gms/retailers-catalogue/")
    fun getRetailersCatalogue(@Query("search") type: String): Deferred<Response<ServerResponse<List<GetInventoryPartItemResponse>>>>

    @GET("gms/retailers-bike-catalogue/")
    fun getRetailersCatalogueBike(@Query("value") type: String): Deferred<Response<ServerResponse<List<GetInventoryPartItemResponse>>>>

    @POST("business/retailers-rack/")
    fun createInventoryRack(@Body request: CreateInventoryRackRequest): Deferred<Response<ServerResponse<RackIdResponse>>>

    @POST("business/retailers-rack/")
    fun updateInventoryRack(@Query("id") id: String,
        @Body request: CreateInventoryRackRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @DELETE("business/retailers-rack/")
    fun deleteInventoryRack(@Query("id") id: String): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("business/retailers-rack/")
    fun getInventoryRack(): Deferred<Response<ServerResponse<MutableList<GetInventoryRackResponse>>>>

    @GET("business/retailers-rack/")
    fun getIndividualInventoryRack(@Query("id") id: String): Deferred<Response<ServerResponse<GetInventoryRackResponse>>>

    @GET("gms/image-search/")
    fun searchImages(@Query("q") q: String): Deferred<Response<ServerResponse<List<SearchImageResponse>>>>

    @POST("business/retailers-rack/")
    fun updateInventoryRackDetails(@Query("id") id: String,
        @Query("update_only_rack") update_only_rack: Int,
        @Body request: CreateInventoryRackRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @POST("orders/order/")
    fun createUpdateRetailOrder(@Body request: RetailCreateOrderRequest?): Deferred<Response<ServerResponse<RetailPlaceOrderResponse>>>

    @GET("business/customers/")
    fun getSavedWorkshopList(@Query("saved") id: String = "1"): Deferred<Response<ServerResponse<List<SavedWorkshopResponseModel>>>>

    @GET("business/go-coin-balance/")
    fun getGoCoinBalance(): Deferred<Response<ServerResponse<GetGoCoinBalanceResponse>>>

    @GET("/gms/accessories-catalogue/")
    fun getAccessoriesCatalogue(@Query("search") type: String): Deferred<Response<ServerResponse<MutableList<GetAccessoriesCatalogueResponse>>>>

    @GET("/gms/bike-accessories-catalogue/")
    fun getAccessoriesCatalogueBike(@Query("value") type: String): Deferred<Response<ServerResponse<MutableList<GetAccessoriesCatalogueResponse>>>>

    @GET("orders/order/")
    fun getRetailOrderDetail(@Query("order_id") mobile: String?): Deferred<Response<ServerResponse<RetailOrderDetailResponse>>>

    @PUT("orders/estimate/")
    fun updateOrderInclusions(@Body orderEstimatesRequest: OrderInclusionsRequestResponse?): Deferred<Response<ServerResponse<OrderInclusionsRequestResponse>>>

    @GET("orders/estimate/")
    fun getOrderInclusions(@Query("order_id") orderId: String?): Deferred<Response<ServerResponse<List<OrderInclusionsRequestResponse>>>>

    @GET("business/retailers-rack/")
    @Streaming
    fun downloadInventoryCSV(@Query("download") download: Int = 1): Deferred<Response<ResponseBody>>


    @GET("business/gocoins-transactions/")
    @Streaming
    fun getAllGoCoinTransactionsCSV(@Query("type") type: String,
        @Query("period") period: String,
        @Query("download") download: Int = 1): Deferred<Response<ResponseBody>>

    @GET("gms/gocoins-value/")
    fun getGoCoinsValue(): Deferred<Response<ServerResponse<GoCoinsValueResponse>>>

    @PATCH("gms/retailers-catalogue/")
    fun updateInventoryItemValue(@Body request: UpdateInventoryItemRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @PATCH("get_all_services")
    fun updateInventoryItemValueForWorkshop(@Body request: UpdateInventoryItemRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @POST("business/retailers-rack-insert/")
    fun addItemToRack(@Query("id") rackId: String?,
        @Body request: CreateInventoryRackRequest?): Deferred<Response<ServerResponse<JsonElement>>>

    @GET(" /business/referal-code/")
    fun getReferralCode(): Deferred<Response<ServerResponse<GetReferralCodeResponse>>>

    @GET(" /users/profile-completion/{userId}/")
    fun getProfileCompletion(@Path("userId") userId: String): Deferred<Response<ServerResponse<GetProfileCompletion>>>

    @POST(" /business/referal-code/")
    fun availReferCode(@Body req: AvailReferralRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @PUT(" /users/profile-completion/{userId}/")
    fun updateProfileCompletion(@Path("userId") userId: String,
        @Body request: SaveProfileCompletionRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @POST("business/send-reminders/")
    fun sendDuesReminder(@Body req: OrderIdResponse): Deferred<Response<ServerResponse<MsgKeyResponse>>>

    @POST("orders/credit-payments/")
    fun collectDue(@Body req: LedgerIdRequest?): Deferred<Response<ServerResponse<JsonElement>>>

    @PUT(" /users/profile-completion/{userId}/")
    fun updateUserLocation(@Path("userId") userId: String,
        @Body request: UpdateLocationRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("/gms/most-use-retailers-catalogue/")
    fun getMostUsedPartsRetailerCatalogue(@Query("two_wheeler") isTwoWheeler: Boolean = false): Deferred<Response<ServerResponse<MutableList<MostUsedPartsResponseModel>>>>

    @GET("/gms/most-use-workshop-catalogue/")
    fun getMostUsedPartsWorkshopCatalogue(@Query("two_wheeler") isTwoWheeler: Boolean = false): Deferred<Response<ServerResponse<MutableList<MostUsedPartsResponseModel>>>>

    @GET("business/available-coupons/")
    fun getAllCoupons(@Query("type") type: String): Deferred<Response<ServerResponse<GetAllCouponsResponse>>>

    @GET("business/coupon/")
    fun applyCoupon(@Query("code") code: String?,
        @Query("type") type: String): Deferred<Response<ServerResponse<ApplyCouponResponse>>>

    @GET("gms/get-location-information/")
    fun getLocationDetails(@Query("ip_address") ipAddress: String): Deferred<Response<ServerResponse<LocationDetailsResponse>>>

    @GET("gms/get-country/")
    fun getCountryCodes(): Deferred<Response<ServerResponse<MutableList<CountryCodeResponseModel>>>>

    @GET("business/previous-months/")
    fun getPreviousMonthData(): Deferred<Response<ServerResponse<PreviousMonthResponse>>>

    @GET("/business/customer-reminder/")
    fun getCustomerReminder(): Deferred<Response<ServerResponse<GetCustomerReminderResponse>>>

    @GET("/business/customer-reminder/")
    fun getAllCustomerReminder(@Query("data") data: String? = "all"): Deferred<Response<ServerResponse<GetCustomerReminderResponse>>>

    @POST("/business/customer-reminder/")
    fun sendInstantReminder(@Body request: SendInstantReminderRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @PUT("/business/customer-reminder/")
    fun collectDueOnReminder(@Body request: PreviousReminder): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("business/failed-orders/")
    fun getRetryPaymentResponse(): Deferred<Response<ServerResponse<RetryPaymentResponse>>>

    @PUT("business/failed-orders/")
    fun cancelPendingPayment(@Body request: IdRequestResponse): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("business/home-dashboard/")
    fun getHomeScreenForMonth(@Query("workshop_id") workshopId: String?,
        @Query("month") monthPos: String?): Deferred<Response<ServerResponse<HomeScreenResponse>>>

    @POST("business/biding-cart/")
    fun createWorkshopBiddingAsync(@Body request: WorkShopBiddingRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("business/biding/")
    fun getNewBidsList(@Query("bid_type_id") bidTypeId: String? = "1"): Deferred<Response<ServerResponse<MutableList<AccRetailBidListResponseModel>>>>

    @GET("business/biding/")
    fun getSubmittedBidsList(@Query("bid_type_id") bidTypeId: String? = "5"): Deferred<Response<ServerResponse<MutableList<AccRetailBidListResponseModelSubmitAccept>>>>

    @GET("business/biding/")
    fun getAcceptedBidsList(@Query("bid_type_id") bidTypeId: String? = "6"): Deferred<Response<ServerResponse<MutableList<AccRetailBidListResponseModelSubmitAccept>>>>

    @GET("business/biding-cart/")
    fun getOnGoingWsBiddingList(@Query("type") type: String?): Deferred<Response<ServerResponse<List<GetWsOngoingBidResponse>>>>

    @GET("business/biding-cart/?history=1")
    fun getWsHistoryBiddingList(): Deferred<Response<ServerResponse<List<GetWsOngoingBidResponse>>>>

    @GET("business/most-recomended-part-biding/")
    fun getWsMostPart(): Deferred<Response<ServerResponse<GetWsMostUsedPart>>>

    @GET("business/biding-cart/")
    fun getWsReceivedBids(@Query("id") id: String): Deferred<Response<ServerResponse<GetWsBidReceivedResponse>>>

    @PATCH("business/biding-cart/")
    fun markCompleteWsBid(@Body() request: MarkCompleteWsBidRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("business/biding-details/{id}/")
    fun getWsBidEnquiry(@Path("id") id: String): Deferred<Response<ServerResponse<GetWsBidEnquiryResponse>>>

    @POST("business/biding-details/{id}/")
    fun changeStatusOfWsBid(@Path("id") id: String,
        @Body request: WsAccRejBid): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("business/biding/")
    fun getBidDetails(@Query("id") orderId: String?): Deferred<Response<ServerResponse<BidDetailResponse>>>

    @POST("business/biding/")
    fun placeBid(@Query("id") bidId: String?,
        @Body request: BidDetailResponse): Deferred<Response<ServerResponse<JsonElement>>>

    @DELETE("business/biding/")
    fun cancelBidRetailer(@Query("id") orderId: String?): Deferred<Response<ServerResponse<JsonElement>>>

    @PATCH("business/biding-cart/")
    fun unlockPhoneRetailer(@Query("id") orderId: String?,
        @Body request: IdTypeRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @PATCH("business/biding/")
    fun markBidCompleteRetailer(@Query("id") orderId: String?,
        @Body request: IdTypeRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @DELETE("business/biding-cart/")
    fun cancelWorkshopBid(@Query("id") orderId: String?): Deferred<Response<ServerResponse<JsonElement>>>

    @PATCH("business/biding/")
    fun unlockPhoneWorkshop(@Query("id") orderId: String?,
        @Body request: IdTypeRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("orders/biding-invoice/")
    fun getWsAccSpareBiddingCSV(@Query("id") id: String): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("business/near-by-workshop/")
    fun getNearByBidingWorkShop(): Deferred<Response<ServerResponse<Map<String, GetNearByBidingWorkshop>>>>

    @POST("gms/user-finger-print/")
    fun updateDeviceTokenForFCM(@Body request: FingerprintRequest): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("gms/rack-sample-file")
    fun downloadSampleCSV(): Deferred<Response<ResponseBody>>

    @Multipart
    @POST("business/retailers-rack/")
    fun uploadInventoryCSVFile(@Part file: MultipartBody.Part,
        @Query("upload") isUpload: Int): Deferred<Response<ServerResponse<Any>>>

    @GET("gms/screens/")
    fun getAvailableTemplates(@Query("config") config: String): Deferred<Response<ServerResponse<List<BusinessCardTemplate>>>>

    @GET("business/business-card/")
    fun getSavedCards(): Deferred<Response<ServerResponse<List<BusinessCardDetails>>>>

    @POST("business/business-card/")
    fun saveBusinessCard(@Body details: BusinessCardDetails): Deferred<Response<ServerResponse<Any>>>

    @PATCH("business/business-card/")
    fun updateBusinessCard(@Body details: BusinessCardDetails): Deferred<Response<ServerResponse<Any>>>

    @GET("shop/gom_part_eg")
    fun getSparesMarketplace(@Query("limit") limit: Int): Deferred<Response<ServerResponse<Any>>>

    @GET("shop/gom_part_eg")
    fun getSSPartsList(@Query("limit") limit: Int = 1,
        @Query("offset") offSet: Int = 0): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>>

    @GET("shop/gom_part_eg/")
    fun getSSPartsListFilter(@QueryMap filters: Map<String, String>): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>>

    @GET("shop/gom_part_eg")
    fun getSSPartsListWithBrandFilter(@Query("brand_name") brandName: String?,
        @Query("limit") limit: Int = 1,
        @Query("offset") offSet: Int = 0): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>>

    @GET("shop/gom_part_eg")
    fun getSSPartsListWithCategoryFilter(@Query("category_name") categoryName: String?,
        @Query("limit") limit: Int = 1,
        @Query("offset") offSet: Int = 0): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>>

    @GET("shop/gom_part_eg")
    fun getSSPartsListWithSearch(@Query("title") searchString: String?,
        @Query("limit") limit: Int = 1,
        @Query("offset") offSet: Int = 0): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>>

    @GET("shop/gom_part_eg")
    fun getSSPartsListWithSearchQuery(@Query("part_id") searchString: String?,
        @Query("limit") limit: Int = 1,
        @Query("offset") offSet: Int = 0): Deferred<Response<ServerResponse<SSItemModel>>>

    @GET("shop/gom_part_eg")
    fun getSSPartsListWithCarFilter(@Query("brand_name") brandName: String?,
        @Query("category_name") categoryName: String?,
        @Query("car_brand") carBrand: String?,
        @Query("car_model") carModel: String?,
        @Query("limit") limit: Int = 1,
        @Query("offset") offSet: Int = 0): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>>

    @GET("shop/home-screen")
    fun getSSSparesBrandsResponse(@Query("config") config: String = "spares_brands"): Deferred<Response<ServerResponse<MutableList<SSBrandsResponseModel>>>>

    @GET("shop/home-screen")
    fun getSSAccBrandsResponse(@Query("config") config: String = "acc_brands"): Deferred<Response<ServerResponse<MutableList<SSBrandsResponseModel>>>>

    @GET("shop/home-screen")
    fun getSSOriginalSparesBrandsResponse(@Query("config") config: String = "original_spares_brands"): Deferred<Response<ServerResponse<MutableList<SSBrandsResponseModel>>>>

    @GET("business/employees-performance/{employee_id}/")
    fun getEmployeePerformance(@Path("employee_id") employeeId: String,
        @Query("month") month: Int,
        @Query("year") year: Int): Deferred<Response<ServerResponse<EmployeePerformanceResponse>>>

    @POST("shop/add-to-cart/v2/{id}")
    fun addToCartSS(@Path("id") productId: String?,
        @Body itemModel: SSItemModel): Deferred<Response<ServerResponse<GetSSCartResponse>>>

    @GET("shop/cart/v3/")
    fun getSSCart(): Deferred<Response<ServerResponse<GetSSCartResponse>>>

    @GET("shop/home-screen")
    fun getSSSparesCategoriesResponse(@Query("config") config: String = "spares_categories"): Deferred<Response<ServerResponse<MutableList<SSCategoryResponseModel>>>>

    @GET("shop/home-screen")
    fun getSSHomeBannersResponse(@Query("config") config: String = "banners"): Deferred<Response<ServerResponse<MutableList<SSHomeBannersResponseModel>>>>

    @GET("shop/home-screen")
    fun getSSAccCategoriesResponse(@Query("config") config: String = "acc_categories"): Deferred<Response<ServerResponse<MutableList<SSCategoryResponseModel>>>>

    @GET("shop/razorpay-key")
    fun getSSRazorPayKey(): Deferred<Response<ServerResponse<GetRazorPayKey>>>

    @POST("shop/checkout-order")
    fun getOrderIdForSSPayment(@Body amountRequest: AmountRequest): Deferred<Response<ServerResponse<OrderIdForPaymentResponse>>>

    @POST("shop/checkout")
    fun checkoutSSOrder(@Body checkoutRequest: SSCheckoutRequest): Deferred<Response<ServerResponse<Any>>>

    @DELETE("business/business-card/")
    fun deleteBusinessCard(@Query("id") id: String): Deferred<Response<ServerResponse<Any>>>

    @GET("shop/add-new-address")
    fun getAllAddress(): Deferred<Response<ServerResponse<MutableList<AddressResponseModel>>>>

    @GET("shop/order-history")
    fun getSparesPurchaseHistory(@Query("limit") limit: Int = 1,
        @Query("offset") offSet: Int = 0): Deferred<Response<ServerResponse<List<SparesOrderHistory>>>>

    @GET("shop/order-history")
    fun getSparesPurchaseHistory(@QueryMap map: Map<String, String>): Deferred<Response<ServerResponse<List<SparesOrderHistory>>>>

    @POST("shop/add-new-address")
    fun addShippingAddressAPI(@Body addressReq: AddressResponseModel): Deferred<Response<ServerResponse<Any>>>

    @PATCH("shop/add-new-address")
    fun editShippingAddressAPI(@Body addressReq: AddressResponseModel): Deferred<Response<ServerResponse<Any>>>

    @DELETE("shop/add-new-address")
    fun deleteShippingAddressAPI(@Query(AppENUM.RefactoredStrings.ADDRESS_ID) id: String): Deferred<Response<ServerResponse<Any>>>

    @GET("shop/apply-discount")
    fun getMarketplaceCoupons(): Deferred<Response<ServerResponse<List<MarketPlaceCoupon>>>>

    @POST("shop/apply-discount")
    fun applyMarketplaceCoupon(@Body req: ShopApplyCouponReq): Deferred<Response<ServerResponse<Any>>>

    @DELETE("shop/apply-discount")
    fun removeMarketplaceCoupon(@Query("id") cartId: String): Deferred<Response<ServerResponse<Any>>>

    @GET("shop/home-screen")
    fun getPDPBannerImage(@Query("config") config: String): Deferred<Response<ServerResponse<PDPBannerResponse>>>

    @GET("shop/marketplace-faqs")
    fun getMarketplaceFAQ(): Deferred<Response<ServerResponse<List<FaqsItem>>>>

    @GET("shop/marketplace-tracking/")
    fun getTrackingURL(): Deferred<Response<ServerResponse<Map<String, String>>>>

    @GET("shop/marketplace-part-filter")
    fun getMarketplaceFilters(): Deferred<Response<ServerResponse<List<MarketplaceFilter>>>>

    @POST("get_all_services")
    fun addServiceByOwn(@Body req: AddServiceByOwnReq): Deferred<Response<ServerResponse<Any>>>

    @POST("{path}")
    fun addBikeServiceByOwn(@Path("path") path: String,
        @Body req: AddServiceByOwnReq): Deferred<Response<ServerResponse<Any>>>

    @DELETE("{path}")
    fun deleteBikeService(@Path("path") path: String,
        @Query("id") id: String): Deferred<Response<ServerResponse<Any>>>

    @POST("shop/marketplace-notification/")
    fun notifyForProduct(@Body req: JsonObject): Deferred<Response<ServerResponse<JsonElement>>>

    @GET("shop/marketplace-notification/")
    fun getNotifiedItemsList(): Deferred<Response<ServerResponse<MutableList<NotifiedItemResponseModel>>>>

    @POST("gms/retailers-catalogue/")
    fun addCustomItemSparesRetailer(@Body req: JsonObject): Deferred<Response<ServerResponse<Map<String, String>>>>

    @POST("gms/accessories-catalogue/")
    fun addCustomItemAccessories(@Body req: JsonObject): Deferred<Response<ServerResponse<Map<String, String>>>>

    @DELETE("get_all_services")
    fun deleteCustomPart(@Query("id") id: String): Deferred<Response<ServerResponse<Any>>>


    @DELETE("gms/accessories-catalogue/")
    fun deleteCustomAccessoriesPart(@Query("sku_code") skuCode: String): Deferred<Response<ServerResponse<Any>>>

    @DELETE("gms/retailers-catalogue/")
    fun deleteCustomRetailersCataloguePart(@Query("sku_code") skuCode: String): Deferred<Response<ServerResponse<Any>>>

    @GET("shop/marketplace-part-filter/v2/")
    fun getSearchAndFilterAttributes(): Deferred<Response<ServerResponse<SearchAndFilterAttrs>>>

    @POST("shop/marketplace-payment-mode/")
    fun getPaymentTypes(@Body request: GetPaymentModesRequest): Deferred<Response<ServerResponse<List<PaymentTypesResponseModel>>>>

    @GET("shop/marketplace-payment-mode/")
    fun getPaymentTypesGet(): Deferred<Response<ServerResponse<List<PaymentTypesResponseModel>>>>

    @GET("shop/marketplace-popular/")
    fun getPopularItems(@Query("type") type: String): Deferred<Response<ServerResponse<List<SSItemModel>>>>

    @GET("shop/calculate-emi/{amount}/")
    fun getEmiAmount(@Path("amount") amount: Double): Deferred<Response<ServerResponse<Map<String, String>>>>

    @POST("/shop/buy-now/{id}")
    fun buyNowProduct(@Path("id") productId: String?,
        @Body itemModel: SSItemModel?): Deferred<Response<ServerResponse<IdRequestResponse>>>

    @GET("shop/home-screen")
    fun getSparesHomeResponse(@Query("config") config: String = "all"): Deferred<Response<ServerResponse<SparesHomeResponseModel>>>

    @GET("shop/gst-verification/")
    fun verifyGST(@Query("gst_num") gstNumber: String): Deferred<Response<ServerResponse<GSTVeriifyRespoonse>>>

    @GET("shop/pdp-marketplace-faq")
    fun getMarketplacePdpFAQ(): Deferred<Response<ServerResponse<List<FaqsItem>>>>

    @POST("shop/enquiry-now/")
    fun sendEnquiry(@Body enquiry: Enquiry): Deferred<Response<ServerResponse<Any>>>

    @GET("shop/enquiry-now/")
    fun getAllEnquiries(): Deferred<Response<ServerResponse<List<Enquiry>>>>

    @POST("shop/out-of-stock/")
    fun removeOOSItems(@Body req: JsonObject): Deferred<Response<ServerResponse<Any>>>

    @GET("shop/enquiry-items-config/")
    fun getEnquiryCost(): Deferred<Response<ServerResponse<Map<String, Int>>>>

    @GET("shop/estimated-delivery/")
    fun getEstimatedDelivery(@Query("pin_code") pinCode: String?): Deferred<Response<ServerResponse<EstimatedDeliveryResponse>>>

    @GET("shop/previously-ordered-items/")
    fun getPreviouslyAddedItem(): Deferred<Response<ServerResponse<List<SSItemModel>>>>

    @POST("shop/add-previously-ordered-items/")
    fun addPrevOrderedItemsToCart(@Body req: AddPrevOrderItemsReq): Deferred<Response<ServerResponse<AddPrevOrderItemsReq>>>

    @Multipart
    @POST("https://ai.gomechanic.app/v1/gm_ocr_img")
    fun getTextFromImage(@Part file: MultipartBody.Part?,
        @Part("token_id") token: RequestBody): Deferred<Response<ServerResponse<List<ImageToTextResponse>>>>

    @GET("gms/membership-config/")
    fun getMembershipConfig(): Deferred<Response<ServerResponse<MembershipPageConfig>>>

    @GET("users/buy-subscription/")
    fun buySubs(@Query("amount") amount: String?): Deferred<Response<ServerResponse<SubsResponse>>>

    @GET("business/workshops/")
    fun getWorkshop(@Query("id")workshopId: String): Deferred<Response<ServerResponse<List<WorkshopListResponseModel>>>>
}

