package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class BusinessCardDetails(@SerializedName("workshop_id") val workshopId: String = "",
                               @SerializedName("business_name") var businessName: String = "",
                               @SerializedName("address") var address: String = "",
                               @SerializedName("is_active") val isActive: Boolean = false,
                               @SerializedName("workshop_type") val workshopType: String = "",
                               @SerializedName("mobile") var mobile: String = "",
                               @SerializedName("created_at") val createdAt: String = "",
                               @SerializedName("created_by") val createdBy: Long = 0,
                               @SerializedName("updated_at") val updatedAt: String = "",
                               @SerializedName("name") var name: String = "",
                               @SerializedName("designation") var designation: String = "",
                               @SerializedName("id") val id: String = "",
                               @SerializedName("email") var email: String = "",
                               @SerializedName("image") var image: String? = "",
                               @SerializedName("color") var textColor: String? = "",
                               @SerializedName("icon") var businessCardIcons: BusinessCardIcons? = null,

):Parcelable