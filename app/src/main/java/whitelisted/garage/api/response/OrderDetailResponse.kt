package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class OrderDetailResponse(@SerializedName("bill_type") val billType: String,
    @SerializedName("car_details") val carDetails: CarDetail?,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("created_by_id") val createdById: Long,
    @SerializedName("current_action") val currentAction: CurrentAction,
    @SerializedName("customer_details") val customerDetails: CustomerDetail, //    @SerializedName("display_gst_in")
    //    val displayGstIn: String,
    @SerializedName("mechanic_id") val mechanicId: String,
    @SerializedName("mechanic_name") val mechanicName: String,
    @SerializedName("order_id") val orderId: String,
    @SerializedName("past_actions") val pastActions: List<PastActions>,
    @SerializedName("past_statuses") val pastStatuses: List<PastStatuses>,
    @SerializedName("payment_info") val paymentInfo: PaymentInfo,
    @SerializedName("payment_status") val paymentStatus: Any,
    @SerializedName("status_id") val statusId: Int,
    @SerializedName("status_name") val statusName: String,
    @SerializedName("tax_type") val taxType: String,
    @SerializedName("updated_at") val updatedAt: String,
    @SerializedName("updated_by_id") val updatedById: String,
    @SerializedName("workshop_id") val workshopId: String)

data class CarDetail(@SerializedName("brand_id") val brandId: Int? = null,
    @SerializedName("brand_name") val brand_name: String? = null,
    @SerializedName("car") val car: String? = null,
    @SerializedName("fuel_type") val fuelType: String? = null,
    @SerializedName("car_icon") val carIcon: String? = null,
    @SerializedName("car_type_id") val carTypeId: Int? = null,
    @SerializedName("mobile") val mobile: String? = null,
    @SerializedName("model_id") val modelId: Int? = null,
    @SerializedName("transmission") val transmission: Int? = null,
    @SerializedName("breaking_system") val breakingSystem: Int? = null,
    @SerializedName("odometer") val odometer: String? = null,
    @SerializedName("is_two_wheeler") val isTwoWheeler: Int = 4,
    @SerializedName("registration_number") val registrationNumber: String? = null,
    @SerializedName("user_id") val userId: String? = null)

data class CurrentAction(@SerializedName("action_remark") val actionRemark: String,
    @SerializedName("car_details") val carDetails: CarDetail,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("created_by_id") val createdById: String,
    @SerializedName("customer_details") val customerDetails: CustomerDetails,
    @SerializedName("order_id") val orderId: String,
    @SerializedName("order_oid") val orderOid: String,
    @SerializedName("status_id") val statusId: Int,
    @SerializedName("status_name") val statusName: String,
    @SerializedName("updated_at") val updatedAt: String,
    @SerializedName("updated_by_id") val updatedById: String,
    @SerializedName("workshop_id") val workshopId: String)


data class PastActions(@SerializedName("action_remark") val actionRemark: String,
    @SerializedName("car_details") val carDetails: CarDetail,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("created_by_id") val createdById: Long,
    @SerializedName("customer_details") val customerDetails: CustomerDetails,
    @SerializedName("order_id") val orderId: String,
    @SerializedName("order_oid") val orderOid: String,
    @SerializedName("status_id") val statusId: Int,
    @SerializedName("status_name") val statusName: String,
    @SerializedName("updated_at") val updatedAt: String,
    @SerializedName("updated_by_id") val updatedById: Long,
    @SerializedName("workshop_id") val workshopId: String)

data class PastStatuses(@SerializedName("status_id") val statusId: Int,
    @SerializedName("status_name") val statusName: String,
    @SerializedName("updated_at") val updatedAt: String,
    @SerializedName("updated_by_id") val updatedById: Long)

data class PaymentInfo(@SerializedName("bill_amount") val billAmount: Double,
    @SerializedName("due_amount") val dueAmount: Double,
    @SerializedName("paid_amount") val paidAmount: Double,
    @SerializedName("discount_value") val discountValue: Double)


data class CustomerDetail(@SerializedName("address") val address: String,
    @SerializedName("email") val email: String?,
    @SerializedName("mobile") val mobile: String,
    @SerializedName("name") val name: String)


