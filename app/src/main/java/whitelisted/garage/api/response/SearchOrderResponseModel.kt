package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchOrderResponseModel(@SerializedName("bill_type") val billType: String?,
    @SerializedName("car_details") val carDetails: CarDetails?,
    @SerializedName("created_at") val createdAt: String?,
    @SerializedName("created_by_id") val createdById: Long?,
    @SerializedName("customer_details") val customerDetails: CustomerDetails?, //    @SerializedName("display_gst_in")
    //    val displayGstIn: String?,
    @SerializedName("mechanic_id") val mechanicId: String?,
    @SerializedName("mechanic_name") val mechanicName: String?,
    @SerializedName("order_id") val orderId: String?,
    @SerializedName("payment_info") val paymentInfo: PaymentInfoObj?,
    @SerializedName("payment_status") val paymentStatus: String?,
    @SerializedName("status_id") val statusId: Int?,
    @SerializedName("status_name") val statusName: String?,
    @SerializedName("tax_type") val taxType: String?,
    @SerializedName("updated_at") val updatedAt: String?,
    @SerializedName("updated_by_id") val updatedById: Long?,
    @SerializedName("workshop_id") val workshopId: String?,
    @SerializedName("bill_name") val bill_name: String? = null,
    @SerializedName("is_two_wheeler") val isTwoWheeler: Int = 4,
    @SerializedName("workshop_name") val workshopName: String?) : Parcelable

@Parcelize
data class CarDetails(@SerializedName("brand_id") val brandId: Int?,
    @SerializedName("car") val car: String?,
    @SerializedName("car_type_id") val carTypeId: Int?,
    @SerializedName("mobile") val mobile: String?,
    @SerializedName("car_icon") val car_icon: String?,
    @SerializedName("model_id") val modelId: Int?,
    @SerializedName("brand_name") var brandName: String? = "",
    @SerializedName("fuel_type") var fuelType: String? = "",
    @SerializedName("odometer") val odometer: Long?,
    @SerializedName("registration_number") val registrationNumber: String?,
    @SerializedName("user_id") val userId: Long?) : Parcelable

@Parcelize
data class CustomerDetails(@SerializedName("address") val address: String?,
    @SerializedName("email") val email: String?,
    @SerializedName("user_id") val userId: String?,
    @SerializedName("mobile") val mobile: String?,
    @SerializedName("save") val save: Boolean? = false,
    @SerializedName("name") val name: String?,
    @SerializedName("country_code") val country_code: String?) : Parcelable

@Parcelize
data class PaymentInfoObj(@SerializedName("bill_amount") val bill_amount: Double? = null,
    @SerializedName("paid_amount") val paid_amount: Double? = null,
    @SerializedName("discount_value") val discount_value: Double? = null,
    @SerializedName("due_amount") val due_amount: Double? = null) : Parcelable