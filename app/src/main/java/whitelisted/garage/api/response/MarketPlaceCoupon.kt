package whitelisted.garage.api.response


import com.google.gson.annotations.SerializedName

data class MarketPlaceCoupon(@SerializedName("image")
                             val image: String = "",
                             @SerializedName("valid_upto")
                             val validUpto: Long = 0,
                             @SerializedName("is_active")
                             val isActive: Boolean = false,
                             @SerializedName("coupon")
                             val coupon: String = "",
                             @SerializedName("workshop_type")
                             val workshopType: String = "",
                             @SerializedName("is_timer")
                             val isTimer: Boolean = false,
                             @SerializedName("created_at")
                             val createdAt: String = "",
                             @SerializedName("text")
                             val text: String = "",
                             @SerializedName("category")
                             val category: String = "",
                             @SerializedName("value")
                             val value: Int = 0)


