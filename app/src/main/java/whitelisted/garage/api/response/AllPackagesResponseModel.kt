package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class AllPackagesResponseModel(

    @field:SerializedName("workshop_id") val workshopId: String? = null,

    @field:SerializedName("total") val total: Double? = null,

    @field:SerializedName("total_services") val totalServices: Int? = null,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("items_count") val items_count: Int? = null,

    @field:SerializedName("updated_by") val updatedBy: Long? = null,

    @field:SerializedName("description") val description: String? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("is_custom") val isCustom: Boolean? = false,

    @field:SerializedName("is_two_wheeler") val isTwoWheeler: Boolean? = false,

    @field:SerializedName("package_id") val packageId: Long? = null,

    @field:SerializedName("created_by") val createdBy: Long? = null,

    // accessories
    @field:SerializedName("id") val id: String? = null,

    )
