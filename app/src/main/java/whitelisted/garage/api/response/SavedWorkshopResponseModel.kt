package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class SavedWorkshopResponseModel(

    @field:SerializedName("workshop_id") val workshopId: String? = null,

    @field:SerializedName("address") val address: String? = null,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("mobile") val mobile: String? = null,

    @field:SerializedName("name") var name: String? = null,

    @field:SerializedName("save") val save: Boolean? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("updated_by_id") val updatedById: Long? = null,

    @field:SerializedName("created_by_id") val createdById: Long? = null,

    @field:SerializedName("last_service_date") val lastServiceDate: String? = null,

    @field:SerializedName("workshop_visits") val workshopVisits: Int? = null,

    @field:SerializedName("email") val email: String? = null)
