package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class RemindersListResponse(

    @field:SerializedName("reminders") val reminders: List<RemindersItem>? = null,
    @field:SerializedName("reminder_restriction") val reminder_restriction: String? = null)

data class RemindersItem(

    @field:SerializedName("workshop_id") val workshopId: String? = null,

    @field:SerializedName("car_name") val carName: String? = null,

    @field:SerializedName("car_type_id") val carTypeId: Long? = null,

    @field:SerializedName("latest_reminder") val latestReminder: String? = null,

    @field:SerializedName("sent_by") val sentBy: Long? = null,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("reminder_sent_from_workshop") val reminderSentFromWorkshop: String? = null,

    @field:SerializedName("mobile") val mobile: String? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,
    @field:SerializedName("scheduled_at") val scheduled_at: String? = null,

    @field:SerializedName("customer_name") val customerName: String? = null,
    @field:SerializedName("car_icon") val car_icon: String? = null,

    @field:SerializedName("order_id") val orderId: String? = null,

    @field:SerializedName("registration_no") val registrationNo: String? = null)
