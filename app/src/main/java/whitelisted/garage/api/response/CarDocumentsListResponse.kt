package whitelisted.garage.api.response

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "carDocsList")
data class CarDocumentsListResponse(@PrimaryKey(autoGenerate = true) val list: List<String>? = null)
