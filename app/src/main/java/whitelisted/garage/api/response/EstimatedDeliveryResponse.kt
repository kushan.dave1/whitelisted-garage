package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class EstimatedDeliveryResponse(

	@field:SerializedName("address_visible")
	val addressVisible: Boolean? = null,

	@field:SerializedName("pincode")
	val pincode: String? = null,

	@field:SerializedName("city_name")
	val cityName: String? = null,

	@field:SerializedName("estimated_delivery_days")
	val estimatedDeliveryDays: Int? = null,

	@field:SerializedName("has_reverse")
	val hasReverse: Int? = null,

	@field:SerializedName("provider")
	val provider: String? = null,

	@field:SerializedName("has_prepaid")
	val hasPrepaid: Int? = null,

	@field:SerializedName("routing_code")
	val routingCode: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("estimated_delivery")
	val estimatedDelivery: String? = null,

	@field:SerializedName("has_cod")
	val hasCod: Int? = null
)
