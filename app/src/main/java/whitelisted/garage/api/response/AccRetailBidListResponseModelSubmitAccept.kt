package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class AccRetailBidListResponseModelSubmitAccept(

    @field:SerializedName("workshop_id") val workshopId: String? = null,

    @field:SerializedName("total_item") val totalItem: Int? = null,

    @field:SerializedName("workshop_name") val workshopName: String? = null,

    @field:SerializedName("contact_number") val contactNumber: String? = null,

    @field:SerializedName("status_id") val statusId: Int? = null,

    @field:SerializedName("distance") val distance: Double? = 0.0,

    @field:SerializedName("total_amount") val totalAmount: Int? = null,

    @field:SerializedName("latitude") val latitude: Double? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("id") val id: String? = null,

    @field:SerializedName("bid_id") val bidId: String? = null,

    @field:SerializedName("is_contact_number") val isContactNumber: List<String>? = listOf(),

    @field:SerializedName("is_items_text_expanded") var isItemTextExpanded: Boolean? = false,

    @field:SerializedName("items") val items: MutableList<String?>? = null,

    @field:SerializedName("status") val status: String? = null,

    @field:SerializedName("remark") val remarks: String? = null,

    @field:SerializedName("longitude") val longitude: Double? = null,

    @field:SerializedName("is_active") val isActive: Boolean? = true,

    @field:SerializedName("biding_cart") val bidingCart: BidingCart? = BidingCart())

data class BidingCart(@field:SerializedName("workshop_id") val workshopId: String? = null,
    @field:SerializedName("workshop_name") val workshopName: String? = null,
    @field:SerializedName("contact_number") val contactNumber: String? = null,
    @field:SerializedName("is_contact_number") val isContactNumber: List<String>? = listOf(),
    @field:SerializedName("latitude") val latitude: Double? = null,
    @field:SerializedName("longitude") val longitude: Double? = null)