package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class BottomTabsResponse(

    @field:SerializedName("navigate") val navigate: List<NavigateItem?>? = null)

data class NavigateItem(

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("icon") val icon: String? = null)
