package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class ImageToTextResponse (
    @SerializedName("text"            ) var text           : String?           = null,
    @SerializedName("prob_percentage" ) var probPercentage : Double?           = null
)