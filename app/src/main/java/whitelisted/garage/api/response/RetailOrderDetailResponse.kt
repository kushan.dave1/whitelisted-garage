package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class RetailOrderDetailResponse(

    @field:SerializedName("workshop_id") val workshopId: String? = null,

    @field:SerializedName("mechanic_id") val mechanicId: String? = null,

    @field:SerializedName("mechanic_name") val mechanicName: String? = null,

    @field:SerializedName("city") val city: String? = null,

    @field:SerializedName("status_name") val statusName: String? = null,

    @field:SerializedName("payment_status") val paymentStatus: String? = null,

    @field:SerializedName("display_gst_in") val displayGstIn: String? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("bill_name") val billName: String? = null,

    @field:SerializedName("gstin") val gstin: String? = null,

    @field:SerializedName("car_details") val carDetails: String? = null,

    @field:SerializedName("customer_details") val customerDetails: CustomerDetails? = null,

    @field:SerializedName("status_id") val statusId: Int? = null,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("tax_type") val taxType: String? = null,

    @field:SerializedName("paid") val paid: Boolean? = null,

    @field:SerializedName("bill_type") val billType: String? = null,

    @field:SerializedName("invoice_id") val invoiceId: String? = null,

    @field:SerializedName("updated_by_id") val updatedById: Long? = null,

    @field:SerializedName("created_by_id") val createdById: Long? = null,

    @field:SerializedName("payment_info") val paymentInfo: PaymentInfo? = null,

    @field:SerializedName("order_id") val orderId: String? = null,

    @field:SerializedName("delivered_at") val deliveredAt: String? = null,

    @field:SerializedName("order_type") val orderType: Int? = null)
