package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class MostUsedPartsResponseModel(

    @field:SerializedName("selling_price") val sellingPrice: Double? = null,

    @field:SerializedName("buying_price") val buyingPrice: Double? = null,

    @field:SerializedName("category_name") val categoryName: String? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("is_private_label") val isPrivateLabel: Boolean? = null,

    @field:SerializedName("sub_category_name") val subCategoryName: String? = null,

    @field:SerializedName("brand") var brand: String? = null,

    @field:SerializedName("quantity") var quantity: String? = null,

    @field:SerializedName("rack_name") var rackName: String? = null,

    @field:SerializedName("is_selected") var isSelected: Boolean = false,

    @field:SerializedName("sku_id") val skuId: String? = null,

    @field:SerializedName("id") val id: Long? = null,

    @field:SerializedName("service_type_id") val serviceTypeId: Long? = null)
