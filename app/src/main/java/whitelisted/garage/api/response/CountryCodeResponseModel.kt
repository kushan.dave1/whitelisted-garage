package whitelisted.garage.api.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class CountryCodeResponseModel(

    @field:SerializedName("image") val image: String? = null,

    @field:SerializedName("code") val code: String? = null,

    @field:SerializedName("emoji") val emoji: String? = null,

    @field:SerializedName("dial_code") val dialCode: String? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("unicode") val unicode: String? = null,

    @field:SerializedName("currency") val currency: String? = null,

    @field:SerializedName("currency_symbol") val currencySymbol: String? = null,

    @field:SerializedName("id") val id: String? = null,

    @field:SerializedName("isSelected") var isSelected: Boolean? = false) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(image)
        parcel.writeString(code)
        parcel.writeString(emoji)
        parcel.writeString(dialCode)
        parcel.writeString(name)
        parcel.writeString(unicode)
        parcel.writeString(currency)
        parcel.writeString(currencySymbol)
        parcel.writeString(id)
        parcel.writeValue(isSelected)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CountryCodeResponseModel> {
        override fun createFromParcel(parcel: Parcel): CountryCodeResponseModel {
            return CountryCodeResponseModel(parcel)
        }

        override fun newArray(size: Int): Array<CountryCodeResponseModel?> {
            return arrayOfNulls(size)
        }
    }
}