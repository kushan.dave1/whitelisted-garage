package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GetInventoryPartItemResponse( //    @SerializedName("category_name") var categoryName: String? = "",
    @SerializedName("is_private_label") var isPrivateLabel: Boolean? = false,
    @SerializedName("sku_id") var skuId: String? = "",
    @SerializedName("selling_price") var sellingPrice: Double? = 0.0,
    @SerializedName("workshop_id") var workshopId: String? = null,
    @SerializedName("status") var status:Int? = null,
    @SerializedName("sub_category_name") var subCategoryName: String? = "")