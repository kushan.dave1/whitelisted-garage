package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class AddPrevOrderItemsReq(
    @field:SerializedName("items") val list: List<SSItemModel>? = null,
)