package whitelisted.garage.api.response

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class BidDetailResponse(

    @field:SerializedName("workshop_id") var workshopId: String? = "",

    @field:SerializedName("total_item") var totalItem: Int? = 0,

    @field:SerializedName("is_accepted") var isAccepted: Boolean? = false,

    @field:SerializedName("is_active") var isActive: Boolean? = false,

    @field:SerializedName("is_contact_number") val isContactNumber: List<String>? = listOf(),

    @field:SerializedName("bid_created") var bidCreated: Boolean? = false,

    @field:SerializedName("distance") var distance: JsonObject? = null,

    @field:SerializedName("workshop_type") var workshopType: String? = "",

    @field:SerializedName("latitude") var latitude: Double? = 0.0,

    @field:SerializedName("created_at") var createdAt: String? = "",

    @field:SerializedName("brand_wise_items") var brandWiseItems: MutableList<BidItem>? = mutableListOf(),

    @field:SerializedName("created_by") var createdBy: Long? = 0,

    @field:SerializedName("contact_number") var contactNumber: String? = "",

    @field:SerializedName("workshop_name") var workshopName: String? = "",

    @field:SerializedName("status_id") var statusId: Int? = 0,

    @field:SerializedName("total_amount") var totalAmount: Double? = 0.0,

    @field:SerializedName("id") var id: String? = "",

    @field:SerializedName("generic_items") var genericItems: MutableList<BidItem>? = mutableListOf(),

    @field:SerializedName("items") var items: MutableList<String>? = mutableListOf(),

    @field:SerializedName("status") var status: String? = "",

    @field:SerializedName("longitude") var longitude: Double? = 0.0)

data class BidItem(

    @field:SerializedName("selected_qty") var selectedQty: Int? = 0,

    @field:SerializedName("quantity_available") var quantityAvailable: Int? = 0,

    @field:SerializedName("selling_price") var sellingPrice: Double? = 0.0,

    @field:SerializedName("price_per_item") var pricePerItem: Double? = 0.0,

    @field:SerializedName("car_model") var carModel: String? = "",

    @field:SerializedName("images") var images: MutableList<String>? = mutableListOf(),

    @field:SerializedName("added_images") var addedImages: MutableList<String>? = mutableListOf(),

    @field:SerializedName("quantity") var quantity: Int? = 1,

    @field:SerializedName("buying_price") var buyingPrice: Double? = 0.0,

    @field:SerializedName("brand_name") var brandName: String? = "",

    @field:SerializedName("sku_id") var skuId: Int? = 0,

    @field:SerializedName("mf_year") var mfYear: String? = "",

    @field:SerializedName("brand_id") var brandId: Int? = 0,

    @field:SerializedName("car_model_id") var carModelId: Int? = 0,

    @field:SerializedName("part_availability") var partAvailability: Int? = 0,

    @field:SerializedName("remark") val remarks: String? = null,

    @field:SerializedName("fuel_type") var fuelType: String? = "",

    @field:SerializedName("part_name") var partName: String? = "",

    @field:SerializedName("is_selected") var isSelected: Boolean? = false,

    @field:SerializedName("is_available") var isAvailable: Boolean? = false,

    @field:SerializedName("is_rejected") var isRejected: Boolean? = false,

    @field:SerializedName("is_generic") var isGeneric: Boolean? = false,

    @field:SerializedName("is_media_uploaded") var isMediaUploaded: Boolean? = false,

    @field:SerializedName("is_images_expanded") var isImagesExpanded: Boolean? = false)

