package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class CartIdResponse(
    @field:SerializedName("cart_id") var cartId: String? = null
)
