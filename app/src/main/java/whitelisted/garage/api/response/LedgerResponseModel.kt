package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

class LedgerResponseModel(@field:SerializedName("ledger") val ledger: List<Ledger>? = null,
    @field:SerializedName("creadit") val creadit: Double? = null,
    @field:SerializedName("debit") val debit: Double? = null,
    @field:SerializedName("due") val due: Double? = null,
    @field:SerializedName("credit_upper") val credit_upper: Long? = null,
    @field:SerializedName("debit_upper") val debit_upper: Long? = null,
    @field:SerializedName("due_upper") val due_upper: Long? = null)
