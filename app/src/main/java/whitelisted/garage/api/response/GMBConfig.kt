package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class GMBConfig(@SerializedName("multi_image") var multiImage: MutableList<MultiImage>? = null,
    @SerializedName("single_image") var singleImage: MutableList<SingleImage>? = null,
    @SerializedName("header") var header: String? = null,
    @SerializedName("preview") var preview: Preview? = null,
    @SerializedName("website_benefits_multi_image") var website_benefits_multi_image: MutableList<String>? = null

) : Parcelable

@Parcelize
data class MultiImage(@SerializedName("header") var header: String? = null,
    @SerializedName("sub_header") var subHeader: String? = null,
    @SerializedName("gocoins") var gocoins: String? = null,
    @SerializedName("images") var images: @RawValue Any? = null

) : Parcelable

@Parcelize
data class SingleImage(@SerializedName("description") var description: String? = null,
    @SerializedName("header") var header: String? = null,
    @SerializedName("icon") var icon: String? = null,
    @SerializedName("orientation") var orientation: String? = null,
    var viewType: Int? = null) : Parcelable

@Parcelize
data class Preview(
    @SerializedName("single") var single: String? = null,
    @SerializedName("multi_image_preview") var previesImages: MutableList<String>? = null,
) : Parcelable