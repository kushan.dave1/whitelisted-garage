package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class OrderDetailWrapperPostUpdateResponse(

    @field:SerializedName("status_id") val statusId: Int? = null,

    @field:SerializedName("status_name") val statusName: String? = null,

    @field:SerializedName("order_id") val orderId: String? = null)
