package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class WorkshopIDResponse(

    @field:SerializedName("workshop_id") val workshopId: String? = null)
