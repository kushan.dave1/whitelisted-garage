package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

class HomeCarouselResponse(

    @field:SerializedName("data") val data: List<CarouselDataItem>? = null,
    @field:SerializedName("background") val background: String? = null

)


data class CarouselDataItem(

    @field:SerializedName("carousel_image") val carouselImage: String? = null,

    @field:SerializedName("redirection") val redirection: String? = null)
