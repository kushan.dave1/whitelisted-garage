package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class HomeScreenResponse(

    @field:SerializedName("open_order") val openOrder: OpenOrder? = null,

    @field:SerializedName("complete_order") val completeOrder: CompleteOrder? = null,

    @field:SerializedName("ongoing_orders") val ongoingOrder: OngoingOrder? = null,

    @field:SerializedName("payment") val payment: Payment? = null,

    @field:SerializedName("workshop") val workshop: Workshop? = null,
    @field:SerializedName("wip_order") val wipOrder: WipOrder? = null,

    @field:SerializedName("is_new_account") val isNewAccount: Boolean? = false,

    @field:SerializedName("ready_for_delivery") val readyForDelivery: ReadyForDelivery? = null,

    @field:SerializedName("employee") val employee: Int? = 0)

data class BiddingSummary(
    @field:SerializedName("approved_bids") val approvedBids: Long? = 0,
    @field:SerializedName("pending_bids") val pendingBids: Long? = 0,
    @field:SerializedName("total_bids") val totalBids: Long? = 0,
    @field:SerializedName("new_bids") val newBids: Long? = 0,
    @field:SerializedName("accepted_bids") val acceptedBids: Long? = 0,
)

data class Payment(@field:SerializedName("Total") val total: Double? = 0.0,
    @field:SerializedName("Pending") val pending: Double? = 0.0,
    @field:SerializedName("Paid") val paid: Double? = 0.0)

data class DataItem(

    @field:SerializedName("registration_number") val registrationNumber: String? = null,

    @field:SerializedName("status_name") val statusName: String? = null,

    @field:SerializedName("payment_status") val paymentStatus: String? = null,

    @field:SerializedName("mobile") val mobile: String? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("car_name") val carName: String? = null,

    @field:SerializedName("status_id") val statusId: Int? = null,

    @field:SerializedName("invoice_amount") val invoiceAmount: Double? = null,

    @field:SerializedName("paid_amount") val paidAmount: Double? = null,

    @field:SerializedName("due_amount") val dueAmount: Double? = null,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("car_image") val carImage: String? = null,

    @field:SerializedName("car_icon") val carIcon: String? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("invoice_id") val invoiceId: String? = null,

    @field:SerializedName("fuel_type") val fuelType: String? = null,

    @field:SerializedName("order_id") val orderId: String? = null,

    @field:SerializedName("car_id") val carId: String? = null,

    @field:SerializedName("email") val email: String? = null)

data class WipOrder(

    @field:SerializedName("data") val data: List<DataItem?>? = null,

    @field:SerializedName("count") val count: Int? = 0)

data class Workshop(

    @field:SerializedName("address") val address: String? = null,

    @field:SerializedName("city") val city: String? = null,

    @field:SerializedName("owner_name") val ownerName: String? = null,

    @field:SerializedName("owner_id") val ownerId: Long? = null,

    @field:SerializedName("employee_ids") val employeeIds: List<Long?>? = null,

    @field:SerializedName("updated_from_login_workshop") val updatedFromLoginWorkshop: String? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("gstin") val gstin: String? = null,

    @field:SerializedName("created_by") val createdBy: Long? = null,

    @field:SerializedName("invoice_name") val invoiceName: String? = null,

    @field:SerializedName("gocoin_balance") val gocoinBalance: Long? = 0,

    @field:SerializedName("biding_summary") val biddingSummary: BiddingSummary? = null,

    @field:SerializedName("gocoin_total") val gocoinTotal: Long? = 0,

    @field:SerializedName("gocoins_usage") val gocoinUsage: Long? = 0,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("map_link") val mapLink: String? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("updated_by") val updatedBy: Long? = null,

    @field:SerializedName("invoice_id") val invoiceId: String? = null,

    @field:SerializedName("id") val id: String? = null,
    @field:SerializedName("membership_status") val membershipStatus: Boolean? = false,
    @field:SerializedName("gocoin_unlocked_features") val gocoin_unlocked_features: GOCoinUnloackFeature? = null)

data class OpenOrder(

    @field:SerializedName("data") val data: List<DataItem?>? = null,

    @field:SerializedName("count") val count: Int? = 0)

data class GOCoinUnloackFeature(

    @field:SerializedName("workshop") val data: List<String?>? = null,
)

data class OngoingOrder(

    @field:SerializedName("data") val data: List<DataItem>? = null,

    @field:SerializedName("count") val count: Int? = 0)

data class ReadyForDelivery(

    @field:SerializedName("data") val data: List<DataItem?>? = null,

    @field:SerializedName("count") val count: Int? = 0)

data class CompleteOrder(

    @field:SerializedName("data") val data: List<DataItem?>? = null,

    @field:SerializedName("count") val count: Int? = 0)
