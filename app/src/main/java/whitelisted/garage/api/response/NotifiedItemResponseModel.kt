package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class NotifiedItemResponseModel(

	@field:SerializedName("finger_print")
	val fingerPrint: String? = null,

	@field:SerializedName("is_active")
	val isActive: Boolean? = null,

	@field:SerializedName("product_id")
	val productId: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("created_at_by")
	val createdAtBy: Long? = null
)
