package whitelisted.garage.api.response


import com.google.gson.annotations.SerializedName

data class BannerImage(@SerializedName("image")
                   val image: String = "",
                   @SerializedName("rediract")
                   val rediract: String = "",
                   @SerializedName("type:")
                   val type: String = "")


data class PDPBannerResponse(@SerializedName("image") val bannerImage: BannerImage? = null)


