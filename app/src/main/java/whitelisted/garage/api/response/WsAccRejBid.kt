package whitelisted.garage.api.response

data class WsAccRejBid(var bid_status: Int? = null)
