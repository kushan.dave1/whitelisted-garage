package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GetSSCartResponse(

    @field:SerializedName("payment_limit") val paymentLimit: Double? = null,

    @field:SerializedName("isDelivery") val isDelivery: Boolean? = null,

    @field:SerializedName("address_visible") val addressVisible: Boolean? = false,

    @field:SerializedName("acc_shipping_charges") val accShippingCharges: Double? = null,

    @field:SerializedName("upfront_percent") val upfrontPercent: Int? = null,

    @field:SerializedName("spares_shipping_charges") val sparesShippingCharges: Double? = null,

    @field:SerializedName("min_emi_amount") val minEmiAmount: Double? = 1000.0,

    @field:SerializedName("cart") val cartList: List<Cart> = mutableListOf(),

    @field:SerializedName("min_item") val minItem: Int? = null,

    @field:SerializedName("pro_discount_text") val proDiscountText: String? = "",

    @field:SerializedName("pro_discount_percent") val proDiscountPercent: Int? = 0,

    @field:SerializedName("basic_discount_text") val basicDiscountText: String? = "",

    @field:SerializedName("gocoin_discount") var gocoinDiscount: Double? = 0.0

)

data class Cart(

    @field:SerializedName("ordered") val ordered: Boolean? = null,

    @field:SerializedName("discount") val discount: Double? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("estimated_delivery") val estimatedDelivery: EstimatedDeliveryResponse? = null,

    @field:SerializedName("line_items") val lineItems: MutableList<SSItemModel> = mutableListOf(),

    @field:SerializedName("no_of_items") val noOfItems: Int? = null,

    @field:SerializedName("gom_price_total_amount") val gomPriceTotalAmount: Double? = null,

    @field:SerializedName("gocoins_discount") val goCoinsDiscount: Double? = null,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("total_amount") val totalAmount: Double? = null,

    @field:SerializedName("updated_by_id") val updatedById: Long? = null,

    @field:SerializedName("id") val id: String = "",

    @field:SerializedName("bulk_discount_rate") var bulkDiscountRate: Double? = 0.0,

    @field:SerializedName("bulk_order_quantity") var bulkOrderQuantity: Int? = 0,

    @field:SerializedName("created_by_id") val createdById: Long? = null,

    @field:SerializedName("order_id") val orderId: String? = null,

    @field:SerializedName("discount_coupon") val discountCoupon: String? = null,

    @field:SerializedName("shipping_charges") val shippingCharges: Double? = null,

    @field:SerializedName("discounted_amount") var discountedAmount: Double = 0.0)
