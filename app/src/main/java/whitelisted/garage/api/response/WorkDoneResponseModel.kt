package whitelisted.garage.api.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

data class WorkDoneResponseModel(

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("id") val id: Int? = null,

    @field:SerializedName("is_labour") val isLabour: Int? = null,

    @field:SerializedName("desc") val desc: String? = null,

    var isSelected: Boolean = false) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readByte() != 0.toByte())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeValue(id)
        parcel.writeValue(isLabour)
        parcel.writeString(desc)
        parcel.writeByte(if (isSelected) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WorkDoneResponseModel> {
        override fun createFromParcel(parcel: Parcel): WorkDoneResponseModel {
            return WorkDoneResponseModel(parcel)
        }

        override fun newArray(size: Int): Array<WorkDoneResponseModel?> {
            return arrayOfNulls(size)
        }
    }
}
