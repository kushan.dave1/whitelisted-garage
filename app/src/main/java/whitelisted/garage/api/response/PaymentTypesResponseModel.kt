package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class PaymentTypesResponseModel(

    @field:SerializedName("is_enabled") val isEnabled: Boolean? = null,

    @field:SerializedName("enum_name") val enumName: String? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("enum_number") val enumNumber: Int? = null,

    @field:SerializedName("cod_otp") val codOtp: Double? = null,

    @field:SerializedName("options") val options: List<OptionsItem>? = null,

    @field:SerializedName("text") val text: String? = null,

    @field:SerializedName("source") val source: String? = null,

    @field:SerializedName("extra_cb") val extraCashback: ExtraCashback? = null,

    @field:SerializedName("id") val id: Int? = null)

data class OptionsItem(

    @field:SerializedName("is_enabled") val isEnabled: Boolean? = null,

    @field:SerializedName("is_selected") var isSelected: Boolean = false,

    @field:SerializedName("enum_name") val enumName: String? = null,

    @field:SerializedName("image_name") val imageName: String? = null,

    @field:SerializedName("enum_sub_gateway") val enumSubGateway: String? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("package_name") val packageName: String? = null,

    @field:SerializedName("enum_mode") val enumMode: String? = null,

    @field:SerializedName("enum_method") val enumMethod: String? = null,

    @field:SerializedName("enum_gateway") val enumGateway: String? = null,

    @field:SerializedName("enum_number") val enumNumber: Int? = null,

    @field:SerializedName("discount_text") val discountText: String? = null,

    @field:SerializedName("offer_details") val offerDetails: OfferDetails? = null,

    @field:SerializedName("offers") val offers: List<Offer>? = null,

    @field:SerializedName("is_offer") val isOffer: Boolean? = null)

data class ExtraCashback(

    @field:SerializedName("heading") val heading: String? = null,

    @field:SerializedName("bg_image") val bgImage: String? = null,

    @field:SerializedName("icon") val icon: String? = null,

    @field:SerializedName("sub_heading") val subHeading: String? = null)

data class OfferDetails(

    @field:SerializedName("offer_text") val offerText: String? = null,

    @field:SerializedName("offer_title") val offerTitle: String? = null)

data class Offer(@SerializedName("is_visible") val isVisible: Boolean? = null,
    @SerializedName("title") val text: String? = null,
    @SerializedName("code") val code: String? = null,
    @SerializedName("type") val type: String? = null,
    @SerializedName("max_discount_amount") val maxDiscountAmount: Int? = null,
    @SerializedName("percentage") val percentage: Int? = null,
    @SerializedName("details") val details: ArrayList<String> = arrayListOf(),
    var name: String? = "",
    var icon: String? = "")
