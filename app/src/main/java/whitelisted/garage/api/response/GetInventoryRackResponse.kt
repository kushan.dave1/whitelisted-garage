package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import whitelisted.garage.api.request.RackItem


@Parcelize
data class GetInventoryRackResponse(@SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("id") var id: String? = null,
    @SerializedName("images") var images: MutableList<String>? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("updated_at") var updatedAt: String? = null,
    @SerializedName("workshop_id") var workshopId: String? = null,
    var isNewRack: Boolean? = null,
    @SerializedName("items_count") var items_count: Int? = 0,
    @SerializedName("rack_items") var rack_items: MutableList<RackItem>? = null) : Parcelable