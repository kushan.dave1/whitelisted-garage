package whitelisted.garage.api.response

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "workshops")
data class WorkshopListResponseModel(

    @PrimaryKey(autoGenerate = true) var dbId: Int = 0,

    @field:SerializedName("is_subscription_active") var isSubscriptionActive: Boolean? = null,

    @field:SerializedName("is_locked") var isLocked: Boolean? = false,

    @field:SerializedName("membership_status") var membershipStatus: Boolean? = null,

    @field:SerializedName("membership_status_code") var membershipStatusCode: String? = null,

    @field:SerializedName("membership_end_at") var membershipEndAt: String? = null,

    @field:SerializedName("id") var workshopId: String? = null,

    @field:SerializedName("invoice_id") var invoiceName: String? = null,

    @field:SerializedName("address") var address: String? = null,

    @field:SerializedName("city") var city: String? = "",

    @field:SerializedName("pin") var pin: String? = "",

    @field:SerializedName("state") var state: String? = "",

    @field:SerializedName("owner_name") var ownerName: String? = null,

    @field:SerializedName("updated_at") var updatedAt: String? = null,

    @field:SerializedName("map_link") var mapLink: String? = null,

    @field:SerializedName("owner_id") var ownerId: Long? = null,

    @field:SerializedName("employee_ids") var employeeIds: List<Long>? = null,

    @field:SerializedName("types") var types: String? = null,

    @field:SerializedName("workshop_role") var workshopRole: String? = null,

    @field:SerializedName("shop_type") var shopType: String? = "",

    @field:SerializedName("name") var name: String? = null,

    @field:SerializedName("created_at") var createdAt: String? = null,

    @field:SerializedName("updated_from_login_workshop") var updatedFromLoginWorkshop: String? = null,

    @field:SerializedName("gstin") var gstin: String? = null,

    @SerializedName("latitude") var latitude: Double? = null,

    @SerializedName("longitude") var longitude: Double? = null,

    @Ignore
    @SerializedName("gocoin_unlocked_features") var gocoinUnlockedFeatures: GocoinUnlockedFeatures? = null,

    @Ignore
    @SerializedName("open_time") var openTime: Any? = null,

    @Ignore
    @SerializedName("gmb_links") var gmbLinks: Any? = null) : Serializable

data class GocoinUnlockedFeatures(
    @field:SerializedName("workshop") var workshop: List<String>? = listOf()
): Serializable
