package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SSBrandsResponseModel(

	@field:SerializedName("icon")
	val icon: String? = null,

	@field:SerializedName("background")
	val background: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("brand_name")
	val brandName: String? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("sku_brand")
	val skuBrand: String? = null,

	@field:SerializedName("top_banner")
	val topBanner: String? = null,

	@field:SerializedName("bottom_banner")
	val bottomBanner: String? = null,

	@field:SerializedName("banners")
    val banners: MutableList<PlpBannerObject>? = mutableListOf(),

	@field:SerializedName("is_selected")
	var isSelected: Boolean = false
):Parcelable
