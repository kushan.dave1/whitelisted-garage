package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SSHomeBannersResponseModel(

    @field:SerializedName("image") val image: String? = null,

    @field:SerializedName("brand_name") val brandName: String? = null,

    @field:SerializedName("top_banner") val topBanner: String? = null,

    @field:SerializedName("banners") val banners: MutableList<PlpBannerObject>? = mutableListOf(),

    @field:SerializedName("bottom_banner") val bottomBanner: String? = null):Parcelable
