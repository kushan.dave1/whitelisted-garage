package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName


data class GetGoCoinBalanceResponse(@SerializedName("gocoin_balance") val gocoinBalance: Int? = null,
    @SerializedName("gocoin_low_balance") val gocoinLowBalance: Int? = null,
    @SerializedName("gocoin_total") val gocoinTotal: Int? = null)