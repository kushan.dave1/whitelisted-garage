package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GetGMDResponse(@SerializedName("address") var address: String? = null,
    @SerializedName("gmb_mobile") var gmb_mobile: String? = null,
    @SerializedName("gmb_links") var gmb_links: GMBLinks? = null,
    @SerializedName("gmb_price") var gmbPrice: String? = null,
    @SerializedName("gocoin_balance") var goCoinsBalance: Int? = null,
    @SerializedName("gmb_attributes") var gmbAttributes: MutableMap<String, MutableList<String>>? = null,
    @SerializedName("gmb_categories") var gmbCategories: List<String>? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("open_time") var openTime: MutableMap<String, List<String>>? = null,
    @SerializedName("workshop_images") var workshopImages: List<String>? = null,
    @SerializedName("gmb_updated") var gmb_updated: Boolean? = null,
    @SerializedName("gocoin_unlocked_features") var gocoin_unlocked_features: List<String>? = null,
    @SerializedName("latitude") var latitude: Double? = 0.0,
    @SerializedName("longitude") var longitude: Double? = 0.0,
    @SerializedName("unlocked_features") var unlocked_features: List<GMBUnlockFeatures>? = null)

data class GMBLinks(
    @SerializedName("map_link") var map_link: String? = null,
    @SerializedName("short_map_link") var short_map_link: String? = null,
    @SerializedName("search_link") var search_link: String? = null,
    @SerializedName("web_link") var web_link: String? = null,
)

data class GMBUnlockFeatures(
    @SerializedName("feature_code") var feature_code: String? = null,
    @SerializedName("expire_on") var expire_on: String? = null,

    )