package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class PartsResponse(

    @field:SerializedName("service_type_id") val serviceTypeId: Int? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("description") val description: String? = null,

    @field:SerializedName("status") val status: Int? = null)
