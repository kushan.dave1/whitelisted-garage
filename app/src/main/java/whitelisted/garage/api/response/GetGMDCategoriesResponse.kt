package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GetGMDCategoriesResponse(
    @field:SerializedName("categories") var categories: List<String>? = null,

    )
