package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class OrderInventoryCheckListItemModel(

    //    @field:SerializedName("id")
    //    var checkId: String? = null,

    @field:SerializedName("name") var name: String? = null,

    @field:SerializedName("isSelected") var isSelected: Boolean? = null)
