package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class RackIdResponse(

    @field:SerializedName("rack_id") val rackId: String? = null)
