package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetWsOngoingBidResponse(
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("id") var id: String? = null,
    @SerializedName("items") var items: List<String>? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("number_of_bids_received") var bidsReceived: String? = null,
    @SerializedName("status_id") var statusId: Int? = null,
    @SerializedName("total_amount") var totalAmount: String? = null,
    @SerializedName("total_item") var totalItem: Int? = null,
    @SerializedName("is_active") var isActive: Boolean? = null,
    @SerializedName("is_items_text_expanded") var isItemTextExpanded: Boolean? = false,
    @SerializedName("is_items_images_expanded") var isItemImageExpanded: Boolean? = false,
    @SerializedName("retailer_details") var retailerDetails: RetailerDetailsData? = null,
) : Parcelable

@Parcelize
data class RetailerDetailsData(
    @SerializedName("workshop_name") var workshopName: String? = null,
    @SerializedName("workshop_address") var address: String? = null,
    @SerializedName("contact_number") var contactNumber: String? = null,
    @SerializedName("map_link") var mapLink: String? = null,
    @SerializedName("longitude") var longitude: String? = null,
    @SerializedName("latitude") var latitude: String? = null,
    @SerializedName("biding_id") var biding_id: String? = null,
    @SerializedName("distance") var distance: String? = null,
) : Parcelable