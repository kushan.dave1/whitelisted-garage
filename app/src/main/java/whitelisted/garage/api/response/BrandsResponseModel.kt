package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class BrandsResponseModel(

    @field:SerializedName("thumbnail") val thumbnail: String? = null,

    @field:SerializedName("name") var name: String? = null,

    @field:SerializedName("icon") val icon: String? = null,

    @field:SerializedName("id") var id: Int? = null)
