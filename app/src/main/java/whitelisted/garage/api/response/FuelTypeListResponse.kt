package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class FuelTypeListResponse(

    @field:SerializedName("model_id") val modelId: String? = null,

    @field:SerializedName("type") val type: List<TypeItem>? = null)

data class TypeItem(

    @field:SerializedName("image") val image: String? = null,

    @field:SerializedName("type") var type: String? = null, var isSelected: Boolean = false)
