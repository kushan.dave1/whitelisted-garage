package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName


data class GetAllCouponsResponse(
    @SerializedName("value") var value: List<Value>? = null,
)

data class Value(
    @SerializedName("coupon") var coupon: String? = null,
    @SerializedName("is_timer") var is_timer: Boolean? = null,
    @SerializedName("valid_upto") var valid_upto: Long? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("text") var text: String? = null,
    var couponValue: Int? = null,

    )