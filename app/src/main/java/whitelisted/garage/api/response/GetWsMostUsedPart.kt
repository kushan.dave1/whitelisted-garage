package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName
import whitelisted.garage.api.request.GenericItem

data class GetWsMostUsedPart(@SerializedName("rack_items") var genericItems: MutableList<GenericItem>? = null)
