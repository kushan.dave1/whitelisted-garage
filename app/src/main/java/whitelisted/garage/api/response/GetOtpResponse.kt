package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GetOtpResponse(

    @field:SerializedName("data") val data: String? = null,

    @field:SerializedName("message") val message: String? = null,

    @field:SerializedName("status") val status: Boolean? = null)
