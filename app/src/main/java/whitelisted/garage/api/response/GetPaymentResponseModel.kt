package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName
import whitelisted.garage.view.fragments.orderInventory.PdfStatusDataItem

data class GetPaymentResponseModel(

    @field:SerializedName("car_details") val carDetails: CarDetailsPayment? = null,

    @field:SerializedName("car_icon") val carIcon: String? = null,

    @field:SerializedName("payment_details") val paymentDetails: PaymentDetails? = null,

    @field:SerializedName("discount_value") val discountValue: Double? = 0.0,

    @field:SerializedName("payment_status") val paymentStatus: String? = null,

    @field:SerializedName("mobile") val mobile: String? = null,

    @field:SerializedName("invoice_id") val invoiceId: String? = null,
    @field:SerializedName("transaction_mode") val transactionMode: String? = null,

    @field:SerializedName("workshop_id") val workshopId: String? = null,

    @field:SerializedName("transaction_status") val transactionStatus: String? = null,

    @field:SerializedName("amount") val amount: Double? = 0.0,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("added_by_id") val addedById: Long? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("order_id") val orderId: String? = null,

    @field:SerializedName("fuel_type") val fuelType: String? = "")

data class CarDetailsPayment(

    @field:SerializedName("car_type_id") val carTypeId: Int? = null,

    @field:SerializedName("odometer") val odometer: Int? = null,

    @field:SerializedName("car") val car: String? = null,

    @field:SerializedName("fuel_type") val fuelType: String? = null,

    @field:SerializedName("user_id") val userId: Long? = null,

    @field:SerializedName("registration_number") val registrationNumber: String? = null,

    @field:SerializedName("mobile") val mobile: String? = null,

    @field:SerializedName("model_id") val modelId: Int? = null,

    @field:SerializedName("brand_id") val brandId: Int? = null)

data class PaymentDetails(

    @field:SerializedName("bill_amount") val billAmount: Double? = 0.0,

    @field:SerializedName("due_amount") val dueAmount: Double? = 0.0,

    @field:SerializedName("paid_amount") val paidAmount: Double? = 0.0,

    @field:SerializedName("pdf_status") var pdfStatus: PdfStatusDataItem? = PdfStatusDataItem(),

    @field:SerializedName("tax_pdf_status") var taxPdfStatus: PdfStatusDataItem? = PdfStatusDataItem(),

    @field:SerializedName("discount_value") val discountValue: Double? = 0.0,

    @field:SerializedName("credit_value") val creditValue: Double? = 0.0,
)
