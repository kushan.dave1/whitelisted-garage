package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GCTransactionsResponse(

    @field:SerializedName("transactions") val transactions: List<GoCoinTransactionsItem>? = null)
