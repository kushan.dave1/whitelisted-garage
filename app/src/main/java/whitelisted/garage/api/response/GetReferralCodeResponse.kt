package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetReferralCodeResponse(@SerializedName("avail_releral_code") var availReleralCode: String? = null,
    @SerializedName("faq") var faq: List<Faq>? = null,
    @SerializedName("ref_code") var refCode: String? = null,
    @SerializedName("refral_msg") var refral_msg: String? = null,
    @SerializedName("refral_text") var referral_text: ReferralText? = null) : Parcelable

@Parcelize
data class Faq(@field:SerializedName("isSelected") var isSelected: Boolean? = false,
    @SerializedName("answer") var answer: String? = null,
    @SerializedName("question") var question: String? = null) : Parcelable

@Parcelize
data class ReferralText(
    @field:SerializedName("header") var header: String? = null,
    @SerializedName("Description") var desc: String? = null,

    ) : Parcelable

