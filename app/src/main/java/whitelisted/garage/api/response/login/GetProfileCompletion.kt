package whitelisted.garage.api.response.login

import com.google.gson.annotations.SerializedName


data class GetProfileCompletion(
    @SerializedName("profile_score") var profileScore: Int? = 0,
    @SerializedName("user_data") var userData: UserData? = null,
)

data class UserData(
    @SerializedName("address") var address: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("gstin") var gstin: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("joining_date") var joiningDate: String? = null,
    @SerializedName("mobile") var mobile: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("role") var role: String? = null,
    @SerializedName("workshop_name") var workshopName: String? = null,
    @field:SerializedName("membership_status") val membershipStatus: Boolean? = false,
    @SerializedName("country_code") var country_code: String? = null,
)