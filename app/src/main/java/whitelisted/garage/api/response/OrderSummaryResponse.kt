package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class OrderSummaryResponse(

    @field:SerializedName("status_name") val statusName: String? = null,

    @field:SerializedName("car_detail") val carDetail: String? = null,

    @field:SerializedName("estimate_amount") val estimateAmount: Double? = null,

    @field:SerializedName("fuel_meter") val fuelMeter: Int? = null,

    @field:SerializedName("inventory_date") val inventoryDate: String? = null,

    @field:SerializedName("jc_date") var jcDate: String? = null,

    @field:SerializedName("order_date") val orderDate: String? = null,

    @field:SerializedName("status_id") val statusId: Int? = null,

    @field:SerializedName("phone") val phone: String? = null,

    @field:SerializedName("ETD_date") val eTDDate: String? = null,

    @field:SerializedName("package_name") var packages: List<String>? = mutableListOf(),

    @field:SerializedName("service_name") var serviceNameList: List<String>? = mutableListOf(),

    @field:SerializedName("services_count") var servicesCount: Int? = null,

    @field:SerializedName("payment_paid") var paymentPaid: Double? = null,

    @field:SerializedName("discount") var discount: Double? = 0.0,

    @field:SerializedName("customer_name") val customerName: String? = null,

    @field:SerializedName("order_id") val orderId: String? = null)
