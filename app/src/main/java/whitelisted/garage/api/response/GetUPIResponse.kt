package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

class GetUPIResponse(
    @field:SerializedName("upi_id") val upi_id: String? = null,
)