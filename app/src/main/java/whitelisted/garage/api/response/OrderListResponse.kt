package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

class OrderListResponse(

    @field:SerializedName("ongoing_orders") val ongoingOrders: List<OrderListResponseModel>? = null,

    @field:SerializedName("open_order") val openOrders: List<OrderListResponseModel>? = null,

    @field:SerializedName("wip_order") val wipOrders: List<OrderListResponseModel>? = null,

    @field:SerializedName("ready_for_delivery") val readyOrders: List<OrderListResponseModel>? = null,

    @field:SerializedName("complete_order") val completeOrders: List<OrderListResponseModel>? = null,
    @field:SerializedName("order_history") val order_history: List<OrderListResponseModel>? = null

)