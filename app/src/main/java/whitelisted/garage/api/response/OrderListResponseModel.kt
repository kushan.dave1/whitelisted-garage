package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class OrderListResponseModel(

    @field:SerializedName("car_name") val carName: String? = null,
    @field:SerializedName("mechanic_name") val mechanic_name: String? = null,

    @field:SerializedName("status_id") val statusId: Int? = null,

    @field:SerializedName("registration_number") val registrationNumber: String? = null,

    @field:SerializedName("status_name") val statusName: String? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("mobile") val mobile: String? = null,
    @field:SerializedName("created_at") val createdAt: String? = null,
    @field:SerializedName("delivered_at") val deliveredAt: String? = null,
    @field:SerializedName("invoice_amount") val invoiceAmount: String? = "",
    @field:SerializedName("order_id") val orderId: String? = null,
    @field:SerializedName("email") val email: String? = null,
    @field:SerializedName("car_icon") val car_icon: String? = null,
    @field:SerializedName("fuel_type") val fuel_type: String? = null,
    @field:SerializedName("payment_status") val payment_status: String? = null,
    @field:SerializedName("brand_name") val brand_name: String? = null,
    @field:SerializedName("brand_id") val brand_id: Long? = null,
    @field:SerializedName("model_id") val model_id: Long? = null,

    )
