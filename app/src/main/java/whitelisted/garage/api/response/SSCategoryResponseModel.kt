package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SSCategoryResponseModel(

	@field:SerializedName("categories_name")
	var categoriesName: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("top_banner")
	val topBanner: String? = null,

	@field:SerializedName("bottom_banner")
	val bottomBanner: String? = null,

	@field:SerializedName("banners")
	val banners: MutableList<PlpBannerObject>? = mutableListOf(),

	@field:SerializedName("is_selected")
	var isSelected: Boolean = false
):Parcelable
