package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class RetailPlaceOrderResponse(@field:SerializedName("order") var orderId: String? = null)
