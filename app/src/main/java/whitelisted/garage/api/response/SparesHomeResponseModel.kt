package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class SparesHomeResponseModel(

    @SerializedName("spares_brands_flag") var sparesBrandsFlag: Boolean? = true,
    @SerializedName("bike_brands_flag") var bikeBrandsFlag: Boolean? = true,
    @SerializedName("spares_categories_flag") var sparesCategoriesFlag: Boolean? = true,
    @SerializedName("bike_categories_flag") var bikeCategoriesFlag: Boolean? = true,
    @SerializedName("accessories_brands_flag") var accBrandsFlag: Boolean? = true,
    @SerializedName("accessories_categories_flag") var accCategoriesFlag: Boolean? = true,
    @SerializedName("original_brands_flag") var originalBrandsFlag: Boolean? = true,
    @SerializedName("ss_home_banners_flag") var ssHomeBannersFlag: Boolean? = true,
    @SerializedName("bike_brands_text") var bikeBrandsText: String? = null,
    @SerializedName("bike_categories_text") var bikeCategoriesText: String? = null,
    @SerializedName("plp_banners_count") var plpBannersCount: Int? = 4,

    @field:SerializedName("acc_brands") val accBrands: MutableList<SSBrandsResponseModel> = mutableListOf(),
    @field:SerializedName("bike_brands") val bikeBrands: MutableList<SSBrandsResponseModel> = mutableListOf(),

    @field:SerializedName("banners") val banners: MutableList<SSHomeBannersResponseModel> = mutableListOf(),

    @field:SerializedName("spares_categories") val sparesCategories: MutableList<SSCategoryResponseModel> = mutableListOf(),

    @field:SerializedName("acc_categories") val accCategories: MutableList<SSCategoryResponseModel> = mutableListOf(),
    @field:SerializedName("bike_categories") val bikeCategories: MutableList<SSCategoryResponseModel> = mutableListOf(),

    @field:SerializedName("spares_brands") val sparesBrands: MutableList<SSBrandsResponseModel> = mutableListOf())
