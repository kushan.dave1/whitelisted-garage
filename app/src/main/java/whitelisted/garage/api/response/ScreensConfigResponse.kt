package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class ScreensConfigResponse(

    @field:SerializedName("bottom_navigate") val bottomNavigate: List<BottomNavigateItem>? = null,
    @field:SerializedName("account") val account: List<AccountItem>? = null,
    @SerializedName("GMB") var gMB: GMBConfig? = null,
    @SerializedName("GMB_WEBSITE") var gmbWebsite: GMBConfig? = null,
    @SerializedName("new_order_banner") var newOrderBanner: NewOrderBanner? = null,
    @SerializedName("spares_home_banner") var sparesHomeBanner: String? = null,
    @SerializedName("screen_images") var screen_images: ScreenImages? = null,
    @SerializedName("gmb_and_website_combo") var gmbWebsiteCombo: GmbWebsiteCombo? = null,
    @SerializedName("membership_config") var membershipConfig: MembershipConfig? = null,
    @SerializedName("membership") var membership: Membership? = null,
    @SerializedName("bidding_workshop_image") var biddingFlowImages: BiddingFlowImages? = null,
    @SerializedName("biding_activate") var biddingEnabled: Boolean? = false,
    @SerializedName("home_popup") var homePopup: List<HomePopupItem>? = mutableListOf(),

)

data class HomePopupItem(

    @field:SerializedName("deeplink") val deeplink: String? = null,

    @field:SerializedName("bg_color") val bgColor: String? = null,

    @field:SerializedName("heading") val heading: String? = null,

    @field:SerializedName("icon") val icon: String? = null,

    @field:SerializedName("sub_heading") val subHeading: String? = null)

data class Membership(

    @SerializedName("membership_list") var membershipList: List<MembershipListObject>? = mutableListOf(),

    @field:SerializedName("membership_price") val membershipPrice: String? = null)

data class BottomNavigateItem(

    @field:SerializedName("active_icon") val activeIcon: String? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("redirection") val redirection: String? = null,

    @field:SerializedName("inactive_icon") val inactiveIcon: String? = null)

data class NewOrderBanner(

    @field:SerializedName("acc") val acc: String? = null,

    @field:SerializedName("spares") val spares: String? = null,

    @field:SerializedName("workshop") val workshop: String? = null,
)

data class MembershipListObject(

    @field:SerializedName("basic") val basic: String? = null,
    @field:SerializedName("icon") val icon: String? = null,
    @field:SerializedName("name") val name: String? = null,
    @field:SerializedName("premium") val premium: String? = null,
    @field:SerializedName("is_active") val isActive: Boolean? = false,
    @field:SerializedName("lock_screen") val lockScreen: Boolean? = false
)

data class MembershipConfig(

    @field:SerializedName("workshop_limit") val workshopLimit: Int? = 0,
    @field:SerializedName("workshop_limit_msg") val workshopLimitMsg: String? = "",
    @field:SerializedName("order_creation") val orderCreation: Int? = 0,
    @field:SerializedName("job_card_creation_msg") val orderCreationMsg: String? = "",
    @field:SerializedName("emp_account") val empAccount: Int? = 0,
    @field:SerializedName("no_of_emp_msg") val empAccountMsg: String? = "",
    @field:SerializedName("package_limit") val packageLimit: Int? = 0,
    @field:SerializedName("basic_bulk_discount") val basicBulkDiscount: Int? = 0,
    @field:SerializedName("membership_bulk_discount") val membershipBulkDiscount: Int? = 0,
    @field:SerializedName("custom_packages_services_msg") val packageLimitMsg: String? = "",
    @field:SerializedName("inventory_limit") val inventoryLimit: Int? = 0,
    @field:SerializedName("inventory_racks_msg") val inventoryRacksMsg: String? = "",
    @field:SerializedName("employee_performance_details_msg") val employeePerformanceMsg: String? = "",
    @field:SerializedName("ledger_csv_msg") val ledgerCSVMsg: String? = "",
    @field:SerializedName("business_card_msg") val businessCardMsg: String? = "",
    @field:SerializedName("bulk_inventory_msg") val bulkInventoryMsg: String? = "",

)

data class ScreenImages(

    @field:SerializedName("home_gmb_nav") val home_gmb_nav: String? = null,
    @field:SerializedName("home_pro_membership") val homeProMembership: String? = null,
    @field:SerializedName("account_pro_membership") val accountProMembership: String? = null,
    @field:SerializedName("account_refer_earn") val account_refer_earn: String? = null,
    @field:SerializedName("account_gmb_combo") val account_gmb_combo: String? = null,

    )

data class BiddingFlowImages(

    @field:SerializedName("bidding_images") val biddingWorkshop: String? = null,

    )

data class AccountItem(

    @field:SerializedName("icon") val icon: String? = null,

    @field:SerializedName("description") val description: String? = null,

    @field:SerializedName("text") val text: String? = null,
    @field:SerializedName("background") val background: String? = null,

    @field:SerializedName("redirection") val redirection: String? = null,
    @field:SerializedName("tags") val tags: List<String>? = null)

data class GmbWebsiteCombo(

    @field:SerializedName("header") val header: String? = null,
    @field:SerializedName("description") val description: String? = null,
    @field:SerializedName("combo_benifit_header") val combo_benifit_header: String? = null,
    @field:SerializedName("combo_benifit_description") val combo_benifit_description: String? = null,
    @SerializedName("multi_images") var multiImage: List<String>? = null,
    @field:SerializedName("go_coin_faqs") val goCoinFaqs: List<FaqsItem>? = null)
