package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class MsgKeyResponse(

    @field:SerializedName("msg") val msg: String? = null)
