package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class EmployeePerformanceResponse(@SerializedName("year_amount")
                                       val yearAmount: List<Float>,
                                       @SerializedName("month_data")
                                       val monthData: MonthData,
                                       @SerializedName("year_count")
                                       val yearCount: List<Float>)


data class MonthData(@SerializedName("open_order")
                     val openOrder: Int = 0,
                     @SerializedName("complete_order")
                     val completeOrder: Int = 0,
                     @SerializedName("wip_order")
                     val wipOrder: Int = 0,
                     @SerializedName("ready_for_delivery")
                     val readyForDelivery: Int = 0)