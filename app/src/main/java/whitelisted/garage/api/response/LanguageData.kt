package whitelisted.garage.api.response

data class LanguageData(var position: Int, var string: String, var isEnable: Boolean)
