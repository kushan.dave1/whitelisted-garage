package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

class IpResponse(@field:SerializedName("ip") val ip: String? = null)