package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import whitelisted.garage.api.request.BrandWiseItem
import whitelisted.garage.api.request.GenericItem

@Parcelize
data class GetWsBidEnquiryResponse(@SerializedName("accepted_at") var acceptedAt: String? = null,
    @SerializedName("bid_created") var bidCreated: Boolean? = null,
    @SerializedName("bid_id") var bidId: String? = null,
    @SerializedName("brand_wise_items") var brandWiseItems: List<BrandWiseItem>? = null,
    @SerializedName("contact_number") var contactNumber: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("created_by") var createdBy: String? = null,
    @SerializedName("distance") var distance: String? = null,
    @SerializedName("generic_items") var genericItems: List<GenericItem>? = null,
    @SerializedName("id") var id: String? = null,
    @SerializedName("is_contact_number") var isContactNumber: List<String>? = null,
    @SerializedName("is_accepted") var isAccepted: Boolean? = null,
    @SerializedName("is_active") var isActive: Boolean? = null,
    @SerializedName("is_completed") var isCcompleted: Boolean? = null,
    @SerializedName("is_canceled") var isCanceled: Boolean? = null,
    @SerializedName("items") var items: List<String>? = null,
    @SerializedName("latitude") var latitude: String? = null,
    @SerializedName("longitude") var longitude: String? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("status_id") var statusId: Int? = null,
    @SerializedName("total_amount") var totalAmount: String? = null,
    @SerializedName("total_available_amount") var totalAvailableAmount: Int? = null,
    @SerializedName("total_item") var totalItem: Int? = null,
    @SerializedName("workshop_address") var workshopAddress: String? = null,
    @SerializedName("workshop_id") var workshopId: String? = null,
    @SerializedName("workshop_name") var workshopName: String? = null,
    @SerializedName("workshop_type") var workshopType: String? = null) : Parcelable

