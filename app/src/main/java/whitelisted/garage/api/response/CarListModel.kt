package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class CarListModel(

    @field:SerializedName("car_icon") val carIcon: String? = null,

    @field:SerializedName("fuel_type") val fuelType: String? = null,

    @field:SerializedName("car_name") val carName: String? = null,

    @field:SerializedName("type_id") val typeId: Int? = null,

    @field:SerializedName("is_popular") val isPopular: Int? = 0,

    @field:SerializedName("car_detail") val carDetail: String? = null,

    @field:SerializedName("id") val id: Int? = null,

    @field:SerializedName("model_id") val modelId: Int? = null,

    @field:SerializedName("is_luxury") val isLuxury: Int? = null,

    @field:SerializedName("brand_id") val brandId: Int? = null)
