package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName


data class BillingResponseModel(@SerializedName("address") val address: String? = null,
    @SerializedName("bill_pdf") val billPdf: String? = null,
    @SerializedName("car_icon") val carIcon: String? = null,
    @SerializedName("car_name") val carName: String? = null,
    @SerializedName("created_at") val createdAt: String? = null,
    @SerializedName("delivered_at") val deliveredAt: String? = null,
    @SerializedName("email") val email: String? = null,
    @SerializedName("fuel_type") val fuelType: String? = null,
    @SerializedName("invoice_amount") val invoiceAmount: Double? = null,
    @SerializedName("paid_amount") val paidAmount: Double? = null,
    @SerializedName("due_amount") val dueAmount: Double? = null,
    @SerializedName("discount_value") val discountValue: Double? = null,
    @SerializedName("invoice_id") val invoiceId: String? = null,
    @SerializedName("mechanic_name") val mechanicName: String? = null,
    @SerializedName("mobile") val mobile: String? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("order_id") val orderId: String? = null,
    @SerializedName("payment_status") val paymentStatus: String? = null,
    @SerializedName("registration_number") val registrationNumber: String? = null,
    @SerializedName("status_id") val statusId: Int? = null,
    @SerializedName("status_name") val statusName: String? = null,
    @SerializedName("workshop_id") val workshopId: String)