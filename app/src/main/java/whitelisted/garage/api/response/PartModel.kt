package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

class PartModel(

    @field:SerializedName("“part_name”") var partName: String? = "",

    @field:SerializedName("“part_desc”") var partDesc: String? = "")