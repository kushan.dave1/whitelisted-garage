package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class Ledger(

    @field:SerializedName("date") val date: String? = null,

    @field:SerializedName("id") val id: String? = null,

    @field:SerializedName("workshop_id") val workshopId: String? = null,

    @field:SerializedName("amount") val amount: Double? = null,

    @field:SerializedName("workshop_type") val workshopType: String? = null,

    @field:SerializedName("order_id") val orderId: String? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("remark") val remark: String? = null,

    @field:SerializedName("type") val type: String? = null,

    @field:SerializedName("created_by") val createdBy: Long? = null,

    @field:SerializedName("customer_details") val customerDetails: CustomerDetails? = null,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("updated_by") val updatedBy: Long? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("payment_info") val paymentInfo: PaymentInfo? = null,

    @field:SerializedName("credit") val credit: Double? = null,

    @field:SerializedName("status") val status: Boolean? = null)