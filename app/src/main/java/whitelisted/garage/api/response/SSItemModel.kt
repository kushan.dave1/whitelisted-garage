package whitelisted.garage.api.response

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "ss_item")
data class SSItemModel(
    @field:SerializedName("brand") var brand: String? = "",

    @field:SerializedName("category_details_pdf") var categoryDetailsPdf: String? = null,

    @field:SerializedName("image_url") var imageUrl: String? = "",

    @field:SerializedName("sub_heading") var subHeading: String? = "",

    @field:SerializedName("min_qty") var minQty: Int? = null,

    @field:SerializedName("description") var description: String? = null,

    @field:SerializedName("mrp") var mrp: Double? = null,

    @field:SerializedName("total_item_amount") var totalItemAmount: Double? = null,

    @field:SerializedName("gom_price_total_item_amount") var gomTotalAmount: Double? = null,

    @field:SerializedName("gom_price") var gomPrice: Double? = null,

    @field:SerializedName("quantity") var quantity: Int? = null,

    @field:SerializedName("title") var title: String? = null,

    @field:SerializedName("inventory") var inventory: Int? = null,

    @Ignore
    @field:SerializedName("specifications") val specifications: List<SpecificationsItem>? = listOf(),

    @Ignore
    @field:SerializedName("bundle_items") var bundleItems: List<SSItemModel>? = listOf(),

    @field:SerializedName("max_qty") var maxQty: Int? = null,

    @Ignore
    @field:SerializedName("highlights") val highlights: List<HighlightsItem> = listOf(),

    @Ignore
    @field:SerializedName("compatibility") val compatibility: List<Compatibility>? = listOf(),

    @PrimaryKey
    @field:SerializedName("product_id") var productId: String = "",

    @field:SerializedName("multiplication_difference") var multiplicationDifference: Int? = null,

    @Ignore
    @field:SerializedName("_id") val id: Id? = null,

    @field:SerializedName("sku_category") var skuCategory: String? = null,

    @field:SerializedName("sku_code") var skuCode: String? = null,

    @field:SerializedName("alternative_title") var alternativeTitle: String? = null,

//    @field:SerializedName("is_acc") var isAcc: Boolean = false,

    @field:SerializedName("is_accessories") var isAccessories: Boolean? = false,

//    @field:SerializedName("added_qty") var addedQty: Int = 0,

    @field:SerializedName("type") var type: Int = 1,

    @Ignore
    @field:SerializedName("tag") var tag: TagItem? = null,

    @field:SerializedName("is_notified") var isNotified: Boolean? = false,

    @field:SerializedName("is_enquiry") var isEnquiry: Boolean = false,

    @field:SerializedName("is_fastmoving") var isfastmoving: Boolean = false,

    @field:SerializedName("is_shimmer") var isShimmer: Boolean = false,

    @field:SerializedName("is_banner") var isBanner: Boolean = false,

    @field:SerializedName("bulk_discount_rate") var bulkDiscountRate: Double? = 0.0,

    @field:SerializedName("bulk_order_quantity") var bulkOrderQuantity: Int? = 0,

    @Ignore
    @field:SerializedName("media_url") var mediaUrl: List<MediaUrl> = listOf(),

    @Ignore
    @field:SerializedName("category_alternative") var categoryAlternative: List<SSItemModel> = listOf(),

    @Ignore
    @field:SerializedName("eg_alternative") var egAlternative: List<SSItemModel> = listOf(),

    @Ignore
    @field:SerializedName("installation_steps") var installationSteps: InstallationSteps? = null,

    @Ignore
    var bannerObject: PlpBannerObject?=null,

    @Ignore
    var isSelected: Boolean = false
) : Parcelable

@Parcelize
data class SpecificationsItem(

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("value") val value: String? = null) : Parcelable

@Parcelize
data class HighlightsItem(

    @field:SerializedName("icon_url") val iconUrl: String? = null,

    @field:SerializedName("name") val name: String? = null) : Parcelable

@Parcelize
data class TagItem(

    @field:SerializedName("tag_name") val tagName: String? = null,

    @field:SerializedName("bg_color") val bgColor: String? = null,

    @field:SerializedName("text_color") val textColor: String? = null) : Parcelable

@Parcelize
data class Id(

    @field:SerializedName("oid") val oid: String? = null) : Parcelable

@Parcelize
data class Compatibility(@field:SerializedName("brand") val brand: String? = null,
    @field:SerializedName("brand_image") val brandImage: String? = null,
    @field:SerializedName("model_image") val modelImage: String? = null,
    @field:SerializedName("model") val model: String? = null,
    @field:SerializedName("fuel_type") val fuelType: String? = null,
    @field:SerializedName("varient") val variant: String? = null) : Parcelable

@Parcelize
data class MediaUrl(
    @field:SerializedName("url") val url: String = "",
    @field:SerializedName("type") var type: Int = 0
): Parcelable


@Parcelize
data class InstallationSteps(
    @field:SerializedName("title") val title: String? = null,
    @field:SerializedName("steps") val steps: List<Step>? = null,
): Parcelable


@Parcelize
data class Step(
    @field:SerializedName("text") val text: String? = null
): Parcelable