package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GetWebsiteResponse(@SerializedName("address") var address: String? = null,
    @SerializedName("gmbwebsite_mobile") var gmb_mobile: String? = null,
    @SerializedName("gmb_links") var gmb_links: GMBLinks? = null,
    @SerializedName("gmb_price") var gmbPrice: Int? = null,
    @SerializedName("gocoin_balance") var goCoinsBalance: Int? = null,
    @SerializedName("gmbwebsite_attributes") var gmbAttributes: MutableMap<String, MutableList<String>>? = null,
    @SerializedName("gmbwebsite_categories") var gmbCategories: List<String>? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("gmbwebsite_open_time") var openTime: MutableMap<String, List<String>>? = null,
    @SerializedName("gmbwebsite_workshop_images") var workshopImages: List<String>? = null,
    @SerializedName("gmbwebsite_updated") var gmb_updated: Boolean? = null,
    @SerializedName("gocoin_unlocked_features") var gocoin_unlocked_features: List<String>? = null,
    @SerializedName("unlocked_features") var unlocked_features: List<GMBUnlockFeatures>? = null)

