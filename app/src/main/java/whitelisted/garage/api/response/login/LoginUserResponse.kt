package whitelisted.garage.api.response.login

import com.google.gson.annotations.SerializedName
import whitelisted.garage.api.response.WorkshopListResponseModel

data class LoginUserResponse(

    @field:SerializedName("user_role") val userRole: String? = null,

    @field:SerializedName("user_id") val userId: String? = "",

    @field:SerializedName("mobile") val mobile: String? = "",

    @field:SerializedName("email") val email: String? = "",

    @field:SerializedName("country_code") val countryCode: String? = "",

    @field:SerializedName("workshop_list") val workshopList: List<WorkshopListResponseModel>? = null,

    @field:SerializedName("default_workshop") val defaultWorkshop: DefaultWorkshop? = null,

    @field:SerializedName("auth_token") val authToken: String? = null)

data class DefaultWorkshop(

    @field:SerializedName("workshop_id") val workshopId: String? = null,

    @field:SerializedName("is_subscription_active") val isSubscriptionActive: Boolean? = null,

    @field:SerializedName("workshop_name") val workshopName: String? = null,

    @field:SerializedName("workshop_mobile") val workshopMobile: String? = null)

data class WorkshopListItem(

    @field:SerializedName("address") val address: String? = null,

    @field:SerializedName("owner_name") val ownerName: String? = null,

    @field:SerializedName("owner_id") val ownerId: String? = null,

    @field:SerializedName("employee_ids") val employeeIds: List<Long>? = null,

    @field:SerializedName("updated_from_login_workshop") val updatedFromLoginWorkshop: String? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("gstin") val gstin: String? = null,

    @field:SerializedName("created_by") val createdBy: Long? = null,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("map_link") val mapLink: String? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("updated_by") val updatedBy: Long? = null,

    @field:SerializedName("id") val id: String? = null,

    @field:SerializedName("is_subscription_active") val isSubscriptionActive: Boolean? = null,

    @field:SerializedName("workshop_id") val workshopId: String? = null)
