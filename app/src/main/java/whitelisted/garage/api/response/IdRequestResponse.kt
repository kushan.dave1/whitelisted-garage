package whitelisted.garage.api.response

data class IdRequestResponse(var id: String = "")