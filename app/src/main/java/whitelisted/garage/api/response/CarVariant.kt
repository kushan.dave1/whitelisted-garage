package whitelisted.garage.api.response

import android.annotation.SuppressLint
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity(tableName = "car_variant")
data class CarVariant(@PrimaryKey @SerializedName("id") val id: String,
    @SerializedName("base_variant_id") val baseVariantId: String? = "0",
    @SerializedName("brand_id") val brandId: String? = "0",
    @SerializedName("created_at") val createdAt: String? = "",
    @SerializedName("fuel_type") val fuelType: String? = "",
    @SerializedName("image_path") val imagePath: String? = "",
    @SerializedName("is_base") val isBase: Int? = 0,
    @SerializedName("is_test") val isTest: Int? = 0,
    @SerializedName("name") val name: String? = "",
    @SerializedName("status") val status: Int? = 0,
    @SerializedName("updated_at") val updatedAt: String? = "",
    @SerializedName("variant") val variant: String? = "",
    @SerializedName("yome") val yome: String? = "",
    @SerializedName("yoms") val yoms: String? = "") : Parcelable, Comparable<CarVariant> {
    @SuppressLint("DefaultLocale")
    override fun compareTo(other: CarVariant): Int {
        return (this.name ?: "").toLowerCase(Locale.getDefault())
            .compareTo((other.name ?: "").toLowerCase(Locale.getDefault()))
    }
}