package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CarAttr(@SerializedName("id") var id: Int? = null,
    @SerializedName("car_type_id") var carTypeId: Int? = null,
    @SerializedName("car_detail") var carDetail: String? = null,
    @SerializedName("is_luxury") var isLuxury: Int? = null,
    @SerializedName("brand_id") var brandId: Int? = null,
    @SerializedName("model_id") var modelId: Int? = null,
    @SerializedName("is_popular") var isPopular: Int? = null,
    @SerializedName("type_id") var typeId: Int? = null,
    @SerializedName("fuel_type") var fuelType: String? = null,
    @SerializedName("brand_name") var brandName: String = "",
    @SerializedName("car_name") var carName: String = "",
    @SerializedName("car_icon") var carIcon: String? = null,
    var isSelected: Boolean = false
): Parcelable

@Parcelize
data class SearchAndFilterAttrs(
    @SerializedName("filters") var filters: List<MarketplaceFilter> = listOf(),
    @SerializedName("popular_search") var popularSearch: List<String> = listOf(),
    @SerializedName("part_name") var partName: List<String> = listOf(),
    @SerializedName("car") var car: List<CarAttr> = listOf(),
    @SerializedName("category") var categories: List<Category> = listOf(),
    @SerializedName("brands") var brands: List<Brand> = listOf(),
    var brandCount:Int = 0, var categoryCount:Int = 0,
    var textSearch: String = "", var isImageSearch: Boolean = false
): Parcelable


@Parcelize
data class Category(@SerializedName("_id") var Id: String? = null,
    @SerializedName("category_name") var categoryName: String? = null,
    @SerializedName("icon") var icon: String? = null,
    @SerializedName("sub_category") var subCategory: List<SubCategory> = arrayListOf(),
    var isSelected: Boolean = false
): Parcelable

@Parcelize
data class SubCategory(@SerializedName("sub_category") var subCategory: String? = null,
                       @SerializedName("is_enable") var isEnable: Boolean = false,
                       @SerializedName("sub_array") var subArray: List<SubArray> = arrayListOf(),
                       var isSelected: Boolean = false): Parcelable

@Parcelize
data class SubArray(@SerializedName("name") var name: String = "",
    @SerializedName("display_name") var displayName: String = "",
    @SerializedName("category_name") var categoryName: String? = null,
    @SerializedName("is_enable") var isEnable: Boolean? = false,
    @SerializedName("segment_name") var segmentName: String? = null,
    @SerializedName("image") var image: String? = null,
    var isSelected: Boolean = false): Parcelable

@Parcelize
data class Brand(@SerializedName("name") var name: String = "",
    @SerializedName("filter_value") var filterValue: String? = null,
    @SerializedName("brand_icon") var brandIcon: String? = null,
    @SerializedName("is_accessories") var isAccessories: Boolean? = false,
    @SerializedName("is_popular") var isPopular: Boolean? = false,
    @SerializedName("is_bike") var isBike: Boolean? = false,
    @SerializedName("sub_category") var subCategories: List<String> = listOf(),


    var isSelected: Boolean = false): Parcelable
