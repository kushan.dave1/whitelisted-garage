package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName


data class ApplyCouponResponse(@SerializedName("value") var value: Int?)