package whitelisted.garage.api.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Invoice(@SerializedName("invoice_details") val invoiceDetails: InvoiceDetails? = InvoiceDetails()) :
    Parcelable

@Parcelize
data class InvoiceDetails(@SerializedName("encodedInvoice") val encodedInvoice: String?="",
    @SerializedName("displayCode") val displayCode: String?="") : Parcelable

@Parcelize
data class DspTrackingDetails(@SerializedName("uc_data") val ucData: UcData?= UcData(),
    @SerializedName("tracking_data") val trackingData: TrackingData?= TrackingData()) : Parcelable

@Parcelize
class TrackingData(@SerializedName("history") val dspHistory: List<DspHistory> = listOf()) :
    Parcelable

@Parcelize
class DspHistory(@SerializedName("status") val status: String?="",
    @SerializedName("dsp_status") val dspStatus: String? = "",
    @SerializedName("description") val description: String? = "",
    @SerializedName("timestamp") val timestamp: String? = "",
    @SerializedName("place") val place: String? = "",
    @SerializedName("objects") val historyObjects: List<HistoryObject>?= listOf()) : Parcelable

@Parcelize
data class HistoryObject(@SerializedName("status") var status: String? = "",
    @SerializedName("dsp_status") var dspStatus: String? = "",
    @SerializedName("sku_code") var skuCode: String? = "",
    @SerializedName("description") var description: String? = "",
    @SerializedName("timestamp") var timestamp: String? = "",
    @SerializedName("updated_at") var updatedAt: String? = "",
    @SerializedName("place") var place: String? = "") : Parcelable

@Parcelize
data class UcData(@SerializedName("trackingNumber") val trackingNumber: String?="") : Parcelable

@Parcelize
data class GomPrice(@SerializedName("gom_price") val gomPrice: Double? = 0.0) : Parcelable

@Parcelize
data class BillingAddress(@SerializedName("pincode") val pincode: String? = "",
    @SerializedName("address") val address: String? = "",
    @SerializedName("city") val city: String? = "",
    @SerializedName("state") val state: String? = "") : Parcelable

@Parcelize
data class ClientCustomerDetails(@SerializedName("name") val name: String? = "",
    @SerializedName("mobile") val mobile: String? = "",
    @SerializedName("workshop_name") val workshopName: String? = "",
    @SerializedName("client_customer_id") val clientCustomerId: Long? = 0,
    @SerializedName("billing_address") val billingAddress: BillingAddress? = BillingAddress(),
    @SerializedName("shipping_address") val shippingAddress: ShippingAddress? = ShippingAddress()) : Parcelable

@Parcelize
data class ShippingAddress(@SerializedName("pincode") val pincode: String? = "",
    @SerializedName("address") val address: String? = "",
    @SerializedName("city") val city: String? = "",
    @SerializedName("state") val state: String? = "") : Parcelable

@Parcelize
data class HistoryItem(@SerializedName("status") val status: String? = "",
    @SerializedName("timestamp") val timestamp: String? = "") : Parcelable

@Parcelize
data class OrderItem(@SerializedName("product_id") val productId: String? = "",
    @SerializedName("discount") val discount: Double? = 0.0,
    @SerializedName("tax") val tax: Double? = 0.0,
    @SerializedName("mrp") val mrp: Double? = 0.0,
    @SerializedName("title") val title: String? = "",
    @SerializedName("hsn_code") val hsnCode: String? = "",
    @SerializedName("sku_code") val skuCode: String? = "",
    @SerializedName("sku_brand") val skuBrand: String? = "",
    @SerializedName("quantity") val quantity: Int? = 1,
    @SerializedName("image_url") val imageUrl: String? = null,
    @SerializedName("status") var status: String? = "",
    var history: List<HistoryObject>? = listOf()) : Parcelable

@Parcelize
data class SparesOrderHistory(@SerializedName("client_order_oid") val clientOrderOid: String? = "",
    @SerializedName("rzp_payment_id") val rzpPaymentId: String? = "",
    @SerializedName("created_at") val createdAt: String? = "",
    @SerializedName("client_customer_details") val clientCustomerDetails: ClientCustomerDetails?= ClientCustomerDetails(),
    @SerializedName("discount") val discount: Double? = 0.0,
    @SerializedName("history") val history: List<HistoryItem>? = listOf(),
    @SerializedName("total") val total: Double? = 0.0,
    @SerializedName("updated_at") val updatedAt: String? = "",
    @SerializedName("id") val id: String? = "",
    @SerializedName("created_by_id") val createdById: Long? = 0,
    @SerializedName("order_id") val orderId: String? = "",
    @SerializedName("line_items_array") val items: List<OrderItem>? = listOf(),
    @SerializedName("order_type") val orderType: String? = "",
    @SerializedName("result") val gomPrices: List<GomPrice>? = listOf(),
    @SerializedName("dsp_tracking_details") val dspTrackingDetails: List<DspTrackingDetails>?= listOf(),
    @SerializedName("statuses") val historyObjects: List<HistoryObject>?= listOf(),
    @SerializedName("invoices") val invoices: List<Invoice>?= listOf(),
    @SerializedName("status") val status: String? = "") : Parcelable
