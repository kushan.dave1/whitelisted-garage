package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class CarModelResponseModel(

    @field:SerializedName("car_type_id") var carTypeId: Int? = null,

    @field:SerializedName("car_name") var carName: String? = null,

    @field:SerializedName("car_icon") var carIcon: String? = null,

    @field:SerializedName("is_popular") val isPopular: Int? = null,

    @field:SerializedName("type_id") val typeId: Int? = null,

    @field:SerializedName("car_detail") val carDetail: String? = null,

    @field:SerializedName("id") val id: Int? = null,

    @field:SerializedName("model_id") var modelId: Int? = null,

    @field:SerializedName("fuel_type") val fuelType: String? = null,

    @field:SerializedName("is_luxury") val isLuxury: Int? = null,

    @field:SerializedName("brand_id") val brandId: Int? = null)
