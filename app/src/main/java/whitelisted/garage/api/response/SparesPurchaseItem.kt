package whitelisted.garage.api.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PurchaseItem(@SerializedName("workshop_id")
                     val workshopId: String? = null,
                     @SerializedName("ordered")
                     val ordered: Boolean = false,
                     @SerializedName("quantity")
                     val quantity: Int = 0,
                     @SerializedName("workshop_type")
                     val workshopType: String? = null,
                     @SerializedName("image_url")
                     val imageUrl: String = "",
                     @SerializedName("created_at")
                     val createdAt: String = "",
                     @SerializedName("mrp")
                     val mrp: Int = 0,
                     @SerializedName("title")
                     val title: String = "",
                     @SerializedName("cart_id")
                     val cartId: String = "",
                     @SerializedName("updated_at")
                     val updatedAt: String = "",
                     @SerializedName("total_item_amount")
                     val totalItemAmount: Int = 0,
                     @SerializedName("product_id")
                     val productId: String = "",
                     @SerializedName("updated_by_id")
                     val updatedById: Long = 0,
                     @SerializedName("created_by_id")
                     val createdById: Long = 0,
                     @SerializedName("sku_code")
                     val skuCode: String = "",
                     @SerializedName("payment_received")
                     val paymentReceived: Boolean = false):Parcelable


@Parcelize
data class SparesPurchaseItem(@SerializedName("ordered") val ordered: Boolean = false,
                              @SerializedName("updated_at") val updatedAt: String = "",
                              @SerializedName("total_amount") val totalAmount: Int = 0,
                              @SerializedName("created_at") val createdAt: String = "",
                              @SerializedName("updated_by_id") val updatedById: Long = 0,
                              @SerializedName("id") val id: String = "",
                              @SerializedName("created_by_id") val createdById: Long = 0,
                              @SerializedName("no_of_items") val noOfItems: Int = 0,
                              @SerializedName("order_id") val orderId: String = "",
                              @SerializedName("discount_coupon") val discountCoupon: String? = null,
                              @SerializedName("discount") val discount:Int = 0,
                              @SerializedName("items") val items: List<PurchaseItem> = listOf(),
                              @SerializedName("payment_received") val paymentReceived: Boolean = false):Parcelable


