package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class AddJobCardResponse(

    @field:SerializedName("order_status") val orderStatus: Int? = null,

    @field:SerializedName("job_card_items") val jobCardItems: List<Any?>? = null)
