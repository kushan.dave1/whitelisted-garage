package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName


data class GetUserProfileResponse(
    @field:SerializedName("name")

    val name: String? = null,
    @field:SerializedName("role")

    val role: String? = null,

    @field:SerializedName("mobile")

    val mobile: String? = null,

    @field:SerializedName("email")

    val email: String? = null,

    @field:SerializedName("password")

    val password: String? = null,

    @field:SerializedName("user_id")

    val userId: Long? = null,

    @field:SerializedName("created_at")

    val createdAt: String? = null,

    @field:SerializedName("updated_at")

    val updatedAt: String? = null,

    @field:SerializedName("mobile_verified")

    val mobileVerified: Boolean? = null,

    @field:SerializedName("deleted_at")

    val deletedAt: Any? = null,

    @field:SerializedName("roles")

    val roles: List<String>? = null,

    @field:SerializedName("service_workshops")

    val serviceWorkshops: Any? = null,

    @field:SerializedName("address")

    val address: Any? = null,

    @field:SerializedName("last_service_date")

    val lastServiceDate: Any? = null,

    @field:SerializedName("workshops")

    val workshops: Any? = null,

    @field:SerializedName("updated_by_id")

    val updatedById: Long? = null,

    @field:SerializedName("created_by_id")

    val createdById: Long? = null,

    @field:SerializedName("owner_id")

    val ownerId: Long? = null,

    @field:SerializedName("image")

    val image: String? = null,

    @field:SerializedName("user_type")

    val userType: String? = null,
    @field:SerializedName("joining_date")

    val joiningDate: String? = null,
)
