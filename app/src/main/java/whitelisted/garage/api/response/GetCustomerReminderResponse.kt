package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName


data class GetCustomerReminderResponse(@SerializedName("msg_text") var msgText: Map<String, String>? = null,
    @SerializedName("previous_reminder") var previousReminder: List<PreviousReminder>? = null,
    @SerializedName("reminder_cost") var reminderCost: Long? = null

)


data class PreviousReminder(
    @SerializedName("amount") var amount: Double? = null,
    @SerializedName("country_code") var countryCode: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("customer_name") var customerName: String? = null,
    @SerializedName("mobile") var mobile: String? = null,
    @SerializedName("sendby") var sendby: String? = null,
    @SerializedName("text_msg") var textMsg: String? = null,
    @SerializedName("workshop_id") var workshopId: String? = null,
    @SerializedName("workshop_type") var workshopType: String? = null,
    @SerializedName("id") var id: String? = null,
    @SerializedName("due_colleted") var due_collected: Boolean? = false,
    @SerializedName("language") var language: String? = null,
)