package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class PlaceOrderResponse(@field:SerializedName("order_id") val orderId: String? = "")
