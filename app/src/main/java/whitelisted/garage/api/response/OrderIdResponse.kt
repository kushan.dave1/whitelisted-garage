package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

class OrderIdResponse {
    @field:SerializedName("order_id")
    var orderId: String? = null
}