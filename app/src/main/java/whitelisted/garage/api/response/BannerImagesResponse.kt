package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class BannerImagesResponse(

    @field:SerializedName("data") val data: List<BannerDataItem>? = null)

data class BannerDataItem(

    @field:SerializedName("banner_image") val bannerImage: String? = null,

    @field:SerializedName("redirection") val redirection: String? = null)
