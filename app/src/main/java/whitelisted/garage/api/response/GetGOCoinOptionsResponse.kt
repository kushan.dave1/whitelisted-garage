package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GetGOCoinOptionsResponse(

    @SerializedName("default_select") var defaultSelect: Int? = null,


    @SerializedName("gocoins_options") var gocoinsOptions: List<String>? = null)
