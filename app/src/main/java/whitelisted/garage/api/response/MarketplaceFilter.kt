package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MarketplaceFilter(@SerializedName("name") var name: String? = null,
                             @SerializedName("filter_key") var filterKey: String? = null,
                             @SerializedName("isMultiple") var isMultiple: Boolean? = null,
                             @SerializedName("types") var filterTypes: List<FilterType>
                             = arrayListOf(),
                             var selectionCount: Int = 0
):Parcelable

@Parcelize
data class FilterType(@SerializedName("name") var name: String? = null,
                      @SerializedName("filter_value") var filterValue: String? = null,
                      var isSelected: Boolean = false): Parcelable