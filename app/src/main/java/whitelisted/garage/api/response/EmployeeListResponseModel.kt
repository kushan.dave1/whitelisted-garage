package whitelisted.garage.api.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EmployeeListResponseModel(@SerializedName("created_at") val createdAt: String? = "",
    @SerializedName("mobile") val mobile: String? = "",
    @SerializedName("email") val email: String? = "",
    @SerializedName("joining_date") val joiningDate: String? = "",
    @SerializedName("mobile_verified") val mobileVerified: Boolean = false,
    @SerializedName("name") val name: String? = "",
    @SerializedName("updated_at") val updatedAt: String? = "",
    @SerializedName("user_role") val userRole: String? = "",
    @SerializedName("image") val image: String? = "",
    @SerializedName("user_id") val userId: Long? = 0) : Parcelable