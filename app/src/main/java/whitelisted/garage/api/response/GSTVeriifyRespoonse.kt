package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GSTVeriifyRespoonse(

	@field:SerializedName("gstin_info")
	val gstinInfo: String? = null,

	@field:SerializedName("update_payload")
	val updatePayload: UpdatePayload? = null,

	@field:SerializedName("gstin_warning")
	val gstinWarning: String? = null,

	@field:SerializedName("gstin_status")
	val gstinStatus: Int? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class UpdatePayload(

	@field:SerializedName("pin")
	val pin: Int? = null,

	@field:SerializedName("gst_address")
	val gstAddress: String? = null,

	@field:SerializedName("gst_details")
	val gstDetails: GstDetails? = null,

	@field:SerializedName("gstin")
	val gstin: String? = null,

	@field:SerializedName("billing_name")
	val billingName: String? = null
)

data class GstDetails(

	@field:SerializedName("TradeName")
	val tradeName: String? = null,

	@field:SerializedName("AddrLoc")
	val addrLoc: String? = null,

	@field:SerializedName("Status")
	val status: String? = null,

	@field:SerializedName("LegalName")
	val legalName: String? = null,

	@field:SerializedName("BlkStatus")
	val blkStatus: String? = null,

	@field:SerializedName("TxpType")
	val txpType: String? = null,

	@field:SerializedName("StateCode")
	val stateCode: Int? = null,

	@field:SerializedName("AddrSt")
	val addrSt: String? = null,

	@field:SerializedName("DtDReg")
	val dtDReg: String? = null,

	@field:SerializedName("AddrFlno")
	val addrFlno: String? = null,

	@field:SerializedName("DtReg")
	val dtReg: String? = null,

	@field:SerializedName("Gstin")
	val gstin: String? = null,

	@field:SerializedName("AddrPncd")
	val addrPncd: Int? = null,

	@field:SerializedName("AddrBno")
	val addrBno: String? = null,

	@field:SerializedName("AddrBnm")
	val addrBnm: String? = null
)
