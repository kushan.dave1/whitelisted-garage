package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class BusinessCardTemplate(@SerializedName("image") val image: String? = null,
                                @SerializedName("template_id") val templateId: String = "",
                                @SerializedName("color") val textColor: String? = null,
                                @SerializedName("icon") val businessCardIcons: BusinessCardIcons? = null,
                                var data: BusinessCardDetails? = null
)

@Parcelize
data class BusinessCardIcons(@SerializedName("name") val nameIcon: String = "",
                              @SerializedName("phone") val phoneIcon: String = "",
                              @SerializedName("email") val emailIcon: String = "",
                              @SerializedName("address") val addressIcon: String = ""

):Parcelable