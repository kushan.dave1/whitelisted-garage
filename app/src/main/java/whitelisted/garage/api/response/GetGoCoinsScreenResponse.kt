package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GetGoCoinsScreenResponse(

    @field:SerializedName("gocoin_balance") val gocoinBalance: Long? = 0,

    @field:SerializedName("gocoin_total_rupe") val goCoinTotalBalance: Double? = 0.0,

    @field:SerializedName("more_transactions") val moreTransactions: Boolean? = false,

    @field:SerializedName("gocoins_banner") var GoCoinUsageItem: MutableList<GoCoinUsageItem>? = null,

    @field:SerializedName("gocoin_buy_options") var GoCoinBuyOptionsItem: MutableList<GoCoinBuyOptionsItem>? = mutableListOf(),

    @field:SerializedName("go_coin_transactions") val goCoinTransactions: List<GoCoinTransactionsItem>? = null,

    @field:SerializedName("go_coin_faqs") val goCoinFaqs: List<FaqsItem>? = null)

data class GoCoinTransactionsItem(

    @field:SerializedName("created_at") val date: String? = null,

    @field:SerializedName("amount") val amount: Long? = null,

    @field:SerializedName("source") val heading: String? = null,

    @field:SerializedName("remark") val remark: String? = null,

    @field:SerializedName("type") val type: String? = null)

data class FaqsItem(

    @field:SerializedName("que") val que: String? = null,
    @field:SerializedName("ques") val ques: String? = null,
    @field:SerializedName("isSelected") var isSelected: Boolean? = false,
    @field:SerializedName("ans") val ans: String? = null,
    @field:SerializedName("mcq") val mcq: List<String>? = null,
    @field:SerializedName("type") val type: List<String>? = null,

)

data class GoCoinUsageItem(

    @field:SerializedName("url") val usageUrl: String? = null,

    @field:SerializedName("redirection") val redirection: String? = null)

data class GoCoinBuyOptionsItem(

    @field:SerializedName("no_of_coins") var noOfCoins: String? = null,

    @field:SerializedName("isSelected") var isSelected: Boolean? = false,

    @field:SerializedName("amount") var amount: String? = null)
