package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GetNearByBidingWorkshop(@SerializedName("distance") var distance: Double? = null,
    @SerializedName("id") var id: String? = null,
    @SerializedName("latitude") var latitude: Double? = null,
    @SerializedName("longitude") var longitude: Double? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("types") var types: String? = null)