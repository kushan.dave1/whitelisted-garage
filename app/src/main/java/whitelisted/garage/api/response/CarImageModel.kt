package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class CarImageModel(@field:SerializedName("car_image") var carImage: String? = "")
