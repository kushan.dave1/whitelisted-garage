package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GetAddPaymentResponseModel(

    @field:SerializedName("transaction_mode") val transactionMode: String? = null,

    @field:SerializedName("workshop_id") val workshopId: String? = null,

    @field:SerializedName("transaction_status") val transactionStatus: String? = null,

    @field:SerializedName("amount") val amount: Int? = null,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("added_by_id") val addedById: Long? = null,

    @field:SerializedName("mobile") val mobile: String? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("order_id") val orderId: String? = null)
