package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName
import whitelisted.garage.api.request.BrandWiseItem
import whitelisted.garage.api.request.GenericItem


data class GetWsBidReceivedResponse(@SerializedName("acc_retail_biding") var accRetailBiding: List<AccRetailBiding>? = null,
    @SerializedName("workshop_biding") var workshopBiding: WorkshopBiding? = null)

data class AccRetailBiding(
    @SerializedName("status") var status: String? = null,
    @SerializedName("status_id") var statusId: Int? = null,
    @SerializedName("bid_id") var bidId: String? = null,
    @SerializedName("contact_number") var contactNumber: String? = null,
    @SerializedName("id") var id: String? = null,
    @SerializedName("is_active") var isActive: Boolean? = null,
    @SerializedName("is_accepted") var isAccepted: Boolean? = null,
    @SerializedName("items") var items: List<String>? = null,
    @SerializedName("total_amount") var totalAmount: String? = null,
    @SerializedName("total_available_amount") var totalAvailableAmount: Long? = null,
    @SerializedName("total_item") var totalItem: Long? = null,
    @SerializedName("workshop_address") var workshopAddress: String? = null,
    @SerializedName("workshop_id") var workshopId: String? = null,
    @SerializedName("workshop_name") var workshopName: String? = null,
    @SerializedName("is_items_text_expanded") var isItemTextExpanded: Boolean? = false,
    @SerializedName("is_items_images_expanded") var isItemImageExpanded: Boolean? = false,
    @SerializedName("distance") var distance: String? = null,
    @SerializedName("longitude") var longitude: String? = null,
    @SerializedName("latitude") var latitude: String? = null,
    @SerializedName("is_contact_number") var isContactNumber: List<String>? = null,
)

data class WorkshopBiding(
    @SerializedName("accepted_at") var acceptedAt: Any? = null,
    @SerializedName("bid_created") var bidCreated: Boolean? = null,
    @SerializedName("brand_wise_items") var brandWiseItems: List<BrandWiseItem>? = null,
    @SerializedName("contact_number") var contactNumber: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("created_by") var createdBy: Long? = null,
    @SerializedName("generic_items") var genericItems: List<GenericItem>? = null,
    @SerializedName("id") var id: String? = null,
    @SerializedName("is_accepted") var isAccepted: Boolean? = null,
    @SerializedName("is_active") var isActive: Boolean? = null,
    @SerializedName("is_contact_number") var isContactNumber: List<String>? = null,
    @SerializedName("items") var items: List<String>? = null,
    @SerializedName("near_by_workshops") var nearByWorkshops: List<String>? = null,
    @SerializedName("number_of_bids_received") var numberOfBidsReceived: Int? = null,
    @SerializedName("status") var status: String? = null,
    @SerializedName("status_id") var statusId: Int? = null,
    @SerializedName("total_amount") var totalAmount: Any? = null,
    @SerializedName("total_item") var totalItem: Int? = null,
    @SerializedName("workshop_address") var workshopAddress: String? = null,
    @SerializedName("workshop_id") var workshopId: String? = null,
    @SerializedName("workshop_name") var workshopName: String? = null,
    @SerializedName("workshop_type") var workshopType: String? = null,
    @SerializedName("longitude") var longitude: String? = null,
    @SerializedName("latitude") var latitude: String? = null, // @SerializedName("distance") var distance: Map<String, DistanceObject>? = null,
)



