package whitelisted.garage.api.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlpBannerObject(@field:SerializedName("url") val url: String? = null,

    @field:SerializedName("deeplink") val deeplink: String? = null,

    @field:SerializedName("thumbnail") val thumbnail: String? = null,

    @field:SerializedName("lottie_images") val lottieImages: List<String>? = null,

    @field:SerializedName("type") val type: Int? = null) : Parcelable