package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class OrderIdForPaymentResponse(

    @field:SerializedName("workshop_id") val workshopId: String? = null,

    @field:SerializedName("amount") val amount: Double? = null,

    @field:SerializedName("amount_paid") val amountPaid: Double? = null,

    @field:SerializedName("coins") val coins: String? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("offer_id") val offerId: String? = null,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("amount_due") val amountDue: Double? = null,

    @field:SerializedName("currency") val currency: String? = null,

    @field:SerializedName("receipt") val receipt: String? = null,

    @field:SerializedName("id") val id: String? = null,

    @field:SerializedName("entity") val entity: String? = null,

    @field:SerializedName("status") val status: String? = null,

    @field:SerializedName("attempts") val attempts: Int? = null)
