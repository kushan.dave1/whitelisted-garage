package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class SubscriptionStatusResponse(

    @field:SerializedName("“subscription_validity”") val subscriptionValidity: String? = null,

    @field:SerializedName("“is_subscription_active”") val isSubscriptionActive: Boolean? = null)
