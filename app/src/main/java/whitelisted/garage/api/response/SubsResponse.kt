package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class SubsResponse(

	@field:SerializedName("end_at")
	val endAt: Int? = null,

	@field:SerializedName("paid_count")
	val paidCount: Int? = null,

	@field:SerializedName("quantity")
	val quantity: Int? = null,

	@field:SerializedName("notes")
	val notes: Notes? = null,

	@field:SerializedName("has_scheduled_changes")
	val hasScheduledChanges: Boolean? = null,

	@field:SerializedName("remaining_count")
	val remainingCount: Int? = null,

	@field:SerializedName("total_count")
	val totalCount: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: Int? = null,

	@field:SerializedName("source")
	val source: String? = null,

	@field:SerializedName("start_at")
	val startAt: Int? = null,

	@field:SerializedName("current_end")
	val currentEnd: String? = null,

	@field:SerializedName("charge_at")
	val chargeAt: Int? = null,

	@field:SerializedName("short_url")
	val shortUrl: String? = null,

	@field:SerializedName("change_scheduled_at")
	val changeScheduledAt: String? = null,

	@field:SerializedName("customer_notify")
	val customerNotify: Boolean? = null,

	@field:SerializedName("auth_attempts")
	val authAttempts: Int? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("entity")
	val entity: String? = null,

	@field:SerializedName("plan_id")
	val planId: String? = null,

	@field:SerializedName("ended_at")
	val endedAt: String? = null,

	@field:SerializedName("expire_by")
	val expireBy: Any? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("current_start")
	val currentStart: String? = null
)

data class Notes(

	@field:SerializedName("key1")
	val key1: String? = null,

	@field:SerializedName("key2")
	val key2: String? = null
)
