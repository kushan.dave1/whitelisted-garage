package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class SearchImageResponse(@field:SerializedName("icon") val icon: String? = null)
