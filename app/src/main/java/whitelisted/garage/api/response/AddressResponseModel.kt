package whitelisted.garage.api.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddressResponseModel(

	@field:SerializedName("address")
	var address: String? = null,

	@field:SerializedName("pin")
	var pin: String? = null,

	@field:SerializedName("user_id")
	var userId: String? = null,

	@field:SerializedName("workshop_name")
	var workshopName: String? = null,

	@field:SerializedName("city")
	var city: String? = null,

	@field:SerializedName("latitude")
	var latitude: Double? = null,

	@field:SerializedName("longitude")
	var longitude: Double? = null,

	@field:SerializedName("is_edit_del")
	var isEditDel: Boolean? = false,

	@field:SerializedName("address_type")
	var address_type: String? = null,

	@field:SerializedName("address_id")
	var addressId: String? = null,

	@field:SerializedName("name")
	var name: String? = null,

	@field:SerializedName("is_default")
	var isDefault: Boolean? = false,

	@field:SerializedName("mobile")
	var mobile: String? = null,

	@field:SerializedName("state")
	var state: String? = null
):Parcelable