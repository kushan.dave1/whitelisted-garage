package whitelisted.garage.api.response

//@Entity(tableName = "inventoryCheckList")
data class OrderInventoryCheckListResponse( //    @PrimaryKey(autoGenerate = true)
    //    var id: Long,
    var list: List<String>? = null)