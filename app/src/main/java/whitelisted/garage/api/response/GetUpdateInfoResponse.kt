package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName


data class GetUpdateInfoResponse(@field:SerializedName("backward_compatible_till") var backwardCompatibleTill: String? = null,
    @field:SerializedName("latest") var latest: String? = null)
