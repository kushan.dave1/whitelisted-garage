package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class CustomPackageResponse(

    @field:SerializedName("workshop_id") val workshopId: String? = null,

    @field:SerializedName("total") val total: Int? = null,

    @field:SerializedName("total_services") val totalServices: Int? = null,

    @field:SerializedName("deleted") val deleted: Boolean? = null,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("updated_by") val updatedBy: Int? = null,

    @field:SerializedName("description") val description: String? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("package_id") val packageId: Long? = null,

    @field:SerializedName("created_by") val createdBy: Int? = null,

    @field:SerializedName("deleted_at") val deletedAt: String? = null)
