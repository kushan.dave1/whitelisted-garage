package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GetRazorPayKey(@field:SerializedName("key") val key: String? = null)