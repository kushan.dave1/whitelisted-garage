package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class PackagesResponse(

    @field:SerializedName("sub_category_id") val subCategoryId: Int? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("desc_details") val descDetails: DescDetails? = null,

    @field:SerializedName("desc_tags") val descTags: List<String>? = null,

    @field:SerializedName("description") val description: String? = null)

data class DescDetails(

    @field:SerializedName("inclusion") val inclusion: List<String?>? = null,

    @field:SerializedName("warranty") val warranty: String? = null,

    @field:SerializedName("interval") val interval: String? = null,

    @field:SerializedName("time_taken") val timeTaken: String? = null)
