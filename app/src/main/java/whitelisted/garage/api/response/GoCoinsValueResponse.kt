package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class GoCoinsValueResponse(

    @field:SerializedName("gocoins_value") val gocoinsValue: Int? = null)
