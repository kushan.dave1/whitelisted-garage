package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class ServiceResponseModel(

    @field:SerializedName("service_type_id") val serviceTypeId: Long? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("selling_price") var selling_price: Double? = 0.0,

    @field:SerializedName("description") val description: String? = null,

    @field:SerializedName("id") val id: Long? = null,

    @field:SerializedName("status") val status: Int? = null)
