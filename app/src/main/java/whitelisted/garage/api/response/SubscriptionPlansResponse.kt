package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class SubscriptionPlansResponse(

	@field:SerializedName("period")
	val period: String? = null,

	@field:SerializedName("item")
	val item: Item? = null,

	@field:SerializedName("created_at")
	val createdAt: Int? = null,

	@field:SerializedName("interval")
	val interval: Int? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("entity")
	val entity: String? = null
)

data class Item(

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("tax_group_id")
	val taxGroupId: String? = null,

	@field:SerializedName("active")
	val active: Boolean? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("tax_inclusive")
	val taxInclusive: Boolean? = null,

	@field:SerializedName("created_at")
	val createdAt: Int? = null,

	@field:SerializedName("unit_amount")
	val unitAmount: Int? = null,

	@field:SerializedName("sac_code")
	val sacCode: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("hsn_code")
	val hsnCode: String? = null,

	@field:SerializedName("tax_rate")
	val taxRate: String? = null,

	@field:SerializedName("tax_id")
	val taxId: String? = null,

	@field:SerializedName("unit")
	val unit: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)
