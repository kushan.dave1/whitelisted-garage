package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class SSCartResponseModel(

    @field:SerializedName("ordered") val ordered: Boolean? = null,

    @field:SerializedName("min_emi_amount") val minEmiAmount: Double? = 1000.0,

    @field:SerializedName("discount") val discount: Double? = null,

    @field:SerializedName("created_at") val createdAt: String? = null,

    @field:SerializedName("line_items") val lineItems: List<SSItemModel>? = null,

    @field:SerializedName("no_of_items") val noOfItems: Int? = null,

    @field:SerializedName("gom_price_total_amount") val gomPriceTotalAmount: Double? = null,

    @field:SerializedName("is_spares") val isSpares: Boolean? = null,

    @field:SerializedName("gocoins_discount") val gocoinsDiscount: Double? = null,

    @field:SerializedName("updated_at") val updatedAt: String? = null,

    @field:SerializedName("total_amount") val totalAmount: Double? = null,

    @field:SerializedName("updated_by_id") val updatedById: Long? = null,

    @field:SerializedName("id") val id: String? = null,

    @field:SerializedName("created_by_id") val createdById: Long? = null,

    @field:SerializedName("order_id") val orderId: String? = null,

    @field:SerializedName("discount_coupon") val discountCoupon: String? = null)
