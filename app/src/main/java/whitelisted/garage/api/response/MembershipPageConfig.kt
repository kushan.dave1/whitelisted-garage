package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class MembershipPageConfig(
    @SerializedName("pro_benefits") var proBenefits: ArrayList<ProBenefits>? = arrayListOf(),
    @SerializedName("pro_benefits_title") var proBenefitsTitle: String? = null,
    @SerializedName("mrp") var mrp: String? = null,
    @SerializedName("top_header") var topHeader: String? = null,
    @SerializedName("header_sub_heading") var subHeading: String? = null,
    @SerializedName("membership_status_code") var membershipStatusCode: String? = null,
    @SerializedName("offer_price") var offerPrice: String? = null,
    @SerializedName("offer_price_text") var offerPriceText: String? = null,
    @SerializedName("membership_benefits_title") var membershipBenefitsTitle: String? = null,
    @SerializedName("membership_benefits") var membershipBenefits: ArrayList<MembershipBenefits>? = arrayListOf(),
    @SerializedName("extra_discount_title") var extraDiscountTitle: String? = null,
    @SerializedName("extra_discount_brands") var extraDiscountBrands: ArrayList<ExtraDiscountBrands>? = arrayListOf(),
    @SerializedName("pro_compare_title") var proCompareTitle: String? = null,
    @SerializedName("pro_compare_list") var proCompareList: ArrayList<ProCompare>? = arrayListOf(),
    @SerializedName("faq") var faq: ArrayList<FaqsItem>? = arrayListOf(),
    @SerializedName("membership_price") var membershipPrice: String? = null,
    @SerializedName("membership_list") var membershipList: ArrayList<MembershipList>? = arrayListOf(),
    @SerializedName("config") var config: String? = null
)

data class ProCompare(
    @SerializedName("title") var title: String? = null,
    @SerializedName("basic") var basic: String? = null,
    @SerializedName("premium") var premium: String? = null,
    @SerializedName("color_basic") var colorBasic: String? = null,
    @SerializedName("color_preminm") var colorPreminm: String? = null,
    @SerializedName("basic_header") var basicHeader: String? = null,
    @SerializedName("preminu_header") var premiumHeader: String? = null

)


data class ProBenefits(

    @SerializedName("icon") var icon: String? = null,
    @SerializedName("name") var name: String? = null

)


data class MembershipBenefits(

    @SerializedName("header") var header: String? = null,
    @SerializedName("sub_header") var subHeader: String? = null,
    @SerializedName("id") var id: String? = null,
    @SerializedName("icon") var icon: String? = null,
    @SerializedName("deeplink") var deeplink: String? = null,
    @SerializedName("header_color") var headerColor: String? = null,
    @SerializedName("sub_header_color") var subHeaderColor: String? = null,
    @SerializedName("bg_color") var bgColor: String? = null

)


data class ExtraDiscountBrands(

    @SerializedName("icon") var icon: String? = null,
    @SerializedName("discount") var discount: String? = null,
    @SerializedName("name") var name: String? = null

)


data class MembershipList(

    @SerializedName("name") var name: String? = null,
    @SerializedName("basic") var basic: String? = null,
    @SerializedName("premium") var premium: String? = null,
    @SerializedName("is_active") var isActive: Boolean? = false

)