package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

data class PreviousMonthResponse(

    @field:SerializedName("total_order") val totalOrder: String? = null,

    @field:SerializedName("payment_data") val paymentData: PaymentData? = null)

data class PaymentData(

    @field:SerializedName("Total") val total: Double? = null,

    @field:SerializedName("Paid") val paid: Double? = null,

    @field:SerializedName("Pending") val pending: Double? = null)
