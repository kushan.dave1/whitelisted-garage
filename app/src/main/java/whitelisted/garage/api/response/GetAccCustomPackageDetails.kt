package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName


data class GetAccCustomPackageDetails(@SerializedName("created_at") val createdAt: String? = null,
    @SerializedName("id") val id: String? = null,
    @SerializedName("items_count") val itemsCount: Int? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("pkg_items") val pkgItems: List<PkgItem>? = null,
    @SerializedName("updated_at") val updatedAt: String? = null,
    @SerializedName("workshop_id") val workshopId: String? = null,
    @SerializedName("workshop_type") val workshopType: String? = null)

data class PkgItem(@SerializedName("created_at") val createdAt: String? = null,
    @SerializedName("part_name") val partName: String? = null,
    @SerializedName("pkg_id") val pkgId: String? = null,
    @SerializedName("pkg_item_id") val pkgItemId: String? = null,
    @SerializedName("price_per_item") val pricePerItem: Double? = null,
    @SerializedName("quantity") val quantity: Int? = null,
    @SerializedName("service_id") val serviceId: Int? = null,
    @SerializedName("service_name") val serviceName: String? = null,
    @SerializedName("sku_id") val skuId: String? = null,
    @SerializedName("tax_rate") val taxRate: String? = null,
    @SerializedName("total") val total: Double? = null,
    @SerializedName("updated_at") val updatedAt: String? = null,
    @SerializedName("work_done") val workDone: String? = null,
    @SerializedName("work_done_id") val workDoneId: Int? = null,
    @SerializedName("workshop_id") val workshopId: String? = null,
    @SerializedName("workshop_type") val workshopType: String? = null)