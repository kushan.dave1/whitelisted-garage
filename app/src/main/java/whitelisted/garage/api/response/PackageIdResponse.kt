package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName

class PackageIdResponse(@field:SerializedName("package_id") val workshopId: String? = null)