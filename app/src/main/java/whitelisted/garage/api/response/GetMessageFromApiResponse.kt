package whitelisted.garage.api.response

import com.google.gson.annotations.SerializedName


data class GetMessageFromApiResponse(@SerializedName("sms_text") val smsText: String?,
    @SerializedName("whatsapp_text") val whatsappText: String?)