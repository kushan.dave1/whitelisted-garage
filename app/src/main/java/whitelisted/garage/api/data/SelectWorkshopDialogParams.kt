package whitelisted.garage.api.data

data class SelectWorkshopDialogParams(var isStart: Boolean? = null, var isHome: Boolean? = null)
