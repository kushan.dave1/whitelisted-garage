package whitelisted.garage.api.data

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class CustomerDetailsModel(
    @field:SerializedName("email") var email: String? = "",
    @field:SerializedName("address") var address: String? = "",
    @field:SerializedName("name") var name: String? = "",
    @field:SerializedName("mobile") var phoneNumber: String? = "",
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(email)
        parcel.writeString(address)
        parcel.writeString(name)
        parcel.writeString(phoneNumber)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CustomerDetailsModel> {
        override fun createFromParcel(parcel: Parcel): CustomerDetailsModel {
            return CustomerDetailsModel(parcel)
        }

        override fun newArray(size: Int): Array<CustomerDetailsModel?> {
            return arrayOfNulls(size)
        }
    }
}