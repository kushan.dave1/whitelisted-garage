package whitelisted.garage.api.data

import com.google.gson.annotations.SerializedName

data class GetAccessoriesCatalogueResponse(@SerializedName("brand_id") val brandId: String? = null,
    @SerializedName("brand_name") val brandName: String? = null,
    @SerializedName("workshop_id") val workshopID: String? = null,
    @SerializedName("brand_slug") val brandSlug: String? = null,
    @SerializedName("car_dependent") val carDependent: Int? = null,
    @SerializedName("category_id") val categoryId: String? = null,
    @SerializedName("category_name") val categoryName: String? = null,
    @SerializedName("color") val color: List<String>? = null,
    @SerializedName("currency") val currency: String? = null,
    @SerializedName("description") val description: Description? = null,
    @SerializedName("display_name") val displayName: String? = null,
    @SerializedName("features") val features: Features? = null,
    @SerializedName("final_price") val finalPrice: Double? = null,
    @SerializedName("gst_rate") val gstRate: Double? = null,
    @SerializedName("materials") val materials: Materials? = null,
    @SerializedName("meta_description") val metaDescription: String? = null,
    @SerializedName("mrp") val mrp: Double? = null,
    @SerializedName("price") val price: Double? = null,
    @SerializedName("product_id") val productId: Long? = null,
    @SerializedName("qty_capacity") val qtyCapacity: Int? = null,
    @SerializedName("segment_id") val segmentId: String? = null,
    @SerializedName("segment_name") val segmentName: String? = null,
    @SerializedName("segment_slug") val segmentSlug: String? = null,
    @SerializedName("slug") val slug: String? = null,
    @SerializedName("tags") val tags: List<String>? = null,
    @SerializedName("title") val title: String? = null,
    @SerializedName("variants") val variants: Variants? = null,
    @SerializedName("warehouse_id") val warehouseId: String? = null,
    @SerializedName("warranty") val warranty: Warranty? = null,
    @SerializedName("year") val year: Int? = null)

data class Description(@SerializedName("details") val details: String? = null)

data class Features(@SerializedName("text") val text: String? = null)

data class Materials(@SerializedName("type") val type: String? = null)

data class Variants(@SerializedName("aroma") val aroma: String? = null,
    @SerializedName("attributes") val attributes: Attributes? = null,
    @SerializedName("availability_status") val availabilityStatus: Int? = null,
    @SerializedName("car") val car: Car? = null,
    @SerializedName("car_dependent") val carDependent: Int? = null,
    @SerializedName("color") val color: String? = null,
    @SerializedName("compatibility") val compatibility: String? = null,
    @SerializedName("dimensions") val dimensions: Dimensions? = null,
    @SerializedName("discount_percent") val discountPercent: Double? = null,
    @SerializedName("final_price") val finalPrice: Double? = null,
    @SerializedName("gst_rate") val gstRate: Int? = null,
    @SerializedName("images") val images: List<String>? = null,
    @SerializedName("inventory_count") val inventoryCount: Int? = null,
    @SerializedName("is_google_yes") val isGoogleYes: Int? = null,
    @SerializedName("is_local_inventory") val isLocalInventory: Int? = null,
    @SerializedName("mrp") val mrp: Double? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("price") val price: Double? = null,
    @SerializedName("product_id") val productId: Int? = null,
    @SerializedName("sku_code") val skuCode: String? = null,
    @SerializedName("specifications") val specifications: Specifications? = null,
    @SerializedName("thumbnail_img") val thumbnailImg: String? = null,
    @SerializedName("unicommerce_skucode") val unicommerceSkucode: String? = null,
    @SerializedName("upc_code") val upcCode: String? = null,
    @SerializedName("volume") val volume: String? = null,
    @SerializedName("weight") val weight: String? = null,
    @SerializedName("xml_inclusion") val xmlInclusion: Int? = null)

data class Warranty(@SerializedName("condition") val condition: String? = null,
    @SerializedName("value") val value: String? = null)

data class Attributes(@SerializedName("category") val category: String? = null,
    @SerializedName("din size") val dinSize: String? = null,
    @SerializedName("screen size") val screenSize: String? = null,
    @SerializedName("segment") val segment: String? = null,
    @SerializedName("type") val type: String? = null)

data class Car(@SerializedName("brand") val brand: String? = null,
    @SerializedName("end_year") val endYear: Double? = null,
    @SerializedName("model") val model: String? = null,
    @SerializedName("model_id") val modelId: Double? = null,
    @SerializedName("start_year") val startYear: Double? = null)

data class Dimensions(@SerializedName("diameter") val diameter: Any? = null,
    @SerializedName("height") val height: Any? = null,
    @SerializedName("length") val length: Any? = null,
    @SerializedName("shape") val shape: String? = null,
    @SerializedName("unit") val unit: String? = null,
    @SerializedName("width") val width: Any? = null)

data class Specifications(@SerializedName("Brand") val brand: String? = null,
    @SerializedName("Colour") val colour: String? = null,
    @SerializedName("Fabric") val fabric: String? = null,
    @SerializedName("Light Emission") val lightEmission: String? = null,
    @SerializedName("Service Hours") val serviceHours: Double? = null,
    @SerializedName("Shape") val shape: String? = null,
    @SerializedName("Size") val size: String? = null,
    @SerializedName("Type") val type: String? = null)