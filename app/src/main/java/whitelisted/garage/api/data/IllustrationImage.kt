package whitelisted.garage.api.data

data class IllustrationImage(val image: Int)
