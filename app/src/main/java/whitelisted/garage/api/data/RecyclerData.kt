package whitelisted.garage.api.data

data class RecyclerData<T>(var position: Int, var data: T)
