package whitelisted.garage.api.data

data class SelectTabParams(var position: Int? = null, var navigate: Boolean? = null)