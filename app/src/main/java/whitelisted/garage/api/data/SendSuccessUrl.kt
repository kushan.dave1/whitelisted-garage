package whitelisted.garage.api.data

data class SendSuccessUrl(var url: String? = "", var isSuccessFailure: Boolean? = false)