package whitelisted.garage.api.data

import android.net.Uri

data class IdUriModel(var uri: Uri? = Uri.EMPTY, var id: String? = "", var position: Int = -1)