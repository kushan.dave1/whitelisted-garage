package whitelisted.garage.api.data

data class CarNameOrderIdStatusModel(var orderId: String? = "",
    var carName: String? = "",
    var status: String? = "")