package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class RetailCreateOrderRequest(

    @field:SerializedName("address") var address: String? = null,

    @field:SerializedName("tax_type") var taxType: String? = null,

    @field:SerializedName("name") var name: String? = null,

    @field:SerializedName("mobile") var mobile: String? = null,

    @field:SerializedName("save") var save: Boolean? = null,

    @field:SerializedName("bill_type") var billType: String? = null,

    @field:SerializedName("bill_name") var billName: String? = null,

    @field:SerializedName("gstin") var gstin: String? = null,

    @field:SerializedName("order_id") var orderId: String? = null,

    @field:SerializedName("mechanic_id") var mechanicId: String? = "",

    @field:SerializedName("mechanic_name") var mechanicName: String? = "",

    @field:SerializedName("email") var email: String? = null)
