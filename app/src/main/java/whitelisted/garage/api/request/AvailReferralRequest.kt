package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class AvailReferralRequest(
    @SerializedName("avail_referal_code") var ref_code: String? = null,
)
