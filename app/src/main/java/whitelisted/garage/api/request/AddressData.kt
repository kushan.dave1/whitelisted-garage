package whitelisted.garage.api.request

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddressData(
    var latitude: Double? = null,
    var longitude: Double? = null,
    var fullAddress: String? = null,
) : Parcelable