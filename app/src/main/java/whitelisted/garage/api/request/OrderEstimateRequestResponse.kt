package whitelisted.garage.api.request

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import whitelisted.garage.view.fragments.orderInventory.PdfStatusDataItem

data class OrderEstimateRequestResponse(

    @field:SerializedName("services") var orderEstimateItems: MutableList<OEServicesItem>? = mutableListOf(),

    @field:SerializedName("packages") var packages: MutableList<Packages>? = mutableListOf(),

    @field:SerializedName("order_id") var orderId: String? = "",

    @field:SerializedName("pdf_status") var pdfStatus: PdfStatusDataItem? = PdfStatusDataItem(),

    @field:SerializedName("job_card_pdf_status") var jobCardPdfStatus: PdfStatusDataItem? = PdfStatusDataItem(),

    @field:SerializedName("bill_amount") var billAmount: Double? = 0.0) : Parcelable {
    constructor(parcel: Parcel) : this(TODO("orderEstimateItems"),
        TODO("packages"),
        parcel.readString(),
        TODO("pdfStatus"),
        TODO("jobCardPdfStatus"),
        parcel.readValue(Double::class.java.classLoader) as? Double)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(orderId)
        parcel.writeValue(billAmount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrderEstimateRequestResponse> {
        override fun createFromParcel(parcel: Parcel): OrderEstimateRequestResponse {
            return OrderEstimateRequestResponse(parcel)
        }

        override fun newArray(size: Int): Array<OrderEstimateRequestResponse?> {
            return arrayOfNulls(size)
        }
    }
}

data class OEServicesItem(

    @field:SerializedName("total") var total: Double? = 0.0,

    @field:SerializedName("quantity") var quantity: Int? = 1,

    @field:SerializedName("total_quantity") var totalQuantity: Int? = 1,

    @field:SerializedName("total_stock_left") var totalStockLeft: Int? = 0,

    @field:SerializedName("sku_id") var skuId: String? = "0",

    @field:SerializedName("id") var id: String? = "",

    @field:SerializedName("work_done_name") var workDoneName: String? = "",

    @field:SerializedName("part_already_added") var partAlreadyAdded: Boolean? = false,

    @field:SerializedName("type") var type: String? = "",

    @field:SerializedName("taxable") var taxable: Double? = 0.0,

    @field:SerializedName("service_name") var serviceName: String? = "",

    @field:SerializedName("brand_name") var brandName: String? = "",

    @field:SerializedName("rack_item_id") var rackItemId: String? = null,

    @field:SerializedName("service_id") var serviceId: Long? = 0,

    @field:SerializedName("work_done") var workDone: Long? = 0,

    @field:SerializedName("description") var description: String? = "",

    @field:SerializedName("hsn") var hsn: String? = "",

    @field:SerializedName("price_per_quantity") var pricePerQuantity: Double? = 0.0,

    @field:SerializedName("discount_per_quantity") var discountPerQuantity: Double? = 0.0,

    @field:SerializedName("tax_rate") var taxRate: String? = "0%") : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? String,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(total)
        parcel.writeValue(quantity)
        parcel.writeValue(totalQuantity)
        parcel.writeValue(totalStockLeft)
        parcel.writeValue(skuId)
        parcel.writeValue(id)
        parcel.writeString(workDoneName)
        parcel.writeValue(partAlreadyAdded)
        parcel.writeString(type)
        parcel.writeValue(taxable)
        parcel.writeString(serviceName)
        parcel.writeString(brandName)
        parcel.writeString(rackItemId)
        parcel.writeValue(serviceId)
        parcel.writeValue(workDone)
        parcel.writeString(description)
        parcel.writeString(hsn)
        parcel.writeValue(pricePerQuantity)
        parcel.writeValue(discountPerQuantity)
        parcel.writeString(taxRate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OEServicesItem> {
        override fun createFromParcel(parcel: Parcel): OEServicesItem {
            return OEServicesItem(parcel)
        }

        override fun newArray(size: Int): Array<OEServicesItem?> {
            return arrayOfNulls(size)
        }
    }
}