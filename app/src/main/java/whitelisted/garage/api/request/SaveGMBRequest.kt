package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName


data class SaveGMBRequest(@SerializedName("email_id") var email_id: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("gmb_mobile") var gmbMobile: String? = null,
    @SerializedName("gmb_attributes") var gmbAttributes: MutableMap<String, MutableList<String>>? = null,
    @SerializedName("gmb_categories") var gmbCategories: List<String>? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("open_time") var openTime: MutableMap<String, List<String>>? = null,
    @SerializedName("workshop_images") var workshopImages: List<String>? = null,
    @SerializedName("latitude") var latitude: Double? = null,
    @SerializedName("longitude") var longitude: Double? = null)

