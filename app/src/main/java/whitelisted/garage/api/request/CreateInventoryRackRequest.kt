package whitelisted.garage.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


data class CreateInventoryRackRequest(@SerializedName("rack_items") var rackItems: List<RackItem>? = null,
    @SerializedName("rack_name") var rackName: String? = null,
    @SerializedName("images") var images: MutableList<String>? = null,
    var isNewRack: Boolean = false,
    var isUpdate: Boolean = false,
    var id: String? = null)

@Parcelize
data class RackItem(
    @SerializedName("brand_id") var brandId: Int? = null,
    @SerializedName("brand_name") var brandName: String? = "",
    @SerializedName("fuel_type") var fuel_type: String? = null,
    @SerializedName("buying_price") var buyingPrice: Double? = null,
    @SerializedName("selling_price") var selling_price: Double? = null,
    @SerializedName("car_model") var carModel: String? = null,
    @SerializedName("mf_year") var mf_year: String? = null,
    @SerializedName("car_model_id") var carModelId: Int? = null,
    @SerializedName("part_name") var partName: String? = null,
    @SerializedName("rack_name") var rackName: String? = null,
    @SerializedName("rack_item_id") var rackItemId: String? = null,
    @SerializedName("quantity") var quantity: Int? = 0,
    @SerializedName("selected_qty") var selectedQty: Int? = 0,
    @SerializedName("vehicle_type") var vehicleType: Int = 4,
    @SerializedName("sku_id") var skuId: String? = null,
    @SerializedName("id") var id: String? = null,
    @SerializedName("images") var images: List<String>? = null,
) : Parcelable