package whitelisted.garage.api.request

import android.net.Uri

data class BiddinngPhotoModel(var isInternal: Boolean, var uri: Uri)
