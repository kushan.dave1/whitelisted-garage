package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class ResetPasswordRequest(@field:SerializedName("user_id") var mobile: String? = null,

    @field:SerializedName("new_password") var password: String? = null)
