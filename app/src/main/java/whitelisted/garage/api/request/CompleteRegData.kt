package whitelisted.garage.api.request

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CompleteRegData(var address: String? = null,
    var name: String? = null,
    var addressData: AddressData? = null,
    var latitude: Double? = 0.0,
    var longitude: Double? = 0.0) : Parcelable
