package whitelisted.garage.api.request

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DayTimeData(var timeList: MutableList<StartAndEndTimeData>) : Parcelable


@Parcelize
data class StartAndEndTimeData(var startTime: String,
    var endTime: String,
    var days: String,
    var isEnable: Boolean) : Parcelable