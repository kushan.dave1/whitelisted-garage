package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName
import whitelisted.garage.view.fragments.orderInventory.PdfStatusDataItem

data class JobCardRequest(

    @field:SerializedName("services") var jobCardItems: MutableList<JobCardItem>? = mutableListOf(),

    @field:SerializedName("packages") var packages: MutableList<Packages>? = mutableListOf(),

    @field:SerializedName("pdf_status") var pdfStatus: PdfStatusDataItem? = PdfStatusDataItem(),

    @field:SerializedName("order_id") var orderId: String? = "")

data class JobCardItem(

    @field:SerializedName("work_done_name") var workDoneName: String? = "",

    @field:SerializedName("service_name") var serviceName: String? = "",

    @field:SerializedName("type") var type: String? = "",

    @field:SerializedName("total_quantity") var totalQuantity: Int? = 1,

    @field:SerializedName("price_per_quantity") var pricePerQuantity: Double? = 0.0,

    @field:SerializedName("total") var total: Double? = 0.0,

    @field:SerializedName("quantity") var quantity: Int? = 1,

    @field:SerializedName("rack_item_id") var rackItemId: String? = null,

    @field:SerializedName("service_id") var serviceId: Long? = 0,

    @field:SerializedName("sku_id") var skuId: String? = "0",

    @field:SerializedName("work_done") var workDone: Long? = 0,

    @field:SerializedName("description") var description: String? = "",

    @field:SerializedName("order_id") var orderId: String? = "")

data class Packages(

    @field:SerializedName("package_name") var packageName: String? = "",

    @field:SerializedName("id") var id: String? = "",

    @field:SerializedName("package_id") var packageId: Long? = null,

    //	@field:SerializedName("package_id")
    //	var packageIdString: String? = "",

    @field:SerializedName("package_total") var packageTotal: Double? = 0.0)
