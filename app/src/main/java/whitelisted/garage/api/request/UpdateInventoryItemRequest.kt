package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class UpdateInventoryItemRequest(

    @field:SerializedName("sku_id") var sku_id: String? = "",

    @field:SerializedName("id") var id: String? = "",

    @field:SerializedName("selling_price") var selling_price: Double? = 0.0,

    @field:SerializedName("buying_price") var buying_price: Double? = 0.0,

    )