package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class LedgerIdRequest(
    @field:SerializedName("ledger_id") var ledgerId: String? = "",
)
