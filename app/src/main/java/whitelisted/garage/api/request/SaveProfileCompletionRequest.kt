package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class SaveProfileCompletionRequest(
    @SerializedName("address") var address: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("gstin") var gstin: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("joining_date") var joiningDate: String? = null,
    @SerializedName("mobile") var mobile: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("role") var role: String? = null,
    @SerializedName("workshop_name") var workshopName: String? = null,
    @SerializedName("map_link") var mapLink: String? = null,
    @SerializedName("latitude") var latitude: Double? = null,
    @SerializedName("longitude") var longitude: Double? = null,
)
