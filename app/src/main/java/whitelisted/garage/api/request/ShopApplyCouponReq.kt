package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class ShopApplyCouponReq(
    @field:SerializedName("id")
    val id: String? = "",
    @field:SerializedName("code")
    val code:String? = "",
    @field:SerializedName("type")
    val type:String? = ""
)