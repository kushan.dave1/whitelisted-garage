package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName
import whitelisted.garage.api.response.AddressResponseModel

data class SSCheckoutRequest(
    @field:SerializedName("razorpay_order_id")
    var  razorPayOrderId: String? = "",
    @field:SerializedName("razorpay_payment_id")
    var  razorPayPaymentId: String? = "",
    @field:SerializedName("payment_amount")
    var  paymentAmount: String? = "",
    @field:SerializedName("payment_type")
    var paymentType: String = "online",
    @field:SerializedName("payment_status")
    var paymentStatus: String = "now",
    @field:SerializedName("payment_currency")
    var  paymentCurrency: String? = "",
    @field:SerializedName("cart_id")
    var  cartId: String? = "",
    @field:SerializedName("client_customer_details")
    var  clientAddressDetails: AddressResponseModel? = AddressResponseModel()
)