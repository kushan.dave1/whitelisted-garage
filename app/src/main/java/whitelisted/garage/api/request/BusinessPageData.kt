package whitelisted.garage.api.request

data class BusinessPageData(
    var name: String? = null,
    var mobile: String? = null,

    )