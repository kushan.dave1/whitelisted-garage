package whitelisted.garage.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


data class WorkShopBiddingRequest(@SerializedName("brand_wise_items") var brandWiseItems: MutableList<BrandWiseItem>? = null,
    @SerializedName("generic_items") var genericItems: MutableList<GenericItem>? = null,
    @SerializedName("total_item") var totalItem: Long? = null)

@Parcelize
data class BrandWiseItem(
    @SerializedName("brand_id") var brandId: Long? = null,
    @SerializedName("brand_name") var brandName: String? = null,
    @SerializedName("buying_price") var buyingPrice: Long? = null,
    @SerializedName("car_model") var carModel: String? = null,
    @SerializedName("car_model_id") var carModelId: Long? = null,
    @SerializedName("fuel_type") var fuelType: String? = null,
    @SerializedName("images") var images: List<String>? = null,
    @SerializedName("mf_year") var mfYear: String? = null,
    @SerializedName("part_name") var partName: String? = null,
    @SerializedName("quantity") var quantity: Long? = null,
    @SerializedName("quantity_available") var quantity_available: Long? = null,
    @SerializedName("part_availability") var part_availability: Int? = null,
    @SerializedName("sku_id") var skuId: Long? = null,
    @SerializedName("remark") var remark: String? = null,
    @SerializedName("is_available") var is_available: Boolean? = null,
    @SerializedName("price_per_item") var price_per_item: String? = null, // taG FOR SEPARATION


) : Parcelable

@Parcelize
data class GenericItem(
    @SerializedName("brand_id") var brandId: Long? = -999999,
    @SerializedName("brand_name") var brandName: String? = "Generic",
    @SerializedName("price_per_item") var price_per_item: String? = null,
    @SerializedName("buying_price") var buyingPrice: Long? = null,
    @SerializedName("car_model") var carModel: String? = "Generic",
    @SerializedName("car_model_id") var carModelId: Long? = -999999,
    @SerializedName("fuel_type") var fuelType: String? = "Generic",
    @SerializedName("images") var images: List<String>? = null,
    @SerializedName("mf_year") var mfYear: String? = null,
    @SerializedName("part_name") var partName: String? = null,
    @SerializedName("quantity") var quantity: Long? = null,
    @SerializedName("part_availability") var part_availability: Int? = null,
    @SerializedName("quantity_available") var quantity_available: Long? = null,
    @SerializedName("sku_id") var skuId: Long? = null, // taG FOR SEPARATION
    @SerializedName("remark") var remark: String? = null,
    @SerializedName("is_available") var is_available: Boolean? = null,
    @SerializedName("is_added") var isAdded: Boolean? = null,
) : Parcelable