package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class CreateCustomPackageRequest(

    @field:SerializedName("total") var total: Double? = 0.0,
    @field:SerializedName("package_id") var package_id: String? = null,
    @field:SerializedName("name") var name: String? = "",

    @field:SerializedName("description") var description: String? = "",

    @field:SerializedName("services") var services: MutableList<ServicesItem>? = mutableListOf(),
    @field:SerializedName("items") var items: MutableList<ServicesItem>? = mutableListOf()

)

data class ServicesItem(

    @field:SerializedName("price_per_item") var pricePerItem: Double? = 0.0,

    @field:SerializedName("total") var total: Double? = 0.0,

    @field:SerializedName("quantity") var quantity: Int? = 1,

    @field:SerializedName("service_name") var serviceName: String? = "",
    @field:SerializedName("part_name") var part_name: String? = "",

    @field:SerializedName("sku_id") var skuId: String? = "",

    @field:SerializedName("tax_rate") var taxRate: String? = "18%",

    @field:SerializedName("service_id") var serviceId: Long? = 0,

    @field:SerializedName("work_done_id") var workDoneId: Int? = 0,

    @field:SerializedName("work_done") var workDone: String? = "")
