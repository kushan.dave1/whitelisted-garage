package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class AddEditEmployee(

    @field:SerializedName("name") var name: String? = null,

    @field:SerializedName("user_role") var userRole: String? = null,

    @field:SerializedName("mobile") var mobile: String? = null,

    @field:SerializedName("email") var email: String? = null,

    @field:SerializedName("password") var password: String? = null,

    @field:SerializedName("joining_date") var joiningDate: String? = null,

    @field:SerializedName("image") var image: String? = null)