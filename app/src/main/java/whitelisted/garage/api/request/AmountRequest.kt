package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class AmountRequest(
    @field:SerializedName("amount")
    val amount: Double? = 0.0,

    @field:SerializedName("cart_id")
    val cartId: String? = null
)