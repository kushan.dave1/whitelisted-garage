package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

class SaveUPIIdRequest(@field:SerializedName("upi_id") var upiId: String? = null,

    @field:SerializedName("first_time") var firstTime: Boolean? = false)