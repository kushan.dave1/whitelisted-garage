package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName


data class SendInstantReminderRequest(
    @SerializedName("amount") var amount: Double? = null,
    @SerializedName("country_code") var countryCode: String? = null,
    @SerializedName("customer_name") var customerName: String? = null,
    @SerializedName("mobile") var mobile: String? = null,
    @SerializedName("text_msg") var textMsg: String? = null,
    @SerializedName("language") var language: String? = null,
)