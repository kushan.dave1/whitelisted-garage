package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

class UpdateLocationRequest(@SerializedName("city") var city: String? = "",
    @SerializedName("state") var state: String? = "",
    @SerializedName("country") var country: String? = "",
    @SerializedName("latitude") var latitude: Double? = 0.0,
    @SerializedName("longitude") var longitude: Double? = 0.0)