package whitelisted.garage.api.request

import android.net.Uri
import com.google.gson.annotations.SerializedName
import java.io.File

class UploadItem(

    @field:SerializedName("file") var file: File? = null,

    @field:SerializedName("uri") var uri: Uri? = null,

    @field:SerializedName("file_type") // 0->exteriorImage, 1->interiorImage 2->sign 3->audio
    var fileType: Int? = null

)