package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class UpdatePackageNameRequest(

    @field:SerializedName("total") var total: Double? = null,

    @field:SerializedName("name") var name: String? = null,

    @field:SerializedName("description") var description: String? = "")
