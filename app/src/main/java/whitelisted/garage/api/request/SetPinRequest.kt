package whitelisted.garage.api.request

data class SetPinRequest(

    var pin: String? = "")