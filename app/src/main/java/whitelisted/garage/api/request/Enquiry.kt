package whitelisted.garage.api.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Enquiry(
    @field:SerializedName("remarks") var remarks: String = "",
    @field:SerializedName("photos") var photos: List<String> = listOf(),
    @field:SerializedName("quantity") var quantity: Int = 0,
    @field:SerializedName("product_name") var productName: String? = "",
    @field:SerializedName("product_brand") var productBrand: String? = "",
    @field:SerializedName("product_image") var productImage: String? = "",
    @field:SerializedName("product_id") var productID: String? = "",
    @field:SerializedName("price") var price: Double? = 0.0,
    @field:SerializedName("created_at") var createdAt: String? = "",
    @field:SerializedName("vehicle") var vehicle: String? = "",
    @field:SerializedName("vehicle_image") var vehicleImage: String? = "",
    @field:SerializedName("fuel_type") var fuelType: String? = ""

): Parcelable
