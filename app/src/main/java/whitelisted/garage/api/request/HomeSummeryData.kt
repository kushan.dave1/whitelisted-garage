package whitelisted.garage.api.request

data class HomeSummeryData(var ordersCount: String,
    var openTotal: String,
    var inProgressPending: String,
    var completePaid: String,
    var isOrderSummery: Boolean)
