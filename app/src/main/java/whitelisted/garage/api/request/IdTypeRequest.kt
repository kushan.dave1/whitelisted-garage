package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class IdTypeRequest(@field:SerializedName("id") var id: String? = "",
    @field:SerializedName("type") var type: String? = "")