package whitelisted.garage.api.request

import whitelisted.garage.api.response.AllPackagesResponseModel

data class JobCardPackageModel(var packageModel: AllPackagesResponseModel? = AllPackagesResponseModel(),

    var inclusivesList: MutableList<String>? = mutableListOf())
