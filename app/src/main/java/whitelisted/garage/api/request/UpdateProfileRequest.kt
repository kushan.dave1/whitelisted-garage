package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName


data class UpdateProfileRequest
    (
    @field:SerializedName("name")

    var name: String? = null,
    @field:SerializedName("role")

    var role: String? = null,

    @field:SerializedName("mobile")

    var mobile: String? = null,

    @field:SerializedName("email")

    var email: Any? = null,

    @field:SerializedName("password")

    var password: String? = null,

    @field:SerializedName("user_id")

    var userId: Long? = null,

    @field:SerializedName("created_at")

    var createdAt: String? = null,

    @field:SerializedName("updated_at")

    var updatedAt: String? = null,

    @field:SerializedName("mobile_verified")

    var mobileVerified: Boolean? = null,

    @field:SerializedName("deleted_at")

    var deletedAt: Any? = null,

    @field:SerializedName("roles")

    var roles: List<String>? = null,

    @field:SerializedName("service_workshops")

    var serviceWorkshops: Any? = null,

    @field:SerializedName("address")

    var address: Any? = null,

    @field:SerializedName("last_service_date")

    var lastServiceDate: Any? = null,

    @field:SerializedName("workshops")

    var workshops: Any? = null,

    @field:SerializedName("updated_by_id")

    var updatedById: Long? = null,

    @field:SerializedName("created_by_id")

    var createdById: Long? = null,

    @field:SerializedName("owner_id")

    var ownerId: Long? = null,

    @field:SerializedName("image")

    var image: Any? = null,

    @field:SerializedName("user_type")

    var userType: Any? = null,
    @field:SerializedName("joining_date")

    var joiningDate: String? = null,
)