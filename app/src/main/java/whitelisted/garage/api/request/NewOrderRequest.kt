package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class NewOrderRequest(@field:SerializedName("name") var name: String? = "",

    @field:SerializedName("order_id") var orderId: String? = "",

    @field:SerializedName("mobile") var phoneNumber: String? = "",

    @field:SerializedName("city") var city: String? = "",

    @field:SerializedName("email") var email: String? = "",

    @field:SerializedName("address") var address: String? = "",

    @field:SerializedName("bill_type") var billType: String? = "",

    @field:SerializedName("tax_type") var taxType: String? = "",

    @field:SerializedName("display_gst_in") var gstinDisplay: String? = "",

    @field:SerializedName("registration_number") var regNumber: String? = "",

    @field:SerializedName("odometer") var odometerReading: String? = "",

    @field:SerializedName("car") var car: String? = "",

    @field:SerializedName("car_icon") var carIcon: String? = "",

    @field:SerializedName("brand_id") var brandId: Int? = 0,

    @field:SerializedName("model_id") var modelId: Int? = 0,

    @field:SerializedName("car_type_id") var carTypeId: Int? = 0,

    @field:SerializedName("fuel_type") var fuelType: String? = "",

    @field:SerializedName("transmission") var transmission: Int? = 0,

    @field:SerializedName("breaking_system") var breakingSystem: Int? = 1,

    @field:SerializedName("mechanic_id") var mechanicId: String? = "",

    @field:SerializedName("mechanic_name") var mechanicName: String? = "",

    @field:SerializedName("country_code") var countryCode: String? = "",

    @field:SerializedName("is_two_wheeler") var isTwoWheeler: Int = 4,

    @field:SerializedName("brand_name") var brandName: String? = "")