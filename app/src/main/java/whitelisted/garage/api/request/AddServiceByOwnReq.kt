package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class AddServiceByOwnReq(
    @SerializedName("name") val name: String
)
