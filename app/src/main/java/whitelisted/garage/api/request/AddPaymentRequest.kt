package whitelisted.garage.api.request

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class AddPaymentRequest(

    @field:SerializedName("transaction_mode") var transactionMode: String? = "",

    @field:SerializedName("amount") var amount: Double? = 0.0,

    @field:SerializedName("mobile") var mobile: String? = "",

    @field:SerializedName("transaction_type") var transactionType: String? = null,

    @field:SerializedName("discount_value") var discountValue: Double? = 0.0,

    @field:SerializedName("order_id") var orderId: String? = "") : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(transactionMode)
        parcel.writeValue(amount)
        parcel.writeString(mobile)
        parcel.writeString(transactionType)
        parcel.writeValue(discountValue)
        parcel.writeString(orderId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AddPaymentRequest> {
        override fun createFromParcel(parcel: Parcel): AddPaymentRequest {
            return AddPaymentRequest(parcel)
        }

        override fun newArray(size: Int): Array<AddPaymentRequest?> {
            return arrayOfNulls(size)
        }
    }
}