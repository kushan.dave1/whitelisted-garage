package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

class AddWorkshopRequest(

    @field:SerializedName("name") var workshopName: String? = "",

    @field:SerializedName("address") var address: String? = "",

    @field:SerializedName("gstin") var gstIn: String? = "",

    @field:SerializedName("types") var types: String? = "",

    @field:SerializedName("map_link") var mapLink: String? = "",

    @field:SerializedName("invoice_id") var invoiceName: String? = "",

    @field:SerializedName("owner_name") var ownerName: String? = "",

    @field:SerializedName("owner_id") var ownerId: Long? = 0,

    @field:SerializedName("latitude") var latitude: Double? = 0.0,

    @field:SerializedName("longitude") var longitude: Double? = 0.0,

    @field:SerializedName("city") var city: String? = "",

    @field:SerializedName("pin") var pin: String? = "",

    @field:SerializedName("state") var state: String? = "",

    @field:SerializedName("owner_type") var ownerType: String? = "",

    @field:SerializedName("country") var country: String? = "")