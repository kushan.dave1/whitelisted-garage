package whitelisted.garage.api.request

data class AttributeData(var list: MutableList<AttributeListData>)

data class AttributeListData(var attributeName: String, var atrChildList: MutableList<Attribute>)

data class Attribute(var name: String, var isEnable: Boolean)


