package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName


data class AddToLedgerRequest
    (
    @field:SerializedName("amount") val amount: Double? = null,

    @field:SerializedName("type")

    val type: String? = null,

    @field:SerializedName("remark")

    val remark: String? = null,

    @field:SerializedName("name")

    val name: String? = null,
    @field:SerializedName("date")

    val date: String? = null,
)