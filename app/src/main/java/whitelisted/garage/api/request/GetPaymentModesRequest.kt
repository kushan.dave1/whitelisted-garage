package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName
import whitelisted.garage.api.response.AddressResponseModel

data class GetPaymentModesRequest(

    @field:SerializedName("cart_id") var cartId: String? = "",

    @field:SerializedName("cus_address") var cus_address: AddressResponseModel? = AddressResponseModel()

)