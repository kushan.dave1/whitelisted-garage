package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class CancelOrderRequest(

    @field:SerializedName("status_id") var statusId: Int? = 130,

    @field:SerializedName("order_id") var orderId: String? = "")
