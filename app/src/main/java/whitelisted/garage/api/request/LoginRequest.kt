package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class LoginRequest(

    @field:SerializedName("user_id") var mobile: String? = null,

    @field:SerializedName("password") var password: String? = null)