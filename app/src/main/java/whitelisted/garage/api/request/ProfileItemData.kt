package whitelisted.garage.api.request

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


data class ProfileItemData(var dataListHeader: List<Int>? = null,
    var dataListConstant: List<String>? = null,
    var dataListDes: List<String>? = null,
    var image: List<Int>? = null)

@Parcelize
data class ProfileNavigationData(
    var constant: String? = null,
    var header: String? = null,
    var value: String? = null,

    ) : Parcelable