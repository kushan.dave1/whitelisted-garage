package whitelisted.garage.api.request

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MyCustomerModel(

    @field:SerializedName("name") val name: String? = null,
    @field:SerializedName("registration_number") val registration_number: String? = null,
    @field:SerializedName("odometer") var odometerReading: String? = "",
    @field:SerializedName("mobile") val mobile: String? = null,
    @field:SerializedName("brand_name") var brandName: String? = "",
    @field:SerializedName("email") val email: String? = null,
    @field:SerializedName("user_id") val userId: String? = null,
    @field:SerializedName("orders") val orderId: String? = null,
    @field:SerializedName("car_name") val carName: String? = null,
    @field:SerializedName("mechanic_name") val mechanicName: String? = null,
    @field:SerializedName("mechanic_id") val mechanicId: String? = null,
    @field:SerializedName("car_type_id") var carTypeId: Int? = 0,
    @field:SerializedName("fuel_type") var fuelType: String? = "",
    @field:SerializedName("address") val address: String? = null,
    @field:SerializedName("last_service_date") val lastVisit: String? = null,
    @field:SerializedName("car_icon") val car_icon: String? = null,
    @field:SerializedName("car_brand") val car_brand: Int? = null,
    @field:SerializedName("car_model") val car_model: Int? = null,
    @field:SerializedName("workshop_visit_count") val workshopVisits: Int? = null,
    @field:SerializedName("save") val isSave: Boolean? = false,
    @field:SerializedName("due_amount") val dueAmount: Double? = null,
    @field:SerializedName("lifetime_amount") var lifetime_amount: Double? = null,
    @field:SerializedName("bill_name") val bill_name: String? = null,
    @field:SerializedName("country_code") val country_code: String? = null,
    @SerializedName("is_two_wheeler") val isTwoWheeler: Int = 4,
    @field:SerializedName("order_details") var orderDetails: OrderDetails? = OrderDetails(),
    var isExpanded: Boolean = false) : Parcelable

@Parcelize
data class Services(

    @SerializedName("brand_name") var brandName: String? = null,
    @SerializedName("description") var description: String? = null,
    @SerializedName("discount_per_quantity") var discountPerQuantity: Double? = null,
    @SerializedName("hsn") var hsn: String? = null,
    @SerializedName("id") var id: String? = null,
    @SerializedName("part_already_added") var partAlreadyAdded: Boolean? = null,
    @SerializedName("price_per_quantity") var pricePerQuantity: Double? = null,
    @SerializedName("quantity") var quantity: Double? = null,
    @SerializedName("rack_item_id") var rackItemId: String? = null,
    @SerializedName("service_id") var serviceId: String? = null,
    @SerializedName("service_name") var serviceName: String? = null,
    @SerializedName("sku_id") var skuId: String? = null,
    @SerializedName("tax_rate") var taxRate: String? = null,
    @SerializedName("taxable") var taxable: Double? = null,
    @SerializedName("total") var total: Double? = null,
    @SerializedName("total_quantity") var totalQuantity: Double? = null,
    @SerializedName("total_stock_left") var totalStockLeft: Double? = null,
    @SerializedName("type") var type: String? = null,
    @SerializedName("work_done") var workDone: Double? = null,
    @SerializedName("work_done_name") var workDoneName: String? = null,
    @SerializedName("discount_per_item") var discountPerItem: String? = null,
    @SerializedName("tax") var tax: Double? = null) : Parcelable

data class Packages1(@SerializedName("id") var id: String? = null,
    @SerializedName("package_id") var packageId: String? = null,
    @SerializedName("package_name") var packageName: String? = null,
    @SerializedName("package_total") var packageTotal: Double? = null) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(packageId)
        parcel.writeString(packageName)
        parcel.writeValue(packageTotal)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Packages1> {
        override fun createFromParcel(parcel: Parcel): Packages1 {
            return Packages1(parcel)
        }

        override fun newArray(size: Int): Array<Packages1?> {
            return arrayOfNulls(size)
        }
    }
}

data class OrderDetails(@SerializedName("services") var services: ArrayList<Services> = arrayListOf(),
    @SerializedName("packages") var packages: ArrayList<Packages1> = arrayListOf(),
    @SerializedName("bill_amount") var billAmount: Double? = null) : Parcelable {
    constructor(parcel: Parcel) : this(TODO("services"),
        TODO("packages"),
        parcel.readValue(Double::class.java.classLoader) as? Double) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(billAmount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrderDetails> {
        override fun createFromParcel(parcel: Parcel): OrderDetails {
            return OrderDetails(parcel)
        }

        override fun newArray(size: Int): Array<OrderDetails?> {
            return arrayOfNulls(size)
        }
    }
}