package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName


data class MarkCompleteWsBidRequest(@SerializedName("id") var id: String? = null,
    @SerializedName("type") var type: String? = "completed")