package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

class BidStatusRequest(
    @field:SerializedName("name") var name: Int? = null,
)