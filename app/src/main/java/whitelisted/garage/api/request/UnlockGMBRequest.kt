package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName


data class UnlockGMBRequest(
    @SerializedName("remark") var remark: String? = "Unlock GMB",
    @SerializedName("source") var source: String? = "GMB Unlock",
    @SerializedName("type") var type: String? = "debit",
    @SerializedName("value") var value: Double? = null,
    @SerializedName("coupon") var coupon: String? = null,

    )