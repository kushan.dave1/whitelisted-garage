package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class SignUpRequest(

    @field:SerializedName("password") var password: String? = "",

    @field:SerializedName("workshop_name") var workshopName: String? = "",

    @field:SerializedName("mobile") var mobile: String? = "",

    @field:SerializedName("country_code") var countryCode: String? = "",

    @field:SerializedName("name") var ownerName: String? = "",

    @field:SerializedName("types") var types: List<String>? = mutableListOf(),

    @field:SerializedName("otp") var otp: String? = "",

    @field:SerializedName("workshop_address") var workshopAddress: String? = "",

    @field:SerializedName("workshop_gstin") var workshopGstNo: String? = "",

    @field:SerializedName("owner_type") var ownerType: String? = "",

    @field:SerializedName("workshop_map_link") var workshopMapLink: String? = "")
