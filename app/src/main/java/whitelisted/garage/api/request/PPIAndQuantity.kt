package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

class PPIAndQuantity(@field:SerializedName("price_per_item") var pricePerItem: Double? = 0.0,

    @field:SerializedName("quantity") var quantity: Int? = 0,

    @field:SerializedName("tax_rate") var taxRate: String? = "18%",

    @field:SerializedName("totalAmount") var totalAmount: Double? = 0.0)