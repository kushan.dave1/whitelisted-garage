package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class VerifyOtpRequest(

    @field:SerializedName("mobile") var mobile: String? = null,

    @field:SerializedName("country_code") var countryCode: String? = null,

    @field:SerializedName("otp") var otp: String? = null,

    @field:SerializedName("action") var action: String? = null,
)