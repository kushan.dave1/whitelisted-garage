package whitelisted.garage.api.request

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class OrderInclusionsRequestResponse(

    @field:SerializedName("bill_amount") var billAmount: Double? = 0.0,

    @field:SerializedName("services") var services: MutableList<OrderInclusionsItem>? = mutableListOf(),

    @field:SerializedName("order_id") var orderId: String? = null,

    @field:SerializedName("pdf_status") var pdfStatus: PdfStatus? = null)

data class PdfStatus(

    @field:SerializedName("pdf_file") var pdfFile: String? = null,

    @field:SerializedName("status") var status: Boolean? = false)

data class OrderInclusionsItem(

    @field:SerializedName("hsn") var hsn: String? = "",

    @field:SerializedName("rack_item_id") var rackItemId: String? = null,

    @field:SerializedName("total") var total: Double? = 0.0,

    @field:SerializedName("total_quantity") var totalQuantity: Int? = 1,

    @field:SerializedName("part_already_added") var partAlreadyAdded: Boolean? = false,

    @field:SerializedName("quantity") var quantity: Int? = 1,
    @field:SerializedName("brand_id") var brandId: Int? = 0,
    @field:SerializedName("model_id") var modelId: Int? = 0,
    @field:SerializedName("car_id") var carId: Int? = 0,

    @field:SerializedName("taxable") var taxable: Double? = 0.0,

    @field:SerializedName("total_stock_left") var totalStockLeft: Int? = 0,

    @field:SerializedName("service_name") var serviceName: String? = "",

    @field:SerializedName("brand_name") var brandName: String? = "",

    @field:SerializedName("model_name") var modelname: String? = "",

    @field:SerializedName("fuel_type") var fuelType: String? = "",

    @field:SerializedName("price_per_quantity") var pricePerQuantity: Double? = 0.0,

    @field:SerializedName("discount_per_quantity") var discountPerQuantity: Double? = 0.0,

    @field:SerializedName("sku_id") var skuId: String? = "0",

    @field:SerializedName("type") var type: String? = "",

    @field:SerializedName("tax_rate") var taxRate: String? = "0%") : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString(),
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Int::class.java.classLoader) as? String,
        parcel.readString(),
        parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(hsn)
        parcel.writeString(rackItemId)
        parcel.writeValue(total)
        parcel.writeValue(totalQuantity)
        parcel.writeValue(partAlreadyAdded)
        parcel.writeValue(quantity)
        parcel.writeValue(brandId)
        parcel.writeValue(modelId)
        parcel.writeValue(carId)
        parcel.writeValue(taxable)
        parcel.writeValue(totalStockLeft)
        parcel.writeString(serviceName)
        parcel.writeString(brandName)
        parcel.writeString(modelname)
        parcel.writeString(fuelType)
        parcel.writeValue(pricePerQuantity)
        parcel.writeValue(discountPerQuantity)
        parcel.writeValue(skuId)
        parcel.writeString(type)
        parcel.writeString(taxRate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrderInclusionsItem> {
        override fun createFromParcel(parcel: Parcel): OrderInclusionsItem {
            return OrderInclusionsItem(parcel)
        }

        override fun newArray(size: Int): Array<OrderInclusionsItem?> {
            return arrayOfNulls(size)
        }
    }
}