package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class TrueCallerLoginRequest(

    @field:SerializedName("mobile") var mobile: String? = null,

    @field:SerializedName("bypass_otp_match") var bypassOTP: Boolean? = true,

    @field:SerializedName("action") var action: String? = null,
)