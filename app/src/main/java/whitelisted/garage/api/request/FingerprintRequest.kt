package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName

data class FingerprintRequest(@field:SerializedName("finger_print") var fingerprint: String? = "")
