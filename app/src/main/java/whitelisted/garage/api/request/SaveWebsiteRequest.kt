package whitelisted.garage.api.request

import com.google.gson.annotations.SerializedName


data class SaveWebsiteRequest(@SerializedName("address") var address: String? = null,
    @SerializedName("gmbwebsite_mobile") var gmbMobile: String? = null,
    @SerializedName("gmbwebsite_attributes") var gmbAttributes: MutableMap<String, MutableList<String>>? = null,
    @SerializedName("gmbwebsite_categories") var gmbCategories: List<String>? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("gmbwebsite_open_time") var openTime: MutableMap<String, List<String>>? = null,
    @SerializedName("gmbwebsite_workshop_images") var workshopImages: List<String>? = null)

