package whitelisted.garage.api

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.Path
import retrofit2.http.Query
import whitelisted.garage.api.data.GetAccessoriesCatalogueResponse
import whitelisted.garage.api.request.*
import whitelisted.garage.api.response.*
import whitelisted.garage.api.response.login.GetProfileCompletion
import whitelisted.garage.api.response.login.LoginUserResponse
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.AppStorePreferences
import whitelisted.garage.view.fragments.orderInventory.OrderInventoryRequest

class APIRepositoryImpl(private val api: API, private val sharePref: AppStorePreferences) :
    APIRepository {

    override fun getSharedPreferences(): AppStorePreferences {
        return sharePref
    }

    override fun getOTP(hashCode: String?,
        countryCode: String?,
        mobileNumber: String?): Deferred<Response<ServerResponse<String>>> {
        return api.getOTP(hashCode, countryCode, mobileNumber)
    }

    override fun signUpUser(signUpRequest: SignUpRequest?): Deferred<Response<ServerResponse<LoginUserResponse>>> {
        return api.signUpUser(signUpRequest!!)
    }

    override fun loginUser(loginRequest: LoginRequest): Deferred<Response<ServerResponse<LoginUserResponse>>> {
        return api.loginUser(loginRequest)
    }

    override fun addEmployee(workshopId: String, addEditEmployee: AddEditEmployee

    ): Deferred<Response<ServerResponse<LoginUserResponse>>> {
        return api.addEmployee(workshopId, addEditEmployee)
    }


    override fun editEmployee(addEditEmployee: AddEditEmployee,
        editEmployeeId: String?): Deferred<Response<ServerResponse<LoginUserResponse>>> {
        return api.editEmployee(addEditEmployee, editEmployeeId)
    }

    override fun verifyOTP(verifyOtpRequest: VerifyOtpRequest): Deferred<Response<ServerResponse<Any>>> {
        return api.verifyOTP(verifyOtpRequest)
    }


    override fun getManageEmployeeData(workshopId: String): Deferred<Response<ServerResponse<List<EmployeeListResponseModel>>>> {
        return api.getManageEmployeeData(workshopId)
    }

    override fun resetPassword(resetPasswordRequest: ResetPasswordRequest): Deferred<Response<ServerResponse<Any>>> {
        return api.resetPassword(resetPasswordRequest)
    }


    override fun getWorkshopList(): Deferred<Response<ServerResponse<List<WorkshopListResponseModel>>>> {
        return api.getWorkshopList()
    }

    override fun getSubscriptionStatus(workshopId: String): Deferred<Response<ServerResponse<SubscriptionStatusResponse>>> {
        return api.getSubscriptionStatus(workshopId)
    }

    override fun getHomeScreen(workshopId: String?): Deferred<Response<ServerResponse<HomeScreenResponse>>> {
        return api.getHomeScreen(workshopId)
    }

    override fun placeOrderAPI(request: NewOrderRequest?): Deferred<Response<ServerResponse<PlaceOrderResponse>>> {
        return api.placeOrder(request)
    }

    override fun getOrderDetail(orderId: String?): Deferred<Response<ServerResponse<OrderDetailResponse>>> {
        return api.getOrderDetail(orderId)
    }

    override fun getInventoryCheckList(isTwoWheeler: Boolean): Deferred<Response<ServerResponse<List<String>>>> {
        return api.getOrderInventoryCheckList(isTwoWheeler)
    }

    override fun getCarDocsList(): Deferred<Response<ServerResponse<List<String>>>> {
        return api.getCarDocsList()
    }

    override fun getInventoryDetail(orderId: String): Deferred<Response<ServerResponse<OrderInventoryRequest>>> {
        return api.getOrderInventory(orderId)
    }

    override fun getCarList(): Deferred<Response<ServerResponse<List<CarListModel>>>> {
        return api.getCarsList()
    }

    override fun getServicesList(searchedString: String?): Deferred<Response<ServerResponse<List<ServiceResponseModel>>>> {
        return api.getServicesList(searchedString)
    }

    override fun getPackagesList(): Deferred<Response<ServerResponse<List<PackagesResponse>>>> {
        return api.getPackagesList()
    }

    override fun addWorkshop(addWorkshopRequest: AddWorkshopRequest): Deferred<Response<ServerResponse<WorkshopIDResponse>>> {
        return api.addWorkshop(addWorkshopRequest)
    }

    override fun sendReminder(orderId: String?): Deferred<Response<ServerResponse<OrderIdResponse>>> {
        return api.sendReminder(orderId)
    }

    override fun deleteWorkshop(workshopId: String): Deferred<Response<ServerResponse<WorkshopIDResponse>>> {
        return api.deleteWorkshop(workshopId)
    }

    override fun deleteEmployee(id: String): Deferred<Response<ServerResponse<WorkshopIDResponse>>> {
        return api.deleteEmployee(id)
    }

    override fun getUserProfile(id: String): Deferred<Response<ServerResponse<GetUserProfileResponse>>> {
        return api.getUserProfile(id)
    }

    override fun updateUserProfile(id: String,
        updateProfileRequest: UpdateProfileRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.updateUserProfile(id, updateProfileRequest)
    }

    override fun updateWorkshop(workshopId: String,
        addWorkshopRequest: AddWorkshopRequest): Deferred<Response<ServerResponse<WorkshopIDResponse>>> {
        return api.updateWorkshop(workshopId, addWorkshopRequest)
    }

    override fun getWorkDoneList(): Deferred<Response<ServerResponse<List<WorkDoneResponseModel>>>> {
        return api.getWorkDoneList()
    }

    override fun getCustomPackagesList(): Deferred<Response<ServerResponse<List<AllPackagesResponseModel>>>> {
        return api.getCustomPackageList()
    }

    override fun getMyCustomerList(): Deferred<Response<ServerResponse<List<MyCustomerModel>>>> {
        return api.getMyCustomerList()
    }

    override fun getMyCustomerListWithSearch(search: String): Deferred<Response<ServerResponse<List<MyCustomerModel>>>> {
        return api.getMyCustomerListWithSearch(search)
    }

    override fun createCustomPackage(createPackageRequest: CreateCustomPackageRequest): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.createCustomPackage(createPackageRequest)
    }

    override fun deletePackage(packageId: String?): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.deleteCustomPackage(packageId)
    }

    override fun updateServiceOfCustomPackage(packageId: String?,
        serviceId: Int?,
        servicesItem: ServicesItem): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.updateServiceOfPackage(packageId, serviceId, servicesItem)
    }

    override fun addServiceToCustomPackage(packageId: String?,
        item: ServicesItem): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.addServiceToPackage(packageId, item)
    }

    override fun deleteServiceOfCustomPackage(packageId: String?,
        serviceId: Int?): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.deleteServiceOfPackage(packageId, serviceId)
    }

    override fun getServicesOfPackage(packageId: String?): Deferred<Response<ServerResponse<List<ServicesItem>>>> {
        return api.getServicesOfPackage(packageId)
    }

    override fun updatePackageName(packageId: String?,
        request: UpdatePackageNameRequest?): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.updateCustomPackageName(packageId, request)
    }

    override fun getExistingPackages(): Deferred<Response<ServerResponse<List<AllPackagesResponseModel>>>> {
        return api.getExistingPackages()
    }

    override fun duplicatePackage(packageId: String?): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.duplicatePackage(packageId)
    }

    override fun getRemindersList(type: String?,
        periodType: String): Deferred<Response<ServerResponse<RemindersListResponse>>> {
        return api.getRemindersList(type, periodType)
    }

    override fun getBillingList(startDate: String,
        endDate: String): Deferred<Response<ServerResponse<List<BillingResponseModel>>>> {
        return api.getBillingList(startDate, endDate)
    }

    override fun getOtpForLogin(hashCode: String?,
        countryCode: String?,
        mobileNumber: String?): Deferred<Response<ServerResponse<String>>> {
        return api.getOTPForLogin(hashCode, countryCode, mobileNumber)
    }

    override fun verifyOTPForLogin(req: VerifyOtpRequest): Deferred<Response<ServerResponse<LoginUserResponse>>> {
        return api.verifyOTPForLogin(req)
    }

    override fun getRoleBasedEmployees(type: String): Deferred<Response<ServerResponse<List<EmployeeListResponseModel>>>> {
        return api.getRoleBasedEmployeeList(type)
    }

    override fun updateOrderDetail(orderId: String?,
        value: NewOrderRequest?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>> {
        return api.updateOrderDetail(orderId, value)
    }

    override fun getOrdersList(type: String?): Deferred<Response<ServerResponse<OrderListResponse>>> {
        return api.getOrdersList(type)
    }

    override fun getFilteredOrdersList(date_filter: String?,
        start_date: String?,
        end_date: String?,
        limit: Int,
        offset: Int): Deferred<Response<ServerResponse<OrderListResponse>>> {
        return api.getFilteredOrdersList(date_filter, start_date, end_date, limit, offset)
    }

    override fun getLedger(type: String?,
        transactionType: String?): Deferred<Response<ServerResponse<LedgerResponseModel>>> {
        return api.getLedger(type, transactionType)
    }

    override fun addLedger(addToLedgerRequest: AddToLedgerRequest): Deferred<Response<ServerResponse<LedgerResponseModel>>> {
        return api.addLedger(addToLedgerRequest)
    }

    override fun addInventory(orderInventoryDetailResponse: OrderInventoryRequest): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>> {
        return api.addOrderInventory(orderInventoryDetailResponse)
    }

    override fun getOrdersBySearch(searchedString: String?): Deferred<Response<ServerResponse<List<SearchOrderResponseModel>>>> {
        return api.getSearchOrdersList(searchedString)
    }

    override fun addJobCard(jobCarRequest: JobCardRequest?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>> {
        return api.addJobCard(jobCarRequest)
    }

    override fun getJobCard(orderId: String?): Deferred<Response<ServerResponse<List<JobCardRequest>>>> {
        return api.getJobCard(orderId)
    }

    override fun getOrderEstimates(orderId: String?): Deferred<Response<ServerResponse<List<OrderEstimateRequestResponse>>>> {
        return api.getOrderEstimates(orderId)
    }

    override fun updateOrderEstimates(request: OrderEstimateRequestResponse?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>> {
        return api.updateOrderEstimates(request)
    }

    override fun getPaymentDetails(orderId: String?): Deferred<Response<ServerResponse<List<GetPaymentResponseModel>>>> {
        return api.getPaymentDetails(orderId)
    }

    override fun addPayment(paymentModel: AddPaymentRequest?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>> {
        return api.addPayment(paymentModel)
    }

    override fun completePayment(orderIdReq: OrderIdResponse?): Deferred<Response<ServerResponse<JSONObject>>> {
        return api.completePayment(orderIdReq)
    }

    override fun getMessageFromAPI(orderId: String?): Deferred<Response<ServerResponse<GetMessageFromApiResponse>>> {
        return api.getMessageFromAPI(orderId)
    }

    override fun getMessageFromAPIRetailer(mobile: String?): Deferred<Response<ServerResponse<GetMessageFromApiResponse>>> {
        return api.getMessageFromAPIRetailer(mobile)
    }

    override fun getBannerImages(): Deferred<Response<ServerResponse<BannerImagesResponse>>> {
        return api.getBannerImages()
    }

    override fun trueCallerLogin(trueCallerLoginRequest: TrueCallerLoginRequest): Deferred<Response<ServerResponse<LoginUserResponse>>> {
        return api.trueCallerLogin(trueCallerLoginRequest)
    }

    override fun downloadLedgerCSV(type: String): Deferred<Response<ResponseBody>> {
        return api.downloadLedgerCSV(type)
    }

    override fun downloadBillingCSV(startDate: String,
        endDate: String): Deferred<Response<ResponseBody>> {
        return api.downloadBillingCSV(startDate, endDate)
    }

    override fun downloadOrderHistoryCSV(orderType: String?,
        startDate: String?,
        endDate: String?): Deferred<Response<ResponseBody>> {
        return api.downloadOrderHistoryCSV(orderType, startDate, endDate)
    }

    override fun cancelOrder(cancelOrderReq: CancelOrderRequest?): Deferred<Response<ServerResponse<JSONObject>>> {
        return api.cancelOrder(cancelOrderReq)
    }

    override fun getUPIId(): Deferred<Response<ServerResponse<GetUPIResponse>>> {
        return api.getUPIId()
    }

    override fun saveUPIId(saveUPIIdRequest: SaveUPIIdRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.saveUPIId(saveUPIIdRequest)
    }

    override fun getOrderSummary(orderId: String?): Deferred<Response<ServerResponse<List<OrderSummaryResponse>>>> {
        return api.getOrderSummary(orderId)
    }

    override fun getCarousel(): Deferred<Response<ServerResponse<HomeCarouselResponse>>> {
        return api.getCarousel()
    }

    override fun getOTPForProfile(mobile: String): Deferred<Response<ServerResponse<String>>> {
        return api.getOTPForProfile(mobile)
    }

    override fun verifyOTPForProfile(verifyOtpRequest: VerifyOtpRequest): Deferred<Response<ServerResponse<Any>>> {
        return api.verifyOTPForProfile(verifyOtpRequest)
    }

    override fun searchRemindersList(reminderType: String?): Deferred<Response<ServerResponse<RemindersListResponse>>> {
        return api.searchRemindersList(reminderType)
    }

    override fun scheduleReminder(orderId: String?,
        schedule_at: String?): Deferred<Response<ServerResponse<OrderIdResponse>>> {
        return api.scheduleReminder(orderId, schedule_at)
    }

    override fun getConfigAPI(): Deferred<Response<ServerResponse<ScreensConfigResponse>>> {
        return api.getConfig()
    }

    override fun getUpdateInfo(): Deferred<Response<ServerResponse<GetUpdateInfoResponse>>> {
        return api.getUpdateInfo()
    }

    override fun getGMBCategories(): Deferred<Response<ServerResponse<GetGMDCategoriesResponse>>> {
        return api.getGMBCategories()
    }

    override fun getGoCoinScreenResponse(): Deferred<Response<ServerResponse<GetGoCoinsScreenResponse>>> {
        return api.getGoCoinsScreenResponse()
    }

    override fun getPin(userId: String): Deferred<Response<ServerResponse<SetPinRequest>>> {
        return api.getPin(userId)
    }

    override fun setPin(userId: String,
        req: SetPinRequest): Deferred<Response<ServerResponse<SetPinRequest>>> {
        return api.setPin(userId, req)
    }

    override fun getGMB(): Deferred<Response<ServerResponse<GetGMDResponse>>> {
        return api.getGMB()
    }

    override fun saveGMB(saveGMBRequest: SaveGMBRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.saveGMB(saveGMBRequest)
    }

    override fun getGMBAttributes(): Deferred<Response<ServerResponse<MutableMap<String, MutableList<String>>>>> {
        return api.getGMBAttributes()
    }

    override fun getGoCoinsOptions(): Deferred<Response<ServerResponse<GetGOCoinOptionsResponse>>> {
        return api.getGoCoinsOptions()
    }

    override fun unLockGMB(unlockGMBRequest: UnlockGMBRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.unLockGMB(unlockGMBRequest)
    }

    override fun getBrandsList(): Deferred<Response<ServerResponse<List<BrandsResponseModel>>>> {
        return api.getBrandsList()
    }

    override fun getCarModelList(brandId: String?): Deferred<Response<ServerResponse<List<CarModelResponseModel>>>> {
        return api.getCarModelsList(brandId)
    }

    override fun getFuelTypeList(modelId: String?): Deferred<Response<ServerResponse<FuelTypeListResponse>>> {
        return api.getFuelTypeList(modelId)
    }

    override fun getOrderIdForPayment(map: MutableMap<String, String>): Deferred<Response<ServerResponse<OrderIdForPaymentResponse>>> {
        return api.getOrderIdForPayment(map)
    }

    override fun getRazorPayKey(): Deferred<Response<ServerResponse<GetRazorPayKey>>> {
        return api.getRazorPayKey()
    }

    override fun verifyPaymentDetails(map: MutableMap<String, String>): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.verifyRazorPayPayment(map)
    }

    override fun getGoCoinTransactions(type: String,
        period: String): Deferred<Response<ServerResponse<GCTransactionsResponse>>> {
        return api.getAllGoCoinTransactions(type, period)
    }

    override fun getRetailersCatalogue(type: String): Deferred<Response<ServerResponse<List<GetInventoryPartItemResponse>>>> {
        return api.getRetailersCatalogue(type)
    }

    override fun createInventoryRack(request: CreateInventoryRackRequest): Deferred<Response<ServerResponse<RackIdResponse>>> {
        return api.createInventoryRack(request)
    }

    override fun updateInventoryRack(id: String,
        request: CreateInventoryRackRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.updateInventoryRack(id, request)
    }

    override fun deleteInventoryRack(id: String): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.deleteInventoryRack(id)
    }

    override fun getInventoryRack(): Deferred<Response<ServerResponse<MutableList<GetInventoryRackResponse>>>> {
        return api.getInventoryRack()
    }

    override fun getIndividualInventoryRack(id: String): Deferred<Response<ServerResponse<GetInventoryRackResponse>>> {
        return api.getIndividualInventoryRack(id)
    }

    override fun updateInventoryRackDetails(id: String,
        update_only_rack: Int,
        request: CreateInventoryRackRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.updateInventoryRackDetails(id, update_only_rack, request)
    }

    override fun createUpdateRetailOrder(createOrderRequest: RetailCreateOrderRequest): Deferred<Response<ServerResponse<RetailPlaceOrderResponse>>> {
        return api.createUpdateRetailOrder(createOrderRequest)
    }

    override fun getSavedWorkshops(): Deferred<Response<ServerResponse<List<SavedWorkshopResponseModel>>>> {
        return api.getSavedWorkshopList()
    }

    override fun searchImages(q: String): Deferred<Response<ServerResponse<List<SearchImageResponse>>>> {
        return api.searchImages(q)
    }

    override fun getGoCoinBalance(): Deferred<Response<ServerResponse<GetGoCoinBalanceResponse>>> {
        return api.getGoCoinBalance()
    }

    override fun getRetailOrderDetail(orderId: String): Deferred<Response<ServerResponse<RetailOrderDetailResponse>>> {
        return api.getRetailOrderDetail(orderId)
    }

    override fun getAccCustomPackageDetails(packageId: String?): Deferred<Response<ServerResponse<GetAccCustomPackageDetails>>> {
        return api.getAccCustomPackageDetails(packageId)
    }

    override fun getAccessoriesCatalogue(type: String): Deferred<Response<ServerResponse<MutableList<GetAccessoriesCatalogueResponse>>>> {
        return api.getAccessoriesCatalogue(type)
    }

    override fun updateOrderInclusions(request: OrderInclusionsRequestResponse?): Deferred<Response<ServerResponse<OrderInclusionsRequestResponse>>> {
        return api.updateOrderInclusions(request)
    }

    override fun getOrderInclusions(orderId: String?): Deferred<Response<ServerResponse<List<OrderInclusionsRequestResponse>>>> {
        return api.getOrderInclusions(orderId)
    }

    override fun downloadInventoryCSV(): Deferred<Response<ResponseBody>> {
        return api.downloadInventoryCSV()
    }

    override fun getGoCoinsValue(): Deferred<Response<ServerResponse<GoCoinsValueResponse>>> {
        return api.getGoCoinsValue()
    }

    override fun updateInventoryItemValue(request: UpdateInventoryItemRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.updateInventoryItemValue(request)
    }

    override fun getWebsite(): Deferred<Response<ServerResponse<GetWebsiteResponse>>> {
        return api.getWebsite()
    }

    override fun saveGMBWebsite(saveGMBRequest: SaveWebsiteRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.saveGMBWebsite(saveGMBRequest)
    }

    override fun addItemToRack(id: String?,
        req: CreateInventoryRackRequest?): Deferred<Response<ServerResponse<JsonElement>>> {

        return api.addItemToRack(id, req)
    }

    override fun getReferralCode(): Deferred<Response<ServerResponse<GetReferralCodeResponse>>> {
        return api.getReferralCode()
    }

    override fun availReferCode(req: AvailReferralRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.availReferCode(req)
    }

    override fun getProfileCompletion(wrokshopId: String): Deferred<Response<ServerResponse<GetProfileCompletion>>> {
        return api.getProfileCompletion(wrokshopId)
    }

    override fun updateProfileCompletion(userId: String,
        request: SaveProfileCompletionRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.updateProfileCompletion(userId, request)
    }

    override fun updateInventoryItemValueForWorkshop(updateInventoryItemRequest: UpdateInventoryItemRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.updateInventoryItemValueForWorkshop(updateInventoryItemRequest)
    }

    override fun sendDuesReminder(req: OrderIdResponse): Deferred<Response<ServerResponse<MsgKeyResponse>>> {
        return api.sendDuesReminder(req)
    }

    override fun collectDuesAPI(req: LedgerIdRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.collectDue(req)
    }

    override fun getAllGoCoinTransactionsCSV(type: String,
        period: String): Deferred<Response<ResponseBody>> {
        return api.getAllGoCoinTransactionsCSV(type, period)
    }

    override fun updateUserLocation(userId: String,
        request: UpdateLocationRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.updateUserLocation(userId, request)
    }

    override fun getMostUsedPartsRetailerCatalogue(isTwoWheeler: Boolean): Deferred<Response<ServerResponse<MutableList<MostUsedPartsResponseModel>>>> {
        return api.getMostUsedPartsRetailerCatalogue(isTwoWheeler)
    }

    override fun getMostUsedPartsWorkshopCatalogue(isTwoWheeler: Boolean): Deferred<Response<ServerResponse<MutableList<MostUsedPartsResponseModel>>>> {
        return api.getMostUsedPartsWorkshopCatalogue(isTwoWheeler)
    }

    override fun getAllCoupons(type: String): Deferred<Response<ServerResponse<GetAllCouponsResponse>>> {
        return api.getAllCoupons(type)
    }

    override fun applyCoupon(code: String?,
        type: String): Deferred<Response<ServerResponse<ApplyCouponResponse>>> {
        return api.applyCoupon(code, type)
    }

    override fun getLocationDetails(ipAddress: String): Deferred<Response<ServerResponse<LocationDetailsResponse>>> {
        return api.getLocationDetails(ipAddress)
    }

    override fun getCountryCodes(): Deferred<Response<ServerResponse<MutableList<CountryCodeResponseModel>>>> {
        return api.getCountryCodes()
    }

    override fun getPreviousMonthData(): Deferred<Response<ServerResponse<PreviousMonthResponse>>> {
        return api.getPreviousMonthData()
    }

    override fun getRetryPaymentStatus(): Deferred<Response<ServerResponse<RetryPaymentResponse>>> {
        return api.getRetryPaymentResponse()
    }

    override fun cancelRetryPayment(request: IdRequestResponse): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.cancelPendingPayment(request)
    }

    override fun sendInstantReminder(request: SendInstantReminderRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.sendInstantReminder(request)
    }

    override fun getCustomerReminder(): Deferred<Response<ServerResponse<GetCustomerReminderResponse>>> {
        return api.getCustomerReminder()
    }

    override fun getAllCustomerReminder(): Deferred<Response<ServerResponse<GetCustomerReminderResponse>>> {
        return api.getAllCustomerReminder()
    }

    override fun collectDueOnReminder(request: PreviousReminder): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.collectDueOnReminder(request)
    }

    override fun getHomeScreenForMonth(workshopId: String?,
        monthPosition: String): Deferred<Response<ServerResponse<HomeScreenResponse>>> {
        return api.getHomeScreenForMonth(workshopId, monthPosition)
    }

    override fun createWorkshopBiddingAsync(request: WorkShopBiddingRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.createWorkshopBiddingAsync(request)
    }

    override fun getNewBidsList(): Deferred<Response<ServerResponse<MutableList<AccRetailBidListResponseModel>>>> {
        return api.getNewBidsList()
    }

    override fun getSubmittedBidsList(): Deferred<Response<ServerResponse<MutableList<AccRetailBidListResponseModelSubmitAccept>>>> {
        return api.getSubmittedBidsList()
    }

    override fun getAcceptedBidsList(): Deferred<Response<ServerResponse<MutableList<AccRetailBidListResponseModelSubmitAccept>>>> {
        return api.getAcceptedBidsList()
    }

    override fun getOnGoingWsBiddingList(type: String?): Deferred<Response<ServerResponse<List<GetWsOngoingBidResponse>>>> {
        return api.getOnGoingWsBiddingList(type)
    }

    override fun getWsHistoryBiddingList(): Deferred<Response<ServerResponse<List<GetWsOngoingBidResponse>>>> {
        return api.getWsHistoryBiddingList()
    }

    override fun getWsMostPart(): Deferred<Response<ServerResponse<GetWsMostUsedPart>>> {
        return api.getWsMostPart()
    }

    override fun getWsReceivedBids(id: String): Deferred<Response<ServerResponse<GetWsBidReceivedResponse>>> {
        return api.getWsReceivedBids(id)
    }

    override fun markCompleteWsBid(request: MarkCompleteWsBidRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.markCompleteWsBid(request)
    }

    override fun getWsBidEnquiry(id: String): Deferred<Response<ServerResponse<GetWsBidEnquiryResponse>>> {
        return api.getWsBidEnquiry(id)
    }

    override fun changeStatusOfWsBid(id: String,
        req: WsAccRejBid): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.changeStatusOfWsBid(id, req)
    }

    override fun getBidDetails(orderId: String): Deferred<Response<ServerResponse<BidDetailResponse>>> {
        return api.getBidDetails(orderId)
    }

    override fun placeBid(bidId: String,
        bidDetail: BidDetailResponse): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.placeBid(bidId, bidDetail)
    }

    override fun cancelBidRetailer(bidId: String): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.cancelBidRetailer(bidId)
    }

    override fun unlockContactRetailer(bidId: String,
        req: IdTypeRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.unlockPhoneRetailer(bidId, req)
    }

    override fun unlockPhoneWorkshop(bidId: String,
        req: IdTypeRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.unlockPhoneWorkshop(bidId, req)
    }

    override fun markCompleteBidRetailer(bidId: String,
        req: IdTypeRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.markBidCompleteRetailer(bidId, req)
    }

    override fun cancelWorkshopBid(bidId: String?): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.cancelWorkshopBid(bidId)
    }

    override fun getWsAccSpareBiddingCSV(id: String): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.getWsAccSpareBiddingCSV(id)
    }

    override fun getNearByBidingWorkShop(): Deferred<Response<ServerResponse<Map<String, GetNearByBidingWorkshop>>>> {
        return api.getNearByBidingWorkShop()
    }

    override fun updateDeviceTokenForFCM(fingerprintRequest: FingerprintRequest): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.updateDeviceTokenForFCM(fingerprintRequest)
    }

    override fun uploadInventoryCSVFile(file: MultipartBody.Part): Deferred<Response<ServerResponse<Any>>> {
        return api.uploadInventoryCSVFile(file, 1)
    }

    override fun getAvailableTemplates(): Deferred<Response<ServerResponse<List<BusinessCardTemplate>>>> {
        return api.getAvailableTemplates("business_card")
    }

    override fun getSavedCards(): Deferred<Response<ServerResponse<List<BusinessCardDetails>>>> {
        return api.getSavedCards()
    }

    override fun saveBusinessCard(details: BusinessCardDetails): Deferred<Response<ServerResponse<Any>>> {
        return if (details.id.isEmpty()) api.saveBusinessCard(details) else api.updateBusinessCard(
            details)
    }

    override fun getSSItemsWithFilter(limit: Int,
        offset: Int): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>> {
        return api.getSSPartsList(limit, offset)
    }

    override fun getEmployeePerformance(employeeId: String,
        month: Int,
        year: Int): Deferred<Response<ServerResponse<EmployeePerformanceResponse>>> {
        return api.getEmployeePerformance(employeeId, month, year)
    }

    //    override fun getSSBrandsResponse(): Deferred<Response<ServerResponse<MutableList<SSBrandsResponseModel>>>> {
    //        return api.getSSBrandsResponse()
    //    }

    override fun addToCartSS(productId: String?,
        itemModel: SSItemModel): Deferred<Response<ServerResponse<GetSSCartResponse>>> {
        return api.addToCartSS(productId, itemModel)
    }

    override fun getCartSS(): Deferred<Response<ServerResponse<GetSSCartResponse>>> {
        return api.getSSCart()
    }

    //    override fun getSSCategoriesResponse(): Deferred<Response<ServerResponse<MutableList<SSBrandsResponseModel>>>> {
    //        return api.getSSCategoriesResponse()
    //    }

    override fun getSSSparesBrandsResponse(): Deferred<Response<ServerResponse<MutableList<SSBrandsResponseModel>>>> {
        return api.getSSSparesBrandsResponse()
    }

    override fun getSSSparesCategoriesResponse(): Deferred<Response<ServerResponse<MutableList<SSCategoryResponseModel>>>> {
        return api.getSSSparesCategoriesResponse()
    }

    override fun getRazorPayKeyForSS(): Deferred<Response<ServerResponse<GetRazorPayKey>>> {
        return api.getSSRazorPayKey()
    }

    override fun getOrderIdForSSPayment(amountRequest: AmountRequest): Deferred<Response<ServerResponse<OrderIdForPaymentResponse>>> {
        return api.getOrderIdForSSPayment(amountRequest)
    }

    override fun checkoutSSOrder(ssCheckoutRequest: SSCheckoutRequest): Deferred<Response<ServerResponse<Any>>> {
        return api.checkoutSSOrder(ssCheckoutRequest)
    }

    override fun deleteBusinessCard(cardId: String): Deferred<Response<ServerResponse<Any>>> {
        return api.deleteBusinessCard(cardId)
    }

    override fun getAllAddress(): Deferred<Response<ServerResponse<MutableList<AddressResponseModel>>>> {
        return api.getAllAddress()
    }

    override fun getSparesPurchaseHistory(  limit: Int,
        offset: Int): Deferred<Response<ServerResponse<List<SparesOrderHistory>>>> {
        return api.getSparesPurchaseHistory(limit, offset)
    }

    override fun getSparesPurchaseHistory(map:Map<String,String>): Deferred<Response<ServerResponse<List<SparesOrderHistory>>>> {
        return api.getSparesPurchaseHistory(map)
    }

    override fun addAddressAPI(addressResponseModel: AddressResponseModel): Deferred<Response<ServerResponse<Any>>> {
        return api.addShippingAddressAPI(addressResponseModel)
    }

    override fun deleteShippingAddress(addressId: String): Deferred<Response<ServerResponse<Any>>> {
        return api.deleteShippingAddressAPI(addressId)
    }

    override fun editAddressAPI(addressResponseModel: AddressResponseModel): Deferred<Response<ServerResponse<Any>>> {
        return api.editShippingAddressAPI(addressResponseModel)
    }

    override fun getSSItemsWithBrandFilter(brandName: String?,
        limit: Int,
        offset: Int): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>> {
        return api.getSSPartsListWithBrandFilter(brandName, limit, offset)
    }

    override fun getSSItemsWithCategoryFilter(categoryName: String?,
        limit: Int,
        offset: Int): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>> {
        return api.getSSPartsListWithCategoryFilter(categoryName, limit, offset)
    }

    override fun getSSHomeBannersResponse(): Deferred<Response<ServerResponse<MutableList<SSHomeBannersResponseModel>>>> {
        return api.getSSHomeBannersResponse()
    }

    override fun getSearchSSItems(searchString: String,
        limit: Int,
        offset: Int): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>> {
        return api.getSSPartsListWithSearch(searchString, limit, offset)
    }

    override fun getMarketPlaceCoupons(): Deferred<Response<ServerResponse<List<MarketPlaceCoupon>>>> {
        return api.getMarketplaceCoupons()
    }

    override fun applyMarketPlaceCoupon(req: ShopApplyCouponReq): Deferred<Response<ServerResponse<Any>>> {
        return api.applyMarketplaceCoupon(req)
    }

    override fun removeMarketPlaceCoupon(cartId: String): Deferred<Response<ServerResponse<Any>>> {
        return api.removeMarketplaceCoupon(cartId)
    }

    override fun getProductSearchById(productId: String): Deferred<Response<ServerResponse<SSItemModel>>> {
        return api.getSSPartsListWithSearchQuery(productId)
    }

    override fun getSSItemsWithCarFilter(brand: String?,
        category: String?,
        brandName: String?,
        modelName: String?,
        limit: Int,
        offset: Int): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>> {
        return api.getSSPartsListWithCarFilter(brand, category, brandName, modelName, limit, offset)
    }

    override fun getPDPBannerImage(): Deferred<Response<ServerResponse<PDPBannerResponse>>> {
        return api.getPDPBannerImage(AppENUM.ConfigConstants.PDP_BANNER)
    }

    override fun downloadSampleCSV(): Deferred<Response<ResponseBody>> {
        return api.downloadSampleCSV()
    }

    override fun getMarketplaceFAQ(): Deferred<Response<ServerResponse<List<FaqsItem>>>> {
        return api.getMarketplaceFAQ()
    }

    override fun getTrackingURL(): Deferred<Response<ServerResponse<Map<String, String>>>> {
        return api.getTrackingURL()
    }

    override fun getMarketplaceFilters(): Deferred<Response<ServerResponse<List<MarketplaceFilter>>>> {
        return api.getMarketplaceFilters()
    }

    override fun addServiceByOwn(req: AddServiceByOwnReq): Deferred<Response<ServerResponse<Any>>> {
        return api.addServiceByOwn(req)
    }

    override fun getSSPartsListFilter(filters: Map<String, String>): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>> {
        return api.getSSPartsListFilter(filters)
    }

    override fun notifyUser(productIdJson: JsonObject): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.notifyForProduct(productIdJson)
    }

    override fun getNotifiedItemsList(): Deferred<Response<ServerResponse<MutableList<NotifiedItemResponseModel>>>> {
        return api.getNotifiedItemsList()
    }

    override fun addCustomItemSparesRetailer(req: JsonObject): Deferred<Response<ServerResponse<Map<String, String>>>> {
        return api.addCustomItemSparesRetailer(req)
    }

    override fun addCustomItemAccessories(req: JsonObject): Deferred<Response<ServerResponse<Map<String, String>>>> {
        return api.addCustomItemAccessories(req)
    }

    override fun deleteCustomPart(id: String): Deferred<Response<ServerResponse<Any>>> {
        return api.deleteCustomPart(id)
    }

    override fun deleteCustomAccessoriesPart(skuCode: String): Deferred<Response<ServerResponse<Any>>> {
        return api.deleteCustomAccessoriesPart(skuCode)
    }

    override fun deleteCustomRetailersCataloguePart(skuCode: String): Deferred<Response<ServerResponse<Any>>> {
        return api.deleteCustomRetailersCataloguePart(skuCode)
    }

    override fun getSSAccBrandsResponse(): Deferred<Response<ServerResponse<MutableList<SSBrandsResponseModel>>>> {
        return api.getSSAccBrandsResponse()
    }

    override fun getSSAccCategoriesResponse(): Deferred<Response<ServerResponse<MutableList<SSCategoryResponseModel>>>> {
        return api.getSSAccCategoriesResponse()
    }

    override fun getSSOriginalBrandsResponse(): Deferred<Response<ServerResponse<MutableList<SSBrandsResponseModel>>>> {
        return api.getSSOriginalSparesBrandsResponse()
    }

    override fun getPaymentTypes(req:GetPaymentModesRequest): Deferred<Response<ServerResponse<List<PaymentTypesResponseModel>>>> {
        return api.getPaymentTypes(req)
    }

    override fun getPaymentTypesGet(): Deferred<Response<ServerResponse<List<PaymentTypesResponseModel>>>> {
        return api.getPaymentTypesGet()
    }

    override fun getSearchAndFilterAttributes(): Deferred<Response<ServerResponse<SearchAndFilterAttrs>>> {
        return api.getSearchAndFilterAttributes()
    }

    override fun getPopularItems(type: String): Deferred<Response<ServerResponse<List<SSItemModel>>>> {
        return api.getPopularItems(type)
    }

    override fun getEmiAmount(amount: Double): Deferred<Response<ServerResponse<Map<String, String>>>> {
        return api.getEmiAmount(amount)
    }

    override fun buyNowProduct(productId: String?,
        product: SSItemModel?): Deferred<Response<ServerResponse<IdRequestResponse>>> {
        return api.buyNowProduct(productId, product)
    }

    override fun getSparesHomeResponse(): Deferred<Response<ServerResponse<SparesHomeResponseModel>>> {
        return api.getSparesHomeResponse()
    }

    override fun getMarketplacePdpFAQ(): Deferred<Response<ServerResponse<List<FaqsItem>>>> {
        return api.getMarketplacePdpFAQ()
    }

    override fun sendEnquiry(enquiry: Enquiry): Deferred<Response<ServerResponse<Any>>> {
        return api.sendEnquiry(enquiry)
    }

    override fun getAllEnquiries(): Deferred<Response<ServerResponse<List<Enquiry>>>> {
        return api.getAllEnquiries()
    }

    override fun getBikeBrandsList(): Deferred<Response<ServerResponse<List<BrandsResponseModel>>>> {
        return api.getBikeBrandsList()
    }

    override fun getBikeModelList(brandId: String?): Deferred<Response<ServerResponse<List<CarModelResponseModel>>>> {
        return api.getBikeModelsList(brandId)
    }

    override fun getBikeFuelTypeList(modelId: String?): Deferred<Response<ServerResponse<FuelTypeListResponse>>> {
        return api.getBikeFuelTypeList(modelId)
    }

    override fun getBikeServicesList(search: String?): Deferred<Response<ServerResponse<List<ServiceResponseModel>>>> {
        return api.getBikeServicesList(search)
    }

    override fun createCustomPackageBike(createPackageRequest: CreateCustomPackageRequest): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.createCustomPackageBike(createPackageRequest)
    }

    override fun getAccessoriesCatalogueBike(searchText: String): Deferred<Response<ServerResponse<MutableList<GetAccessoriesCatalogueResponse>>>> {
        return api.getAccessoriesCatalogueBike(searchText)
    }

    override fun getCustomPackagesListBike(): Deferred<Response<ServerResponse<List<AllPackagesResponseModel>>>> {
        return api.getCustomPackageListBike()
    }

    override fun getServicesOfPackageBike(packageId: String?): Deferred<Response<ServerResponse<List<ServicesItem>>>> {
        return api.getServicesOfPackageBike(packageId)
    }

    override fun updateServiceOfCustomPackageBike(packageId: String,
        serviceId: Int,
        servicesItem: ServicesItem): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.updateServiceOfPackageBike(packageId, serviceId, servicesItem)
    }

    override fun addServiceToCustomPackageBike(packageId: String,
        item: ServicesItem): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.addServiceToPackageBike(packageId, item)
    }

    override fun deleteServiceOfCustomPackageBike(packageId: String?,
        serviceId: Int?): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.deleteServiceOfPackageBike(packageId, serviceId)
    }

    override fun updatePackageNameBike(packageId: String?,
        request: UpdatePackageNameRequest): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.updateCustomPackageNameBike(packageId, request)
    }

    override fun deletePackageBike(packageId: String): Deferred<Response<ServerResponse<PackageIdResponse>>> {
        return api.deleteCustomPackageBike(packageId)
    }

    override fun addBikeServiceByOwn(path: String,
        req: AddServiceByOwnReq): Deferred<Response<ServerResponse<Any>>> {
        return api.addBikeServiceByOwn(path, req)
    }

    override fun deleteBikeService(@Path("path") path: String,
        @Query("id") id: String): Deferred<Response<ServerResponse<Any>>> {
        return api.deleteBikeService(path, id)
    }

    override fun getAccCustomPackageDetailsBike(packageId: String): Deferred<Response<ServerResponse<GetAccCustomPackageDetails>>> {
        return api.getAccCustomPackageDetailsBike(packageId)
    }

    override fun getRetailersCatalogueBike(type: String): Deferred<Response<ServerResponse<List<GetInventoryPartItemResponse>>>> {
        return api.getRetailersCatalogueBike(type)
    }

    override fun removeOOSItems(req: JsonObject): Deferred<Response<ServerResponse<Any>>> {
        return api.removeOOSItems(req)
    }

    override fun getEnquiryCost(): Deferred<Response<ServerResponse<Map<String,Int>>>> {
        return api.getEnquiryCost()
    }

    override fun getPreviouslyAddedItem(): Deferred<Response<ServerResponse<List<SSItemModel>>>> {
        return api.getPreviouslyAddedItem()
    }

    override fun addPrevOrderedItemsToCart(items: List<SSItemModel>): Deferred<Response<ServerResponse<AddPrevOrderItemsReq>>> {
        return api.addPrevOrderedItemsToCart(AddPrevOrderItemsReq(items))
    }

    override fun getEstimatedDelivery(pin: String?): Deferred<Response<ServerResponse<EstimatedDeliveryResponse>>> {
        return api.getEstimatedDelivery(pin)
    }

    override fun getTextFromImage(file: MultipartBody.Part?,formData: RequestBody): Deferred<Response<ServerResponse<List<ImageToTextResponse>>>> {
        return api.getTextFromImage(file,formData)
    }

    override fun buyMembership(map: MutableMap<String, String>): Deferred<Response<ServerResponse<JsonElement>>> {
        return api.buyMembership(map)
    }

    override fun buySubs(amount:String?): Deferred<Response<ServerResponse<SubsResponse>>> {
        return api.buySubs(amount)
    }

    override fun verifyGST(gstNumber: String): Deferred<Response<ServerResponse<GSTVeriifyRespoonse>>> {
        return api.verifyGST(gstNumber)
    }

    override fun getMembershipConfig(): Deferred<Response<ServerResponse<MembershipPageConfig>>> {
        return api.getMembershipConfig()
    }

    override fun getWorkshop(id:String): Deferred<Response<ServerResponse<List<WorkshopListResponseModel>>>> {
        return api.getWorkshop(id)
    }

}