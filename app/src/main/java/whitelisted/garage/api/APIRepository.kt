@file:Suppress("DeferredIsResult")

package whitelisted.garage.api

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import kotlinx.coroutines.Deferred
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.*
import whitelisted.garage.api.data.GetAccessoriesCatalogueResponse
import whitelisted.garage.api.request.*
import whitelisted.garage.api.response.*
import whitelisted.garage.api.response.login.GetProfileCompletion
import whitelisted.garage.api.response.login.LoginUserResponse
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.utils.AppStorePreferences
import whitelisted.garage.view.fragments.orderInventory.OrderInventoryRequest

interface APIRepository {

    fun getSharedPreferences(): AppStorePreferences

    fun getOTP(hashCode: String?,
        countryCode: String?,
        mobileNumber: String?): Deferred<Response<ServerResponse<String>>>

    fun signUpUser(signUpRequest: SignUpRequest?): Deferred<Response<ServerResponse<LoginUserResponse>>>

    fun loginUser(loginRequest: LoginRequest): Deferred<Response<ServerResponse<LoginUserResponse>>>

    fun addEmployee(workshopId: String,
        addEditEmployee: AddEditEmployee): Deferred<Response<ServerResponse<LoginUserResponse>>>

    fun editEmployee(addEditEmployee: AddEditEmployee,
        editEmployeeId: String?): Deferred<Response<ServerResponse<LoginUserResponse>>>

    fun verifyOTP(verifyOtpRequest: VerifyOtpRequest): Deferred<Response<ServerResponse<Any>>>

    fun getManageEmployeeData(workshopId: String): Deferred<Response<ServerResponse<List<EmployeeListResponseModel>>>>

    fun resetPassword(resetPasswordRequest: ResetPasswordRequest): Deferred<Response<ServerResponse<Any>>>

    fun getWorkshopList(): Deferred<Response<ServerResponse<List<WorkshopListResponseModel>>>>

    fun getSubscriptionStatus(workshopId: String): Deferred<Response<ServerResponse<SubscriptionStatusResponse>>>

    fun getHomeScreen(workshopId: String?): Deferred<Response<ServerResponse<HomeScreenResponse>>>

    fun placeOrderAPI(request: NewOrderRequest?): Deferred<Response<ServerResponse<PlaceOrderResponse>>>

    fun getOrderDetail(orderId: String?): Deferred<Response<ServerResponse<OrderDetailResponse>>>

    fun getInventoryCheckList(isTwoWheeler: Boolean): Deferred<Response<ServerResponse<List<String>>>>

    fun getCarDocsList(): Deferred<Response<ServerResponse<List<String>>>>

    fun getInventoryDetail(orderId: String): Deferred<Response<ServerResponse<OrderInventoryRequest>>>

    fun getCarList(): Deferred<Response<ServerResponse<List<CarListModel>>>>

    fun getServicesList(searchedString: String?): Deferred<Response<ServerResponse<List<ServiceResponseModel>>>>

    fun getPackagesList(): Deferred<Response<ServerResponse<List<PackagesResponse>>>>

    fun addWorkshop(addWorkshopRequest: AddWorkshopRequest): Deferred<Response<ServerResponse<WorkshopIDResponse>>>

    fun sendReminder(orderId: String?): Deferred<Response<ServerResponse<OrderIdResponse>>>

    fun deleteWorkshop(workshopId: String): Deferred<Response<ServerResponse<WorkshopIDResponse>>>

    fun deleteEmployee(id: String): Deferred<Response<ServerResponse<WorkshopIDResponse>>>

    fun getUserProfile(id: String): Deferred<Response<ServerResponse<GetUserProfileResponse>>>
    fun updateUserProfile(id: String,
        updateProfileRequest: UpdateProfileRequest): Deferred<Response<ServerResponse<JsonElement>>>

    fun updateWorkshop(workshopId: String,
        addWorkshopRequest: AddWorkshopRequest): Deferred<Response<ServerResponse<WorkshopIDResponse>>>

    fun getWorkDoneList(): Deferred<Response<ServerResponse<List<WorkDoneResponseModel>>>>

    fun getCustomPackagesList(): Deferred<Response<ServerResponse<List<AllPackagesResponseModel>>>>
    fun getMyCustomerListWithSearch(search: String): Deferred<Response<ServerResponse<List<MyCustomerModel>>>>
    fun getMyCustomerList(): Deferred<Response<ServerResponse<List<MyCustomerModel>>>>

    fun createCustomPackage(createPackageRequest: CreateCustomPackageRequest): Deferred<Response<ServerResponse<PackageIdResponse>>>

    fun deletePackage(packageId: String?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    fun updateServiceOfCustomPackage(packageId: String?,
        serviceId: Int?,
        servicesItem: ServicesItem): Deferred<Response<ServerResponse<PackageIdResponse>>>

    fun addServiceToCustomPackage(packageId: String?,
        item: ServicesItem): Deferred<Response<ServerResponse<PackageIdResponse>>>

    fun deleteServiceOfCustomPackage(packageId: String?,
        serviceId: Int?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    fun getServicesOfPackage(packageId: String?): Deferred<Response<ServerResponse<List<ServicesItem>>>>

    fun updatePackageName(packageId: String?,
        request: UpdatePackageNameRequest?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    fun getExistingPackages(): Deferred<Response<ServerResponse<List<AllPackagesResponseModel>>>>

    fun duplicatePackage(packageId: String?): Deferred<Response<ServerResponse<PackageIdResponse>>>

    fun getRemindersList(type: String?,
        periodType: String): Deferred<Response<ServerResponse<RemindersListResponse>>>

    fun getBillingList(startDate: String,
        endDate: String): Deferred<Response<ServerResponse<List<BillingResponseModel>>>>

    fun getOtpForLogin(hashCode: String?,
        countryCode: String?,
        mobileNumber: String?): Deferred<Response<ServerResponse<String>>>

    fun verifyOTPForLogin(req: VerifyOtpRequest): Deferred<Response<ServerResponse<LoginUserResponse>>>

    fun getRoleBasedEmployees(type: String): Deferred<Response<ServerResponse<List<EmployeeListResponseModel>>>>

    fun updateOrderDetail(orderId: String?,
        value: NewOrderRequest?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>

    fun getOrdersList(type: String?): Deferred<Response<ServerResponse<OrderListResponse>>>
    fun getFilteredOrdersList(
        date_filter: String?,
        start_date: String?,
        end_date: String?,
        limit: Int,
        offset: Int
    ): Deferred<Response<ServerResponse<OrderListResponse>>>

    fun getLedger(type: String?,
        transactionType: String?): Deferred<Response<ServerResponse<LedgerResponseModel>>>

    fun addLedger(addToLedgerRequest: AddToLedgerRequest): Deferred<Response<ServerResponse<LedgerResponseModel>>>
    fun addInventory(orderInventoryDetailResponse: OrderInventoryRequest): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>
    fun getOrdersBySearch(searchedString: String?): Deferred<Response<ServerResponse<List<SearchOrderResponseModel>>>>
    fun addJobCard(jobCarRequest: JobCardRequest?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>
    fun getJobCard(orderId: String?): Deferred<Response<ServerResponse<List<JobCardRequest>>>>
    fun getOrderEstimates(orderId: String?): Deferred<Response<ServerResponse<List<OrderEstimateRequestResponse>>>>
    fun updateOrderEstimates(request: OrderEstimateRequestResponse?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>
    fun getPaymentDetails(orderId: String?): Deferred<Response<ServerResponse<List<GetPaymentResponseModel>>>>
    fun addPayment(paymentModel: AddPaymentRequest?): Deferred<Response<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>
    fun completePayment(orderId: OrderIdResponse?): Deferred<Response<ServerResponse<JSONObject>>>
    fun getMessageFromAPI(orderId: String?): Deferred<Response<ServerResponse<GetMessageFromApiResponse>>>
    fun getMessageFromAPIRetailer(mobile: String?): Deferred<Response<ServerResponse<GetMessageFromApiResponse>>>
    fun getBannerImages(): Deferred<Response<ServerResponse<BannerImagesResponse>>>
    fun trueCallerLogin(trueCallerLoginRequest: TrueCallerLoginRequest): Deferred<Response<ServerResponse<LoginUserResponse>>>
    fun downloadLedgerCSV(type: String): Deferred<Response<ResponseBody>>
    fun downloadBillingCSV(startDate: String, endDate: String): Deferred<Response<ResponseBody>>
    fun downloadOrderHistoryCSV(orderType: String?,
        startDate: String?,
        endDate: String?): Deferred<Response<ResponseBody>>

    fun cancelOrder(cancelOrderReq: CancelOrderRequest?): Deferred<Response<ServerResponse<JSONObject>>>
    fun getUPIId(): Deferred<Response<ServerResponse<GetUPIResponse>>>
    fun saveUPIId(saveUPIIdRequest: SaveUPIIdRequest): Deferred<Response<ServerResponse<JsonElement>>>
    fun getOrderSummary(orderId: String?): Deferred<Response<ServerResponse<List<OrderSummaryResponse>>>>
    fun getCarousel(): Deferred<Response<ServerResponse<HomeCarouselResponse>>>
    fun getOTPForProfile(mobile: String): Deferred<Response<ServerResponse<String>>>
    fun verifyOTPForProfile(verifyOtpRequest: VerifyOtpRequest): Deferred<Response<ServerResponse<Any>>>
    fun searchRemindersList(reminderType: String?): Deferred<Response<ServerResponse<RemindersListResponse>>>
    fun scheduleReminder(orderId: String?,
        schedule_at: String?): Deferred<Response<ServerResponse<OrderIdResponse>>>

    fun getConfigAPI(): Deferred<Response<ServerResponse<ScreensConfigResponse>>>
    fun getUpdateInfo(): Deferred<Response<ServerResponse<GetUpdateInfoResponse>>>
    fun getGMBCategories(): Deferred<Response<ServerResponse<GetGMDCategoriesResponse>>>
    fun getGoCoinScreenResponse(): Deferred<Response<ServerResponse<GetGoCoinsScreenResponse>>>
    fun getPin(userId: String): Deferred<Response<ServerResponse<SetPinRequest>>>
    fun setPin(userId: String,
        req: SetPinRequest): Deferred<Response<ServerResponse<SetPinRequest>>>

    fun getGMB(): Deferred<Response<ServerResponse<GetGMDResponse>>>
    fun saveGMB(saveGMBRequest: SaveGMBRequest): Deferred<Response<ServerResponse<JsonElement>>>
    fun getGMBAttributes(): Deferred<Response<ServerResponse<MutableMap<String, MutableList<String>>>>>
    fun getGoCoinsOptions(): Deferred<Response<ServerResponse<GetGOCoinOptionsResponse>>>
    fun unLockGMB(unlockGMBRequest: UnlockGMBRequest): Deferred<Response<ServerResponse<JsonElement>>>
    fun getBrandsList(): Deferred<Response<ServerResponse<List<BrandsResponseModel>>>>
    fun getCarModelList(brandId: String?): Deferred<Response<ServerResponse<List<CarModelResponseModel>>>>
    fun getFuelTypeList(modelId: String?): Deferred<Response<ServerResponse<FuelTypeListResponse>>>
    fun getOrderIdForPayment(map: MutableMap<String, String>): Deferred<Response<ServerResponse<OrderIdForPaymentResponse>>>
    fun getRazorPayKey(): Deferred<Response<ServerResponse<GetRazorPayKey>>>
    fun verifyPaymentDetails(map: MutableMap<String, String>): Deferred<Response<ServerResponse<JsonElement>>>
    fun getGoCoinTransactions(type: String,
        period: String): Deferred<Response<ServerResponse<GCTransactionsResponse>>>

    fun getRetailersCatalogue(type: String): Deferred<Response<ServerResponse<List<GetInventoryPartItemResponse>>>>
    fun createInventoryRack(request: CreateInventoryRackRequest): Deferred<Response<ServerResponse<RackIdResponse>>>
    fun updateInventoryRack(id: String,
        request: CreateInventoryRackRequest): Deferred<Response<ServerResponse<JsonElement>>>

    fun deleteInventoryRack(id: String): Deferred<Response<ServerResponse<JsonElement>>>
    fun getInventoryRack(): Deferred<Response<ServerResponse<MutableList<GetInventoryRackResponse>>>>
    fun getIndividualInventoryRack(id: String): Deferred<Response<ServerResponse<GetInventoryRackResponse>>>
    fun updateInventoryRackDetails(id: String,
        update_only_rack: Int,
        request: CreateInventoryRackRequest): Deferred<Response<ServerResponse<JsonElement>>>

    fun createUpdateRetailOrder(createOrderRequest: RetailCreateOrderRequest): Deferred<Response<ServerResponse<RetailPlaceOrderResponse>>>
    fun getSavedWorkshops(): Deferred<Response<ServerResponse<List<SavedWorkshopResponseModel>>>>
    fun searchImages(q: String): Deferred<Response<ServerResponse<List<SearchImageResponse>>>>
    fun getGoCoinBalance(): Deferred<Response<ServerResponse<GetGoCoinBalanceResponse>>>
    fun getAccessoriesCatalogue(type: String): Deferred<Response<ServerResponse<MutableList<GetAccessoriesCatalogueResponse>>>>
    fun getRetailOrderDetail(orderId: String): Deferred<Response<ServerResponse<RetailOrderDetailResponse>>>
    fun getAccCustomPackageDetails(packageId: String?): Deferred<Response<ServerResponse<GetAccCustomPackageDetails>>>

    fun updateOrderInclusions(request: OrderInclusionsRequestResponse?): Deferred<Response<ServerResponse<OrderInclusionsRequestResponse>>>
    fun getOrderInclusions(orderId: String?): Deferred<Response<ServerResponse<List<OrderInclusionsRequestResponse>>>>
    fun downloadInventoryCSV(): Deferred<Response<ResponseBody>>
    fun getGoCoinsValue(): Deferred<Response<ServerResponse<GoCoinsValueResponse>>>
    fun updateInventoryItemValue(request: UpdateInventoryItemRequest): Deferred<Response<ServerResponse<JsonElement>>>
    fun getWebsite(): Deferred<Response<ServerResponse<GetWebsiteResponse>>>
    fun saveGMBWebsite(saveGMBRequest: SaveWebsiteRequest): Deferred<Response<ServerResponse<JsonElement>>>
    fun addItemToRack(id: String?,
        req: CreateInventoryRackRequest?): Deferred<Response<ServerResponse<JsonElement>>>

    fun getReferralCode(): Deferred<Response<ServerResponse<GetReferralCodeResponse>>>
    fun availReferCode(req: AvailReferralRequest): Deferred<Response<ServerResponse<JsonElement>>>
    fun getProfileCompletion(wrokshopId: String): Deferred<Response<ServerResponse<GetProfileCompletion>>>
    fun updateProfileCompletion(userId: String,
        request: SaveProfileCompletionRequest): Deferred<Response<ServerResponse<JsonElement>>>

    fun updateInventoryItemValueForWorkshop(updateInventoryItemRequest: UpdateInventoryItemRequest): Deferred<Response<ServerResponse<JsonElement>>>
    fun sendDuesReminder(req: OrderIdResponse): Deferred<Response<ServerResponse<MsgKeyResponse>>>
    fun collectDuesAPI(req: LedgerIdRequest): Deferred<Response<ServerResponse<JsonElement>>>
    fun getAllGoCoinTransactionsCSV(type: String, period: String): Deferred<Response<ResponseBody>>
    fun updateUserLocation(userId: String,
        request: UpdateLocationRequest): Deferred<Response<ServerResponse<JsonElement>>>

    fun getMostUsedPartsRetailerCatalogue(isTwoWheeler:Boolean): Deferred<Response<ServerResponse<MutableList<MostUsedPartsResponseModel>>>>
    fun getMostUsedPartsWorkshopCatalogue(isTwoWheeler:Boolean): Deferred<Response<ServerResponse<MutableList<MostUsedPartsResponseModel>>>>
    fun getAllCoupons(type: String): Deferred<Response<ServerResponse<GetAllCouponsResponse>>>
    fun applyCoupon(code: String?,
        type: String): Deferred<Response<ServerResponse<ApplyCouponResponse>>>

    fun getLocationDetails(ipAddress: String): Deferred<Response<ServerResponse<LocationDetailsResponse>>>
    fun getCountryCodes(): Deferred<Response<ServerResponse<MutableList<CountryCodeResponseModel>>>>
    fun getPreviousMonthData(): Deferred<Response<ServerResponse<PreviousMonthResponse>>>
    fun getRetryPaymentStatus(): Deferred<Response<ServerResponse<RetryPaymentResponse>>>
    fun cancelRetryPayment(request: IdRequestResponse): Deferred<Response<ServerResponse<JsonElement>>>
    fun sendInstantReminder(request: SendInstantReminderRequest): Deferred<Response<ServerResponse<JsonElement>>>
    fun getCustomerReminder(): Deferred<Response<ServerResponse<GetCustomerReminderResponse>>>
    fun getAllCustomerReminder(): Deferred<Response<ServerResponse<GetCustomerReminderResponse>>>
    fun collectDueOnReminder(request: PreviousReminder): Deferred<Response<ServerResponse<JsonElement>>>
    fun getHomeScreenForMonth(workshopId: String?,
        monthPosition: String): Deferred<Response<ServerResponse<HomeScreenResponse>>>

    fun createWorkshopBiddingAsync(request: WorkShopBiddingRequest): Deferred<Response<ServerResponse<JsonElement>>>
    fun getNewBidsList(): Deferred<Response<ServerResponse<MutableList<AccRetailBidListResponseModel>>>>
    fun getSubmittedBidsList(): Deferred<Response<ServerResponse<MutableList<AccRetailBidListResponseModelSubmitAccept>>>>
    fun getAcceptedBidsList(): Deferred<Response<ServerResponse<MutableList<AccRetailBidListResponseModelSubmitAccept>>>>
    fun getOnGoingWsBiddingList(type: String?): Deferred<Response<ServerResponse<List<GetWsOngoingBidResponse>>>>
    fun getWsHistoryBiddingList(): Deferred<Response<ServerResponse<List<GetWsOngoingBidResponse>>>>
    fun getWsMostPart(): Deferred<Response<ServerResponse<GetWsMostUsedPart>>>
    fun getWsReceivedBids(id: String): Deferred<Response<ServerResponse<GetWsBidReceivedResponse>>>
    fun markCompleteWsBid(request: MarkCompleteWsBidRequest): Deferred<Response<ServerResponse<JsonElement>>>
    fun getWsBidEnquiry(id: String): Deferred<Response<ServerResponse<GetWsBidEnquiryResponse>>>
    fun changeStatusOfWsBid(id: String,
        req: WsAccRejBid): Deferred<Response<ServerResponse<JsonElement>>>

    fun getBidDetails(orderId: String): Deferred<Response<ServerResponse<BidDetailResponse>>>
    fun placeBid(bidId: String,
        bidDetail: BidDetailResponse): Deferred<Response<ServerResponse<JsonElement>>>

    fun cancelBidRetailer(bidId: String): Deferred<Response<ServerResponse<JsonElement>>>
    fun unlockContactRetailer(bidId: String,
        req: IdTypeRequest): Deferred<Response<ServerResponse<JsonElement>>>

    fun unlockPhoneWorkshop(bidId: String,
        req: IdTypeRequest): Deferred<Response<ServerResponse<JsonElement>>>

    fun markCompleteBidRetailer(bidId: String,
        req: IdTypeRequest): Deferred<Response<ServerResponse<JsonElement>>>

    fun cancelWorkshopBid(bidId: String?): Deferred<Response<ServerResponse<JsonElement>>>
    fun getWsAccSpareBiddingCSV(id: String): Deferred<Response<ServerResponse<JsonElement>>>
    fun getNearByBidingWorkShop(): Deferred<Response<ServerResponse<Map<String, GetNearByBidingWorkshop>>>>
    fun updateDeviceTokenForFCM(fingerprintRequest: FingerprintRequest): Deferred<Response<ServerResponse<JsonElement>>>

    fun downloadSampleCSV(): Deferred<Response<ResponseBody>>
    fun uploadInventoryCSVFile(file: MultipartBody.Part): Deferred<Response<ServerResponse<Any>>>
    fun getAvailableTemplates(): Deferred<Response<ServerResponse<List<BusinessCardTemplate>>>>
    fun getSavedCards(): Deferred<Response<ServerResponse<List<BusinessCardDetails>>>>
    fun saveBusinessCard(details: BusinessCardDetails): Deferred<Response<ServerResponse<Any>>>
    fun getSSItemsWithFilter(limit: Int,
        offset: Int): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>>

    fun getEmployeePerformance(employeeId: String,
        month: Int,
        year: Int): Deferred<Response<ServerResponse<EmployeePerformanceResponse>>>

    //    fun getSSBrandsResponse(): Deferred<Response<ServerResponse<MutableList<SSBrandsResponseModel>>>>
    fun addToCartSS(productId: String?,
        itemModel: SSItemModel): Deferred<Response<ServerResponse<GetSSCartResponse>>>

    fun getCartSS(): Deferred<Response<ServerResponse<GetSSCartResponse>>>

    //    fun getSSCategoriesResponse(): Deferred<Response<ServerResponse<MutableList<SSCategoryResponseModel>>>>
    fun getSSSparesBrandsResponse(): Deferred<Response<ServerResponse<MutableList<SSBrandsResponseModel>>>>
    fun getSSSparesCategoriesResponse(): Deferred<Response<ServerResponse<MutableList<SSCategoryResponseModel>>>>
    fun getRazorPayKeyForSS(): Deferred<Response<ServerResponse<GetRazorPayKey>>>
    fun getOrderIdForSSPayment(amountRequest: AmountRequest): Deferred<Response<ServerResponse<OrderIdForPaymentResponse>>>
    fun checkoutSSOrder(ssCheckoutRequest: SSCheckoutRequest): Deferred<Response<ServerResponse<Any>>>
    fun deleteBusinessCard(cardId: String): Deferred<Response<ServerResponse<Any>>>
    fun getAllAddress(): Deferred<Response<ServerResponse<MutableList<AddressResponseModel>>>>

    fun getSparesPurchaseHistory(  limit: Int,
        offset: Int): Deferred<Response<ServerResponse<List<SparesOrderHistory>>>>
    fun getSparesPurchaseHistory(map:Map<String,String>): Deferred<Response<ServerResponse<List<SparesOrderHistory>>>>
    fun addAddressAPI(addressResponseModel: AddressResponseModel): Deferred<Response<ServerResponse<Any>>>
    fun deleteShippingAddress(addressId: String): Deferred<Response<ServerResponse<Any>>>
    fun editAddressAPI(addressResponseModel: AddressResponseModel): Deferred<Response<ServerResponse<Any>>>
    fun getSSItemsWithBrandFilter(brandName: String?,
        limit: Int,
        offset: Int): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>>

    fun getSSItemsWithCategoryFilter(categoryName: String?,
        limit: Int,
        offset: Int): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>>

    fun getSSHomeBannersResponse(): Deferred<Response<ServerResponse<MutableList<SSHomeBannersResponseModel>>>>
    fun getSearchSSItems(searchString: String,
        limit: Int,
        offset: Int): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>>

    fun getMarketPlaceCoupons(): Deferred<Response<ServerResponse<List<MarketPlaceCoupon>>>>
    fun applyMarketPlaceCoupon(req: ShopApplyCouponReq): Deferred<Response<ServerResponse<Any>>>
    fun removeMarketPlaceCoupon(cartId: String): Deferred<Response<ServerResponse<Any>>>
    fun getProductSearchById(productId: String): Deferred<Response<ServerResponse<SSItemModel>>>
    fun getSSItemsWithCarFilter(brand: String?,
        category: String?,
        brandName: String?,
        modelName: String?,
        limit: Int,
        offset: Int): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>>

    fun getPDPBannerImage(): Deferred<Response<ServerResponse<PDPBannerResponse>>>
    fun getMarketplaceFAQ(): Deferred<Response<ServerResponse<List<FaqsItem>>>>
    fun getTrackingURL(): Deferred<Response<ServerResponse<Map<String, String>>>>
    fun getMarketplaceFilters(): Deferred<Response<ServerResponse<List<MarketplaceFilter>>>>

    fun addServiceByOwn(req: AddServiceByOwnReq): Deferred<Response<ServerResponse<Any>>>
    fun addBikeServiceByOwn(path: String, req: AddServiceByOwnReq): Deferred<Response<ServerResponse<Any>>>
    fun getSSPartsListFilter(filters: Map<String, String>): Deferred<Response<ServerResponse<MutableList<SSItemModel>>>>
    fun notifyUser(productIdJson: JsonObject): Deferred<Response<ServerResponse<JsonElement>>>
    fun getNotifiedItemsList(): Deferred<Response<ServerResponse<MutableList<NotifiedItemResponseModel>>>>

    fun addCustomItemSparesRetailer(req: JsonObject): Deferred<Response<ServerResponse<Map<String, String>>>>

    fun addCustomItemAccessories(req: JsonObject): Deferred<Response<ServerResponse<Map<String, String>>>>
    fun deleteCustomPart(id: String): Deferred<Response<ServerResponse<Any>>>
    fun deleteCustomAccessoriesPart(skuCode: String): Deferred<Response<ServerResponse<Any>>>
    fun deleteCustomRetailersCataloguePart(skuCode: String): Deferred<Response<ServerResponse<Any>>>

    fun getSearchAndFilterAttributes(): Deferred<Response<ServerResponse<SearchAndFilterAttrs>>>
    fun getSSAccBrandsResponse(): Deferred<Response<ServerResponse<MutableList<SSBrandsResponseModel>>>>
    fun getSSAccCategoriesResponse(): Deferred<Response<ServerResponse<MutableList<SSCategoryResponseModel>>>>
    fun getSSOriginalBrandsResponse(): Deferred<Response<ServerResponse<MutableList<SSBrandsResponseModel>>>>
    fun getPaymentTypes(req:GetPaymentModesRequest): Deferred<Response<ServerResponse<List<PaymentTypesResponseModel>>>>
    fun getPaymentTypesGet(): Deferred<Response<ServerResponse<List<PaymentTypesResponseModel>>>>
    fun getPopularItems(type: String): Deferred<Response<ServerResponse<List<SSItemModel>>>>

    fun getEmiAmount(amount: Double): Deferred<Response<ServerResponse<Map<String, String>>>>
    fun buyNowProduct(productId: String?,
        product: SSItemModel?): Deferred<Response<ServerResponse<IdRequestResponse>>>

    fun getSparesHomeResponse(): Deferred<Response<ServerResponse<SparesHomeResponseModel>>>
    fun getMarketplacePdpFAQ(): Deferred<Response<ServerResponse<List<FaqsItem>>>>
    fun sendEnquiry(enquiry: Enquiry): Deferred<Response<ServerResponse<Any>>>
    fun getAllEnquiries(): Deferred<Response<ServerResponse<List<Enquiry>>>>
    fun getBikeBrandsList(): Deferred<Response<ServerResponse<List<BrandsResponseModel>>>>
    fun getBikeModelList(brandId: String?): Deferred<Response<ServerResponse<List<CarModelResponseModel>>>>
    fun getBikeFuelTypeList(modelId: String?): Deferred<Response<ServerResponse<FuelTypeListResponse>>>
    fun getBikeServicesList(search: String?): Deferred<Response<ServerResponse<List<ServiceResponseModel>>>>
    fun createCustomPackageBike(createPackageRequest: CreateCustomPackageRequest): Deferred<Response<ServerResponse<PackageIdResponse>>>
    fun getAccessoriesCatalogueBike(searchText: String): Deferred<Response<ServerResponse<MutableList<GetAccessoriesCatalogueResponse>>>>
    fun getCustomPackagesListBike(): Deferred<Response<ServerResponse<List<AllPackagesResponseModel>>>>
    fun getServicesOfPackageBike(packageId: String?): Deferred<Response<ServerResponse<List<ServicesItem>>>>
    fun updateServiceOfCustomPackageBike(packageId: String,
        serviceId: Int,
        servicesItem: ServicesItem): Deferred<Response<ServerResponse<PackageIdResponse>>>

    fun addServiceToCustomPackageBike(packageId: String, item: ServicesItem): Deferred<Response<ServerResponse<PackageIdResponse>>>
    fun deleteServiceOfCustomPackageBike(packageId: String?, serviceId: Int?): Deferred<Response<ServerResponse<PackageIdResponse>>>
    fun updatePackageNameBike(packageId: String?, request: UpdatePackageNameRequest):  Deferred<Response<ServerResponse<PackageIdResponse>>>
    fun deletePackageBike(packageId: String):  Deferred<Response<ServerResponse<PackageIdResponse>>>
    fun deleteBikeService(path: String, id: String): Deferred<Response<ServerResponse<Any>>>
    fun getAccCustomPackageDetailsBike(packageId: String): Deferred<Response<ServerResponse<GetAccCustomPackageDetails>>>
    fun getRetailersCatalogueBike(type: String): Deferred<Response<ServerResponse<List<GetInventoryPartItemResponse>>>>
    fun removeOOSItems(req: JsonObject): Deferred<Response<ServerResponse<Any>>>

    fun getEnquiryCost(): Deferred<Response<ServerResponse<Map<String,Int>>>>
    fun getPreviouslyAddedItem(): Deferred<Response<ServerResponse<List<SSItemModel>>>>
    fun addPrevOrderedItemsToCart(items: List<SSItemModel>): Deferred<Response<ServerResponse<AddPrevOrderItemsReq>>>
    fun getEstimatedDelivery(pin: String?): Deferred<Response<ServerResponse<EstimatedDeliveryResponse>>>
    fun verifyGST(gstNumber: String): Deferred<Response<ServerResponse<GSTVeriifyRespoonse>>>

    fun getTextFromImage(file: MultipartBody.Part?,formData: RequestBody): Deferred<Response<ServerResponse<List<ImageToTextResponse>>>>

    fun getMembershipConfig(): Deferred<Response<ServerResponse<MembershipPageConfig>>>
    fun buyMembership(map: MutableMap<String, String>): Deferred<Response<ServerResponse<JsonElement>>>
    fun buySubs(amount:String?) : Deferred<Response<ServerResponse<SubsResponse>>>

    fun getWorkshop(id:String): Deferred<Response<ServerResponse<List<WorkshopListResponseModel>>>>
}