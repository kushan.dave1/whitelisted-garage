package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import org.json.JSONObject
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.AddPaymentRequest
import whitelisted.garage.api.request.SaveUPIIdRequest
import whitelisted.garage.api.response.GetPaymentResponseModel
import whitelisted.garage.api.response.GetUPIResponse
import whitelisted.garage.api.response.OrderDetailWrapperPostUpdateResponse
import whitelisted.garage.api.response.OrderIdResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment

class PaymentViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {
    private lateinit var getUPIResponse: MutableLiveData<Result<ServerResponse<GetUPIResponse>>>

    private lateinit var getPaymentResponse: MutableLiveData<Result<ServerResponse<List<GetPaymentResponseModel>>>>
    fun provideGetPaymentResponse(): MutableLiveData<Result<ServerResponse<List<GetPaymentResponseModel>>>> {
        if (!::getPaymentResponse.isInitialized) {
            getPaymentResponse = MutableLiveData()
        }
        return getPaymentResponse
    }

    private lateinit var addPaymentResponse: MutableLiveData<Result<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>
    fun provideAddPaymentResponse(): MutableLiveData<Result<ServerResponse<OrderDetailWrapperPostUpdateResponse>>> {
        if (!::addPaymentResponse.isInitialized) {
            addPaymentResponse = MutableLiveData()
        }
        return addPaymentResponse
    }

    private lateinit var completePaymentResponse: MutableLiveData<Result<ServerResponse<JSONObject>>>
    fun provideCompletePaymentResponse(): MutableLiveData<Result<ServerResponse<JSONObject>>> {
        if (!::completePaymentResponse.isInitialized) {
            completePaymentResponse = MutableLiveData()
        }
        return completePaymentResponse
    }

    fun addPaymentAPI(paymentModel: AddPaymentRequest) {
        viewModelScope.launch {
            val result = apiRepo.addPayment(paymentModel).awaitAndGet()
            addPaymentResponse.postValue(result)
        }
    }

    fun getPaymentDetails() {
        viewModelScope.launch {
            val result = apiRepo.getPaymentDetails(OrderDetailWrapperFragment.orderId).awaitAndGet()
            getPaymentResponse.postValue(result)
        }
    }

    fun markComplete() {
        val orderIdReq = OrderIdResponse()
        orderIdReq.orderId = OrderDetailWrapperFragment.orderId
        viewModelScope.launch {
            val result = apiRepo.completePayment(orderIdReq).awaitAndGet()
            completePaymentResponse.postValue(result)
        }
    }

    fun provideGetUPIResponse(): MutableLiveData<Result<ServerResponse<GetUPIResponse>>> {
        if (!::getUPIResponse.isInitialized) {
            getUPIResponse = MutableLiveData()
        }
        return getUPIResponse
    }

    fun getUPIId() {
        viewModelScope.launch {
            val result = apiRepo.getUPIId().awaitAndGet()
            getUPIResponse.postValue(result)
        }
    }

    private lateinit var saveUPIResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>

    fun provideSaveUPIResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::saveUPIResponse.isInitialized) {
            saveUPIResponse = MutableLiveData()
        }
        return saveUPIResponse
    }

    fun saveUPIId(saveUPIIdRequest: SaveUPIIdRequest, isFirstTime: Boolean) {
        if (isFirstTime) saveUPIIdRequest.firstTime = isFirstTime
        viewModelScope.launch {
            val result = apiRepo.saveUPIId(saveUPIIdRequest).awaitAndGet()
            saveUPIResponse.postValue(result)
        }
    }

}