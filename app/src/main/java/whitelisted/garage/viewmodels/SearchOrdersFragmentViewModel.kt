package whitelisted.garage.viewmodels

import android.app.Application
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.SearchOrderResponseModel
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class SearchOrdersFragmentViewModel(app: Application, apiRepo: APIRepository) :
    BaseViewModel(app, apiRepo) {

    private lateinit var searchOrdersResponse: MutableLiveData<Result<ServerResponse<List<SearchOrderResponseModel>>>>
    fun provideSearchOrdersResponse(): MutableLiveData<Result<ServerResponse<List<SearchOrderResponseModel>>>> {
        if (!::searchOrdersResponse.isInitialized) searchOrdersResponse = MutableLiveData()
        return searchOrdersResponse
    }

    fun getOrders(searchedString: String?) {
        Handler(Looper.getMainLooper()).postDelayed({
            launch {
                val result = apiRepo.getOrdersBySearch(searchedString).awaitAndGet()
                searchOrdersResponse.postValue(result)
            }
        }, 500)

    }


}