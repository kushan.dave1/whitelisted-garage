package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.OrderListResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class OrderHistoryFragmentViewModel(app: Application, apiRepo: APIRepository) :
    BaseViewModel(app, apiRepo) {
    private lateinit var orderListResponse: MutableLiveData<Result<ServerResponse<OrderListResponse>>>
    fun provideOrderListResponse(): MutableLiveData<Result<ServerResponse<OrderListResponse>>> {
        if (!::orderListResponse.isInitialized) {
            orderListResponse = MutableLiveData()
        }
        return orderListResponse
    }

    fun getFilteredOrdersList(orderType: String?,
        startDate: String?,
        endDate: String?,
        limit: Int,
        offset: Int) {
        viewModelScope.launch {
            val result = apiRepo.getFilteredOrdersList(orderType, startDate, endDate, limit, offset)
                .awaitAndGet()
            orderListResponse.postValue(result)
        }

    }

    private lateinit var downloadCSVResponse: MutableLiveData<ResponseBody>
    fun provideDownloadCSVResponse(): MutableLiveData<ResponseBody> {
        if (!::downloadCSVResponse.isInitialized) {
            downloadCSVResponse = MutableLiveData()
        }
        return downloadCSVResponse
    }

    fun downloadCSV(orderType: String?, startDate: String?, endDate: String?) {
        viewModelScope.launch {
            val result =
                apiRepo.downloadOrderHistoryCSV(orderType, startDate, endDate).awaitAndGet()
            when (result) {
                is Result.Success -> {
                    downloadCSVResponse.postValue(result.body)
                }
                is Result.Failure -> {

                }
            }
        }
    }
}