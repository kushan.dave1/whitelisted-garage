package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.BrandsResponseModel
import whitelisted.garage.api.response.CarModelResponseModel
import whitelisted.garage.api.response.FuelTypeListResponse
import whitelisted.garage.api.response.TypeItem
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class SelectCarDialogViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    var brandsList: List<BrandsResponseModel> = mutableListOf()
    var carModelsList: List<CarModelResponseModel> = mutableListOf()
    var fuelTypeList: List<TypeItem> = mutableListOf()

    private lateinit var selectedBrand: MutableLiveData<BrandsResponseModel>
    private lateinit var selectedModel: MutableLiveData<CarModelResponseModel>
    private lateinit var selectedFuelType: MutableLiveData<TypeItem>
    private lateinit var registrationNumber: MutableLiveData<String>
    private lateinit var odometerReading: MutableLiveData<String>

    fun provideSelectedBrand(): MutableLiveData<BrandsResponseModel> {
        return if (::selectedBrand.isInitialized) {
            selectedBrand
        } else {
            selectedBrand = MutableLiveData(BrandsResponseModel())
            return selectedBrand
        }
    }

    fun provideSelectedModel(): MutableLiveData<CarModelResponseModel> {
        return if (::selectedModel.isInitialized) {
            selectedModel
        } else {
            selectedModel = MutableLiveData(CarModelResponseModel())
            return selectedModel
        }
    }

    fun provideSelectedFuelType(): MutableLiveData<TypeItem> {
        return if (::selectedFuelType.isInitialized) {
            selectedFuelType
        } else {
            selectedFuelType = MutableLiveData(TypeItem())
            return selectedFuelType
        }
    }

    fun provideRegistrationNumber(): MutableLiveData<String> {
        return if (::registrationNumber.isInitialized) {
            registrationNumber
        } else {
            registrationNumber = MutableLiveData("")
            return registrationNumber
        }
    }

    fun provideOdometerReading(): MutableLiveData<String> {
        return if (::odometerReading.isInitialized) {
            odometerReading
        } else {
            odometerReading = MutableLiveData("")
            return odometerReading
        }
    }

    private lateinit var brandsListResponse: MutableLiveData<Result<ServerResponse<List<BrandsResponseModel>>>>
    fun provideBrandListResponse(): MutableLiveData<Result<ServerResponse<List<BrandsResponseModel>>>> {
        if (!::brandsListResponse.isInitialized) {
            brandsListResponse = MutableLiveData()
        }
        return brandsListResponse
    }

    private lateinit var modelsListResponse: MutableLiveData<Result<ServerResponse<List<CarModelResponseModel>>>>
    fun provideModelsListResponse(): MutableLiveData<Result<ServerResponse<List<CarModelResponseModel>>>> {
        if (!::modelsListResponse.isInitialized) {
            modelsListResponse = MutableLiveData()
        }
        return modelsListResponse
    }

    private lateinit var fuelTypeListResponse: MutableLiveData<Result<ServerResponse<FuelTypeListResponse>>>
    fun provideFuelTypeListResponse(): MutableLiveData<Result<ServerResponse<FuelTypeListResponse>>> {
        if (!::fuelTypeListResponse.isInitialized) {
            fuelTypeListResponse = MutableLiveData()
        }
        return fuelTypeListResponse
    }

    fun getBrandsList(isBikeSelection: Boolean = false) {
        viewModelScope.launch {
            val result = if (isBikeSelection) apiRepo.getBikeBrandsList()
                .awaitAndGet() else apiRepo.getBrandsList().awaitAndGet()
            brandsListResponse.postValue(result)
        }
    }

    fun getCarModelsList(brandId: String?, isBikeSelection: Boolean = false) {
        viewModelScope.launch {
            val result = if (isBikeSelection)apiRepo.getBikeModelList(brandId).awaitAndGet() else apiRepo.getCarModelList(brandId).awaitAndGet()
            modelsListResponse.postValue(result)
        }
    }

    fun getFuelTypeList(modelId: String?, isBikeSelection: Boolean = false) {
        viewModelScope.launch {
            val result = if (isBikeSelection)apiRepo.getBikeFuelTypeList(modelId).awaitAndGet() else apiRepo.getFuelTypeList(modelId).awaitAndGet()
            fuelTypeListResponse.postValue(result)
        }
    }

}