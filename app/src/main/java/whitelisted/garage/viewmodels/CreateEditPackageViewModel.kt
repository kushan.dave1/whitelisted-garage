package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.AddServiceByOwnReq
import whitelisted.garage.api.request.CreateCustomPackageRequest
import whitelisted.garage.api.request.ServicesItem
import whitelisted.garage.api.request.UpdatePackageNameRequest
import whitelisted.garage.api.response.PackageIdResponse
import whitelisted.garage.api.response.ServiceResponseModel
import whitelisted.garage.api.response.WorkDoneResponseModel
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM

class CreateEditPackageViewModel(application: Application, apiRepository: APIRepository) :
    BaseViewModel(application, apiRepository) {

    var searchJob :Job?= null

    private lateinit var workDoneResponseFromAPI: MutableLiveData<Result<ServerResponse<List<WorkDoneResponseModel>>>>
    fun provideWorkDoneListResponseFromAPI(): MutableLiveData<Result<ServerResponse<List<WorkDoneResponseModel>>>> {
        if (!::workDoneResponseFromAPI.isInitialized) {
            workDoneResponseFromAPI = MutableLiveData()
        }
        return workDoneResponseFromAPI
    }

    private lateinit var servicesListResponseFromAPI: MutableLiveData<Result<ServerResponse<List<ServiceResponseModel>>>>
    fun provideServicesListResponseFromAPI(): MutableLiveData<Result<ServerResponse<List<ServiceResponseModel>>>> {
        if (!::servicesListResponseFromAPI.isInitialized) {
            servicesListResponseFromAPI = MutableLiveData()
        }
        return servicesListResponseFromAPI
    }

    private lateinit var servicesOfPackageResponse: MutableLiveData<Result<ServerResponse<List<ServicesItem>>>>
    fun provideServicesOfResponse(): MutableLiveData<Result<ServerResponse<List<ServicesItem>>>> {
        if (!::servicesOfPackageResponse.isInitialized) {
            servicesOfPackageResponse = MutableLiveData()
        }
        return servicesOfPackageResponse
    }

    private lateinit var createPackageResponse: MutableLiveData<Result<ServerResponse<PackageIdResponse>>>
    fun provideCreatePackageResponse(): MutableLiveData<Result<ServerResponse<PackageIdResponse>>> {
        if (!::createPackageResponse.isInitialized) createPackageResponse = MutableLiveData()
        return createPackageResponse
    }

    private lateinit var updatePackageResponse: MutableLiveData<Result<ServerResponse<PackageIdResponse>>>
    fun provideUpdatePackageResponse(): MutableLiveData<Result<ServerResponse<PackageIdResponse>>> {
        if (!::updatePackageResponse.isInitialized) updatePackageResponse = MutableLiveData()
        return updatePackageResponse
    }

    fun getWorkDoneListAPI() {
        viewModelScope.launch {
            val result = apiRepo.getWorkDoneList().awaitAndGet()
            workDoneResponseFromAPI.postValue(result)
        }
    }

    fun getServicesListAPI(searchedString: String) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            val result = apiRepo.getServicesList(searchedString).awaitAndGet()
            servicesListResponseFromAPI.postValue(result)
        }
    }

    fun createCustomPackageAPI(createPackageRequest: CreateCustomPackageRequest,
        isTwoWheeler: Boolean) {
        viewModelScope.launch {
            val result = if (isTwoWheeler) apiRepo.createCustomPackageBike(createPackageRequest)
                .awaitAndGet() else apiRepo.createCustomPackage(createPackageRequest).awaitAndGet()
            createPackageResponse.postValue(result)
        }
    }

    fun updateService(packageId: String,
        serviceId: Int,
        servicesItem: ServicesItem,
        isTwoWheeler: Boolean) {
        viewModelScope.launch {
            val result = if (isTwoWheeler) apiRepo.updateServiceOfCustomPackageBike(packageId,
                serviceId,
                servicesItem).awaitAndGet() else apiRepo.updateServiceOfCustomPackage(packageId,
                serviceId,
                servicesItem).awaitAndGet()
            updatePackageResponse.postValue(result)
        }
    }

    fun addServiceToCustomPackage(packageId: String, item: ServicesItem, isTwoWheeler: Boolean) {
        viewModelScope.launch {
            val result = if (isTwoWheeler) apiRepo.addServiceToCustomPackageBike(packageId, item)
                .awaitAndGet() else apiRepo.addServiceToCustomPackage(packageId, item).awaitAndGet()
            updatePackageResponse.postValue(result)
        }
    }

    fun deleteService(packageId: String?, serviceId: Int?, isTwoWheeler: Boolean) {
        viewModelScope.launch {
            val result = if (isTwoWheeler)apiRepo.deleteServiceOfCustomPackageBike(packageId, serviceId).awaitAndGet() else apiRepo.deleteServiceOfCustomPackage(packageId, serviceId).awaitAndGet()
            updatePackageResponse.postValue(result)
        }
    }

    fun getServicesOfPackage(packageId: String?) {
        viewModelScope.launch {
            val result = apiRepo.getServicesOfPackage(packageId).awaitAndGet()
            servicesOfPackageResponse.postValue(result)
        }
    }

    fun getServicesOfPackageBike(packageId: String?) {
        viewModelScope.launch {
            val result = apiRepo.getServicesOfPackageBike(packageId).awaitAndGet()
            servicesOfPackageResponse.postValue(result)
        }
    }

    fun updatePackageName(packageId: String?, name: String?, totalAmount: Double?, isTwoWheeler: Boolean) {
        val request = UpdatePackageNameRequest()
        request.name = name
        request.total = totalAmount
        viewModelScope.launch {
            val result = if (isTwoWheeler)apiRepo.updatePackageNameBike(packageId, request).awaitAndGet() else apiRepo.updatePackageName(packageId, request).awaitAndGet()
            updatePackageResponse.postValue(result)
        }
    }

    suspend fun addAccessoriesPartByOwn(partName: String): Boolean = withContext(Dispatchers.IO) {
        val req = JsonObject().apply { addProperty("name", partName) }
        val isAdded: Boolean = when (apiRepo.addCustomItemAccessories(req).awaitAndGet()) {
            is Result.Success -> true
            else -> false
        }
        return@withContext isAdded
    }

    suspend fun addServiceByOwn(searchText: String): Boolean = withContext(Dispatchers.IO) {
        return@withContext when (apiRepo.addServiceByOwn(AddServiceByOwnReq(searchText))
            .awaitAndGet()) {
            is Result.Success<*> -> true
            else -> false
        }
    }

    fun getBikeServicesAPI(searchText: String) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            val result = apiRepo.getBikeServicesList(searchText).awaitAndGet()
            servicesListResponseFromAPI.postValue(result)
        }
    }


}