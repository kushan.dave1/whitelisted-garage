package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.EmployeeListResponseModel
import whitelisted.garage.api.response.EmployeePerformanceResponse
import whitelisted.garage.api.response.WorkshopIDResponse
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.database.repository.RoomRepository
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class ManageEmployeeFragmentViewModel(app: Application,
    apiRepo: APIRepository,
    var roomRepository: RoomRepository) : BaseViewModel(app, apiRepo) {


    suspend fun getEmployeePerformance(employeeId: String,month: Int, year: Int): EmployeePerformanceResponse? {
        return when(val result = apiRepo.getEmployeePerformance(employeeId,month,year).awaitAndGet()) {
            is Result.Success -> result.body.data
            else -> null
        }
    }

    private lateinit var workshopListResponseFromRoom: MutableLiveData<List<WorkshopListResponseModel>>
    fun provideWorkshopListResponseFromRoom(): MutableLiveData<List<WorkshopListResponseModel>> {
        if (!::workshopListResponseFromRoom.isInitialized) {
            workshopListResponseFromRoom = MutableLiveData()
        }
        return workshopListResponseFromRoom
    }

    fun getWorkshopsListFromRoom() {
        viewModelScope.launch {
            val result = roomRepository.getWorkshopList()
            if (!::workshopListResponseFromRoom.isInitialized) {
                workshopListResponseFromRoom = MutableLiveData(result)
            } else workshopListResponseFromRoom.postValue(result)

        }
    }

    fun getManageEmployeeData(workShopId: String) {
        viewModelScope.launch {
            val result = apiRepo.getManageEmployeeData(workShopId).awaitAndGet()
            getEmployeeLiveDataResponseModel.postValue(result)
        }
    }


    fun deleteEmployee(id: String) {
        viewModelScope.launch {
            val result = apiRepo.deleteEmployee(id).awaitAndGet()
            deleteWorkshopResponse.postValue(result)
        }
    }


    private lateinit var deleteWorkshopResponse: MutableLiveData<Result<ServerResponse<WorkshopIDResponse>>>
    fun provideDeleteWorkshopResponse(): MutableLiveData<Result<ServerResponse<WorkshopIDResponse>>> {
        if (!::deleteWorkshopResponse.isInitialized) {
            deleteWorkshopResponse = MutableLiveData()
        }
        return deleteWorkshopResponse
    }


    private lateinit var getEmployeeLiveDataResponseModel: MutableLiveData<Result<ServerResponse<List<EmployeeListResponseModel>>>>
    fun observeManageEmployee(): MutableLiveData<Result<ServerResponse<List<EmployeeListResponseModel>>>> {
        if (!::getEmployeeLiveDataResponseModel.isInitialized) {
            getEmployeeLiveDataResponseModel = MutableLiveData()
        }
        return getEmployeeLiveDataResponseModel
    }
}