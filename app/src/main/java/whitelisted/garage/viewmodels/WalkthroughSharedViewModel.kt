package whitelisted.garage.viewmodels

import android.app.Application
import whitelisted.garage.api.APIRepository
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.utils.SingleLiveEvent

class WalkthroughSharedViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private lateinit var homeWalkThroughEvent: SingleLiveEvent<Int>
    fun provideHomeWalkThroughEvent(): SingleLiveEvent<Int> {
        if (!::homeWalkThroughEvent.isInitialized) homeWalkThroughEvent = SingleLiveEvent()
        return homeWalkThroughEvent
    }

    fun setHomeWalkThrough(id: Int) {
        if (::homeWalkThroughEvent.isInitialized) homeWalkThroughEvent.postValue(id)
    }

    private lateinit var sparesWalkThroughEvent: SingleLiveEvent<Int>
    fun provideSparesWalkThroughEvent(): SingleLiveEvent<Int> {
        if (!::sparesWalkThroughEvent.isInitialized) sparesWalkThroughEvent = SingleLiveEvent()
        return sparesWalkThroughEvent
    }

    fun setSparesWalkThrough(id: Int) {
        if (::sparesWalkThroughEvent.isInitialized) sparesWalkThroughEvent.postValue(id)
    }

    private lateinit var shopWithFilterWalkThroughEvent: SingleLiveEvent<Int>
    fun provideShopWithFilterWalkThroughEvent(): SingleLiveEvent<Int> {
        if (!::shopWithFilterWalkThroughEvent.isInitialized) shopWithFilterWalkThroughEvent =
            SingleLiveEvent()
        return shopWithFilterWalkThroughEvent
    }

    fun setShopWithFilterWalkThrough(id: Int) {
        if (::shopWithFilterWalkThroughEvent.isInitialized) shopWithFilterWalkThroughEvent.postValue(
            id)
    }

    private lateinit var inventoryWalkThroughEvent: SingleLiveEvent<Int>
    fun provideInventoryWalkThroughEvent(): SingleLiveEvent<Int> {
        if (!::inventoryWalkThroughEvent.isInitialized) inventoryWalkThroughEvent =
            SingleLiveEvent()
        return inventoryWalkThroughEvent
    }

    fun setInventoryWalkThrough(id: Int) {
        if (::inventoryWalkThroughEvent.isInitialized) inventoryWalkThroughEvent.postValue(id)
    }

    private lateinit var accountWalkThroughEvent: SingleLiveEvent<Int>
    fun provideAccountWalkThroughEvent(): SingleLiveEvent<Int> {
        if (!::accountWalkThroughEvent.isInitialized) accountWalkThroughEvent = SingleLiveEvent()
        return accountWalkThroughEvent
    }

    fun setAccountWalkThrough(id: Int) {
        if (::accountWalkThroughEvent.isInitialized) accountWalkThroughEvent.postValue(id)
    }

    private lateinit var settingsWalkThroughEvent: SingleLiveEvent<Int>
    fun provideSettingsWalkThroughEvent(): SingleLiveEvent<Int> {
        if (!::settingsWalkThroughEvent.isInitialized) settingsWalkThroughEvent = SingleLiveEvent()
        return settingsWalkThroughEvent
    }

    fun setSettingsWalkThrough(id: Int) {
        if (::settingsWalkThroughEvent.isInitialized) settingsWalkThroughEvent.postValue(id)
    }

}