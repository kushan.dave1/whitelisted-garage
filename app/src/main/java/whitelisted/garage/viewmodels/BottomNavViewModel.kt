package whitelisted.garage.viewmodels

import android.app.Application
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.FingerprintRequest
import whitelisted.garage.api.request.UpdateLocationRequest
import whitelisted.garage.api.response.GoCoinsValueResponse
import whitelisted.garage.api.response.ScreensConfigResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM
import java.lang.Exception
import java.net.URL
import java.util.concurrent.Executors

class BottomNavViewModel(app: Application, apiRepo: APIRepository) : BaseViewModel(app, apiRepo) {

    private lateinit var goCoinsValueResponse: MutableLiveData<Result<ServerResponse<GoCoinsValueResponse>>>
    fun provideGoCoinsValueResponse(): MutableLiveData<Result<ServerResponse<GoCoinsValueResponse>>> {
        if (!::goCoinsValueResponse.isInitialized) goCoinsValueResponse = MutableLiveData()
        return goCoinsValueResponse
    }

    var screensConfigResponse: MutableLiveData<ScreensConfigResponse> = MutableLiveData()
    private lateinit var configResponse: MutableLiveData<Result<ServerResponse<ScreensConfigResponse>>>
    fun provideConfigResponse(): MutableLiveData<Result<ServerResponse<ScreensConfigResponse>>> {
        if (!::configResponse.isInitialized) configResponse = MutableLiveData()
        return configResponse
    }

    fun sendLocationToServer(request: UpdateLocationRequest) {
        launch {
            apiRepo.updateUserLocation(getStringSharedPreference(AppENUM.USER_ID), request)
                .awaitAndGet() //            updateLocationResponse.postValue(result)
        }
    }

    fun getConfigAPI() {
        viewModelScope.launch {
            val goCoinsValueResult = apiRepo.getGoCoinsValue().awaitAndGet()
            val result = apiRepo.getConfigAPI().awaitAndGet()
            if (!::goCoinsValueResponse.isInitialized) {
                goCoinsValueResponse = MutableLiveData()
            }
            goCoinsValueResponse.postValue(goCoinsValueResult)
            configResponse.postValue(result)
        }
    }

    fun updateDeviceTokenAPI(token: String?) {
        viewModelScope.launch {
            apiRepo.updateDeviceTokenForFCM(FingerprintRequest(token)).awaitAndGet()
        }
    }

}