package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.IdTypeRequest
import whitelisted.garage.api.response.BidDetailResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class PlaceBidFragmentViewModel(app: Application, apiRepo: APIRepository) :
    BaseViewModel(app, apiRepo) {

    private lateinit var placeBidResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun providePlaceBidResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::placeBidResponse.isInitialized) {
            placeBidResponse = MutableLiveData()
        }
        return placeBidResponse
    }

    fun placeBidAPI(bidId: String, bidDetail: BidDetailResponse) {
        viewModelScope.launch {
            val result = apiRepo.placeBid(bidId, bidDetail).awaitAndGet()
            placeBidResponse.postValue(result)
        }
    }

    private lateinit var bidDetailResponse: MutableLiveData<Result<ServerResponse<BidDetailResponse>>>
    fun provideGetBidDetailResponse(): MutableLiveData<Result<ServerResponse<BidDetailResponse>>> {
        if (!::bidDetailResponse.isInitialized) {
            bidDetailResponse = MutableLiveData()
        }
        return bidDetailResponse
    }

    fun getBidDetails(orderId: String) {
        viewModelScope.launch {
            val result = apiRepo.getBidDetails(orderId).awaitAndGet()
            bidDetailResponse.postValue(result)
        }
    }

    private lateinit var cancelBidResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideCancelBidResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::cancelBidResponse.isInitialized) {
            cancelBidResponse = MutableLiveData()
        }
        return cancelBidResponse
    }

    fun cancelBid(bidId: String) {
        viewModelScope.launch {
            val result = apiRepo.cancelBidRetailer(bidId).awaitAndGet()
            cancelBidResponse.postValue(result)
        }
    }

    private lateinit var unlockContactResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideUnlockContactResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::unlockContactResponse.isInitialized) {
            unlockContactResponse = MutableLiveData()
        }
        return unlockContactResponse
    }

    fun unlockContact(bidId: String) {
        val req = IdTypeRequest(id = bidId, type = "phone")
        viewModelScope.launch {
            val result = apiRepo.unlockContactRetailer(bidId, req).awaitAndGet()
            unlockContactResponse.postValue(result)
        }
    }


}