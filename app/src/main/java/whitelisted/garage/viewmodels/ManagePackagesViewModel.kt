package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.AllPackagesResponseModel
import whitelisted.garage.api.response.PackageIdResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class ManagePackagesViewModel(application: Application, apiRepository: APIRepository) :
    BaseViewModel(application, apiRepository) {

    private lateinit var customPackagesListResponse: MutableLiveData<Result<ServerResponse<List<AllPackagesResponseModel>>>>
    fun provideCustomPackagesListResponse(): MutableLiveData<Result<ServerResponse<List<AllPackagesResponseModel>>>> {
        if (!::customPackagesListResponse.isInitialized) {
            customPackagesListResponse = MutableLiveData()
        }
        return customPackagesListResponse
    }

    private lateinit var deletePackageResponse: MutableLiveData<Result<ServerResponse<PackageIdResponse>>>
    fun provideDeletePackageResponse(): MutableLiveData<Result<ServerResponse<PackageIdResponse>>> {
        if (!::deletePackageResponse.isInitialized) {
            deletePackageResponse = MutableLiveData()
        }
        return deletePackageResponse
    }

    fun getCustomPackagesList() {
        viewModelScope.launch {
            val result = apiRepo.getCustomPackagesList().awaitAndGet()
            customPackagesListResponse.postValue(result)
        }
    }

    fun deletePackage(packageId: String?, isTwoWheeler:Boolean) {
        viewModelScope.launch {
            val result = if (isTwoWheeler)apiRepo.deletePackageBike(packageId.toString()).awaitAndGet() else apiRepo.deletePackage(packageId.toString()).awaitAndGet()
            deletePackageResponse.postValue(result)
        }
    }

    suspend fun getCustomPackagesListBike(): List<AllPackagesResponseModel>? =
        withContext(Dispatchers.IO) {
            return@withContext when (val result =
                apiRepo.getCustomPackagesListBike().awaitAndGet()) {
                is Result.Success -> {
                    result.body.data
                }
                else -> {
                    null
                }
            }
        }


}