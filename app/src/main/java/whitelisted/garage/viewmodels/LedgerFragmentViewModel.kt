package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.LedgerIdRequest
import whitelisted.garage.api.response.LedgerResponseModel
import whitelisted.garage.api.response.MsgKeyResponse
import whitelisted.garage.api.response.OrderIdResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class LedgerFragmentViewModel(app: Application, apiRepo: APIRepository) :
    BaseViewModel(app, apiRepo) {

    fun getLedger(type: String?, transactionType: String?) {
        viewModelScope.launch {
            val result = apiRepo.getLedger(type, transactionType).awaitAndGet()
            ledgerResponseModel.postValue(result)
        }
    }

    private lateinit var ledgerResponseModel: MutableLiveData<Result<ServerResponse<LedgerResponseModel>>>
    fun provideLedgerResponse(): MutableLiveData<Result<ServerResponse<LedgerResponseModel>>> {
        if (!::ledgerResponseModel.isInitialized) {
            ledgerResponseModel = MutableLiveData()
        }
        return ledgerResponseModel
    }

    private lateinit var downloadCSVResponse: MutableLiveData<ResponseBody>
    fun provideDownloadCSVResponse(): MutableLiveData<ResponseBody> {
        if (!::downloadCSVResponse.isInitialized) {
            downloadCSVResponse = MutableLiveData()
        }
        return downloadCSVResponse
    }

    fun getDownloadCSV(type: String) {
        viewModelScope.launch {
            when (val result = apiRepo.downloadLedgerCSV(type).awaitAndGet()) {
                is Result.Success -> {
                    downloadCSVResponse.postValue(result.body)
                }
                is Result.Failure -> {

                }
            }
        }
    }

    private lateinit var getReminderMsg: MutableLiveData<Result<ServerResponse<MsgKeyResponse>>>
    fun provideReminderMsgResponse(): MutableLiveData<Result<ServerResponse<MsgKeyResponse>>> {
        if (!::getReminderMsg.isInitialized) {
            getReminderMsg = MutableLiveData()
        }
        return getReminderMsg
    }

    fun getReminderSMSAPI(req: OrderIdResponse) {
        viewModelScope.launch {
            val result = apiRepo.sendDuesReminder(req).awaitAndGet()
            getReminderMsg.postValue(result)
        }
    }

    private lateinit var collectDuesResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideCollectDuesResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::collectDuesResponse.isInitialized) {
            collectDuesResponse = MutableLiveData()
        }
        return collectDuesResponse
    }

    fun collectDueAPI(req: LedgerIdRequest) {
        viewModelScope.launch {
            val result = apiRepo.collectDuesAPI(req).awaitAndGet()
            collectDuesResponse.postValue(result)
        }
    }

}