package whitelisted.garage.viewmodels

import DateUtil
import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.*
import whitelisted.garage.api.response.*
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.SingleLiveEvent

class WorkShopBiddingViewModel(val app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    var brandWiseItems: MutableMap<String, MutableList<BrandWiseItem>> = mutableMapOf()
    var genericItems: MutableList<GenericItem> = mutableListOf()
    private lateinit var getDateRange: SingleLiveEvent<ArrayList<String>>
    private lateinit var getCreateWorkshopBidCartResponse: SingleLiveEvent<Result<ServerResponse<JsonElement>>>
    private lateinit var getOnGoingBidList: SingleLiveEvent<Result<ServerResponse<List<GetWsOngoingBidResponse>>>>
    private lateinit var getHistoryBidList: SingleLiveEvent<Result<ServerResponse<List<GetWsOngoingBidResponse>>>>
    private lateinit var getWSMostUsedPart: SingleLiveEvent<Result<ServerResponse<GetWsMostUsedPart>>>
    private lateinit var getWSBidReceived: SingleLiveEvent<Result<ServerResponse<GetWsBidReceivedResponse>>>
    private lateinit var markCompleteWsBidResponse: SingleLiveEvent<Result<ServerResponse<JsonElement>>>
    private lateinit var getWsBidEnquiryResponse: SingleLiveEvent<Result<ServerResponse<GetWsBidEnquiryResponse>>>
    private lateinit var getChangesBidStatusResponse: SingleLiveEvent<Result<ServerResponse<JsonElement>>>
    private lateinit var isMediaUploadedSuccessFully: SingleLiveEvent<Boolean>
    private lateinit var carBrand: SingleLiveEvent<BrandWiseItem>
    private lateinit var isMediaDataFetched: SingleLiveEvent<Boolean>
    private lateinit var refreshReceivedBidList: SingleLiveEvent<Boolean>
    private lateinit var cancelWsBidResponse: SingleLiveEvent<Result<ServerResponse<JsonElement>>>
    private lateinit var unlockContactResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideUnlockContactResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::unlockContactResponse.isInitialized) {
            unlockContactResponse = MutableLiveData()
        }
        return unlockContactResponse
    }

    fun unlockPhoneWorkshop(bidId: String) {
        val req = IdTypeRequest(id = bidId, type = "phone")
        viewModelScope.launch {
            val result = apiRepo.unlockPhoneWorkshop(bidId, req).awaitAndGet()
            unlockContactResponse.postValue(result)
        }
    }

    fun provideCancelWsBid(): SingleLiveEvent<Result<ServerResponse<JsonElement>>> {
        if (!::cancelWsBidResponse.isInitialized) {
            cancelWsBidResponse = SingleLiveEvent()
        }
        return cancelWsBidResponse
    }

    fun cancelWorkshopBid(id: String) {
        viewModelScope.launch {
            val result = apiRepo.cancelWorkshopBid(id).awaitAndGet()
            cancelWsBidResponse.postValue(result)
        }
    }


    fun setRefreshReceivedBidList(value: Boolean) {
        refreshReceivedBidList.value = value
    }

    fun provideRefreshReceivedBidList(): SingleLiveEvent<Boolean> {
        if (!::refreshReceivedBidList.isInitialized) refreshReceivedBidList = SingleLiveEvent()
        return refreshReceivedBidList
    }

    fun setIsMediaDataFetched(value: Boolean) {
        isMediaDataFetched.value = value
    }

    fun provideIsMediaDataFetched(): SingleLiveEvent<Boolean> {
        if (!::isMediaDataFetched.isInitialized) isMediaDataFetched = SingleLiveEvent()
        return isMediaDataFetched
    }

    fun setIsMediaUploadedSuccess(value: Boolean) {
        isMediaUploadedSuccessFully.value = value
    }

    fun provideIsMediaUploadedSuccessFully(): SingleLiveEvent<Boolean> {
        if (!::isMediaUploadedSuccessFully.isInitialized) isMediaUploadedSuccessFully =
            SingleLiveEvent()
        return isMediaUploadedSuccessFully
    }

    fun provideChangeBidStatusResponse(): SingleLiveEvent<Result<ServerResponse<JsonElement>>> {
        if (!::getChangesBidStatusResponse.isInitialized) {
            getChangesBidStatusResponse = SingleLiveEvent()
        }
        return getChangesBidStatusResponse
    }

    fun changeStatusOfWsBid(id: String, bidStatus: WsAccRejBid) {
        viewModelScope.launch {
            val result = apiRepo.changeStatusOfWsBid(id, bidStatus).awaitAndGet()
            getChangesBidStatusResponse.postValue(result)
        }
    }

    fun provideWsBidEnquiry(): SingleLiveEvent<Result<ServerResponse<GetWsBidEnquiryResponse>>> {
        if (!::getWsBidEnquiryResponse.isInitialized) {
            getWsBidEnquiryResponse = SingleLiveEvent()
        }
        return getWsBidEnquiryResponse
    }

    fun getWsBidEnquiry(id: String) {
        viewModelScope.launch {
            val result = apiRepo.getWsBidEnquiry(id).awaitAndGet()
            getWsBidEnquiryResponse.postValue(result)
        }
    }

    fun provideMarkCompleteWsBid(): SingleLiveEvent<Result<ServerResponse<JsonElement>>> {
        if (!::markCompleteWsBidResponse.isInitialized) {
            markCompleteWsBidResponse = SingleLiveEvent()
        }
        return markCompleteWsBidResponse
    }

    fun markCompleteWsBid(markCompleteWsBidRequest: MarkCompleteWsBidRequest) {
        viewModelScope.launch {
            val result = apiRepo.markCompleteWsBid(markCompleteWsBidRequest).awaitAndGet()
            markCompleteWsBidResponse.postValue(result)
        }
    }

    fun provideGetWsBidReceived(): SingleLiveEvent<Result<ServerResponse<GetWsBidReceivedResponse>>> {
        if (!::getWSBidReceived.isInitialized) {
            getWSBidReceived = SingleLiveEvent()
        }
        return getWSBidReceived
    }

    fun getWsReceivedBids(id: String) {
        viewModelScope.launch {
            val result = apiRepo.getWsReceivedBids(id).awaitAndGet()
            getWSBidReceived.postValue(result)
        }
    }

    fun provideWSMostUsedPart(): SingleLiveEvent<Result<ServerResponse<GetWsMostUsedPart>>> {
        if (!::getWSMostUsedPart.isInitialized) {
            getWSMostUsedPart = SingleLiveEvent()
        }
        return getWSMostUsedPart
    }

    fun getWsMostPart() {
        viewModelScope.launch {
            val result = apiRepo.getWsMostPart().awaitAndGet()
            getWSMostUsedPart.postValue(result)
        }
    }

    fun provideHistoryBidList(): SingleLiveEvent<Result<ServerResponse<List<GetWsOngoingBidResponse>>>> {
        if (!::getHistoryBidList.isInitialized) {
            getHistoryBidList = SingleLiveEvent()
        }
        return getHistoryBidList
    }

    fun getWsHistoryBiddingList() {
        viewModelScope.launch {
            val result = apiRepo.getWsHistoryBiddingList().awaitAndGet()
            getHistoryBidList.postValue(result)
        }
    }

    fun provideOnGoingBidList(): SingleLiveEvent<Result<ServerResponse<List<GetWsOngoingBidResponse>>>> {
        if (!::getOnGoingBidList.isInitialized) {
            getOnGoingBidList = SingleLiveEvent()
        }
        return getOnGoingBidList
    }

    fun getOnGoingWsBiddingList(type: String) {
        viewModelScope.launch {
            val result = apiRepo.getOnGoingWsBiddingList(type).awaitAndGet()
            getOnGoingBidList.postValue(result)
        }
    }


    fun provideCreateWorkshopBidCartResponse(): SingleLiveEvent<Result<ServerResponse<JsonElement>>> {
        if (!::getCreateWorkshopBidCartResponse.isInitialized) {
            getCreateWorkshopBidCartResponse = SingleLiveEvent()
        }
        return getCreateWorkshopBidCartResponse
    }

    fun createWorkshopBiddingAsync(request: WorkShopBiddingRequest) {
        viewModelScope.launch {
            val result = apiRepo.createWorkshopBiddingAsync(request).awaitAndGet()
            getCreateWorkshopBidCartResponse.postValue(result)
        }
    }


    fun provideDateYearRange(): SingleLiveEvent<ArrayList<String>> {
        if (!::getDateRange.isInitialized) getDateRange = SingleLiveEvent()
        return getDateRange
    }


    fun getDateYearRange() {
        viewModelScope.launch {
            getDateRange.postValue(DateUtil.giveDateRange())
        }
    }

    private lateinit var getPartItemResponse: MutableLiveData<Result<ServerResponse<List<GetInventoryPartItemResponse>>>>
    fun providePartItemResponse(): MutableLiveData<Result<ServerResponse<List<GetInventoryPartItemResponse>>>> {
        if (!::getPartItemResponse.isInitialized) {
            getPartItemResponse = MutableLiveData()
        }
        return getPartItemResponse
    }

    fun getInventoryPartItem(type: String) {
        viewModelScope.launch {
            val result = apiRepo.getRetailersCatalogue(type).awaitAndGet()
            getPartItemResponse.postValue(result)
        }
    }

    fun getAllBrandBidsList(data: MutableMap<String, MutableList<BrandWiseItem>>): MutableList<BrandWiseItem> {
        val list = mutableListOf<BrandWiseItem>()
        data.forEach {
            list.addAll(it.value)
        }
        return list
    }

    fun getAllMapOfBrandBidsList(data: MutableList<BrandWiseItem>): MutableMap<String, MutableList<BrandWiseItem>> {
        val list = mutableMapOf<String, MutableList<BrandWiseItem>>()
        data.forEach {
            list[it.brandName ?: ""] = mutableListOf()
        }
        data.forEach {
            if (list.contains(it.brandName)) {
                val dataList = list[it.brandName]
                dataList?.add(it)
                list[it.brandName ?: ""] = dataList ?: mutableListOf()
            }
        }

        return list
    }

    fun getCompleteCartSize(brand: MutableMap<String, MutableList<BrandWiseItem>>,
        generic: MutableList<GenericItem>): Int {
        return (getAllBrandBidsList(brand).size + generic.size)
    }

    fun setCarBrand(value: BrandWiseItem) {
        carBrand.value = value
    }

    fun provideCarBrand(): SingleLiveEvent<BrandWiseItem> {
        if (!::carBrand.isInitialized) carBrand = SingleLiveEvent()
        return carBrand
    }


    fun getAllBrandWise(): ArrayList<BrandWiseItem> {
        val data = ArrayList<BrandWiseItem>()
        for ((key, value) in brandWiseItems) {
            data.addAll(value)
        }
        return data
    }

    fun getAllGenericWise(): ArrayList<GenericItem> {
        val data = ArrayList<GenericItem>()
        for (value in genericItems) {
            data.add(value)
        }
        return data
    }

    private lateinit var genericIItemRemoved: SingleLiveEvent<GenericItem>
    fun setGenericItemRemovedFromCart(value: GenericItem) {
        genericIItemRemoved.value = value
    }

    fun provideGenericItemRemovedFromCart(): SingleLiveEvent<GenericItem> {
        if (!::genericIItemRemoved.isInitialized) genericIItemRemoved = SingleLiveEvent()
        return genericIItemRemoved
    }
}