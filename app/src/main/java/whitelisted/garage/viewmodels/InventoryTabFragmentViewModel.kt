package whitelisted.garage.viewmodels

import android.app.Application
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.gson.JsonElement
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import whitelisted.garage.BuildConfig
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.data.SendSuccessUrl
import whitelisted.garage.api.request.CreateInventoryRackRequest
import whitelisted.garage.api.response.GetInventoryRackResponse
import whitelisted.garage.api.response.RackIdResponse
import whitelisted.garage.api.response.SearchImageResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM
import java.io.File
import java.util.*

class InventoryTabFragmentViewModel(
    val app: Application,
    apiRepository: APIRepository,
) : BaseViewModel(app, apiRepository) {
    private lateinit var downloadCSVResponse: MutableLiveData<ResponseBody>
    private lateinit var getCreateInventoryRackResponse: MutableLiveData<Result<ServerResponse<RackIdResponse>>>
    private lateinit var getUpdateInventoryRackDetailsResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    private lateinit var getDeleteInventoryRackResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    private lateinit var getInventoryRackResponse: MutableLiveData<Result<ServerResponse<MutableList<GetInventoryRackResponse>>>>
    private lateinit var searchImageResponse: MutableLiveData<Result<ServerResponse<List<SearchImageResponse>>>>
    lateinit var storageRef: StorageReference
    lateinit var childRef: StorageReference

    suspend fun downloadSampleCSV(): ResponseBody? {
        return when(val result = apiRepo.downloadSampleCSV().awaitAndGet()){
            is Result.Success ->  result.body
            else -> null
        }
    }

    suspend fun uploadCSVFile(file: MultipartBody.Part): Boolean {
        return when(apiRepo.uploadInventoryCSVFile(file).awaitAndGet()){
            is Result.Success -> {
                getInventoryRack()
                true
            }
            else -> false
        }
    }

    fun provideDownloadCSVResponse(): MutableLiveData<ResponseBody> {
        if (!::downloadCSVResponse.isInitialized) {
            downloadCSVResponse = MutableLiveData()
        }
        return downloadCSVResponse
    }

    fun downloadInventoryCSV() {
        viewModelScope.launch {
            val result = apiRepo.downloadInventoryCSV().awaitAndGet()
            when (result) {
                is Result.Success -> {
                    downloadCSVResponse.postValue(result.body)
                }
                is Result.Failure -> {

                }
            }
        }
    }

    fun provideCreateInventoryResponse(): MutableLiveData<Result<ServerResponse<RackIdResponse>>> {
        if (!::getCreateInventoryRackResponse.isInitialized) {
            getCreateInventoryRackResponse = MutableLiveData()
        }
        return getCreateInventoryRackResponse
    }


    fun createInventoryRack(type: CreateInventoryRackRequest) {
        viewModelScope.launch {
            val result = apiRepo.createInventoryRack(type).awaitAndGet()
            getCreateInventoryRackResponse.postValue(result)
        }
    }

    fun provideUpdateInventoryDetailsResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::getUpdateInventoryRackDetailsResponse.isInitialized) {
            getUpdateInventoryRackDetailsResponse = MutableLiveData()
        }
        return getUpdateInventoryRackDetailsResponse
    }


    fun updateInventoryRackDetails(id: String, type: CreateInventoryRackRequest) {
        viewModelScope.launch {
            val result = apiRepo.updateInventoryRackDetails(id, 1, type).awaitAndGet()
            getUpdateInventoryRackDetailsResponse.postValue(result)
        }
    }

    fun deleteInventoryRack(type: String?) {
        viewModelScope.launch {
            val result = apiRepo.deleteInventoryRack(type ?: "").awaitAndGet()
            getDeleteInventoryRackResponse.postValue(result)
        }
    }

    fun provideDeleteInventoryResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::getDeleteInventoryRackResponse.isInitialized) {
            getDeleteInventoryRackResponse = MutableLiveData()
        }
        return getDeleteInventoryRackResponse
    }

    fun provideInventoryRackResponse(): MutableLiveData<Result<ServerResponse<MutableList<GetInventoryRackResponse>>>> {
        if (!::getInventoryRackResponse.isInitialized) {
            getInventoryRackResponse = MutableLiveData()
        }
        return getInventoryRackResponse
    }


    fun getInventoryRack() {
        viewModelScope.launch {
            val result = apiRepo.getInventoryRack().awaitAndGet()
            getInventoryRackResponse.postValue(result)
        }
    }

    fun provideSearchImageResponse(): MutableLiveData<Result<ServerResponse<List<SearchImageResponse>>>> {
        if (!::searchImageResponse.isInitialized) {
            searchImageResponse = MutableLiveData()
        }
        return searchImageResponse
    }

    fun searchImages(q: String) {
        viewModelScope.launch {
            val result = apiRepo.searchImages(q).awaitAndGet()
            searchImageResponse.postValue(result)
        }
    }

    private lateinit var uploadInventoryImageResponse: MutableLiveData<SendSuccessUrl>

    fun provideUploadInventoryImageResponse(): MutableLiveData<SendSuccessUrl> {
        if (!::uploadInventoryImageResponse.isInitialized) uploadInventoryImageResponse =
            MutableLiveData()
        return uploadInventoryImageResponse
    }


    fun uploadInventoryPic(profilePicture: String?, id: String) {

        val isSuccessFailure = SendSuccessUrl()
        viewModelScope.launch {
            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
            val fileNameRand: String = (Calendar.getInstance().timeInMillis.toString())
            childRef = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
                .child(getStringSharedPreference(AppENUM.USER_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                .child(getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_INVENTORY_RACKS).child("$id/")
                .child(fileNameRand)
            val file = Uri.fromFile(File(profilePicture ?: ""))

            val uploadTask =
                childRef.putFile(file) // Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener {
                isSuccessFailure.isSuccessFailure = false
                isSuccessFailure.url = ""
                uploadInventoryImageResponse.postValue(isSuccessFailure) // Handle unsuccessful uploads
            }.addOnSuccessListener {
                childRef.downloadUrl.addOnSuccessListener {
                    isSuccessFailure.isSuccessFailure = true
                    isSuccessFailure.url = it.toString()
                    uploadInventoryImageResponse.postValue(isSuccessFailure)
                }
            }.addOnFailureListener {
                isSuccessFailure.isSuccessFailure = false
                uploadInventoryImageResponse.postValue(isSuccessFailure)

            }.addOnCanceledListener {
                isSuccessFailure.isSuccessFailure = false
                uploadInventoryImageResponse.postValue(isSuccessFailure)
            }
        }
    }

    fun deleteInventoryFromFirebase(id: String) {
        viewModelScope.launch {
            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
            childRef = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
                .child(getStringSharedPreference(AppENUM.USER_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                .child(getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_INVENTORY_RACKS).child("$id/")
            val uploadTask =
                childRef.delete() // Register observers to listen for when the download is done or if it fails
            uploadTask.addOnSuccessListener {

            }.addOnFailureListener {

            // Handle unsuccessful uploads
            }.addOnCanceledListener {

            }
        }
    }
}