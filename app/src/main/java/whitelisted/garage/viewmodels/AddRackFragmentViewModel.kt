package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.CreateInventoryRackRequest
import whitelisted.garage.api.request.RackItem
import whitelisted.garage.api.response.GetInventoryRackResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class AddRackFragmentViewModel(
    val app: Application,
    apiRepository: APIRepository,

    ) : BaseViewModel(app, apiRepository) {

    private lateinit var getInventory: MutableLiveData<Result<ServerResponse<GetInventoryRackResponse>>>
    private lateinit var listOfRacksItems: MutableList<RackItem>

    fun provideListOfRacksItems(): MutableList<RackItem> {
        return if (::listOfRacksItems.isInitialized) listOfRacksItems
        else {
            listOfRacksItems = mutableListOf()
            listOfRacksItems
        }
    }

    fun setListOfRackItem(list: MutableList<RackItem>) {
        listOfRacksItems = list
    }

    fun provideIndividualInventoryRackResponse(): MutableLiveData<Result<ServerResponse<GetInventoryRackResponse>>> {
        if (!::getInventory.isInitialized) {
            getInventory = MutableLiveData()
        }
        return getInventory
    }

    fun getIndividualInventoryRack(id: String) {
        viewModelScope.launch {
            val result = apiRepo.getIndividualInventoryRack(id).awaitAndGet()
            getInventory.postValue(result)
        }
    }

    private lateinit var getUpdateInventoryRackResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>

    fun provideUpdateInventoryResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::getUpdateInventoryRackResponse.isInitialized) {
            getUpdateInventoryRackResponse = MutableLiveData()
        }
        return getUpdateInventoryRackResponse
    }

    fun updateInventoryRack(id: String, type: CreateInventoryRackRequest) {
        viewModelScope.launch {
            val result = apiRepo.updateInventoryRack(id, type).awaitAndGet()
            getUpdateInventoryRackResponse.postValue(result)
        }
    }

}