package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.IdTypeRequest
import whitelisted.garage.api.response.AccRetailBidListResponseModel
import whitelisted.garage.api.response.AccRetailBidListResponseModelSubmitAccept
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.SingleLiveEvent

class EnquiryOrderRequestsFragmentVIewModel(app: Application, apiRepo: APIRepository) :
    BaseViewModel(app, apiRepo) {

    private lateinit var bidsListResponse: MutableLiveData<Result<ServerResponse<MutableList<AccRetailBidListResponseModel>>>>
    fun provideBidsListResponse(): MutableLiveData<Result<ServerResponse<MutableList<AccRetailBidListResponseModel>>>> {
        if (!::bidsListResponse.isInitialized) {
            bidsListResponse = MutableLiveData()
        }
        return bidsListResponse
    }

    fun getNewBidsList() {
        viewModelScope.launch {
            val result = apiRepo.getNewBidsList().awaitAndGet()
            bidsListResponse.postValue(result)
        }
    }

    private lateinit var submittedBidsListResponse: MutableLiveData<Result<ServerResponse<MutableList<AccRetailBidListResponseModelSubmitAccept>>>>
    fun provideSubmittedBidsListResponse(): MutableLiveData<Result<ServerResponse<MutableList<AccRetailBidListResponseModelSubmitAccept>>>> {
        if (!::submittedBidsListResponse.isInitialized) {
            submittedBidsListResponse = MutableLiveData()
        }
        return submittedBidsListResponse
    }

    fun getSubmittedBidsList() {
        viewModelScope.launch {
            val result = apiRepo.getSubmittedBidsList().awaitAndGet()
            submittedBidsListResponse.postValue(result)
        }
    }

    private lateinit var acceptedBidsListResponse: MutableLiveData<Result<ServerResponse<MutableList<AccRetailBidListResponseModelSubmitAccept>>>>
    fun provideAcceptedBidsListResponse(): MutableLiveData<Result<ServerResponse<MutableList<AccRetailBidListResponseModelSubmitAccept>>>> {
        if (!::acceptedBidsListResponse.isInitialized) {
            acceptedBidsListResponse = MutableLiveData()
        }
        return acceptedBidsListResponse
    }

    fun getAcceptedBidsList() {
        viewModelScope.launch {
            val result = apiRepo.getAcceptedBidsList().awaitAndGet()
            acceptedBidsListResponse.postValue(result)
        }
    }

    private lateinit var isUpdateList: SingleLiveEvent<Boolean>
    fun provideIsUpdateList(): SingleLiveEvent<Boolean> {
        if (!::isUpdateList.isInitialized) isUpdateList = SingleLiveEvent()
        return isUpdateList
    }

    private lateinit var unlockContactResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideUnlockContactResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::unlockContactResponse.isInitialized) {
            unlockContactResponse = MutableLiveData()
        }
        return unlockContactResponse
    }

    fun unlockContact(bidId: String) {
        val req = IdTypeRequest(id = bidId, type = "phone")
        viewModelScope.launch {
            val result = apiRepo.unlockContactRetailer(bidId, req).awaitAndGet()
            unlockContactResponse.postValue(result)
        }
    }

    private lateinit var markCompleteResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideMarkCompleteResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::markCompleteResponse.isInitialized) {
            markCompleteResponse = MutableLiveData()
        }
        return markCompleteResponse
    }

    fun markComplete(bidId: String) {
        val req = IdTypeRequest(id = bidId, type = "completed")
        viewModelScope.launch {
            val result = apiRepo.markCompleteBidRetailer(bidId, req).awaitAndGet()
            markCompleteResponse.postValue(result)
        }
    }


}