package whitelisted.garage.viewmodels

import android.app.Application
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.coroutines.launch
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.data.SendSuccessUrl
import whitelisted.garage.api.request.AddEditEmployee
import whitelisted.garage.api.response.login.LoginUserResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*

class AddEditEmployeeViewModel(app: Application, apiRepo: APIRepository) :
    BaseViewModel(app, apiRepo) {

    lateinit var storageRef: StorageReference
    lateinit var workshopImageRef: StorageReference


    fun addEmployee(workshopId: String, addEditEmployee: AddEditEmployee) {
        viewModelScope.launch {
            val result = apiRepo.addEmployee(workshopId, addEditEmployee).awaitAndGet()
            addEmployeeLiveData.postValue(result)
        }
    }

    fun editEmployee(addEditEmployee: AddEditEmployee, editEmployeeId: String?) {
        viewModelScope.launch {
            val result = apiRepo.editEmployee(addEditEmployee, editEmployeeId).awaitAndGet()
            addEmployeeLiveData.postValue(result)
        }
    }

    private lateinit var uploadEmployeeImageResponse: MutableLiveData<SendSuccessUrl>
    fun provideUploadEmployeeImageResponse(): MutableLiveData<SendSuccessUrl> {
        if (!::uploadEmployeeImageResponse.isInitialized) uploadEmployeeImageResponse =
            MutableLiveData()
        return uploadEmployeeImageResponse
    }

    fun uploadProfilePicture(context : Context, mobileNumber: String, profilePicture: Uri) {
        val isSuccessFailure = SendSuccessUrl()
        viewModelScope.launch {
            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
            workshopImageRef = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
                .child(getStringSharedPreference(AppENUM.USER_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS)
                .child(getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_EMPLOYEES).child(mobileNumber)

            val emptyBitmap = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(emptyBitmap)
            canvas.drawColor(ContextCompat.getColor(context, R.color.white))
            var bmp: Bitmap = try {
                if (Build.VERSION.SDK_INT < 28) {
                    MediaStore.Images.Media.getBitmap(context.contentResolver, profilePicture)
                        ?: emptyBitmap
                } else {
                    ImageDecoder.createSource(context.contentResolver, profilePicture).let {
                        ImageDecoder.decodeBitmap(it)
                    }
                }

            } catch (e: Exception) {
                emptyBitmap
            }
            val byteArrayOutputStream = ByteArrayOutputStream()
            bmp = CommonUtils.scaleBitmap(bmp)
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            val data = byteArrayOutputStream.toByteArray()

            val uploadTask =
                workshopImageRef.putBytes(data) // Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener {
                isSuccessFailure.isSuccessFailure = false
                isSuccessFailure.url = ""
                uploadEmployeeImageResponse.postValue(isSuccessFailure) // Handle unsuccessful uploads
            }.addOnSuccessListener { taskSnapshot ->
                workshopImageRef.path.let {
                    isSuccessFailure.isSuccessFailure = true
                    isSuccessFailure.url = it
                    uploadEmployeeImageResponse.postValue(isSuccessFailure)
                }
            }.addOnCompleteListener {
                if (it.isSuccessful) {
                    val downloadUri = it.result
                }
            }
        }
    }


    private lateinit var addEmployeeLiveData: MutableLiveData<Result<ServerResponse<LoginUserResponse>>>
    fun provideAddEmployeeObserver(): MutableLiveData<Result<ServerResponse<LoginUserResponse>>> {
        if (!::addEmployeeLiveData.isInitialized) {
            addEmployeeLiveData = MutableLiveData()
        }
        return addEmployeeLiveData
    }
}