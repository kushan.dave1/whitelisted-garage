package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.AddressResponseModel
import whitelisted.garage.api.response.EstimatedDeliveryResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class SelectAddressViewModel(app:Application, apiRepo:APIRepository) : BaseViewModel(app, apiRepo) {

    private lateinit var getAddressesResponse: MutableLiveData<Result<ServerResponse<MutableList<AddressResponseModel>>>>
    fun provideAddressesResponse(): MutableLiveData<Result<ServerResponse<MutableList<AddressResponseModel>>>> {
        if (!::getAddressesResponse.isInitialized) getAddressesResponse = MutableLiveData()
        return getAddressesResponse
    }

    suspend fun getAddresses(): List<AddressResponseModel> {
        val result = apiRepo.getAllAddress().awaitAndGet()
        return if(result is Result.Success) result.body.data else listOf()
    }

    fun getAllAddresses() {
        viewModelScope.launch {
            val result = apiRepo.getAllAddress().awaitAndGet()
            getAddressesResponse.postValue(result)
        }
    }

    private lateinit var deleteAddressResponse: MutableLiveData<Result<ServerResponse<Any>>>
    fun provideDeleteAddressResponse(): MutableLiveData<Result<ServerResponse<Any>>> {
        if (!::deleteAddressResponse.isInitialized) deleteAddressResponse = MutableLiveData()
        return deleteAddressResponse
    }
    fun deleteAddress(addressId: String) {
        viewModelScope.launch {
            val result = apiRepo.deleteShippingAddress(addressId).awaitAndGet()
            deleteAddressResponse.postValue(result)
        }
    }

    private lateinit var addAddressResponse: MutableLiveData<Result<ServerResponse<Any>>>
    fun provideAddAddressResponse(): MutableLiveData<Result<ServerResponse<Any>>> {
        if (!::addAddressResponse.isInitialized) addAddressResponse = MutableLiveData()
        return addAddressResponse
    }

    fun addAddressAPI(addressResponseModel: AddressResponseModel) {
        viewModelScope.launch {
            val result = apiRepo.addAddressAPI(addressResponseModel).awaitAndGet()
            addAddressResponse.postValue(result)
        }
    }

    fun editAddressAPI(addressResponseModel: AddressResponseModel) {
        viewModelScope.launch {
            val result = apiRepo.editAddressAPI(addressResponseModel).awaitAndGet()
            addAddressResponse.postValue(result)
        }
    }

    suspend fun getEstimatedDelivery(pin: String): EstimatedDeliveryResponse? = withContext(Dispatchers.IO) {
        return@withContext when (val result =
            apiRepo.getEstimatedDelivery(pin).awaitAndGet()) {
            is Result.Success -> result.body.data
            else -> null
        }
    }

}