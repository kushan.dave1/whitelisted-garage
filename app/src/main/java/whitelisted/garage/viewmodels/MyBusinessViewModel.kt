package whitelisted.garage.viewmodels

import android.app.Application
import android.net.Uri
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.*
import whitelisted.garage.api.response.GetGMDCategoriesResponse
import whitelisted.garage.api.response.GetGMDResponse
import whitelisted.garage.api.response.GetGOCoinOptionsResponse
import whitelisted.garage.api.response.GetWebsiteResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.database.repository.RoomRepository
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.SingleLiveEvent

class MyBusinessViewModel(application: Application,
    apiRepository: APIRepository,
    private val roomRepository: RoomRepository) : BaseViewModel(application, apiRepository) {

    private lateinit var getCategories: SingleLiveEvent<Result<ServerResponse<GetGMDCategoriesResponse>>>
    private lateinit var getGoCoinOptions: SingleLiveEvent<Result<ServerResponse<GetGOCoinOptionsResponse>>>
    private lateinit var saveGMBResponse: SingleLiveEvent<Result<ServerResponse<JsonElement>>>
    private lateinit var getGMD: SingleLiveEvent<Result<ServerResponse<GetGMDResponse>>>
    private lateinit var getGMDWebsite: SingleLiveEvent<Result<ServerResponse<GetWebsiteResponse>>>
    private lateinit var getGMDAttributes: SingleLiveEvent<Result<ServerResponse<MutableMap<String, MutableList<String>>>>>
    private lateinit var getPhotoUriFromAdd: SingleLiveEvent<MutableList<Uri>>
    private lateinit var getBusinessPageData: SingleLiveEvent<BusinessPageData>
    private lateinit var getDayTimeObject: SingleLiveEvent<DayTimeData>
    private lateinit var getAttributeData: SingleLiveEvent<AttributeData>
    private lateinit var getCategoryData: SingleLiveEvent<MutableList<String>>
    private lateinit var gmbUnLockedSuccess: SingleLiveEvent<Boolean>
    private lateinit var isMediaUploadedSuccessFully: SingleLiveEvent<Boolean>


    fun setIsMediaUploadedSuccess(value: Boolean) {
        isMediaUploadedSuccessFully.value = value
    }

    fun provideIsMediaUploadedSuccessFully(): SingleLiveEvent<Boolean> {
        if (!::isMediaUploadedSuccessFully.isInitialized) isMediaUploadedSuccessFully =
            SingleLiveEvent()
        return isMediaUploadedSuccessFully
    }

    private lateinit var isMediaDataFetched: SingleLiveEvent<Boolean>
    fun setIsMediaDataFetched(value: Boolean) {
        isMediaDataFetched.value = value
    }

    fun provideIsMediaDataFetched(): SingleLiveEvent<Boolean> {
        if (!::isMediaDataFetched.isInitialized) isMediaDataFetched = SingleLiveEvent()
        return isMediaDataFetched
    }

    fun providePhotoUriFromAdd(): SingleLiveEvent<MutableList<Uri>> {
        if (!::getPhotoUriFromAdd.isInitialized) getPhotoUriFromAdd = SingleLiveEvent()
        return getPhotoUriFromAdd
    }

    fun setPhotoUriFromAdd(value: MutableList<Uri>) {
        getPhotoUriFromAdd.value = value
    }

    fun provideBusinessPageData(): SingleLiveEvent<BusinessPageData> {
        if (!::getBusinessPageData.isInitialized) getBusinessPageData = SingleLiveEvent()
        return getBusinessPageData
    }

    fun setGMBUnlockSuccess(value: Boolean) {
        gmbUnLockedSuccess.value = value
    }

    fun provideGMBUnlockSuccess(): SingleLiveEvent<Boolean> {
        if (!::gmbUnLockedSuccess.isInitialized) gmbUnLockedSuccess = SingleLiveEvent()
        return gmbUnLockedSuccess
    }

    fun setBusinessPageData(value: BusinessPageData) {
        getBusinessPageData.value = value
    }

    fun provideGMDCategories(): SingleLiveEvent<Result<ServerResponse<GetGMDCategoriesResponse>>> {
        if (!::getCategories.isInitialized) getCategories = SingleLiveEvent()
        return getCategories
    }

    fun getGMBCategories() {
        viewModelScope.launch {
            val result = apiRepo.getGMBCategories().awaitAndGet()
            getCategories.postValue(result)
        }
    }

    fun provideGMD(): SingleLiveEvent<Result<ServerResponse<GetGMDResponse>>> {
        if (!::getGMD.isInitialized) getGMD = SingleLiveEvent()
        return getGMD
    }

    fun provideGMDWebsite(): SingleLiveEvent<Result<ServerResponse<GetWebsiteResponse>>> {
        if (!::getGMDWebsite.isInitialized) getGMDWebsite = SingleLiveEvent()
        return getGMDWebsite
    }

    fun getGMBWebsite() {
        viewModelScope.launch {
            val result = apiRepo.getWebsite().awaitAndGet()
            getGMDWebsite.postValue(result)

        }

    }

    fun getGMB() {
        viewModelScope.launch {
            val result = apiRepo.getGMB().awaitAndGet()
            getGMD.postValue(result)
        }
    }

    fun provideGMBAttributes(): SingleLiveEvent<Result<ServerResponse<MutableMap<String, MutableList<String>>>>> {
        if (!::getGMDAttributes.isInitialized) getGMDAttributes = SingleLiveEvent()
        return getGMDAttributes
    }

    fun getGMBAttributes() {
        viewModelScope.launch {
            val result = apiRepo.getGMBAttributes().awaitAndGet()
            getGMDAttributes.postValue(result)

        }
    }

    fun provideSaveGMD(): SingleLiveEvent<Result<ServerResponse<JsonElement>>> {
        if (!::saveGMBResponse.isInitialized) saveGMBResponse = SingleLiveEvent()
        return saveGMBResponse
    }

    fun saveGMB(saveGMDRequest: SaveGMBRequest) {
        viewModelScope.launch {
            val result = apiRepo.saveGMB(saveGMDRequest).awaitAndGet()
            saveGMBResponse.postValue(result)
        }
    }

    fun saveWebsite(saveGMDRequest: SaveWebsiteRequest) {
        viewModelScope.launch {
            val result = apiRepo.saveGMBWebsite(saveGMDRequest).awaitAndGet()
            saveGMBResponse.postValue(result)
        }
    }

    fun provideGetGoCoinOptions(): SingleLiveEvent<Result<ServerResponse<GetGOCoinOptionsResponse>>> {
        if (!::getGoCoinOptions.isInitialized) getGoCoinOptions = SingleLiveEvent()
        return getGoCoinOptions
    }

    fun getGoCoinsOptions() {
        viewModelScope.launch {
            val result = apiRepo.getGoCoinsOptions().awaitAndGet()
            getGoCoinOptions.postValue(result)

        }
    }


    fun provideDayTimeObject(): SingleLiveEvent<DayTimeData> {
        if (!::getDayTimeObject.isInitialized) getDayTimeObject = SingleLiveEvent()
        return getDayTimeObject
    }

    fun setDayTimeData(value: DayTimeData) {
        getDayTimeObject.value = value
    }

    fun provideAttributeData(): SingleLiveEvent<AttributeData> {
        if (!::getAttributeData.isInitialized) getAttributeData = SingleLiveEvent()
        return getAttributeData
    }

    fun setAttributeData(value: AttributeData) {
        getAttributeData.value = value
    }


    fun setCategory(value: MutableList<String>) {
        getCategoryData.value = value
    }


    fun getPrimaryDayTimeData(openTime: MutableMap<String, List<String>>?): DayTimeData {
        val days = AppENUM.DAYS_DATA


        val timeList = mutableListOf<StartAndEndTimeData>()
        if (openTime != null || openTime?.isEmpty() == true) {
            for ((key, value) in openTime) {
                if (days.contains(key)) {
                    timeList.add(StartAndEndTimeData(value[0], value[1], key, true))
                }
            }

            for (values in days) {
                if (!openTime.keys.contains(values)) {
                    timeList.add(StartAndEndTimeData("09:00", "21:00", values, false))
                }
            }

        } else {
            for (values in days) {
                timeList.add(StartAndEndTimeData("09:00", "21:00", values, true))
            }
        }

        return DayTimeData(timeList)
    }

    fun getSubList(attributeData: MutableMap<String, MutableList<String>>?): MutableMap<String, MutableList<String>> {
        val map = mutableMapOf<String, MutableList<String>>()
        attributeData?.let {
            val key = attributeData.keys

            if (key.size > 2) {
                key.elementAt(0).let {
                    map.put(it, attributeData[it] ?: mutableListOf())
                }
                key.elementAt(1).let {
                    map.put(it, attributeData[it] ?: mutableListOf())
                }
            }
        }
        return map
    }

    fun getMapKeyList(attributeData: MutableMap<String, MutableList<String>>?): MutableSet<String> {
        return (attributeData?.keys ?: mutableSetOf())
    }

    fun getEditCategoriesData(adapterData: MutableList<String>?): MutableList<String> {
        val dataToBeInserted = mutableListOf<String>()
        if (adapterData != null) {
            for (values in adapterData) {
                dataToBeInserted.add(values)
            }
        }
        return dataToBeInserted
    }

    fun getMappedDayTimeData(dayTime: DayTimeData): MutableMap<String, List<String>> {
        val openTime = mutableMapOf<String, List<String>>()
        for (value in dayTime.timeList) {
            openTime[value.days] = mutableListOf<String>().apply {
                add(value.startTime)
                add(value.endTime)
            }
        }
        return openTime
    }

    fun getMappedAttributeData(atrData: AttributeData): MutableMap<String, MutableList<String>> {
        val gmbAttributes = mutableMapOf<String, MutableList<String>>()
        for (value in atrData.list) {
            val listOfSubAtr = mutableListOf<String>()
            for (data in value.atrChildList) {
                if (data.isEnable) {
                    listOfSubAtr.add(data.name)
                }
            }
            if (listOfSubAtr.size > 0) {
                gmbAttributes[value.attributeName] = listOfSubAtr
            }
        }
        return gmbAttributes
    }


}