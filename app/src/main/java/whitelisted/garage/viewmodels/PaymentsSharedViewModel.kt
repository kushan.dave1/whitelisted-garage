package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.OEServicesItem
import whitelisted.garage.api.request.OrderInclusionsItem
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.utils.SingleLiveEvent

class PaymentsSharedViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private lateinit var orderEstimateItems: MutableLiveData<MutableList<OEServicesItem>>
    fun provideOrderEstimateItemsResponse(): MutableLiveData<MutableList<OEServicesItem>> {
        if (!::orderEstimateItems.isInitialized) {
            orderEstimateItems = MutableLiveData()
        }
        return orderEstimateItems
    }

    private lateinit var orderInclusionItems: MutableList<OrderInclusionsItem>
    fun provideOrderInclusionItemsResponse(): MutableList<OrderInclusionsItem> {
        if (!::orderInclusionItems.isInitialized) {
            orderInclusionItems = mutableListOf()
        }
        return orderInclusionItems
    }

    fun setOrderInclusionItems(list:MutableList<OrderInclusionsItem>){
        orderInclusionItems = list
    }

}