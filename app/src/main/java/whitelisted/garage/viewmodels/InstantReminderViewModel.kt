package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.SendInstantReminderRequest
import whitelisted.garage.api.response.GetCustomerReminderResponse
import whitelisted.garage.api.response.PreviousReminder
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.database.repository.RoomRepository
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM

class InstantReminderViewModel(val app: Application,
    apiRepository: APIRepository,
    private val roomRepository: RoomRepository) : BaseViewModel(app, apiRepository) {


    private lateinit var collectDueResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideCollectDueResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::collectDueResponse.isInitialized) collectDueResponse = MutableLiveData()
        return collectDueResponse
    }

    fun collectDueOnReminder(request: PreviousReminder) {
        viewModelScope.launch {
            val result = apiRepo.collectDueOnReminder(request).awaitAndGet()
            collectDueResponse.postValue(result)
        }
    }

    private lateinit var sendReminderResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideSendReminderResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::sendReminderResponse.isInitialized) sendReminderResponse = MutableLiveData()
        return sendReminderResponse
    }

    fun sendInstantReminder(request: SendInstantReminderRequest) {
        viewModelScope.launch {
            val result = apiRepo.sendInstantReminder(request).awaitAndGet()
            sendReminderResponse.postValue(result)
        }
    }

    private lateinit var getReminderResponse: MutableLiveData<Result<ServerResponse<GetCustomerReminderResponse>>>

    fun provideGetReminderResponse(): MutableLiveData<Result<ServerResponse<GetCustomerReminderResponse>>> {
        if (!::getReminderResponse.isInitialized) getReminderResponse = MutableLiveData()
        return getReminderResponse
    }

    fun getCustomerReminder() {
        viewModelScope.launch {
            val result = apiRepo.getCustomerReminder().awaitAndGet()
            getReminderResponse.postValue(result)
        }
    }

    private lateinit var getAllReminderResponse: MutableLiveData<Result<ServerResponse<GetCustomerReminderResponse>>>
    fun provideGetAllReminderResponse(): MutableLiveData<Result<ServerResponse<GetCustomerReminderResponse>>> {
        if (!::getAllReminderResponse.isInitialized) getAllReminderResponse = MutableLiveData()
        return getAllReminderResponse
    }

    fun getAllCustomerReminder() {
        viewModelScope.launch {
            val result = apiRepo.getAllCustomerReminder().awaitAndGet()
            getAllReminderResponse.postValue(result)
        }
    }

}