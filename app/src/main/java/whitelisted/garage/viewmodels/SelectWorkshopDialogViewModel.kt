package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.database.repository.RoomRepository
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class SelectWorkshopDialogViewModel(val app: Application,
    apiRepository: APIRepository,
    private val roomRepository: RoomRepository) : BaseViewModel(app, apiRepository) {


    private lateinit var workshopListResponseFromRoom: MutableLiveData<List<WorkshopListResponseModel>>
    fun provideWorkshopListResponseFromRoom(): MutableLiveData<List<WorkshopListResponseModel>> {
        if (!::workshopListResponseFromRoom.isInitialized) {
            workshopListResponseFromRoom = MutableLiveData()
        }
        return workshopListResponseFromRoom
    }

    private lateinit var workshopListResponseFromAPI: MutableLiveData<Result<ServerResponse<List<WorkshopListResponseModel>>>>
    fun provideWorkshopListResponseFromAPI(): MutableLiveData<Result<ServerResponse<List<WorkshopListResponseModel>>>> {
        if (!::workshopListResponseFromAPI.isInitialized) {
            workshopListResponseFromAPI = MutableLiveData()
        }
        return workshopListResponseFromAPI
    }

    fun getWorkshopListFromDB() {
        viewModelScope.launch {
            val result = roomRepository.getWorkshopList()
            if (!::workshopListResponseFromRoom.isInitialized) {
                workshopListResponseFromRoom = MutableLiveData(result)
            } else workshopListResponseFromRoom.postValue(result)

        }
    }

    fun getWorkshopListAPI() {
        viewModelScope.launch {
            val result = apiRepo.getWorkshopList().awaitAndGet()
            workshopListResponseFromAPI.postValue(result)
        }
    }

    fun addWorkshopsToRoom(workshopList: List<WorkshopListResponseModel>?) {
        viewModelScope.launch (Dispatchers.Default){
            roomRepository.deleteWorkshopList()
            roomRepository.insertWorkshopList(workshopList)
            workshopListResponseFromRoom.postValue(workshopList)
        }
    }

}