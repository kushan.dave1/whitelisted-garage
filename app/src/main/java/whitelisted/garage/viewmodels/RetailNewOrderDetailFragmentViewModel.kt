package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.RetailCreateOrderRequest
import whitelisted.garage.api.response.EmployeeListResponseModel
import whitelisted.garage.api.response.RetailOrderDetailResponse
import whitelisted.garage.api.response.RetailPlaceOrderResponse
import whitelisted.garage.api.response.SavedWorkshopResponseModel
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment

class RetailNewOrderDetailFragmentViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private lateinit var createOrderRequest: RetailCreateOrderRequest
    fun provideCreateOrderRequest(): RetailCreateOrderRequest {
        return if (::createOrderRequest.isInitialized) createOrderRequest
        else {
            createOrderRequest = RetailCreateOrderRequest()
            createOrderRequest
        }
    }


    private lateinit var placeOrderResponse: MutableLiveData<Result<ServerResponse<RetailPlaceOrderResponse>>>
    fun providePlaceOrderResponse(): MutableLiveData<Result<ServerResponse<RetailPlaceOrderResponse>>> {
        if (!::placeOrderResponse.isInitialized) {
            placeOrderResponse = MutableLiveData()
        }
        return placeOrderResponse
    }

    fun createNewOrder() {
        viewModelScope.launch {
            val result = apiRepo.createUpdateRetailOrder(createOrderRequest).awaitAndGet()
            placeOrderResponse.postValue(result)
        }
    }

    private lateinit var updateOrderResponse: MutableLiveData<Result<ServerResponse<RetailPlaceOrderResponse>>>
    fun provideUpdateOrderResponse(): MutableLiveData<Result<ServerResponse<RetailPlaceOrderResponse>>> {
        if (!::updateOrderResponse.isInitialized) {
            updateOrderResponse = MutableLiveData()
        }
        return updateOrderResponse
    }

    fun updateOrder() {
        createOrderRequest.orderId = OrderDetailWrapperFragment.orderId
        viewModelScope.launch {
            val result = apiRepo.createUpdateRetailOrder(createOrderRequest).awaitAndGet()
            updateOrderResponse.postValue(result)
        }
    }

    private lateinit var savedWorkshopListResponse: MutableLiveData<Result<ServerResponse<List<SavedWorkshopResponseModel>>>>
    fun provideSavedWorkshopListResponse(): MutableLiveData<Result<ServerResponse<List<SavedWorkshopResponseModel>>>> {
        if (!::savedWorkshopListResponse.isInitialized) {
            savedWorkshopListResponse = MutableLiveData()
        }
        return savedWorkshopListResponse
    }

    fun getSavedWorkshopsAPI() {
        viewModelScope.launch {
            val result = apiRepo.getSavedWorkshops().awaitAndGet()
            savedWorkshopListResponse.postValue(result)
        }
    }

    private lateinit var orderDetailResponse: MutableLiveData<Result<ServerResponse<RetailOrderDetailResponse>>>
    fun provideOrderDetailResponse(): MutableLiveData<Result<ServerResponse<RetailOrderDetailResponse>>> {
        if (!::orderDetailResponse.isInitialized) {
            orderDetailResponse = MutableLiveData()
        }
        return orderDetailResponse
    }

    fun getOrderDetail() {
        viewModelScope.launch {
            val result =
                apiRepo.getRetailOrderDetail(OrderDetailWrapperFragment.orderId).awaitAndGet()
            orderDetailResponse.postValue(result)
        }
    }

    private lateinit var employeeResponseResponseModel: MutableLiveData<Result<ServerResponse<List<EmployeeListResponseModel>>>>
    fun provideManageEmployeeResponse(): MutableLiveData<Result<ServerResponse<List<EmployeeListResponseModel>>>> {
        if (!::employeeResponseResponseModel.isInitialized) {
            employeeResponseResponseModel = MutableLiveData()
        }
        return employeeResponseResponseModel
    }
    fun getEmployess() {
        viewModelScope.launch {
            val resultEmployees = apiRepo.getRoleBasedEmployees("Mechanic,Owner,Supervisor,Driver")
                .awaitAndGet()
            employeeResponseResponseModel.postValue(resultEmployees)
        }
    }

}