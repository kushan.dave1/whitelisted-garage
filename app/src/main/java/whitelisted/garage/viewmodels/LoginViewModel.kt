package whitelisted.garage.viewmodels

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.*
import whitelisted.garage.api.response.*
import whitelisted.garage.api.response.login.LoginUserResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.database.repository.RoomRepository
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import whitelisted.garage.utils.FirebaseAnalyticsLog
import java.net.URL

class LoginViewModel(val app: Application,
    apiRepo: APIRepository,
    private val roomRepository: RoomRepository) : BaseViewModel(app, apiRepo) {

    private lateinit var getOtpResponse: MutableLiveData<Result<ServerResponse<String>>>
    private lateinit var getUpdateInfoResponse: MutableLiveData<Result<ServerResponse<GetUpdateInfoResponse>>>

    fun provideGetUpdateInfoResponse(): MutableLiveData<Result<ServerResponse<GetUpdateInfoResponse>>> {
        if (!::getUpdateInfoResponse.isInitialized) {
            getUpdateInfoResponse = MutableLiveData()
        }
        return getUpdateInfoResponse
    }

    fun provideGetOtpResponse(): MutableLiveData<Result<ServerResponse<String>>> {
        if (!::getOtpResponse.isInitialized) {
            getOtpResponse = MutableLiveData()
        }
        return getOtpResponse
    }

    private lateinit var signUpRequest: MutableLiveData<SignUpRequest>
    fun provideSignUpRequest(): MutableLiveData<SignUpRequest> {
        if (!::signUpRequest.isInitialized) {
            signUpRequest = MutableLiveData()
        }
        return signUpRequest
    }

    private lateinit var signUpResponse: MutableLiveData<Result<ServerResponse<LoginUserResponse>>>
    fun provideSignUpResponse(): MutableLiveData<Result<ServerResponse<LoginUserResponse>>> {
        if (!::signUpResponse.isInitialized) {
            signUpResponse = MutableLiveData()
        }
        return signUpResponse
    }

    private lateinit var getOtpForLoginResponse: MutableLiveData<Result<ServerResponse<String>>>
    fun provideOTPForLoginResponse(): MutableLiveData<Result<ServerResponse<String>>> {
        if (!::getOtpForLoginResponse.isInitialized) {
            getOtpForLoginResponse = MutableLiveData()
        }
        return getOtpForLoginResponse
    }

    private lateinit var trueCallerLoginResponse: MutableLiveData<Result<ServerResponse<LoginUserResponse>>>
    fun provideTrueCallerLoginResponse(): MutableLiveData<Result<ServerResponse<LoginUserResponse>>> {
        if (!::trueCallerLoginResponse.isInitialized) {
            trueCallerLoginResponse = MutableLiveData()
        }
        return trueCallerLoginResponse
    }

    private lateinit var verifyOtpResponse: MutableLiveData<Result<ServerResponse<LoginUserResponse>>>
    fun provideVerifyOtpResponse(): MutableLiveData<Result<ServerResponse<LoginUserResponse>>> {
        if (!::verifyOtpResponse.isInitialized) {
            verifyOtpResponse = MutableLiveData()
        }
        return verifyOtpResponse
    }

    private lateinit var resetPasswordResponse: MutableLiveData<Result<ServerResponse<Any>>>
    fun provideResetPasswordOtpResponse(): MutableLiveData<Result<ServerResponse<Any>>> {
        if (!::resetPasswordResponse.isInitialized) {
            resetPasswordResponse = MutableLiveData()
        }
        return resetPasswordResponse
    }

    fun callGetOTPAPI(hashCode: String?, countryCode: String?, mobileNumber: String?) {
        viewModelScope.launch {
            val result = apiRepo.getOTP(hashCode, countryCode, mobileNumber).awaitAndGet()
            getOtpResponse.postValue(result)
        }
    }

    fun callSignUpAPI(countryCode: String?, otp: String) {
        signUpRequest.value?.countryCode = countryCode
        signUpRequest.value?.otp = otp
        if (signUpRequest.value != null) {
            viewModelScope.launch {
                val result = apiRepo.signUpUser(signUpRequest.value).awaitAndGet()
                handleSignUpResponse(result, true)
                firebaseEventLog(result, false, FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_OTP)
                signUpResponse.postValue(result)
            }
        }
    }

    fun callTrueCallerLogin(trueCallerLoginRequest: TrueCallerLoginRequest) {
        viewModelScope.launch {
            val result = apiRepo.trueCallerLogin(trueCallerLoginRequest).awaitAndGet()
            handleSignUpResponse(result, false)
            firebaseEventLog(result, true, FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_LOGIN)
            trueCallerLoginResponse.postValue(result)
        }
    }

    private fun handleSignUpResponse(result: Result<ServerResponse<LoginUserResponse>>,
        isSignup: Boolean) {
        when (result) {
            is Result.Success<*> -> {
                result.body.let { data ->
                    val res = CommonUtils.genericCastOrNull<ServerResponse<LoginUserResponse>>(data)
                    putStringSharedPreference(AppENUM.ACCESS_TOKEN_, res?.data?.authToken ?: "")
                    putStringSharedPreference(AppENUM.USER_ID, res?.data?.userId ?: "")
                    putStringSharedPreference(AppENUM.MOBILE_NUMBER, res?.data?.mobile ?: "")
                    putStringSharedPreference(AppENUM.UserKeySaveENUM.EMAIL, res?.data?.email ?: "")
                    putStringSharedPreference(AppENUM.UserKeySaveENUM.USER_ROLE, res?.data?.userRole ?: "")
                    putStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                        res?.data?.countryCode ?: "+91")
                    putStringSharedPreference(AppENUM.USER_DATA, Gson().toJson(res?.data))
                    FirebaseAnalyticsLog.setUniqueUserOrAttribute(FirebaseAnalyticsLog.FirebaseEventNameENUM.UNIQUE_ID,
                        res?.data?.userId ?: "")
                    FirebaseAnalyticsLog.setUniqueUserOrAttribute(FirebaseAnalyticsLog.FirebaseEventNameENUM.MOBILE_NUMBER,
                        res?.data?.mobile ?: "")
                    if ((res?.data?.workshopList?.size ?: 0) >= 1 && isSignup) {
                        putStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                            res?.data?.workshopList?.get(0)?.workshopId.toString())
                        putStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_ADDRESS,
                            res?.data?.workshopList?.get(0)?.address.toString())
                        putStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                            res?.data?.workshopList?.get(0)?.types
                                ?: AppENUM.RefactoredStrings.WORKSHOP_CONSTANT)
                        putStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_NAME,
                            res?.data?.workshopList?.get(0)?.name.toString())
                        putStringSharedPreference(AppENUM.UserKeySaveENUM.OWNER_NAME,
                            res?.data?.workshopList?.get(0)?.ownerName.toString())
                        putStringSharedPreference(AppENUM.UserKeySaveENUM.OWNER_ID,
                            res?.data?.workshopList?.get(0)?.ownerId.toString())
                        putStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_GSTIN,
                            res?.data?.workshopList?.get(0)?.gstin.toString())
                        putStringSharedPreference(AppENUM.USER_ROLE, res?.data?.workshopList?.get(0)?.workshopRole ?: "")
                    }
                    addWorkshopsToRoom(res?.data?.workshopList)
                    if (isSignup) putBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN, false)
                    else putBooleanSharedPreference(AppENUM.WALKTHROUGH_SHOWN, true)
                }
            }
            is Result.Failure -> {

            }
        }
    }

    private fun addWorkshopsToRoom(workshopList: List<WorkshopListResponseModel>?) {
        launch {
            roomRepository.deleteWorkshopList()
            roomRepository.insertWorkshopList(workshopList) //            workshopListResponseFromRoom.postValue(workshopList)
        }
    }

    fun loginUserAPI(hashCode: String?, countryCode: String?, mobileNumber: String?) {
        viewModelScope.launch {
            val result = apiRepo.getOtpForLogin(hashCode, countryCode, mobileNumber)
                .awaitAndGet() //            handleSignUpResponse(result)
            getOtpForLoginResponse.postValue(result)
        }
    }

    fun callVerifyOtpAPI(mobile: String,
        action: String,
        otp: String) { //        viewModelScope.launch {
        //            val req = VerifyOtpRequest()
        //            req.mobile = mobile
        //            req.action = action
        //            req.otp = otp
        //            val result = apiRepo.verifyOTP(req).awaitAndGet()
        //            verifyOtpResponse.postValue(result)
        //        }
    }

    fun callVerifyOtpAPIForLogin(countryCode: String, mobile: String, action: String, otp: String) {
        viewModelScope.launch {
            val req = VerifyOtpRequest()
            req.countryCode = countryCode
            req.mobile = mobile
            req.action = action
            req.otp = otp
            val result = apiRepo.verifyOTPForLogin(req).awaitAndGet()
            handleSignUpResponse(result, false)
            firebaseEventLog(result, true, FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_OTP)
            verifyOtpResponse.postValue(result)
        }
    }

    private fun firebaseEventLog(result: Result<ServerResponse<LoginUserResponse>>,
        b: Boolean,
        firescreen: String) {
        when (result) {
            is Result.Success<*> -> {

                Bundle().apply {
                    putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN, firescreen)
                    if (b) {
                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_LOGIN,
                            this)

                    } else {
                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_REGISTER,
                            this)
                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_LOGIN_REG,
                            this)

                    }
                }

            }
            is Result.Failure -> {

            }
        }
    }

    fun callResetPasswordAPI(mobile: String, password: String) {
        viewModelScope.launch {
            val request = ResetPasswordRequest()
            request.mobile = mobile
            request.password = password
            val result = apiRepo.resetPassword(request).awaitAndGet()
            resetPasswordResponse.postValue(result)
        }
    }

    fun getUpdateInfo() {
        viewModelScope.launch {
            val result = apiRepo.getUpdateInfo().awaitAndGet()
            getUpdateInfoResponse.postValue(result)
        }
    }

    private lateinit var getPinResponse: MutableLiveData<Result<ServerResponse<SetPinRequest>>>
    fun provideGetPinResponse(): MutableLiveData<Result<ServerResponse<SetPinRequest>>> {
        if (!::getPinResponse.isInitialized) getPinResponse = MutableLiveData()
        return getPinResponse
    }

    fun getPin() {
        viewModelScope.launch {
            val result =
                apiRepo.getPin(getStringSharedPreference(AppENUM.USER_ID, "")).awaitAndGet()
            getPinResponse.postValue(result)
        }
    }

    private lateinit var setPinResponse: MutableLiveData<Result<ServerResponse<SetPinRequest>>>
    fun provideSetPinResponse(): MutableLiveData<Result<ServerResponse<SetPinRequest>>> {
        if (!::setPinResponse.isInitialized) setPinResponse = MutableLiveData()
        return setPinResponse
    }

    fun setPin(pin: String) {
        var req: SetPinRequest? = null
        req = SetPinRequest(pin)

        viewModelScope.launch {
            val result =
                apiRepo.setPin(getStringSharedPreference(AppENUM.USER_ID, ""), req).awaitAndGet()

            setPinResponse.postValue(result)
        }
    }

    private lateinit var locationDetailResponse: MutableLiveData<Result<ServerResponse<LocationDetailsResponse>>>
    fun provideLocationDetailResponse(): MutableLiveData<Result<ServerResponse<LocationDetailsResponse>>> {
        if (!::locationDetailResponse.isInitialized) locationDetailResponse = MutableLiveData()
        return locationDetailResponse
    }

    fun getLocationDetails(ipAddress: String) {
        viewModelScope.launch {
            val result = apiRepo.getLocationDetails(ipAddress).awaitAndGet()
            locationDetailResponse.postValue(result)
        }
    }

    private lateinit var countryCodesResponse: MutableLiveData<Result<ServerResponse<MutableList<CountryCodeResponseModel>>>>
    fun provideCountryCodesResponse(): MutableLiveData<Result<ServerResponse<MutableList<CountryCodeResponseModel>>>> {
        if (!::countryCodesResponse.isInitialized) countryCodesResponse = MutableLiveData()
        return countryCodesResponse
    }

    fun getCountryCodes() {
        viewModelScope.launch {
            val result = apiRepo.getCountryCodes().awaitAndGet()
            countryCodesResponse.postValue(result)
        }
    }

    private lateinit var ipResponse: MutableLiveData<IpResponse>
    fun provideIPResponse(): MutableLiveData<IpResponse> {
        if (!::ipResponse.isInitialized) ipResponse = MutableLiveData()
        return ipResponse
    }

    fun getIp(url: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val ip = URL(url).readText()
            ipResponse.postValue(Gson().fromJson(ip, IpResponse::class.java))
        }
    }

    fun saveUserCountryCodeDetails(flagImage: String?,
        dialCode: String?,
        currency: String?,
        currencySymbol: String?,
        countryId: String?) {
        putStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_FLAG,
            flagImage ?: AppENUM.RefactoredStrings.defaultFlag)
        putStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
            dialCode ?: AppENUM.RefactoredStrings.defaultCountryCode)
        putStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY,
            currency ?: AppENUM.RefactoredStrings.defaultCurrency)
        putStringSharedPreference(AppENUM.UserKeySaveENUM.CURRENCY_SYMBOL,
            currencySymbol ?: AppENUM.RefactoredStrings.defaultCurrencySymbol)
        putStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_ID,
            countryId ?: AppENUM.RefactoredStrings.defaultCountryId)
    }

    suspend fun checkGST(gstNumber: String) :GSTVeriifyRespoonse? =
    withContext(Dispatchers.IO) {
        return@withContext when (val result =
            apiRepo.verifyGST(gstNumber).awaitAndGet()) {
            is Result.Success -> result.body.data
            else -> null
        }
    }

}

