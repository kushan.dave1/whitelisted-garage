package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.MyCustomerModel
import whitelisted.garage.api.response.GetMessageFromApiResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class MyCustomerViewModel(application: Application, apiRepository: APIRepository) :
    BaseViewModel(application, apiRepository) {

    private lateinit var myCustomerList: MutableLiveData<Result<ServerResponse<List<MyCustomerModel>>>>

    private lateinit var messageFromApi: MutableLiveData<Result<ServerResponse<GetMessageFromApiResponse>>>

    fun provideMessageFromApi(): MutableLiveData<Result<ServerResponse<GetMessageFromApiResponse>>> {
        if (!::messageFromApi.isInitialized) {
            messageFromApi = MutableLiveData()
        }
        return messageFromApi
    }

    fun getMessageFromApi(orderId: String?) {
        viewModelScope.launch {
            val result = apiRepo.getMessageFromAPI(orderId).awaitAndGet()
            messageFromApi.postValue(result)
        }
    }

    fun getMessageFromAPIRetailer(mobile: String?) {
        viewModelScope.launch {
            val result = apiRepo.getMessageFromAPIRetailer(mobile).awaitAndGet()
            messageFromApi.postValue(result)
        }
    }


    fun provideMyCustomerList(): MutableLiveData<Result<ServerResponse<List<MyCustomerModel>>>> {
        if (!::myCustomerList.isInitialized) {
            myCustomerList = MutableLiveData()
        }
        return myCustomerList
    }

    fun getMyCustomerList() {
        viewModelScope.launch {
            val result = apiRepo.getMyCustomerList().awaitAndGet()
            myCustomerList.postValue(result)
        }
    }

    fun getMyCustomerListWithSearch(search: String) {
        viewModelScope.launch {
            val result = apiRepo.getMyCustomerListWithSearch(search).awaitAndGet()
            myCustomerList.postValue(result)
        }
    }


}