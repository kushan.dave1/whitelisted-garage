package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.*
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.SingleLiveEvent

class HomeFragmentViewModel(val app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private lateinit var homeScreenResponse: MutableLiveData<Result<ServerResponse<HomeScreenResponse>>>
    fun provideHomeScreenResponse(): MutableLiveData<Result<ServerResponse<HomeScreenResponse>>> {
        if (!::homeScreenResponse.isInitialized) homeScreenResponse = MutableLiveData()
        return homeScreenResponse
    }

    private lateinit var carouselResponse: MutableLiveData<Result<ServerResponse<HomeCarouselResponse>>>
    fun provideCarouselResponse(): MutableLiveData<Result<ServerResponse<HomeCarouselResponse>>> {
        if (!::carouselResponse.isInitialized) carouselResponse = MutableLiveData()
        return carouselResponse
    }

    private lateinit var retryPaymentResponse: MutableLiveData<Result<ServerResponse<RetryPaymentResponse>>>
    fun provideRetryPaymentResponse(): MutableLiveData<Result<ServerResponse<RetryPaymentResponse>>> {
        if (!::retryPaymentResponse.isInitialized) retryPaymentResponse = MutableLiveData()
        return retryPaymentResponse
    }

    private lateinit var cancelRetryPaymentResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideCancelRetryPaymentResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::cancelRetryPaymentResponse.isInitialized) cancelRetryPaymentResponse =
            MutableLiveData()
        return cancelRetryPaymentResponse
    }

    fun getHomeScreenAPI(workshopId: String?) {
        viewModelScope.launch {
//            val bannerResult = apiRepo.getCarousel()
//            val retryPaymentResult =
//                apiRepo.getRetryPaymentStatus()
//            val resultGMB = apiRepo.getGMB()
            val result = apiRepo.getHomeScreen(workshopId)
            if (::homeScreenResponse.isInitialized) homeScreenResponse.postValue(result.awaitAndGet())
        }
    }

    fun getAfterHomeaPIs(){
        viewModelScope.launch {
            val bannerResult = apiRepo.getCarousel()
            val retryPaymentResult = apiRepo.getRetryPaymentStatus()
            val resultGMB = apiRepo.getGMB()
            if (::carouselResponse.isInitialized) carouselResponse.postValue(bannerResult.awaitAndGet())
            if (::retryPaymentResponse.isInitialized) retryPaymentResponse.postValue(
                retryPaymentResult.awaitAndGet())
            if (::getGMD.isInitialized) getGMD.postValue(resultGMB.awaitAndGet())
        }
    }

    fun getOnlyHomeScreenAPI(workshopId: String?) {
        viewModelScope.launch {
            val result = apiRepo.getHomeScreen(workshopId).awaitAndGet()
            if (::homeScreenResponse.isInitialized) homeScreenResponse.postValue(result)
        }
    }

    private lateinit var getGMD: SingleLiveEvent<Result<ServerResponse<GetGMDResponse>>>
    fun provideGMD(): SingleLiveEvent<Result<ServerResponse<GetGMDResponse>>> {
        if (!::getGMD.isInitialized) getGMD = SingleLiveEvent()
        return getGMD
    }

    fun cancelRetryPayment(requestResponse: IdRequestResponse) {
        viewModelScope.launch {
            val cancelRetryPaymentResult = apiRepo.cancelRetryPayment(requestResponse).awaitAndGet()
            cancelRetryPaymentResponse.postValue(cancelRetryPaymentResult)
        }
    }

    private lateinit var homeScreenResponseForMonth: MutableLiveData<Result<ServerResponse<HomeScreenResponse>>>
    fun provideHomeScreenResponseForMonth(): MutableLiveData<Result<ServerResponse<HomeScreenResponse>>> {
        if (!::homeScreenResponseForMonth.isInitialized) homeScreenResponseForMonth =
            MutableLiveData()
        return homeScreenResponseForMonth
    }

    fun getDataForMonth(workshopId: String?, monthPosition: String) {
        viewModelScope.launch {
            val result = apiRepo.getHomeScreenForMonth(workshopId, monthPosition).awaitAndGet()
            homeScreenResponseForMonth.postValue(result)
        }
    }

    private lateinit var getNearByBidWorkshop: MutableLiveData<Result<ServerResponse<Map<String, GetNearByBidingWorkshop>>>>
    fun provideGetNearByBidingWorkshop(): MutableLiveData<Result<ServerResponse<Map<String, GetNearByBidingWorkshop>>>> {
        if (!::getNearByBidWorkshop.isInitialized) getNearByBidWorkshop = MutableLiveData()
        return getNearByBidWorkshop
    }

    fun getNearByWorkshop() {
        viewModelScope.launch {
            val resultNearbyWorkshop = apiRepo.getNearByBidingWorkShop().awaitAndGet()
            getNearByBidWorkshop.postValue(resultNearbyWorkshop)
        }
    }

}