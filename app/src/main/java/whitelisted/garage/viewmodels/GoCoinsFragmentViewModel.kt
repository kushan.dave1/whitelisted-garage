package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.AmountRequest
import whitelisted.garage.api.request.SSCheckoutRequest
import whitelisted.garage.api.response.*
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.SingleLiveEvent

class GoCoinsFragmentViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {
    private lateinit var getGMD: SingleLiveEvent<Result<ServerResponse<GetGMDResponse>>>
    fun provideGMD(): SingleLiveEvent<Result<ServerResponse<GetGMDResponse>>> {
        if (!::getGMD.isInitialized) getGMD = SingleLiveEvent()
        return getGMD
    }
    fun getGMB() {
        viewModelScope.launch {
            val resultGMB = apiRepo.getGMB().awaitAndGet()
            getGMD.postValue(resultGMB)
        }
    }

    private lateinit var goCoinsScreenResponse: MutableLiveData<Result<ServerResponse<GetGoCoinsScreenResponse>>>
    fun provideGoCoinsScreenResponse(): MutableLiveData<Result<ServerResponse<GetGoCoinsScreenResponse>>> {
        if (!::goCoinsScreenResponse.isInitialized) goCoinsScreenResponse = MutableLiveData()
        return goCoinsScreenResponse
    }

    fun getGoCoinsScreenResponse() {
        viewModelScope.launch {
            val result = apiRepo.getGoCoinScreenResponse().awaitAndGet()
            goCoinsScreenResponse.postValue(result)
        }
    }

    private lateinit var orderIdForPaymentResponse: MutableLiveData<Result<ServerResponse<OrderIdForPaymentResponse>>>
    fun provideOrderIdForPaymentResponse(): MutableLiveData<Result<ServerResponse<OrderIdForPaymentResponse>>> {
        if (!::orderIdForPaymentResponse.isInitialized) orderIdForPaymentResponse =
            MutableLiveData()
        return orderIdForPaymentResponse
    }
    fun getOrderId(amount: String, goCoins: String, displayMsg: String, isMembership:String?=null) {
        val map = mutableMapOf<String, String>()
        map["amount"] = amount
        map["coins"] = goCoins
        map["display_msg"] = displayMsg
        isMembership?.let {
            map["is_membership"] = "true"
        }
        viewModelScope.launch {
            val result = apiRepo.getOrderIdForPayment(map).awaitAndGet()
            orderIdForPaymentResponse.postValue(result)
        }
    }

    private lateinit var verifyPaymentResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideVerifyPaymentResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::verifyPaymentResponse.isInitialized) verifyPaymentResponse = MutableLiveData()
        return verifyPaymentResponse
    }

    private lateinit var buyMembershipResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideBuyMembershipResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::buyMembershipResponse.isInitialized) buyMembershipResponse = MutableLiveData()
        return buyMembershipResponse
    }

    fun sendPaymentDetailsToServer(paymentId: String,
        orderId: String,
        amount: String,
        currency: String) {
        val map = mutableMapOf<String, String>()
        map["razorpay_payment_id"] = paymentId
        map["razorpay_order_id"] = orderId
        map["payment_amount"] = amount
        map["payment_currency"] = currency
        viewModelScope.launch {
            val result = apiRepo.verifyPaymentDetails(map).awaitAndGet()
            verifyPaymentResponse.postValue(result)
        }
    }

    fun buyMembership(paymentId: String,
//        signature: String,
//        subscriptionId: String,
        orderId: String,
        amount: String,
        currency: String) {
        val map = mutableMapOf<String, String>()
        map["razorpay_payment_id"] = paymentId
        map["razorpay_order_id"] = orderId
        map["payment_amount"] = amount
//        map["razorpay_signature"] = signature
//        map["razorpay_subscription_id"] = subscriptionId
        map["payment_currency"] = currency
        viewModelScope.launch {
            val result = apiRepo.buyMembership(map).awaitAndGet()
            buyMembershipResponse.postValue(result)
        }
    }

    private lateinit var getRazorPayKeyResponse: MutableLiveData<Result<ServerResponse<GetRazorPayKey>>>
    fun provideRazorPayKeyResponse(): MutableLiveData<Result<ServerResponse<GetRazorPayKey>>> {
        if (!::getRazorPayKeyResponse.isInitialized) getRazorPayKeyResponse = MutableLiveData()
        return getRazorPayKeyResponse
    }
    fun getRazorPayKey() {
        viewModelScope.launch {
            val result = apiRepo.getRazorPayKey().awaitAndGet()
            getRazorPayKeyResponse.postValue(result)
        }
    }

    private lateinit var getGoCoinOptions: SingleLiveEvent<Result<ServerResponse<GetGOCoinOptionsResponse>>>

    fun provideGetGoCoinOptions(): SingleLiveEvent<Result<ServerResponse<GetGOCoinOptionsResponse>>> {
        if (!::getGoCoinOptions.isInitialized) getGoCoinOptions = SingleLiveEvent()
        return getGoCoinOptions
    }

    suspend fun getGoCoinAmountOptions() : GetGOCoinOptionsResponse? {
        return when(val result = apiRepo.getGoCoinsOptions().awaitAndGet()) {
            is Result.Success -> result.body.data
            else -> null
        }
    }

    fun getGoCoinsOptions() {
        viewModelScope.launch {
            val result = apiRepo.getGoCoinsOptions().awaitAndGet()
            getGoCoinOptions.postValue(result)

        }
    }

    private lateinit var getRazorPayKeySSResponse: MutableLiveData<Result<ServerResponse<GetRazorPayKey>>>
    fun provideRazorPayKeySSResponse(): MutableLiveData<Result<ServerResponse<GetRazorPayKey>>> {
        if (!::getRazorPayKeySSResponse.isInitialized) getRazorPayKeySSResponse = MutableLiveData()
        return getRazorPayKeySSResponse
    }
    fun getRazorPayKeyForSS() {
        viewModelScope.launch {
            val result = apiRepo.getRazorPayKeyForSS().awaitAndGet()
            getRazorPayKeySSResponse.postValue(result)
        }
    }

    private lateinit var orderIdForSSPaymentResponse: MutableLiveData<Result<ServerResponse<OrderIdForPaymentResponse>>>
    fun provideOrderIdForSSPaymentResponse(): MutableLiveData<Result<ServerResponse<OrderIdForPaymentResponse>>> {
        if (!::orderIdForSSPaymentResponse.isInitialized) orderIdForSSPaymentResponse =
            MutableLiveData()
        return orderIdForSSPaymentResponse
    }
    fun getOrderIdForSS(amountRequest: AmountRequest) {
        viewModelScope.launch {
            val result = apiRepo.getOrderIdForSSPayment(amountRequest).awaitAndGet()
            orderIdForSSPaymentResponse.postValue(result)
        }
    }

    private lateinit var ssCheckoutResponse: MutableLiveData<Result<ServerResponse<Any>>>
    fun provideSSCheckoutResponse(): MutableLiveData<Result<ServerResponse<Any>>> {
        if (!::ssCheckoutResponse.isInitialized) ssCheckoutResponse = MutableLiveData()
        return ssCheckoutResponse
    }
    fun checkoutSSOrder(ssCheckoutRequest: SSCheckoutRequest) {
        viewModelScope.launch {
            val result = apiRepo.checkoutSSOrder(ssCheckoutRequest).awaitAndGet()
            ssCheckoutResponse.postValue(result)
        }
    }

    suspend fun initiateSubscription(amount:String?):SubsResponse? = withContext(Dispatchers.IO) {
        return@withContext when(val result = apiRepo.buySubs(amount).awaitAndGet()){
            is Result.Success -> result.body.data
            else -> null
        }
    }

}