package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.SearchImageResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class ChooseImageFragmentViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {
    private lateinit var getPartItemResponse: MutableLiveData<Result<ServerResponse<List<SearchImageResponse>>>>


    fun providePartItemResponse(): MutableLiveData<Result<ServerResponse<List<SearchImageResponse>>>> {
        if (!::getPartItemResponse.isInitialized) {
            getPartItemResponse = MutableLiveData()
        }
        return getPartItemResponse
    }

    fun searchImages(type: String) {
        viewModelScope.launch {
            val result = apiRepo.searchImages(type).awaitAndGet()
            getPartItemResponse.postValue(result)
        }
    }
}