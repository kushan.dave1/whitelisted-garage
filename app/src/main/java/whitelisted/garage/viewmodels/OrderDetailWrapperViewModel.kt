package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.CancelOrderRequest
import whitelisted.garage.api.response.OrderDetailResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class OrderDetailWrapperViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private lateinit var exteriorImagesResult: MutableLiveData<ArrayList<String>>
    lateinit var orderDetails: MutableLiveData<OrderDetailResponse>
    fun provideExteriorImagesResult(): MutableLiveData<ArrayList<String>> {
        return if (!::exteriorImagesResult.isInitialized) {
            exteriorImagesResult = MutableLiveData<ArrayList<String>>()
            exteriorImagesResult
        } else exteriorImagesResult
    }

    private lateinit var interiorImagesResult: MutableLiveData<ArrayList<String>>
    fun provideInteriorImagesResult(): MutableLiveData<ArrayList<String>> {
        return if (!::interiorImagesResult.isInitialized) {
            interiorImagesResult = MutableLiveData<ArrayList<String>>()
            interiorImagesResult
        } else interiorImagesResult
    }

    private lateinit var cancelOrderResponse: MutableLiveData<Result<ServerResponse<JSONObject>>>
    fun provideCancelOrderResponse(): MutableLiveData<Result<ServerResponse<JSONObject>>> {
        if (!::cancelOrderResponse.isInitialized) {
            cancelOrderResponse = MutableLiveData()
        }
        return cancelOrderResponse
    }

    fun cancelOrder(orderId: String) {
        val cancelOrderReq = CancelOrderRequest()
        cancelOrderReq.orderId = orderId
        cancelOrderReq.statusId = 130
        viewModelScope.launch {
            val result = apiRepo.cancelOrder(cancelOrderReq).awaitAndGet()
            cancelOrderResponse.postValue(result)
        }
    }

    private lateinit var orderDetailResponse: MutableLiveData<Result<ServerResponse<OrderDetailResponse>>>
    fun provideOrderDetailResponse(): MutableLiveData<Result<ServerResponse<OrderDetailResponse>>> {
        if (!::orderDetailResponse.isInitialized) {
            orderDetailResponse = MutableLiveData()
        }
        return orderDetailResponse
    }


    fun getOrderDetail(orderId: String) {
        viewModelScope.launch {
            val result = apiRepo.getOrderDetail(orderId).awaitAndGet()
            handleResponse(result)
            orderDetailResponse.postValue(result)

        }
    }

    private fun handleResponse(
        result: Result<ServerResponse<OrderDetailResponse>>,
    ) {
        when (result) {
            is Result.Success -> {
                if (!::orderDetails.isInitialized) {
                    orderDetails = MutableLiveData()
                }
                result.body.let { data ->
                    orderDetails.value = data.data
                }
            }
            is Result.Failure -> {

            }
        }
    }

}