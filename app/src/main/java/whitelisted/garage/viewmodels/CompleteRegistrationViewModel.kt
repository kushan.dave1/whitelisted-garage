package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.AddWorkshopRequest
import whitelisted.garage.api.request.UpdateProfileRequest
import whitelisted.garage.api.response.WorkshopIDResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM

class CompleteRegistrationViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private lateinit var updateWorkshopResponse: MutableLiveData<Result<ServerResponse<WorkshopIDResponse>>>
    fun provideUpdateWorkshopResponse(): MutableLiveData<Result<ServerResponse<WorkshopIDResponse>>> {
        if (!::updateWorkshopResponse.isInitialized) updateWorkshopResponse = MutableLiveData()
        return updateWorkshopResponse
    }

    fun updateWorkshop(addWorkshopRequest: AddWorkshopRequest) {
        viewModelScope.launch {
            val result =
                apiRepo.updateWorkshop(getStringSharedPreference(AppENUM.UserKeySaveENUM.WORKSHOP_ID,
                    ""), addWorkshopRequest).awaitAndGet()
            updateWorkshopResponse.postValue(result)
        }
    }

    private lateinit var updateProfileResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideUpdateUserProfileResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::updateProfileResponse.isInitialized) {
            updateProfileResponse = MutableLiveData()
        }
        return updateProfileResponse
    }

    fun updateUserProfile(updateProfileRequest: UpdateProfileRequest) {
        viewModelScope.launch {
            val result = apiRepo.updateUserProfile(getStringSharedPreference(AppENUM.USER_ID, ""),
                updateProfileRequest).awaitAndGet()
            updateProfileResponse.postValue(result)
        }
    }

}