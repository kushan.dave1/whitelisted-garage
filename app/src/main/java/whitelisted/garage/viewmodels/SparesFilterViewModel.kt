package whitelisted.garage.viewmodels

import android.app.Application
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.MarketplaceFilter
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.awaitAndGet

class SparesFilterViewModel(app: Application, apiRepo: APIRepository):
        BaseViewModel(app, apiRepo) {

     suspend fun getFilters(): List<MarketplaceFilter> {
         return withContext(Dispatchers.IO) {
             return@withContext when(val result = apiRepo.getMarketplaceFilters().awaitAndGet()){
                 is Result.Success -> result.body.data
                 else -> listOf()
             }
         }
     }


}