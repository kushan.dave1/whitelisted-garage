package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.data.GetAccessoriesCatalogueResponse
import whitelisted.garage.api.request.AddServiceByOwnReq
import whitelisted.garage.api.request.CreateCustomPackageRequest
import whitelisted.garage.api.request.ServicesItem
import whitelisted.garage.api.response.GetAccCustomPackageDetails
import whitelisted.garage.api.response.PackageIdResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM

class CreateEditAccessoriesPackageViewModel(application: Application,
    apiRepository: APIRepository) : BaseViewModel(application, apiRepository) {

    private var searchJob: Job? = null
    private lateinit var createPackageResponse: MutableLiveData<Result<ServerResponse<PackageIdResponse>>>
    private lateinit var searchCatalogugeResponse: MutableLiveData<Result<ServerResponse<MutableList<GetAccessoriesCatalogueResponse>>>>
    private lateinit var getAccCustomPackageDetailsResponse: MutableLiveData<Result<ServerResponse<GetAccCustomPackageDetails>>>
    fun provideCreatePackageResponse(): MutableLiveData<Result<ServerResponse<PackageIdResponse>>> {
        if (!::createPackageResponse.isInitialized) createPackageResponse = MutableLiveData()
        return createPackageResponse
    }

    fun provideSearchCatalogueResponse(): MutableLiveData<Result<ServerResponse<MutableList<GetAccessoriesCatalogueResponse>>>> {
        if (!::searchCatalogugeResponse.isInitialized) searchCatalogugeResponse = MutableLiveData()
        return searchCatalogugeResponse
    }


    fun createCustomPackageAPI(createPackageRequest: CreateCustomPackageRequest,
        isTwoWheeler: Boolean) {
        viewModelScope.launch {
            val result = if (isTwoWheeler) apiRepo.createCustomPackageBike(createPackageRequest)
                .awaitAndGet() else apiRepo.createCustomPackage(createPackageRequest).awaitAndGet()
            createPackageResponse.postValue(result)
        }
    }

    fun getAccessoriesCatalogue(type: String, isTwoWheeler: Boolean) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            val result = if (isTwoWheeler) apiRepo.getAccessoriesCatalogueBike(type)
                .awaitAndGet() else apiRepo.getAccessoriesCatalogue(type).awaitAndGet()
            searchCatalogugeResponse.postValue(result)
        }
    }

    fun provideGetAccCustomPackageDetails(): MutableLiveData<Result<ServerResponse<GetAccCustomPackageDetails>>> {
        if (!::getAccCustomPackageDetailsResponse.isInitialized) getAccCustomPackageDetailsResponse =
            MutableLiveData()
        return getAccCustomPackageDetailsResponse
    }

    fun getAccCustomPackageDetails(packageId: String, isTwoWheeler: Boolean) {
        viewModelScope.launch {
            val result = if (isTwoWheeler) apiRepo.getAccCustomPackageDetailsBike(packageId)
                .awaitAndGet() else apiRepo.getAccCustomPackageDetails(packageId).awaitAndGet()
            getAccCustomPackageDetailsResponse.postValue(result)
        }
    }

    fun convertIntoListOfServiceItemObject(data: GetAccCustomPackageDetails): MutableList<ServicesItem> {
        val servicelist = mutableListOf<ServicesItem>()
        for (value in data.pkgItems ?: mutableListOf()) {
            val data = ServicesItem().apply {
                this.part_name = value.partName
                this.pricePerItem = value.pricePerItem
                this.quantity = value.quantity
                this.serviceId = value.serviceId?.toLong()
                this.serviceName = value.serviceName
                this.skuId = value.skuId
                this.taxRate = value.taxRate
                this.total = value.total
                this.workDone = value.workDone
                this.workDoneId = value.workDoneId
            }
            servicelist.add(data)
        }
        return servicelist
    }

    suspend fun addAccessoriesPartByOwn(partName: String): Boolean = withContext(Dispatchers.IO) {
        val req = JsonObject().apply { addProperty("name", partName) }
        val isAdded: Boolean = when (apiRepo.addCustomItemAccessories(req).awaitAndGet()) {
            is Result.Success -> true
            else -> false
        }
        return@withContext isAdded
    }
}