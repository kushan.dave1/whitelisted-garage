package whitelisted.garage.viewmodels

import android.app.Application
import android.net.Uri
import androidx.lifecycle.viewModelScope
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.launch
import whitelisted.garage.BuildConfig
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.BusinessCardDetails
import whitelisted.garage.api.response.BusinessCardTemplate
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM
import java.io.File
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class BusinessCardViewModel(app: Application, apiRepository: APIRepository) : BaseViewModel(app, apiRepository)  {

    suspend fun getAvailableTemplates(): List<BusinessCardTemplate> {
        return when(val result = apiRepo.getAvailableTemplates().awaitAndGet()){
            is Result.Success ->  result.body.data
            else -> listOf()
        }
    }

    suspend fun getSavedCards(): List<BusinessCardDetails> {
        return when(val result = apiRepo.getSavedCards().awaitAndGet()){
            is Result.Success -> result.body.data
            else -> listOf()
        }
    }

    suspend fun saveCard(details: BusinessCardDetails): Boolean {
        return when(apiRepo.saveBusinessCard(details).awaitAndGet()){
            is Result.Success -> true
            else -> false
        }
    }

    suspend fun deleteCard(cardId: String): Boolean {
        return when(apiRepo.deleteBusinessCard(cardId).awaitAndGet()) {
            is Result.Success -> true
            else -> false
        }
    }

}