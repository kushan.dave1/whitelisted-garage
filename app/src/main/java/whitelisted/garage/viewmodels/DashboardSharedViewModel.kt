package whitelisted.garage.viewmodels

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.data.SelectTabParams
import whitelisted.garage.api.data.SelectWorkshopDialogParams
import whitelisted.garage.api.request.UpdateLocationRequest
import whitelisted.garage.api.response.GetGoCoinBalanceResponse
import whitelisted.garage.api.response.ScreensConfigResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.database.repository.RoomRepository
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.SingleLiveEvent

class DashboardSharedViewModel(app: Application, apiRepository: APIRepository,
    val roomRepository: RoomRepository) : BaseViewModel(app, apiRepository) {

    lateinit var isNewLedgerAdded: MutableLiveData<Boolean>
    fun provideIsNewLedgerAdded(): MutableLiveData<Boolean> {
        if (!::isNewLedgerAdded.isInitialized) {
            isNewLedgerAdded = MutableLiveData()
        }
        return isNewLedgerAdded
    }

    lateinit var membershipPurchasedEvent: MutableLiveData<Boolean>
    fun provideMembershipPurchasedEvent(): MutableLiveData<Boolean> {
        if (!::membershipPurchasedEvent.isInitialized) {
            membershipPurchasedEvent = MutableLiveData()
        }
        return membershipPurchasedEvent
    }

    private lateinit var isWorkshopRefresh: MutableLiveData<Boolean>
    fun provideIsWorkshopRefresh(): MutableLiveData<Boolean> {
        return if (::isWorkshopRefresh.isInitialized) {
            isWorkshopRefresh
        } else {
            isWorkshopRefresh = MutableLiveData(false)
            isWorkshopRefresh
        }
    }

    private lateinit var openInstantReminder: SingleLiveEvent<Boolean>
    fun provideOpenInstantReminder(): SingleLiveEvent<Boolean> {
        if (!::openInstantReminder.isInitialized) {
            openInstantReminder = SingleLiveEvent()
        }
        return openInstantReminder
    }

    private lateinit var getGoCoinResponse: MutableLiveData<Result<ServerResponse<GetGoCoinBalanceResponse>>>
    fun provideGetGoCoinResponse(): MutableLiveData<Result<ServerResponse<GetGoCoinBalanceResponse>>> {
        if (!::getGoCoinResponse.isInitialized) {
            getGoCoinResponse = MutableLiveData()
        }
        return getGoCoinResponse
    }

    fun getGoCoinBalance() {
        viewModelScope.launch {
            val result = apiRepo.getGoCoinBalance().awaitAndGet()
            getGoCoinResponse.postValue(result)
        }
    }

    var screensConfigResponse: MutableLiveData<ScreensConfigResponse> = MutableLiveData()
//    private lateinit var configResponse: MutableLiveData<Result<ServerResponse<ScreensConfigResponse>>>
//    fun provideConfigResponse(): MutableLiveData<Result<ServerResponse<ScreensConfigResponse>>> {
//        if (!::configResponse.isInitialized) configResponse = MutableLiveData()
//        return configResponse
//    }

    private lateinit var updateLocationResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideUpdateLocationResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::updateLocationResponse.isInitialized) updateLocationResponse = MutableLiveData()
        return updateLocationResponse
    }

    fun logoutUser() {
        val lan = getStringSharedPreference(AppENUM.RefactoredStrings.LANGUAGE_CODE,
            AppENUM.LanguageConstants.LAN_DEFAULT)
        clearData() // clear local sf
        roomRepository.deleteWorkshopList() // clear local room db
        putStringSharedPreference(AppENUM.RefactoredStrings.LANGUAGE_CODE, lan)
        Bundle().apply {
            putString(FirebaseAnalyticsLog.FirebaseEventNameENUM.FIRE_SCREEN,
                FirebaseAnalyticsLog.FirebaseEventNameENUM.FS_ACCOUNT)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseAnalyticsLog.FirebaseEventNameENUM.EVENT_LOG_OUT,
                this)
        }
        FirebaseAnalyticsLog.setUniqueUserOrAttribute(FirebaseAnalyticsLog.FirebaseEventNameENUM.LOG_OUT_USER,
            "")

        roomRepository.deleteRecentItems()
    }

    private lateinit var deeplinkPageNameResponse: SingleLiveEvent<MutableList<String>>
    fun provideDeeplinkPageNameResponse(): SingleLiveEvent<MutableList<String>> {
        if (!::deeplinkPageNameResponse.isInitialized) deeplinkPageNameResponse = SingleLiveEvent()
        return deeplinkPageNameResponse
    }

    fun savePageNameAndRedirect(pageName: String, param: String? = null) {
        if (::deeplinkPageNameResponse.isInitialized) {
            param?.let {
                deeplinkPageNameResponse.postValue(mutableListOf(pageName, param))
            } ?: deeplinkPageNameResponse.postValue(mutableListOf(pageName))
        }
    }

    private lateinit var workshopDialogTrigger: SingleLiveEvent<SelectWorkshopDialogParams>
    fun selectWorkshopDialogTrigger(params: SelectWorkshopDialogParams) {
        workshopDialogTrigger.postValue(params)
    }

    fun provideSelectWSDialogParams(): SingleLiveEvent<SelectWorkshopDialogParams> {
        if (!::workshopDialogTrigger.isInitialized) workshopDialogTrigger = SingleLiveEvent()
        return workshopDialogTrigger
    }

    private lateinit var selectTabParams: SingleLiveEvent<SelectTabParams>
    fun onTabSelected(params: SelectTabParams) {
        selectTabParams.postValue(params)
    }

    fun provideTabSelectedEvent(): SingleLiveEvent<SelectTabParams> {
        if (!::selectTabParams.isInitialized) selectTabParams = SingleLiveEvent()
        return selectTabParams
    }

    private var checkBackPressEvent = SingleLiveEvent<Boolean>()
    fun onCheckBackPressEvent(value: Boolean) {
        checkBackPressEvent.postValue(value)
    }

    fun provideCheckBackPressEvent(): SingleLiveEvent<Boolean> {
        return checkBackPressEvent
    }

    var isUserWalkthroughStarted = false

}