package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.OrderInclusionsRequestResponse
import whitelisted.garage.api.request.UpdateInventoryItemRequest
import whitelisted.garage.api.response.MostUsedPartsResponseModel
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment

class OrderInclusionsViewModel(app: Application, apieRepo: APIRepository) :
    BaseViewModel(app, apieRepo) {

    private lateinit var updateOrderInclusionsResponse: MutableLiveData<Result<ServerResponse<OrderInclusionsRequestResponse>>>
    fun provideUpdateOrderInclusionsResponse(): MutableLiveData<Result<ServerResponse<OrderInclusionsRequestResponse>>> {
        if (!::updateOrderInclusionsResponse.isInitialized) {
            updateOrderInclusionsResponse = MutableLiveData()
        }
        return updateOrderInclusionsResponse
    }

    fun updateOrderInclusionsAPI(request: OrderInclusionsRequestResponse?) {
        viewModelScope.launch {
            val result = apiRepo.updateOrderInclusions(request).awaitAndGet()
            updateOrderInclusionsResponse.postValue(result)
        }
    }

    private lateinit var getOrderInclusionsResponse: MutableLiveData<Result<ServerResponse<List<OrderInclusionsRequestResponse>>>>
    fun provideGetOrderInclusionsResponse(): MutableLiveData<Result<ServerResponse<List<OrderInclusionsRequestResponse>>>> {
        if (!::getOrderInclusionsResponse.isInitialized) {
            getOrderInclusionsResponse = MutableLiveData()
        }
        return getOrderInclusionsResponse
    }

    fun getOrderInclusionsAPI() {
        viewModelScope.launch {
            val result =
                apiRepo.getOrderInclusions(OrderDetailWrapperFragment.orderId).awaitAndGet()
            getOrderInclusionsResponse.postValue(result)
        }
    }

    private lateinit var updateInventoryItemPrice: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideUpdateInventoryPriceResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::updateInventoryItemPrice.isInitialized) {
            updateInventoryItemPrice = MutableLiveData()
        }
        return updateInventoryItemPrice
    }

    fun updatePriceForAddedParts(req: UpdateInventoryItemRequest) {
        viewModelScope.launch {
            val result = apiRepo.updateInventoryItemValue(req).awaitAndGet()
            updateInventoryItemPrice.postValue(result)
        }
    }

    private lateinit var mostUsedPartsResponse: MutableLiveData<Result<ServerResponse<MutableList<MostUsedPartsResponseModel>>>>
    fun provideMostUsedPartsResponse(): MutableLiveData<Result<ServerResponse<MutableList<MostUsedPartsResponseModel>>>> {
        if (!::mostUsedPartsResponse.isInitialized) {
            mostUsedPartsResponse = MutableLiveData()
        }
        return mostUsedPartsResponse
    }

    fun getMostUsedParts(isTwoWheeler:Boolean) {
        viewModelScope.launch {
            val result = apiRepo.getMostUsedPartsRetailerCatalogue(isTwoWheeler).awaitAndGet()
            mostUsedPartsResponse.postValue(result)
        }
    }

}