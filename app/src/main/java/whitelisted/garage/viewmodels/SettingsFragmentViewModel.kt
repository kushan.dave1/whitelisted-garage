package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.storage.StorageReference
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.data.SendSuccessUrl
import whitelisted.garage.api.request.SetPinRequest
import whitelisted.garage.api.request.UpdateProfileRequest
import whitelisted.garage.api.response.GetUserProfileResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM

class SettingsFragmentViewModel(app: Application, apiRepo: APIRepository) :
    BaseViewModel(app, apiRepo) {
    lateinit var storageRef: StorageReference
    lateinit var workshopImageRef: StorageReference

    private lateinit var ledgerResponseModel: MutableLiveData<Result<ServerResponse<GetUserProfileResponse>>>
    private lateinit var updateProfileResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    private lateinit var getOtpResponse: MutableLiveData<Result<ServerResponse<String>>>

    private lateinit var uploadWorkshopImageResponse: MutableLiveData<SendSuccessUrl>
    fun provideUploadWorkshopImageResponse(): MutableLiveData<SendSuccessUrl> {
        if (!::uploadWorkshopImageResponse.isInitialized) uploadWorkshopImageResponse =
            MutableLiveData()
        return uploadWorkshopImageResponse
    }

    fun provideGetOtpResponse(): MutableLiveData<Result<ServerResponse<String>>> {
        if (!::getOtpResponse.isInitialized) {
            getOtpResponse = MutableLiveData()
        }
        return getOtpResponse
    }

    fun callGetOTPAPI(mobileNumber: String) {
        viewModelScope.launch {
            val result = apiRepo.getOTPForProfile(mobileNumber).awaitAndGet()
            getOtpResponse.postValue(result)
        }
    }

    fun getUserProfile(id: String) {
        viewModelScope.launch {
            val result = apiRepo.getUserProfile(id).awaitAndGet()
            ledgerResponseModel.postValue(result)
        }
    }

    fun provideGetUserProfileResponse(): MutableLiveData<Result<ServerResponse<GetUserProfileResponse>>> {
        if (!::ledgerResponseModel.isInitialized) {
            ledgerResponseModel = MutableLiveData()
        }
        return ledgerResponseModel
    }

    fun provideUpdateUserProfileResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::updateProfileResponse.isInitialized) {
            updateProfileResponse = MutableLiveData()
        }
        return updateProfileResponse
    }

    fun updateUserProfile(id: String, updateProfileRequest: UpdateProfileRequest) {
        viewModelScope.launch {
            val result = apiRepo.updateUserProfile(id, updateProfileRequest).awaitAndGet()
            updateProfileResponse.postValue(result)
        }
    }

    //    fun uploadProfilePicture(profilePicture: String?) {
    //        val isSuccessFailure = SendSuccessUrl()
    //        viewModelScope.launch {
    //            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
    //            workshopImageRef =
    //                storageRef.child(AppENUM.RefactoredStrings.EMPLOYEE_IMAGE_PATH + System.currentTimeMillis())
    //            val file = Uri.fromFile(File(profilePicture ?: ""))
    //            val uploadTask = workshopImageRef.putFile(file)
    //            // Register observers to listen for when the download is done or if it fails
    //            uploadTask.addOnFailureListener {
    //                isSuccessFailure.isSuccessFailure = false
    //                isSuccessFailure.url = ""
    //                uploadWorkshopImageResponse.postValue(isSuccessFailure)
    //                // Handle unsuccessful uploads
    //            }.addOnSuccessListener { taskSnapshot ->
    //                workshopImageRef.path.let {
    //                    isSuccessFailure.isSuccessFailure = true
    //                    isSuccessFailure.url = it
    //                    uploadWorkshopImageResponse.postValue(isSuccessFailure)
    //                }
    //            }.addOnCompleteListener {
    //                if (it.isSuccessful) {
    //                    val downloadUri = it.result
    //                }
    //            }
    //        }
    //    }

    private lateinit var getPinResponse: MutableLiveData<Result<ServerResponse<SetPinRequest>>>
    fun provideGetPinResponse(): MutableLiveData<Result<ServerResponse<SetPinRequest>>> {
        if (!::getPinResponse.isInitialized) getPinResponse = MutableLiveData()
        return getPinResponse
    }

    fun getSavedPin() {
        viewModelScope.launch {
            val result =
                apiRepo.getPin(getStringSharedPreference(AppENUM.USER_ID, "")).awaitAndGet()
            getPinResponse.postValue(result)
        }
    }

    private lateinit var setPinResponse: MutableLiveData<Result<ServerResponse<SetPinRequest>>>
    fun provideSetPinResponse(): MutableLiveData<Result<ServerResponse<SetPinRequest>>> {
        if (!::setPinResponse.isInitialized) setPinResponse = MutableLiveData()
        return setPinResponse
    }

    fun setPin(pin: String) {
        val req = SetPinRequest(pin)
        viewModelScope.launch {
            val result =
                apiRepo.setPin(getStringSharedPreference(AppENUM.USER_ID, ""), req).awaitAndGet()

            setPinResponse.postValue(result)
        }
    }

}