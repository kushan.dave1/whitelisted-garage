package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.AddServiceByOwnReq
import whitelisted.garage.api.request.OrderEstimateRequestResponse
import whitelisted.garage.api.request.ServicesItem
import whitelisted.garage.api.request.UpdateInventoryItemRequest
import whitelisted.garage.api.response.*
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM

class OrderEstimatesViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private lateinit var getOrderEstimatesResponse: MutableLiveData<Result<ServerResponse<List<OrderEstimateRequestResponse>>>>
    fun provideGetOrderEstimatesResponse(): MutableLiveData<Result<ServerResponse<List<OrderEstimateRequestResponse>>>> {
        if (!::getOrderEstimatesResponse.isInitialized) {
            getOrderEstimatesResponse = MutableLiveData()
        }
        return getOrderEstimatesResponse
    }

    private lateinit var updateOrderEstimatesResponse: MutableLiveData<Result<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>
    fun provideUpdateOrderEstimatesResponse(): MutableLiveData<Result<ServerResponse<OrderDetailWrapperPostUpdateResponse>>> {
        if (!::updateOrderEstimatesResponse.isInitialized) {
            updateOrderEstimatesResponse = MutableLiveData()
        }
        return updateOrderEstimatesResponse
    }


    private lateinit var packagesListResponse: MutableLiveData<Result<ServerResponse<List<AllPackagesResponseModel>>>>
    fun providePackagesListResponse(): MutableLiveData<Result<ServerResponse<List<AllPackagesResponseModel>>>> {
        if (!::packagesListResponse.isInitialized) {
            packagesListResponse = MutableLiveData()
        }
        return packagesListResponse
    }

    private lateinit var workDoneResponseFromAPI: MutableLiveData<Result<ServerResponse<List<WorkDoneResponseModel>>>>
    fun provideWorkDoneListResponseFromAPI(): MutableLiveData<Result<ServerResponse<List<WorkDoneResponseModel>>>> {
        if (!::workDoneResponseFromAPI.isInitialized) {
            workDoneResponseFromAPI = MutableLiveData()
        }
        return workDoneResponseFromAPI
    }

    private lateinit var servicesOfPackageResponse: MutableLiveData<Result<ServerResponse<List<ServicesItem>>>>
    fun provideServicesOfPackageResponse(): MutableLiveData<Result<ServerResponse<List<ServicesItem>>>> {
        if (!::servicesOfPackageResponse.isInitialized) {
            servicesOfPackageResponse = MutableLiveData()
        }
        return servicesOfPackageResponse
    }

    private lateinit var partsListResponse: MutableLiveData<Result<ServerResponse<List<ServiceResponseModel>>>>
    fun providePartsListResponse(): MutableLiveData<Result<ServerResponse<List<ServiceResponseModel>>>> {
        if (!::partsListResponse.isInitialized) {
            partsListResponse = MutableLiveData()
        }
        return partsListResponse
    }

    fun getServicesOfPackage(packageId: String?) {
        viewModelScope.launch {
            val result = apiRepo.getServicesOfPackage(packageId).awaitAndGet()
            servicesOfPackageResponse.postValue(result)
        }
    }

    fun getServicesAPI(search: String?, isTwoWheeler: Boolean) {
        viewModelScope.launch {
            val partsResult = if (isTwoWheeler)apiRepo.getBikeServicesList(search).awaitAndGet() else apiRepo.getServicesList(search).awaitAndGet()
            partsListResponse.postValue(partsResult)
        }
    }

    fun getOrderEstimatesAPI(orderId: String?) {
        viewModelScope.launch {
            val result = apiRepo.getOrderEstimates(orderId).awaitAndGet()
            getOrderEstimatesResponse.postValue(result)
        }
    }

    fun updateOrderEstimatesAPI(request: OrderEstimateRequestResponse?) {
        viewModelScope.launch {
            val result = apiRepo.updateOrderEstimates(request).awaitAndGet()
            updateOrderEstimatesResponse.postValue(result)
        }
    }

    fun getPackagesAndWorkDoneAPI(isTwoWheeler: Boolean) {
        viewModelScope.launch {
            val packagesResult = if (isTwoWheeler) apiRepo.getCustomPackagesListBike()
                .awaitAndGet() else apiRepo.getExistingPackages().awaitAndGet()
            val workDoneResult = apiRepo.getWorkDoneList().awaitAndGet()
            packagesListResponse.postValue(packagesResult)
            workDoneResponseFromAPI.postValue(workDoneResult)
        }
    }

    private lateinit var customPackagesListResponse: MutableLiveData<Result<ServerResponse<List<AllPackagesResponseModel>>>>
    fun provideCustomPackagesListResponse(): MutableLiveData<Result<ServerResponse<List<AllPackagesResponseModel>>>> {
        if (!::customPackagesListResponse.isInitialized) {
            customPackagesListResponse = MutableLiveData()
        }
        return customPackagesListResponse
    }

    fun getCustomPackagesAPI(isTwoWheeler: Boolean) {
        viewModelScope.launch {
            val result = if (isTwoWheeler) apiRepo.getCustomPackagesListBike()
                .awaitAndGet() else apiRepo.getCustomPackagesList().awaitAndGet()
            customPackagesListResponse.postValue(result)
        }
    }

    private lateinit var updateInventoryItemPrice: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideUpdateInventoryPriceResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::updateInventoryItemPrice.isInitialized) {
            updateInventoryItemPrice = MutableLiveData()
        }
        return updateInventoryItemPrice
    }

    fun updatePriceForAddedParts(req: UpdateInventoryItemRequest) {
        if (getStringSharedPreference(AppENUM.UserKeySaveENUM.SHOP_TYPE,
                AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) == AppENUM.RefactoredStrings.WORKSHOP_CONSTANT) {
            viewModelScope.launch {
                val result = apiRepo.updateInventoryItemValueForWorkshop(req).awaitAndGet()
                updateInventoryItemPrice.postValue(result)
            }
        } else {
            viewModelScope.launch {
                val result = apiRepo.updateInventoryItemValue(req).awaitAndGet()
                updateInventoryItemPrice.postValue(result)
            }
        }
    }

    private lateinit var mostUsedPartsResponse: MutableLiveData<Result<ServerResponse<MutableList<MostUsedPartsResponseModel>>>>
    fun provideMostUsedPartsRetailerCatalogueResponse(): MutableLiveData<Result<ServerResponse<MutableList<MostUsedPartsResponseModel>>>> {
        if (!::mostUsedPartsResponse.isInitialized) {
            mostUsedPartsResponse = MutableLiveData()
        }
        return mostUsedPartsResponse
    }

    fun getMostUsedPartsRetailerCatalogue(isTwoWheeler: Boolean) {
        viewModelScope.launch {
            val result = apiRepo.getMostUsedPartsRetailerCatalogue(isTwoWheeler).awaitAndGet()
            mostUsedPartsResponse.postValue(result)
        }
    }


    private lateinit var mostUsedPartsWorkshopCatalogueResponse: MutableLiveData<Result<ServerResponse<MutableList<MostUsedPartsResponseModel>>>>
    fun provideMostUsedPartsWorkshopCatalogueResponse(): MutableLiveData<Result<ServerResponse<MutableList<MostUsedPartsResponseModel>>>> {
        if (!::mostUsedPartsWorkshopCatalogueResponse.isInitialized) {
            mostUsedPartsWorkshopCatalogueResponse = MutableLiveData()
        }
        return mostUsedPartsWorkshopCatalogueResponse
    }

    fun getMostUsedPartsWorkshopCatalogue(isTwoWheeler: Boolean) {
        viewModelScope.launch {
            val result = apiRepo.getMostUsedPartsWorkshopCatalogue(isTwoWheeler).awaitAndGet()
            mostUsedPartsWorkshopCatalogueResponse.postValue(result)
        }
    }

    suspend fun addServiceByOwn(searchText: String): Boolean = withContext(Dispatchers.IO) {
        return@withContext when (apiRepo.addServiceByOwn(AddServiceByOwnReq(searchText))
            .awaitAndGet()) {
            is Result.Success<*> -> true
            else -> false
        }
    }

    suspend fun deleteCustomBikePart(id: Long?) = withContext(Dispatchers.IO) {
        val path = getBikeServicePath()
        return@withContext when (apiRepo.deleteBikeService(path, id.toString()).awaitAndGet()) {
            is Result.Success -> true
            else -> false
        }
    }

    suspend fun deleteCustomPart(id: Long?): Boolean = withContext(Dispatchers.IO) {
        return@withContext when (apiRepo.deleteCustomPart(id.toString()).awaitAndGet()) {
            is Result.Success -> true
            else -> false
        }
    }

}