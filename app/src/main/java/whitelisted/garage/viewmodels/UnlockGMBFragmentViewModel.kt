package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.UnlockGMBRequest
import whitelisted.garage.api.response.GetGMDResponse
import whitelisted.garage.api.response.GetWebsiteResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.database.repository.RoomRepository
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.SingleLiveEvent

class UnlockGMBFragmentViewModel(val app: Application,
    apiRepository: APIRepository,
    private val roomRepository: RoomRepository) : BaseViewModel(app, apiRepository) {
    private lateinit var getGMD: SingleLiveEvent<Result<ServerResponse<GetGMDResponse>>>
    private lateinit var getGMDWebsite: SingleLiveEvent<Result<ServerResponse<GetWebsiteResponse>>>
    private lateinit var getUnlockGMBResponse: SingleLiveEvent<Result<ServerResponse<JsonElement>>>

    fun provideUnLockGMB(): SingleLiveEvent<Result<ServerResponse<JsonElement>>> {
        if (!::getUnlockGMBResponse.isInitialized) getUnlockGMBResponse = SingleLiveEvent()
        return getUnlockGMBResponse
    }

    fun unLockGMB(unlockGMBRequest: UnlockGMBRequest) {
        viewModelScope.launch {
            val result = apiRepo.unLockGMB(unlockGMBRequest).awaitAndGet()
            getUnlockGMBResponse.postValue(result)

        }
    }

    fun provideGMD(): SingleLiveEvent<Result<ServerResponse<GetGMDResponse>>> {
        if (!::getGMD.isInitialized) getGMD = SingleLiveEvent()
        return getGMD
    }

    fun getGMB() {
        viewModelScope.launch {
            val result = apiRepo.getGMB().awaitAndGet()
            getGMD.postValue(result)

        }

    }

    fun provideGMDWebsite(): SingleLiveEvent<Result<ServerResponse<GetWebsiteResponse>>> {
        if (!::getGMDWebsite.isInitialized) getGMDWebsite = SingleLiveEvent()
        return getGMDWebsite
    }

    fun getGMBWebsite() {
        viewModelScope.launch {
            val result = apiRepo.getWebsite().awaitAndGet()
            getGMDWebsite.postValue(result)

        }

    }
}