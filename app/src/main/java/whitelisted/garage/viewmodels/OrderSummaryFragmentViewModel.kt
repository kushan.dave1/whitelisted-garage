package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.OrderIdResponse
import whitelisted.garage.api.response.OrderSummaryResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class OrderSummaryFragmentViewModel(app: Application, apiRepo: APIRepository) :
    BaseViewModel(app, apiRepo) {

    private lateinit var orderDetailResponse: MutableLiveData<Result<ServerResponse<List<OrderSummaryResponse>>>>
    fun provideOrderDetailResponse(): MutableLiveData<Result<ServerResponse<List<OrderSummaryResponse>>>> {
        if (!::orderDetailResponse.isInitialized) {
            orderDetailResponse = MutableLiveData()
        }
        return orderDetailResponse
    }

    fun getOrderSummary(orderId: String) {
        viewModelScope.launch {
            val result = apiRepo.getOrderSummary(orderId).awaitAndGet()
            orderDetailResponse.postValue(result)
        }
    }

    private lateinit var completePaymentResponse: MutableLiveData<Result<ServerResponse<JSONObject>>>
    fun provideCompletePaymentResponse(): MutableLiveData<Result<ServerResponse<JSONObject>>> {
        if (!::completePaymentResponse.isInitialized) {
            completePaymentResponse = MutableLiveData()
        }
        return completePaymentResponse
    }

    fun markComplete(orderId: String) {
        val orderIdReq = OrderIdResponse()
        orderIdReq.orderId = orderId
        viewModelScope.launch {
            val result = apiRepo.completePayment(orderIdReq).awaitAndGet()
            completePaymentResponse.postValue(result)
        }
    }


}

