package whitelisted.garage.viewmodels

import android.app.Application
import whitelisted.garage.api.APIRepository
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.database.repository.RoomRepository

class CommonSharedViewModel(val app: Application,
    apiRepository: APIRepository,
    val roomRepository: RoomRepository) : BaseViewModel(app, apiRepository) {}