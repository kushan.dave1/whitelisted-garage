package whitelisted.garage.viewmodels

import android.app.Application
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.data.SendSuccessUrl
import whitelisted.garage.api.request.SaveProfileCompletionRequest
import whitelisted.garage.api.response.GSTVeriifyRespoonse
import whitelisted.garage.api.response.login.GetProfileCompletion
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import java.io.ByteArrayOutputStream
import java.io.File

class ProfileFragmentViewModel(app: Application, apiRepo: APIRepository) :
    BaseViewModel(app, apiRepo) {
    lateinit var storageRef: StorageReference
    lateinit var profileImageRef: StorageReference

    private lateinit var getOtpResponse: MutableLiveData<Result<ServerResponse<String>>>

    private lateinit var uploadWorkshopImageResponse: MutableLiveData<SendSuccessUrl>

    fun provideUploadWorkshopImageResponse(): MutableLiveData<SendSuccessUrl> {
        if (!::uploadWorkshopImageResponse.isInitialized) uploadWorkshopImageResponse =
            MutableLiveData()
        return uploadWorkshopImageResponse
    }

    fun provideGetOtpResponse(): MutableLiveData<Result<ServerResponse<String>>> {
        if (!::getOtpResponse.isInitialized) {
            getOtpResponse = MutableLiveData()
        }
        return getOtpResponse
    }

    fun callGetOTPAPI(mobileNumber: String) {
        viewModelScope.launch {
            val result = apiRepo.getOTPForProfile(mobileNumber).awaitAndGet()
            getOtpResponse.postValue(result)
        }
    }

    fun uploadProfilePicture(context:Context, profilePicture: Uri, userId: String) {
        val isSuccessFailure = SendSuccessUrl()
        viewModelScope.launch {
            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
            profileImageRef =
                storageRef.child(AppENUM.RefactoredStrings.PATH_USERS).child("$userId/")
                    .child(AppENUM.RefactoredStrings.PATH_PROFILE)
            val emptyBitmap = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(emptyBitmap)
            canvas.drawColor(ContextCompat.getColor(context, R.color.white))
            var bmp: Bitmap = try {
                if (Build.VERSION.SDK_INT < 28) {
                    MediaStore.Images.Media.getBitmap(context.contentResolver, profilePicture)
                        ?: emptyBitmap
                } else {
                    ImageDecoder.createSource(context.contentResolver, profilePicture).let {
                        ImageDecoder.decodeBitmap(it)
                    }
                }

            } catch (e: Exception) {
                emptyBitmap
            }
            val byteArrayOutputStream = ByteArrayOutputStream()
            bmp = CommonUtils.scaleBitmap(bmp)
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            val data = byteArrayOutputStream.toByteArray()
            val uploadTask =
                profileImageRef.putBytes(data) // Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener {
                isSuccessFailure.isSuccessFailure = false
                isSuccessFailure.url = ""
                uploadWorkshopImageResponse.postValue(isSuccessFailure) // Handle unsuccessful uploads
            }.addOnSuccessListener {
                profileImageRef.downloadUrl.addOnSuccessListener {
                    isSuccessFailure.isSuccessFailure = true
                    isSuccessFailure.url = it.toString()
                    uploadWorkshopImageResponse.postValue(isSuccessFailure)
                }
            }.addOnFailureListener {
                isSuccessFailure.isSuccessFailure = false
                uploadWorkshopImageResponse.postValue(isSuccessFailure)

            }.addOnCanceledListener {
                isSuccessFailure.isSuccessFailure = false
                uploadWorkshopImageResponse.postValue(isSuccessFailure)
            }
        }
    }

    private lateinit var profileResponse: MutableLiveData<Result<ServerResponse<GetProfileCompletion>>>
    fun provideProfileResponse(): MutableLiveData<Result<ServerResponse<GetProfileCompletion>>> {
        if (!::profileResponse.isInitialized) {
            profileResponse = MutableLiveData()
        }
        return profileResponse
    }

    fun getProfileCompletion(startDate: String) {
        viewModelScope.launch {
            val result = apiRepo.getProfileCompletion(startDate).awaitAndGet()
            profileResponse.postValue(result)
        }
    }

    private lateinit var updateProfileResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>

    fun provideUpdateProfileResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::updateProfileResponse.isInitialized) {
            updateProfileResponse = MutableLiveData()
        }
        return updateProfileResponse
    }

    fun updateProfileCompletion(userId: String, request: SaveProfileCompletionRequest) {
        viewModelScope.launch {
            val result = apiRepo.updateProfileCompletion(userId, request).awaitAndGet()
            updateProfileResponse.postValue(result)
        }
    }

    suspend fun checkGST(gstNumber: String) : GSTVeriifyRespoonse? =
        withContext(Dispatchers.IO) {
            return@withContext when (val result =
                apiRepo.verifyGST(gstNumber).awaitAndGet()) {
                is Result.Success -> result.body.data
                else -> null
            }
        }

}