package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.AllPackagesResponseModel
import whitelisted.garage.api.response.PackageIdResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class CreatePackagesFragmentViewModel(application: Application, apiRepository: APIRepository) :
    BaseViewModel(application, apiRepository) {

    private lateinit var packagesListResponse: MutableLiveData<Result<ServerResponse<List<AllPackagesResponseModel>>>>
    fun providePackagesListResponse(): MutableLiveData<Result<ServerResponse<List<AllPackagesResponseModel>>>> {
        if (!::packagesListResponse.isInitialized) {
            packagesListResponse = MutableLiveData()
        }
        return packagesListResponse
    }

    private lateinit var duplicatePackageResponse: MutableLiveData<Result<ServerResponse<PackageIdResponse>>>
    fun provideDuplicatePackageResponse(): MutableLiveData<Result<ServerResponse<PackageIdResponse>>> {
        if (!::duplicatePackageResponse.isInitialized) duplicatePackageResponse = MutableLiveData()
        return duplicatePackageResponse
    }

    fun getExistingPackages() {
        viewModelScope.launch {
            val result = apiRepo.getExistingPackages().awaitAndGet()
            packagesListResponse.postValue(result)
        }
    }

    fun duplicatePackageAPI(packageId: String?) {
        viewModelScope.launch {
            val result = apiRepo.duplicatePackage(packageId).awaitAndGet()
            duplicatePackageResponse.postValue(result)
        }
    }

    suspend fun getCustomPackagesListBike(): List<AllPackagesResponseModel>? =
        withContext(Dispatchers.IO) {
            return@withContext when (val result =
                apiRepo.getCustomPackagesListBike().awaitAndGet()) {
                is Result.Success -> {
                    result.body.data
                }
                else -> {
                    null
                }
            }
        }


}