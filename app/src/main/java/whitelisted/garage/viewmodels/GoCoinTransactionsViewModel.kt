package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.GCTransactionsResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class GoCoinTransactionsViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {


    private lateinit var transactionsResponse: MutableLiveData<Result<ServerResponse<GCTransactionsResponse>>>
    fun provideTransactionsResponse(): MutableLiveData<Result<ServerResponse<GCTransactionsResponse>>> {
        if (!::transactionsResponse.isInitialized) transactionsResponse = MutableLiveData()
        return transactionsResponse
    }

    fun getTransactions(type: String, period: String) {
        viewModelScope.launch {
            val result = apiRepo.getGoCoinTransactions(type, period).awaitAndGet()
            transactionsResponse.postValue(result)
        }
    }

    private lateinit var downloadTransCSVResponse: MutableLiveData<ResponseBody>
    fun provideDownloadTransCSVResponse(): MutableLiveData<ResponseBody> {
        if (!::downloadTransCSVResponse.isInitialized) {
            downloadTransCSVResponse = MutableLiveData()
        }
        return downloadTransCSVResponse
    }

    fun downloadCSV(type: String, period: String) {
        viewModelScope.launch {
            val result = apiRepo.getAllGoCoinTransactionsCSV(type, period).awaitAndGet()
            when (result) {
                is Result.Success -> {
                    downloadTransCSVResponse.postValue(result.body)
                }
                is Result.Failure -> {

                }
            }
        }
    }

}