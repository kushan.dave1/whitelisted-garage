package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.VerifyOtpRequest
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class VerifyOtpFragmentViewModel(app: Application, apiRepo: APIRepository) :
    BaseViewModel(app, apiRepo) {


    private lateinit var getOtpResponse: MutableLiveData<Result<ServerResponse<String>>>
    private lateinit var verifyOtpResponse: MutableLiveData<Result<ServerResponse<Any>>>


    fun provideVerifyOtpResponse(): MutableLiveData<Result<ServerResponse<Any>>> {
        if (!::verifyOtpResponse.isInitialized) {
            verifyOtpResponse = MutableLiveData()
        }
        return verifyOtpResponse
    }

    fun provideGetOtpResponse(): MutableLiveData<Result<ServerResponse<String>>> {
        if (!::getOtpResponse.isInitialized) {
            getOtpResponse = MutableLiveData()
        }
        return getOtpResponse
    }

    fun callVerifyOtpAPI(mobile: String, otp: String) {
        viewModelScope.launch {
            val req = VerifyOtpRequest()
            req.mobile = mobile
            req.otp = otp
            val result = apiRepo.verifyOTPForProfile(req).awaitAndGet()
            verifyOtpResponse.postValue(result)
        }
    }

    fun callGetOTPAPI(mobileNumber: String) {
        viewModelScope.launch {
            val result = apiRepo.getOTPForProfile(mobileNumber).awaitAndGet()
            getOtpResponse.postValue(result)
        }
    }
}