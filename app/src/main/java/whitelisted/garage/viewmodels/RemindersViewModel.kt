package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.GetGoCoinBalanceResponse
import whitelisted.garage.api.response.OrderIdResponse
import whitelisted.garage.api.response.RemindersListResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class RemindersViewModel(application: Application, apiRepository: APIRepository) :
    BaseViewModel(application, apiRepository) {

    private lateinit var remindersListResponse: MutableLiveData<Result<ServerResponse<RemindersListResponse>>>

    //    private lateinit var getGoCoinResponse: MutableLiveData<Result<ServerResponse<GetGoCoinBalanceResponse>>>
    fun provideRemindersListResponse(): MutableLiveData<Result<ServerResponse<RemindersListResponse>>> {
        if (!::remindersListResponse.isInitialized) {
            remindersListResponse = MutableLiveData()
        }
        return remindersListResponse
    }

    //    fun provideGetGoCOinResponse(): MutableLiveData<Result<ServerResponse<GetGoCoinBalanceResponse>>> {
    //        if (!::getGoCoinResponse.isInitialized) {
    //            getGoCoinResponse = MutableLiveData()
    //        }
    //        return getGoCoinResponse
    //    }

    //    fun getGoCoinBalance() {
    //        viewModelScope.launch {
    //            val result = apiRepo.getGoCoinBalance().awaitAndGet()
    //            getGoCoinResponse.postValue(result)
    //        }
    //    }

    private lateinit var sendReminderResponse: MutableLiveData<Result<ServerResponse<OrderIdResponse>>>
    fun provideSendReminderResponse(): MutableLiveData<Result<ServerResponse<OrderIdResponse>>> {
        if (!::sendReminderResponse.isInitialized) {
            sendReminderResponse = MutableLiveData()
        }
        return sendReminderResponse
    }

    fun sendReminder(orderId: String?) {
        viewModelScope.launch {
            val result = apiRepo.sendReminder(orderId).awaitAndGet()
            sendReminderResponse.postValue(result)
        }
    }

    fun scheduleReminder(orderId: String?, scheduleAt: String?) {
        viewModelScope.launch {
            val result = apiRepo.scheduleReminder(orderId, scheduleAt).awaitAndGet()
            sendReminderResponse.postValue(result)
        }
    }

    fun getRemindersList(type: String?, periodType: String) {
        viewModelScope.launch {
            val result = apiRepo.getRemindersList(type, periodType).awaitAndGet()
            remindersListResponse.postValue(result)
        }
    }

    fun searchRemindersList(searchString: String?) {
        viewModelScope.launch {
            val result = apiRepo.searchRemindersList(searchString).awaitAndGet()
            remindersListResponse.postValue(result)
        }
    }

    private lateinit var getGoCoinResponse: MutableLiveData<Result<ServerResponse<GetGoCoinBalanceResponse>>>
    fun provideGetGoCoinResponse(): MutableLiveData<Result<ServerResponse<GetGoCoinBalanceResponse>>> {
        if (!::getGoCoinResponse.isInitialized) {
            getGoCoinResponse = MutableLiveData()
        }
        return getGoCoinResponse
    }
    fun getGoCoinBalance() {
        viewModelScope.launch {
            val result = apiRepo.getGoCoinBalance().awaitAndGet()
            getGoCoinResponse.postValue(result)
        }
    }


}