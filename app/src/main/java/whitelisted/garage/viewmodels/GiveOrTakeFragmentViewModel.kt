package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.AddToLedgerRequest
import whitelisted.garage.api.response.LedgerResponseModel
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class GiveOrTakeFragmentViewModel(app: Application, apiRepo: APIRepository) :
    BaseViewModel(app, apiRepo) {
    private lateinit var addToLedgerResponse: MutableLiveData<Result<ServerResponse<LedgerResponseModel>>>


    fun provideAddLedgerResponse(): MutableLiveData<Result<ServerResponse<LedgerResponseModel>>> {
        if (!::addToLedgerResponse.isInitialized) {
            addToLedgerResponse = MutableLiveData()
        }
        return addToLedgerResponse
    }

    fun callToAddLedgerApi(addToLedgerRequest: AddToLedgerRequest) {
        viewModelScope.launch {
            val result = apiRepo.addLedger(addToLedgerRequest).awaitAndGet()
            addToLedgerResponse.postValue(result)

        }
    }

}