package whitelisted.garage.viewmodels

import android.app.Application
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.data.GetAccessoriesCatalogueResponse
import whitelisted.garage.api.request.AddServiceByOwnReq
import whitelisted.garage.api.response.GetInventoryPartItemResponse
import whitelisted.garage.api.response.ServiceResponseModel
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class SelectRackItemDialogViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    var searchJob: Job? = null

    private lateinit var getPartItemResponse: MutableLiveData<Result<ServerResponse<List<GetInventoryPartItemResponse>>>>
    fun providePartItemResponse(): MutableLiveData<Result<ServerResponse<List<GetInventoryPartItemResponse>>>> {
        if (!::getPartItemResponse.isInitialized) {
            getPartItemResponse = MutableLiveData()
        }
        return getPartItemResponse
    }

    fun getRetailersPartsList(type: String, isTwoWheeler: Boolean) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            val result = if (isTwoWheeler)apiRepo.getRetailersCatalogueBike(type).awaitAndGet() else apiRepo.getRetailersCatalogue(type).awaitAndGet()
            getPartItemResponse.postValue(result)
        }
    }


    private lateinit var partsListResponse: MutableLiveData<Result<ServerResponse<List<ServiceResponseModel>>>>
    fun providePartsListResponse(): MutableLiveData<Result<ServerResponse<List<ServiceResponseModel>>>> {
        if (!::partsListResponse.isInitialized) {
            partsListResponse = MutableLiveData()
        }
        return partsListResponse
    }

    fun getServicesAPI(search: String?, isTwoWheeler: Boolean) {
        Handler(Looper.getMainLooper()).postDelayed({
            searchJob?.cancel()
            searchJob = viewModelScope.launch {
                val partsResult = if (isTwoWheeler) apiRepo.getBikeServicesList(search)
                    .awaitAndGet() else apiRepo.getServicesList(search).awaitAndGet()
                partsListResponse.postValue(partsResult)
            }
        }, 300)

    }

    private lateinit var getAccPartItemResponse: MutableLiveData<Result<ServerResponse<MutableList<GetAccessoriesCatalogueResponse>>>>
    fun provideAccPartItemResponse(): MutableLiveData<Result<ServerResponse<MutableList<GetAccessoriesCatalogueResponse>>>> {
        if (!::getAccPartItemResponse.isInitialized) {
            getAccPartItemResponse = MutableLiveData()
        }
        return getAccPartItemResponse
    }

    fun getAccessoriesPartsList(search: String, isTwoWheeler: Boolean) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            val result = if (isTwoWheeler) apiRepo.getAccessoriesCatalogueBike(search)
                .awaitAndGet() else apiRepo.getAccessoriesCatalogue(search).awaitAndGet()
            getAccPartItemResponse.postValue(result)
        }
    }

    suspend fun addAccessoriesPartByOwn(partName: String): Boolean = withContext(Dispatchers.IO) {
        val req = JsonObject().apply { addProperty("name", partName) }
        val isAdded: Boolean = when (apiRepo.addCustomItemAccessories(req).awaitAndGet()) {
            is Result.Success -> true
            else -> false
        }
        return@withContext isAdded
    }

    suspend fun addSparesRetailerPartByOwn(partName: String): Boolean =
        withContext(Dispatchers.IO) {
            val req = JsonObject().apply {
                addProperty("name", partName)
                addProperty("category_name", partName)
            }
            val isAdded: Boolean = when (apiRepo.addCustomItemSparesRetailer(req).awaitAndGet()) {
                is Result.Success -> true
                else -> false
            }
            return@withContext isAdded
        }

    suspend fun addWorkshopServiceByOwn(searchText: String): Boolean = withContext(Dispatchers.IO) {
        return@withContext when (apiRepo.addServiceByOwn(AddServiceByOwnReq(searchText))
            .awaitAndGet()) {
            is Result.Success<*> -> true
            else -> false
        }
    }

    suspend fun deleteCustomPart(id: String?): Boolean = withContext(Dispatchers.IO) {
        return@withContext when (apiRepo.deleteCustomPart(id ?: "").awaitAndGet()) {
            is Result.Success -> true
            else -> false
        }
    }

    suspend fun deleteAccessoriesCustomPart(id: String?): Boolean = withContext(Dispatchers.IO) {
        return@withContext when (apiRepo.deleteCustomAccessoriesPart(id ?: "").awaitAndGet()) {
            is Result.Success -> true
            else -> false
        }
    }


    suspend fun deleteCustomBikePart(id: Long?) = withContext(Dispatchers.IO) {
        val path = getBikeServicePath()
        return@withContext when (apiRepo.deleteBikeService(path, id.toString()).awaitAndGet()) {
            is Result.Success -> true
            else -> false
        }
    }

    suspend fun deleteRetailersCustomPart(id: String?): Boolean = withContext(Dispatchers.IO) {
        return@withContext when (apiRepo.deleteCustomRetailersCataloguePart(id ?: "")
            .awaitAndGet()) {
            is Result.Success -> true
            else -> false
        }
    }

}