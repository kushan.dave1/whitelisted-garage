package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.GetGMDResponse
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.api.response.login.GetProfileCompletion
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.database.repository.RoomRepository
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.SingleLiveEvent

class AccountFragmentViewModel(val app: Application,
    apiRepository: APIRepository,
    private val roomRepository: RoomRepository) : BaseViewModel(app, apiRepository) {

    private lateinit var workshopListResponseFromRoom: MutableLiveData<List<WorkshopListResponseModel>>

    fun provideWorkshopListResponseFromRoom(): MutableLiveData<List<WorkshopListResponseModel>> {
        if (!::workshopListResponseFromRoom.isInitialized) {
            workshopListResponseFromRoom = MutableLiveData()
        }
        return workshopListResponseFromRoom
    }

    fun getWorkshopsListFromRoom() {
        viewModelScope.launch {
            val result = roomRepository.getWorkshopList()
            if (!::workshopListResponseFromRoom.isInitialized) {
                workshopListResponseFromRoom = MutableLiveData(result)
            } else workshopListResponseFromRoom.postValue(result)

        }
    }

    private lateinit var profileResponse: SingleLiveEvent<Result<ServerResponse<GetProfileCompletion>>>
    fun provideProfileResponse(): SingleLiveEvent<Result<ServerResponse<GetProfileCompletion>>> {
        if (!::profileResponse.isInitialized) {
            profileResponse = SingleLiveEvent()
        }
        return profileResponse
    }

    fun getProfileCompletion(startDate: String) {
        viewModelScope.launch {
            val result = apiRepo.getProfileCompletion(startDate).awaitAndGet()

            val resultGMB = apiRepo.getGMB().awaitAndGet()
            if (::getGMD.isInitialized) {
                getGMD.postValue(resultGMB)
            }
            if (::profileResponse.isInitialized) profileResponse.postValue(result)
        }
    }

    private lateinit var getGMD: SingleLiveEvent<Result<ServerResponse<GetGMDResponse>>>
    fun provideGMD(): SingleLiveEvent<Result<ServerResponse<GetGMDResponse>>> {
        if (!::getGMD.isInitialized) getGMD = SingleLiveEvent()
        return getGMD
    }
}