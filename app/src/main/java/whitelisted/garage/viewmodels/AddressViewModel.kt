package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.AddressData
import whitelisted.garage.base.BaseViewModel

class AddressViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private lateinit var addressData: MutableLiveData<AddressData>
    fun provideAddressDataResponse(): MutableLiveData<AddressData> {
        if (!::addressData.isInitialized) {
            addressData = MutableLiveData()
        }
        return addressData
    }

    fun setAddressData(data: AddressData) {
        if (!::addressData.isInitialized) {
            addressData = MutableLiveData()
        }
        addressData.value = data

    }


}