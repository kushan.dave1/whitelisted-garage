package whitelisted.garage.viewmodels

import android.app.Application
import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.NewOrderRequest
import whitelisted.garage.api.response.EmployeeListResponseModel
import whitelisted.garage.api.response.OrderDetailResponse
import whitelisted.garage.api.response.OrderDetailWrapperPostUpdateResponse
import whitelisted.garage.api.response.PlaceOrderResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment
import whitelisted.garage.view.fragments.orderInventory.OrderInventoryRequest

class NewOrderFragmentViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private var getEmployeesJob:Job?=null
    private lateinit var newOrderRequest: MutableLiveData<NewOrderRequest>
    fun provideNewOrderRequest(): MutableLiveData<NewOrderRequest> {
        if (!::newOrderRequest.isInitialized) {
            newOrderRequest = MutableLiveData()
        }
        return newOrderRequest
    }

    private lateinit var placeOrderResponse: MutableLiveData<Result<ServerResponse<PlaceOrderResponse>>>
    fun providePlaceOrderResponse(): MutableLiveData<Result<ServerResponse<PlaceOrderResponse>>> {
        if (!::placeOrderResponse.isInitialized) {
            placeOrderResponse = MutableLiveData()
        }
        return placeOrderResponse
    }

    private lateinit var updateOrderDetailResponse: MutableLiveData<Result<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>
    fun provideUpdateOrderDetailResponse(): MutableLiveData<Result<ServerResponse<OrderDetailWrapperPostUpdateResponse>>> {
        if (!::updateOrderDetailResponse.isInitialized) {
            updateOrderDetailResponse = MutableLiveData()
        }
        return updateOrderDetailResponse
    }

    private lateinit var orderDetailResponse: MutableLiveData<Result<ServerResponse<OrderDetailResponse>>>
    fun provideOrderDetailResponse(): MutableLiveData<Result<ServerResponse<OrderDetailResponse>>> {
        if (!::orderDetailResponse.isInitialized) {
            orderDetailResponse = MutableLiveData()
        }
        return orderDetailResponse
    }

    private lateinit var employeeResponseResponseModel: MutableLiveData<Result<ServerResponse<List<EmployeeListResponseModel>>>>
    fun provideManageEmployeeResponse(): MutableLiveData<Result<ServerResponse<List<EmployeeListResponseModel>>>> {
        if (!::employeeResponseResponseModel.isInitialized) {
            employeeResponseResponseModel = MutableLiveData()
        }
        return employeeResponseResponseModel
    }

    fun createNewOrder() {
        newOrderRequest.value?.countryCode =
            getStringSharedPreference(AppENUM.UserKeySaveENUM.COUNTRY_CODE,
                AppENUM.RefactoredStrings.defaultCountryCode)
        viewModelScope.launch {
            val result = apiRepo.placeOrderAPI(newOrderRequest.value).awaitAndGet()
            placeOrderResponse.postValue(result)
        }
    }

    fun updateOrderDetail(orderId: String) {
        newOrderRequest.value?.orderId = orderId
        viewModelScope.launch {
            val result = apiRepo.updateOrderDetail(orderId, newOrderRequest.value).awaitAndGet()
            updateOrderDetailResponse.postValue(result)
        }
    }

    fun getOrderDetail(orderId: String?) {
        viewModelScope.launch {
            val result = apiRepo.getOrderDetail(orderId).awaitAndGet()
            orderDetailResponse.postValue(result)
        }
    }

    fun getEmployess() {
        getEmployeesJob?.cancel()
       getEmployeesJob =  viewModelScope.launch {
            val resultEmployees = apiRepo.getRoleBasedEmployees("Mechanic,Owner,Supervisor,Driver")
                .awaitAndGet()
            employeeResponseResponseModel.postValue(resultEmployees)
        }
    }


    //----------inventory------------
    private lateinit var signatureBitmap: MutableLiveData<Bitmap>
    fun provideSignatureBitmap(): MutableLiveData<Bitmap> {
        return if (!::signatureBitmap.isInitialized) {
            signatureBitmap = MutableLiveData<Bitmap>()
            signatureBitmap
        } else signatureBitmap
    }

    private lateinit var inventoryCheckListResponse: MutableLiveData<Result<ServerResponse<List<String>>>>
    fun provideInventoryCheckListResponse(): MutableLiveData<Result<ServerResponse<List<String>>>> {
        if (!::inventoryCheckListResponse.isInitialized) {
            inventoryCheckListResponse = MutableLiveData()
        }
        return inventoryCheckListResponse
    }

    private lateinit var carDocumentsListResponse: MutableLiveData<Result<ServerResponse<List<String>>>>
    fun provideCarDocumentsListResponse(): MutableLiveData<Result<ServerResponse<List<String>>>> {
        if (!::carDocumentsListResponse.isInitialized) {
            carDocumentsListResponse = MutableLiveData()
        }
        return carDocumentsListResponse
    }

    private lateinit var orderInventoryDetailResponse: MutableLiveData<Result<ServerResponse<OrderInventoryRequest>>>
    fun provideInventoryDetailResponse(): MutableLiveData<Result<ServerResponse<OrderInventoryRequest>>> {
        if (!::orderInventoryDetailResponse.isInitialized) {
            orderInventoryDetailResponse = MutableLiveData()
        }
        return orderInventoryDetailResponse
    }

    private lateinit var addInventoryResponse: MutableLiveData<Result<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>
    fun provideAddInventoryResponse(): MutableLiveData<Result<ServerResponse<OrderDetailWrapperPostUpdateResponse>>> {
        if (!::addInventoryResponse.isInitialized) {
            addInventoryResponse = MutableLiveData()
        }
        return addInventoryResponse
    }

    private lateinit var orderInventoryDetailRequest: OrderInventoryRequest
    fun provideInventoryDetailRequest(): OrderInventoryRequest {
        if (!::orderInventoryDetailRequest.isInitialized) {
            orderInventoryDetailRequest = OrderInventoryRequest()
        }
        return orderInventoryDetailRequest
    }


    fun getInventoryCheckListAndCarDocsAPI(isTwoWheeler:Boolean) {
        viewModelScope.launch {
            val result =  apiRepo.getInventoryCheckList(isTwoWheeler).awaitAndGet()
            val result2 = apiRepo.getCarDocsList().awaitAndGet()
            inventoryCheckListResponse.postValue(result)
            carDocumentsListResponse.postValue(result2)
        }
    }

    fun getInventoryDetail() {
        viewModelScope.launch {
            val result =
                apiRepo.getInventoryDetail(OrderDetailWrapperFragment.orderId).awaitAndGet()
            orderInventoryDetailResponse.postValue(result)
        }
    }

    fun addInventory() {
        viewModelScope.launch {
            val result = apiRepo.addInventory(orderInventoryDetailRequest).awaitAndGet()
            addInventoryResponse.postValue(result)
        }
    }

}