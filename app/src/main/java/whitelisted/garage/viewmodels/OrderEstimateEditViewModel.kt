package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.CreateInventoryRackRequest
import whitelisted.garage.api.response.GetInventoryRackResponse
import whitelisted.garage.api.response.RackIdResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class OrderEstimateEditViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private lateinit var getInventoryRackResponse: MutableLiveData<Result<ServerResponse<MutableList<GetInventoryRackResponse>>>>
    fun provideGetInventoryRackResponse(): MutableLiveData<Result<ServerResponse<MutableList<GetInventoryRackResponse>>>> {
        if (!::getInventoryRackResponse.isInitialized) {
            getInventoryRackResponse = MutableLiveData()
        }
        return getInventoryRackResponse
    }

    fun getInventoryRack() {
        viewModelScope.launch {
            val result = apiRepo.getInventoryRack().awaitAndGet()
            getInventoryRackResponse.postValue(result)
        }
    }

    private lateinit var getCreateInventoryRackResponse: MutableLiveData<Result<ServerResponse<RackIdResponse>>>
    fun provideCreateInventoryRackResponse(): MutableLiveData<Result<ServerResponse<RackIdResponse>>> {
        if (!::getCreateInventoryRackResponse.isInitialized) {
            getCreateInventoryRackResponse = MutableLiveData()
        }
        return getCreateInventoryRackResponse
    }

    fun createInventoryRack(type: CreateInventoryRackRequest) {
        viewModelScope.launch {
            val result = apiRepo.createInventoryRack(type).awaitAndGet()
            getCreateInventoryRackResponse.postValue(result)
        }
    }

    private lateinit var getUpdateInventoryRackResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    fun provideUpdateInventoryResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::getUpdateInventoryRackResponse.isInitialized) {
            getUpdateInventoryRackResponse = MutableLiveData()
        }
        return getUpdateInventoryRackResponse
    }

    fun addItemToRack(id: String, req: CreateInventoryRackRequest) {
        viewModelScope.launch {
            val result = apiRepo.addItemToRack(id, req).awaitAndGet()
            getUpdateInventoryRackResponse.postValue(result)
        }
    }

}