package whitelisted.garage.viewmodels

import android.app.Application
import android.net.Uri
import android.util.Log
import android.webkit.MimeTypeMap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.shadow.apache.commons.lang3.text.StrBuilder
import retrofit2.http.Multipart
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.Enquiry
import whitelisted.garage.api.request.GetPaymentModesRequest
import whitelisted.garage.api.request.SSCheckoutRequest
import whitelisted.garage.api.request.ShopApplyCouponReq
import whitelisted.garage.api.response.*
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.database.repository.RoomRepository
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils.roundOff
import whitelisted.garage.utils.FileProcessing
import whitelisted.garage.utils.SingleLiveEvent
import java.io.File

class SparesShopFragmentViewModel(
    app: Application,
    apiRepository: APIRepository,
    private val roomRepository: RoomRepository
) :
    BaseViewModel(app, apiRepository) {

    private var searchItemsJob: Job? = null
    private var getItemsJob: Job? = null
    private var allFilters: List<MarketplaceFilter> = listOf()
    private var enquiryCost = -1
    private var previouslyOrderedItems: List<SSItemModel>? = null

    private var searchAttrs: SearchAndFilterAttrs? = null
    val searchAndFilterAttrs: SearchAndFilterAttrs
        get() = searchAttrs ?: provideSearchAndFilterAttributes()

    private lateinit var sparesMarketplaceFilters: MutableLiveData<SearchAndFilterAttrs>
    fun provideSparesMarketplaceFilters(): LiveData<SearchAndFilterAttrs> {
        if (!::sparesMarketplaceFilters.isInitialized) sparesMarketplaceFilters = MutableLiveData()
        return sparesMarketplaceFilters
    }

    fun updateFilters() = searchAttrs?.let {
        if (!::sparesMarketplaceFilters.isInitialized) sparesMarketplaceFilters = MutableLiveData()
        sparesMarketplaceFilters.postValue(it)
    }

    private lateinit var ssBrandsList: MutableList<SSBrandsResponseModel>
    fun provideSSBrandsList(): MutableList<SSBrandsResponseModel> {
        if (!::ssBrandsList.isInitialized) ssBrandsList = mutableListOf()
        return ssBrandsList
    }

    private lateinit var ssAccBrandsList: MutableList<SSBrandsResponseModel>
    fun provideSSAccBrandsList(): MutableList<SSBrandsResponseModel> {
        if (!::ssAccBrandsList.isInitialized) ssAccBrandsList = mutableListOf()
        return ssAccBrandsList
    }

    private lateinit var ssOriginalSparesBrandsList: MutableList<SSBrandsResponseModel>
    fun provideSSOriginalSparesBrandsList(): MutableList<SSBrandsResponseModel> {
        if (!::ssOriginalSparesBrandsList.isInitialized) ssOriginalSparesBrandsList =
            mutableListOf()
        return ssOriginalSparesBrandsList
    }

    fun setSSBrandsList(list: MutableList<SSBrandsResponseModel>) {
        ssBrandsList = list
    }

    fun setSSAccBrandsList(list: MutableList<SSBrandsResponseModel>) {
        ssAccBrandsList = list
    }

    fun setSSOriginalBrandsList(list: MutableList<SSBrandsResponseModel>) {
        ssOriginalSparesBrandsList = list
    }

    private lateinit var ssHomeBrandsResponse: MutableLiveData<Result<ServerResponse<MutableList<SSBrandsResponseModel>>>>
    fun provideSSHomeBrandsResponse(): MutableLiveData<Result<ServerResponse<MutableList<SSBrandsResponseModel>>>> {
        if (!::ssHomeBrandsResponse.isInitialized) ssHomeBrandsResponse = MutableLiveData()
        return ssHomeBrandsResponse
    }

    private lateinit var ssAccBrandsResponse: MutableLiveData<Result<ServerResponse<MutableList<SSBrandsResponseModel>>>>
    fun provideSSAccBrandsResponse(): MutableLiveData<Result<ServerResponse<MutableList<SSBrandsResponseModel>>>> {
        if (!::ssAccBrandsResponse.isInitialized) ssAccBrandsResponse = MutableLiveData()
        return ssAccBrandsResponse
    }

    private lateinit var ssOriginalBrandsResponse: MutableLiveData<Result<ServerResponse<MutableList<SSBrandsResponseModel>>>>
    fun provideSSOriginalBrandsResponse(): MutableLiveData<Result<ServerResponse<MutableList<SSBrandsResponseModel>>>> {
        if (!::ssOriginalBrandsResponse.isInitialized) ssOriginalBrandsResponse = MutableLiveData()
        return ssOriginalBrandsResponse
    }

    //    fun getSSBrandsResponse() {
    //        viewModelScope.launch {
    //            val result = apiRepo.getSSBrandsResponse().awaitAndGet()
    //            ssHomeBrandsResponse.postValue(result)
    //        }
    //    }

    private lateinit var itemsWithFilterResponse: MutableLiveData<Result<ServerResponse<MutableList<SSItemModel>>>>
    fun provideItemsWithFilterResponse(): MutableLiveData<Result<ServerResponse<MutableList<SSItemModel>>>> {
        if (!::itemsWithFilterResponse.isInitialized) itemsWithFilterResponse = MutableLiveData()
        return itemsWithFilterResponse
    }

    //    fun getSSItemsList(limit: Int, offset: Int) {
    //        getItemsJob = viewModelScope.launch {
    //            val result = apiRepo.getSSItemsWithFilter(limit, offset).awaitAndGet()
    //            itemsWithFilterResponse.postValue(result)
    //        }
    //    }

    private lateinit var addToCartResponse: MutableLiveData<Result<ServerResponse<GetSSCartResponse>>>
    fun provideAddToCartResponse(): MutableLiveData<Result<ServerResponse<GetSSCartResponse>>> {
        if (!::addToCartResponse.isInitialized) addToCartResponse = MutableLiveData()
        return addToCartResponse
    }

    fun addToCart(productId: String?, itemModel: SSItemModel) {
        viewModelScope.launch {
            val result = apiRepo.addToCartSS(productId, itemModel).awaitAndGet()
            getSSCartAPI()
            addToCartResponse.postValue(result)
            getCartResponseAPI.postValue(result)
        }
    }

    private lateinit var getCartResponse: GetSSCartResponse
    fun provideGetCartResponse(): GetSSCartResponse {
        if (!::getCartResponse.isInitialized) getCartResponse = GetSSCartResponse()
        return getCartResponse
    }

    var cartItemChanged: SSItemModel? = null

    fun setGetCartResponse(data: GetSSCartResponse) {
        getCartResponse = data
    }

    private lateinit var getCartResponseAPI: MutableLiveData<Result<ServerResponse<GetSSCartResponse>>>
    fun provideGetCartResponseAPI(): MutableLiveData<Result<ServerResponse<GetSSCartResponse>>> {
        if (!::addToCartResponse.isInitialized) addToCartResponse = MutableLiveData()
        if (!::getCartResponseAPI.isInitialized) getCartResponseAPI = MutableLiveData()
        return getCartResponseAPI
    }

    fun getSSCartAPI() {
        viewModelScope.launch {
            val result = apiRepo.getCartSS().awaitAndGet()
            getCartResponseAPI.postValue(result)
        }
    }

    suspend fun getSSCart() {
        val result = apiRepo.getCartSS().awaitAndGet()
        getCartResponseAPI.postValue(result)
        (result as? Result.Success)?.let { setGetCartResponse(it.body.data) }
    }

    private lateinit var ssCategoriesList: MutableList<SSCategoryResponseModel>
    fun provideSSCategoriesList(): MutableList<SSCategoryResponseModel> {
        if (!::ssCategoriesList.isInitialized) ssCategoriesList = mutableListOf()
        return ssCategoriesList
    }

    fun setSSCategoriesList(list: MutableList<SSCategoryResponseModel>) {
        ssCategoriesList = list
    }

    private lateinit var ssAccCategoriesList: MutableList<SSCategoryResponseModel>
    fun provideSSAccCategoriesList(): MutableList<SSCategoryResponseModel> {
        if (!::ssAccCategoriesList.isInitialized) ssAccCategoriesList = mutableListOf()
        return ssAccCategoriesList
    }

    fun setSSAccCategoriesList(list: MutableList<SSCategoryResponseModel>) {
        ssAccCategoriesList = list
    }

    private lateinit var ssHomeCategoriesResponse: MutableLiveData<Result<ServerResponse<MutableList<SSCategoryResponseModel>>>>
    fun provideSSHomeCategoriesResponse(): MutableLiveData<Result<ServerResponse<MutableList<SSCategoryResponseModel>>>> {
        if (!::ssHomeCategoriesResponse.isInitialized) ssHomeCategoriesResponse = MutableLiveData()
        return ssHomeCategoriesResponse
    }

    private lateinit var ssAccCategoriesResponse: MutableLiveData<Result<ServerResponse<MutableList<SSCategoryResponseModel>>>>
    fun provideSSAccCategoriesResponse(): MutableLiveData<Result<ServerResponse<MutableList<SSCategoryResponseModel>>>> {
        if (!::ssAccCategoriesResponse.isInitialized) ssAccCategoriesResponse = MutableLiveData()
        return ssAccCategoriesResponse
    }

    //    fun getSSCategoriesResponse() {
    //        viewModelScope.launch {
    //            val result = apiRepo.getSSCategoriesResponse().awaitAndGet()
    //            ssHomeCategoriesResponse.postValue(result)
    //        }
    //    }

    fun getSSSparesBrandsResponse() {
        viewModelScope.launch {
            val result = apiRepo.getSSSparesBrandsResponse().awaitAndGet()
            ssHomeBrandsResponse.postValue(result)
        }
    }

    fun getSSSparesCategoriesResponse() {
        viewModelScope.launch {
            val result = apiRepo.getSSSparesCategoriesResponse().awaitAndGet()
            ssHomeCategoriesResponse.postValue(result)
        }
    }

    private lateinit var checkoutSuccessEvent: SingleLiveEvent<Any>
    fun provideCheckoutSuccessEvent(): SingleLiveEvent<Any> {
        if (!::checkoutSuccessEvent.isInitialized) checkoutSuccessEvent = SingleLiveEvent()
        return checkoutSuccessEvent
    }

    private lateinit var checkoutFailureEvent: SingleLiveEvent<Any>
    fun provideCheckoutFailureEvent(): SingleLiveEvent<Any> {
        if (!::checkoutFailureEvent.isInitialized) checkoutFailureEvent = SingleLiveEvent()
        return checkoutFailureEvent
    }

    private lateinit var retryPaymentEvent: SingleLiveEvent<Any>
    fun provideRetryPaymentEvent(): SingleLiveEvent<Any> {
        if (!::retryPaymentEvent.isInitialized) retryPaymentEvent = SingleLiveEvent()
        return retryPaymentEvent
    }

    private lateinit var purchaseHistoryResponse: MutableLiveData<ServerResponse<List<SparesOrderHistory>>>
    fun providePurchaseHistoryResponse(): MutableLiveData<ServerResponse<List<SparesOrderHistory>>> {
        if (!::purchaseHistoryResponse.isInitialized) purchaseHistoryResponse = MutableLiveData()
        return purchaseHistoryResponse
    }

    suspend fun getPurchaseHistory(
        limit: Int,
        offset: Int,
        filter: String?
    ): List<SparesOrderHistory>? {
        val map = mutableMapOf("limit" to limit.toString(), "offset" to offset.toString())
        filter?.let { map["filter"] = it }
        return when (val result = apiRepo.getSparesPurchaseHistory(map).awaitAndGet()) {
            is Result.Success -> {
                purchaseHistoryResponse.postValue(result.body)
                result.body.data
            }
            else -> null
        }
    }

    //    fun getSSItemsListWithBrandFilter(brandName: String?, limit: Int, offset: Int) {
    //        getItemsJob?.cancel()
    //        getItemsJob = viewModelScope.launch {
    //            val result = apiRepo.getSSItemsWithBrandFilter(brandName, limit, offset).awaitAndGet()
    //            itemsWithFilterResponse.postValue(result)
    //        }
    //    }
    //
    //    fun getSSItemsListWithCategoryFilter(categoryName: String?, limit: Int, offset: Int) {
    //        getItemsJob?.cancel()
    //        getItemsJob = viewModelScope.launch {
    //            val result =
    //                apiRepo.getSSItemsWithCategoryFilter(categoryName, limit, offset).awaitAndGet()
    //            itemsWithFilterResponse.postValue(result)
    //        }
    //    }

    private lateinit var ssHomeBannersResponse: MutableLiveData<Result<ServerResponse<MutableList<SSHomeBannersResponseModel>>>>
    fun provideSSHomeBannersResponse(): MutableLiveData<Result<ServerResponse<MutableList<SSHomeBannersResponseModel>>>> {
        if (!::ssHomeBannersResponse.isInitialized) ssHomeBannersResponse = MutableLiveData()
        return ssHomeBannersResponse
    }

    fun getSSHomeBannersResponse() {
        viewModelScope.launch {
            val result = apiRepo.getSSHomeBannersResponse().awaitAndGet()
            ssHomeBannersResponse.postValue(result)
        }
    }

    private lateinit var offlineCheckoutResponse: SingleLiveEvent<Result<ServerResponse<Any>>>
    fun provideOfflineCheckoutResponse(): SingleLiveEvent<Result<ServerResponse<Any>>> {
        if (!::offlineCheckoutResponse.isInitialized) offlineCheckoutResponse = SingleLiveEvent()
        return offlineCheckoutResponse
    }

    fun checkoutOffline(ssCheckoutRequest: SSCheckoutRequest) {
        viewModelScope.launch {
            val result = apiRepo.checkoutSSOrder(ssCheckoutRequest).awaitAndGet()
            offlineCheckoutResponse.postValue(result)
        }
    }

    private lateinit var searchItemsResponse: MutableLiveData<Result<ServerResponse<MutableList<SSItemModel>>>>
    fun provideSearchItemsResponse(): MutableLiveData<Result<ServerResponse<MutableList<SSItemModel>>>> {
        if (!::searchItemsResponse.isInitialized) searchItemsResponse = MutableLiveData()
        return searchItemsResponse
    }

    suspend fun getMarketPlaceCoupons(): List<MarketPlaceCoupon> {
        return when (val result = apiRepo.getMarketPlaceCoupons().awaitAndGet()) {
            is Result.Success -> result.body.data
            else -> listOf()
        }
    }

    suspend fun applyMarketPlaceCoupon(
        cartId: String,
        couponCode: String = "",
        type: String
    ): String {
        val req = ShopApplyCouponReq(cartId, couponCode, type)
        return when (val result = apiRepo.applyMarketPlaceCoupon(req).awaitAndGet()) {
            is Result.Success -> result.body.status.toString()
            is Result.Failure -> result.errorMessage
        }
    }

    suspend fun removeMarketPlaceCoupon(cartId: String): Boolean {
        return when (val result = apiRepo.removeMarketPlaceCoupon(cartId).awaitAndGet()) {
            is Result.Success -> result.body.status
            else -> false
        }
    }

    suspend fun getGoCoinsBalance(): String {
        return when (val result = apiRepo.getGoCoinScreenResponse().awaitAndGet()) {
            is Result.Success -> result.body.data.gocoinBalance.toString()
            else -> ""
        }.also {
            putStringSharedPreference(AppENUM.GO_COIN_BALANCE_AMOUNT, it)
        }
    }

    suspend fun getProductDetails(productId: String): SSItemModel = withContext(Dispatchers.IO) {
        return@withContext when (val result =
            apiRepo.getProductSearchById(productId).awaitAndGet()) {
            is Result.Success -> result.body.data
            else -> SSItemModel()
        }
    }

    //    fun getSSItemsViaCarFilter(brand: String? = "",
    //        category: String? = "",
    //        brandName: String?,
    //        modelName: String?,
    //        limit: Int,
    //        offset: Int) {
    //        getItemsJob?.cancel()
    //        getItemsJob = viewModelScope.launch {
    //            val result = apiRepo.getSSItemsWithCarFilter(brand,
    //                category,
    //                brandName,
    //                modelName,
    //                limit,
    //                offset).awaitAndGet()
    //            itemsWithFilterResponse.postValue(result)
    //        }
    //    }

    suspend fun getPDPBannerImage(): PDPBannerResponse? = withContext(Dispatchers.IO) {
        return@withContext when (val result = apiRepo.getPDPBannerImage().awaitAndGet()) {
            is Result.Success -> result.body.data
            else -> null
        }
    }

    suspend fun getMarketplaceFAQ(): List<FaqsItem> = withContext(Dispatchers.IO) {
        return@withContext when (val result = apiRepo.getMarketplaceFAQ().awaitAndGet()) {
            is Result.Success -> result.body.data
            else -> listOf()
        }
    }

    suspend fun getTrackingUrl(): String? = withContext(Dispatchers.IO) {
        return@withContext when (val result = apiRepo.getTrackingURL().awaitAndGet()) {
            is Result.Success -> {
                if (result.body.data.isNotEmpty()) result.body.data["url"]
                else null
            }
            else -> null
        }
    }

    fun getItemWithAppliedFilters(limit: Int, offset: Int) {
        getItemsJob?.cancel()
        getItemsJob = viewModelScope.launch {
            val filterMap = mutableMapOf<String, String>()

            val brands = StrBuilder()
            searchAttrs?.brands?.forEach { brand ->
                if (brand.isSelected) brands.append(brand.name).append(",")
            }

            if (brands.isNotEmpty()) {
                brands.deleteCharAt(brands.size() - 1)
                filterMap["brand_name"] = brands.toString()
            }

            val categories = StrBuilder()
            searchAttrs?.categories?.forEach { category ->
                category.subCategory.forEach { subCategory ->
                    subCategory.subArray.forEach {
                        if (it.isSelected) categories.append(it.displayName).append(",")
                    }
                }
            }
            if (categories.isNotEmpty()) {
                categories.deleteCharAt(categories.size() - 1)
                filterMap["category_name"] = categories.toString()
            }

            searchAttrs?.filters?.forEach { f ->
                val value = StrBuilder()
                if (f.selectionCount > 0) {
                    val selectedTypes = f.filterTypes.filter { it.isSelected }
                    selectedTypes.forEachIndexed { index, filterType ->
                        value.append(filterType.filterValue)
                        if (index != selectedTypes.size - 1) value.append(",")
                    }
                    filterMap[f.filterKey.toString()] = value.toString()
                }
            }

            val selectedCar = searchAttrs?.car?.find { it.isSelected }

            if (selectedCar != null) {
                filterMap["car_brand"] = selectedCar.brandName
                filterMap["car_model"] = selectedCar.carName
            }


            if (!searchAttrs?.textSearch.isNullOrEmpty()) {
                filterMap.clear()
                filterMap[if (searchAttrs?.isImageSearch == true) "image_search" else "title"] =
                    searchAttrs?.textSearch ?: ""
            }


            filterMap["limit"] = limit.toString()
            filterMap["offset"] = offset.toString()

            val result = apiRepo.getSSPartsListFilter(filterMap).awaitAndGet()
            itemsWithFilterResponse.postValue(result)

        }
    }

    suspend fun getMultipleParts(partIds: String): List<SSItemModel> {
        if (partIds.isEmpty()) return listOf()
        val map = mapOf(
            "search" to partIds,
            "limit" to "10", "offset" to "0"
        )
        val result = apiRepo.getSSPartsListFilter(map).awaitAndGet()
        return if (result is Result.Success) result.body.data else listOf()
    }

    fun getItemsWithFilter(
        marketplaceFilters: List<MarketplaceFilter>? = null,
        brandName: String? = null,
        categoryName: String? = null,
        carBrand: String? = null,
        carModel: String? = null,
        limit: Int,
        offset: Int
    ) {
        getItemsJob?.cancel()
        getItemsJob = viewModelScope.launch {
            val filterMap = mutableMapOf<String, String>()
            marketplaceFilters?.forEach { f ->
                val value = StrBuilder()
                if (f.selectionCount > 0) {
                    val selectedTypes = f.filterTypes.filter { it.isSelected }
                    selectedTypes.forEachIndexed { index, filterType ->
                        value.append(filterType.filterValue)
                        if (index != selectedTypes.size - 1) value.append(",")
                    }
                    filterMap[f.filterKey.toString()] = value.toString()
                }
            }

            filterMap["limit"] = limit.toString()
            filterMap["offset"] = offset.toString()
            if (!categoryName.isNullOrEmpty()) filterMap["category_name"] = categoryName
            if (!carBrand.isNullOrEmpty()) filterMap["car_brand"] = carBrand
            if (!carModel.isNullOrEmpty()) filterMap["car_model"] = carModel
            if (!brandName.isNullOrEmpty()) filterMap["brand_name"] = brandName

            val result = apiRepo.getSSPartsListFilter(filterMap).awaitAndGet()
            itemsWithFilterResponse.postValue(result)
        }
    }

    fun cancelJob() {
        getItemsJob?.cancel()
    }

    private lateinit var notifyMeLiveEvent: SingleLiveEvent<SSItemModel>
    fun provideNotifyMeLiveEvent(): SingleLiveEvent<SSItemModel> {
        if (!::notifyMeLiveEvent.isInitialized) notifyMeLiveEvent = SingleLiveEvent()
        return notifyMeLiveEvent
    }

    suspend fun notifyForProduct(skuCode: String?, title: String?, image: String?): JsonElement? =
        withContext(Dispatchers.IO) {
            return@withContext when (val result = apiRepo.notifyUser(JsonObject().apply {
                addProperty("product_id", skuCode)
                addProperty("title", title)
                addProperty("image", image)
            }).awaitAndGet()) {
                is Result.Success -> {
                    result.body.data
                }
                else -> null
            }
        }

    private lateinit var getNotifiedItems: MutableList<NotifiedItemResponseModel>
    fun provideGetNotifiedItemsResponse(): List<NotifiedItemResponseModel> {
        if (!::getNotifiedItems.isInitialized) getNotifiedItems = mutableListOf()
        return getNotifiedItems
    }

    fun setNotifiedItems(data: MutableList<NotifiedItemResponseModel>) {
        getNotifiedItems = data
    }

    suspend fun getNotifiedItemsAPI(): MutableList<NotifiedItemResponseModel> =
        withContext(Dispatchers.IO) {
            return@withContext when (val result = apiRepo.getNotifiedItemsList().awaitAndGet()) {
                is Result.Success -> {
                    result.body.data
                }
                else -> mutableListOf()
            }
        }

    fun clearMarketplaceFilters() {
        searchAttrs?.apply {
            categories.forEach { c ->
                c.isSelected = false
                c.subCategory.forEach { sc ->
                    sc.isSelected = false
                    sc.subArray.forEach { sa ->
                        sa.isSelected = false
                    }
                }
            }

            brands.forEach { it.isSelected = false }

            car.find { it.isSelected }?.isSelected = false
            filters.forEach {
                it.selectionCount = 0
                it.filterTypes.forEach { ft ->
                    ft.isSelected = false
                }
            }
            categoryCount = 0; brandCount = 0; textSearch = ""
            isImageSearch = false
        }
    }

    private lateinit var searchAndFiltersResponse: MutableLiveData<SearchAndFilterAttrs>
    fun provideSearchAndFiltersResponse(): MutableLiveData<SearchAndFilterAttrs> {
        if (!::searchAndFiltersResponse.isInitialized) searchAndFiltersResponse = MutableLiveData()
        return searchAndFiltersResponse
    }

    suspend fun getSearchAndFilterAttributes(): SearchAndFilterAttrs? =
        withContext(Dispatchers.IO) {
            return@withContext if (searchAttrs == null) when (val result =
                apiRepo.getSearchAndFilterAttributes().awaitAndGet()) {
                is Result.Success -> {
                    searchAttrs = result.body.data
                    searchAttrs?.let {
                        allFilters = it.filters
                        if (::searchAndFiltersResponse.isInitialized) searchAndFiltersResponse.postValue(
                            it
                        )
                        it
                    }
                }
                else -> null
            }
            else searchAttrs
        }

    fun getSSAccBrandsResponse() {
        viewModelScope.launch {
            val result = apiRepo.getSSAccBrandsResponse().awaitAndGet()
            ssAccBrandsResponse.postValue(result)
        }
    }

    fun getSSAccCategoriesResponse() {
        viewModelScope.launch {
            val result = apiRepo.getSSAccCategoriesResponse().awaitAndGet()
            ssAccCategoriesResponse.postValue(result)
        }
    }

    fun getSSOriginalBrandsResponse() {
        viewModelScope.launch {
            val result = apiRepo.getSSOriginalBrandsResponse().awaitAndGet()
            ssOriginalBrandsResponse.postValue(result)
        }
    }

    private lateinit var searchAndFilterAttributes: SearchAndFilterAttrs
    fun provideSearchAndFilterAttributes(): SearchAndFilterAttrs {
        if (!::searchAndFilterAttributes.isInitialized) searchAndFilterAttributes =
            SearchAndFilterAttrs()
        return searchAndFilterAttributes
    }

    fun setSearchAndFilterAttributes(data: SearchAndFilterAttrs?) {
        searchAndFilterAttributes = data ?: SearchAndFilterAttrs()
    }

    fun reset(filters: SearchAndFilterAttrs?) {
        searchAttrs = filters
    }

    private lateinit var paymentTypesResponse: MutableLiveData<List<PaymentTypesResponseModel>>
    fun providePaymentTypesResponse(): MutableLiveData<List<PaymentTypesResponseModel>> {
        if (!::paymentTypesResponse.isInitialized) paymentTypesResponse = MutableLiveData()
        return paymentTypesResponse
    }

    fun setPaymentTypes(paymentTypes: List<PaymentTypesResponseModel>) {
        if (!::paymentTypesResponse.isInitialized)
            paymentTypesResponse = MutableLiveData()
        paymentTypesResponse.value = paymentTypes
    }

    suspend fun getPaymentTypes(req: GetPaymentModesRequest): List<PaymentTypesResponseModel> =
        withContext(Dispatchers.IO) {
            return@withContext when (val result = apiRepo.getPaymentTypes(req).awaitAndGet()) {
                is Result.Success -> {
                    result.body.data
                }
                else -> listOf()
            }
        }

    suspend fun getPaymentTypesGet(): List<PaymentTypesResponseModel> =
        withContext(Dispatchers.IO) {
            return@withContext when (val result = apiRepo.getPaymentTypesGet().awaitAndGet()) {
                is Result.Success -> {
                    result.body.data
                }
                else -> listOf()
            }
        }

    suspend fun getPopularItems(type: String): List<SSItemModel> = withContext(Dispatchers.IO) {
        return@withContext when (val result = apiRepo.getPopularItems(type).awaitAndGet()) {
            is Result.Success -> result.body.data
            else -> listOf()
        }
    }

    suspend fun getEmiAmount(amount: Double): String? = withContext(Dispatchers.IO) {
        return@withContext when (val result = apiRepo.getEmiAmount(amount).awaitAndGet()) {
            is Result.Success -> result.body.data["emi"]?.toDouble().roundOff().toString()
            else -> null
        }

    }

    fun setBrandsSelected(isSparesBrands: Boolean) {
        clearMarketplaceFilters()
        var count = 0
        searchAndFilterAttrs.brands.forEach {
            if (isSparesBrands && it.isAccessories == false) {
                it.isSelected = true; count++
            } else if (!isSparesBrands && it.isAccessories == true) {
                it.isSelected = true; count++
            }
        }
        searchAndFilterAttrs.brandCount = count
    }

    fun setBikeBrandsSelected() {
        clearMarketplaceFilters()
        var count = 0
        searchAndFilterAttrs.brands.forEach {
            if (it.isBike == true) {
                it.isSelected = true; count++
            }
        }
        searchAndFilterAttrs.brandCount = count
    }

    suspend fun buyNowProduct(productId: String?, product: SSItemModel?): IdRequestResponse? =
        withContext(Dispatchers.IO) {
            return@withContext when (val result =
                apiRepo.buyNowProduct(productId, product).awaitAndGet()) {
                is Result.Success -> result.body.data
                else -> null
            }
        }

    private lateinit var sparesHomeResponse: MutableLiveData<Result<ServerResponse<SparesHomeResponseModel>>>
    fun provideSparesHomeResponse(): MutableLiveData<Result<ServerResponse<SparesHomeResponseModel>>> {
        if (!::sparesHomeResponse.isInitialized) sparesHomeResponse = MutableLiveData()
        return sparesHomeResponse
    }

    fun getSparesHomeResponse() {
        viewModelScope.launch {
            val result = apiRepo.getSparesHomeResponse().awaitAndGet()
            sparesHomeResponse.postValue(result)
        }
    }

    suspend fun getMarketplacePdpFAQ(): List<FaqsItem> {
        val result = apiRepo.getMarketplacePdpFAQ().awaitAndGet()
        return if (result is Result.Success) result.body.data else listOf()
    }

    suspend fun sendEnquiry(enquiry: Enquiry): Boolean {
        val result = apiRepo.sendEnquiry(enquiry).awaitAndGet()
        return result is Result.Success
    }

    suspend fun sendOTP(hashcode: String, countryCode: String, mobileNumber: String): String? {
        val result = apiRepo.getOtpForLogin(hashcode, countryCode, mobileNumber).awaitAndGet()
        return (result as? Result.Success)?.body?.data
    }


    suspend fun getAllEnquiries() = when (val result = apiRepo.getAllEnquiries().awaitAndGet()) {
        is Result.Success -> result.body.data
        else -> listOf()
    }

    suspend fun getEnquiryCost() = with(apiRepo.getEnquiryCost().awaitAndGet()) {
        if (enquiryCost == -1)
            enquiryCost = if (this is Result.Success) body.data.getOrDefault("price", 0) else 0
        enquiryCost
    }


    suspend fun removeOOSItems(cartId: String): Boolean = withContext(Dispatchers.IO) {
        val req = JsonObject().apply { addProperty("cart_id", cartId) }
        return@withContext when (apiRepo.removeOOSItems(req).awaitAndGet()) {
            is Result.Success -> true
            else -> false
        }
    }

    suspend fun getRecentItems() = roomRepository.getRecentItems()

    private suspend fun deleteRecentItems() = roomRepository.deleteRecentItems()

    fun insertRecentItem(item: SSItemModel) = viewModelScope.launch(Dispatchers.IO) {
        val items = getRecentItems().toMutableList()
        if (!items.any { it.productId == item.productId } && !item.isEnquiry && (item.inventory
                ?: 0) > 0) {
            deleteRecentItems()
            items.add(0, item)
            roomRepository.insertRecentItems(items.take(if (items.size > 5) 5 else items.size))
        }
    }

    suspend fun getPreviouslyOrderedItems(): List<SSItemModel> {
        if (previouslyOrderedItems == null)
            previouslyOrderedItems =
                when (val result = apiRepo.getPreviouslyAddedItem().awaitAndGet()) {
                    is Result.Success -> result.body.data
                    else -> listOf()
                }
        return previouslyOrderedItems ?: listOf()
    }

    suspend fun addPreviouslyOrdered(items: List<SSItemModel>): Boolean {
        val added = apiRepo.addPrevOrderedItemsToCart(items).awaitAndGet() is Result.Success
        if (added) getSSCart()
        return added
    }

    suspend fun getTextFromImage(file: File): String? {

        val fileReqBody = file.asRequestBody(
            MimeTypeMap.getSingleton().getMimeTypeFromExtension(file.extension)?.toMediaTypeOrNull()
        )
        val part = MultipartBody.Part.createFormData("file", file.name, fileReqBody)
        val token = AppENUM.AI_TOKEN.toRequestBody(MultipartBody.FORM)
        val result = apiRepo.getTextFromImage(part, token).awaitAndGet()
        return if (result is Result.Success) result.body.data[0].text else null

    }

    suspend fun getMembershipConfigs(): MembershipPageConfig? = withContext(Dispatchers.IO) {
        return@withContext when (val result = apiRepo.getMembershipConfig().awaitAndGet()) {
            is Result.Success -> result.body.data
            else -> null
        }
    }

    suspend fun getWorkshopStatus(id: String): WorkshopListResponseModel? {
        val result = apiRepo.getWorkshop(id).awaitAndGet()
        return if (result is Result.Success) result.body.data[0] else {
            Log.i("API_FAIL", (result as? Result.Failure)?.errorMessage ?: "")
            null
        }
    }


}