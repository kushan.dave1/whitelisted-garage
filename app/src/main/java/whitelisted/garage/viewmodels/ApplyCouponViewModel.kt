package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.ApplyCouponResponse
import whitelisted.garage.api.response.GetAllCouponsResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class ApplyCouponViewModel(application: Application, apiRepository: APIRepository) :
    BaseViewModel(application, apiRepository) {


    private lateinit var applyCouponsResponse: MutableLiveData<Result<ServerResponse<ApplyCouponResponse>>>
    private lateinit var getAllCouponsResponse: MutableLiveData<Result<ServerResponse<GetAllCouponsResponse>>>
    fun provideGetAllCouponsResponse(): MutableLiveData<Result<ServerResponse<GetAllCouponsResponse>>> {
        if (!::getAllCouponsResponse.isInitialized) {
            getAllCouponsResponse = MutableLiveData()
        }
        return getAllCouponsResponse
    }

    fun getAllCoupons(type: String) {
        viewModelScope.launch {
            val result = apiRepo.getAllCoupons(type).awaitAndGet()
            getAllCouponsResponse.postValue(result)
        }
    }

    fun provideApplyCouponsResponse(): MutableLiveData<Result<ServerResponse<ApplyCouponResponse>>> {
        if (!::applyCouponsResponse.isInitialized) {
            applyCouponsResponse = MutableLiveData()
        }
        return applyCouponsResponse
    }

    fun applyCoupon(code: String, type: String) {
        viewModelScope.launch {
            val result = apiRepo.applyCoupon(code, type).awaitAndGet()
            applyCouponsResponse.postValue(result)
        }
    }


}