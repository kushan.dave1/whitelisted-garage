package whitelisted.garage.viewmodels

import android.app.Application
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.JobCardRequest
import whitelisted.garage.api.request.ServicesItem
import whitelisted.garage.api.response.*
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class JobCardFragmentViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {
    fun getParts(partName: String): MutableList<PartModel> {
        val partsList = mutableListOf<PartModel>()
        val partItem = PartModel()
        partItem.partName = "GARNISH GEAR SHIT"
        partItem.partDesc = "Interior/Exterior Mirror"
        partsList.add(partItem)
        partsList.add(partItem)
        partsList.add(partItem)
        partsList.add(partItem)
        partsList.add(partItem)
        return partsList
    }

    private lateinit var partsListResponse: MutableLiveData<Result<ServerResponse<List<ServiceResponseModel>>>>
    fun providePartsListResponse(): MutableLiveData<Result<ServerResponse<List<ServiceResponseModel>>>> {
        if (!::partsListResponse.isInitialized) {
            partsListResponse = MutableLiveData()
        }
        return partsListResponse
    }

    private lateinit var packagesListResponse: MutableLiveData<Result<ServerResponse<List<AllPackagesResponseModel>>>>
    fun providePackagesListResponse(): MutableLiveData<Result<ServerResponse<List<AllPackagesResponseModel>>>> {
        if (!::packagesListResponse.isInitialized) {
            packagesListResponse = MutableLiveData()
        }
        return packagesListResponse
    }

    private lateinit var workDoneResponseFromAPI: MutableLiveData<Result<ServerResponse<List<WorkDoneResponseModel>>>>
    fun provideWorkDoneListResponseFromAPI(): MutableLiveData<Result<ServerResponse<List<WorkDoneResponseModel>>>> {
        if (!::workDoneResponseFromAPI.isInitialized) {
            workDoneResponseFromAPI = MutableLiveData()
        }
        return workDoneResponseFromAPI
    }

    fun getPackagesAndWorkDoneAPI() {
        viewModelScope.launch {
            val packagesResult = apiRepo.getExistingPackages().awaitAndGet()
            val workDoneResult = apiRepo.getWorkDoneList().awaitAndGet()
            packagesListResponse.postValue(packagesResult)
            workDoneResponseFromAPI.postValue(workDoneResult)
        }
    }

    fun getServicesAPI(search: String?) {
        Handler(Looper.getMainLooper()).postDelayed({
            viewModelScope.launch {
                val partsResult = apiRepo.getServicesList(search).awaitAndGet()
                partsListResponse.postValue(partsResult)
            }
        }, 300)

    }

    private lateinit var servicesOfPackageResponse: MutableLiveData<Result<ServerResponse<List<ServicesItem>>>>
    fun provideServicesOfPackageResponse(): MutableLiveData<Result<ServerResponse<List<ServicesItem>>>> {
        if (!::servicesOfPackageResponse.isInitialized) {
            servicesOfPackageResponse = MutableLiveData()
        }
        return servicesOfPackageResponse
    }

    private lateinit var addJobCardResponse: MutableLiveData<Result<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>
    fun provideAddJobCardResponse(): MutableLiveData<Result<ServerResponse<OrderDetailWrapperPostUpdateResponse>>> {
        if (!::addJobCardResponse.isInitialized) {
            addJobCardResponse = MutableLiveData()
        }
        return addJobCardResponse
    }

    private lateinit var getJobCardResponse: MutableLiveData<Result<ServerResponse<List<JobCardRequest>>>>
    fun provideGetJobCardResponse(): MutableLiveData<Result<ServerResponse<List<JobCardRequest>>>> {
        if (!::getJobCardResponse.isInitialized) {
            getJobCardResponse = MutableLiveData()
        }
        return getJobCardResponse
    }

    fun getServicesOfPackage(packageId: String?) {
        viewModelScope.launch {
            val result = apiRepo.getServicesOfPackage(packageId).awaitAndGet()
            servicesOfPackageResponse.postValue(result)
        }
    }

    fun addJobCardAPI(jobCarRequest: JobCardRequest?) {
        viewModelScope.launch {
            val result = apiRepo.addJobCard(jobCarRequest).awaitAndGet()
            addJobCardResponse.postValue(result)
        }
    }

    fun getJobCardAPI(orderId: String?) {
        viewModelScope.launch {
            val result = apiRepo.getJobCard(orderId).awaitAndGet()
            getJobCardResponse.postValue(result)
        }
    }

    private lateinit var customPackagesListResponse: MutableLiveData<Result<ServerResponse<List<AllPackagesResponseModel>>>>
    fun provideCustomPackagesListResponse(): MutableLiveData<Result<ServerResponse<List<AllPackagesResponseModel>>>> {
        if (!::customPackagesListResponse.isInitialized) {
            customPackagesListResponse = MutableLiveData()
        }
        return customPackagesListResponse
    }

    fun getCustomPackagesAPI() {
        viewModelScope.launch {
            val result = apiRepo.getCustomPackagesList().awaitAndGet()
            customPackagesListResponse.postValue(result)
        }
    }

}