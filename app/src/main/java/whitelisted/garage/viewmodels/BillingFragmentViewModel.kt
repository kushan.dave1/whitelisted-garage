package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.BillingResponseModel
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class BillingFragmentViewModel(val app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private lateinit var billingListResponse: MutableLiveData<Result<ServerResponse<List<BillingResponseModel>>>>
    fun provideBillingListResponse(): MutableLiveData<Result<ServerResponse<List<BillingResponseModel>>>> {
        if (!::billingListResponse.isInitialized) {
            billingListResponse = MutableLiveData()
        }
        return billingListResponse
    }

    fun getBillingList(startDate: String, endDate: String) {
        viewModelScope.launch {
            val result = apiRepo.getBillingList(startDate, endDate).awaitAndGet()
            billingListResponse.postValue(result)
        }
    }

    private lateinit var downloadCSVResponse: MutableLiveData<ResponseBody>
    fun provideDownloadCSVResponse(): MutableLiveData<ResponseBody> {
        if (!::downloadCSVResponse.isInitialized) {
            downloadCSVResponse = MutableLiveData()
        }
        return downloadCSVResponse
    }

    fun downloadBillingCSV(startDate: String, endDate: String) {
        viewModelScope.launch {
            when (val result = apiRepo.downloadBillingCSV(startDate, endDate).awaitAndGet()) {
                is Result.Success -> {
                    downloadCSVResponse.postValue(result.body)
                }
                is Result.Failure -> {

                }
            }
        }
    }
}