package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.WorkshopIDResponse
import whitelisted.garage.api.response.WorkshopListResponseModel
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.database.repository.RoomRepository
import whitelisted.garage.eventBus.WorkshopAddedEvent
import whitelisted.garage.eventBus.WorkshopDeletedEvent
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class WorkshopListViewModel(application: Application,
    apiRepository: APIRepository,
    private val roomRepository: RoomRepository) : BaseViewModel(application, apiRepository) {

    var isWorkShopDeleted = MutableLiveData<Boolean>()
    var deletedWorkshopId = MutableLiveData<String>()

    init {
        isWorkShopDeleted.value = false
        deletedWorkshopId.value = ""
    }

    private lateinit var workshopListResponseFromAPI: MutableLiveData<Result<ServerResponse<List<WorkshopListResponseModel>>>>
    fun provideWorkshopListResponseFromAPI(): MutableLiveData<Result<ServerResponse<List<WorkshopListResponseModel>>>> {
        if (!::workshopListResponseFromAPI.isInitialized) {
            workshopListResponseFromAPI = MutableLiveData()
        }
        return workshopListResponseFromAPI
    }

    private lateinit var deleteWorkshopResponse: MutableLiveData<Result<ServerResponse<WorkshopIDResponse>>>
    fun provideDeleteWorkshopResponse(): MutableLiveData<Result<ServerResponse<WorkshopIDResponse>>> {
        if (!::deleteWorkshopResponse.isInitialized) {
            deleteWorkshopResponse = MutableLiveData()
        }
        return deleteWorkshopResponse
    }

    fun getWorkshopListAPI() {
        viewModelScope.launch {
            val result = apiRepo.getWorkshopList().awaitAndGet()
            workshopListResponseFromAPI.postValue(result)
        }
    }

    fun addWorkshopsToRoom(workshopList: List<WorkshopListResponseModel>) {
        launch {
            roomRepository.deleteWorkshopList()
            roomRepository.insertWorkshopList(workshopList)

            if (isWorkShopDeleted.value == true) {
                EventBus.getDefault().post(WorkshopDeletedEvent(deletedWorkshopId.value ?: ""))
                isWorkShopDeleted.postValue(false)
            } else {
                EventBus.getDefault().post(WorkshopAddedEvent())
            }

        }
    }

    fun deleteWorkshop(workshopId: String) {
        viewModelScope.launch {
            val result = apiRepo.deleteWorkshop(workshopId).awaitAndGet()
            deleteWorkshopResponse.postValue(result)
        }
    }

}