package whitelisted.garage.viewmodels

import DateUtil
import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.CreateInventoryRackRequest
import whitelisted.garage.api.request.UpdateInventoryItemRequest
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.SingleLiveEvent

class AddRackItemFragmentViewModel(
    val app: Application,
    apiRepository: APIRepository,
) : BaseViewModel(app, apiRepository) {
    private lateinit var getDateRange: SingleLiveEvent<ArrayList<String>>

    fun provideDateYearRange(): SingleLiveEvent<ArrayList<String>> {
        if (!::getDateRange.isInitialized) getDateRange = SingleLiveEvent()
        return getDateRange
    }

    fun getDateYearRange() {
        viewModelScope.launch {
            if (::getDateRange.isInitialized) getDateRange.postValue(DateUtil.giveDateRange())
        }
    }

    private lateinit var getUpdateInventoryRackResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>
    private lateinit var updateInventoryItemPrice: MutableLiveData<Result<ServerResponse<JsonElement>>>

    fun provideUpdateInventoryPriceResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::updateInventoryItemPrice.isInitialized) {
            updateInventoryItemPrice = MutableLiveData()
        }
        return updateInventoryItemPrice
    }

    fun updateInventoryItemValue(type: UpdateInventoryItemRequest) {
        viewModelScope.launch {
            val result = apiRepo.updateInventoryItemValue(type).awaitAndGet()
            updateInventoryItemPrice.postValue(result)
        }
    }

    fun provideUpdateInventoryResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::getUpdateInventoryRackResponse.isInitialized) {
            getUpdateInventoryRackResponse = MutableLiveData()
        }
        return getUpdateInventoryRackResponse
    }

    fun updateInventoryRack(id: String, type: CreateInventoryRackRequest) {
        viewModelScope.launch {
            val result = apiRepo.updateInventoryRack(id, type).awaitAndGet()
            getUpdateInventoryRackResponse.postValue(result)
        }
    }

    fun updateInventoryItemValueForWorkshop(updateInventoryItemRequest: UpdateInventoryItemRequest) {
        viewModelScope.launch {
            val result = apiRepo.updateInventoryItemValueForWorkshop(updateInventoryItemRequest)
                .awaitAndGet()
            updateInventoryItemPrice.postValue(result)
        }
    }
}