package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.SaveUPIIdRequest
import whitelisted.garage.api.response.GetUPIResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class QRCodeFragmentViewModel(application: Application, var apiRepository: APIRepository) :
    BaseViewModel(application, apiRepository) {


    private lateinit var getUPIResponse: MutableLiveData<Result<ServerResponse<GetUPIResponse>>>
    private lateinit var saveUPIResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>

    fun provideSaveUPIResponse(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::saveUPIResponse.isInitialized) {
            saveUPIResponse = MutableLiveData()
        }
        return saveUPIResponse
    }


    fun saveUPIId(saveUPIIdRequest: SaveUPIIdRequest, isFirstTime: Boolean) {
        if (isFirstTime) saveUPIIdRequest.firstTime = isFirstTime
        viewModelScope.launch {
            val result = apiRepository.saveUPIId(saveUPIIdRequest).awaitAndGet()
            saveUPIResponse.postValue(result)
        }
    }

    fun provideGetUPIResponse(): MutableLiveData<Result<ServerResponse<GetUPIResponse>>> {
        if (!::getUPIResponse.isInitialized) {
            getUPIResponse = MutableLiveData()
        }
        return getUPIResponse
    }


    fun getUPIId() {
        viewModelScope.launch {
            val result = apiRepository.getUPIId().awaitAndGet()
            getUPIResponse.postValue(result)
        }
    }


}