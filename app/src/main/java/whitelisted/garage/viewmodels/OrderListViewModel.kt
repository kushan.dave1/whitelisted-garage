package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.OrderListResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class OrderListViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private lateinit var orderListResponse: MutableLiveData<Result<ServerResponse<OrderListResponse>>>
    fun provideOrderListResponse(): MutableLiveData<Result<ServerResponse<OrderListResponse>>> {
        if (!::orderListResponse.isInitialized) {
            orderListResponse = MutableLiveData()
        }
        return orderListResponse
    }

    fun getOrdersListAPI(type: String?) {
        viewModelScope.launch {
            val result = apiRepo.getOrdersList(type).awaitAndGet()
            orderListResponse.postValue(result)
        }

    }


}