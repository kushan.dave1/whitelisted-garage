package whitelisted.garage.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonElement
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.AvailReferralRequest
import whitelisted.garage.api.response.GetReferralCodeResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet

class ReferAndEarnFragmentViewModel(val app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {


    private lateinit var getReferralCodeResponse: MutableLiveData<Result<ServerResponse<GetReferralCodeResponse>>>
    private lateinit var availReferralCodeResponse: MutableLiveData<Result<ServerResponse<JsonElement>>>


    fun provideAvailReferralCode(): MutableLiveData<Result<ServerResponse<JsonElement>>> {
        if (!::availReferralCodeResponse.isInitialized) {
            availReferralCodeResponse = MutableLiveData()
        }
        return availReferralCodeResponse
    }

    fun provideReferralCode(): MutableLiveData<Result<ServerResponse<GetReferralCodeResponse>>> {
        if (!::getReferralCodeResponse.isInitialized) {
            getReferralCodeResponse = MutableLiveData()
        }
        return getReferralCodeResponse
    }

    fun getReferralCode() {
        viewModelScope.launch {
            val result = apiRepo.getReferralCode().awaitAndGet()
            getReferralCodeResponse.postValue(result)
        }
    }

    fun getReferralCode(req: AvailReferralRequest) {
        viewModelScope.launch {
            val result = apiRepo.availReferCode(req).awaitAndGet()
            availReferralCodeResponse.postValue(result)
        }
    }


}