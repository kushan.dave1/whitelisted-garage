package whitelisted.garage.viewmodels

import DateUtil
import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.RackItem
import whitelisted.garage.api.response.GetInventoryRackResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.SingleLiveEvent

class RetailOrderAddPartViewModel(app: Application, apiRepo: APIRepository) :
    BaseViewModel(app, apiRepo) {

    private lateinit var getDateRange: SingleLiveEvent<ArrayList<String>>

    fun provideDateYearRange(): SingleLiveEvent<ArrayList<String>> {
        if (!::getDateRange.isInitialized) getDateRange = SingleLiveEvent()
        return getDateRange
    }


    fun getDateYearRange() {
        viewModelScope.launch {
            getDateRange.postValue(DateUtil.giveDateRange())
        }
    }

    private var inventoryItems: MutableList<RackItem>? = null
    fun getInventoryItems(): MutableList<RackItem>? {
        return if (inventoryItems == null) {
            inventoryItems = mutableListOf()
            inventoryItems
        } else inventoryItems
    }

    private lateinit var getInventoryRackResponse: MutableLiveData<Result<ServerResponse<MutableList<GetInventoryRackResponse>>>>
    fun provideInventoryRackResponse(): MutableLiveData<Result<ServerResponse<MutableList<GetInventoryRackResponse>>>> {
        if (!::getInventoryRackResponse.isInitialized) {
            getInventoryRackResponse = MutableLiveData()
        }
        return getInventoryRackResponse
    }

    fun getInventoryRack() {
        viewModelScope.launch {
            val result = apiRepo.getInventoryRack().awaitAndGet()
            getInventoryRackResponse.postValue(result)
        }
    }

}