package whitelisted.garage.viewmodels

import android.app.Application
import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.response.OrderDetailWrapperPostUpdateResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.view.fragments.orderDetail.OrderDetailWrapperFragment
import whitelisted.garage.view.fragments.orderInventory.OrderInventoryRequest

class InventoryFragmentViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    private lateinit var signatureBitmap: MutableLiveData<Bitmap>
    fun provideSignatureBitmap(): MutableLiveData<Bitmap> {
        return if (!::signatureBitmap.isInitialized) {
            signatureBitmap = MutableLiveData<Bitmap>()
            signatureBitmap
        } else signatureBitmap
    }

    private lateinit var inventoryCheckListResponse: MutableLiveData<Result<ServerResponse<List<String>>>>
    fun provideInventoryCheckListResponse(): MutableLiveData<Result<ServerResponse<List<String>>>> {
        if (!::inventoryCheckListResponse.isInitialized) {
            inventoryCheckListResponse = MutableLiveData()
        }
        return inventoryCheckListResponse
    }

    private lateinit var carDocumentsListResponse: MutableLiveData<Result<ServerResponse<List<String>>>>
    fun provideCarDocumentsListResponse(): MutableLiveData<Result<ServerResponse<List<String>>>> {
        if (!::carDocumentsListResponse.isInitialized) {
            carDocumentsListResponse = MutableLiveData()
        }
        return carDocumentsListResponse
    }

    private lateinit var orderInventoryDetailResponse: MutableLiveData<Result<ServerResponse<OrderInventoryRequest>>>
    fun provideInventoryDetailResponse(): MutableLiveData<Result<ServerResponse<OrderInventoryRequest>>> {
        if (!::orderInventoryDetailResponse.isInitialized) {
            orderInventoryDetailResponse = MutableLiveData()
        }
        return orderInventoryDetailResponse
    }

    private lateinit var addInventoryResponse: MutableLiveData<Result<ServerResponse<OrderDetailWrapperPostUpdateResponse>>>
    fun provideAddInventoryResponse(): MutableLiveData<Result<ServerResponse<OrderDetailWrapperPostUpdateResponse>>> {
        if (!::addInventoryResponse.isInitialized) {
            addInventoryResponse = MutableLiveData()
        }
        return addInventoryResponse
    }

    private lateinit var orderInventoryDetailRequest: OrderInventoryRequest
    fun provideInventoryDetailRequest(): OrderInventoryRequest {
        if (!::orderInventoryDetailRequest.isInitialized) {
            orderInventoryDetailRequest = OrderInventoryRequest()
        }
        return orderInventoryDetailRequest
    }


//    fun getInventoryCheckListAndCarDocsAPI() {
//        viewModelScope.launch {
//            val result = apiRepo.getInventoryCheckList().awaitAndGet()
//            val result2 = apiRepo.getCarDocsList().awaitAndGet()
//            inventoryCheckListResponse.postValue(result)
//            carDocumentsListResponse.postValue(result2)
//        }
//    }

    fun getInventoryDetail() {
        viewModelScope.launch {
            val result =
                apiRepo.getInventoryDetail(OrderDetailWrapperFragment.orderId).awaitAndGet()
            orderInventoryDetailResponse.postValue(result)
        }
    }

    fun addInventory() {
        viewModelScope.launch {
            val result = apiRepo.addInventory(orderInventoryDetailRequest).awaitAndGet()
            addInventoryResponse.postValue(result)
        }
    }


}