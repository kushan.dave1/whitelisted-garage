package whitelisted.garage.viewmodels

import android.app.Application
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import whitelisted.garage.BuildConfig
import whitelisted.garage.R
import whitelisted.garage.api.APIRepository
import whitelisted.garage.api.request.AddWorkshopRequest
import whitelisted.garage.api.response.GSTVeriifyRespoonse
import whitelisted.garage.api.response.WorkshopIDResponse
import whitelisted.garage.base.BaseViewModel
import whitelisted.garage.network.Result
import whitelisted.garage.network.ServerResponse
import whitelisted.garage.network.awaitAndGet
import whitelisted.garage.utils.AppENUM
import whitelisted.garage.utils.CommonUtils
import java.io.ByteArrayOutputStream
import java.io.File

class AddWorkshopViewModel(app: Application, apiRepository: APIRepository) :
    BaseViewModel(app, apiRepository) {

    lateinit var storageRef: StorageReference
    lateinit var workshopImageRef: StorageReference

    private lateinit var addWorkshopResponse: MutableLiveData<Result<ServerResponse<WorkshopIDResponse>>>
    fun provideAddWorkshopResponse(): MutableLiveData<Result<ServerResponse<WorkshopIDResponse>>> {
        if (!::addWorkshopResponse.isInitialized) addWorkshopResponse = MutableLiveData()
        return addWorkshopResponse
    }

    private lateinit var uploadWorkshopImageResponse: MutableLiveData<String>
    fun provideUploadWorkshopImageResponse(): MutableLiveData<String> {
        if (!::uploadWorkshopImageResponse.isInitialized) uploadWorkshopImageResponse =
            MutableLiveData()
        return uploadWorkshopImageResponse
    }

    fun addWorkshopAPI(addWorkshopRequest: AddWorkshopRequest) {
        viewModelScope.launch {
            val result = apiRepo.addWorkshop(addWorkshopRequest).awaitAndGet()
            addWorkshopResponse.postValue(result)
        }
    }

    fun uploadProfilePicture(context: Context, profilePicture: Uri, workshopId: String?) {
        viewModelScope.launch {
            storageRef = FirebaseStorage.getInstance(BuildConfig.FIREBASE_DATA_STORE).reference
            workshopImageRef = storageRef.child(AppENUM.RefactoredStrings.PATH_USERS)
                .child(getStringSharedPreference(AppENUM.USER_ID) + "/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOPS).child("$workshopId/")
                .child(AppENUM.RefactoredStrings.PATH_WORKSHOP_IMAGE).child("$workshopId")
            val emptyBitmap = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(emptyBitmap)
            canvas.drawColor(ContextCompat.getColor(context, R.color.white))
            var bmp: Bitmap = try {
                if (Build.VERSION.SDK_INT < 28) {
                    MediaStore.Images.Media.getBitmap(context.contentResolver, profilePicture)
                        ?: emptyBitmap
                } else {
                    ImageDecoder.createSource(context.contentResolver, profilePicture).let {
                        ImageDecoder.decodeBitmap(it)
                    }
                }

            } catch (e: Exception) {
                emptyBitmap
            }
            val byteArrayOutputStream = ByteArrayOutputStream()
            bmp = CommonUtils.scaleBitmap(bmp)
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            val data = byteArrayOutputStream.toByteArray()
            val uploadTask =
                workshopImageRef.putBytes(data)
//            val file = Uri.fromFile(File(profilePicture ?: ""))
//            val uploadTask =
//                workshopImageRef.putFile(file) // Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener {
                val result = it.message
                uploadWorkshopImageResponse.postValue(result) // Handle unsuccessful uploads
            }.addOnSuccessListener { taskSnapshot ->
                val result = ""
                uploadWorkshopImageResponse.postValue(result) // taskSnapshot.metadata contains file metadata such as size, content-type, etc.
                // ...
            }.addOnCompleteListener {
                if (it.isSuccessful) {
                    val downloadUri = it.result
                }
            }
        }
    }

    suspend fun checkGST(gstNumber: String) : GSTVeriifyRespoonse? =
        withContext(Dispatchers.IO) {
            return@withContext when (val result =
                apiRepo.verifyGST(gstNumber).awaitAndGet()) {
                is Result.Success -> result.body.data
                else -> null
            }
        }

}