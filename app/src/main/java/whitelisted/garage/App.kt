package whitelisted.garage

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.os.Build.VERSION.SDK_INT
import coil.Coil
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.decode.SvgDecoder
import coil.disk.DiskCache
import coil.memory.MemoryCache
import com.facebook.FacebookSdk
import com.facebook.LoggingBehavior
import com.facebook.stetho.Stetho
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import com.google.firebase.storage.FirebaseStorage
import com.moengage.core.LogLevel
import com.moengage.core.MoEngage
import com.moengage.core.config.FcmConfig
import com.moengage.core.config.LogConfig
import com.moengage.core.config.NotificationConfig
import com.razorpay.Checkout
import io.branch.referral.Branch
import org.koin.android.ext.koin.androidContext
import whitelisted.garage.di.Modules
import whitelisted.garage.utils.FirebaseAnalyticsLog
import whitelisted.garage.utils.RuntimeLocaleChanger

class App : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: App? = null

        fun applicationContext(): Context? {
            return instance?.applicationContext
        }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(RuntimeLocaleChanger.wrapContext(base))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        RuntimeLocaleChanger.overrideLocale(this)
    }

    override fun onCreate() {
        super.onCreate()
        startModule()

        FirebaseAnalyticsLog.init(this)
        Stetho.initializeWithDefaults(this)
        MoEngage.initialiseDefaultInstance(MoEngage.Builder(this,
            BuildConfig.MOENGAGE_ID) // .configureMiPush(MiPushConfig(BuildConfig.MOENGAGE_ID, BuildConfig.MOENGAGE_APP_KEY,true))
            .configureLogs(LogConfig(LogLevel.VERBOSE, true))
            .configureNotificationMetaData(NotificationConfig(R.drawable.notification_icon_new,
                R.drawable.notification_icon_new,
                R.color.colorPrimary,
                isMultipleNotificationInDrawerEnabled = true,
                isBuildingBackStackEnabled = true,
                isLargeIconDisplayEnabled = true)) //                .setDataCenter(DataCenter.DATA_CENTER_1)
            .configureFcm(FcmConfig(false)).build())


        try {

            val remoteConfig = Firebase.remoteConfig //Set default cache duration to 3 hours
            val configSettings = remoteConfigSettings {
                minimumFetchIntervalInSeconds = 60
            }
            remoteConfig.setConfigSettingsAsync(configSettings)
            remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
            FirebaseStorage.getInstance().maxUploadRetryTimeMillis = 15000L

        } catch (e: Exception) { //Nothing we can dp
        }

        // FacebookSdk.sdkInitialize(this)
        FacebookSdk.setAutoLogAppEventsEnabled(true)
        FacebookSdk.fullyInitialize()
        FacebookSdk.setIsDebugEnabled(true)
        FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS) // set coil image loader
        setCoilImageLoader()
        Branch.getAutoInstance(this)
        Branch.enableLogging()
        Checkout.preload(applicationContext()) //sets Coil ImageLoader (Singleton) : has its own bitmap pool and cache

        //         disable crashlytics in debug
        //        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(!BuildConfig.DEBUG)
    }

    private fun startModule() {
        org.koin.core.context.startKoin {
            androidContext(this@App)
            modules(Modules.getAll())
        }
    }

    private fun setCoilImageLoader() {
        val imageLoader = coil.ImageLoader.Builder(this@App).memoryCache {
            MemoryCache.Builder(this).maxSizePercent(0.25).build()
        }.diskCache {
            DiskCache.Builder().directory(this.cacheDir.resolve("image_cache")).maxSizePercent(0.02)
                .build()
        }.components {
            add(SvgDecoder.Factory())
            if (SDK_INT >= 28) {
                add(ImageDecoderDecoder.Factory())
            } else {
                add(GifDecoder.Factory())
            }
        }.build()
        Coil.setImageLoader(imageLoader)
    }

    fun reStartModule() {
        org.koin.core.context.stopKoin()
        org.koin.core.context.startKoin {
            androidContext(this@App)
            modules(Modules.getAll())
        }
    }

    fun getContext(): Context {
        return applicationContext
    }

}